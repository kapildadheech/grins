#JAVAAGENT=-agentlib:hprof=cpu=samples,depth=10,file=app.prof.txt

ENV_FILE="/usr/local/grins/setEnv.sh";
ALT_ENV_FILE="/usr/local/gramvaani-automation/setEnv.sh";

if [ -e ${ENV_FILE} ]; then
	. ${ENV_FILE}
else 
    if [ -e ${ALT_ENV_FILE} ]; then
	. ${ALT_ENV_FILE}
    fi
fi

if [ `echo $JAVAPATH | wc -w` -eq 0 ]; then
	JAVAPATH=java;
fi

export LD_LIBRARY_PATH=lib
LIBDIR=lib
CLASSDIR=classes
JARS="asterisk-java looks-2.2.1 log4j-1.2.15 gstreamer-java-bin-1.0 jna-3.0.4 mysql-connector-java-5.1.7-bin lucene-core-2.4.0 jcalendar-1.3.2 forms-1.3.0 poi-3.7-20101029 commons-codec-1.5 jcommon-1.0.17 jfreechart-1.0.14"


case $(uname) in
    CYGWIN*) 
	SEPARATOR=";" 
	if [ "$1" == "app.RSApp" ]; then
		rm -f ${GRINS_LOG_PATH_CYG}/jakarta_service_*
	fi
	;;
    *) SEPARATOR=":" ;;
esac

#[ "$1" == "app.RSApp" ] && [ -f jcarder.jar ] && JAVAAGENT=-javaagent:jcarder.jar
#[ "$1" == "rscontroller.RSController" ] && [ -f jcarder.jar ] && JAVAAGENT=-javaagent:jcarder.jar
#[ "$3" == "LIBRARY_MACHINE" ] && [ -f jcarder.jar ] && JAVAAGENT=-javaagent:jcarder.jar
#[ "$3" == "AUDIO_RSAPP_MACHINE" ] && [ -f jcarder.jar ] && JAVAAGENT=-javaagent:jcarder.jar
#[ "$3" == "TELEPHONY_MACHINE" ] && [ -f jcarder.jar ] && JAVAAGENT=-javaagent:jcarder.jar

for JAR in $JARS
do
    CLASSPATH="${CLASSPATH}${SEPARATOR}${LIBDIR}/${JAR}.jar"
done

CLASSPATH="${LIBDIR}${SEPARATOR}${CLASSDIR}${SEPARATOR}${CLASSPATH}"


"$JAVAPATH" $JAVAAGENT -cp "${CLASSPATH}" -Xmx512M -Xms512M org.gramvaani.radio.${@}
