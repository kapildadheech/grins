create database gramvaanilib;
connect gramvaanilib;

create table RadioProgramMetadata (
	_itemID varchar(64) not null,
	_level int not null,
	_parentID varchar(64),
	_type varchar(16) not null,
	_creationTime bigint,
	_length bigint,
	_state int,
	_indexed int,
	_checksum varchar(64),
	_language varchar(16) not null,
	_description varchar(1024),
	_title varchar(128),
	_licenseType varchar(128),
	_playoutStoreAttempts int,
	_previewStoreAttempts int,
	_uploadStoreAttempts int,
	_archiveStoreAttempts int,
	_telephonyStoreAttempts int,
	primary key(_itemID)
) ENGINE=innodb;

create table Category (
	_nodeID int not null auto_increment primary key,
	_nodeName varchar(64) not null,
	_label varchar(64),
	_parentID int,
	unique(_nodeName)
) ENGINE=innodb;

create table ItemCategory ( 
	_itemID varchar(64) not null, 
	_categoryNodeID int not null,
	unique(_itemID, _categoryNodeID)
) ENGINE=innodb;

create table Entity (
	_entityID int not null auto_increment primary key,
	_name varchar(64) not null,
	_url varchar(64),
	_phoneType varchar(32),
	_phone varchar(32),
	_email varchar(32),
	_location varchar(32),
	_entityType varchar(32),
	unique(_name, _location, _entityType)
) ENGINE=innodb;

create table Creator (
	_itemID varchar(64) not null, 
	_entityID int not null,
	_affiliation varchar(32),
	unique(_itemID, _entityID)
) ENGINE=innodb;

create table PlayoutHistory (
	_itemID varchar(64) not null,
	_stationName varchar(32) not null,
	_telecastTime bigint not null,
	unique(_itemID, _telecastTime)
) ENGINE=innodb;

create table RelatedContent (
	_itemID1 varchar(64) not null,
	_itemID2 varchar(64) not null,
	_relationship varchar(32) not null,
	unique(_itemID1, _itemID2, _relationship)
) ENGINE=innodb;

create table Tags (
	_itemID varchar(64) not null,
	_startOffset int not null, 
	_endOffset int not null default -1,
	_tagsCSV varchar(64) not null
) ENGINE=innodb;

create table EncodedFiles (
	_oldItemID varchar(64) not null,
	_newItemID varchar(64) not null,
	unique(_oldItemID)
) ENGINE=innodb;

create table Playlist (
	_name varchar(64) not null,
	_playlistID int not null auto_increment primary key,
	_startTime bigint not null,
	unique(_name)
) ENGINE=innodb;
 
create table PlaylistItem (
	_playlistID int not null,
	_itemID varchar(64) not null,
	_order int not null,
	_liveItem int,
	_playBackground int,
	_liveDuration bigint,
	_fadeout bigint,
	unique(_playlistID, _itemID, _order)
) ENGINE=innodb;

create table Hotkey (
       _keyID int not null,
       _itemID varchar(64) not null,
       _color int not null,
       unique(_itemID)
) ENGINE=innodb;

create table SMS (
       _smsID int not null auto_increment primary key,
       _phoneNo varchar(32) not null,
       _message varchar(256) not null,
       _dateTime bigint not null,
       _smsLine varchar(32),
       _type int not null,
       _status int
) ENGINE=innodb;

create table Poll (
        _pollID int not null auto_increment primary key,
        _pollName varchar(256) not null,
        _keyword varchar(8) not null,
	_pollType int not null,
	_state int not null,
        _startTime bigint,
	_endTime bigint,
        unique(_pollName)
) ENGINE=innodb;

create table PollOption (
       _pollOptionID int not null auto_increment primary key,
       _pollID int not null,
       _pollOptionName varchar(32) not null,
       _pollOptionCode varchar(8) not null
) ENGINE=innodb;

create table Vote (
       _voteID int not null auto_increment primary key,
       _smsLine varchar(32) not null,
       _phoneNo varchar(32) not null,
       _dateTime bigint not null,
       _text varchar(256),
       _pollID int not null,
       _pollOptionID int,
       _pollOptionText varchar(256)
) ENGINE=innodb;

create user 'grins'@'localhost' identified by 'junoon';
create user 'grins'@'%' identified by 'junoon';
grant all on gramvaanilib.* to 'grins'@'localhost';
grant all on gramvaanilib.* to 'grins'@'%';

