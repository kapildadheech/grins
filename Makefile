empty:=
space:=$(empty) $(empty)
JAVA_CMD := javac
#OPTS=-Xlint:deprecation -Xlint:unchecked
OPTS := -Xlint:-serial
JAVAC=$(JAVA_CMD) $(OPTS)
CLASSD := classes
LIBD := lib
UNAME := $(shell uname -o)
ifeq ($(UNAME),Cygwin)
	SEP=;
else
	SEP=:
endif
JARS := asterisk-java j2ee cos log4j-1.2.15 gstreamer-java-bin-1.0 jna-3.0.4 lucene-core-2.4.0 jcalendar-1.3.2 forms-1.3.0 poi-3.7-20101029 commons-codec-1.5 jcommon-1.0.17 jfreechart-1.0.14 
JARPATH := $(foreach jar, $(JARS), $(LIBD)/$(jar).jar$(SEP))
JARPATH := $(subst $(space),$(empty),$(JARPATH))

CP= -cp "$(CLASSD)$(SEP)$(LIBD)$(SEP)$(JARPATH)."
CLASSDOPT= -d $(CLASSD)
CC=gcc
CCLIB=-ljack
CCOPT= -o $(CLASSD)


all:  radio #jackclient

clean:
	rm -rf classes/org/gramvaani/*

util:
	mkdir -p $(CLASSD)
	$(JAVAC) $(CLASSDOPT) $(CP) src/org/gramvaani/utilities/*java src/org/gramvaani/utilities/processmonitor/*java src/org/gramvaani/utilities/singleinstance/*java

diagnostics: 
	mkdir -p $(CLASSD)
	$(JAVAC) $(CLASSDOPT) $(CP) src/org/gramvaani/radio/diagnostics/*java
upgrader:
	mkdir -p $(CLASSD)
	$(JAVAC) $(CLASSDOPT) $(CP) src/org/gramvaani/radio/upgrader/*java src/org/gramvaani/radio/upgrader/upgrader1/*java
radio: 
	mkdir -p $(CLASSD)
	$(JAVAC) $(CLASSDOPT) $(CP) src/org/gramvaani/radio/telephonylib/*java src/org/gramvaani/simpleipc/*java src/org/gramvaani/radio/stationconfig/*java src/org/gramvaani/radio/rscontroller/*java src/org/gramvaani/radio/app/*java src/org/gramvaani/radio/app/providers/*java src/org/gramvaani/radio/rscontroller/services/*java src/org/gramvaani/radio/rscontroller/messages/*java src/org/gramvaani/linkmonitor/*.java src/org/gramvaani/radio/timekeeper/*java src/org/gramvaani/radio/medialib/*java src/org/gramvaani/radio/rscontroller/messages/libmsgs/*java src/org/gramvaani/radio/rscontroller/servlets/*java src/org/gramvaani/radio/app/providers/*java src/org/gramvaani/radio/tests/*java src/org/gramvaani/radio/rscontroller/pipeline/*java src/org/gramvaani/radio/rscontroller/messages/indexmsgs/*java src/org/gramvaani/radio/app/gui/*java src/org/gramvaani/radio/diagnostics/*java src/org/gramvaani/utilities/*java src/org/gramvaani/utilities/processmonitor/*java src/org/gramvaani/utilities/singleinstance/*java

jackclient:
	mkdir -p $(CLASSD)
	$(CC) $(CCLIB) $(CCOPT)/org/gramvaani/radio/tests/jack_client src/org/gramvaani/radio/tests/jack_client.c

linkmonitor:
	mkdir -p $(CLASSD)
	$(JAVAC) $(CLASSDOPT) $(CP) src/org/gramvaani/linkmonitor/*.java

ipc: util
	mkdir -p $(CLASSD)
	$(JAVAC) $(CLASSDOPT) $(CP) src/org/gramvaani/simpleipc/*java

tests:
	mkdir -p $(CLASSD)
	$(JAVAC) $(CLASSDOPT) $(CP) src/org/gramvaani/radio/tests/*java

pipeline:
	mkdir -p $(CLASSD)
	$(JAVAC) $(CLASSDOPT) $(CP) src/org/gramvaani/radio/rscontroller/pipeline/*java
