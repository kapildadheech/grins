#!/bin/bash

GV="gramvaani-automation";
SCR="$GV/scripts";

removeCVS()
{
    local curDir=$1
    #echo $curDir;
    cd $curDir;
    for i in `ls -d */ 2> /dev/null`
    do
	if [ $i == "CVS/" ];
	then
	    #echo "Found $i";
	    rm -fr $i;
	else
	    removeCVS $i;
	fi
    done
    cd ..;
}


echo "WARNING: This file assumes that gramvaani-automation is present in current directory!!";
echo "WARNING: Many files present in the CVS but not required for dev package will be removed!!";
echo -n "Are you sure you want to continue?[N/y]";

read -e -n1 YESNO;

if [ "$YESNO" == "" ]; then
    YESNO="N";
fi

if [ "$YESNO" == "y" ] || [ "$YESNO" == "Y" ]; then
    rm -fr $GV/classes
    rm -f $GV/pushwebsite*
    rm -f $GV/runAudioService.sh
    rm -f $GV/runRSController.sh
    rm -f $GV/runTests.sh
    rm -f $GV/cvsdiff
    rm -f $GV/test.conf.sample
    rm -f $GV/categories.sample.demo
    rm -f $GV/makeConfs.pl
    rm -f $GV/logAnalyze.pl
    rm -f $SCR/*.py
    rm -f $SCR/run*.sh
    rm -f $SCR/logAnalyze.pl
    rm -f $SCR/makeConfs.pl
    rm -f $SCR/*.sql
    mv $GV/automation.conf.sample $GV/automation.conf

    removeCVS $GV;
    
    tar -zcf grins-src-0.1.tar.gz gramvaani-automation
    echo "done";
fi