
#set up variables
ENV_FILE=/usr/local/grins/setEnv.sh
CONF_FILE=`cat $ENV_FILE | grep GRAMVAANI_CONF_PATH= | cut -f2 -d= | tr -d "\r\n\""`/automation.conf;
LOG_DIR=`cat $ENV_FILE | grep GRINS_LOG_PATH= | cut -f2 -d= | tr -d "\r\n\""`;
RSC_IP=`cat $CONF_FILE | grep ^RSCONTROLLER_MACHINE, | tr -d " " | cut -f2 -d, | tr -d "\r\n"`;
KEY_FILE=`cat $ENV_FILE | grep SSH_KEY_PATH | cut -f2 -d= | tr -d "\r\n\""`;
if [ "${KEY_FILE}" != "" ]
then
    KEY_FILE=$HOME/.ssh/`basename $KEY_FILE`;
    SSH_KEY_PARAM="-i ${KEY_FILE}";
fi
CUR_DIR=`pwd`;

if [ "`uname | grep CYGWIN`" != "" ]
then
    DEST=${USERPROFILE}\\Desktop;
    WINTEMP="$WINDIR\\TEMP";
else
    DEST=${HOME}/Desktop;
fi

COMPRESS="tar";
C_OPTS="-zcf";
IFCONFIG="ifconfig";

if [ "`uname | grep CYGWIN`" != "" ]
then
    IFCONFIG="ipconfig"
fi

LOG_PARENT=`dirname ${LOG_DIR}`;
LOG_BASENAME=`basename ${LOG_DIR}`;


echo -n "Collecting GRINS logs..."

rm -f "${DEST}/grins*logs*tgz";

#if windows machine then collect event logs
if [ "`uname | grep CYGWIN`" != "" ]
then
    cscript.exe /Nologo DumpEventLog.vbs 127.0.0.1 "$WINTEMP\\win_event_log.csv" /all
    mv "$WINTEMP\\win_event_log.csv" /tmp/
fi


#compress local logs
cd ${LOG_PARENT};
${COMPRESS} ${C_OPTS} /tmp/grins_app_logs.tgz ${LOG_BASENAME};
cd ${CUR_DIR};


#if two machine setup then collect logs from remote machine
if [ "${RSC_IP}" != "127.0.0.1" ] && [ "`${IFCONFIG} | grep "${RSC_IP}"`" == "" ]
then
    ssh ${SSH_KEY_PARAM} -o "StrictHostKeyChecking no" grins@${RSC_IP} < collectRSCLogs.sh;
    scp ${SSH_KEY_PARAM} -o "StrictHostKeyChecking no" grins@${RSC_IP}:grins_rsc_logs.tgz /tmp/.;
    ssh ${SSH_KEY_PARAM} -o "StrictHostKeyChecking no" grins@${RSC_IP} "rm -f grins_rsc_logs.tgz";
fi

#Finally compress remote and local logs into a single file
FINAL_DATE=`date +%F-%R | tr "-" "_" | tr ":" "_"`;
cd /tmp;
if [ "`uname | grep CYGWIN`" != "" ]
then
    ${COMPRESS} ${C_OPTS} grins_logs_${FINAL_DATE}.tgz grins_*tgz win_event_log.csv;
else
    ${COMPRESS} ${C_OPTS} grins_logs_${FINAL_DATE}.tgz grins_*tgz;
fi
cd ${CUR_DIR};

mv /tmp/grins_logs_${FINAL_DATE}.tgz "${DEST}/"

echo "done"

echo "GRINS logs have been compressed and copied to your Desktop."
echo -n "Press any key to exit"
read -n1