
ENV_FILE=/usr/local/grins/setEnv.sh
LOG_DIR=`cat $ENV_FILE | grep GRINS_LOG_PATH= | cut -f2 -d= | tr -d "\r\n"`
CUR_DIR=`pwd`;
COMPRESS="tar";
C_OPTS="-zcf";

LOG_PARENT=`dirname ${LOG_DIR}`;
LOG_BASENAME=`basename ${LOG_DIR}`;

cd ${LOG_PARENT};
tar -zcf /tmp/grins_rsc_logs.tgz ${LOG_BASENAME}
cd ${CUR_DIR};
mv /tmp/grins_rsc_logs.tgz ./