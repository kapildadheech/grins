#File for shutting down grins vms and servlets
#Works for both windows and linux
#Assumes that /etc/init.d/gramvaani script is present in linux
#Assumes that on windows services are running throgh cygrunsrv

if [ `uname | grep CYGWIN | wc -l` -eq 1 ]
then
    net stop tomcat6

    for i in `procps  -eo pid,comm,command | grep perl | grep rsi | sed s/^\ *// | cut -d " " -f 1`; 
    do
        kill $i;
    done

    RSCPID=`procps -eo pid,comm,command | grep perl | grep rsc | sed s/^\ *// | cut -d " " -f 1`;
    if [ "$RSCPID" != "" ]; then
        kill $RSCPID;
    fi
    
    for i in `procps -eo pid,comm,command | grep java | grep RSServiceInstantiator | sed s/^\ *// | cut -d " " -f 1`;
    do
        kill $i;
    done
    
    APPPID=`procps -eo pid,comm,command | grep java | grep RSApp | sed s/^\ *// | cut -d " " -f 1`;
    if [ "$APPPID" != "" ]; then
        kill $APPPID;
    fi

    RSCPID=`procps -eo pid,comm,command | grep java | grep RSController | sed s/^\ *// | cut -d " " -f 1`;
    if [ "$RSCPID" != "" ]; then
        kill $RSCPID;
    fi
else
    sudo /etc/init.d/gramvaani stop
fi