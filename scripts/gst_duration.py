#!/usr/bin/python
import pygtk, gtk, gobject, sys, os, thread, time
import pygst
pygst.require("0.10")
import gst


class Pipeline:
    def __init__(self, filename):
        self.pipe = gst.Pipeline ("DurationPipe")
        
        self.file_src = gst.element_factory_make ("filesrc", "filesrc")
        self.file_src.set_property ("location", filename)

        self.decode_bin = gst.element_factory_make ("decodebin", "decodebin")
        self.decode_bin.connect ("new-decoded-pad", self.on_dynamic_pad)

        self.fake_sink = gst.element_factory_make ("fakesink", "fakesink")

        self.pipe.add (self.file_src, self.decode_bin, self.fake_sink)
        self.file_src.link (self.decode_bin)
        
        bus = self.pipe.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message)

        self.pipe.set_state(gst.STATE_NULL)

    def on_dynamic_pad(self, bin, pad, last):
        self.decode_bin.link(self.fake_sink)

        
    def on_message(self, bus, message):
        type = message.type
        
        if type == gst.MESSAGE_EOS:
            return -1
        elif type == gst.MESSAGE_ERROR:
            return -1
        elif type == gst.MESSAGE_STATE_CHANGED:
            if message.src == self.decode_bin:
                old, new, pending =  message.parse_state_changed()
                if new == gst.STATE_PAUSED and pending == gst.STATE_VOID_PENDING:
                    duration, format = self.pipe.query_duration(gst.FORMAT_TIME, None)
                    print duration/(1000*1000)
                    gtk.main_quit()
                    
    def play(self):
        self.pipe.set_state(gst.STATE_PLAYING)

pipeline = Pipeline(sys.argv[1])
pipeline.play()
        
gtk.main()
