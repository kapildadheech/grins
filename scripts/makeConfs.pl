#!/usr/bin/perl
use Shell qw(cp);

sub printUsage {
    print "You must provide exactly one argument.\n";
    exit();
}

sub printFile {
    print "AutomationConfs/@_[1].conf\n";
    my $fname = "AutomationConfs/@_[1].conf";

    open my $srcfile, '<', "@_[0]" or die "Could not open file @_[0] for reading.";

    open my $file, '>', $fname or die "Can't open file AutomationConfs/$@_[1].conf for writing.\n";
    
    while(<$srcfile>){
	chomp;
	if($_ =~ /SAMPLE_RATE/) {
	    print $file "SAMPLE_RATE, @_[8]\n";
	}else {
	    print $file "$_\n";
	}
    }
    close $srcfile;
    print $file "#resources used by services: [serviceName, resourceName, resourceRole]\n";
    print $file "[serviceresources]\n";
    print $file "AUDIO_SERVICE_WIN, soundcardauto1_out, PLAYOUT\n";
    print $file "AUDIO_SERVICE_LIN, @_[3], PREVIEW\n";
    print $file "ARCHIVER_SERVICE_COMMON, @_[2], BCAST_MIC\n";
    print $file "MIC_SERVICE_COMMON, @_[4], BCAST_MIC\n";
    print $file "MIC_SERVICE_COMMON, @_[5], PLAYOUT\n";
    print $file "MONITOR_SERVICE_COMMON, @_[6], BCAST_MIC\n";
    print $file "MONITOR_SERVICE_COMMON, @_[7], PLAYOUT\n";
    close $file;
    
    #open my $file, '<', "AutomationConfs/$@_[1].conf" or die "Can't open file AutomationConfs/$@_[1].conf for writing.\n";
    #while(<$file>)
    #{
	#chomp;
	#print "$_\n";
    #}
    #getc;
    #close $file;
}

sub main {
    $baseFile = @_[0];
    $ain = "soundcardalsa2_in";
    $aout = "soundcardalsa2_out";
    $jin = "soundcardalsa1_in";
    $jout = "soundcardalsa1_out";

    @inConfs = ($ain, $jin);
    @outConfs = ($aout, $jout);
    @rates = (48000, 44100);
    $count = 1;
    for $rate (@rates) {
	for $arin (@inConfs) {
	    for $micin (@inConfs) {
		for $moin (@inConfs) {
		    for $prout (@outConfs) {
			$moout = $prout;
			if($prout eq $jout) {
			    $micout = $aout;
			}else{
			    $micout = $jout;
			}
			printFile($baseFile,$count,$arin,$prout,$micin,$micout,$moin,$moout,$rate);
			$count++;
			
		    }
		}
	    }
	}
    }
}


if ($#ARGV != 0){
    printUsage();
}
else {
    main($ARGV[0]);
}
