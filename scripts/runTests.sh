#!/bin/bash

logDir="Logs"
automationConfDir="AutomationConfs"
testConfDir="TestConfs"
controllerLog="RSController.log"
appLog="RSApp.log"
audioServiceLog="AudioService.log"
controllerMachine="192.168.1.9"
appMachine="192.168.1.2"
autoConfFile=""
testConfFile=""
remoteGVPath="/cygdrive/c/gramvaani-automation"
localGVPath="/home/zahirk/gramvaani-automation"
curLogDir=""
cpuLogFile="CPU.log"
winCPULogFile="winCPU.log"
#devLogFile="Soundcard.log"
#jackLogFile="Jack.log"

prepareCheckRemoteStatus(){
	echo "echo -n \"\`date +%s\` \" > $remoteGVPath/$winCPULogFile" > checkRemoteStatus.sh
	echo "vmstat.exe 1 1 | head -1 >> $remoteGVPath/$winCPULogFile" >> checkRemoteStatus.sh
	echo "echo -n \"\`date +%s\` \" >> $remoteGVPath/$winCPULogFile" >> checkRemoteStatus.sh 
	echo "vmstat.exe 1 1 | head -2 | tail -1 >> $remoteGVPath/$winCPULogFile" >> checkRemoteStatus.sh
	echo "while [ true ]" >> checkRemoteStatus.sh
	echo "do" >> checkRemoteStatus.sh
	echo "echo -n \"\`date +%s\` \" >> $remoteGVPath/$winCPULogFile" >> checkRemoteStatus.sh
	echo "vmstat.exe 1 2 | tail -1 >> $remoteGVPath/$winCPULogFile" >> checkRemoteStatus.sh
	echo "procps -eo comm,pcpu,tty | egrep \"bash|java\" >> $remoteGVPath/$winCPULogFile" >> checkRemoteStatus.sh
	echo "count=\`ps aux | grep java | grep cygdrive | wc -l\`" >> checkRemoteStatus.sh
	echo "if [ \$count -ne 2 ]" >> checkRemoteStatus.sh 
	echo "then" >> checkRemoteStatus.sh
	echo "touch testCrashed" >> checkRemoteStatus.sh
	echo "scp testCrashed $controllerMachine:$localGVPath/" >> checkRemoteStatus.sh
	echo "rm -f testCrashed" >> checkRemoteStatus.sh
	echo "break" >> checkRemoteStatus.sh
	echo "fi" >> checkRemoteStatus.sh
	
	echo "if [ -e testDone ]" >> checkRemoteStatus.sh
	echo "then" >> checkRemoteStatus.sh
	echo "scp testDone $controllerMachine:$localGVPath/" >> checkRemoteStatus.sh
	echo "rm -f testDone" >> checkRemoteStatus.sh
	echo "break" >> checkRemoteStatus.sh
        echo "fi" >> checkRemoteStatus.sh
	
	echo "done" >> checkRemoteStatus.sh

	chmod a+x checkRemoteStatus.sh

	scp checkRemoteStatus.sh $userName@$appMachine:$remoteGVPath/ > /dev/null
}
waitForEnd(){
    echo -n Waiting for current test iteration to end...
    #$localGVPath/classes/org/gramvaani/radio/tests/jack_client jackportsdump > $curLogDir/$jackLogFile
    #echo $pwd | sudo -S lsof /dev/snd/pcm* 2> /dev/null > $curLogDir/$devLogFile
    done=0
    count=0
    while [ $done -ne 1 ]
    do
	sleep 10
	#$localGVPath/classes/org/gramvaani/radio/tests/jack_client jackportsdump >> $curLogDir/$jackLogFile
	#date +%s >> $curLogDir/$devLogFile
	#echo $pwd | sudo -S lsof /dev/snd/pcm* 2> /dev/null | tail -$((`echo $pwd | sudo -S lsof /dev/snd/pcm* 2> /dev/null | wc -l`-1)) 2> /dev/null >> $curLogDir/$devLogFile
	count=$(($count+1))
	if [ $count -eq 10 ]
	then
	    count=0
	    #echo "ps aux | grep java | grep cygdrive | wc -l > op" > remote1.sh
	    #ssh $userName@$appMachine < remote1.sh
	    #scp $userName@$appMachine:op ./ > /dev/null
	    #if [ `tail op` -ne 2 ]
	    if [ -e testCrashed ]
	    then
		echo Test crashed.
		mv testCrashed $curLogDir/
		done=1
                continue
	    fi
	fi
	#echo "if [ -e $remoteGVPath/testDone ]; then echo 1 > op; rm -f $remoteGVPath/testDone; else echo 0 > op; fi" > remote1.sh
	#ssh $userName@$appMachine < remote1.sh
	#scp $userName@$appMachine:op ./ > /dev/null
	#if [ `tail op` -eq 1 ]
	if [ -e testDone ]
	then
	    echo Test completed.
	    rm testDone
	    done=1
	fi
	#rm -f op
    done
}

runTest(){
    echo Running test with automation.conf = $autoConfFile and test.conf = $testConfFile
    cp $autoConfFile $curLogDir/automation.conf
    cp $testConfFile $curLogDir/test.conf
    cp runTests.sh $curLogDir/
    cp checkRemoteStatus.sh $curLogDir/

    echo -n Copying automation.conf to local \(RS Controller\) Machine...
    cp $autoConfFile $localGVPath/automation.conf
    echo done

    echo $pwd | sudo -S ./logSysInfo.sh > $curLogDir/$cpuLogFile 2>&1 &

    echo -n Starting RS Controller on local machine...
    curPath=`pwd`
    cd $localGVPath
    ./runRSController.sh > $curLogDir/$controllerLog 2>&1 &
    cd $curPath
    echo done
    
    sleep 10

    echo -n Copying automation.conf to RSApp Machine...
    scp $autoConfFile $userName@$appMachine:$remoteGVPath/automation.conf > /dev/null
    echo done
    echo -n Copying test.conf to RSApp Machine...
    scp $testConfFile $userName@$appMachine:$remoteGVPath/test.conf > /dev/null
    echo done

    echo -n Starting Audio Service and RS App on remote Machine...
    echo "cd $remoteGVPath" > remote.sh
    echo "export GST_PLUGIN_PATH='C:\Program Files\Common Files\GStreamer\0.10\lib\gstreamer-0.10'" >> remote.sh
    echo "./runAudioService.sh > $audioServiceLog 2>&1 &" >> remote.sh
    echo "sleep 2" >> remote.sh
    echo "export testfile=test.conf" >> remote.sh
    echo "export appMachine=$appMachine" >> remote.sh
    echo "./runRSApp.sh > $appLog 2>&1 &" >> remote.sh
    echo "sleep 2" >> remote.sh
    echo "./checkRemoteStatus.sh > checkRemoteStatus.log 2>&1 &" >> remote.sh
    ssh $userName@$appMachine < remote.sh
    echo done

    waitForEnd

    echo -n Test completed. Killing RS Controller on the local machine...
    rscpid=`pgrep java`
    #jackpid=`pgrep jackd`

    kill `ps aux | grep bash | grep runRSController | tr -s " " | cut -d " " -f 2`
    pkill java
    #pkill jackd
    sleep 5 #Letting RS App and Audio Service realize that RS Controller has gone down.
    
    if [ `ps aux | grep $rscpid | grep java | wc -l` -ne 0 ]
    then
	echo "Warning! RSController still running. Dirty killing it."
	kill -KILL $rscpid
    fi

    #if [ `ps aux | grep $jackpid | grep jackd | wc -l` -ne 0 ]
    #then
	#echo "Warning! Jack still running. Dirty killing it."
	#kill -KILL $jackpid
    #fi
    echo done

    echo Stopping Audio Service and RSApp on remote machine...
    echo "cd $remoteGVPath" > remote.sh
    echo "kill \`ps aux | grep bash | grep \? | tr -s \" \" | cut -d \" \" -f 2\`" >> remote.sh
    ssh $userName@$appMachine < remote.sh
    echo done

    echo Copying log files from remote machine to local machine...
    scp $userName@$appMachine:$remoteGVPath/$audioServiceLog $curLogDir/
    scp $userName@$appMachine:$remoteGVPath/$appLog $curLogDir/
    scp $userName@$appMachine:$remoteGVPath/$winCPULogFile $curLogDir/
    echo done
}

main(){
    ulimit -c unlimited
    echo Beginning to run tests on Gram Vaani
    echo Log directory set to \'$logDir\'
    echo automation.conf\'s expected to be present \in \'$automationConfDir\'
    echo test.conf\'s expected to be present \in \'$testConfDir\'
    echo RS Controller log file named as \'$controllerLog\'
    echo RS App log file named as \'$appLog\'
    echo Audio Service log file named as \'$audioServiceLog\'
    echo RS Controller set to run on \'$controllerMachine\'
    echo RS App and Audio Service set to run on \'$appMachine\'
    echo Local gramvaani path set to \'$localGVPath\'
    echo Remote gramvaani path set to \'$remoteGVPath\'
    
    if [ -d $automationConfDir ]
    then
	echo Directory $automationConfDir exists.
    else
	echo Error: $automationConfDir not found. Exiting.
	exit -1
    fi
    
    if [ -d $testConfDir ]
    then
	echo Directory $testConfDir exists.
    else
	echo Error: $testConfDir not found. Exiting.
	exit -1
    fi
    
    if [ -d $logDir ]
    then
	echo Directory $logDir exists. Skipping directory creation.
    else
	mkdir $logDir
    fi
   
    echo -n Preparing checkRemoteStatus.sh...
    prepareCheckRemoteStatus
    echo done.
    run=$((`ls -l $automationConfDir/*.conf | wc -l`))
    
    echo $run tests to be run.
   
    #back up the old automation.conf on RSController Machine
    if [ -f $localGVPath/automation.conf ]
    then
	mv $localGVPath/automation.conf $localGVPath/automation.conf.bak
    fi

    #run tests
    i=1
    while [ $i -le $run ]
    do
	autoConfFile=$automationConfDir/$i.conf
	testConfFile=$testConfDir/$i.conf
	curLogDir=$logDir/`eval date +%s`
	if [ -d $curLogDir ]
	then 
	    echo $curLogDir already exists. Directory creation not required.
	else
	    mkdir $curLogDir
	fi
	runTest
	echo Iteration $i completed.
	i=$((i+1))
    done
    
    echo All test iterations completed.
    
    exit
    #restore automation.conf on RSController Machine
}

if [ ! -z "$SUDO_USER" ]
then
    echo Please run without sudo
else
    if [ $# -ne 2 ]
    then
	echo $# parameters provided. You must provide exactly 2 parameters. Remote username and local sudo password.
    else
	userName=$1
	pwd=$2
	main
    fi
fi
