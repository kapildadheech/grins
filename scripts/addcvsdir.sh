echo "You are about to add all the files and directories in the current directory to CVS."
echo "If the directory itself is not added to CVS this will not work."
echo -n "Are you sure you want to continue?[Y/n]"

read -n1 YESNO

if [ "$YESNO" = "N" ] || [ "$YESNO" = "n" ]
then
    exit
fi

find . -type d -print | grep -v CVS | xargs -n1 cvs add
find . -type f -print | grep -v CVS | xargs -n1 cvs add

echo "===================================================================="
echo "Run cvs commit to complete the addition of current directory to cvs."
echo "===================================================================="