#!/usr/bin/perl
use Net::Ping;
#use Net::OpenSSH;

sub printUsage {
    print "Usage: sudo -E ./run.pl WhatToRun login password [ParametersInDoubleQuotes]\n";
    print "WhatToRun = app|rsc|rsi\n";
    print "Default options for app = \"-m RSAPP_MACHINE\"\n";
    print "Default options for rsi = \"-m SERVICE_MACHINE\"\n";
}

sub testLocalIP {
    my $myMachineIP = shift;
    my $curOS = $^O;
    
    if($curOS eq "linux") {
        my $interface;

        foreach ( qx{ (LC_ALL=C /sbin/ifconfig -a 2>&1) } ) {
            $interface = $1 if /^(\S+?):?\s/;
            next unless defined $interface;
            $IPs[$#IPs + 1] = $1 if /inet\D+(\d+\.\d+\.\d+\.\d+)/i;
        }

    } else {

        foreach ( qx{ (ipconfig /all 2>&1) } ) {
            $IPs[$#IPs + 1] = $1 if /IP Address\D+:\s+(\d+\.\d+\.\d+\.\d+)/i;
        }

    }
        
    if($myMachineIP eq "127.0.0.1") {
        return 1;
    }

    for my $ip (@IPs) {
        if($ip eq $myMachineIP) {
            return 1;
        }
    }

    return 0;
}

sub makeSSHConnection {
    my $ip = shift;
    if(($ip ne "") && ($sshConnections{$ip} eq "")){
	print "Adding $ip\n";
	$sshConnections{$ip} = Net::OpenSSH->new("${login}@".$ip,
							ssh_cmd => "/usr/bin/ssh",
							timeout => 3);
	$sshConnections{$ip}->error and\
	    print "Can't ssh to $ip: " . $sshConnections{$ip}->error;
    }
}

sub getDBMachine {
     open $conf, '<', $automationConfPath or die "Could not open configuration file automation.conf for reading.";
    my $inMachines = 0;
    for $line (<$conf>){
	if($line =~ /^#/){
	    next;
	}
	$line =~ s/\s//g;
	$line =~ s/\n//g;
	$line =~ s/\r//g;
	if($line =~ /LIB_DB_SERVER/){
	    my @val = split(",",$line);
	    @val = split(":",$val[1]);
	    $dbMachine = $val[0];
	}
    }
}

sub setupSSH {
    open $conf, '<', $automationConfPath or die "Could not open configuration file automation.conf for reading.";
    my $inMachines = 0;
    for $line (<$conf>){
	if($line =~ /^#/){
	    next;
	}
	$line =~ s/\s//g;
	$line =~ s/\n//g;
	$line =~ s/\r//g;
	if($inMachines == 0){
	    if($line =~ /\[machines\]/){
		$inMachines = 1;
	    }elsif($line =~ /IPC_SERVER/){
		my @val = split(",",$line);
		makeSSHConnection(@val[1]);
	    }elsif($line =~ /LIB_DB_SERVER/){
		my @val = split(",",$line);
		@val = split(":",$val[1]);
		$dbMachine = $val[0];
		makeSSHConnection($val[0]);
	    }elsif($line =~ /UPLOAD_URL/){
		my @val = split(",",$line);
		@val = split("/",$val[1]);
		@val = split(":",$val[2]);
		$servletMachine = $val[0];
		makeSSHConnection($val[0]);
	    }
	}else{
	    if($line =~ /\[.*\]/){
		$inMachines = 0;
		next;
	    }
	    my @machIP = split(",",$line);
	    makeSSHConnection($machIP[1]);
	}
    }
}

sub updateContext {
    my $line = shift;
    my $context = shift;

    if($line =~ /\[machines\]/){
	$context = "machines";
    } elsif($line =~ /\[services\]/) {
	$context = "services";
    } elsif($line =~ /\[resources\]/) {
	$context = "resources";
    } elsif($line =~ /\[resourceconfig\]/) {
	$context = "resourceconfig";
    } elsif($line =~ /\[serviceresources\]/) {
	$context = "serviceresources";
    } elsif($line =~ /\[.*\]/) {
	$context = "";
    }
    
    return $context;
}

sub processConfig {
    open $conf, '<', $automationConfPath or (print "2" and exit);
    my $context = "";
    
    for my $line (<$conf>){
	if($line =~ /^#/){
	    next;
	}
	$line =~ s/\s//g;
	$line =~ s/\n//g;
	$line =~ s/\r//g;

	if($line eq "") {
	    next;
	}

	$context = updateContext($line, $context);

	if($line =~ /\[.*\]/) {
	    next;
	}

	if($context eq "machines") {
	    my @machineInfo = split(",", $line);
	    $machines{$machineInfo[0]} = $machineInfo[1];
	} elsif($context eq "services") {
	    my @serviceInfo = split(",", $line);
	    $serviceMachine{$serviceInfo[3]} = $serviceInfo[2];
	    $serviceInstanceService{$serviceInfo[3]} = $serviceInfo[0];
	} elsif($context eq "resources") {
	    my @resourceInfo = split(",", $line);
	    $resources{$resourceInfo[1]} = $resourceInfo[2];
	} elsif($context eq "resourceconfig") {
	    my @resourceInfo = split(",", $line);
	    $resourceConfig{$resourceInfo[0]} = $resourceInfo[2];
	} elsif($context eq "serviceresources") {
	    my @resourceInfo = split(",", $line);
	    $serviceResources{$resourceInfo[0].",".$resourceInfo[1]} = $resourceInfo[2];
	} elsif($line =~ /TELEPHONY_SERVER/) {
	    my @asteriskInfo = split(",", $line);
	    $asteriskIP = $asteriskInfo[1];
	}
    }
    close $conf;
}

sub testSingleMachineSetup {
    my %machineIPMap;

    for my $machine (keys %machines) {
	my $machineIP = $machines{$machine};
	$machineIPMap{$machineIP} = $machineIP;
    }

    my $ipNum = scalar keys %machineIPMap;
    if($ipNum > 1) {
	return "n";
    } else {
	return "y";
    }
}

sub testServletRequirement {
    #Hard coded service instance names used here. 
    #Should be changed if and when the automatically generated automation.conf 
    #changes service nomenclature
    @producers = ("LIB_SERVICE", "UI_SERVICE", "ARCHIVER_SERVICE_COMMON");
    @consumers = ("LIB_SERVICE", "AUDIO_SERVICE_PLAYOUT", "AUDIO_SERVICE_PREVIEW");

    my $myMachineID = shift;

    $myIP = $machines{$myMachineID};
    
    if($myIP eq "") {
	return "3";
    }
    
    $needServlet = "n";
    
    for $consumer (@consumers) {
	my $consumerIP = $machines{$serviceMachine{$consumer}};
	if($consumerIP eq $myIP) {
	    $needServlet = "y";
	    last;
	}
    }
    
    if($needServlet eq "y") {
	$needServlet = "n";
	for $producer (@producers) {
	    my $producerIP = $machines{$serviceMachine{$producer}};
	    if($producerIP ne $myIP) {
		#print STDERR "Found producer different ".$producer." ".$producerIP." ".$myIP;
		$needServlet = "y";
		last;
	    }
	}
    }
    
    if(($needServlet eq "n") && ($asteriskIP ne $myIP)) {
	$needServlet = "y";
    }

    return $needServlet;
}

sub populateServlets {
    processConfig();
    my %machineIPs;
    for my $machine (keys %machines) {
	my $ip = $machines{$machine};
	$machineIPs{$ip} = $machine;
    }

    for my $ip (keys %machineIPs) {
	$retVal = testServletRequirement($machineIPs{$ip});
	if($retVal eq "y") {
	    $servletMachines[$#servletMachines + 1] = $ip;
	}
    }
}

sub buildCommand {
    $command = "bash run.sh ";
    
	
    if($runWhat eq "app"){
	$command .= "app.RSApp ";
	if($runParams ne ""){
	    $command .= $runParams;
	} else {
	    $command .= "-m RSAPP_MACHINE";
	}
    } elsif($runWhat eq "rsc"){
	$command .= "rscontroller.RSController ";
	if($runParams ne ""){
	    $command .= $runParams;
	}
    } elsif($runWhat eq "rsi"){
	$command .= "rscontroller.RSServiceInstantiator ";
	if($runParams ne ""){
	    $command .= $runParams;
	} else {
	    $command .= "-m SERVICE_MACHINE";
	}
    } else{
	printUsage();
	exit;
    }
}

sub restartDatabase {
    my $ip = shift;

    my $remoteCommand = "if [ \\\`uname | grep CYGWIN | wc -l\\\` -eq 1 ]; then $winDBRestartCommand; else $linDBRestartCommand; fi";
    my $localCommand = "if [ \`uname | grep CYGWIN | wc -l\` -eq 1 ]; then $winDBRestartCommand; else $linDBRestartCommand; fi";
    if(($ip ne "") && (testLocalIP($ip) == 0)) {
	runOverSSH($ip, $remoteCommand);
    } else {
	runLocally($localCommand);
    }
}

sub stopDatabase {
    my $ip = shift;

    my $remoteCommand = "if [ \\\`uname | grep CYGWIN | wc -l\\\` -eq 1 ]; then $winDBStopCommand; else $linDBStopCommand; fi";
    my $localCommand = "if [ \`uname | grep CYGWIN | wc -l\` -eq 1 ]; then $winDBStopCommand; else $linDBStopCommand; fi";
    
    if(($ip ne "") && (testLocalIP($ip) == 0)) {
	runOverSSH($ip, $remoteCommand);
    } else {
	runLocally($localCommand);
    }
}

sub restartServlet {
    my $ip = shift;
    
    my $remoteCommand = "if [ \\\`uname | grep CYGWIN | wc -l\\\` -eq 1 ]; then $winRemoteServletRestartCommand; else $linRemoteServletRestartCommand; fi";
    my $localCommand = "if [ \`uname | grep CYGWIN | wc -l\` -eq 1 ]; then $winLocalServletRestartCommand; else $linLocalServletRestartCommand; fi";

    if(($ip ne "") && (testLocalIP($ip) == 0)) {
	runOverSSH($ip, $remoteCommand);
    } else {
	runLocally($localCommand);
    }
}

sub stopServlet {
    my $ip = shift;
    
    my $remoteCommand = "if [ \\\`uname | grep CYGWIN | wc -l\\\` -eq 1 ]; then $winRemoteServletStopCommand; else $linRemoteServletStopCommand; fi";
    my $localCommand = "if [ \`uname | grep CYGWIN | wc -l\` -eq 1 ]; then $winLocalServletStopCommand; else $linLocalServletStopCommand; fi";

    if(($ip ne "") && (testLocalIP($ip) == 0)) {
	runOverSSH($ip, $remoteCommand);
    } else {
	runLocally($localCommand);
    }
}


sub restartAsterisk {
    my $ip = shift;

    my $remoteCommand = "if [ \\\`uname | grep CYGWIN | wc -l\\\` -eq 1 ]; then $winAsteriskRestartCommand; else $linAsteriskRestartCommand; fi";
    my $localCommand = "if [ \`uname | grep CYGWIN | wc -l\` -eq 1 ]; then $winAsteriskRestartCommand; else $linAsteriskRestartCommand; fi";

    if(($ip ne "") && (testLocalIP($ip) == 0)) {
	runOverSSH($ip, $remoteCommand);
    } else {
	runLocally($localCommand);
    }
}

sub stopAsterisk {
    my $ip = shift;

    my $remoteCommand = "if [ \\\`uname | grep CYGWIN | wc -l\\\` -eq 1 ]; then $winAsteriskStopCommand; else $linAsteriskStopCommand; fi";
    my $localCommand = "if [ \`uname | grep CYGWIN | wc -l\` -eq 1 ]; then $winAsteriskStopCommand; else $linAsteriskStopCommand; fi";
    
    if(($ip ne "") && (testLocalIP($ip) == 0)) {
	runOverSSH($ip, $remoteCommand);
    } else {
	runLocally($localCommand);
    }
}

sub displayMessageOnLCD {
    my $line = shift;
    chomp($line);
    my $line1 = getLocalDateTime();
    my $line2;
    my @message = split($delimiter, $line);
    if($line =~ /CHECK_NETWORK/) {
	
	$line1 .= " NETWORK";
	$line2 = "$message[1]:";
	if($message[2] =~ /true/) {
	    $line2 .= "UP";
	} else {
	    $line2 .= "DOWN";
	}
    } else {
	$line2 = $message[0];
    }

    #$line = $tstamp.":".$line;

    `echo $password | sudo -S usblcd clear > /dev/null 2>&1`;
    `echo $password | sudo -S usblcd text 0 "$line1" > /dev/null 2>&1`;
    `echo $password | sudo -S usblcd text 1 "$line2" > /dev/null 2>&1`;

}

sub displayMessage {
    my $message = shift;
    
    $lcdDaemonExists = `ps aux | grep LCD | grep -v grep | wc -l`;
    if($lcdDaemonExists > 0) {
	displayMessageOnLCD($message);
    }
}

sub getLocalDateTime {
    my @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
    my @weekDays = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
    my ($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = localtime();
    my $year = 1900 + $yearOffset;
    my $theTime = "$hour:$minute:$second";

    return $theTime;
}


sub checkNetwork {
    my $timeout = 2; #2 seconds
    my $line = shift;
    my ($id, $cmd, $ip) = split($delimiter,$line);
    
    my $ping = Net::Ping->new("icmp", $timeout);
    if($ping->ping($ip)){
	$line=($id.$delimiter.$cmd.$delimiter."true");
    }else{
	$line=($id.$delimiter.$cmd.$delimiter."false");
    }
    
    displayMessage($line);
    return $line."\n";
}


sub startDatabase {
    $line = shift;
    my ($id, $cmd, $ip) = split($delimiter, $line);

    restartDatabase($ip);

    displayMessage($line);
    return $line."\n";
}


sub startServlet {
    $line = shift;
    my($id, $cmd, $ip) = split($delimiter, $line);
    
    restartServlet($ip);
    displayMessage($line);
    return $line."\n";
}

sub startAsterisk {
    $line = shift;
    my($id, $cmd, $ip) = split($delimiter, $line);

    restartAsterisk($ip);

    displayMessage($line);
    return $line."\n";
}

sub restartSystem {
    $line = shift;
    #print TERMINAL $line;
    my ($id, @ips) = split($delimiter, $line);
    my $response = $id.$delimiter.$ips[0];
    my $cmd, $retval;
    my $psCommand;

    if($curOS eq "linux") {
	$psCommand = "ps -eo pid,command";
    } elsif($curOS eq "cygwin" ) {
	$psCommand = "procps -eo pid,comm,command";
    }

    my @rsiop = `$psCommand | grep java | grep RSServiceInstantiator | tr -s " "`;
    my @rscop = `$psCommand | grep java | grep RSController | tr -s " "`;
    
    $rsiop[0] =~ s/^ //;
    $rscop[0] =~ s/^ //;

    @rscpid = split(" ", $rscop[0]);
    @rsipid = split(" ", $rsiop[0]);

    #print TERMINAL "$rsipid[0], $rsipid[1] and $rscpid[0], $rscpid[1]\n";
    if($rsipid[0] ne "") {
	if($curOS eq "linux") {
	    `echo $password | sudo -S kill $rsipid[0] > /dev/null 2>&1`;
	} elsif($curOS eq "cygwin") {
	    `kill $rsipid[0] > /dev/null 2>&1`;
	}
    }

    if($rscpid[0] ne "") {
	if($curOS eq "linux") {
	    `echo $password | sudo -S kill $rscpid[0] > /dev/null 2>&1`;
	} elsif($curOS eq "cygwin") {
	    `kill $rscpid[0] > /dev/null 2>&1`;
	}
    }

    restartDatabase($dbMachine);
    for my $servletMachine (@servletMachines) {
	restartServlet($servletMachine);
    }
    restartAsterisk($asteriskIP);

    for($i = 1; $i <= $#ips; $i++) {
	$retval = runOverSSH($ips[$i], $machineRebootCommand);
	$response .= ($delimiter.$retval);

    }
    #XXX may need to change to \r\n for windows
    displayMessage($ips[0]);
    return $response."\n";
}

sub shutdownSystem {
    $line = shift;
    #print TERMINAL $line;

    my ($id, @ips) = split($delimiter, $line);
    my $response = $id.$delimiter.$ips[0];
    my $cmd, $retval;
     my $psCommand;

    if($curOS eq "linux") {
	$psCommand = "ps -eo pid,command";
    } elsif($curOS eq "cygwin" ) {
	$psCommand = "procps -eo pid,comm,command";
    }
    
    my @rsiop = `$psCommand | grep perl | grep rsi | sed s/^\\ *// | tr -s " " | cut -d" " -f1`;
    my @rscop = `$psCommand | grep perl | grep rsc | sed s/^\\ *// | tr -s " " | cut -d" " -f1`;

    for my $pid (@rsiop) {
	chomp($pid);
    	if($pid ne "") {
	    if($curOS eq "linux") {
		`echo $password | sudo -S kill $pid > /dev/null 2>&1`;
	    } elsif($curOS eq "cygwin") {
	    	`kill $pid > /dev/null 2>&1`;
	    }
    	}
    }	

    chomp($rscop[0]);
    if($rscop[0] ne "") {
	if($curOS eq "linux") {
	    `echo $password | sudo -S kill $rscop[0] > /dev/null 2>&1`;
	} elsif($curOs eq "cygwin") {
	    `kill $rscop[0] > /dev/null 2>&1`;
	}
    }

    my @rsiop = `$psCommand | grep java | grep RSServiceInstantiator | sed s/^\\ *// | tr -s " " | cut -d" " -f1`;
    my @rscop = `$psCommand | grep java | grep RSController | sed s/^\\ *// | tr -s " " | cut -d" " -f1`;

    for my $pid (@rsiop) {
	chomp($pid);
    	if($pid ne "") {
	    if($curOS eq "linux") {
	    	`echo $password | sudo -S kill $pid > /dev/null 2>&1`;
	    } elsif($curOS eq "cygwin") {
	    	`kill $pid > /dev/null 2>&1`;
	    }
	}
    }

    chomp($rscop[0]);
    if($rscop[0] ne "") {
	if($curOS eq "linux") {
	    `echo $password | sudo -S kill $rscop[0] > /dev/null 2>&1`;
	} elsif($curOs eq "cygwin") {
	    `kill $rscop[0] > /dev/null 2>&1`;
	}
    }

    #stopDatabase($dbMachine);
       
    for my $servletMachine (@servletMachines) {
	stopServlet($servletMachine);
    }

    stopAsterisk($asteriskIP);

    for($i = 1; $i <= $#ips; $i++) {
	$retval = runOverSSH($ips[$i], $machineShutdownCommand);
	$response .= ($delimiter.$retval);

    }

    displayMessage($ips[0]);
    return $response."\n";
}

sub runLocally {
    my $cmd = shift;
    print TERMINAL "RunLocally: Command=\"".$cmd."\"\n";
    print TERMINAL "RunLocally: Output:\n";
    open(OP, $cmd." |");
    while(<OP>) {
	print TERMINAL $_;
    }
    close OP;
}


sub runOverSSH {
    my $ip = shift;
    my $cmd = shift;
    my $keyFilePresent = true;
    
    print TERMINAL "ip=".$ip." cmd=".$cmd."\n";

    if($ip eq "") {
	return $false;
    }

    runLocally("ssh -i ~/.ssh/$keyFile -o \"StrictHostKeyChecking no\" ${login}@".$ip." \"$cmd\"");

    sleep 1;
    return $true;
}

sub wrapper {
    my $programName = shift;
    my $dieCount = 0;
    my $curDieTime = 0;
    my $prevDieTime = 0;
    my $retLine;

    while (true) {
	pipe (FROM_PERL, TO_PROGRAM);
	pipe (FROM_PROGRAM, TO_PERL);
     	$pid = fork;
	die "Couldn't fork: $!" unless defined $pid;
	
#child process 
	if ($pid == 0)  {
	    
	    ##### attach standard input/output/error to the pipes
	    close  STDIN;
	    open  (STDIN,  '<&FROM_PERL') || die ("open: $!");
	    
	    close  STDOUT;
	    open  (STDOUT, '>&TO_PERL')   || die ("open: $!");
	    
	    ##### close unused parts of pipes
	    close FROM_PROGRAM;
	    close TO_PROGRAM;
	    
	    ##### unbuffer the outputs
	    select STDERR; $| = 1;
	    select STDOUT; $| = 1;
	    
	    ##### execute the program
	    exec $programName;
	    
	    ##### shouldn't get here!!!
	    die;
	} else {
	    
##### parent process is the perl script
	    open (TERMINAL, '>&STDOUT');
	    open (KEYBOARD, '<&STDIN');
	    
	    close STDIN;
	    open (STDIN,    '<&FROM_PROGRAM') || die ("open: $!");
	    
	    close STDOUT;
	    open (STDOUT,   '>&TO_PROGRAM')   || die ("open: $!");
	    
	    close FROM_PERL;
	    close TO_PERL;
	    
##### unbuffer all the outputs
	    select TERMINAL; $| = 1;
	    select STDERR;   $| = 1;
	    select STDOUT;   $| = 1;

	    
	    while (<STDIN>)  {
		$_ =~ s/\n//g;
		$_ =~ s/\r//g;

		print TERMINAL $_."\n";
		$retLine = "";

		if($_ =~ /CHECK_NETWORK/){
		    $retLine = checkNetwork($_);
		}elsif($_ =~ /START_DATABASE/){
		    $retLine = startDatabase($_);
		}elsif($_ =~ /START_SERVLET/){
		    $retLine = startServlet($_);
		}elsif($_ =~ /RESTART_SYSTEM/){
		    $retLine = restartSystem($_);
		}elsif($_ =~ /SHUTDOWN_SYSTEM/){
		    $retLine = shutdownSystem($_);
		}elsif($_ =~ /START_ASTERISK/){
		    $retLine = startAsterisk($_);
		}
		
		print TERMINAL $retLine;
		print $retLine;
	    }
	    
	    #child terminated
	    waitpid($pid, WNOHANG);
	    
	    close STDIN;
	    open (STDIN, '<&KEYBOARD');
	    close KEYBOARD;
	    
	    close STDOUT;
	    open (STDOUT, '>&TERMINAL');
	    close TERMINAL;
	    
	    select STDOUT; $| = 1;
	    select STDIN; $|= 1;
	    
	    if($runWhat eq "app") {
		if($isSingleMachineSetup eq "y" and $curOS eq "linux" and $? != 64768) {
		    print STDOUT "Shutting down ...";
		    sleep(1);
		    shutdownSystem();
		}
		last;
	    } elsif(($? == 0) || ($? == 65024) || ($? == 64768)) {
		#clean exit or 
		#exit due to high temperature or
		#exit due to existence of multiple instances for services/rsc
		last;
	    } else {
		$curDieTime = time;
		if(($curDieTime - $prevDieTime) < 60){
		    $dieCount++;
		}else{
		    $dieCount = 0;
		}
		if($dieCount > 10){
		    print STDOUT "\"$programName\" dying too often. Terminating wrapper script.\n";
		    last;
		}
		$prevDieTime = $curDieTime;
	    }

	    sleep 2;
	    print STDOUT "\"$programName\" died with status $?. Starting again.\n";
	}
    }
}    

sub testPing {
    print "Testing if ping works. If not then try running the program with root privileges.\n";
    #my $tmp = Net::Ping->new("icmp");
    print "It works. No worries here!!\n";
}

if($#ARGV < 0 || $#ARGV > 1){
    printUsage();
    exit;
}

$automationConfPath="automation.conf";
$delimiter = "!";
$curOS = $^O;
$runWhat = $ARGV[0];
$login = "grins";
$password = "junoon";
$runParams;
$command;
$dbMachine;
@servletMachines;
$false = "false";
$true = "true";
%sshConnections;
$keyFile = "grins_rsa";

#various commands

##Linux DB commands
$linDBRestartCommand = "sudo /etc/init.d/mysql restart";
$linDBStopCommand = "sudo /etc/init.d/mysql stop";

##Windows DB commands
$winDBRestartCommand = "net stop mysql; net start mysql";
$winDBStopCommand = "net stop mysql";

##Linux servlet commands
$linRemoteServletRestartCommand = "if [ -e /usr/local/grins ]; then GRINSPATH=\\\"/usr/local/grins\\\"; else GRINSPATH=\\\"/usr/local/gramvaani-automation\\\"; fi; TOMCATPID=\\\`ps -eo pid,command | grep java | grep tomcat | grep -v grep | sed s/^\\\ *// | cut -d \\\" \\\" -f 1\\\`; sudo kill \\\$TOMCATPID >/dev/null 2>&1; sudo \\\${GRINSPATH}/apache-tomcat-5.5.27/bin/startup.sh >/dev/null 2>&1";
$linLocalServletRestartCommand = "if [ -e /usr/local/grins ]; then GRINSPATH=\"/usr/local/grins\"; else GRINSPATH=\"/usr/local/gramvaani-automation\"; fi; TOMCATPID=\`ps -eo pid,command | grep java | grep tomcat | grep -v grep | sed s/^\\\ *// | cut -d \" \" -f 1\`; echo \$TOMCATPID; sudo kill \$TOMCATPID ; sudo \${GRINSPATH}/apache-tomcat-5.5.27/bin/startup.sh";
$linRemoteServletStopCommand = "TOMCATPID=\\\`ps -eo pid,command | grep java | grep tomcat | grep -v grep | sed s/^\\\ *// | cut -d \\\" \\\" -f 1\\\`; sudo kill \$TOMCATPID >/dev/null 2>&1";
$linLocalServletStopCommand = "TOMCATPID=\`ps -eo pid,command | grep java | grep tomcat | grep -v grep | sed s/^\\\ *// | cut -d \" \" -f 1\`; sudo kill \$TOMCATPID >/dev/null 2>&1";

##Windows servlet commands
$winRemoteServletRestartCommand = "net stop tomcat6 > /dev/null 2>&1; net start tomcat6 > /dev/null 2>&1";
$winLocalServletRestartCommand = "net stop tomcat6; net start tomcat6";
$winRemoteServletStopCommand = "net stop tomcat6 > /dev/null 2>&1";
$winLocalServletStopCommand = "net stop tomcat6";

##Linux Asterisk commands
$linAsteriskRestartCommand = "sudo /etc/init.d/asterisk restart";
$linAsteriskStopCommand = "sudo /etc/init.d/asterisk stop";

##Windows Asterisk commands: Not implemented as yet
$winAsteriskRestartCommand = "ls > /dev/null";
$winAsteriskStopCommand = "ls > /dev/null";

##Machine restart and shutdown commands: always executed over ssh
$machineRebootCommand = "if [ \\\`uname | grep CYGWIN | wc -l\\\` -eq 1 ] ; then reboot -f now; else sudo reboot; fi >/dev/null 2>&1";
$machineShutdownCommand = "if [ \\\`uname | grep CYGWIN | wc -l\\\` -eq 1 ] ; then shutdown -s -f; else sudo shutdown -P now; fi  >/dev/null 2>&1";

if($#ARGV == 1){
   $runParams = $ARGV[1];
}

#Populated by processConfig
%machines; #map from machineID to IP address
%serviceMachine; #map from service instance name to machineid
%serviceInstanceSerivce; #map from service instance name to service type
%resources; #map from resource name to machineid
%resourceConfig; #map from resource name to actual resource (e.g. hw:0, etc)
%serviceResources; #map from service instance name + resource name to resource role
$asteriskIP;



testPing();
buildCommand();
populateServlets();
getDBMachine();
$isSingleMachineSetup=testSingleMachineSetup();
#Using system("ssh.."); for linux also for now.
#We will directly be using system("ssh ...."); when we are on windows.
#if($curOS eq "linux") {
    #setupSSH();
#}
if($runWhat eq "app" and $curOS eq "linux" and $isSingleMachineSetup eq "y") {
    if(-e "/etc/init.d/gramvaani") {
	`/etc/init.d/gramvaani start`
    } elsif(-e "/etc/init.d/grins") {
	`/etc/init.d/grins start`
    }
}
wrapper($command);

    
