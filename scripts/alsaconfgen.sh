#!/bin/bash

ALSACONF=/etc/asterisk/alsa.conf

function device_id() {
    DEVICE=`grep ^$1 $AUTO | cut -d"," -f 3 | awk '{ print $1}'`
    echo $DEVICE
}

function card_id() {
    CARD=`grep $1 $AUTO| cut -d"," -f 2 | awk '{ print $1}'`
    echo $CARD
}

function dev_card() {
    echo $(device_id $(card_id $1))
}

if [ "$#" -gt 1 ]; then
    echo "Usage: $0 [automation.conf]"
    exit
elif [ "$#" -eq 1 ]; then
    AUTO=$1
else
    AUTO=/usr/local/grins/automation.conf
fi

echo
echo Using $AUTO:
echo

INPUT_DEVICE="input_device="$(dev_card offair_capture)!$(dev_card onair_capture)
OUTPUT_DEVICE="output_device="$(dev_card offair_playback)!$(dev_card onair_playback)
echo $INPUT_DEVICE
echo $OUTPUT_DEVICE

echo
echo From $ALSACONF:
echo

grep ^input_device $ALSACONF
grep ^output_device $ALSACONF

echo