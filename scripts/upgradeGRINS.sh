#Script for upgrading GRINS
##Assumes root privileges for linux

WIN=WINDOWS;
LIN=LINUX;
STOP_SCRIPT=shutdownGRINS.sh;
RERUN_STATUS=10;
REMOTE_UPGRADE=remoteUpgrade.sh;
ENV_PATH=/usr/local/grins
ALT_ENV_PATH=/usr/local/gramvaani-automation
ENV_FILE="setEnv.sh";
VERSION_FILE=".grins_version";
MAX_VERSION_FILE=".grins_version_max";

if [ -e $ENV_PATH/$ENV_FILE ]
then
    . $ENV_PATH/$ENV_FILE;
else
    if [ -e $ALT_ENV_PATH/$ENV_FILE ]
    then
	. $ALT_ENV_PATH/$ENV_FILE;
    else
	echo "Cannot find information regarding GRINS installation. Upgrade cannot continue. Press any key to exit.";
	read -n1;
	exit -1;
    fi
fi

if [ `uname | grep CYGWIN | wc -l` -eq 1 ]
then
    CUROS=$WIN;
    RUNFILE=runwin.sh
    GVPATH=$GRAMVAANI_BINARY_PATH_CYG
    GVCONFPATH=$GRAMVAANI_CONF_PATH_CYG
    UPGRADEPATH=$GRAMVAANI_UPGRADE_PATH_CYG
    KEYPATH=$SSH_KEY_PATH_CYG
else
    CUROS=$LIN;
    TEMP=/tmp;
    RUNFILE=run.sh
    GVPATH=$GRAMVAANI_BINARY_PATH
    GVCONFPATH=$GRAMVAANI_CONF_PATH
    UPGRADEPATH=$GRAMVAANI_UPGRADE_PATH
    KEYPATH=$SSH_KEY_PATH
fi



if [ `echo "$GVPATH" | wc -w` -eq 0 ]
then
    echo "Cannot find path to GRINS. Upgrade cannot proceed. Press any key to exit.";
    read -n1;
    exit -1;
fi

if [ ! -d "$UPGRADEPATH" ]
then
    echo "Cannot find upgrade directory. Upgrade cannot proceed. Press any key to exit";
    read -n1;
    exit -1;
fi

if [ `ls -l "$UPGRADEPATH" | wc -l` -eq 0 ]
then
    echo "No upgrade packeges found. Press any key to exit.";
    read -n1;
    exit -1;
fi

if [ ! -e "$GVCONFPATH/automation.conf" ]
then
    echo "Cannot find GRINS configuration file. Upgrade cannot proceed. Press any key to exit";
    read -n1;
    exit -1;
fi

#Shut down grins on all the machines
#1. Shut all servlets
#2. Stop all java processes
#3. Stop all perl processes
perl upgradeUtilities.pl RSAPP_MACHINE POPULATE_REMOTE_MACHINES "$GVCONFPATH/automation.conf" $TEMP/remote_machines;
perl upgradeUtilities.pl RSAPP_MACHINE GET_LOCAL_IP "$GVCONFPATH/automation.conf" $TEMP/local_ip;
LOCALIP=`cat $TEMP/local_ip`;

echo "Stopping GRINS on local machine";
bash "$GVPATH/$STOP_SCRIPT";
echo "done";

for ip in `cat $TEMP/remote_machines`
do
    echo "Stopping GRINS on $ip";
    ssh -i "$KEYPATH" -o "StrictHostKeyChecking no" $SSH_LOGIN@$ip < "$GVPATH/$STOP_SCRIPT";
    echo "done";
done

echo "Upgrading GRINS on local machine..."
STATUS=$RERUN_STATUS

while [ $STATUS -eq $RERUN_STATUS ]
do 
    bash "$GVPATH/$RUNFILE" upgrader.UpgradeLoader
    STATUS=$?
done

echo "done";



#Prepare script to be run on each remote host
rm "$TEMP/$REMOTE_UPGRADE";

##Execute the ENV file to get local paths
echo "if [ -e $ENV_PATH/$ENV_FILE ]; then . $ENV_PATH/$ENV_FILE; else . $ALT_ENV_PATH/$ENV_FILE; fi" >> "$TEMP/$REMOTE_UPGRADE"

##Get the right run file to execute
echo "if [ \`uname | grep CYGWIN | wc -l\` -eq 1 ]; then" >> "$TEMP/$REMOTE_UPGRADE"
echo "RUNFILE=runwin.sh;" >> "$TEMP/$REMOTE_UPGRADE"
echo "SUDO=\"\";" >> "$TEMP/$REMOTE_UPGRADE"
echo "GVPATH=\$GRAMVAANI_BINARY_PATH_CYG;" >> "$TEMP/$REMOTE_UPGRADE"
echo "KEYPATH=\$SSH_KEY_PATH_CYG;" >> "$TEMP/$REMOTE_UPGRADE"
echo "else RUNFILE=run.sh;" >> "$TEMP/$REMOTE_UPGRADE"
echo "GVPATH=\$GRAMVAANI_BINARY_PATH;" >> "$TEMP/$REMOTE_UPGRADE"
echo "KEYPATH=\$SSH_KEY_PATH;" >> "$TEMP/$REMOTE_UPGRADE"
echo "SUDO=sudo; fi" >> "$TEMP/$REMOTE_UPGRADE"

echo "cd \$GVPATH" >> "$TEMP/$REMOTE_UPGRADE"

##Copy the .grins_version from app machine to .grins_version_max on local machine for upgrader to use
echo "\$SUDO -E scp -i \"\$KEYPATH\" -o \"StrictHostKeyChecking no\" \$SSH_LOGIN@$LOCALIP:$GVPATH/$VERSION_FILE ./$MAX_VERSION_FILE" >> "$TEMP/$REMOTE_UPGRADE"

##Execute the upgrader
echo "RERUN_STATUS=$RERUN_STATUS" >> "$TEMP/$REMOTE_UPGRADE"
echo "STATUS=\$RERUN_STATUS" >> "$TEMP/$REMOTE_UPGRADE"
echo "while [ \$STATUS -eq \$RERUN_STATUS ]" >> "$TEMP/$REMOTE_UPGRADE"
echo "do" >> "$TEMP/$REMOTE_UPGRADE"
echo "\$SUDO bash \$RUNFILE upgrader.UpgradeLoader" >> "$TEMP/$REMOTE_UPGRADE"
echo "STATUS=\$?" >> "$TEMP/$REMOTE_UPGRADE"
echo "done" >> "$TEMP/$REMOTE_UPGRADE"

for ip in `cat $TEMP/remote_machines`
do
    echo "Upgrading GRINS on $ip";
    ssh -i "$KEYPATH" -o "StrictHostKeyChecking no" $SSH_LOGIN@$ip < "$TEMP/$REMOTE_UPGRADE";
    echo "done";
done

echo "Upgrade completed. New version of GRINS is:";
cat $GVPATH/$VERSION_FILE;
echo
echo "Please restart the local and remote machines (if any) for the changes to take effect.";
echo "Press any key to exit.";
read -n1;
exit 0;
