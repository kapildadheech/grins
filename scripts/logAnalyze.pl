#!/usr/bin/perl
use POSIX;

$trace;

sub main {
    $logDir = @_[0];
    #print "LogDir=$logDir\n";
    my @exptDirs = `find $logDir/123* -type d`;
    chomp @exptDirs;
    for $i (@exptDirs) {
	doAnalysis($i);
    }
}

sub printUsage {
    print "You must provide exactly one argument: Log directory path!!\n";
}

sub doAnalysis {
    print "@_[0]\n";
    doErrorAnalysis(@_[0]);
    doStatAnalysis(@_[0]);
    #print "Statistics analysis done.\n";
    doTestAnalysis(@_[0]);
}

sub doErrorAnalysis {
    my $rsAppLog = "@_[0]/RSApp.log";
    my $rsControllerLog = "@_[0]/RSController.log";
    my $audioServiceLog = "@_[0]/AudioService.log";
    my $line;
	
    open $app, "$rsAppLog" or die "Could not open file: $rsAppLog";
    open $rsc, "$rsControllerLog" or die "Could not open file: $rsControllerLog";
    open $as, "$audioServiceLog" or die "Could not open file: $audioServiceLog";
    open my $err, '>', "@_[0]/error.op" or die "Could not open file: @_[0]/error.op";
    
    my $count=1;
    print $err "RSController.log\n";
    for $line (<$rsc>){
	if(($line =~ /ERROR/) || ($line =~ /glibc/) || ($line =~ /SIGSEGV/)){
	    print $err "$count:$line";
	}
	$count++;
    }
    
    $count=1;
    print $err "\n\nAudioService.log\n";
    for $line (<$as>){
	if(($line =~ /ERROR/) || ($line =~ /glibc/) || ($line =~ /SIGSEGV/)){
	    print $err "$count:$line";
	}
	$count++
    }

    $count=1;
    print $err "\n\nRSApp.log\n";
    for $line (<$app>){
	if(($line =~ /ERROR/) || ($line =~ /glibc/) || ($line =~ /SIGSEGV/)){
	    print $err "$count:$line";
	}
	$count++;
    }

    close $app;
    close $rsc;
    close $as;
}

sub tracePlayoutPlayCommand {
    my $state = 0; #Play called by App
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $aud = shift;
    my $rsc = shift;
    my $app = shift;


    my $lapp, $lrsc, $laud;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";
    open my $haud, $aud or die "Could not open file: $aud for reading.\n";

    my @rscdata = <$hrsc>;
    my @auddata = <$haud>;

    my @tmp = split(":",$line);
    my $tgttime = $tmp[0];

    print $trace "Tracing playout play command at $tgttime\n";

    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	    for $laud (@auddata){
		@tmp = split(":",$laud);
		my $audtime = $tmp[0];
		if($state == 0){
		    if($audtime >= $tgttime){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($laud =~ /s:UI_SERVICE/){
			$state = 2;
			#print "Play message received at audio service:\n$laud";
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    #print $laud;
		    if($laud =~ /fileid:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($laud =~ /Play: ResourceResquest message sent/){
			$state = 4;
			#print "Resource request sent by Audio Service.\n";
			@tmp = split(":",$laud);
			$tgttime = $tmp[0];
		    }elsif($laud =~ /ERROR/){
			print $trace "Error found in Audio service at line: $laud.";
			return 0;
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    for $lrsc (@rscdata){
			@tmp = split(":",$lrsc);
			my $rsctime = $tmp[0];
			if($state == 4){
			    if($rsctime >= ($tgttime - $maxskew)){
				$state = 5;
				#print "State=5 at $rsctime";
			    }
			}elsif($state == 5){
			    if(($lrsc =~ /HandleResourceRequest: Entering./)&&($lrsc =~ /AUDIO_SERVICE_WIN/)){
				$state = 6;
			    }elsif($rsctime > ($tgttime + $maxdelay)){
				print $trace "ERROR: $state.\n";
				print $trace "$rsctime,$tgttime";
				return 0;
			    }
			}elsif($state == 6){
			    if ($lrsc =~ /available/){
				#print "Resource available: $lrsc";
				$state = 7;
				last;
			    }elsif ($lrsc =~ /unavailable/){
				print $trace "ERROR: $state";
				return 0;
			    }
			}
		    }
		}elsif($state == 7){
		    if($laud =~ /ResourceAck: Entering. Status=SUCCESS/){
			#print "Resource acquired successfully.\n";
			$state = 8;
		    }elsif ($laud =~ /ResourceAck: Entering. Status=FAILURE/){
			print $trace "ERROR: Resource acquire failed";
			return 0;
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 8){
		    if($laud =~ /PAUSED:PLAYING:VOID_PENDING/){
			print $trace "PlayoutPlay trace successful.\n";
			return 1;
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state";
			return 0;
		    }
		}
	    }
	}
    }
    return 0;
}

sub tracePlayoutPauseCommand {
    my $state = 0; #Play called by App
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $aud = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc, $laud;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";
    open my $haud, $aud or die "Could not open file: $aud for reading.\n";

    my @rscdata = <$hrsc>;
    my @auddata = <$haud>;
    
    my @tmp = split(":",$line);
    my $tgttime = $tmp[0];

    print $trace "Tracing playout pause command at $tgttime\n";

    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	    for $laud (@auddata){
		@tmp = split(":",$laud);
		my $audtime = $tmp[0];
		if($state == 0){
		    if($audtime >= $tgttime){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($laud =~ /s:UI_SERVICE/){
			$state = 2;
			#print "Pause message received at audio service:\n$laud";
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    #print $laud;
		    if($laud =~ /fileid:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($laud =~ /Pause: ResourceRelease message sent/){
			$state = 4;
			#print "Resource release sent by Audio Service.\n";
			@tmp = split(":",$laud);
			$tgttime = $tmp[0];
		    }elsif($laud =~ /ERROR/){
			print $trace "Error found in Audio service at line: $laud.";
			return 0;
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    for $lrsc (@rscdata){
			my @tmp = split(":",$lrsc);
			my $rsctime = $tmp[0];
			if($state == 4){
			    if($rsctime >= ($tgttime - $maxskew)){
				$state = 5;
			    }
			}elsif($state == 5){
			    if(($lrsc =~ /HandleResourceRelease: Entering./)&&($lrsc =~ /AUDIO_SERVICE_WIN/)){
				$state = 6;
			    }elsif($rsctime > ($tgttime + $maxdelay)){
				print $trace "ERROR: $state\n";
				return 0;
			    }
			}elsif($state == 6){
			    if ($lrsc =~ /HandleResourceRelease: Leaving/){
				#print "Resource released.\n";
				$state = 7;
				last;
			    }elsif ($lrsc =~ /ERROR/){
				print $trace "Error found in resource release. $lrsc";
				return 0;
			    }
			}
		    }
		}elsif($state == 7){
		    if($laud =~ /Pause: Ack message sent/){
			print $trace "PlayoutPause trace successful.\n";
			return 1;
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state";
			return 0;
		    }
		}
	    }	
	}
    }
}

sub tracePlayoutStopCommand {
    my $state = 0; #Play called by App
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $aud = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc, $laud;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";
    open my $haud, $aud or die "Could not open file: $aud for reading.\n";

    my @rscdata = <$hrsc>;
    my @auddata = <$haud>;

    my @tmp = split(":",$line);	    
    my $tgttime = $tmp[0];

    print $trace "Tracing playout stop command at $tgttime\n";

    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	    for $laud (@auddata){
		@tmp = split(":",$laud);
		my $audtime = $tmp[0];
		if($state == 0){
		    if($audtime >= $tgttime){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($laud =~ /s:UI_SERVICE/){
			$state = 2;
			#print "Stop message received at audio service:\n$laud";
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    #print $laud;
		    if($laud =~ /fileid:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($laud =~ /ResourceRelease: ResourceRelease message sent/){
			$state = 4;
			#print "Resource release sent by Audio Service\n.";
			@tmp = split(":",$laud);
			$tgttime = $tmp[0];
		    }elsif(($laud =~ /ERROR/) && ($laud !~ /Gstreamer exited/)){
			print $trace "Error found in Audio service at line: $laud.";
			return 0;
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    for $lrsc (@rscdata){
			my @tmp = split(":",$lrsc);
			my $rsctime = $tmp[0];
			if($state == 4){
			    if($rsctime >= ($tgttime - $maxskew)){
				$state = 5;
			    }
			}elsif($state == 5){
			    if(($lrsc =~ /HandleResourceRelease: Entering./)&&($lrsc =~ /AUDIO_SERVICE_WIN/)){
				$state = 6;
			    }elsif($rsctime > ($tgttime + $maxdelay)){
				print $trace "ERROR: $state\n";
				return 0;
			    }
			}elsif($state == 6){
			    if ($lrsc =~ /HandleResourceRelease: Leaving/){
				#print "Resource released.\n";
				$state = 7;
				last;
			    }elsif ($lrsc =~ /ERROR/){
				print $trace "Error found in resource release. $lrsc";
				return 0;
			    }
			}
		    }
		}elsif($state == 7){
		    if($laud =~ /Stop: ACK sent/){
			print $trace "PlayoutPause trace successful.\n";
			return 1;
		    }elsif($audtime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state";
			return 0;
		    }
		}
	    }	
	}
    }
}

sub doPlayoutTestAnalysis {
    my $line, $prevline="", $retval;
    my $aud = @_[0]."/AudioService.log";
    my $rsc = @_[0]."/RSController.log";
    my $app = @_[0]."/RSApp.log";

    #open my $aud, "@_[0]/AudioService.log" or die "Could not open file: @_[0]/AudioService.log for reading.\n";
    #open my $rsc, "@_[0]/RSController.log" or die "Could not open file: @_[0]/RSController.log for reading.\n";
    open my $rsapp, "$app" or die "PlayoutTestAnalysis: Could not open file: $app for reading.\n";

    for $line (<$rsapp>){
	#print $prevline."\n";
	if($prevline =~ /PlayoutTest/){
	    $retval = -1;
	    if($line =~ /WARN/){
		print $trace "$prevline command failed. Error $line\n";
		$retval = 0;
	    }elsif($prevline =~ /About to play/){
		$retval = tracePlayoutPlayCommand($prevline, $aud, $rsc, $app);
	    }elsif($prevline =~ /About to pause/){
		$retval = tracePlayoutPauseCommand($prevline, $aud, $rsc, $app);
	    }elsif (($prevline =~ /About to stop/)||($prevline =~ /Stopping finally/)||($prevline =~ /Done testing/)){
		$retval = tracePlayoutStopCommand($prevline, $aud, $rsc, $app);
	    }
	    if($retval == 0){
		#<>;
	    }
	}
	chomp($line);
	$prevline = $line;
    }
    
}

sub tracePreviewPlayCommand {
    my $state = 0; #Play called by App
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;

    my @tmp = split(":",$line);	    
    my $tgttime = $tmp[0];

    print $trace "Tracing preview play command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
			#print "Play message received at audio service:\n$lrsc";
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /fileid:/){
			$state = 3;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 3){
		    if($lrsc =~ /Sending resource request/){
			$state = 4;
			#print "Resource request sent by Audio Service.\n";
			@tmp = split(":",$lrsc);
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /HandleResourceRequest: Entering./)&&($lrsc =~ /AUDIO_SERVICE_LIN/)){
			$state = 6;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 6){
		    if ($lrsc =~ /available/){
			#print "Resource available: $lrsc";
			$state = 7;
		    }elsif ($lrsc =~ /unavailable/){
			print $trace "ERROR: $state";
			return 0;
		    }elsif ($lrsc =~ /can be preempted/){
			$state = 15;
		    }
		}elsif($state == 15){
		    if($lrsc =~ /Preempting resource/){
			$state = 16;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 16){
		    if($lrsc =~ /HandlePreemptResourceAck: Entering/){
			if($lrsc =~ /TRUE/){
			    $state = 7;
			}else {
			    #print "Preempt resource request recevied negative ack.\n";
			    print $trace "ERROR: $state\n";
			    return 0;
			}
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
		
		elsif($state == 7){
		    if($lrsc =~ /ResourceAck: Entering. Status=SUCCESS/){
			#print "Resource acquired successfully.\n";
			$state = 8;
		    }elsif ($lrsc =~ /ResourceAck: Entering. Status=FAILURE/){
			print $trace "Resource acquire failed.";
			return 0;
		    }elsif($tmptime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 8){
		    if($lrsc =~ /PLAYING/ && $lrsc =~ /previewBin/){
			$state = 9;
			#print "Gstreamer pipeline started.\n";
		    }elsif($tmptime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 9){
		    if($lrsc =~ /ResourceAck: StatisticsUpdate message sent/){
			print $trace "PreviewPlay trace successful.\n";
			return 1;
		    }elsif($tmptime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state";
			return 0;
		    }
		}
	    }
	}
    }
}

sub tracePreviewPauseCommand {
    my $state = 0; #Play called by App
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;
    my @tmp = split(":",$line);	    
    my $tgttime = $tmp[0];


    print $trace "Tracing preview pause command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
			#print "Pause message received at audio service:\n$lrsc";
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /fileid:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($lrsc =~ /preview to PAUSED/){
			$state = 4;
			#print "Resource release sent by Audio Service.\n";
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /HandleResourceRelease/)&&($lrsc =~ /AUDIO_SERVICE_LIN/)){
			$state = 5;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 5){
		    if ($lrsc =~ /HandleResourceRelease: Leaving/){
			#print "Resource released.";
			$state = 6;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 6){
		    if($lrsc =~ /Pause: Ack message sent/){
			print $trace "Ack message sent.\nPreviewPause trace successful.\n";
			return 1;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state";
			return 0;
		    }
		}
		
	    }
	}
    }
}


sub tracePreviewStopCommand {
    my $state = 0; #Play called by App
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;
    my @tmp = split(":",$line);	    
    my $tgttime = $tmp[0];

    print $trace "Tracing preview stop command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
			#print $trace "Stop message received at audio service:\n$lrsc";
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /fileid:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($lrsc =~ /Stop: Entering/){
			$state = 4;
			#print "Resource release sent by Audio Service.\n";
			@tmp = split(":",$lrsc);
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /HandleResourceRelease: Entering./)&&($lrsc =~ /AUDIO_SERVICE_LIN/)){
			$state = 5;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 5){
		    if ($lrsc =~ /HandleResourceRelease: Leaving/){
			#print "Resource released.";
			$state = 6;
		    }
		}elsif($state == 6){
		    if($lrsc =~ /Stop: ACK sent/){
			print $trace "PreviewStop trace successful.\n";
			return 1;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state";
			return 0;
		    }
		}
		
	    }
	}
    }
}


sub doPreviewTestAnalysis {
    my $line, $prevline="", $retval;
    my $rsc = @_[0]."/RSController.log";
    my $app = @_[0]."/RSApp.log";

    #open my $aud, "@_[0]/AudioService.log" or die "Could not open file: @_[0]/AudioService.log for reading.\n";
    #open my $rsc, "@_[0]/RSController.log" or die "Could not open file: @_[0]/RSController.log for reading.\n";
    open my $rsapp, "$app" or die "PreviewTestAnalysis: Could not open file: $app for reading.\n";

    for $line (<$rsapp>){
	#print $prevline."\n";
	if($prevline =~ /PreviewTest/){
	    $retval = -1;
	    if($line =~ /WARN/){
		print $trace "$prevline command failed. Error $line\n";
		$retval = 0;
	    }elsif($prevline =~ /About to play/){
		$retval = tracePreviewPlayCommand($prevline, $rsc, $app);
	    }elsif($prevline =~ /About to pause/){
		$retval = tracePreviewPauseCommand($prevline, $rsc, $app);
	    }elsif (($prevline =~ /About to stop/)||($prevline =~ /Stopping finally/)||($prevline =~ /Done testing/)){
		$retval = tracePreviewStopCommand($prevline, $rsc, $app);
	    }
	    if($retval == 0){
		#<>;
	    }
	}
	chomp($line);
	$prevline = $line;
    }
}

sub traceArchiverStartCommand {
    my $state = 0; 
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;
    my @tmp = split(":",$line);	    
    my $tgttime = $tmp[0];

    print $trace "Tracing archiver start command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){

	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
			#print "Start message received at archover service:\n$lrsc";
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /language:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($lrsc =~ /HandleArchiverCommandMessage: portType=JACK/){
			$state = 15;
		    }elsif($lrsc =~ /HandleArchiverCommandMessage: portType=ALSA/){
			$state = 4;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /READY:PAUSED:PLAYING/ && $lrsc =~ /archiverBin/)){
			$state = 5;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }		    
		}elsif($state == 15){
		    if(($lrsc =~ /READY:PAUSED:PLAYING/ && $lrsc =~ /archiverBin/)){
			$state = 16;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
		elsif($state == 16){
		    if($lrsc =~ /CHANNEL_SERVICE_COMMON: HandleIncomingMessage: Entering. From: ARCHIVER_SERVICE_COMMON/){
			$state = 17;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 17){
		    if($lrsc =~ /HandleChannelCommandMessage: Connected jack ports/){
			$state = 18;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 18){
		    if($lrsc =~ / HandleChannelAckMessage: Entering. Received ACK:SUCCESS/){
			$state = 6;
		    }elsif($lrsc =~ / HandleChannelAckMessage: Entering. Received ACK:FAILURE/){
			print $trace "Error channel ack returned failure.";
			return 0;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 5){
		    if($lrsc =~ /HandleArchiverCommandMessage: Archiving started/){
			$state = 6;
		    }elsif ($lrsc =~ /HandleArchiverCommandMessage: Archiving Failed/){
			print $trace "Archiving failed.";
			return 0;
		    }elsif($tmptime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 6){
		    print $trace "ArchiverStart trace succeeded.\n";
		    return 1;
		}
	    }
	}
    }
}

sub traceArchiverStopCommand {
    my $state = 0; 
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;
    my @tmp = split(":",$line);	    
    my $tgttime = $tmp[0];

    print $trace "Tracing archiver stop command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	  
	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
			#print "Stopo message received at archiver service:\n$lrsc";
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /language:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if(($lrsc =~ /HandleArchiverCommandMessage/) && ($lrsc =~ /STOP_MSG/)){
			$state = 4;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /ARCHIVER_SERVICE_COMMON: StopArchiving: Stopping Archival./)){
			$state = 5;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }		    
		}elsif($state == 5){
		    if(($lrsc =~ /READY:NULL:VOID_PENDING/)){
			$state = 6;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
		elsif($state == 6){
		    if($lrsc =~ / HandleArchiverCommandMessage: ACK sent/){
			print $trace "Archiver Stop command trace succeeded.\n";
			return 1;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
	    }
	}
    }
}


sub doArchiverTestAnalysis {
    my $line, $prevline="", $retval;
    my $rsc = @_[0]."/RSController.log";
    my $app = @_[0]."/RSApp.log";

    #open my $aud, "@_[0]/AudioService.log" or die "Could not open file: @_[0]/AudioService.log for reading.\n";
    #open my $rsc, "@_[0]/RSController.log" or die "Could not open file: @_[0]/RSController.log for reading.\n";
    open my $rsapp, "$app" or die "ArchiverTestAnalysis: Could not open file: $app for reading.\n";

    for $line (<$rsapp>){
	#print $prevline."\n";
	if($prevline =~ /ArchiverTest/){
	    $retval = -1;
	    if($line =~ /WARN/){
		print $trace "$prevline command failed. Error $line\n";
		$retval = 0;
	    }elsif($prevline =~ /About to start/){
		$retval = traceArchiverStartCommand($prevline, $rsc, $app);
	    }elsif (($prevline =~ /About to stop/)||($prevline =~ /Done testing/)){
		$retval = traceArchiverStopCommand($prevline, $rsc, $app);
	    }
	    if($retval == 0){
		#<>;
	    }
	}
	chomp($line);
	$prevline = $line;
    }
}

sub traceMonitorStartCommand {
    my $state = 0; 
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;
    my @tmp = split(":",$line);
    my $tgttime = $tmp[0];

    print $trace "Tracing monitor start command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	
	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /widgetname:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if(($lrsc =~ /HandleMonitorCommandMessage: srcPortType=JACK/)||
		       ($lrsc =~ /HandleMonitorCommandMessage: srcPortType=ALSA/)){
			$state = 4;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /HandleMonitorCommandMessage: ResourceResquest message sent/)){
			$state = 5;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }		    
		}elsif($state == 5){
		    if(($lrsc =~ /held for service: MONITOR_SERVICE_COMMON/)){
			$state = 6;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
		elsif($state == 6){
		    if($lrsc =~ /MONITOR_SERVICE_COMMON: HandleResourceAckMessage: Positive Ack received: ALSA/){
			$state = 7;
		    }elsif($lrsc =~ /MONITOR_SERVICE_COMMON: HandleResourceAckMessage: Positive Ack received: JACK/){
			$state = 15;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 7){
		    if($lrsc =~ /MonitorPipe: Start: Leaving. Gstreamer: Started pipeline successfully/){
			$state = 8;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 8){
		    if($lrsc =~ /HandleResourceAckMessage: Monitoring started/){
			$state = 9;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
		elsif($state == 15){
		    if($lrsc =~ /HandleChannelCommandMessage: Connected jack ports/){
			$state = 16;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 16){
		    if($lrsc =~ /HandleChannelAckMessage: Entering. Received ACK:SUCCESS/){
			$state = 17;
		    }elsif($lrsc =~ / HandleChannelAckMessage: Entering. Received ACK:FAILURE/){
			print $trace "Error channel ack returned failure.";
			return 0;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 17){
		    if($lrsc =~ /HandleChannelAckMessage: Monitoring started/){
			$state = 9;
		    }elsif ($lrsc =~ /HandleChannelAckMessage: Monitoring failed/){
			print $trace "Monitoring failed.";
			return 0;
		    }elsif($tmptime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 9){
		    print $trace "MonitorStart trace succeeded.\n";
		    return 1;
		}
	    }
	}
    }
}

sub traceMonitorStopCommand {
    my $state = 0; 
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;
    my @tmp = split(":",$line);
    my $tgttime = $tmp[0];

    print $trace "Tracing monitor stop command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	   

	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /widgetname:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($lrsc =~ /HandleMonitorCommandMessage: Entering/){
			$state = 4;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /MONITOR_SERVICE_COMMON: StopMonitoring: Stopping/)){
			$state = 5;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }		    
		}elsif($state == 5){
		    if(($lrsc =~ /StopMonitoring: Monitoring stopped/)){
			$state = 6;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 6){
		    if($lrsc =~ /MONITOR_SERVICE/ && $lrsc =~ /ReleaseResources: ResourceRelease message sent/){
			$state = 7;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 7){
		    if($lrsc =~ /HandleMonitorCommandMessage: ACK sent/){
			print $trace "MonitorStop trace successful\n";
			return 1;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
	    }
	}
    }
}


sub doMonitorTestAnalysis {
    my $line, $prevline="", $retval;
    my $rsc = @_[0]."/RSController.log";
    my $app = @_[0]."/RSApp.log";

    #open my $aud, "@_[0]/AudioService.log" or die "Could not open file: @_[0]/AudioService.log for reading.\n";
    #open my $rsc, "@_[0]/RSController.log" or die "Could not open file: @_[0]/RSController.log for reading.\n";
    open my $rsapp, "$app" or die "MonitorTestAnalysis: Could not open file: $app for reading.\n";

    for $line (<$rsapp>){
	#print $prevline."\n";
	if($prevline =~ /MonitorTest/){
	    $retval = -1;
	    if($line =~ /WARN/){
		print $trace "$prevline command failed. Error $line\n";
		$retval = 0;
	    }elsif($prevline =~ /About to start/){
		$retval = traceMonitorStartCommand($prevline, $rsc, $app);
	    }elsif (($prevline =~ /About to stop/)||($prevline =~ /Done testing/)){
		$retval = traceMonitorStopCommand($prevline, $rsc, $app);
	    }
	    if($retval == 0){
		#<>;
	    }
	}
	chomp($line);
	$prevline = $line;
    }
}

sub traceMicStartCommand {
    my $state = 0; 
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;
    my @tmp = split(":",$line);
    my $tgttime = $tmp[0];

    print $trace "Tracing mic enable command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /widgetname:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($lrsc =~ /HandleMicCommandMessage: srcPortType/){
			$state = 4;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /HandleResourceRequest/ && $lrsc =~ /MIC_SERVICE/)){
			$state = 5;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }		    
		}elsif($state == 5){
		    if(($lrsc =~ /held for service: MIC_SERVICE/)){
			$state = 6;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
		elsif($state == 6){
		    if($lrsc =~ /MIC_SERVICE_COMMON: HandleResourceAckMessage: Positive Ack received: ALSA/){
			$state = 7;
		    }elsif($lrsc =~ /MONITOR_SERVICE_COMMON: HandleResourceAckMessage: Positive Ack received: JACK/){
			$state = 15;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 7){
		    if($lrsc =~ /READY:PAUSED:PLAYING/ && $lrsc =~ /micBin/){
			$state = 8;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 8){
		    if($lrsc =~ /HandleResourceAckMessage: Enabled. ACK message sent./){
			$state = 9;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
		elsif($state == 15){
		    if($lrsc =~ /HandleChannelCommandMessage: Connected jack ports/){
			$state = 16;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 16){
		    if($lrsc =~ /HandleChannelAckMessage: Entering. Received ACK:SUCCESS/){
			$state = 17;
		    }elsif($lrsc =~ /HandleChannelAckMessage: Entering. Received ACK:FAILURE/){
			print "Error channel ack returned failure.";
			return 0;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 17){
		    if($lrsc =~ /HandleChannelAckMessage: Enabled/){
			$state = 9;
		    }elsif($tmptime > ($tgttime + $maxdelay)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 9){
		    print $trace "MicStart trace succeeded.\n";
		    return 1;
		}
	    }
	}
    }
}

sub traceMicStopCommand {
    my $state = 0; 
    my $maxskew = 200, $maxdelay=2000;
    my $line = shift;
    my $rsc = shift;
    my $app = shift;
    
    my $lapp, $lrsc;

    open my $happ, $app or die "Could not open file: $app for reading.\n";
    open my $hrsc, $rsc or die "Could not open file: $rsc for reading.\n";

    my @rscdata = <$hrsc>;
    my @tmp = split(":",$line);
    my $tgttime = $tmp[0];
    
    print $trace "Tracing mic disable command at $tgttime\n";
    for $lapp (<$happ>){
	chomp($lapp);
	if($lapp eq $line){
	    for $lrsc (@rscdata){
		@tmp = split(":",$lrsc);
		my $tmptime = $tmp[0];
		if($state == 0){
		    if($tmptime >= $tgttime-$maxskew){
			$state = 1;
		    }
		}elsif($state == 1){
		    if($lrsc =~ /s:UI_SERVICE/){
			$state = 2;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 2){
		    if($lrsc=~ /widgetname:/){
			$state = 3;
		    }
		}elsif($state == 3){
		    if($lrsc =~ /HandleMicCommandMessage: Entering/){
			$state = 4;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 4){
		    if(($lrsc =~ /MIC_SERVICE_COMMON: DisableMicService: Entering/)){
			$state = 5;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }		    
		}elsif($state == 5){
		    if(($lrsc =~ /PAUSED:READY:NULL/) && ($lrsc =~ /micBin/)){
			$state = 6;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 6){
		    if($lrsc =~ /MIC_SERVICE_COMMON: ReleaseResources: ResourceRelease message sent/){
			$state = 7;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}elsif($state == 7){
		    if($lrsc =~ /HandleMicCommandMessage: ACK sent/){
			print $trace "MicDisable trace successful.\n";
			return 1;
		    }elsif($tmptime > ($tgttime + $maxdelay + $maxskew)){
			print $trace "ERROR: $state\n";
			return 0;
		    }
		}
	    }
	}
    }
}


sub doMicTestAnalysis {
    my $line, $prevline="", $retval;
    my $rsc = @_[0]."/RSController.log";
    my $app = @_[0]."/RSApp.log";

    #open my $aud, "@_[0]/AudioService.log" or die "Could not open file: @_[0]/AudioService.log for reading.\n";
    #open my $rsc, "@_[0]/RSController.log" or die "Could not open file: @_[0]/RSController.log for reading.\n";
    open my $rsapp, "$app" or die "MicTestAnalysis: Could not open file: $app for reading.\n";

    for $line (<$rsapp>){
	#print $prevline."\n";
	if($prevline =~ /MicTest/){
	    $retval = -1;
	    if($line =~ /WARN/){
		print $trace "$prevline command failed. Error $line\n";
		$retval = 0;
	    }elsif($prevline =~ /Enable/){
		$retval = traceMicStartCommand($prevline, $rsc, $app);
	    }elsif (($prevline =~ /Disable/)||($prevline =~ /Done testing/)){
		$retval = traceMicStopCommand($prevline, $rsc, $app);
	    }
	    if($retval == 0){
		#<>;
	    }
	}
	chomp($line);
	$prevline = $line;
    }
}


sub doTestAnalysis {
    my $line;
    my @tests;
    my %status;
    my %calls;
    open my $testfile, "@_[0]/test.conf" or die "Could not open file @_[0]/test.conf for reading.\n";
    open $trace, '>', "@_[0]/trace.op" or die "Could not open file @_[0]/trace.op for writing.\n";
    %calls = (
	"PlayoutTest" => \&doPlayoutTestAnalysis,
	"PreviewTest" => \&doPreviewTestAnalysis,
	"ArchiverTest" => \&doArchiverTestAnalysis,
	"MicTest" => \&doMicTestAnalysis,
	"MonitorTest" => \&doMonitorTestAnalysis, );

    for $line (<$testfile>){
	chomp($line);
	$line =~ s/^\s*//;

	if($line =~ /^#/){
	    next;
	}
	
	$line =~ s/\s*$//;
	$line =~ s/\s+//g;
	
	@tests = split(/,/,$line);

	if($#tests == 0){#ignoring the test duration for now. use it if needed.
	    next;
	}
	
	for(my $i=1; $i<=$#tests; $i++){
	    print $trace "test: $tests[$i]";
	    if($status{$tests[$i]} == 1){
		next;
	    }
	    $status{$tests[$i]} = 1;

	    if($calls{$tests[$i]}){
		$calls{$tests[$i]}->(@_[0]);
	    }
	}
	
    }
}

sub doStatAnalysis {
    my $rsAppLog = "@_[0]/RSApp.log";
    my $rsControllerLog = "@_[0]/RSController.log";
    my $audioServiceLog = "@_[0]/AudioService.log";
    my $cpuLog = "@_[0]/CPU.log";
    my $winCpuLog = "@_[0]/winCPU.log";
    my $statFile = "@_[0]/Stat.op";
    my $stat;
    #open $stat, '>', "$statFile" or die "Could not open file: $statFile for writing";
    doCPUAnalysis($cpuLog,$statFile,@_[0]);
    doHeartBeatAnalysis($rsControllerLog,$statFile);
    doCommandExecutionAnalysis($rsAppLog, $statFile);
    doWinCPUAnalysis($winCpuLog, $statFile, @_[0]);
    plotGraphs(@_[0]);
    #doXrunAnalysis($rsControllerLog, $statFile);
}

sub plotGraphs {
    my $cmd="gnuplot ~/gv/gramvaani-automation/scripts/cpu.plot > @_[0]/cpu.png";
    my $cmd1="gnuplot ~/gv/gramvaani-automation/scripts/wincpu.plot > @_[0]/wincpu.png";
    my $cmd2="gnuplot ~/gv/gramvaani-automation/scripts/msgTime.plot > @_[0]/msgTime.png";
    
    system $cmd;
    system $cmd1;
    system $cmd2;
}

sub doXrunAnalysis {
    my $line;
    my $count=0, $avg=0;
    open my $rsc, "@_[0]" or die "Could not open file: @_[0] for reading.\n";
    open my $stat, '>>', "@_[1]" or die "Could not open file: @_[1] for appending.\n";
    
    print $stat "XRun Statistics\n";
    print $stat "Time\t\txrun (msecs)\n";
    for $line (<$rsc>){
	if($line =~ /xrun/){
	    $count++;
	    chomp($line);
	    $line =~ s/^\s*//;
	    $line =~ s/\s*$//;
	    $line =~ s/\s+/ /g;
	    my @vals = split(/ /,$line);
	    my $xrun = $vals[10];
	    my @time = split(/:/,$vals[0]);
	    print $stat "$time[0]\t$xrun\n";
	    $avg=$avg+$xrun;
	}
    }
    
    $avg = $avg/$count if $count!=0;
    printf $stat "Count\t\t%.2f\n", $count;
    printf $stat "Avg\t\t%.2f\n",$avg;

    close $stat;
    close $rsc;
}

sub printArray {
    my $i;
    my $arrayref = shift;
    print "ArrayContents below:\n";
    for($i=0;$i<$#$arrayref;$i=$i+3){
	my $string = $arrayref->[$i].":".$arrayref->[$i+1].":".$arrayref->[$i+2];
	print "$string\n";
    }
    <>;
}

sub addCommand {
    my $lref = shift;
    my $cref = shift;
    my $i;
    for($i=0;$i<$#$lref;$i=$i+3){
	if($lref->[$i] eq $cref->[0]){
	    my $str1 = join(":",@$cref);
	    my $str2 = $$lref[$i].":".$$lref[$i+1].":".$$lref[$i+2];
	    print "Failed to add $str1 because $str2 is already present.\n";
	    return;
	}
    }
    push(@$lref, @$cref);
}

sub doCommandExecutionAnalysis {
    my $line, @lines, $l;
    my @command, @ack, @commandlist;
    my @successdiffs;
    my @failurediffs;
    my $type;
    my $i, $j;
    my $matched=0;

    open my $rsa, "@_[0]" or die "Could not open file: @_[0] for reading.\n";
    open my $stat, '>>', "@_[1]" or die "Could not open file: @_[1] for appending.\n";
    open my $cmfile, '>', "/tmp/cm.dat" or die "Could not open file: /tmp/cm.dat for writtin.\n";

    for $l (<$rsa>) {
	push(@lines,$l);
	if($#lines < 6){
	    next;
	}elsif($#lines == 8){
	    shift(@lines);
	}
	$line = $lines[0];
	if($line =~ /About/){
	    chomp($line);
	    my $nextline = $lines[1];
	    if($nextline =~ /WARN/){
		#the command has failed. Just ignore.
		next;
	    }
	    if($line =~ /PlayoutTest/){
		@command = processPlayoutCommand($line);
	    }elsif($line =~ /PreviewTest/){
		@command = processPreviewCommand($line);
	    }elsif($line =~ /ArchiverTest/){
		@command = processArchiverCommand($line);
	    }elsif($line =~ /MicTest/){
		@command = processMicCommand($line);
	    }elsif($line =~ /MonitorTest/){
		@command = processMonitorCommand($line);
	    }else {
		#no match. Just ignore
		next;
	    }
	    #my $string = join(":",@command);
	    #print "Found a command: $string\n";
	    #printArray(\@commandlist);
	    addCommand(\@commandlist,\@command);
	    @command=("","","");
	}elsif($line =~ /IPCMessage/){
	    if($line =~ /AUDIO_SERVICE_WIN/){
		@ack = processAck(\@lines);
		$type = "Playout";
	    }elsif($line =~ /AUDIO_SERVICE_LIN/) {
		@ack = processAck(\@lines);
		$type = "Preview";
	    }elsif($line =~ /ARCHIVER_SERVICE/){
		@ack = processAck(\@lines);
		$type = "Archive";
	    }elsif($line =~ /MIC_SERVICE/){
		@ack = processAck(\@lines);
		$type = "Mic";
	    }elsif($line =~ /MONITOR_SERVICE/){
		@ack = processAck(\@lines);
		$type = "Monitor";
	    }else {
		#None of the above. Just ignore.
		next;
	    }
	    my @tmp = split(/ /,$line);
	    @tmp = split(/:/,$tmp[0]);
	    $ack[2]=$tmp[0];
	    
	    #my $ackstring = join(":",@ack);
	    #print "Found ack: $ackstring.\n";
	    #printArray(\@commandlist);

	    my $comm = $ack[0];
	    my $pos=0;$item;
	    for ($i=0; $i <= $#commandlist; $i++){
		$item = $commandlist[$i];
		if($item eq $type){
		    if($commandlist[$i+1] eq $comm){
			if($ack[1] eq "success"){
			    my $tmpdiff = eval($ack[2]-$commandlist[$pos+2]);
			    print $cmfile "$commandlist[$pos+2] $tmpdiff";
			    push(@successdiffs, eval($ack[2]-$commandlist[$i+2]));
			    $matched++;
			    #if($matched > 90 && $matched < 100){
				#my $string = join("&",@lines);
				#my $string2 = $item."____".$commandlist[$i]."____".$commandlist[$i+1]."____".$commandlist[$i+2];
				#print "$matched:Found a match in success - $string - $string2\n";
				#<>;
			    #}
			    splice(@commandlist,$pos,$#command+1);
			    last;
			}
			else{
			    my $tmpdiff = eval($ack[2]-$commandlist[$pos+2]);
			    print $cmfile "$commandlist[$pos+2] $tmpdiff";
			    push(@failurediffs, $tmpdiff);
			    splice(@commandlist,$pos,$#command+1);
			    #print "Found a match in failure. $ack[1]\n";
			    last;
			}
		    }
		}
		$pos++;
	    }
	    if($pos = $#commandlist+1){
		my $string = join(":",@ack);
		#print "Warning! Scanned through the commands sent but did not find one matching to the ack - $string - $type - $line\n";
	    }
	    @ack="";
	}
    }
    print $stat "Command Message Statistics\n";
    print $stat "\t\tCount\t\tAvg RTT\t\tMin RTT\t\tMax RTT\t\tStd-dev RTT\n";

    my $avgRTT, $minRTT, $maxRTT, $stdDevRTT;
    $maxRTT=0;
    $minRTT=100000; # just some very large amount
    $avgRTT=0;
    $stdDevRTT=0;
    $i=0;
    for my $val (@successdiffs) {
	$maxRTT = $val if $val > $maxRTT;
	$minRTT = $val if $val < $minRTT;
	$avgRTT = $avgRTT + $val;
	$stdDevRTT = $stdDevRTT + ($val * $val);
    }
    if($#successdiffs >= 0){
	$avgRTT = $avgRTT/($#successdiffs+1);
	$stdDevRTT = ($stdDevRTT/($#successdiffs+1)) - ($avgRTT * $avgRTT);
	if($stdDevRTT > 0){
	    $stdDevRTT = sqrt($stdDevRTT);
	}
    }else{
	$minRTT = 0;
    }
    printf $stat "Success\t\t%.2f\t\t%.2f\t\t%.2f\t\t%.2f\t\t%.2f\n",($#successdiffs+1),$avgRTT, $minRTT, $maxRTT, $stdDevRTT;

    #for failurediffs
    $maxRTT=0;
    $minRTT=100000; # just some very large amount
    $avgRTT=0;
    $stdDevRTT=0;

    for my $val (@failurediffs) {
	$maxRTT = $val if $val > $maxRTT;
	$minRTT = $val if $val < $minRTT;
	$avgRTT = $avgRTT + $val;
	$stdDevRTT = $stdDevRTT + ($val * $val);
    }
    if($#failurdiffs >= 0){
	$avgRTT = $avgRTT/($#failurediffs+1);
	$stdDevRTT = ($stdDevRTT/($#failurediffs+1)) - ($avgRTT * $avgRTT);
	if($stdDevRTT > 0){
	    $stdDevRTT = sqrt($stdDevRTT);
	}
    }else{
	$minRTT = 0;
    }
    printf $stat "Failure\t\t%.2f\t\t%.2f\t\t%.2f\t\t%.2f\t\t%.2f\n\n",($#failurediffs+1), $avgRTT, $minRTT, $maxRTT, $stdDevRTT;

    close $stat;
    close $rsa;
    close $cmfile;
}

sub processAck {
    my $linref = @_[0];
    my @ack = ("","");

    for my $line (@$linref){
	#print "ProcessAck: $line\n";
	chomp($line);
	if($line =~ /command:/){
	    my @tmp1 = split(/:/,$line);
	    my @tmp2 = split(/_/,$tmp1[1]);
	    $ack[0]=lc($tmp2[0]);
	}elsif($line =~ /ack:/){
	    my @tmp1 = split(/:/,$line);
	    $ack[1] = lc($tmp1[1]);
	}
    }
    return @ack;
}

sub processMonitorCommand {
    my $line = @_[0];
    my @command;
    
    push(@command,"Monitor");

    $line =~ s/\s+/ /g;
    @linarr = split(/ /,$line);
    
    push(@command, lc($linarr[6]));
    my @time = split(/:/,$linarr[0]);
    push(@command, $time[0]);

    return @command;
}

sub processMicCommand {
    my $line = @_[0];
    my @command;
    
    push(@command,"Mic");

    $line =~ s/\s+/ /g;
    if($line =~ /Enable/) {
	push(@command, "enable");
    }elsif(($line =~ /Disable/) || ($line =~ /disabling/)) {
	push(@command, "disable");
    }elsif($line =~ /Muting/){
	pushh(@command, "mute");
    }elsif($line =~ /Unmuting/){
	push(@command, "unmute");
    }elsif($line =~ /Volume/){
	push(@command, "volume");
    }else{
	print "Warning! Unknown command in - $line\n";
	push(@command,"Unknown");
    }
    
    @linarr = split(/ /,$line);
    
    my @time = split(/:/,$linarr[0]);
    push(@command, $time[0]);

    return @command;
}

sub processArchiverCommand {
    my $line = @_[0];
    my @command;
    
    push(@command,"Archive");

    $line =~ s/\s+/ /g;
    @linarr = split(/ /,$line);
    
    push(@command, $linarr[6]);
    my @time = split(/:/,$linarr[0]);
    push(@command, $time[0]);

    return @command;
}

sub processPreviewCommand {
    my $line = @_[0];
    my @command;
    
    push(@command,"Preview");

    chop($line);
    $line =~ s/\s+/ /g;
    @linarr = split(/ /,$line);
    
    push(@command, lc($linarr[6]));
    my @time = split(/:/,$linarr[0]);
    push(@command, $time[0]);

    return @command;
}

sub processPlayoutCommand {
    my $line = @_[0];
    my @command;
    
    push(@command,"Playout");

    chop($line);
    $line =~ s/\s+/ /g;
    @linarr = split(/ /,$line);
    
    push(@command, lc($linarr[6]));
    my @time = split(/:/,$linarr[0]);
    push(@command, $time[0]);

    return @command;
}



sub doHeartBeatAnalysis {
    my @vals, $line;
    my %message;
    my %time;
    my @timediffs;
    my @hbtime;

    open my $rsc, "@_[0]" or die "Could not open file: @_[0] for reading.\n";
    open my $stat, '>>', "@_[1]" or die "Could not open file: @_[1] for appending.\n";
    open my $hbfile, '>', "/tmp/hb.dat" or die "Could not open file: /tmp/hb.dat for writing.\n";

    print $stat "Heartbeat Statistics\n";
    print $stat "Count\t\tAvg RTT\t\tMin RTT\t\tMax RTT\t\tStdDev RTT\n";
    
    for $line (<$rsc>) {
	if ($line =~ /Heartbeat/ and $line =~ /sequence/) {
	    chomp($line);
	    $line =~ s/^\s+//;
	    $line =~ s/\s+$//;
	    $line =~ s/\s+/ /g;
	    if($line =~ /to:/) {
		@vals = split(/ /,$line);
		my $service = $vals[7];
		if($message{$service} ne "") {
		    print "Warning: A message to $service with seq no $message{$service} has been sent but not received back.\n";
		}else {
		    my @seqno = split(/=/,$vals[9]);
		    my $seq = $seqno[1];
		    my @msgtime = split(/:/,$vals[0]);
		    $time{$service}=$msgtime[0];
		    $message{$service}=$seq;
		    #print "$service => $seq";
		}
	    }else {
		if($line =~ /from/) {
		    @vals = split(/ /,$line);
		    my $service = $vals[7];
		    my @seqno = split(/=/,$vals[9]);
		    my $seq = $seqno[1];
		    #print "$service => $seq\n";
		    if($message{$service} eq "") {
			print "Warning: Heartbeat message received from $service with seq no $seq but no corresponding message on record.\n";
		    }else {
			if ($message{$service}==$seq){
			    my @msgtime = split(/:/,$vals[0]);
			    my $timediff = $msgtime[0] - $time{$service};
			    #print "$timediff\n";
			    print $hbfile "$time{$service} $timediff\n";
			    @timediffs[$#timediffs+1] = $timediff;
			    $message{$service}="";
			}
			#print "$service => $seq";
		    }
		}
	    }
	    #$line = join(" ",@vals);
	    #print "$line\n";
	    
	}
    }#end of for loop

    my $avgRTT, $minRTT, $maxRTT, $stdDevRTT;
    $maxRTT=0;
    $minRTT=100000; # just some very large amount
    $avgRTT=0;
    $stdDevRTT=0;

    for my $val (@timediffs) {
	$maxRTT = $val if $val > $maxRTT;
	$minRTT = $val if $val < $minRTT;
	$avgRTT = $avgRTT + $val;
	$stdDevRTT = $stdDevRTT + ($val * $val);
    }

    $avgRTT = $avgRTT/($#timediffs+1);
    $stdDevRTT = ($stdDevRTT/($#timediffs+1)) - ($avgRTT * $avgRTT);
    if($stdDevRTT > 0){
	$stdDevRTT = sqrt($stdDevRTT);
    }
    printf $stat "%d\t\t%.2f\t\t%.2f\t\t%.2f\t\t%.2f\n\n",($#timediffs+1),$avgRTT, $minRTT, $maxRTT, $stdDevRTT;
    #open my $td, '>',"timediff.txt" or die "Cant open file.\n";
    #for my $t (@timediffs){
	#my $val = ceil($t/10)*10;
	#print $td "$t $val 1\n";
    #}
    #print "timefiddcount=$#timediffs\n";
    close($stat);
    close($rsc);
    close($hbfile);
}


sub doCPUAnalysis {
    my $cpu;
    my @vals, @pruned;
    my $line;
    my $count=0;
    my @usr, $usrMax=0, $usrMin, $usrAvg=0, $usrStdDev, $usrSquare=0;
    my @nice, $niceMax=0, $niceMin, $niceAvg=0, $niceStdDev, $niceSquare=0;
    my @sys, $sysMax=0, $sysMin, $sysAvg=0, $sysStdDev, $sysSquare=0;
    my @idle, $idleMax=0, $idleMin, $idleAvg=0, $idleStdDev, $idleSquare=0;
    my @freq, $freqMax=0, $freqMin, $freqAvg=0, $freqStdDev, $freqSquare=0;
    my @ctemp, $ctempMax=0, $ctempMin, $ctempAvg=0, $ctempStdDev, $ctempSquare=0;
    my @itemp, $itempMax=0, $itempMin, $itempAvg=0, $itempStdDev, $itempSquare=0;

    open $cpu, "@_[0]" or die "Could not open file: @_[0] for reading";
    open my $stat, '>', "@_[1]" or die "Could not open file: @_[1] for writing";
    open my $data, '>', "/tmp/cpu.dat" or die "Could not open file: /tmp/cpu.dat for writing.";
    print $stat "CPU Statistics\n";
    print $stat "\tUser\tNice\tSys\tIdle\tFreq\t\tCPU Temp\t Int Temp\n";
    for $line (<$cpu>) {
	chomp($line);
	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	$line =~ s/\s+/ /g;
	@vals = split(/ /,$line);
	if ($line =~ /all/) {
	    $usr[$count] = $vals[3];
	    $nice[$count] = $vals[4];
	    $sys[$count] = $vals[5];
	    $idle[$count] = $vals[8];
	    $freq[$count] = $vals[9];
	    #for my $val (@vals) {
		#print "$val";
	    #}
	    print $data "$count @usr[$count] @sys[$count] @idle[$count]\n";
	}elsif($line =~ /CPU/){
	    my @tempVal = split(//,$vals[2]);
	    #for my $val (@tempVal){
		#print $val;
	    #}
	    $ctemp[$count] = eval($tempVal[1]*10 + $tempVal[2] + $tempVal[4]/10);
	    #print "$ctemp[$count]\n";
	}elsif($line =~ /Int/){
	    my @tempVal = split(//,$vals[2]);
	    @itemp[$count] = eval($tempVal[1]*10 + $tempVal[2] + $tempVal[4]/10);
	    $count++;
	}
    }
    $usrMin = 100;
    for my $val (@usr) {
	$usrAvg = $usrAvg + $val;
	$usrMax = $val if $val > $usrMax;
	$usrMin = $val if $val < $usrMin;
	$usrSquare = $usrSquare + ($val * $val);
	#print "Sum of square=$usrSquare\n";
    }
    $usrAvg = $usrAvg/($#usr+1);
    #print "Sum of square=$usrSquare\n";
    $usrStdDev = sqrt(($usrSquare/($#usr+1)) - ($usrAvg * $usrAvg));
    
    
    
    $niceMin=100;
    for my $val (@nice) {
	$niceAvg = $niceAvg + $val;
	$niceMax = $val if $val > $niceMax;
	$niceMin = $val if $val < $niceMin;
	$niceSquare = $niceSquare + ($val * $val);
    }
    $niceAvg = $niceAvg/($#nice+1);
    $niceStdDev = ($niceSquare/($#nice+1)) - ($niceAvg * $niceAvg);
    if($niceStdDev > 0){
	$niceStdDev = sqrt($niceStdDev);
    }

    $sysMin=100;
    for my $val (@sys) {
	$sysAvg = $sysAvg + $val;
	$sysMax = $val if $val > $sysMax;
	$sysMin = $val if $val < $sysMin;
	$sysSquare = $sysSquare + ($val * $val);
    }
    $sysAvg = $sysAvg/($#sys+1);
    $sysStdDev = ($sysSquare/($#sys+1)) - ($sysAvg * $sysAvg);
    if($sysStdDev > 0){
	$sysStdDev = sqrt($sysStdDev);
    }
    
    $idleMin = 100;
    for my $val (@idle) {
	$idleAvg = $idleAvg + $val;
	$idleMax = $val if $val > $idleMax;
	$idleMin = $val if $val < $idleMin;
	$idleSquare = $idleSquare + ($val * $val);
    }
    $idleAvg = $idleAvg/($#idle+1);
    $idleStdDev = ($idleSquare/($#idle+1)) - ($idleAvg *$idleAvg);
    if($idleStdDev > 0){
	$idleStdDev = sqrt($idleStdDev);
    }

    $freqMin = 5000000; #in KHz
    for my $val (@freq) {
	$freqAvg = $freqAvg + $val;
	$freqMax = $val if $val > $freqMax;
	$freqMin = $val if $val < $freqMin;
	$freqSquare = $freqSquare + ($val * $val);
    }
    $freqAvg = $freqAvg/($#freq+1);
    $freqStdDev = ($freqSquare/($#freq+1)) - ($freqAvg * $freqAvg);
    if($freqStdDev > 0){
	$freqStdDev = sqrt($freqStdDev);
    }
    
    $ctempMin=200; #in deg C
    for my $val (@ctemp) {
	$ctempAvg = $ctempAvg + $val;
	$ctempMax = $val if $val > $ctempMax;
	$ctempMin = $val if $val < $ctempMin;
	$ctempSquare = $ctempSquare + ($val * $val);
    }
    $ctempAvg = $ctempAvg/($#ctemp+1);
    $ctempStdDev = ($ctempSquare/($#ctemp+1)) - ($ctempAvg * $ctempAvg);
    if($ctempStdDev > 0){
       $ctempStdDev = sqrt($ctempStdDev);
    }

    $itempMin=200; #in deg C
    for my $val (@itemp) {
	$itempAvg = $itempAvg + $val;
	$itempMax = $val if $val > $itempMax;
	$itempMin = $val if $val < $itempMin;
	$itempSquare = $itempSquare + ($val * $val);
    }
    $itempAvg = $itempAvg/($#itemp+1);
    $itempStdDev = ($itempSquare/($#itemp+1)) - ($itempAvg * $itempAvg);
    if($itempStdDev > 0){
       $itempStdDev = sqrt($itempStdDev);
    }

    printf $stat "count\t%d\t%d\t%d\t%d\t%d\t\t%d\t\t%d\n",($#usr+1), ($#nice+1), ($#sys+1), ($#idle+1), ($#freq+1), ($#ctemp+1), ($#itemp+1);
    printf $stat "avg\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t\t%.2f\n",$usrAvg, $niceAvg, $sysAvg, $idleAvg, $freqAvg, $ctempAvg, $itempAvg;
    printf $stat "min\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t\t%.2f\n",$usrMin, $niceMin, $sysMin, $idleMin, $freqMin, $ctempMin, $itempMin;
    printf $stat "max\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t\t%.2f\n",$usrMax, $niceMax, $sysMax, $idleMax, $freqMax, $ctempMax, $itempMax;
    printf $stat "std-dev\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t\t%.2f\n",$usrStdDev, $niceStdDev, $sysStdDev, $idleStdDev, $freqStdDev, $ctempStdDev, $itempStdDev;
    print $stat "\n\n";

    close($stat);
    close($cpu);
    close($data);
}


sub doWinCPUAnalysis {
    my $cpu;
    my @vals, @pruned;
    my $line;
    my $count=0;
    my @usr, $usrMax=0, $usrMin, $usrAvg=0, $usrStdDev, $usrSquare=0;
    my @sys, $sysMax=0, $sysMin, $sysAvg=0, $sysStdDev, $sysSquare=0;
    my @idle, $idleMax=0, $idleMin, $idleAvg=0, $idleStdDev, $idleSquare=0;
    my @gv, $gvMax=0, $gvMin, $gvAvg=0, $gvStdDev, $gvSquare=0;
   

    open $cpu, "@_[0]" or die "Could not open file: @_[0] for reading";
    open my $stat, '>>', "@_[1]" or die "Could not open file: @_[1] for writing";
    open my $data, '>', "/tmp/wincpu.dat" or die "Could not open file: /tmp/wincpu.dat for writing.";
    print $stat "CPU Statistics for Windows Machine\n";
    print $stat "\tUser\tSys\tIdle\n";
    for $line (<$cpu>) {
	chomp($line);
	$line =~ s/^\s+//;
	$line =~ s/\s+$//;
	$line =~ s/\s+/ /g;
	@vals = split(/ /,$line);
	if ($line =~ /^\d{10}/ && $line !~ /[a-z]/) {
	    $usr[$count] = $vals[13];
	    $sys[$count] = $vals[14];
	    $idle[$count] = $vals[15];
	    #for my $val (@vals) {
		#print "$val";
	    #}
	    print $data "$count @usr[$count] @sys[$count] @idle[$count]\n";
	    $count++;
	}#elsif($line =~ /CPU/){
	  #  my @tempVal = split(//,$vals[2]);
	    #for my $val (@tempVal){
		#print $val;
	    #}
	    #$ctemp[$count] = eval($tempVal[1]*10 + $tempVal[2] + $tempVal[4]/10);
	    #print "$ctemp[$count]\n";
	#}elsif($line =~ /Int/){
	 #   my @tempVal = split(//,$vals[2]);
	 #   @itemp[$count] = eval($tempVal[1]*10 + $tempVal[2] + $tempVal[4]/10);
	 #   $count++;
	#}
    }
    $usrMin = 100;
    for my $val (@usr) {
	$usrAvg = $usrAvg + $val;
	$usrMax = $val if $val > $usrMax;
	$usrMin = $val if $val < $usrMin;
	$usrSquare = $usrSquare + ($val * $val);
	#print "Sum of square=$usrSquare\n";
    }
    $usrAvg = $usrAvg/($#usr+1);
    #print "Sum of square=$usrSquare\n";
    $usrStdDev = sqrt(($usrSquare/($#usr+1)) - ($usrAvg * $usrAvg));
    
    
    $sysMin=100;
    for my $val (@sys) {
	$sysAvg = $sysAvg + $val;
	$sysMax = $val if $val > $sysMax;
	$sysMin = $val if $val < $sysMin;
	$sysSquare = $sysSquare + ($val * $val);
    }
    $sysAvg = $sysAvg/($#sys+1);
    $sysStdDev = ($sysSquare/($#sys+1)) - ($sysAvg * $sysAvg);
    if($sysStdDev > 0){
	$sysStdDev = sqrt($sysStdDev);
    }
    
    $idleMin = 100;
    for my $val (@idle) {
	$idleAvg = $idleAvg + $val;
	$idleMax = $val if $val > $idleMax;
	$idleMin = $val if $val < $idleMin;
	$idleSquare = $idleSquare + ($val * $val);
    }
    $idleAvg = $idleAvg/($#idle+1);
    $idleStdDev = ($idleSquare/($#idle+1)) - ($idleAvg *$idleAvg);
    if($idleStdDev > 0){
	$idleStdDev = sqrt($idleStdDev);
    }

    #$gvMin = 100;
    #for my $val (@gv) {
	#$gvAvg = $gvAvg + $val;
	#$gvMax = $val if $val > $gvMax;
	#$gvMin = $val if $val < $gvMin;
	#$gvSquare = $gvSquare + ($val * $val);
    #}
    #$gvAvg = $gvAvg/($#gv+1);
    #$gvStdDev = ($gvSquare/($#gv+1)) - ($gvAvg *$gvAvg);
    #if($gvStdDev > 0){
	#$gvStdDev = sqrt($gvStdDev);
    #}

    printf $stat "count\t%d\t%d\t%d\n",($#usr+1), ($#sys+1), ($#idle+1);
    printf $stat "avg\t%.2f\t%.2f\t%.2f\n",$usrAvg, $sysAvg, $idleAvg;
    printf $stat "min\t%.2f\t%.2f\t%.2f\n",$usrMin, $sysMin, $idleMin;
    printf $stat "max\t%.2f\t%.2f\t%.2f\n",$usrMax, $sysMax, $idleMax;
    printf $stat "std-dev\t%.2f\t%.2f\t%.2f\n",$usrStdDev, $sysStdDev, $idleStdDev;
    print $stat "\n\n";

    close($stat);
    close($cpu);
    close($data);
    
}



#print "$#ARGV\n";

if ($#ARGV != 0){
    printUsage();
}
else {
    main($ARGV[0]);
}
