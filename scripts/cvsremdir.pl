#!/usr/bin/perl
use strict;

if ($#ARGV == -1) {
    printUsage();
    exit;
}
my $origDir = `pwd`;
chomp($origDir);
my $dirToRemove = $ARGV[0];
$dirToRemove =~ s/\/$//;

if (! -d $dirToRemove) {
    abort("ERROR: $dirToRemove is not a directory\n");
}

if (! -d "$dirToRemove/CVS") {
    abort("ERROR: Cannot find CVS directory in the directory to be removed from cvs. Please check that the directory is indeed part of a cvs repository");
}

print "WARNING: You are about to remove the directory $dirToRemove from cvs.\nAll files and subdirectories will be removed from the cvs as well as local disk.\nAre you sure you want to continue?[y/N]";
my $confirm = <STDIN>;
chomp($confirm);
if ($confirm eq "y" or $confirm eq "Y") {
    my $parentPath = getParentPath($dirToRemove);
    chdir($parentPath);
    my $dirName = getDirName($dirToRemove);
    cvsRemoveDir($dirToRemove);
}

sub getParentPath {
    my $dir = shift;
    my $i = rindex($dir,"/");
    return substr($dir, 0, $i);
}

sub getDirName {
    my $dir = shift;
    my $i = rindex($dir,"/");
    return substr($dir, $i);
}

#Assumption: the current working directory is the parent directory of
#the directory to be removed from CVS when this method is called.
#While it may work in some other cases let's be safe.
sub cvsRemoveDir {
    my $dir = shift;
    my $parentDir = `pwd`;
    chomp($parentDir);
    print "Removing: $parentDir/$dir\n";
    chdir($dir);
    for my $file (`ls`) {
	chomp($file);
	if (-d $file) {
	    my $subDir = $file;
	    $subDir =~ s/\///g;
	    if ($subDir eq "CVS") {
		next;
	    }
	    cvsRemoveDir($subDir);
	} 
    }
    
    #Files must be removed after all subdirectories have been removed
    for my $file (`ls`) {
	chomp($file);
	if(-f $file) {
	    $file =~ s/\$/\\\$/g;
	    $file =~ s/\%/\\\%/g;
	    $file =~ s/\~/\\\~/g;
	    $file =~ s/\_/\\\_/g;
	    $file =~ s/\-/\\\-/g;
	    `rm -f $file`;
	}	    
    }
    
    `cvs remove`;
    `cvs commit -m "Removing $parentDir/$dir"`;
    chdir($parentDir);
    `rm -fr $dir`;
    `cvs update -Pd`;

    print "Remove Done: $parentDir/$dir\n";
    return;
}

sub printUsage {
    print "Usage: $0 dir_name\n";
}

sub abort {
    my $str = shift;
    print "$str";
    chdir($origDir);
    exit;
}

