#!/usr/bin/perl
use Encode;

if($#ARGV < 1) {
    print STDERR "ERROR: Must provide machineid of current machine as parameter\n"; 
    exit -1;
}
$localhost = "127.0.0.1";
$LIB_SERVICE = "LIB_SERVICE";
$myMachineID = $ARGV[0];
$request = $ARGV[1];
$automationConfPath = "../automation.conf";
$myIP;
$asteriskIP;
%machines; #map from machineID to IP address
%serviceMachine; #map from service instance name to machineid
%serviceInstanceSerivce; #map from service instance name to service type
%resources; #map from resource name to machineid
%resourceType; #map from resource name to its type i.e. IN/OUT
%resourceConfig; #map from resource name to actual resource (e.g. hw:0, etc)
%resourceMixer; #map from resource name to mixer (e.g. ALSA, WIN, JACK, etc)
%serviceResources; #map from service instance name + resource name to resource role
@physicalMachineNames; #list of machine names specified in configuration wizard
%ataInfo; #map of sip user name to sip password for atas

my $retVal;

if($request eq "POPULATE_REMOTE_MACHINES") {
    if($#ARGV < 3) {
	print STDERR "ERROR: Insufficient arguments provided to populate remote machines";
	exit -1;
    } else {
	my $confPath = $ARGV[2];
	my $opFile = $ARGV[3];
	populateRemoteMachines($confPath, $opFile);
    }
} elsif($request eq "GET_LOCAL_IP") {
    if($#ARGV < 3) {
	print STDERR "ERROR: Insufficient arguments provided to populate remote machines";
	exit -1;
    } else {
	my $confPath = $ARGV[2];
	my $opFile = $ARGV[3];
	getLocalIP($confPath, $opFile);
    }
} else {
    print STDERR "ERROR: Invalid request received: $request\n";
}

sub getLocalIP {
    my $confPath = shift;
    my $opFile = shift;
    open $op, '>', "$opFile" or (print STDERR "ERROR: Could not open $opFile for writing\n" and return);

    processConfig($confPath);
    my $myIP = $machines{$myMachineID};
    
    print $op $myIP;

    close $op;
    return;
}

sub populateRemoteMachines {
    my $confPath = shift;
    my $opFile = shift;
    open $rm, '>', "$opFile" or (print STDERR "ERROR: Could not open $opFile for writing\n" and return);

    processConfig($confPath);
    %remoteIPs;
    my $myIP = $machines{$myMachineID};
    for my $machine (keys %machines) {
	my $ip = $machines{$machine};
	if($myIP ne $ip) {
	    $remoteIPs{$ip} = $ip;
	}
    }
    
    for my $ip (keys %remoteIPs) {
	print $rm $ip."\n";
    }
    close $ip;

    return;
}

sub updateContext {
    my $line = shift;
    my $context = shift;

    if($line =~ /\[machines\]/){
	$context = "machines";
    } elsif($line =~ /\[services\]/) {
	$context = "services";
    } elsif($line =~ /\[resources\]/) {
	$context = "resources";
    } elsif($line =~ /\[resourceconfig\]/) {
	$context = "resourceconfig";
    } elsif($line =~ /\[serviceresources\]/) {
	$context = "serviceresources";
    } elsif($line =~ /\[machinenames\]/) {
	$context = "machinenames";
    } elsif($line =~ /\[telephonyata\]/) {
	$context = "telephonyata";
    } elsif($line =~ /\[.*\]/) {
	$context = "";
    }
    
    return $context;
}


sub processConfig {
    my $confPath = shift;
    open $conf, '<', $confPath or (print STDERR "failed\n" and exit);
    my $context = "";
    
    for my $line (<$conf>){
	if($line =~ /^#/){
	    next;
	}
	$line =~ s/\s//g;
	$line =~ s/\n//g;
	$line =~ s/\r//g;

	if($line eq "") {
	    next;
	}

	$context = updateContext($line, $context);

	if($line =~ /\[.*\]/) {
	    next;
	}

	if($context eq "machines") {
	    my @machineInfo = split(",", $line);
	    $machines{$machineInfo[0]} = $machineInfo[1];
	} elsif($context eq "services") {
	    my @serviceInfo = split(",", $line);
	    #if($serviceInfo[0] eq "AUDIO_SERVICE") {
		#if($line =~ "PLAYOUT") {
		#    $serviceMachine{"AUDIO_SERVICE_PLAYOUT"} = $serviceInfo[2];
		#} elsif($line =~ "PREVIEW") {
		#    $serviceMachine{"AUDIO_SERVICE_PREVIEW"} = $serviceInfo[2];
		#}
	    #} else {
		$serviceMachine{$serviceInfo[3]} = $serviceInfo[2];
	    #}
	    $serviceInstanceService{$serviceInfo[3]} = $serviceInfo[0];
	} elsif($context eq "resources") {
	    my @resourceInfo = split(",", $line);
	    $resources{$resourceInfo[1]} = $resourceInfo[2];
	    $resourceType{$resourceInfo[1]} = $resourceInfo[0];
	} elsif($context eq "resourceconfig") {
	    my @resourceInfo = split(",", $line);
	    $resourceConfig{$resourceInfo[0]} = $resourceInfo[2];
	    $resourceMixer{$resourceInfo[0]} = $resourceInfo[1];
	} elsif($context eq "serviceresources") {
	    my @resourceInfo = split(",", $line);
	    $serviceResources{$resourceInfo[0].",".$resourceInfo[1]} = $resourceInfo[2];
	} elsif($context eq "machinenames") {
	    $physicalMachineNames[$#physicalMachineNames + 1] = $line;
	} elsif($line =~ /TELEPHONY_SERVER/) {
	    my @asteriskInfo = split(",", $line);
	    $asteriskIP = $asteriskInfo[1];
	} elsif($context eq "telephonyata") {
	    my @ata = split(",",$line);
	    $ataInfo{$ata[1]} = $ata[2];
	}
    }
    close $conf;
}

