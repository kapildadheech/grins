#!/usr/bin/python
import sys, os, thread, time
import pygtk, gtk, gobject
import pygst
pygst.require("0.10")
import gst


class Pipeline:
    def __init__(self, filename, interval):
        self.pipe = gst.Pipeline ("DurationPipe")
        
        self.file_src = gst.element_factory_make ("filesrc", "filesrc")
        self.file_src.set_property ("location", filename)

        self.decode_bin = gst.element_factory_make ("decodebin", "decodebin")
        self.decode_bin.connect ("new-decoded-pad", self.on_dynamic_pad)

        self.spectrum = gst.element_factory_make("spectrum", "spectrum")
        self.spectrum.set_property("bands", 22050)
        self.spectrum.set_property("interval",  int(interval * 1000000000) )

        self.fake_sink = gst.element_factory_make ("fakesink", "fakesink")

        self.pipe.add (self.file_src, self.decode_bin, self.spectrum, self.fake_sink)
        self.file_src.link (self.decode_bin)
        self.spectrum.link(self.fake_sink)

        bus = self.pipe.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message)

        self.pipe.set_state(gst.STATE_NULL)
        self.freqs = []
        self.magns = []

    def on_dynamic_pad(self, bin, pad, last):
        self.decode_bin.link(self.spectrum)

        
    def on_message(self, bus, message):
        type = message.type
        
        if type == gst.MESSAGE_EOS:
            print self.freqs
            print self.magns
            self.pipe.set_state(gst.STATE_NULL)
            
            gtk.main_quit()

        elif type == gst.MESSAGE_ERROR:
            pass

        elif message.structure:
            message.structure.foreach(self.on_field)
                    
    def play(self):
        self.pipe.set_state(gst.STATE_PLAYING)

    def on_field(self, key, value):
        if key == "magnitude":
            index = 0
            maxindex = -1
            maxvalue = -100
            for i in value:
                if i > maxvalue:
                    maxvalue = i
                    maxindex = index
                index += 1
            self.freqs.append(maxindex)
            self.magns.append(maxvalue)
        return True

pipeline = Pipeline(sys.argv[1], float(sys.argv[2]))
pipeline.play()
        
gtk.main()
