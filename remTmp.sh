#!/bin/bash

find ./ -name "*~" | xargs -i rm "{}"
find ./ -name "#*" | xargs -i rm "{}"
find ./ -name ".#*" | xargs -i rm "{}"
