#!/usr/bin/bash

tomcathome="$1";
tomcatconf="$2";
gramvaanihome="$3";

if [ `uname | grep CYGWIN | wc -l` -eq 1 ]
then
    mkdir -p "$gramvaanihome"/WEB-INF
    cp -r "$gramvaanihome"/classes "$gramvaanihome"/WEB-INF/.
    cp -r "$gramvaanihome"/lib "$gramvaanihome"/WEB-INF/.
    cp "$gramvaanihome"/web.xml "$gramvaanihome"/WEB-INF/.
else
    ln -s "$gramvaanihome" "$gramvaanihome"/WEB-INF
fi

cp "$gramvaanihome"/gramvaani.xml "$tomcatconf"/Catalina/localhost/.
chmod 777 "$tomcatconf"/Catalina/localhost/gramvaani.xml;



