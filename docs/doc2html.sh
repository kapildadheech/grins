#!/bin/bash

XSLPATH="";
XSLCOMMAND="xsltproc"
XSLPARAMS="--stringparam section.autolabel 1 --stringparam section.label.includes.component.label 1"
if [ "$2" == "" ]
then
    echo "Usage: $0 DOCBOOK_FILE OUTPUT_FILE [GEN_TOC]"
else
    INFILE=$1;
    OUTFILE=$2
    if [ "$3" == "0" ]
    then
	XSLPARAMS=${XSLPARAMS}" --stringparam generate.toc 0";
    fi

    if [ "`uname -a | grep Linux`" == "" ]
    then
	XSLPATH="/usr/share/docbook-xsl/html/docbook.xsl"
    else
	XSLPATH="/usr/share/xml/docbook/stylesheet/nwalsh/xhtml/docbook.xsl"
    fi

    #echo "${XSLCOMMAND} ${XSLPARAMS} ${XSLPATH} ${INFILE} > ${OUTFILE}"
    ${XSLCOMMAND} ${XSLPARAMS} ${XSLPATH} ${INFILE} > ${OUTFILE}
fi