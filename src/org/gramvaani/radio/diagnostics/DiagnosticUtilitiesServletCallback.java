package org.gramvaani.radio.diagnostics;
import org.gramvaani.simpleipc.*;

public interface DiagnosticUtilitiesServletCallback {
    public void attemptUpload();
}