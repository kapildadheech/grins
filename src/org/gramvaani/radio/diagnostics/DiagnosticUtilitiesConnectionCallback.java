package org.gramvaani.radio.diagnostics;
import org.gramvaani.simpleipc.*;

public interface DiagnosticUtilitiesConnectionCallback {
    public IPCServer attemptReconnect();
    public void serviceDown(String service);
}