package org.gramvaani.radio.diagnostics;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.simpleipc.*;

import java.util.*;
import java.io.*;
import java.util.concurrent.*;

public class DiagnosticUtilities {
    public final static long PING_TIMEOUT = 3000;         // 3 seconds
    public static final long DB_RECONNECTION_INTERVAL = 10000; //10 seconds

    public static final String CHK_NW_MSG = "CHECK_NETWORK";
    public static final String START_DB_MSG = "START_DATABASE";
    public static final String START_SERVLET_MSG = "START_SERVLET";
    public static final String START_ASTERISK_MSG = "START_ASTERISK";
    public static final String RESTART_SYSTEM_MSG = "RESTART_SYSTEM";
    public static final String SHUTDOWN_SYSTEM_MSG = "SHUTDOWN_SYSTEM";
    public static final String DELIMITER = "!";

    public static final String DISK_SPACE_MSG = "DISK_SPACE_MSG";

    //message type
    public static final int SHUTDOWN_SYSTEM_TYPE = 1;
    public static final int RESTART_SYSTEM_TYPE = 2;
    public static final int SERVICE_CONNECTION_ERROR_TYPE = 3;
    public static final int DB_ERROR_TYPE = 4;
    public static final int RSC_CONNECTION_ERROR_TYPE = 5;
    public static final int SERVLET_ERROR_TYPE = 6;
    public static final int INITIATE_PING_TYPE = 7;
    public static final int ASTERISK_ERROR_TYPE = 8;

    public static DiagnosticUtilities diagUtil;
    protected StationConfiguration stationConfiguration;
    protected LogUtilities logger;
    protected String highTempFileName;
    protected UUID myID;
    protected File highTempFile;
    protected String state;
    protected Thread tempWatcher;
    protected Thread communicationThread;
    protected LinkedBlockingQueue<String> messageQueue;
    protected IPCServer ipcServer;

    protected boolean pingingIP = false;
    //protected boolean checkingRSC=false;
    protected boolean checkingDB = false;
    protected boolean checkingTelephony = false;
    protected boolean dbNetworkDown = false;
    protected boolean restartingSystem = false;
    protected boolean shutingDownSystem = false;
    protected int dbNetworkAttempts = 0;
    protected static long messageID = 0;
    protected static Object messageIDSyncObject = new Object();

    protected Hashtable<String, Long> checkingServices = new Hashtable<String, Long>();
    protected HashSet<DiagnosticUtilitiesConnectionCallback> connectionListeners;
    protected Hashtable<Long, ScriptCommandMessage> messageList = new Hashtable<Long, ScriptCommandMessage>();
      
    public DiagnosticUtilities(StationConfiguration stationCfg, IPCServer ipcServer){
	stationConfiguration = stationCfg;
	this.ipcServer = ipcServer;
	logger = new LogUtilities("DiagnosticsUtilities");
	logger.debug("DiagnosticsUtilities: Initializing");
	messageQueue = new  LinkedBlockingQueue<String>();
	connectionListeners = new HashSet<DiagnosticUtilitiesConnectionCallback>();
	communicationThread = new Thread("DiagnosticUtilities:CommunicationThread") {
		public void run(){
		    processMessages(DiagnosticUtilities.getDiagnosticUtilities());
		}
	    };
	communicationThread.start();
	startTemperatureWatch();
    }

    public void processMessages(DiagnosticUtilities diag){
	String message;
	String response;
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	while (true){
	    try {
		message = messageQueue.take();
		logger.debug("ProcessMessages: message="+message);
		System.out.println(message);
		response = br.readLine();
		logger.debug("ProcessMessages: response="+response);
		if (response != null){
		    ResponseProcessor p = new ResponseProcessor(message, response, diag);
		    p.start();
		}
	    } catch (Exception e) {
		logger.error("Error in process messages thread", e);
	    }
	}
    }

    public void startTemperatureWatch(){
	highTempFileName = stationConfiguration.getStringParam(StationConfiguration.HIGH_TEMPERATURE_FILE_NAME);
	if (highTempFileName == null){
	    logger.warn("DiagnosticsUtilities: high temperature file name not specified in configuration file. Failures due to high temperature will not be detected.");
	}else{
	    tempWatcher = new Thread("DiagnosticUtilities:TempWatcher") {
		    public void run(){
			watchTemperature();
		    }
		};
	    tempWatcher.start();
	}
    }

    /*    
    public void registerListener(DiagnosticUtilitiesConnectionCallback callback){
	connectionListeners.add(callback);
    }

    public void registerDBListener(DiagnosticUtilitiesDBCallback callback){
	dbListeners.add(callback);
    }

    public void registerServletListener(DiagnosticUtilitiesServletCallback callback){
	servletListener = callback;
	}

    public void registerPingListener(DiagnosticUtilitiesPingCallback callback) {
	pingListener = callback;
    }*/

    //Only UI makes this call. Only one listener required
    public synchronized void shutdownMachines(DiagnosticUtilitiesSystemCallback listener) {
	if (!shutingDownSystem) {
	    shutingDownSystem = true;
	} else {
	    logger.debug("ShutdownSystem: Already trying to shutdown system.");
	    return;
	}
	
	//Send message to shutdown all machines except local machine
	//On receiving this message the script restarts local services also
	String[] machines = stationConfiguration.getNonLocalMachineIPs();
	ScriptCommandMessage shutdownMessage = new ScriptCommandMessage(SHUTDOWN_SYSTEM_MSG, machines, SHUTDOWN_SYSTEM_TYPE);
	shutdownMessage.addSystemCallback(listener);
	messageList.put(shutdownMessage.getMessageID(), shutdownMessage);
	sendMessage(shutdownMessage);
    }
    
    //Only UI makes this call. Only one listener required
    public synchronized void restartMachines(DiagnosticUtilitiesSystemCallback listener) {
	if (!restartingSystem) {
	    restartingSystem = true;
	} else {
	    logger.debug("RestartSystem: Already trying to restart system.");
	    return;
	}
	
	//Send message to restart all  machines except local machine
	//On receiving this message the script restarts local services also
	String[] machines = stationConfiguration.getNonLocalMachineIPs();
	ScriptCommandMessage restartMessage = new ScriptCommandMessage(RESTART_SYSTEM_MSG, machines, RESTART_SYSTEM_TYPE);
	restartMessage.addSystemCallback(listener);
	messageList.put(restartMessage.getMessageID(), restartMessage);
	sendMessage(restartMessage);
    }

   
    public synchronized void initiatePing(String ipAddress, DiagnosticUtilitiesPingCallback callback) {
	if (!pingingIP) {
	    pingingIP = true;
	} else {
	    logger.debug("InitiatePing: Ping already initiated. Removing previous ping message.");
	    removePreviousPingMessage();
	}
	
	ScriptCommandMessage pingIPMessage = new ScriptCommandMessage(CHK_NW_MSG, new String[] {ipAddress}, INITIATE_PING_TYPE);
	pingIPMessage.addPingCallback(callback);
	messageList.put(pingIPMessage.getMessageID(), pingIPMessage);
	sendMessage(pingIPMessage);
    }

    protected synchronized void removePreviousPingMessage() {
	if (messageList.size() == 0)
	    return;
	
	for (Long id: messageList.keySet()){
	    ScriptCommandMessage scriptMessage = messageList.get(id);
	    int messageType = scriptMessage.getMessageType();
	    if (messageType == INITIATE_PING_TYPE) {
		logger.warn("RemovePreviousPingMessage: Removing message: " + messageList.get(id).getMessageString());
		messageList.remove(id);
		break;
	    }
	}
    }

    protected ScriptCommandMessage checkingServlet(String servletName) {
	if (messageList.size() == 0)
	    return null;

	for (Long id: messageList.keySet()){
	    ScriptCommandMessage message = messageList.get(id);
	    if (message.getServletName().equals(servletName)) 
		return message;
	}
	return null;
    }

    public synchronized void servletError(String fileStore, DiagnosticUtilitiesServletCallback listener){
	String servletip = ((StationNetwork)stationConfiguration.getStationNetwork()).getStoreMachine(fileStore);
	String myip = stationConfiguration.getMachineIP(stationConfiguration.getLocalMachineID());
	boolean messageFound = false;

	ScriptCommandMessage message = checkingServlet(fileStore);
	if (message != null) {
	    message.addServletCallback(listener);
	    logger.info("ServletError: Already checking for servlet:" + fileStore);
	    return;
	} 
      

	if (servletip == null){
	    logger.error("ServletError: Could not find the IP address of upload server machine for file store:" + fileStore);
	    return;
	}

	ScriptCommandMessage servletCheckMessage;
	if (servletip.equals(myip)){
	    servletCheckMessage = new ScriptCommandMessage(DiagnosticUtilities.START_SERVLET_MSG, new String[]{}, SERVLET_ERROR_TYPE, fileStore);
	}else{
	    servletCheckMessage = new ScriptCommandMessage(DiagnosticUtilities.CHK_NW_MSG, new String[]{servletip}, SERVLET_ERROR_TYPE, fileStore);
	}
	servletCheckMessage.addServletCallback(listener);
	
	long msgID = servletCheckMessage.getMessageID();
	messageList.put(msgID, servletCheckMessage);
	sendMessage(servletCheckMessage);
    }

    //Note: DB and lib service are assumed to be on the same machine
    //Note: Only lib service calls this method
    public synchronized void dbError(DiagnosticUtilitiesDBCallback listener){
	if (!checkingDB) {
	    checkingDB=true;
	} else {
	    logger.debug("DBError: Already checking for DB.");
	    return;
	}

	ScriptCommandMessage dbCheckMessage = new ScriptCommandMessage(START_DB_MSG, new String[] {}, DB_ERROR_TYPE);
	dbCheckMessage.addDBCallback(listener);
	messageList.put(dbCheckMessage.getMessageID(), dbCheckMessage);
	sendMessage(dbCheckMessage);
    }

    protected ScriptCommandMessage checkingAsterisk() {
	if (messageList.size() == 0)
	    return null;

	for (Long id: messageList.keySet()){
	    ScriptCommandMessage message = messageList.get(id);
	    if (message.getMessageType() == ASTERISK_ERROR_TYPE) 
		return message;
	}
	return null;
    }

    public synchronized void telephonyError(DiagnosticUtilitiesAsteriskCallback listener) {
	String asteriskIP = stationConfiguration.getStringParam(StationConfiguration.TELEPHONY_SERVER);
	String myIP = stationConfiguration.getMachineIP(stationConfiguration.getLocalMachineID());
	if (asteriskIP == null) {
	    logger.error("TelephonyError: Could not find IP for asterisk server in configuration file.");
	    return;
	}

	ScriptCommandMessage message = checkingAsterisk();
	if (message != null) {
	    message.addAsteriskCallback(listener);
	    logger.info("TelephonyError: Already checking for asterisk at " + asteriskIP);
	    return;
	} 
	
	if (asteriskIP.equals(myIP)){
	    message = new ScriptCommandMessage(START_ASTERISK_MSG, new String[] {asteriskIP}, ASTERISK_ERROR_TYPE);
	}else{
	    message = new ScriptCommandMessage(DiagnosticUtilities.CHK_NW_MSG, new String[]{asteriskIP}, ASTERISK_ERROR_TYPE);
	}

	message.addAsteriskCallback(listener);
	messageList.put(message.getMessageID(), message);
	sendMessage(message);
    }

    //Assumption: If one service detects a connection error. All services in the same VM also
    //detect the same error. As a result all the DiagnosticUtilitiesConnectionCallbacks are made
    //to reconnect.
    public synchronized void serviceConnectionError(DiagnosticUtilitiesConnectionCallback connectionListener){
	if(connectionListeners.contains(connectionListener)) {
	    (new Exception()).printStackTrace();
	} else {
	    String rscIP = stationConfiguration.getRSControllerIP();
	    ScriptCommandMessage rscCheckMessage = new ScriptCommandMessage(CHK_NW_MSG, new String[] {rscIP}, SERVICE_CONNECTION_ERROR_TYPE);
	    rscCheckMessage.addConnectionCallback(connectionListener);
	    connectionListeners.add(connectionListener);
	    messageList.put(rscCheckMessage.getMessageID(), rscCheckMessage);
	    sendMessage(rscCheckMessage);
	}
    }

   
    public synchronized void rscConnectionError(String name, DiagnosticUtilitiesConnectionCallback listener) {
	String serviceIP = stationConfiguration.getServiceIP(name);

	if (serviceIP != null) {
	    if (checkingServices.get(name) != null) {
		logger.info("RSCConnectionError: Already checking for service: " + name);
		return;
	    } else {
		ScriptCommandMessage nwCheckMessage = new ScriptCommandMessage(CHK_NW_MSG, new String[] {serviceIP}, RSC_CONNECTION_ERROR_TYPE);
		nwCheckMessage.addConnectionCallback(listener);
		checkingServices.put(name, nwCheckMessage.getMessageID());
		messageList.put(nwCheckMessage.getMessageID(), nwCheckMessage);
		sendMessage(nwCheckMessage);
	    }
	}else{
	    logger.error("RSCConnectionError: Could not determine the IP address of "+name+" from configuration file. Declaring node down.");
	    listener.serviceDown(name);
	}
    }

    public void handlePingIPNetworkResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage) {
	String[] responses = responseMessage.getResponses();
	String[] ipAddresses = sentMessage.getIPAddresses();

	synchronized(this) {
	    DiagnosticUtilitiesPingCallback[] pingListeners = sentMessage.getAllPingCallbacks();
	    if (pingListeners.length == 0) {
		logger.error("HandlePingIPNetworkResponse: No ping listeners found.");
		return;
	    }
	
	    Long msgID = responseMessage.getMessageID();
	    
	    if (responses[0].equalsIgnoreCase("true")) {
		pingListeners[0].pingResponse(ipAddresses[0], true);
	    } else if (responses[0].equalsIgnoreCase("false")) {
		pingListeners[0].pingResponse(ipAddresses[0], false);
	    } else {
		logger.error("HandlePingIPNetworkResponse: Unknown response message for check network request.");
		pingListeners[0].pingResponse(ipAddresses[0], false);
	    }
	    pingingIP = false;
	}
    }

    //Assumption: If one service detects a connection error. All services in the same VM also
    //detect the same error. As a result all the DiagnosticUtilitiesConnectionCallbacks are made
    //to reconnect.
    protected void handleConnectionErrorNetworkResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage){
	boolean tryWaiting = false;
	String[] responses = responseMessage.getResponses();
      
	if (responses[0].equalsIgnoreCase("true")){
	    //Network is up => RSC is down.
	    logger.error("HandleConnectionErrorNetworkResponse: RSController down.");
	    tryWaiting = true;
	    
	    //Making services and app wait for timeout whether RSC down detected or app down detected.
	    //The registration ack message can tell services whether RSC went down and they can take
	    //action accordingly at that point of time.
	    
	}else if (responses[0].equalsIgnoreCase("false")){
		//Network down
	    logger.warn("HandleConnectionErrorNetworkResponse: Network down detected.");
	    tryWaiting = true;
	}else {
	    logger.error("HandleConnectionErrorNetworkResponse: Unknown response message for check network request.");
	}
	
	if (tryWaiting) {
	    int waitTime = stationConfiguration.getIntParam(StationConfiguration.NETWORK_DOWN_WAIT_TIME);
	    if (waitTime == -1){
		logger.error("HandleConnectionErrorNetworkResponse: Could not find network down wait time parameter in configuration file. Giving up immediately..");
		giveUpReconnection(sentMessage);
	    } else {
		attemptReconnect(waitTime, sentMessage);
	    }
	}
	
    }

   
    //This method should not be called now given that lib is always on the same machine as database
    /*protected void handleDBErrorNetworkResponse(String[] msgArr){
	long interval = stationConfiguration.getIntParam(StationConfiguration.NETWORK_DOWN_RECONNECTION_INTERVAL);
	if (interval == -1)
	    interval = 10;
	if (dbCheckMessage.contains(msgArr[1])){
	    if (msgArr[2].equalsIgnoreCase("true")){
		if (!dbNetworkDown){
		    logger.info("HandleCheckNetworkResponse: Network is up. DB may be down. Trying to bring up the DB");
		    String dbip = msgArr[1];
		    dbCheckMessage = DiagnosticUtilities.START_DB+DiagnosticUtilities.DELIMITER+dbip;
		    sendMessage(dbCheckMessage);
		}else{
		    logger.info("HandleCheckNetworkResponse: Network is up again.");
		    dbNetworkDown = false;
		    attemptDBConnect();
		    return;
		}
	    }else if (msgArr[2].equalsIgnoreCase("false")){
		//Network down
		if (dbNetworkDown){
		    logger.warn("HandleCheckNetworkResponse: Network still down.");
		}else{
		    logger.warn("HandleCheckNetworkResponse: Network down detected.");
		    dbNetworkDown=true;
		}
		dbNetworkAttempts++;
		if (dbNetworkAttempts == 3){
		    logger.error("HandleCheckNetworkResponse: Network did not come up in " + dbNetworkAttempts + ". Giving up.");
		    //XXX Flash error somewhere visible
		    return;
		}
		try{
		    Thread.sleep(interval*1000);
		}catch(Exception e){}
		sendMessage(dbCheckMessage);
	    }else {
		logger.error("HandleCheckNetworkResponse: Unknown response message for check network request.");
	    }
	}
	}*/

    protected synchronized void handleServletErrorNetworkResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage){
	long interval = stationConfiguration.getIntParam(StationConfiguration.NETWORK_DOWN_RECONNECTION_INTERVAL);
	if (interval == -1)
	    interval = 10;
	
	if (sentMessage != null) {
	    String[] addresses = sentMessage.getIPAddresses();
	    String[] responses = responseMessage.getResponses();

	    if (responses[0].equalsIgnoreCase("true")){
		if (!sentMessage.networkDown()){
		    logger.info("HandleCheckNetworkResponse: Network is up. Apache/Tomcat may be down. Trying to bring them up.");
		    String webip = addresses[0];
		    ScriptCommandMessage servletCheckMessage;
		    String servletName;
		    synchronized(this) {
			messageList.remove(sentMessage.getMessageID());
			servletName = sentMessage.getServletName();
			servletCheckMessage = new ScriptCommandMessage(START_SERVLET_MSG, new String[] {webip}, SERVLET_ERROR_TYPE, servletName);
			DiagnosticUtilitiesServletCallback[] callbacks = sentMessage.getAllServletCallbacks();
			for (DiagnosticUtilitiesServletCallback callback : callbacks) {
			    servletCheckMessage.addServletCallback(callback);
			}
			messageList.put(servletCheckMessage.getMessageID(), servletCheckMessage);
		    }
		    sendMessage(servletCheckMessage);
		}else{
		    logger.info("HandleServletErrorNetworkResponse: Network is up again.");
		    sentMessage.setNetworkDown(false);
		    attemptUpload(sentMessage);
		}
	    }else if (responses[0].equalsIgnoreCase("false")){
		if (sentMessage.networkDown()){
		    logger.warn("HandleServletErrorNetworkResponse: Network still down.");
		}else{
		    logger.warn("HandleServletNetworkResponse: Network down detected.");
		    sentMessage.setNetworkDown(true);
		}
		String servletName = sentMessage.getServletName();
		int servletAttempts = sentMessage.incrementServletAttempts();
		
		if (servletAttempts == 3){
		    logger.error("HandleServletNetworkResponse: Network did not come up in " + servletAttempts + ". Giving up.");
		    synchronized(this) {
			messageList.remove(sentMessage.getMessageID());
		    }
		    //removeServlet(sentMessage.getMessageID());
		    //XXX Flash error somewhere visible
		    return;
		}

		try{
		    Thread.sleep(interval * 1000);
		}catch(Exception e){}

		sendMessage(sentMessage);
	    }else {
		logger.error("HandleCheckNetworkResponse: Unknown response message received for check network request.");
	    }
	}
    }

    protected synchronized void handleAsteriskErrorNetworkResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage){
	long interval = stationConfiguration.getIntParam(StationConfiguration.NETWORK_DOWN_RECONNECTION_INTERVAL);
	if (interval == -1)
	    interval = 10;
	
	String[] addresses = sentMessage.getIPAddresses();
	String[] responses = responseMessage.getResponses();
	
	if (responses[0].equalsIgnoreCase("true")){
	    if (!sentMessage.networkDown()){
		logger.info("HandleAsteriskErrorNetworkResponse: Network is up. Asterisk may be down. Trying to bring it up.");
		String asteriskIP = addresses[0];
		ScriptCommandMessage message;
		synchronized(this) {
		    messageList.remove(sentMessage.getMessageID());
		    message = new ScriptCommandMessage(START_ASTERISK_MSG, new String[] {asteriskIP}, ASTERISK_ERROR_TYPE);
		    DiagnosticUtilitiesAsteriskCallback[] callbacks = sentMessage.getAllAsteriskCallbacks();
		    for (DiagnosticUtilitiesAsteriskCallback callback : callbacks) {
			message.addAsteriskCallback(callback);
		    }
		    messageList.put(message.getMessageID(), message);
		}
		sendMessage(message);
	    }else{
		logger.info("HandleAsteriskErrorNetworkResponse: Network is up again.");
		sentMessage.setNetworkDown(false);
		attemptTelephonyUp(sentMessage);
	    }
	}else if (responses[0].equalsIgnoreCase("false")){
	    if (sentMessage.networkDown()){
		logger.warn("HandleAsteriskErrorNetworkResponse: Network still down.");
	    }else{
		logger.warn("HandleAsteriskNetworkResponse: Network down detected.");
		sentMessage.setNetworkDown(true);
	    }
	    	    
	    try{
		Thread.sleep(interval * 1000);
	    }catch(Exception e){}
	    
	    sendMessage(sentMessage);
	} else {
	    logger.error("HandleCheckNetworkResponse: Unknown response message received for check network request.");
	}
    }


    protected void handleServiceErrorNetworkResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage){
	String[] responses = responseMessage.getResponses();
	String[] ipAddresses = sentMessage.getIPAddresses();
	String curService = "";
	
	synchronized(this) {
	    messageList.remove(sentMessage.getMessageID());
	}

	int checkingServicesSize = checkingServices.size();
	
	if (checkingServicesSize != 0){
	    synchronized(this) {
		for (String service: checkingServices.keySet()){
		    long id = checkingServices.get(service);
		    if (id == sentMessage.getMessageID()) {
			curService = service;
		    }
		}
	    }
	    if (!curService.equals("")){
		if (responses[0].equalsIgnoreCase("true")) {
		    //Network is up => service is down.
			logger.error("HandleServiceErrorNetworkResponse: Service down: " + curService);

		} else if (responses[0].equalsIgnoreCase("false")) {
		    //Network down
		    logger.warn("HandleServiceErrorNetworkResponse: Network down detected:" + ipAddresses[0]);

		} else {
		    logger.warn("HandleServiceErrorNetworkResponse: Unknown response message for check network request. IPAddesses:" + ipAddresses + " Responses:" + responses);
		    return;
		}
		
		int waitTime = stationConfiguration.getIntParam(StationConfiguration.NETWORK_DOWN_WAIT_TIME);
		waitForService(waitTime, ipAddresses[0], sentMessage, curService);
	    }
	} else {
	    logger.warn("HandleServiceErrorNetworkResponse: Unknown message type received from the script. We are not checking for any services.");
	}
    }

    protected void waitForService(int waitTime, String address, ScriptCommandMessage sentMessage, String service) {
	//There would be just one listener
	
	DiagnosticUtilitiesConnectionCallback listener = sentMessage.getAllConnectionCallbacks()[0];
	
	if (waitTime < 0){
	    logger.error("WaitForService: Could not find network down wait time in configuration file.");
	    logger.error("WaitForService: Removing service: " + service);
	    listener.serviceDown(service);

	    synchronized(this) {
		removeService(service);
	    }
	} else if (waitTime == 0) {
	    logger.info("WaitForService: Waiting infinitely.");
	    int checkInterval = stationConfiguration.getIntParam(StationConfiguration.NETWORK_DOWN_RECONNECTION_INTERVAL);
	    if (checkInterval == -1)
		checkInterval = 10;
	    while (true) {
		try{
		    Thread.sleep(checkInterval * 1000);
		}catch(Exception e){}
		synchronized(this){
		    if (checkingServices.get(service) == null) {
			return;
		    }
		}
	    }

	} else {	    
	    try{
		Thread.sleep(waitTime*1000);
	    }catch(Exception e){}
	    
	    synchronized(this){
		if (checkingServices.get(service) != null){
		    //The entry has not been removed by a new connection.
		    //declare node down and then remove the entry
		    logger.error("WaitForService: Service " + service + " not back up after " + waitTime + " seconds.");
		    listener.serviceDown(service);
		    removeService(service);
		}
	    }
	}
    }

    protected void handleCheckNetworkResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage){
	int messageType = sentMessage.getMessageType();
	
	switch(messageType) {
	case SERVICE_CONNECTION_ERROR_TYPE:
	    handleConnectionErrorNetworkResponse(responseMessage, sentMessage);
	    break;
	case INITIATE_PING_TYPE:
	    handlePingIPNetworkResponse(responseMessage, sentMessage);
	    break;
	case SERVLET_ERROR_TYPE:
	    handleServletErrorNetworkResponse(responseMessage, sentMessage);
	    break;
	case RSC_CONNECTION_ERROR_TYPE:
	    handleServiceErrorNetworkResponse(responseMessage, sentMessage);
	    break;
	case ASTERISK_ERROR_TYPE:
	    handleAsteriskErrorNetworkResponse(responseMessage, sentMessage);
	    break;
	default:
	    logger.error("HandleCheckNetworkResponse: Invalid message type received: " + messageType);
	}
	    
    }

    public void removeService(String service){
	checkingServices.remove(service);
    }

    protected void attemptTelephonyUp(ScriptCommandMessage sentMessage){
	synchronized(this) {
	    messageList.remove(sentMessage.getMessageID());
	    DiagnosticUtilitiesAsteriskCallback[] listeners = sentMessage.getAllAsteriskCallbacks();
	    for (DiagnosticUtilitiesAsteriskCallback listener : listeners) {
		listener.attemptTelephonyUp();
	    }
	}
    }

    protected void attemptDBConnect(ScriptCommandMessage sentMessage){
	//dbNetworkAttempts = 0;
	//There should be only one listener only lib service calls db error.
	synchronized(this) {
	    messageList.remove(sentMessage.getMessageID());
	}

	DiagnosticUtilitiesDBCallback[] dbListeners = sentMessage.getAllDBCallbacks();
	DiagnosticUtilitiesDBCallback listener = dbListeners[0];
	while (true) {
	    if (listener.attemptDBConnect() == true){
		for (int i=1; i<dbListeners.length; i++){
		    listener = dbListeners[i];
		    listener.attemptDBConnect();
		}
		synchronized(this) {
		    checkingDB = false;
		}
		break;
	    } else {
		logger.error("AttemptDBConnect: Failed to reconnect to the database.");
		try {
		    Thread.sleep(DB_RECONNECTION_INTERVAL);
		} catch (InterruptedException ex) {}
	    }
	}

	return;
    }

    protected void attemptReconnect(int waitTime, ScriptCommandMessage sentMessage){
	IPCServer ipcServer = null;
	boolean reconnectionDone = false;
	long curTime = System.currentTimeMillis();
	long timeout = curTime + waitTime*1000;
	long interval = stationConfiguration.getIntParam(StationConfiguration.NETWORK_DOWN_RECONNECTION_INTERVAL);
	
	if (interval == -1)
	    interval = 10;

	DiagnosticUtilitiesConnectionCallback[] listeners = sentMessage.getAllConnectionCallbacks();

	synchronized(this) {
	    messageList.remove(sentMessage.getMessageID());
	    for(int i = 0; i < listeners.length; i++) {
		connectionListeners.remove(listeners[i]);
	    }
	}

	//either wait for timeout or attempt continuously if waitTime = 0
	//if reconnection is done then go and reconnect other listeners
	while (((timeout >= curTime) || waitTime == 0) 
	      && !reconnectionDone){
	    ipcServer = listeners[0].attemptReconnect();
	    if (ipcServer != null){
		//reconnect successful. send callbacks with new ipcServer to all services.
		reconnectionDone = true;
		break;
	    }else{
		try{
		    Thread.sleep(interval * 1000);
		}catch(Exception e){}
		curTime = System.currentTimeMillis();
	    }
	}
	if (!reconnectionDone){
	    logger.error("AttemptReconnect: Could not reconnect within "+waitTime+" seconds. Giving up...");
	    giveUpReconnection(sentMessage);
	} else {
	    for (int i = 1; i < listeners.length; i++){
		ipcServer = listeners[i].attemptReconnect();
	    }
	    DiagnosticUtilities d = DiagnosticUtilities.getDiagnosticUtilities();
	    if (d != null)
		d.setIPCServer(ipcServer);
	}
	
    }
    
    protected void giveUpReconnection(ScriptCommandMessage sentMessage) {
	//iterate through the listeners and make them give-up
	//basically all services would reset them selves and then continue to
	//attempt to reconnect
	synchronized(this) {
	    DiagnosticUtilitiesConnectionCallback[] listeners = sentMessage.getAllConnectionCallbacks();
	    DiagnosticUtilitiesConnectionCallback listener;
	    
	    for (int i = 0; i < listeners.length; i++){
		listener = listeners[i];
		listener.serviceDown(RSController.RESOURCE_MANAGER);
	    }
	    messageList.remove(sentMessage.getMessageID());
	}
	//Keep probing rscontroller forever
	attemptReconnect(0, sentMessage);
    }

    public void setIPCServer(IPCServer ipcServer){
	this.ipcServer = ipcServer;
    }

    protected void handleStartDBResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage){
	
	if (checkingDB){
	    attemptDBConnect(sentMessage);
	}
    }

    protected void handleStartAsteriskResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage){
	attemptTelephonyUp(sentMessage);
    }

    protected void handleStartServletResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage){
	if (sentMessage != null)
	    attemptUpload(sentMessage);
    }

    protected void attemptUpload(ScriptCommandMessage sentMessage){
	String servletName;
	//removeServlet(sentMessage.getMessageID());
	synchronized(this) {
	    DiagnosticUtilitiesServletCallback[] listeners = sentMessage.getAllServletCallbacks();
	    for (DiagnosticUtilitiesServletCallback listener : listeners) {
		listener.attemptUpload();
	    }
	    messageList.remove(sentMessage.getMessageID());
	}

    }


    public void receiveMessage(String msg) {
	logger.debug("ReceiveMessage: Received message from the script: " + msg);
	ScriptResponseMessage responseMessage = new ScriptResponseMessage(msg);

	if (responseMessage.getMessageID() == -1) {
	    logger.error("ReceiveMessage: Malformed message received.");
	    return;
	}
	ScriptCommandMessage sentMessage;
	synchronized(this) {
	    sentMessage = messageList.get(responseMessage.getMessageID());
	}
	if (sentMessage == null) {
	    logger.error("ReceiveMessage: Could not find a message sent with the id: " + responseMessage.getMessageID());
	    return;
	} else if (!sentMessage.getCommand().equals(responseMessage.getCommand())) {
	    logger.error("ReceiveMessage: Sent and received message command dont match. sent:" + sentMessage.getCommand() + ", received: " + responseMessage.getCommand());
	    return;
	}
	
	String[] ipAddresses = sentMessage.getIPAddresses();
	String[] responses = responseMessage.getResponses();
	if (ipAddresses.length != responses.length) {
	    logger.error("ReceiveMessage: Incorrect number of parameters received in response message. sent: " + ipAddresses.length + ", recieved: " + responses.length);
	    return;
	}

	if ((responseMessage.getCommand()).equals(DiagnosticUtilities.CHK_NW_MSG)){
	    handleCheckNetworkResponse(responseMessage, sentMessage);
	} else if ((responseMessage.getCommand()).equals(DiagnosticUtilities.START_DB_MSG)){
	    handleStartDBResponse(responseMessage, sentMessage);
	} else if ((responseMessage.getCommand()).equals(DiagnosticUtilities.START_SERVLET_MSG)){
	    handleStartServletResponse(responseMessage, sentMessage);
	} else if ((responseMessage.getCommand()).equals(DiagnosticUtilities.RESTART_SYSTEM_MSG)) {
	    handleRestartSystemResponse(responseMessage, sentMessage);
	} else if ((responseMessage.getCommand()).equals(DiagnosticUtilities.SHUTDOWN_SYSTEM_MSG)){
	    handleShutdownSystemResponse(responseMessage, sentMessage);
	} else if ((responseMessage.getCommand()).equals(DiagnosticUtilities.START_ASTERISK_MSG)){
	    handleStartAsteriskResponse(responseMessage, sentMessage);
	} else {
	    logger.error("ReceiveMessage: Unknown message type received from teh script.");
	}
    }

    protected void handleRestartSystemResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage) {
	ArrayList<String> successfulMachines = new ArrayList<String>();
	ArrayList<String> failedMachines = new ArrayList<String>();

	Long msgID = responseMessage.getMessageID();
	String[] receivedMsgArr = responseMessage.getResponses();
	String[] sentMsgArr = sentMessage.getIPAddresses();

	for (int i = 0; i < receivedMsgArr.length; i++) {
	    if (receivedMsgArr[i].equals("true"))
		successfulMachines.add(sentMsgArr[i]);
	    else
		failedMachines.add(sentMsgArr[i]);
	}

	synchronized(this) {
	    DiagnosticUtilitiesSystemCallback[] restartListeners = sentMessage.getAllSystemCallbacks();
	    if (restartListeners.length != 0) {
		restartListeners[0].restartDone(successfulMachines.toArray(new String[]{}), failedMachines.toArray(new String[]{}));
	    } else {
		logger.error("HandleRestartSystemResponse: System listener found null.");
	    }
	    messageList.remove(sentMessage.getMessageID());
	    restartingSystem = false;
	}
    }

    protected void handleShutdownSystemResponse(ScriptResponseMessage responseMessage, ScriptCommandMessage sentMessage) {
	ArrayList<String> successfulMachines = new ArrayList<String>();
	ArrayList<String> failedMachines = new ArrayList<String>();

	Long msgID = responseMessage.getMessageID();;
	String[] receivedMsgArr = responseMessage.getResponses();
	String[] sentMsgArr = sentMessage.getIPAddresses();

	for (int i = 0; i < receivedMsgArr.length; i++) {
	    if (receivedMsgArr[i].equals("true"))
		successfulMachines.add(sentMsgArr[i]);
	    else
		failedMachines.add(sentMsgArr[i]);
	}

	synchronized(this) {
	    DiagnosticUtilitiesSystemCallback[] shutdownListeners = sentMessage.getAllSystemCallbacks();
	    if (shutdownListeners.length != 0) {
		shutdownListeners[0].shutdownDone(successfulMachines.toArray(new String[]{}), failedMachines.toArray(new String[]{}));
	    } else {
		logger.error("HandleShutdownSystemResponse: System listener found null.");
	    }
	    messageList.remove(sentMessage.getMessageID());
	    shutingDownSystem = false;
	}
    }


    public void sendMessage(ScriptCommandMessage message){
	messageQueue.add(message.getMessageString());
    }

    protected void watchTemperature(){
	long interval = stationConfiguration.getIntParam(StationConfiguration.HIGH_TEMPERATURE_TEST_INTERVAL);
	if (interval == -1)
	    interval = 10;
	highTempFile = new File(highTempFileName);
	/*try{
	    logger.error(highTempFile.getCanonicalPath());
	}catch(Exception e){
	    logger.error("Exception", e);
	    }*/
	String readFileName = highTempFileName+".read";
	File readFile = new File(readFileName);

	if (highTempFile.exists())
	    highTempFile.delete();
	if (readFile.exists())
	    readFile.delete();

	while (true){
	    try{
		Thread.sleep(interval * 1000);
	    }catch(Exception e){
		//Thread interrupted
	    }
	    if (highTempFile.exists()){
		logger.warn("WatchTemperature: High temperature detectected.");
		//indicates whether some otehr VM on the machine has read this file. 
		//this helps reduce messages sent to RSC 
		
		if (!readFile.exists()){
		    try{
		    readFile.createNewFile();
		    }catch(Exception e){/*Ignore*/}
		    
		    alertHighTemperature(true);
		    
		}else{
		    alertHighTemperature(false);
		}
		break;
	    }
	}
    }

    protected void alertHighTemperature(boolean firstReader){
	TemperatureAlertMessage msg;
	logger.error("AlertHighTemperature: High temperature found.");
	if (firstReader){
	    String machineID = stationConfiguration.getLocalMachineID();
	    String rscID = stationConfiguration.getRSControllerMachineID();
	    String[] affectedServices = stationConfiguration.getAllServiceInstancesOnMachine(machineID);
	    if (rscID.equals(machineID)){
		String[] services = new String[affectedServices.length + 1];
		System.arraycopy(affectedServices, 0, services, 0, affectedServices.length);
		services[services.length - 1] = "RSController";
		msg = new TemperatureAlertMessage(RSController.ANON_SERVICE, 
						  RSController.UI_SERVICE, 
						  machineID,
						  services);
	    } else {
		msg = new TemperatureAlertMessage(RSController.ANON_SERVICE, 
						  RSController.UI_SERVICE, 
						  machineID,
						  affectedServices);
	    }
	    if (ipcServer.handleOutgoingMessage(msg) > 0) {
		logger.error("AlertHighTemperature: Unable to send alert message.");
	    }
	    else{
		logger.debug("AlertHighTemperature: Alert message sent.");
	    }

	    //Display error on lcd if possible.
	}
	try{
	    Thread.sleep(10000);
	}catch(Exception e){}
	logger.debug("AlertHighTemperature: Exiting..");
	System.exit(-2);
    }

   

    public static DiagnosticUtilities getDiagnosticUtilities(StationConfiguration stationCfg, IPCServer ipcServer){
	if (diagUtil == null){
	    diagUtil = new DiagnosticUtilities(stationCfg, ipcServer);
	}

	return(diagUtil);
    }

    public static DiagnosticUtilities getDiagnosticUtilities(){
	return(diagUtil);
    }

    public DiskSpace getDiskSpace(String dirName){
	try{
	    File file = new File(dirName);
	    return new DiskSpace(file.getTotalSpace(), file.getUsableSpace());

	} catch (Exception e){
	    logger.error("GetDiskSpace: Unable to get DiskSpace for: "+dirName);
	    return new DiskSpace(-1, -1);
	}
    }

    public class DiskSpace{

	long availableSpace;
	long usedSpace;
	
	public DiskSpace(long total, long available){
	    availableSpace = available;
	    usedSpace = total - available;
	}
	
	public long getAvailableSpace(){
	    return availableSpace;
	}

	public long getUsedSpace(){
	    return usedSpace;
	}
    }

    public class ResponseProcessor implements Runnable {
	protected String message;
	protected String response;
	protected Thread thread;
	protected DiagnosticUtilities diagUtil;
	
	public ResponseProcessor(String msg, String resp, DiagnosticUtilities diagUtil){
	    message=msg;
	    response = resp;
	    this.diagUtil = DiagnosticUtilities.getDiagnosticUtilities();
	    thread = new Thread(this, msg+":"+response);
	}

	public void start(){
	    thread.start();
	}

	public void run(){
	    diagUtil.receiveMessage(response);
	}
    }

    public class ScriptCommandMessage {
	long id;
	int messageType;
	String messageCommand;
	String[] ipAddresses;
	String servletName;
	int servletAttempts;
	boolean networkDown = false;
	//XXX: need a better way to have these call backs. For a message only one of these call 
	//back types is used.
	HashSet<DiagnosticUtilitiesConnectionCallback> connectionCallbacks = new HashSet<DiagnosticUtilitiesConnectionCallback>();
	HashSet<DiagnosticUtilitiesDBCallback> dbCallbacks = new HashSet<DiagnosticUtilitiesDBCallback>();
	HashSet<DiagnosticUtilitiesServletCallback> servletCallbacks = new HashSet<DiagnosticUtilitiesServletCallback>();
	HashSet<DiagnosticUtilitiesSystemCallback> systemCallbacks = new HashSet<DiagnosticUtilitiesSystemCallback>();
	HashSet<DiagnosticUtilitiesPingCallback> pingCallbacks = new HashSet<DiagnosticUtilitiesPingCallback>();
	HashSet<DiagnosticUtilitiesAsteriskCallback> asteriskCallbacks = new HashSet<DiagnosticUtilitiesAsteriskCallback>();

	public ScriptCommandMessage(String messageCommand, String[] ipAddresses, int messageType) {
	    id = getUniqueMessageID();
	    this.messageCommand = messageCommand;
	    this.messageType = messageType;
	    this.ipAddresses = ipAddresses;
	    servletName = "";
	    servletAttempts = 0;
	}

	public ScriptCommandMessage(String messageCommand, String[] ipAddresses, int messageType, String servletName) {
	    id = getUniqueMessageID();
	    this.messageCommand = messageCommand;
	    this.messageType = messageType;
	    this.ipAddresses = ipAddresses;
	    this.servletName = servletName;
	    servletAttempts = 0;
	}

	public synchronized int getServletAttempts() {
	    return servletAttempts;
	}

	public synchronized boolean networkDown() {
	    return networkDown;
	}

	public synchronized void setNetworkDown(boolean set) {
	    networkDown = set;
	}

	public synchronized int incrementServletAttempts() {
	    servletAttempts++;
	    return servletAttempts;
	}

	public synchronized String getServletName() {
	    return servletName;
	}

	public synchronized long getUniqueMessageID() {
	    long tmpID;
	    synchronized(messageIDSyncObject) {
		messageID++;
		if (messageID < 0)
		    messageID = 0;
		tmpID = messageID;
	    }
	    return tmpID;
	}

	public synchronized String getMessageString() {
	    String message = id + DiagnosticUtilities.DELIMITER + messageCommand;
	    for (String ip : ipAddresses) {
		message += (DiagnosticUtilities.DELIMITER + ip);
	    }

	    return message;
	}

	public synchronized int getMessageType() {
	    return messageType;
	}

	public synchronized long getMessageID() {
	    return id;
	}

	public synchronized String[] getIPAddresses() {
	    return ipAddresses;
	}

	public synchronized String getCommand() {
	    return messageCommand;
	}

	public synchronized DiagnosticUtilitiesConnectionCallback[] getAllConnectionCallbacks() {
	    return connectionCallbacks.toArray(new DiagnosticUtilitiesConnectionCallback[] {});
	}

	public synchronized DiagnosticUtilitiesDBCallback[] getAllDBCallbacks() {
	    return dbCallbacks.toArray(new DiagnosticUtilitiesDBCallback[] {});
	}

	public synchronized DiagnosticUtilitiesServletCallback[] getAllServletCallbacks() {
	    return servletCallbacks.toArray(new DiagnosticUtilitiesServletCallback[] {});
	}

	public synchronized DiagnosticUtilitiesSystemCallback[] getAllSystemCallbacks() {
	    return systemCallbacks.toArray(new DiagnosticUtilitiesSystemCallback[] {});
	}

	public synchronized DiagnosticUtilitiesPingCallback[] getAllPingCallbacks() {
	    return pingCallbacks.toArray(new DiagnosticUtilitiesPingCallback[] {});
	}

	public synchronized DiagnosticUtilitiesAsteriskCallback[] getAllAsteriskCallbacks() {
	    return asteriskCallbacks.toArray(new DiagnosticUtilitiesAsteriskCallback[] {});
	}

	public synchronized void addConnectionCallback(DiagnosticUtilitiesConnectionCallback callback) {
	    connectionCallbacks.add(callback);
	}

	public synchronized void addDBCallback(DiagnosticUtilitiesDBCallback callback) {
	    dbCallbacks.add(callback);
	}

	public synchronized void addServletCallback(DiagnosticUtilitiesServletCallback callback) {
	    servletCallbacks.add(callback);
	}
	
	public synchronized void addAsteriskCallback(DiagnosticUtilitiesAsteriskCallback callback) {
	    asteriskCallbacks.add(callback);
	}

	public synchronized void addSystemCallback(DiagnosticUtilitiesSystemCallback callback) {
	    systemCallbacks.add(callback);
	}

	public synchronized void addPingCallback(DiagnosticUtilitiesPingCallback callback) {
	    pingCallbacks.add(callback);
	}
    }
    

    public class ScriptResponseMessage {
	long id;
	String messageCommand;
	String[] responses;
	public ScriptResponseMessage(String messageString) {
	    String[] msgArray = messageString.split(DiagnosticUtilities.DELIMITER);
	    try {
		id = Long.parseLong(msgArray[0]);
	    } catch(NumberFormatException e) {
		id = -1;
		return;
	    }
	    
	    messageCommand = msgArray[1];
	    responses = new String[msgArray.length - 2];
	    for (int i = 2; i < msgArray.length; i++) {
		responses[i - 2] = msgArray[i];
	    }
	}

	public synchronized String getMessageString() {
	    String message = id + DiagnosticUtilities.DELIMITER + messageCommand;
	    for (String response : responses) {
		message += (DiagnosticUtilities.DELIMITER + response);
	    }

	    return message;
	}

	public synchronized long getMessageID() {
	    return id;
	}

	public synchronized String[] getResponses() {
	    return responses;
	}

	public synchronized String getCommand() {
	    return messageCommand;
	}

    }
}
