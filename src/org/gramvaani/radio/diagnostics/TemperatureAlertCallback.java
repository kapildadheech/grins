package org.gramvaani.radio.diagnostics;

public interface TemperatureAlertCallback {
    public void alertHighTemperature(Boolean firstReader);
}