package org.gramvaani.radio.diagnostics;

public interface DefaultUncaughtExceptionCallback {

    public void handleUncaughtException(Thread t, Throwable ex);
}