package org.gramvaani.radio.diagnostics;

import java.util.*;
import org.gramvaani.utilities.*;

public class DefaultUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    protected LogUtilities logger;
    protected String logPrefix;
    protected DefaultUncaughtExceptionCallback callbackClass;

    public DefaultUncaughtExceptionHandler (DefaultUncaughtExceptionCallback cls, String logPrefix){
	logger = new LogUtilities(logPrefix+":DefaultUncaughtExceptionHandler");
	callbackClass = cls;
	logger.debug("New default exception handler set.");

    }
    public void uncaughtException(Thread t, Throwable ex){
	logger.error("An unhandled exception has occured in thread "+t.getName(), ex);
	callbackClass.handleUncaughtException(t, ex);
    }
}

