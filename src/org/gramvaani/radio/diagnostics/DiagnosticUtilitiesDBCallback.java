package org.gramvaani.radio.diagnostics;
import org.gramvaani.simpleipc.*;

public interface DiagnosticUtilitiesDBCallback {
    public boolean attemptDBConnect();
}