package org.gramvaani.radio.diagnostics;

public interface DiagnosticUtilitiesPingCallback {

    public void pingResponse(String ipAddress, boolean status);

}