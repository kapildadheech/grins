package org.gramvaani.radio.diagnostics;
import org.gramvaani.simpleipc.*;

public interface DiagnosticUtilitiesSystemCallback {
    public void restartDone(String[] restartedMachines, String[] failedMachines);
    public void shutdownDone(String[] restartedMachines, String[] failedMachines);
}