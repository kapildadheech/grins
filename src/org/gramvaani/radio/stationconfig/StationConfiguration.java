package org.gramvaani.radio.stationconfig;

import java.util.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.InetAddress;
import java.net.NetworkInterface;

import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.gui.GUIUtilities;
import org.gramvaani.radio.telephonylib.TelephonyLine;
import org.gramvaani.radio.telephonylib.SMSLine;
import org.gramvaani.radio.telephonylib.RSCallerID;

public class StationConfiguration {

    public static final String CONFIG_FILE_NAME = "automation.conf";

    public static final String AUDIO_SERVICE_DEFAULT_INSTANCE 	= "AUDIO_SERVICE_COMMON";
    public static final String ARCHIVER_SERVICE_DEFAULT_INSTANCE= "ARCHIVER_SERVICE_COMMON";
    public static final String MIC_SERVICE_DEFAULT_INSTANCE 	= "MIC_SERVICE_COMMON";
    public static final String MONITOR_SERVICE_DEFAULT_INSTANCE = "MONITOR_SERVICE_COMMON";
    public static final String STREAMING_SERVICE_DEFAULT_INSTANCE= "STREAMING_SERVICE_COMMON";

    public static final String RSCONTROLLER_MACHINE = "RSCONTROLLER_MACHINE";
    public static final String RSAPP_MACHINE 	    = "RSAPP_MACHINE";
    public static final String UPLOAD_MACHINE       = "UPLOAD_MACHINE";

    public static final String LINUX = "LINUX";
    public static final String WINDOWS = "WINDOWS";
    public static final String UNKNOWN = "UNKNOWN";

    public static final String IPC_SERVER 	= "IPC_SERVER";
    public static final String IPC_SERVER_PORT 	= "IPC_PORT";
    public static final String SYNC_MESSAGE_TIMEOUT = "SYNC_MESSAGE_TIMEOUT";
    public static final String MESSAGE_FACTORY 	= "MESSAGE_FACTORY";
    public static final String SERVICE_CONN_RETRIES = "SERVICE_CONN_RETRIES";
    public static final String WIDGET 		= "WIDGET";
    public static final String WIDGET_SERVICE 	= "WIDGET_SERVICE";
    public static final String MACHINE 		= "MACHINE";
    public static final String SERVICE 		= "SERVICE";
    public static final String RESOURCE 	= "RESOURCE";
    public static final String SERVICE_RESOURCE = "SERVICE_RESOURCE";
    public static final String RESOURCE_CONFIG 	= "RESOURCE_CONFIG";
    public static final String SERVER_REFRESH_TIME = "SERVER_REFRESH_TIME";
    public static final String SERVER_TIMEOUT 	= "SERVER_TIMEOUT";
    public static final String CLIENT_REFRESH_TIME = "CLIENT_REFRESH_TIME";
    public static final String LIB_DB_SERVER 	= "LIB_DB_SERVER";
    public static final String LIB_DB_NAME 	= "LIB_DB_NAME";
    public static final String LIB_DB_LOGIN 	= "LIB_DB_LOGIN";
    public static final String LIB_DB_PASSWORD 	= "LIB_DB_PASSWORD";
    public static final String LIB_BASE_DIR 	= "LIB_BASE_DIR";
    public static final String LIB_ENC_DIR 	= "LIB_ENC_DIR";
    public static final String LIB_CODEC 	= "LIB_CODEC";
    public static final String MAX_CACHE_OBJECTS = "MAX_CACHE_OBJECTS";
    public static final String PROGRAM_INDEX_DIR = "PROGRAM_INDEX_DIR";
    public static final String ANCHOR_INDEX_DIR = "ANCHOR_INDEX_DIR";
    public static final String CATEGORIES_FILE  = "CATEGORIES_FILE";
    public static final String MAX_SEARCH_SESSIONS = "MAX_SEARCH_SESSIONS";
    public static final String IDLE_MONITOR_POLL_INTERVAL = "IDLE_MONITOR_POLL_INTERVAL";
    public static final String UPLOAD_URL 	= "UPLOAD_URL";
    public static final String SERVLET_PORT 	= "SERVLET_PORT";
    public static final String UPLOAD_MAX_FILESIZE = "UPLOAD_MAX_FILESIZE";
    public static final String STATION_NAME 	= "STATION_NAME";
    public static final String DEFAULT_LANGUAGE = "DEFAULT_LANGUAGE";
    public static final String SCRIPTS_DIR      = "SCRIPTS_DIR";
    public static final String LOG_FILE_SIZE 	= "LOG_FILE_SIZE";

    public static final String TELEPHONY_SERVER            = "TELEPHONY_SERVER";
    public static final String TELEPHONY_PORT              = "TELEPHONY_PORT";
    public static final String TELEPHONY_ADMIN             = "TELEPHONY_ADMIN";
    public static final String TELEPHONY_PASSWORD          = "TELEPHONY_PASSWORD";
    public static final String CONTEXT_GRACEFUL_HANGUP     = "CONTEXT_GRACEFUL_HANGUP";
    public static final String EXTN_GRACEFUL_HANGUP        = "EXTN_GRACEFUL_HANGUP";
    public static final String PRIORITY_GRACEFUL_HANGUP    = "PRIORITY_GRACEFUL_HANGUP";
    public static final String TELEPHONY_STATUS_OFFLINE    = "TELEPHONY_STATUS_OFFLINE";
    public static final String TELEPHONY_STATUS_ONLINE     = "TELEPHONY_STATUS_ONLINE";
    public static final String TELEPHONY_STATUS_FILE       = "TELEPHONY_STATUS_FILE";
    public static final String TELEPHONY_LINES             = "TELEPHONY_LINES";
    public static final String INTERNATIONAL_CALL_PREFIX   = "INTERNATIONAL_CALL_PREFIX";
    public static final String NATIONAL_CALL_PREFIX        = "NATIONAL_CALL_PREFIX";
    public static final String LOCAL_COUNTRY_CODE          = "LOCAL_COUNTRY_CODE";
    public static final String LOCAL_AREA_CODE             = "LOCAL_AREA_CODE";

    public static final String SMS_LINES     = "SMS_LINES";

    public static final String STREAMING_NAME        = "STREAMING_NAME";
    public static final String STREAMING_HOST        = "STREAMING_HOST";
    public static final String STREAMING_PORT        = "STREAMING_PORT";
    public static final String STREAMING_USER        = "STREAMING_USER";
    public static final String STREAMING_PASSWORD    = "STREAMING_PASSWORD";

    public static final String MONITOR_TEST_FREQUENCY = "MONITOR_TEST_FREQUENCY";
    public static final String GOLIVE_BG_GAIN   = "GOLIVE_BG_GAIN";
    public static final String ARCHIVER_CODEC 	= "ARCHIVER_CODEC";
    public static final String ARCHIVER_SAMPLE_RATE = "ARCHIVER_SAMPLE_RATE";
    public static final String MONITOR_SAMPLE_RATE   = "MONITOR_SAMPLE_RATE";
    public static final String ENCODE_SAMPLE_RATE   = "ENCODE_SAMPLE_RATE";
    public static final String RESAMPLER        = "RESAMPLER";
    public static final String CLOCK_SYNC_TIMEOUT = "CLOCK_SYNC_TIMEOUT";
    public static final String ALSASINK_LATENCY_TIME = "ALSASINK_LATENCY_TIME";
    public static final String ALSASINK_BUFFER_TIME = "ALSASINK_BUFFER_TIME";
    public static final String ALSASINK_SYNC = "ALSASINK_SYNC";
    public static final String ALSASRC_LATENCY_TIME = "ALSASRC_LATENCY_TIME";
    public static final String ALSASRC_BUFFER_TIME = "ALSASRC_BUFFER_TIME";
    public static final String DIRECTSOUNDSINK_LATENCY_TIME = "ALSASINK_LATENCY_TIME";
    public static final String DIRECTSOUNDSINK_BUFFER_TIME = "DIRECTSOUNDSINK_BUFFER_TIME";
    public static final String DIRECTSOUNDSINK_SYNC = "DIRECTSOUNDSINK_SYNC";
    public static final String PERSISTENT_QUEUE_DIR = "PERSISTENT_QUEUE_DIR";

    public static final String OPERATING_SYSTEM = "OPERATING_SYSTEM";
  
    public static final String HIGH_TEMPERATURE_FILE_NAME = "HIGH_TEMPERATURE_FILE_NAME";
    public static final String NETWORK_DOWN_WAIT_TIME = "NETWORK_DOWN_WAIT_TIME";

    public static final String ICON_PATH 		= "ICON_PATH";
    public static final String PLAY_ICON 		= "PLAY_ICON";
    public static final String PAUSE_ICON 		= "PAUSE_ICON";
    public static final String STOP_ICON 		= "STOP_ICON";
    public static final String RECORD_ICON 		= "RECORD_ICON";
    public static final String RECORD_ACTIVE_ICON       = "RECORD_ACTIVE_ICON";
    public static final String UP_ICON 		        = "UP_ICON";
    public static final String DOWN_ICON 		= "DOWN_ICON";
    public static final String FWD_ICON 		= "FWD_ICON";
    public static final String ADD_ICON 		= "ADD_ICON";
    public static final String REMOVE_ICON		= "REMOVE_ICON";
    public static final String DELETE_ICON 		= "DELETE_ICON";
    public static final String PREVIEW_ICON 		= "PREVIEW_ICON";
    public static final String GOLIVE_ICON 		= "GOLIVE_ICON";
    public static final String GOLIVE_ACTIVE_ICON       = "GOLIVE_ACTIVE_ICON";
    public static final String INFO_ICON 		= "INFO_ICON";
    public static final String PREVIEW_STOP_ICON 	= "PREVIEW_STOP_ICON";
    public static final String LOAD_PLAYLIST_ICON 	= "LOAD_PLAYLIST_ICON";
    public static final String SAVE_ICON 	        = "SAVE_ICON";
    public static final String CANCEL_ICON		= "CANCEL_ICON";
    public static final String SETSTART_ICON 	        = "SETSTART_ICON";
    public static final String NEXT_ICON 	        = "NEXT_ICON";
    public static final String PREV_ICON 	        = "PREV_ICON";
    public static final String COLOR_PICKER_ICON	= "COLOR_PICKER_ICON";

    public static final String LIBRARY_ICON 	        = "LIBRARY_ICON";
    public static final String UPLOAD_ICON 	        = "UPLOAD_ICON";
    public static final String HOTKEYS_ICON 	        = "HOTKEYS_ICON";
    public static final String LOGVIEW_ICON 	        = "LOGVIEW_ICON";
    public static final String BACKUP_ICON		= "BACKUP_ICON";
    public static final String EDIT_ICON 	        = "EDIT_ICON";
    public static final String SEARCH_ICON 	        = "SEARCH_ICON";
    public static final String LIST_ICON 	        = "LIST_ICON";
    public static final String CLOSE_ICON 	        = "CLOSE_ICON";
    public static final String MINIMIZE_ICON 	        = "MINIMIZE_ICON";
    public static final String STATS_ICON 	        = "STATS_ICON";
    public static final String DIAGNOSTICS_ICON         = "DIAGNOSTICS_ICON";
    public static final String NETWORK_ICON             = "NETWORK_ICON";
    public static final String ADMIN_ICON               = "ADMIN_ICON";
    public static final String OK_ICON                  = "OK_ICON";
    public static final String NOTOK_ICON               = "NOTOK_ICON";
    public static final String UNKNOWN_ICON             = "UNKNOWN_ICON";
    public static final String CRITICAL_ICON            = "CRITICAL_ICON";
    public static final String SWITCH_ICON		= "SWITCH_ICON";
    public static final String RESTART_ICON             = "RESTART_ICON";
    public static final String SHUTDOWN_ICON            = "SHUTDOWN_ICON";
    public static final String SPEAKER_ICON             = "SPEAKER_ICON";
    public static final String FADEOUT_ICON             = "FADEOUT_ICON";
    public static final String SLIDER_ICON		= "SLIDER_ICON";
    public static final String GRINS_ICON               = "GRINS_ICON";
    public static final String TELEPHONY_ICON           = "TELEPHONY_ICON";
    public static final String CONTACTS_ICON		= "CONTACTS_ICON";
    public static final String STREAMING_ICON		= "STREAMING_ICON";
    public static final String CONNECTING_ICON		= "CONNECTING_ICON";

    public static final String NEW_CREATOR_ICON		= "NEW_CREATOR_ICON";
    public static final String EDIT_CREATOR_ICON	= "EDIT_CREATOR_ICON";
    public static final String REMOVE_CREATOR_ICON	= "REMOVE_CREATOR_ICON";
    public static final String RIGHT_ARROW_ICON		= "RIGHT_ARROW_ICON";
    public static final String EXPORT_ICON		= "EXPORT_ICON";
    public static final String MAKE_REPORT_ICON		= "MAKE_REPORT_ICON";
    public static final String CHECK_ICON		= "CHECK_ICON";
    public static final String REDO_ICON		= "REDO_ICON";
    public static final String CLEAR_BAR_RTL_ICON	= "CLEAR_BAR_RTL_ICON";
    public static final String PICKUP_CALL_ICON         = "PICKUP_CALL_ICON";
    public static final String HANGUP_CALL_ICON         = "HANGUP_CALL_ICON";
    public static final String AUDIO_HIGH_ICON		= "AUDIO_HIGH_ICON";
    public static final String AUDIO_MUTED_ICON		= "AUDIO_MUTED_ICON";
    public static final String RAW_TELEPHONY_ICON	= "RAW_TELEPHONY_ICON";
    public static final String IMPORTED_TELEPHONY_ICON	= "IMPORTED_TELEPHONY_ICON";
    public static final String INCOMING_ARROW_ICON	= "INCOMING_ARROW_ICON";
    public static final String SMS_ICON                 = "SMS_ICON";
    public static final String SEND_SMS_ICON            = "SEND_SMS_ICON";
    public static final String POLL_ICON                = "POLL_ICON";
    public static final String NEW_POLL_ICON            = "NEW_POLL_ICON";
    public static final String EDIT_POLL_ICON           = "EDIT_POLL_ICON";

    public static final String PLAYLIST_TOPPANE_COLOR 	= "PLAYLIST_TOPPANE_COLOR";
    public static final String PLAYLIST_LIVEPANE_COLOR 	= "PLAYLIST_LIVEPANE_COLOR";
    public static final String PLAYLIST_ITEM_BGCOLOR 	= "PLAYLIST_ITEM_BGCOLOR";
    public static final String PLAYLIST_ITEM_ERROR_BGCOLOR= "PLAYLIST_ITEM_ERROR_BGCOLOR";
    public static final String PLAYLIST_ITEM_GOLIVECOLOR = "PLAYLIST_ITEM_GOLIVECOLOR";
    public static final String PLAYLIST_ITEM_SELECTCOLOR = "PLAYLIST_ITEM_SELECTCOLOR";
    public static final String PLAYLIST_CONTROLPANE_COLOR= "PLAYLIST_CONTROLPANE_COLOR";
    public static final String TOPPANE_COLOR            = "TOPPANE_COLOR";
    public static final String BOTTOMPANE_COLOR         = "BOTTOMPANE_COLOR";
    public static final String SIMULATION_BUTTON_COLOR  = "SIMULATION_BUTTON_COLOR";
    public static final String NEXTPREV_BUTTON_COLOR    = "NEXTPREV_BUTTON_COLOR";
    public static final String LOADSAVE_POPUP_COLOR     = "LOADSAVE_POPUP_COLOR";

    public static final String LIBRARY_TOPPANE_COLOR    = "LIBRARY_TOPPANE_COLOR";
    public static final String SEARCHCLEAR_BUTTON_COLOR    = "SEARCHCLEAR_BUTTON_COLOR";
    public static final String LIBRARY_DISPLAYFILTERPANE_COLOR    = "LIBRARY_DISPLAYFILTERPANE_COLOR";
    public static final String LIBRARY_LISTITEM_COLOR    = "LIBRARY_LISTITEM_COLOR";
    public static final String LIBRARY_LISTITEM_SELECT_COLOR    = "LIBRARY_LISTITEM_SELECT_COLOR";
    public static final String LIBRARY_ACTIVEFILTERPANE_COLOR    = "LIBRARY_ACTIVEFILTERPANE_COLOR";
    public static final String LIBRARY_ACTIVEFILTERPANE_BGCOLOR    = "LIBRARY_ACTIVEFILTERPANE_BGCOLOR";
    public static final String LIBRARY_DISPLAYFILTER_BUTTON_COLOR    = "LIBRARY_DISPLAYFILTER_BUTTON_COLOR";
    public static final String LIBRARY_ACTIVEFILTER_BUTTON_COLOR    = "LIBRARY_ACTIVEFILTER_BUTTON_COLOR";
    public static final String LIBRARY_ACTIVEFILTERPANE_BUTTON_COLOR    = "LIBRARY_ACTIVEFILTERPANE_BUTTON_COLOR";

    public static final String CALL_RINGING_COLOR 	= "CALL_RINGING_COLOR";
    public static final String CALL_CONNECTED_COLOR	= "CALL_CONNECTED_COLOR";
    public static final String TELEPHONY_INCOMING_COLOR = "TELEPHONY_INCOMING_COLOR";
    public static final String TELEPHONY_ACTIVE_COLOR 	= "TELEPHONY_ACTIVE_COLOR";
    public static final String TELEPHONY_SELECTED_COLOR = "TELEPHONY_SELECTED_COLOR";
    public static final String TELEPHONY_DIAL_COLOR 	= "TELEPHONY_DIAL_COLOR";
    public static final String TELEPHONY_METADATA_COLOR = "TELEPHONY_METADATA_COLOR";

    public static final String STD_BORDER_COLOR             = "STD_BORDER_COLOR";
    public static final String STD_SELECTED_BORDER_COLOR    = "STD_SELECTED_BORDER_COLOR";
    public static final String LIST_ITEM_BORDER_COLOR       = "LIST_ITEM_BORDER_COLOR";    

    public static final String GRAPH_COLOR                  = "GRAPH_COLOR";

    public static final String LISTITEM_TITLE_COLOR         = "LISTITEM_TITLE_COLOR";
    public static final String LISTITEM_DETAILS_COLOR       = "LISTITEM_DETAILS_COLOR";
    
    public static final String UPLOADLIST_DUPLICATE_COLOR   = "UPLOADLIST_DUPLICATE_COLOR";
    public static final String BUTTON_DISABLED_COLOR        = "BUTTON_DISABLED_COLOR";
    public static final String ERROR_FIELD_COLOR	    = "ERROR_FIELD_COLOR";

    public static final String SELECTED_TAG_BG_COLOR	    = "SELECTED_TAG_BG_COLOR";
    public static final String SELECTEDCHILD_TAG_BG_COLOR   = "SELECTEDCHILD_TAG_BG_COLOR";
    public static final String EDITED_TAG_BG_COLOR	    = "EDITED_TAG_BG_COLOR";
    public static final String CURRENT_TAG_BG_COLOR	    = "CURRENT_TAG_BG_COLOR";

    public static final String CONFIRM_HIGHLIGHT_COLOR	    = "CONFIRM_HIGHLIGHT_COLOR";

    public static final String NETWORK_DOWN_RECONNECTION_INTERVAL = "NETWORK_DOWN_RECONNECTION_INTERVAL";
    public static final String HIGH_TEMPERATURE_TEST_INTERVAL ="HIGH_TEMPERATURE_TEST_INTERVAL";
    public static final String CONFIGPATH = "CONFIGPATH";

    private Hashtable<String,Object> configParams;
    private ArrayList<String> machines;
    private ArrayList<String> localAddresses;
    private String localMachineID;
    private StationNetwork stationNetwork;

    private LogUtilities logger;


    public StationConfiguration(){
	this(null);
    }

    public StationConfiguration(String localMachineID) {
	logger = new LogUtilities("StationConfiguration");
	logger.debug("StationConfiguration: Initialized");

	this.localMachineID = localMachineID;
	configParams = new Hashtable<String,Object>();
	machines = new ArrayList<String>();
	localAddresses = new ArrayList<String>();
	getLocalAddresses(localAddresses);
	getOperatingSystem();
    }

    public void readConfig(String path) {
	logger.debug("ReadConfig: Entering");

	configParams.put(CONFIGPATH, path);

	initOperatingSystem();
	try {
	    logger.debug("ReadConfig: Reading the config file.");
	    BufferedReader in = new BufferedReader(new FileReader(path));
	    String str;
	    String context, contextMethod;
	    Method m = null;
	    while ((str = in.readLine()) != null) {
		str.trim();
		if (str.equals("") || str.startsWith("#"))
		    continue;
		if (str.startsWith("[")) {
		    context = str.substring(1, str.length() - 1).trim();
		    contextMethod = context + "Config";
		    m = StationConfiguration.class.getDeclaredMethod(contextMethod, new Class[] {String.class});
		} else {		    
		    if (m != null)
			m.invoke(this, new Object[] {str});
		    else{
			logger.error("ReadConfig: Syntax error in config file. Cannot continue.");
			System.exit(-1);
			//initDefaults();
		    }
		}
	    }
	} catch (Exception e) {
	    logger.error("ReadConfig: Cannot read config file", e);
	    System.exit(-1);
	}

	if (localMachineID == null || localMachineID.equals("127.0.0.1"))
	    findLocalMachineID();
	stationNetwork = new StationNetwork(this);

	populateTelephonyData();
	logger.debug("ReadConfig: Leaving");
    }

    public StationNetwork getStationNetwork() {
	return stationNetwork;
    }

    public String getOperatingSystem() {
	return (String)(configParams.get(OPERATING_SYSTEM));
    }

    public boolean isLinux(){
	return (getOperatingSystem().equals(LINUX));
    }

    public boolean isWindows(){
	return (getOperatingSystem().equals(WINDOWS));
    }

    String getWinUserDesktopPath() {
	final String REGQUERY_UTIL = "reg query ";
	final String REGSTR_TOKEN = "REG_SZ";
	final String DESKTOP_FOLDER_CMD = REGQUERY_UTIL +
	    "\"HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\" 
	    + "Explorer\\Shell Folders\" /v DESKTOP";

	try {
	    Process process = Runtime.getRuntime().exec(DESKTOP_FOLDER_CMD);
	    InputStream is = process.getInputStream();
	    StringWriter sw = new StringWriter();
	    int c;

	    try {
		while ((c = is.read()) != -1)
		    sw.write(c);
	    } catch (IOException e) { }
	    String result = sw.toString();
	    int p = result.indexOf(REGSTR_TOKEN);
	    
	    if (p == -1)
		return null;
	    
	    return result.substring(p + REGSTR_TOKEN.length()).trim();
	} catch (Exception e) {
	    return null;
	}
    }

    public String getUserDesktopPath(){
	String desktop;
	if (isWindows())
	    desktop = getWinUserDesktopPath();
	else
	    desktop = System.getenv("HOME") + "/Desktop";

	return desktop;
    }

    public void initOperatingSystem() {
	String osType = System.getProperty("os.name");
	if (osType.toLowerCase().startsWith("linux"))
	    configParams.put(OPERATING_SYSTEM, LINUX);
	else if (osType.toLowerCase().startsWith("windows"))
	    configParams.put(OPERATING_SYSTEM, WINDOWS);
	else
	    configParams.put(OPERATING_SYSTEM, UNKNOWN);
    }

    public boolean isValidInstance() {
	if(localMachineID == null)
	    return false;

	String localIP = getMachineIP(localMachineID);

	if((localIP == null) || !localAddresses.contains(localIP)) 
	    return false;
	else
	    return true;
    }

    protected void gstreamerConfig(String str){
	String[] args=getArrayFromString(str);
	if (args.length != 2)
	    logger.warn("GstreamerConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void diagnosticsConfig(String str){
	String[] args=getArrayFromString(str);
	if (args.length != 2)
	    logger.warn("DiagnosticsConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void widgetsConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("WidgetsConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(WIDGET + "." + args[0], args[1]);
    }

    protected void machinesConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("MachinesConfig: Syntax error in '"+str+"'");
	else{
	    configParams.put(MACHINE + "." + args[0], args[1]);
	    machines.add(args[0]);
	}
    }

    protected void machinenamesConfig(String str) {
	//Just ignore
	//entries used for installation purposes only
    }

    @SuppressWarnings("unchecked") 
    protected void servicesConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 4)
	    logger.warn("ServicesConfig: Syntax error in '"+str+"'");
	else{
	    ArrayList<Service> serviceDesc = (ArrayList<Service>)(configParams.get(SERVICE + "." + args[0]));
	    if (serviceDesc == null) {
		serviceDesc = new ArrayList<Service>();
		configParams.put(SERVICE + "." + args[0], serviceDesc);
	    }
	    Service service = new Service(args[1], args[2], args[3], args[0]);
	    serviceDesc.add(service);
	    configParams.put(SERVICE + "INSTANCE." + args[3], service); 
	}
    }

    protected void widgetservicesConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 3)
	    logger.warn("WidgetServicesConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(WIDGET_SERVICE + "." + args[0] + "." + args[2], args[1]);
    }

    @SuppressWarnings("unchecked") 
    protected void resourcesConfig(String str) {
	String[] args = getArrayFromString(str);
       	if (args.length < 3)
	    logger.warn("ResourcesConfig: Syntax error in '"+str+"'");
	else{
	    ArrayList<Resource> resourceDesc = (ArrayList)(configParams.get(RESOURCE + "." + args[0]));
	    if (resourceDesc == null) {
		resourceDesc = new ArrayList<Resource>();
		configParams.put(RESOURCE + "." + args[0], resourceDesc);
	    }
	    resourceDesc.add(new Resource(args[1], args[2]));
	}
    }

    @SuppressWarnings("unchecked") 
    protected void serviceresourcesConfig(String str) {
	String[] args = getArrayFromString(str);
       	if (args.length < 3)
	    logger.warn("ServiceResourcesConfig: Syntax error in '"+str+"'");
	else {
	    ArrayList<ServiceResource> serviceresourceDesc = (ArrayList)(configParams.get(SERVICE_RESOURCE + "." + args[0]));
	    if (serviceresourceDesc == null) {
		serviceresourceDesc = new ArrayList<ServiceResource>();
		configParams.put(SERVICE_RESOURCE + "." + args[0], serviceresourceDesc);
	    }
	    serviceresourceDesc.add(new ServiceResource(args[1], args[2]));
	}
    }

    protected void resourceconfigConfig(String str) {
	String args[] = getArrayFromString(str);
	if (args.length < 3)
	    logger.warn("ResourceConfigConfig: Syntax error in '"+str+"'");
	else{
	    logger.debug("ResourceconfigConfig: ---- " + args[0] + ":" + args[1] + ":" + args[2]);
	    //if (getOperatingSystem().equals(WINDOWS)) {
	    //configParams.put(RESOURCE_CONFIG + "." + args[0], new ResourceConfig("WIN", args[2], args[0]));
	    //} else {
	    configParams.put(RESOURCE_CONFIG + "." + args[0], new ResourceConfig(args[1], args[2], args[0]));
	    //}
	}
    }

    protected void ipcConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("IPConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void heartbeatConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("HeartbeatConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void streamingConfig(String str) {
	String[] args = getArrayFromString(str);
	if(args.length < 2)
	    logger.warn("StreamingConfig: Syntax error in '" + str + "'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void telephonyConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("TelephonyConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    @SuppressWarnings("unchecked")
    protected void telephonylineConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 5)
	    logger.warn("TelephonyLineConfig: Syntax error in '"+str+"'");
	else {
	    Hashtable<String, TelephonyLine> phonelineTable = (Hashtable<String, TelephonyLine>)configParams.get(TELEPHONY_LINES);
	    if (phonelineTable == null) {
		phonelineTable = new Hashtable<String, TelephonyLine>();
		configParams.put(TELEPHONY_LINES, phonelineTable);
	    }
	    phonelineTable.put(args[0], new TelephonyLine(args[0], args[1], args[2], args[3], args[4]));
	}
    }

    @SuppressWarnings("unchecked")
    protected void smslineConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 3)
	    logger.warn("SMSLineConfig: Syntax error in '"+str+"'");
	else {
	    Hashtable<String, SMSLine> smsLineTable = (Hashtable<String, SMSLine>)configParams.get(SMS_LINES);
	    if (smsLineTable == null) {
		smsLineTable = new Hashtable<String, SMSLine>();
		configParams.put(SMS_LINES, smsLineTable);
	    }
	    smsLineTable.put(args[0], new SMSLine(args[0], args[1], args[2]));
	}
    }

    protected void libraryConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("LibraryConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void installationConfig(String str){
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("InstallationConfig: Syntax error in'"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }
    
    protected void physicalmachinesConfig(String str){
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("PhysicalmachinesConfig: Syntax error in'"+str+"'");
	else{
	    configParams.put(args[0], str.substring(args[0].length()+1).trim());
	}
    }

    protected void archiverConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("ArchiverConfig: Syntax error in '"+str+"'");
	else {
	    configParams.put(args[0], args[1]);
	}
    }

    protected void audioConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("AudioConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void timekeeperConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("TimeKeeperConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void radiostationConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("RadioStationConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    protected void guiConfig(String str) {
	String[] args = getArrayFromString(str);
	if (args.length < 2)
	    logger.warn("GUIConfig: Syntax error in '"+str+"'");
	else
	    configParams.put(args[0], args[1]);
    }

    public String getIconPath(String str) {
	return configParams.get(ICON_PATH) + "/" + configParams.get(str);
    }

    public java.awt.Color getColor(String str) {
	try{
	    String colorString = (String)(configParams.get(str));
	    return GUIUtilities.getColorFromString(colorString);
	} catch (Exception e){
	    logger.error("GetColor: Could not load color: "+str);
	    return null;
	}
    }

    public int getIntParam(String key) {
	try {
	    return Integer.parseInt((String)(configParams.get(key)));
	} catch(Exception e) {
	    logger.error("GetIntParam: Unable to read integer value: "+key);
	    return -1;
	}
    }

    public long getLongParam(String key) {
	try {
	    return Long.parseLong((String)(configParams.get(key)));
	} catch(Exception e) {
	    logger.error("GetLongParam: Unable to read long value: "+key);
	    return -1;
	}
    }

    public String getStringParam(String key) {
	if (configParams.get(key) == null)
	    logger.error("GetStringParam: Unable to read string value: "+key);
	return (String)(configParams.get(key));
    }
    
    public double getDoubleParam(String key){
	try{
	    return Double.parseDouble((String) configParams.get(key));
	} catch(Exception e){
	    logger.error("GetDoubleParam: Unable to read double value: "+key);
	    return -1.0;
	}
    }

    public Class getClassParam(String classNameKey) {
	try {
	    return Class.forName((String)(configParams.get(classNameKey)));
	} catch(Exception e) {
	    logger.fatal("GetClassParam: Unable to find class:"+classNameKey, e);
	    //System.exit(-1);
	}
	
	return null;
    }

    public String getProviderClassName(String providerName) {
	return (String)(configParams.get(WIDGET + "." + providerName));
    }

    public String getProviderServiceInstance(String providerName, String providerRole) {
	return (String)(configParams.get(WIDGET_SERVICE + "." + providerName + "." + providerRole));
    }

    @SuppressWarnings("unchecked") 
    public ServiceResource[] getServiceResources(String serviceInstanceName) {
	ArrayList serviceresourceDesc = (ArrayList)(configParams.get(SERVICE_RESOURCE + "." + serviceInstanceName));
	if (serviceresourceDesc != null) {
	    return (ServiceResource[])(serviceresourceDesc.toArray(new ServiceResource[serviceresourceDesc.size()]));
	} else {
	    return new ServiceResource[]{};
	}
    }

    @SuppressWarnings("unchecked") 
    public Resource[] getResources(String resourceType) {
	ArrayList resourceDesc = (ArrayList)(configParams.get(RESOURCE + "." + resourceType));
	if (resourceDesc != null) {
	    return (Resource[])(resourceDesc.toArray(new Resource[]{}));
	} else {
	    return new Resource[]{};
	}
    }

    public ResourceConfig getResourceConfig(String resourceName) {
	return (ResourceConfig)(configParams.get(RESOURCE_CONFIG + "." + resourceName));
    }

    public TelephonyLine getTelephonyLine(String lineID) {
	Hashtable phonelineTable = (Hashtable)configParams.get(TELEPHONY_LINES);
	if (phonelineTable == null)
	    return null;

	return (TelephonyLine) phonelineTable.get(lineID);
    }

    //Always returns list sorted by lineID
    @SuppressWarnings("unchecked")
    public TelephonyLine[] getAllTelephonyLines() {
	ArrayList<TelephonyLine> phonelines = new ArrayList<TelephonyLine>();
	Hashtable<String, TelephonyLine> phonelineTable = (Hashtable<String, TelephonyLine>)configParams.get(TELEPHONY_LINES);
	if (phonelineTable != null) {
	    for (String id : getAllTelephonyLineIDs())
		phonelines.add(phonelineTable.get(id));
	}

	return phonelines.toArray(new TelephonyLine[phonelines.size()]);
    }

    @SuppressWarnings("unchecked")
    public String[] getAllTelephonyLineIDs() {
	Hashtable<String, TelephonyLine> phonelineTable = (Hashtable<String, TelephonyLine>)configParams.get(TELEPHONY_LINES);
	if (phonelineTable == null) {
	    return new String[]{};
	} else {
	    String[] ids = phonelineTable.keySet().toArray(new String[]{});
	    Arrays.sort(ids);
	    return ids;
	}
    }

    @SuppressWarnings("unchecked")
    public SMSLine getSMSLine(String id) {
	Hashtable<String, SMSLine> smsLineTable = (Hashtable<String, SMSLine>)configParams.get(SMS_LINES);
	if (smsLineTable == null) {
	    return null;
	} else {
	    return smsLineTable.get(id);
	}
    }

    @SuppressWarnings("unchecked")
    public String[] getAllSMSLineIDs() {
	Hashtable<String, SMSLine> smsLineTable = (Hashtable<String, SMSLine>)configParams.get(SMS_LINES);
	if (smsLineTable == null) {
	    return new String[]{};
	} else {
	    String[] ids = smsLineTable.keySet().toArray(new String[]{});
	    Arrays.sort(ids);
	    return ids;
	}	
    }

    public Service getServiceInstance(String instanceName) {
	return (Service)configParams.get(SERVICE+"INSTANCE."+instanceName);
    }

    public String getMachineIP(String machineID){
	return (String)configParams.get(MACHINE+"."+machineID);
    }

    public boolean isMicServiceActive() {
	return (getServices(RSController.MIC_SERVICE).length > 0);
    }

    public boolean isMonitorServiceActive(){
	return (getServices(RSController.MONITOR_SERVICE).length > 0);
    }

    public boolean isStreamingServiceActive(){
	return (getServices(RSController.STREAMING_SERVICE).length > 0);
    }

    public boolean isServiceActive(String service){
	return (getServices(service).length > 0);
    }

    @SuppressWarnings("unchecked") 
    public Service[] getServices(String serviceName) {
	ArrayList serviceDesc = (ArrayList)(configParams.get(SERVICE + "." + serviceName));
	if (serviceDesc != null) {
	    return (Service[])(serviceDesc.toArray(new Service[]{}));
	} else {
	    return new Service[]{};
	}
    }

    //Returns the instance name of first lib service found
    public String getLibServiceInstanceName(){
	ArrayList serviceDesc = (ArrayList)(configParams.get(SERVICE + "." + RSController.LIB_SERVICE));
	if (serviceDesc != null){
	    Service s = (Service)(serviceDesc.get(0));
	    return s.serviceInstanceName();
	}else{
	    return null;
	}
    }

    public Service[] getAllServices(){
	ArrayList<Service> list = new ArrayList<Service>();

	for (String serviceName: RSController.SERVICES){
	    for (Service service: getServices(serviceName))
		list.add(service);
	}

	return list.toArray(new Service[list.size()]);
    }

    void populateTelephonyData() {
	RSCallerID.internationalCallPrefix = getStringParam(INTERNATIONAL_CALL_PREFIX);
	RSCallerID.nationalCallPrefix = getStringParam(NATIONAL_CALL_PREFIX);
	RSCallerID.localCountryCode = getStringParam(LOCAL_COUNTRY_CODE);
	RSCallerID.localAreaCode = getStringParam(LOCAL_AREA_CODE); 
    }

    public void getLocalAddresses(ArrayList<String> list){
	Enumeration<NetworkInterface> netInterfaces = null;
	try {
	    netInterfaces = NetworkInterface.getNetworkInterfaces();
	} catch (Exception e) {
	    logger.error("GetLocalAddresses: ", e);
	    list.add("127.0.0.1");
	    return;
	}

	while (netInterfaces.hasMoreElements()) {
	    NetworkInterface ni = netInterfaces.nextElement();
	    Enumeration<InetAddress> address = ni.getInetAddresses();
	    while (address.hasMoreElements()) {
		InetAddress addr = address.nextElement();
		logger.debug("getLocalAddresses: " + addr.getHostAddress() + ":" + addr.isLoopbackAddress() + ":" + 
			     addr.isSiteLocalAddress());
		// XXX siteLocalAddress does not work on the thinkpad for some reason
		//		if (!addr.isLoopbackAddress() && addr.isSiteLocalAddress()
		//		    && !(addr.getHostAddress().indexOf(":") > -1)) {
		//		    list.add(addr.getHostAddress());
		//		    logger.debug("getLocalAddresses: added to list");
		//		}
		
		if (!(addr.getHostAddress().indexOf(":") > -1)) {
		    list.add(addr.getHostAddress());
		    logger.debug("getLocalAddresses: added to list");
		}
	    }
	}
	
    }

    public String getRSControllerIP(){
	return getStringParam(StationConfiguration.IPC_SERVER);
    }

    public String getDBIP(){
	String ipPort = getStringParam(StationConfiguration.LIB_DB_SERVER);
	String[] ip = ipPort.split(":");
	return ip[0];
    }

    //Assumption: Uploat url is always of the form http://IP:PORT/THE_REST
    /*public String getUploadIP(){
	String uploadURL = getStringParam(StationConfiguration.UPLOAD_URL);

	if (uploadURL == null)
	    return null;

	String[] parts = uploadURL.split("/");
	String[] ipPort = parts[2].split(":");
	return ipPort[0];
	}*/

    public String getRSControllerMachineID(){
	String ip = getRSControllerIP();
	String id = null;
	for (String machineID: getMachines())
	    if (ip.equals(getMachineIP(machineID))){
		id = machineID;
		break;
	    }
	
	return id;
   }

    private void findLocalMachineID(){
	localMachineID = null;
	for (String machineID: getMachines()) {
	    logger.debug("FindLocalMachineID: " + machineID + ":" + getMachineIP(machineID));
	    if (localAddresses.contains(getMachineIP(machineID))) {
		if (localMachineID == null){
		    localMachineID = machineID;
		} else {
		    if (!machineID.equals(localMachineID)){
			logger.error("VerifyAddresses: Multiple matches for local addresses: "+
				     localMachineID+","+ machineID);
		    }
		}
	    }
	}
	
	if (localMachineID == null){
	    for (String machineID: getMachines()){
		if (getMachineIP(machineID).equals("localhost")){
		    localMachineID = machineID;
		    break;
		}
	    }
	}
    }

    public String getLocalMachineID(){
	return localMachineID;
    }

    public void setLocalMachineID(String localMachineID){
	this.localMachineID = localMachineID;
    }

    public Service[] getLocalServices(){
	ArrayList<Service> list = new ArrayList<Service>();
	
	for (Service service: getAllServices())
	    if (service.machineID().equals(localMachineID))
		list.add(service);

	return list.toArray(new Service[list.size()]);
    }

    public boolean isLocalService(String serviceInstanceName){
	for (Service service: getAllServices())
	    if (service.serviceInstanceName().equals(serviceInstanceName))
		//XXX
		//Here "localhost" added to handle the situation when we want a single machine to play
		//role of RSController as well as RSApp machine, as often arises during development. 
		//Can be removed for production.
		if (service.machineID().equals(localMachineID) || service.machineID().equals("localhost"))
		    return true;
	return false;
    }
	
    public String getServiceInstanceOnMachine(String serviceName, String machineID) {
	Service[] services = getServices(serviceName);
	for (Service service: services) {
	    if (service.machineID().equals(machineID)) {
		return service.serviceInstanceName();
	    }
	}
	return null;
    }

    public String getServiceMachineID(String serviceInstanceName){
	Service[] services = getAllServices();
	String machineID=null;
	for (Service service: services){
	    if (service.serviceInstanceName().equals(serviceInstanceName)){
		machineID = service.machineID();
		break;
	    }
	}
	return machineID;
    }

    public String getServiceIP(String serviceInstanceName){
	String ip = null;
	String machineID = getServiceMachineID(serviceInstanceName);
	if (machineID != null){
	    ip = getMachineIP(machineID);
	}
	return ip;
    }

    public String[] getAllServiceInstancesOnMachine(String serviceName, String machineID){
	Service[] services = getServices(serviceName);
	ArrayList<String> list = new ArrayList<String>();
	for (Service service: services){
	    if (service.machineID().equals(machineID))
		list.add(service.serviceInstanceName());
	}
	return list.toArray(new String[list.size()]);
    }

    public String[] getAllServiceInstancesOnMachine(String machineID){
	Service[] services = getAllServices();
	ArrayList<String> list = new ArrayList<String>();
	for (Service service: services){
	    if (service.machineID().equals(machineID)){
		list.add(service.serviceInstanceName());
	    }
	}
	return list.toArray(new String[list.size()]);
    }

    public String getServiceInstanceMachineID(String serviceName, String serviceInstanceName) {
	Service[] services = getServices(serviceName);
	for (Service service: services) {
	    if (service.serviceInstanceName().equals(serviceInstanceName)) {
		return service.machineID();
	    }
	}
	return null;
    }

    public String[] getNonLocalMachineIPs() {
	String[] machines = getMachines();
	String machineIP;
	HashSet<String>  machineIPs = new HashSet<String>();
	for (int i = 0; i < machines.length; i++) {
	    machineIP = getMachineIP(machines[i]);
	    if (!localAddresses.contains(machineIP) && !machineIP.equals("127.0.0.1") && !machineIP.equals("localhost")) {
		machineIPs.add(getMachineIP(machines[i]));
	    }
	}
	return machineIPs.toArray(new String[]{});
    }

    public String[] getMachines() {
	return machines.toArray(new String[]{});
    }

    // list of widgets, used by RSApp
    //		<widgetName, widgetClass(uiService)>
    // list of machines
    //		<IP address, machineID>
    // list of services to be instantiated locally on RSController
    //		<serviceName, serviceClass(ipcServer, stationcConfiguration), machineID>
    // list of resources used by ResourceManager
    // 		<resourceName, resourceType, machineID>
    // role of resources used by each respective service
    //		<serviceName, role of resource, resourceName> 
    // MessageFactory implementation class



    public static String[] getArrayFromString(String str) {
	ArrayList<String> args = new ArrayList<String>();
	StringTokenizer st = new StringTokenizer(str, ",");
	String prevToken = "";
	while (st.hasMoreTokens()) {
	    String currToken = st.nextToken().trim();
	    if (currToken.endsWith("\\")) {
		prevToken = currToken.substring(0, currToken.length() - 1);
	    } else {
		if (prevToken.equals("")) {
		    args.add(currToken);
		} else {
		    args.add(prevToken + "," + currToken);
		    prevToken = "";
		}
	    }
	}
	return args.toArray(new String[]{});
    }

}
