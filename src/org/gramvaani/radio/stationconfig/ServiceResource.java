package org.gramvaani.radio.stationconfig;

public class ServiceResource {
    protected String resourceName;
    protected String resourceRole;
    
    public ServiceResource(String resourceName, String resourceRole) {
	this.resourceName = resourceName;
	this.resourceRole = resourceRole;
    }

    public String resourceName() {
	return resourceName;
    }

    public String resourceRole() {
	return resourceRole;
    }
}