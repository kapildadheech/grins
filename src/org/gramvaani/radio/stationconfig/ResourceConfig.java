package org.gramvaani.radio.stationconfig;

public class ResourceConfig {
    protected String resourceMixer;
    protected String resourceDeviceName;
    protected String resourceName;
    
    public ResourceConfig(String resourceMixer, String resourceDeviceName, String resourceName) {
	this.resourceMixer = resourceMixer;
	this.resourceDeviceName = resourceDeviceName;
	this.resourceName = resourceName;
    }

    public String resourceMixer() {
	return resourceMixer;
    }

    public String resourceDeviceName() {
	return resourceDeviceName;
    }

    public String resourceName() {
	return resourceName;
    }

}