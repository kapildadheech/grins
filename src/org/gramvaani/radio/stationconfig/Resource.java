package org.gramvaani.radio.stationconfig;

public class Resource {
    protected String resourceName;
    protected String machineID;

    public Resource(String resourceName, String machineID) {
	this.resourceName = resourceName;
	this.machineID = machineID;
    }

    public String resourceName() {
	return resourceName;
    }

    public String machineID() {
	return machineID;
    }
}