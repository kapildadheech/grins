package org.gramvaani.radio.stationconfig;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.gui.DiagnosticsWidget;
import org.gramvaani.radio.rscontroller.RSController;

import org.gramvaani.utilities.LogUtilities;

import java.util.*;

public class StationNetwork {

    StationConfiguration stationConfig;
    Hashtable<String, ArrayList<String>> serviceRoles;
    Hashtable<String, String> invServiceRoles;

    ArrayList<String> machines;
    LogUtilities logger;

    public final static String AUDIO_PLAYOUT = DiagnosticsWidget.PLAYOUT_OK;
    public final static String AUDIO_PREVIEW = DiagnosticsWidget.PREVIEW_OK;
    public final static String ARCHIVER = DiagnosticsWidget.ARCHIVER_OK;
    public final static String INDEX = DiagnosticsWidget.LIBRARY_OK;
    public final static String MIC = DiagnosticsWidget.MIC_OK;
    public final static String MONITOR = DiagnosticsWidget.MONITOR_REACHABLE;
    public final static String UPLOAD = DiagnosticsWidget.UPLOAD_OK;
    public final static String ONLINETELEPHONY = DiagnosticsWidget.ONLINETELEPHONY_OK;
    public final static String OFFLINETELEPHONY = DiagnosticsWidget.OFFLINETELEPHONY_OK;

    public final static String UI = "UI";
    
    public StationNetwork(StationConfiguration stationConfig) {
	serviceRoles = new Hashtable<String, ArrayList<String>>();
	invServiceRoles = new Hashtable<String, String>();
	logger = new LogUtilities("StationNetwork");

	Service[] services = stationConfig.getAllServices();


	for (Service service: services) {
	    if (serviceRoles.get(service.machineID()) == null) {
		serviceRoles.put(service.machineID(), new ArrayList<String>());
	    }
	    

	    // assuming that only audio service can have two instances
	    if (service.serviceType().equals(RSController.AUDIO_SERVICE)) {

		ServiceResource[] serviceResources = stationConfig.getServiceResources(service.serviceInstanceName());
		if (serviceResources != null && serviceResources.length > 0) {
		    if (serviceResources[0].resourceRole().equals(AudioService.PLAYOUT)) {
			serviceRoles.get(service.machineID()).add(AUDIO_PLAYOUT);
			invServiceRoles.put(AUDIO_PLAYOUT, service.machineID());
		    } else {
			serviceRoles.get(service.machineID()).add(AUDIO_PREVIEW);
			invServiceRoles.put(AUDIO_PREVIEW, service.machineID());
		    }
		}

	    } else if (service.serviceType().equals(RSController.ARCHIVER_SERVICE)) {

		serviceRoles.get(service.machineID()).add(ARCHIVER);
		invServiceRoles.put(ARCHIVER, service.machineID());

	    } else if (service.serviceType().equals(RSController.LIB_SERVICE)) {

		//		serviceInstanceRoles.put(service.serviceInstanceName(), LIBRARY);
		// XXX ignoring. because index service will take of library also. only exposing (lib + index + database) as a single entity to the user

	    } else if (service.serviceType().equals(RSController.INDEX_SERVICE)) {

		serviceRoles.get(service.machineID()).add(INDEX);
		invServiceRoles.put(INDEX, service.machineID());

	    } else if (service.serviceType().equals(RSController.MIC_SERVICE)) {

		serviceRoles.get(service.machineID()).add(MIC);
		invServiceRoles.put(MIC, service.machineID());

	    } else if (service.serviceType().equals(RSController.MONITOR_SERVICE)) {

		serviceRoles.get(service.machineID()).add(MONITOR);
		invServiceRoles.put(MONITOR, service.machineID());

	    } else if (service.serviceType().equals(RSController.SMS_SERVICE)) {

		// XXX ignoring.		serviceInstanceRoles.put(service.serviceInstanceName(), SMS);

	    } else if (service.serviceType().equals(RSController.ONLINE_TELEPHONY_SERVICE)) {
		/* XXX Enable this along with panel in diagnostics */
		serviceRoles.get(service.machineID()).add(ONLINETELEPHONY);
		invServiceRoles.put(ONLINETELEPHONY, service.machineID());
	       

	    } else if (service.serviceType().equals(RSController.OFFLINE_TELEPHONY_SERVICE)) {

		serviceRoles.get(service.machineID()).add(OFFLINETELEPHONY);
		invServiceRoles.put(OFFLINETELEPHONY, service.machineID());

	    } else if (service.serviceType().equals(RSController.UI_SERVICE)) {
		
		serviceRoles.get(service.machineID()).add(UI);
		invServiceRoles.put(UI, service.machineID());

	    }
	}

	if (serviceRoles.get(stationConfig.getStringParam(StationConfiguration.UPLOAD_MACHINE)) == null)
	    serviceRoles.put(stationConfig.getStringParam(StationConfiguration.UPLOAD_MACHINE), new ArrayList<String>());
	serviceRoles.get(stationConfig.getStringParam(StationConfiguration.UPLOAD_MACHINE)).add(UPLOAD);
	invServiceRoles.put(UPLOAD, stationConfig.getStringParam(StationConfiguration.UPLOAD_MACHINE));
	
	machines = new ArrayList<String>();
	for (String machineID: stationConfig.getMachines()) 
	    machines.add(machineID);

	this.stationConfig = stationConfig;
	initStores();
	initServiceIPs();
    }

    final static Hashtable<String, String> serviceIPs = new Hashtable<String, String>();
    final static Hashtable<String, HashSet<String>> ipMachineIDs = new Hashtable<String, HashSet<String>>();

    protected void initServiceIPs(){
	//ip, vm list

	for (String machine :machines){
	    String ip = stationConfig.getMachineIP(machine);
	    if (ipMachineIDs.get(ip) == null)
		ipMachineIDs.put(ip, new HashSet<String>());

	    HashSet<String> vmList = ipMachineIDs.get(ip);
	    vmList.add(machine);
	}

	for (String serviceName: RSController.SERVICES){
	    Service[] services = stationConfig.getServices(serviceName);
	    for (Service service: services){
		String machineID = service.machineID();
		String serviceInstanceName = service.serviceInstanceName();
		
		for (String ip: ipMachineIDs.keySet()){
		    if (ipMachineIDs.get(ip).contains(machineID)){
			serviceIPs.put(ip, serviceInstanceName);
			break;
		    }
		}
	    }
	}

    }

    public String[] getUniqueIPs() {
	return ipMachineIDs.keySet().toArray(new String[]{});
    }

    public ArrayList<String> getMachines() {
	return machines;
    }

    public int getNumMachines() {
	return machines.size();
    }

    public ArrayList<String> getServiceRolesOnMachine(String machineID) {
	ArrayList<String> roles = serviceRoles.get(machineID);
	if (roles != null)
	    return roles;
	else
	    return new ArrayList<String>();
    }

    public String getMachineID(String role) {
	return invServiceRoles.get(role);
    }

    public static final String[] stores = new String[] {StationNetwork.ARCHIVER, StationNetwork.UPLOAD, 
							StationNetwork.AUDIO_PLAYOUT, StationNetwork.AUDIO_PREVIEW, 
							StationNetwork.ONLINETELEPHONY};
    static final String[] producerStores = new String[] {StationNetwork.ARCHIVER, StationNetwork.UPLOAD, 
								StationNetwork.ONLINETELEPHONY};
    static final String[] consumerStores = new String[] {StationNetwork.AUDIO_PLAYOUT, StationNetwork.AUDIO_PREVIEW, 
								StationNetwork.UPLOAD};

    final Hashtable<String, String> storeMachines = new Hashtable<String, String>();
    final Hashtable<String, String> storeDirs = new Hashtable<String, String>();
    final HashSet<String> uniqueStores = new HashSet<String>();
    final static Hashtable<String, HashSet<String>> uniqueStoreSets = new Hashtable<String, HashSet<String>>();
    
    protected void initStores() {

	for (String store: stores) {
	    String machineID = getMachineID(store);
	    if (machineID != null){
		String machineIP = stationConfig.getMachineIP(machineID);
	
		if (machineIP != null)
		    storeMachines.put(store, machineIP);
	    }
	}
	
	storeDirs.put(StationNetwork.ARCHIVER, stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR));
	storeDirs.put(StationNetwork.UPLOAD, stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR));
	storeDirs.put(StationNetwork.AUDIO_PLAYOUT, stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR));
	storeDirs.put(StationNetwork.AUDIO_PREVIEW, stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR));
	storeDirs.put(StationNetwork.INDEX, stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR));
	storeDirs.put(StationNetwork.ONLINETELEPHONY, stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR));
	for (String store: stores)
	    uniqueStores.add(storeMachines.get(store)+storeDirs.get(store));

	prepareUniqueStoreSets();
	
    }

    protected void prepareUniqueStoreSets() {
	int i,j;
	boolean addedToSet;
	for (i = 0; i < stores.length; i++) {
	    addedToSet = false;
	    String curStore = stores[i];
	    for (j = 0; j < i; j++) {
		String prevStore = stores[j];
		if (isSameFileStore(curStore, prevStore)) {
		    HashSet<String> storeSet = uniqueStoreSets.get(prevStore);
		    storeSet.add(curStore);
		    uniqueStoreSets.put(curStore, uniqueStoreSets.get(prevStore));
		    addedToSet = true;
		    break;
		}
	    }
	    if (!addedToSet) {
		HashSet<String> storeSet = new HashSet<String>();
		storeSet.add(curStore);
		uniqueStoreSets.put(curStore, storeSet);
	    }
	}

    }
    
    public String[] getProducerServices() {
	ArrayList<String> producerServices = new ArrayList<String>();
	for (String store: producerStores) {
	    producerServices.add(getServiceInstanceForStore(store));
	}
	
	return producerServices.toArray(new String[producerServices.size()]);
    }

    public static String[] getProducerStores() {
	return producerStores;
    }

    public static String[] getConsumerStores() {
	return consumerStores;
    }

    public String[] getUniqueStores(String[] fileStores) {
	HashSet<String> storeSet = new HashSet<String>();
	int i,j;
	boolean addToSet;
	for (i = 0; i < fileStores.length; i++) {
	    addToSet = true;
	    String curStore = fileStores[i];
	    for (j = 0; j < i; j++) {
		String prevStore = fileStores[j];
		if (isSameFileStore(curStore, prevStore)) {
		    addToSet = false;
		    break;
		}
	    }
	    if (addToSet) {
		storeSet.add(curStore);
	    }
	}
	
	return storeSet.toArray(new String[] {});
    }

    public static String[] getSameStores(String store) {
	HashSet<String> set = uniqueStoreSets.get(store);
	if (store != null) {
	    return set.toArray(new String[]{});
	} else {
	    return new String[]{};
	}
    }

    public int getNumUniqueStores() {
	return uniqueStores.size();
    }

    public String[] getUniqueStores() {
	return uniqueStores.toArray(new String[uniqueStores.size()]);
    }
    
    public static String[] getStores() {
	return stores;
    }
  
    //Assuming that only one store exists per machine,
    public int getLocalStoreID(){
	int i = 0;
	String machineIP = stationConfig.getMachineIP(stationConfig.getLocalMachineID());

	for (String machine: storeMachines.values()){
	    if (machine.startsWith(machineIP))
		break;
	    i++;
	}
	return i;
    }

    //Returns a store present on the specified machine
    public String getAStoreNameOnMachine(String machineIP) {
	for (String store : stores) {
	    String storeIP = getStoreMachine(store);
	    if (storeIP.equals(machineIP))
		return store;
	}
	return "";
    }

    public String getALocalStoreName() {
	String localIP = stationConfig.getMachineIP(stationConfig.getLocalMachineID());
	return getAStoreNameOnMachine(localIP);
    }

    public String getStoreMachine(String store) {
	return storeMachines.get(store);
    }

    public String getStoreDir(String store) {
	return storeDirs.get(store);
    }
    
    public String getLocalStoreDir() {
	String localIP = stationConfig.getMachineIP(stationConfig.getLocalMachineID());
	for (String store: storeMachines.keySet()){
	    String storeIP = storeMachines.get(store);
	    if (storeIP.startsWith(localIP))
		return getStoreDir(store);
	}
	return null;
    }

    public boolean isSameFileStore(String store1, String store2) {
	String uniqueStore1 = storeMachines.get(store1)+storeDirs.get(store1);
	String uniqueStore2 = storeMachines.get(store2)+storeDirs.get(store2);
	
	return uniqueStore1.equals(uniqueStore2);
    }

    public boolean isLocalStore(String store) {
	String localIP = stationConfig.getMachineIP(stationConfig.getLocalMachineID());
	String storeIP = getStoreMachine(store);

	return localIP.equals(storeIP);
    }

    public boolean isStoreLocalToUploader(String store, String uploaderName) {
	String uploaderIP;
	String storeIP;
	String serviceName;

	if (uploaderName.equals(RSApp.MEDIALIB_UPLOAD_PROVIDER)){
	    serviceName = RSController.UI_SERVICE;
	} else {
	    serviceName = uploaderName;
	} 
	    
	uploaderIP = stationConfig.getServiceIP(serviceName);
	storeIP = getStoreMachine(store);
	return storeIP.equals(uploaderIP);
    }

    protected String getServiceInstanceForStore(String store) {
	if (store.equals(AUDIO_PLAYOUT)) {
	    return stationConfig.getProviderServiceInstance(RSApp.PLAYOUT_PROVIDER, AudioService.PLAYOUT);
	} else if (store.equals(AUDIO_PREVIEW)) {
	    return stationConfig.getProviderServiceInstance(RSApp.PREVIEW_PROVIDER, AudioService.PREVIEW);
	} else if (store.equals(ARCHIVER)) {
	    return stationConfig.getProviderServiceInstance(RSApp.ARCHIVER_PROVIDER, ArchiverService.BCAST_MIC);
	} else if (store.equals(ONLINETELEPHONY)) {
	    return stationConfig.getProviderServiceInstance(RSApp.TELEPHONY_PROVIDER, OnlineTelephonyService.ONLINE_TELEPHONY);
	} else if (store.equals(UPLOAD)) {
	    return RSController.LIB_SERVICE;
	} else {
	    return null;
	}
    }

    public String getServletURL(String store) {
	String serviceInstance = "";
	String machineID = "";
	
	if (store == null || store.equals("")){
	    logger.error("GetServletURL: looking up null servlet.");
	    return null;
	}
	
	if (store.equals(UPLOAD)) {
	    machineID = stationConfig.getServiceInstanceMachineID(RSController.LIB_SERVICE, RSController.LIB_SERVICE);
	} else {
	    serviceInstance = getServiceInstanceForStore(store);
	    machineID = stationConfig.getServiceMachineID(serviceInstance);
	}

	
	String machineIP = stationConfig.getMachineIP(machineID);
	String servletPort = stationConfig.getStringParam(StationConfiguration.SERVLET_PORT);

	return "http://"+machineIP+":"+servletPort+"/gramvaani/MediaLibUploadServlet";
    }

    public String getServiceForIP(String machineIP){
	return serviceIPs.get(machineIP);
    }
    
}