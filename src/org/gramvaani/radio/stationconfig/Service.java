package org.gramvaani.radio.stationconfig;

public class Service {
    protected String serviceClass;
    protected String machineID;
    protected String serviceInstanceName;
    protected String serviceType;

    public Service(String serviceClass, String machineID, String serviceInstanceName, String serviceType) {
	this.serviceClass = serviceClass;
	this.machineID = machineID;
	this.serviceInstanceName = serviceInstanceName;
	this.serviceType = serviceType;
    }

    public String serviceClass() {
	return serviceClass;
    }

    public String machineID() {
	return machineID;
    }

    public String serviceInstanceName() {
	return serviceInstanceName;
    }
    
    public String serviceType() {
	return serviceType;
    }

    public String toString(){
	return serviceInstanceName();
    }
}
