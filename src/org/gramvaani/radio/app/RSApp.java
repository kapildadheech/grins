package org.gramvaani.radio.app;

import org.gramvaani.radio.app.providers.RSProviderBase;
import org.gramvaani.radio.app.providers.RSProvider;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.app.gui.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.medialib.MediaLib;
import org.gramvaani.utilities.*;
import org.gramvaani.utilities.singleinstance.*;

import org.gramvaani.radio.tests.RSTestController;

import java.util.*;
import java.lang.reflect.*;
import javax.swing.SwingUtilities;

import gnu.getopt.Getopt;

public class RSApp {

    public static final String ARCHIVER_PROVIDER 	= "ARCHIVER_PROVIDER";
    public static final String CACHE_PROVIDER 	        = "CACHE_PROVIDER";
    public static final String LIBRARY_PROVIDER 	= "LIBRARY_PROVIDER";
    public static final String MEDIALIB_UPLOAD_PROVIDER = "MEDIALIB_UPLOAD_PROVIDER";
    public static final String METADATA_PROVIDER 	= "METADATA_PROVIDER";
    public static final String MIC_PROVIDER 		= "MIC_PROVIDER";
    public static final String MONITOR_PROVIDER 	= "MONITOR_PROVIDER";
    public static final String PLAYOUT_PROVIDER 	= "PLAYOUT_PROVIDER";
    public static final String PREVIEW_PROVIDER 	= "PREVIEW_PROVIDER";
    public static final String STREAMING_PROVIDER	= "STREAMING_PROVIDER";
    public static final String TELEPHONY_PROVIDER 	= "TELEPHONY_PROVIDER";
    public static final String SMS_PROVIDER 	        = "SMS_PROVIDER";

    public static final String[] PROVIDERS = new String[]{ARCHIVER_PROVIDER, CACHE_PROVIDER, LIBRARY_PROVIDER, 
							  MEDIALIB_UPLOAD_PROVIDER, METADATA_PROVIDER, 
							  MIC_PROVIDER, MONITOR_PROVIDER, PLAYOUT_PROVIDER, 
							  PREVIEW_PROVIDER, STREAMING_PROVIDER, TELEPHONY_PROVIDER, 
							  SMS_PROVIDER};
    
    protected UIService uiService;
    protected String machineID;
    StationConfiguration stationConfiguration;
    Hashtable<String,RSProvider> providers;
    Hashtable<Class<? extends RSProvider>, RSProvider> providerClasses;
    
    protected LogUtilities logger;
    protected ControlPanel controlPanel;

    PlaylistWidget playlistWidget;

    public RSApp(StationConfiguration stationConfiguration, String machineID, boolean runTests, String testFilename) {
	this.stationConfiguration = stationConfiguration;
	this.machineID = machineID;
	providers = new Hashtable<String,RSProvider>();
	providerClasses = new Hashtable<Class<? extends RSProvider>, RSProvider>();

	uiService = new UIService(stationConfiguration, machineID);
	logger = new LogUtilities ("RSApp");

	initProviders();
	uiService.registerService();
	activateProviders();

	logger.debug("Initializing with machineID: "+this.machineID+" and runTests: "+runTests);

	if (runTests) {
	    startTestController(stationConfiguration, machineID, providers.values(), uiService, testFilename);
	} else {
	    launchGui();
	}

	SingleInstanceManager.setSingleInstanceListener(new SingleInstanceManager.SingleInstanceListener() {
		public void newInstanceCreated() {
		    newInstanceDetected();
		}
	    });
	
	/*
	Thread t = new Thread() {
		public void run() {
		    while(true) {
			try{
			    Thread.sleep(10000);
			} catch(Exception e) {}

			Runtime runtime = Runtime.getRuntime();  
			logger.info("MEM_STATS: MAX:" + runtime.maxMemory() / 1024 + " ALLOC:" + runtime.totalMemory()/1024 + " FREE:" + runtime.freeMemory()/1024);
		    }
		}
	    };
	t.start();
	*/
    }

    void launchGui(){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    controlPanel = new ControlPanel(logger, RSApp.this);
		    controlPanel.initPlaylistWidget(playlistWidget = new PlaylistWidget(controlPanel, RSApp.this));
		    controlPanel.setDefaultWidget(new BlankWidget("Default", RSApp.this, controlPanel));
		    //controlPanel.setDefaultWidget(new LogViewWidget("Default", RSApp.this, controlPanel));
		    MediaLib medialib = MediaLib.getMediaLib(stationConfiguration, uiService.getTimeKeeper());
		    if (medialib != null) {
			medialib.setIPCServer(uiService.getIPCServer());
		    } else {
			logger.error("Unable to initialize library, and hence the Hotkeys and LogView widgets");
		    }
		    
		    controlPanel.initWidget(new HotkeyWidget(RSApp.this));
		    controlPanel.initWidget(new LibraryWidget(RSApp.this));
		    controlPanel.initWidget(new UploadMediaWidget(RSApp.this, controlPanel));
		    controlPanel.initWidget(new TelephonyWidget(RSApp.this));
		    controlPanel.initWidget(new SMSWidget(RSApp.this));
		    controlPanel.initWidget(new PollWidget(RSApp.this));
		    controlPanel.initWidget(new ContactsWidget(RSApp.this));
		    //controlPanel.initWidget(new DiagnosticsWidget(RSApp.this));
		    controlPanel.initWidget(new LogViewWidget("Default", RSApp.this, controlPanel));
		    controlPanel.initWidget(new AdminWidget(RSApp.this));	    
		    controlPanel.initWidget(new StreamingWidget(RSApp.this));
		    
		    controlPanel.display();
		}
	    });
    }

    public RSProvider getProvider(String providerName) {
	return providers.get(providerName);
    }
    
    @SuppressWarnings("unchecked")
    public <T extends RSProvider> T getProvider(Class<T> providerClass){
	return (T) providerClasses.get(providerClass);
    }

    public ControlPanel getControlPanel() {
	return controlPanel;
    }

    public UIService getUIService() {
	return uiService;
    }

    public StationConfiguration getStationConfiguration() {
	return stationConfiguration;
    }
    
    protected void initProviders() {
	for (String PROVIDER: PROVIDERS) {
	    String providerClassName = stationConfiguration.getProviderClassName(PROVIDER);
	    if (providerClassName != null) {
		RSProvider provider = null;
		Class<?> providerClass = null;
		try {
		    providerClass = Class.forName(providerClassName);
		    Constructor providerConstructor = providerClass.getConstructor(UIService.class);
		    provider = (RSProvider)(providerConstructor.newInstance(new Object[]{uiService}));
		    providers.put(PROVIDER, provider);
		    providerClasses.put(provider.getClass(), provider);
		} catch(Exception e) {
		    logger.error("InitProviders: Could not instantiate service " + PROVIDER, e);
		}
		
	    } else {
		logger.error("InitProviders: Could not activate provider: "+PROVIDER+" since class name is null.");
	    }
	}
    }
   
    protected void activateProviders(){
	for (RSProvider provider: providers.values()) {
	    try{
		logger.debug("ActivateProviders: Activating: "+provider.getName());
		provider.activate();
	    } catch (Exception e){
		logger.error("ActivateProviders: Encountered exception while activating provider: "+provider.getName(), e);
	    }
	}
    }

    void startTestController(StationConfiguration stationConfig, String machineID, Collection<RSProvider> providers, UIService uiService, String testFilename){

	Hashtable<String, Object> env = new Hashtable<String, Object> ();
	env.put(RSTestController.STATION_CONFIGURATION, stationConfig);
	env.put(RSTestController.MACHINE_ID, machineID);
	env.put(RSTestController.UI_SERVICE, uiService);
	env.put(RSTestController.RS_APP, this);

	for (RSProvider provider: providers)
	    env.put(provider.getName(), provider);

	RSTestController testController = new RSTestController(env, testFilename);
	testController.start();
    }

    protected void newInstanceDetected(){
	controlPanel.bringToFront();
    }

    public PlaylistWidget getPlaylistWidget(){
	return playlistWidget;
    }

    public static void main(String args[]) {
	Getopt g = new Getopt("RSapp", args, "c:m:t::");
	int c;
	String configFilePath = "./automation.conf";
	String machineID = "127.0.0.1";
	boolean runTests = false;
	String testFilename = null;
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    case 'm':
		machineID = g.getOptarg();
		break;
	    case 't':
		testFilename = g.getOptarg();
		if (testFilename == null)
		    testFilename = "test.conf";
		runTests = true;
		break;
	    }
	}

	//LogUtilities.addShutdownHook();

	StationConfiguration stationConfiguration = new StationConfiguration(machineID);
	stationConfiguration.readConfig(configFilePath);
	
	if (!SingleInstanceManager.registerInstance(RSController.RS_APP)) {
	    LogUtilities.getDefaultLogger().fatal("Another instance of this application is already running.  Exiting.");
	    System.exit(RSController.MULTIPLE_INSTANCE_EXIT_CODE);
	}

	LogUtilities.logToFile(RSController.RS_APP, stationConfiguration.getStringParam(StationConfiguration.LOG_FILE_SIZE));

	RSApp rsApp = new RSApp(stationConfiguration, machineID, runTests, testFilename);
    }
    
}

