package org.gramvaani.radio.app.gui;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class RSButton extends JButton {
    LineBorder normalBorder, pressedBorder;
    ButtonModel model;
    Color normalBgColor;
    Color disabledBgColor;
    
    public RSButton(Icon icon) {
	super(icon);
	normalBorder = new LineBorder(Color.lightGray, 1);
	pressedBorder = new LineBorder(Color.red, 1);
	//setBorder(normalBorder);
	normalBgColor = getBackground();
	disabledBgColor = Color.decode("0xdcdcdc");
    }
    
    public RSButton(String str) {
	super(str);
	normalBorder = new LineBorder(Color.lightGray, 1);
	pressedBorder = new LineBorder(Color.white, 1);
	//setBorder(normalBorder);
	model = getModel();
	normalBgColor = getBackground();
	disabledBgColor = Color.decode("0xdcdcdc");
    }

    public RSButton(String str, Icon icon){
	super(str, icon);
	normalBorder = new LineBorder(Color.lightGray, 1);
	pressedBorder = new LineBorder(Color.white, 1);
	//setBorder(normalBorder);
	model = getModel();
	normalBgColor = getBackground();
	disabledBgColor = Color.decode("0xdcdcdc");
    }
    
    public void forceEnabled(boolean enabled) {
	super.setEnabled(enabled);
    }

    public void setEnabled(boolean enabled) {
	super.setEnabled(enabled);
	/*
	if(!enabled)
	    super.setBackground(disabledBgColor);
	else
	    super.setBackground(normalBgColor);
	*/
    }

    public void setBackground(Color color) {
	normalBgColor = color;
	super.setBackground(color);
    }

    public void setPressed(boolean pressed) {
	setSelected(pressed);
	model.setArmed(pressed);
	model.setPressed(pressed);
	model.setSelected(pressed);
	setForeground(Color.white);
    }
    
    public boolean isPressed(){
	return model.isPressed();
    }

}