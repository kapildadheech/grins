package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.app.providers.MediaLibUploadProvider;
import org.gramvaani.radio.medialib.FileStoreUploader;
import org.gramvaani.radio.rscontroller.pipeline.RSPipeline;
import org.gramvaani.radio.stationconfig.StationNetwork;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.medialib.IndexLib;
import org.gramvaani.radio.rscontroller.services.OnlineTelephonyService;

import java.util.*;

public class ErrorInference {
    
    static StationNetwork stationNetwork;
    static StationConfiguration stationConfig;

    public static void initStationNetwork(StationNetwork stnNw) {
	stationNetwork = stnNw;
    }

    public static void initStationConfig(StationConfiguration stationCfg) {
	stationConfig = stationCfg;
    }

    public static String[] getMetadataErrorString(String prefix, String[] error) {
	String shortError = "";
	String diagnosticsErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	errorStr.append(prefix);
	errorStr.append(": ");

	if (error.length == 0)
	    return new String[]{"", "", errorStr.toString()};
	else {
	    String errorCode = error[0];
	    if (errorCode.equals(MetadataWidget.UNABLE_TO_ADD_ITEM)) {
		errorStr.append(": Unable to add to index and/or database. \nPlease run a detailed diagnostics to trace the problem");
		shortError = "Possible network error";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else {
		shortError = "";
		diagnosticsErrorCode = "";
	    }

	    return new String[]{diagnosticsErrorCode, shortError, errorStr.toString()};
	}
    }

    public static String[] getIndexLibErrorString(String prefix, String[] error) {
	String shortError = "";
	String diagnosticsErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	errorStr.append(prefix);
	errorStr.append(": ");
	if (error.length == 0)
	    return new String[]{"", "", errorStr.toString()};
	else {
	    String errorCode = error[0];
	    if (errorCode.equals(IndexLib.ERROR_NO_SPACE)) {
		errorStr.append(": No space left on disk. \nPlease run a detailed diagnostics to verify");
		shortError = "Disk full error";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(IndexLib.ERROR_UNKNOWN)) {
		errorStr.append(": Unknown error. \nPlease run a detailed diagnostics to identify the source of the problem");
		shortError = "Unknown error";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;		
	    } else {
		shortError = "";
		diagnosticsErrorCode = "";
	    }

	    return new String[]{diagnosticsErrorCode, shortError, errorStr.toString()};
	}
    }


    public static String[] getDiagnosticsErrorString(String prefix, String[] error){
	String shortError = "";
	String diagnosticsErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	errorStr.append(prefix);
	errorStr.append(": ");
	if (error.length == 0)
	    return new String[] {"", "", errorStr.toString()};
	else {
	    String errorCode = error[0];
	    if (errorCode.equals(DiagnosticsController.ERROR_UNMATCHED_FREQUENCY)){
		errorStr.append(": Output frequency does not match input");
		shortError = "Unmatched frequencies";
		diagnosticsErrorCode = DiagnosticsController.ERROR_UNMATCHED_FREQUENCY;
	    }
	    
	    return new String[] {diagnosticsErrorCode, shortError, errorStr.toString()};
	}
    }


    public static String[] getLibraryErrorString(String prefix, String[] error) {
	String shortError = "";
	String diagnosticsErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	errorStr.append(prefix);
	errorStr.append(": ");
	if (error.length == 0)
	    return new String[]{"", "", errorStr.toString()};
	else {
	    String errorCode = error[0];
	    if (errorCode.equals(LibraryController.UNABLE_TO_SEND_COMMAND)) {
		errorStr.append(": Unable to send command. \nPlease check your network connections");
		shortError = "Network error";
		diagnosticsErrorCode = DiagnosticsController.RSC_NETWORK_ERROR;
	    } else if (errorCode.equals(LibraryController.UNABLE_TO_PLAY)) {
		errorStr.append(": Unable to play. \nPlease run detailed diagnostics to check the audio service");
		shortError = "Unable to play";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(LibraryController.UNABLE_TO_STOP)) {
		errorStr.append(": Unable to stop. \nPlease run detailed diagnostics to check the audio service");
		shortError = "Unable to stop";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(PlaylistController.UNKNOWN_ERROR)) {
		errorStr.append(": Unknown error. \nPlease run detailed diagnostics to check the system health");
		shortError = "Unknown error";
		diagnosticsErrorCode = DiagnosticsController.DEPENDENT_ERROR;
	    } else {
		shortError = "";
		diagnosticsErrorCode = "";
	    }

	    return new String[]{diagnosticsErrorCode, shortError, errorStr.toString()};
	}
    }

    public static String[] getPlaylistErrorString(String prefix, String[] error) {
	String shortError = "";
	String diagnosticsErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	errorStr.append(prefix);
	errorStr.append(": ");
	if (error.length == 0)
	    return new String[]{"", "", errorStr.toString()};
	else {
	    String errorCode = error[0];
	    if (errorCode.equals(PlaylistController.UNABLE_TO_SEND_COMMAND)) {
		errorStr.append(": Unable to send command. \nPlease check your network connections");
		shortError = "Network error";
		diagnosticsErrorCode = DiagnosticsController.RSC_NETWORK_ERROR;
	    } else if (errorCode.equals(PlaylistController.UNABLE_TO_PLAY)) {
		errorStr.append(": Unable to play. \nPlease run detailed diagnostics to check the audio service");
		shortError = "Unable to play";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(PlaylistController.UNABLE_TO_PAUSE)) {
		errorStr.append(": Unable to pause. \nPlease run detailed diagnostics to check the audio service");
		shortError = "Unable to pause";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(PlaylistController.UNABLE_TO_STOP)) {
		errorStr.append(": Unable to stop. \nPlease run detailed diagnostics to check the audio service");
		shortError = "Unable to stop";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(PlaylistController.UNABLE_TO_SAVE)) {
		errorStr.append(": Unable to save. \nPlease run detailed diagnostics to check the library service");
		shortError = "Unable to save";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(PlaylistController.UNKNOWN_ERROR)) {
		errorStr.append(": Unknown error. \nPlease run detailed diagnostics to check the system health");
		shortError = "Unknown error";
		diagnosticsErrorCode = DiagnosticsController.DEPENDENT_ERROR;
	    } else {
		shortError = "";
		diagnosticsErrorCode = "";
	    }

	    return new String[]{diagnosticsErrorCode, shortError, errorStr.toString()};
	}
    }

    public static String[] getUploadErrorString(String prefix, String[] error) {
	String shortError = "";
	String diagnosticsErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	errorStr.append(prefix);
	errorStr.append(": ");
	if (error.length == 0)
	    return new String[]{"", "", errorStr.toString()};
	else {
	    String errorCode = error[0];
	    if (errorCode.equals(MediaLibUploadProvider.ERROR_FILE_PATH)) {

		errorStr.append(": File not found. \nPlease contact a Gram Vaani engineer to fix your config file");
		shortError = "Test file not found for upload";
		diagnosticsErrorCode = DiagnosticsController.CONFIG_FILE_ERROR;
	    } else if (errorCode.equals(MediaLibUploadProvider.ERROR_NO_SPACE)) {

		errorStr.append(": Your library disk is full. \nPlease use a new box and contact a Gram Vaani engineer to migrate your data");
		shortError = "Disk full";
		diagnosticsErrorCode = DiagnosticsController.DISK_WRITE_ERROR;
	    } else if (errorCode.equals(MediaLibUploadProvider.ERROR_NOT_WRITABLE)) {

		errorStr.append(": Your library disk is either full or corrupt. \nPlease use a new box and contact a Gram Vaani engineer to migrate your data");
		shortError = "Disk is probably corrupt";
		diagnosticsErrorCode = DiagnosticsController.DISK_WRITE_ERROR;		
	    } else if (errorCode.equals(MediaLibUploadProvider.ERROR_UNKNOWN)) {

		errorStr.append(": Unknown error. \nPlease restart the system if the error persists.");
		shortError = "Unknown error";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;		
	    } else if (errorCode.equals(MediaLibUploadProvider.ERROR_NOT_READY)) {

		errorStr.append(": Service not ready. \nPlease try again. Plese restart the system if the problem persists.");
		shortError = "Upload service not ready";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;		
	    } else if (errorCode.equals(MediaLibUploadProvider.ERROR_MAX_EXCEEDED)) {

		errorStr.append(": The file is too large. \nPlease contact a Gram Vaani engineer to fix your config file.");
		shortError = "The file is too large";
		diagnosticsErrorCode = DiagnosticsController.CONFIG_FILE_ERROR;
	    } else if (errorCode.equals(MediaLibUploadProvider.ERROR_DB_FAILED)) {

		errorStr.append(": There appears to be a problem with your network connections to " + stationNetwork.getMachineID(StationNetwork.INDEX));
		shortError = "Database is not reachable";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_NETWORK_ERROR;

	    } else if (errorCode.equals(RSController.ERROR_SERVLET_CONNECTION_FAILED)) {
		
		errorStr.append(": Servlet is down. \nPlease restart the system if the problem persists");
		shortError = "Upload service is down";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_DOWN_ERROR;		
	    } else if (errorCode.equals(MediaLibUploadProvider.ERROR_NOT_SUPPORTED)) {
		errorStr.append(" This file type is not supported at the moment.");
		shortError = "Unsupported filetype";
		//diagnosticsErrorCode = DiagnosticsController.
		diagnosticsErrorCode = "";
	    } else {
		diagnosticsErrorCode = "";
		shortError = "";
	    }
	    
	    return new String[]{diagnosticsErrorCode, shortError, errorStr.toString()};
	}
    }

    public static String[] getTelephonyErrorString(String prefix, String[] error){
	String shortError = "";
	String telephonyErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	
	errorStr.append(prefix);
	errorStr.append(": ");
	
	if (error.length == 0)
	    return new String[] {"", "", errorStr.toString()};
	
	String errorCode = error[0];
	
	if (errorCode.equals(OnlineTelephonyService.ERROR_PBX_UNREACHABLE)){
	    shortError = "Unable to connect to PBX software";
	    errorStr.append(": Either PBX software is not running or there is a problem with your network connections to "+stationNetwork.getMachineID(StationNetwork.ONLINETELEPHONY));
	    telephonyErrorCode = DiagnosticsController.SERVICE_DOWN_ERROR;
	}

	return new String[] {telephonyErrorCode, shortError, errorStr.toString()};
    }

    public static String[] getGstErrorString(String prefix, String errorCode) {
	String shortError = "";
	String diagnosticsErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	errorStr.append(prefix);
	errorStr.append(": ");
	if (errorCode == null || errorCode.equals("")) {
	    return new String[]{"", "", errorStr.toString()};
	} else {
	    if (errorCode.equals(RSPipeline.ERROR_NO_DATA)) {
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
		shortError = prefix;
		errorStr.append(RSPipeline.ERROR_NO_DATA_STRING);
	    } else if (errorCode.equals(RSPipeline.ERROR_DEVICE_BUSY)) {
		diagnosticsErrorCode = DiagnosticsController.SERVICE_RESOURCE_ERROR;
		shortError = "Resource is busy";
		errorStr.append(RSPipeline.ERROR_DEVICE_BUSY_STRING);
		errorStr.append(": ");
		errorStr.append("Please make sure that all audio playout is stopped to run diagnostics");
	    } else if (errorCode.equals(RSPipeline.ERROR_DEVICE_CANT_OPEN)) {
		diagnosticsErrorCode = DiagnosticsController.SERVICE_RESOURCE_ERROR;
		shortError = "Resouce is busy or not installed correctly";
		errorStr.append(RSPipeline.ERROR_DEVICE_CANT_OPEN_STRING);
		errorStr.append(": ");
		errorStr.append("Please check if your soundcard is inserted properly, \nand close all audio playout to run diagnostics");
	    } else if (errorCode.equals(RSPipeline.ERROR_UNKNOWN_FORMAT)) {
		diagnosticsErrorCode = DiagnosticsController.CONFIG_FILE_ERROR;
		shortError = "Test file of unknown format";
		errorStr.append(RSPipeline.ERROR_UNKNOWN_FORMAT_STRING);
		errorStr.append(": ");
		errorStr.append("Please consult a Gram Vaani engineer to fix your config file");
	    } else if (errorCode.equals(RSPipeline.ERROR_FILE_OPEN)) {
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
		shortError = "Could not open file";
		errorStr.append(RSPipeline.ERROR_FILE_OPEN_STRING);
		errorStr.append(": ");
		errorStr.append("Please close all audio playout to run diagnostics. \nPlease restart the system if the problem persists");
	    } else if (errorCode.equals(RSPipeline.ERROR_DEVICE_FAULT)) {
		diagnosticsErrorCode = DiagnosticsController.SERVICE_RESOURCE_ERROR;
		shortError = "Resource is busy or not installed properly";
		errorStr.append(RSPipeline.ERROR_DEVICE_FAULT_STRING);
		errorStr.append(": ");
		errorStr.append("Please check if your soundcard is inserted properly, \nand close all audio playout to run diagnostics");
	    } else if (errorCode.equals(RSPipeline.ERROR_REC_DEVICE_CANT_OPEN)) {
		diagnosticsErrorCode = DiagnosticsController.SERVICE_RESOURCE_ERROR;
		shortError = "Resource is busy or not installed properly";
		errorStr.append(RSPipeline.ERROR_REC_DEVICE_CANT_OPEN_STRING);
		errorStr.append(": ");
		errorStr.append("Please check if your soundcard is inserted properly, \nand close all audio playout/recording to run diagnostics");
	    } else if (errorCode.equals(RSPipeline.ERROR_REC_DEVICE_BUSY)) {
		diagnosticsErrorCode = DiagnosticsController.SERVICE_RESOURCE_ERROR;
		shortError = "Resouce is busy";
		errorStr.append(RSPipeline.ERROR_REC_DEVICE_BUSY_STRING);
		errorStr.append(": ");
		errorStr.append("Please close all audio playout and recording to run diagnostics");
	    } else if (errorCode.equals(RSPipeline.ERROR_NO_SPACE)) {
		diagnosticsErrorCode = DiagnosticsController.DISK_WRITE_ERROR;
		shortError = "Disk full";
		errorStr.append(RSPipeline.ERROR_NO_SPACE_STRING);
		errorStr.append(": ");
		errorStr.append("Your library disk is full. \nPlease change your box and contact a Gram Vaani engineer to migrate your data");
	    } else if (errorCode.equals(RSPipeline.ERROR_NO_FILE)) {
		diagnosticsErrorCode = DiagnosticsController.CONFIG_FILE_ERROR;
		shortError = "File not found";
		errorStr.append(RSPipeline.ERROR_NO_FILE_STRING);
		errorStr.append(": ");
		errorStr.append("If the problem occurs with other files too, most likely your config file contains errors.");
	    } else {
		errorStr.append(": " + errorCode);
		shortError = "";
		diagnosticsErrorCode = "";
	    }
	    
	    return new String[]{diagnosticsErrorCode, shortError, errorStr.toString()};
	}
    }

    public static String[] getServiceErrorString(String prefix, String[] errorParams) {
	String shortError = "";
	String diagnosticsErrorCode = "";
	StringBuilder errorStr = new StringBuilder();
	errorStr.append(prefix);
	errorStr.append(": ");
	if (errorParams.length == 0)
	    return new String[]{"", "", errorStr.toString()};
	else {
	    String errorCode = errorParams[0];
	    if (errorCode.equals(RSController.ERROR_WRONG_STATE)) {
		errorStr.append(": Incorrect internal state. \nPlease restart the system if this problem persists");
		shortError = "Wrong internal state";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_RESOURCE_ACK_FAILED)) {
		errorStr.append(": Resource is busy. \nPlease make sure that all audio playout is stopped to run diagnostics");
		shortError = "Resource is busy";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_RESOURCE_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_RESOURCE_NOT_FREE)) {
		errorStr.append(": Resource is busy. \nPlease make sure that all audio playout is stopped to run diagnostics");
		shortError = "Resource is busy";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_RESOURCE_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_RESOURCE_ROLE_NOT_FOUND)) {
		errorStr.append(": Role not found. \nPlease consult a Gram Vaani engineer to fix your configuration file");
		shortError = "Role not found";
		diagnosticsErrorCode = DiagnosticsController.CONFIG_FILE_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_DB_FAILED)) {
		errorStr.append(": The database seems to be down. \nPlease restart the system if this problem persists");
		shortError = "Database down";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_NETWORK_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_IDLE_MONITOR_FAILED)) {
		errorStr.append(": Idle time detection failed. \nYour files will not be encoded. Please contact a Gram Vaani engineer");
		shortError = "Idle time detection failed";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_INVALID_PREEMT_REQUEST)) {
		errorStr.append(": Invalid preempt request. \nPlease restart the system if this problem persists, and contact a Gram Vaani engineer");
		shortError = "Invalid preempt request";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_INTERNAL_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_NO_SPACE)) {
		errorStr.append(": Your library disk space is full. \nPlease use a new box and contact a Gram Vaani engineer to migrate your database");
		shortError = "Disk full";
		diagnosticsErrorCode = DiagnosticsController.DISK_WRITE_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_NOT_WRITABLE)) {
		errorStr.append(": Your disk is not writable. \nLikely cause is a corrupt hard disk. Please use a new box");
		shortError = "Disk not writable";
		diagnosticsErrorCode = DiagnosticsController.DISK_WRITE_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_SERVICE_DISCONNECTED)) {
		errorStr.append(": Service is disconnected. \nPlease check your network connections to the service");
		shortError = "Service is disconnected";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_NETWORK_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_SERVICE_DOWN)) {
		errorStr.append(": Service is down. \nPlease check your network connections to the service");
		shortError = "Service is down";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_DOWN_ERROR;
	    } else if (errorCode.equals(RSController.ERROR_SERVLET_CONNECTION_FAILED)) {
		errorStr.append(": Service is down. \nPlease restart the system if the problem persists");
		shortError = "Upload service is down";
		diagnosticsErrorCode = DiagnosticsController.SERVICE_DOWN_ERROR;		
	    } else {
		errorStr.append(": " + errorParams[0]);
		shortError = "";
		diagnosticsErrorCode = "";
	    }

	    return new String[]{diagnosticsErrorCode, shortError, errorStr.toString()};
	}
    }


    Hashtable<String, ErrorDisplayListener> errorDisplayListeners;
    RecentErrorList recentErrorList;
    
    public ErrorInference(StationConfiguration stationCfg) {
	stationConfig = stationCfg;
	stationNetwork = stationConfig.getStationNetwork();
	errorDisplayListeners = new Hashtable<String, ErrorDisplayListener>();
	recentErrorList = new RecentErrorList();
    }

    public void registerErrorDisplay(ErrorDisplayListener listener) {
	errorDisplayListeners.put(listener.getName(), listener);
    }

    protected void dispatchErrorToUI(ErrorDescriptor error) {
	if (recentErrorList.pushError(error))
	    for (ErrorDisplayListener listener: errorDisplayListeners.values())
		listener.displayError(error.longError());
    }

    public void displayServiceError(String prefix, String[] errors) {
	String[] a = getServiceErrorString(prefix, errors);
	dispatchErrorToUI(new ErrorDescriptor(a[0], a[1], a[2]));
    }

    public void displayGstError(String prefix, String errors) {
	String[] a = getGstErrorString(prefix, errors);
	dispatchErrorToUI(new ErrorDescriptor(a[0], a[1], a[2]));
    }

    public void displayUploadError(String prefix, String[] errors) {
	String[] a = getUploadErrorString(prefix, errors);
	dispatchErrorToUI(new ErrorDescriptor(a[0], a[1], a[2]));
    }

    public void displayPlaylistError(String prefix, String[] errors) {
	String[] a = getPlaylistErrorString(prefix, errors);
	dispatchErrorToUI(new ErrorDescriptor(a[0], a[1], a[2]));
    }

    public void displayLibraryError(String prefix, String[] errors) {
	String[] a = getLibraryErrorString(prefix, errors);
	dispatchErrorToUI(new ErrorDescriptor(a[0], a[1], a[2]));
    }

    public void displayMetadataError(String prefix, String[] errors) {
	String[] a = getMetadataErrorString(prefix, errors);
	dispatchErrorToUI(new ErrorDescriptor(a[0], a[1], a[2]));
    }

    public void displayIndexLibError(String prefix, String[] errors) {
	String[] a = getIndexLibErrorString(prefix, errors);
	dispatchErrorToUI(new ErrorDescriptor(a[0], a[1], a[2]));
    }

    public void displayServiceError(String prefix, String errorCode, String[] errors) {
	String[] a = new String[errors.length + 1];
	for (int i = 0; i < errors.length; i++)
	    a[i + 1] = errors[i];
	a[0] = errorCode;
	displayServiceError(prefix, a);
    }

    static ErrorInference errorInference;
    public static ErrorInference getErrorInference() {
	return errorInference;
    }
    public static ErrorInference getErrorInference(StationConfiguration stationConfig) {
	if (errorInference == null)
	    errorInference = new ErrorInference(stationConfig);
	return errorInference;
    }


    public class ErrorDescriptor {

	String errorCode, shortError, longError;
	long errorTimeInstant;

	public ErrorDescriptor(String errorCode, String shortError, String longError) {
	    this.errorCode = errorCode;
	    this.shortError = shortError;
	    this.longError = longError;
	}

	public String shortError() {
	    return shortError;
	}

	public String longError() {
	    return longError;
	}

	public String errorCode() {
	    return errorCode;
	}

	public void setErrorTimeInstant(long timeInstant) {
	    this.errorTimeInstant = timeInstant;
	}

	public long getErrorTimeInstant() {
	    return errorTimeInstant;
	}
    }

    static final long TIME_WINDOW = 2000;           // 2 seconds
    public class RecentErrorList {

	ArrayList<ErrorDescriptor> errorDescs;

	public RecentErrorList() {
	    errorDescs = new ArrayList<ErrorDescriptor>();
	}

	public synchronized boolean pushError(ErrorDescriptor error) {
	    error.setErrorTimeInstant(System.currentTimeMillis());
	    errorDescs.add(error);
	    cleanOldErrorDescs();
	    if (!removeIfNotUnique())
		return true;
	    else
		return false;
	}

	protected void cleanOldErrorDescs() {
	    if (errorDescs.size() >= 2) {
		long currErrorTimeInstant = errorDescs.get(errorDescs.size() - 1).getErrorTimeInstant();
		long threshold = currErrorTimeInstant - TIME_WINDOW;
		while (errorDescs.size() > 1 && errorDescs.get(0).getErrorTimeInstant() < threshold)
		    errorDescs.remove(0);
	    }
	}

	protected boolean removeIfNotUnique() {
	    ErrorDescriptor lastError = errorDescs.get(errorDescs.size() - 1);
	    for (int i = 0; i < errorDescs.size() - 1; i++) {
		ErrorDescriptor compareError = errorDescs.get(i);
		if (identical(lastError, compareError)) {
		    errorDescs.remove(errorDescs.size() - 1);
		    return true;
		}
	    }
	    return false;
	}

	protected boolean identical(ErrorDescriptor a, ErrorDescriptor b) {
	    String aPrefix = a.shortError().split(":")[0];
	    String bPrefix = b.shortError().split(":")[0];
	    if (a.errorCode().equals(b.errorCode()) && 
	       (aPrefix.equals(bPrefix) || aPrefix.replace("enable", "activate").equals(bPrefix.replace("enable", "activate"))))
		return true;
	    
	    return false;
	}

    }

}