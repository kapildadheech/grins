package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.rscontroller.services.AudioService;
import org.gramvaani.radio.rscontroller.services.LibService;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.providers.PreviewProvider;
import org.gramvaani.radio.app.providers.MetadataListenerCallback;
import org.gramvaani.radio.app.providers.PlayoutPreviewListenerCallback;
import org.gramvaani.radio.app.providers.CacheListenerCallback;
import org.gramvaani.radio.app.providers.CacheObjectListener;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.rscontroller.pipeline.RSPipeline;
import org.gramvaani.radio.rscontroller.RSController;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import javax.swing.*;
import javax.swing.event.*;

import java.util.*;
import java.text.DateFormat;

import java.io.*;

import com.toedter.calendar.JDateChooser;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class LogViewWidget extends RSWidgetBase implements PlayoutPreviewListenerCallback, 
							   MetadataListenerCallback, CacheListenerCallback {
    
    ArrayList<RadioProgramMetadata> programs;
    static final int filterPaneVertFrac = 8;

    RSTitleBar titleBar;

    JPanel filterPane;
    Dimension filterPaneSize;
    JDateChooser fromDateChooser, toDateChooser;
    Date startDate, endDate;
    RSLabel statusLabel;

    LogViewModel listModel;
    JList listPane;
    JScrollPane scrollPane;
    RSMultiList multiList;
    QuickSearch quickSearch;

    MetadataProvider metadataProvider;
    CacheProvider cacheProvider;
    PreviewProvider previewProvider;

    RSApp rsApp;
    StationConfiguration stationConfig;
    ControlPanel controlPanel;
    ErrorInference errorInference;

    LogCellRenderer cellRenderer;
    protected int selectedIndex = -1;

    boolean metadataProviderActive = false;
    boolean cacheProviderActive = false;
    boolean previewProviderActive = false;

    static final int NUM_ENTRIES_PER_SCREEN = 15;

    static final int BUTTON_COLOR = Color.blue.getRGB();
    static final String ERROR_UNKNOWN = "ERROR_UNKNOWN";

    static final long MILLIS_PER_DAY = 1000L * 60L * 60L * 24L;
    static final long TZ_OFFSET;
    static final String PLAYLIST_FILE_EXTENSION = "m3u";
    static final String AUDIO = "audio";
    static final String LOG_FILE_EXTENSION = "xls";

    Color bgColor, componentBgColor;
    
    static {
	Calendar calendar = GregorianCalendar.getInstance();
	TZ_OFFSET = calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET);
    }
    
    public LogViewWidget(String name, RSApp rsApp, ControlPanel controlPanel){
	super("Playout Logs");
	this.rsApp = rsApp;
	this.controlPanel = controlPanel;
	stationConfig = rsApp.getStationConfiguration();

	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.LOGVIEW_ICON;
	bmpToolTip = "Playout Logs";

	metadataProvider = (MetadataProvider)(rsApp.getProvider(RSApp.METADATA_PROVIDER));
	cacheProvider = (CacheProvider)(rsApp.getProvider(RSApp.CACHE_PROVIDER));
	previewProvider = (PreviewProvider)(rsApp.getProvider(RSApp.PREVIEW_PROVIDER));
	
	errorInference = ErrorInference.getErrorInference();
    }

    public boolean onLaunch(){
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);
	componentBgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);

	wpComponent.setLayout(new BoxLayout(wpComponent, BoxLayout.PAGE_AXIS));
	titleBar = new RSTitleBar(name, (int)wpSize.getWidth());
	titleBar.setBackground(bgColor);
	wpComponent.setBackground(bgColor);
	wpComponent.add(titleBar);
	
	buildFilterPane(bgColor, componentBgColor);

	wpComponent.add(filterPane);
	
	listModel = new LogViewModel();
	listPane = new JList(listModel);
	listPane.setVisibleRowCount(NUM_ENTRIES_PER_SCREEN);

	listPane.setDragEnabled(true);

	TransferHandler transferHandler = new TransferHandler(){
		
		public int getSourceActions(JComponent c){
		    return COPY;
		}

		public Transferable createTransferable (JComponent c){
		    ArrayList<RadioProgramMetadata> list = new ArrayList<RadioProgramMetadata>();
		    RadioProgramMetadata rpm;
		    int[] selectionIndices = ((JList)c).getSelectedIndices();
		    
		    for (int i: selectionIndices) {
			if (i < listModel.size()) {
			    rpm = (RadioProgramMetadata)((PlayoutHistory)listModel.getElementAt(i)).getProgram();
			    logger.info("GRINS_USAGE:LOG_VIEW_DRAG:" + rpm.getItemID());
			    list.add(rpm);
			}
		    }
		    
		    return new ProgramSelection(list);
		}

	    };
	
	listPane.setTransferHandler(transferHandler);

	quickSearch = new QuickSearch(new QuickSearch.QuickSearchListener(){
		public void matchedIndices(HashSet<Integer> indices){
		    selectIndices(indices);
		}
	    });

	listPane.addKeyListener(new KeyAdapter(){
		public void keyTyped(KeyEvent e){
		    if (e.getModifiers() > 0)
			return;
		    quickSearch.keyTyped(e.getKeyChar());
		}

		public void keyPressed(KeyEvent e){
		    int keyCode = e.getKeyCode();
		    if (keyCode == KeyEvent.VK_RIGHT)
			multiList.nextPage();
		    else if (keyCode == KeyEvent.VK_LEFT)
			multiList.prevPage();
		}
	    });

	listPane.getModel().addListDataListener(new ListDataListener(){
		public void contentsChanged(ListDataEvent e){
		}
		
		public void intervalAdded(ListDataEvent e){
		    for (int i = e.getIndex0(); i <= e.getIndex1(); i++){
			PlayoutHistory history = (PlayoutHistory) listPane.getModel().getElementAt(i);
			quickSearch.insertString(history.getProgram().getTitle(), i);
		    }
		}

		public void intervalRemoved(ListDataEvent e){
		    quickSearch.removeRange(e.getIndex0(), e.getIndex1());
		}
	    });

	listPane.requestFocusInWindow();

	setInitDates();
	
	scrollPane = new JScrollPane(listPane);
	scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

	listPane.setBackground(componentBgColor);
	scrollPane.setBackground(componentBgColor);

	Dimension nextPrevDim = new Dimension(wpSize.width - scrollPane.getVerticalScrollBar().getMaximumSize().width - 5, 
					      (int)((wpSize.height - titleBar.HEIGHT - filterPaneSize.height) * 0.05));

	scrollPane.setPreferredSize(new Dimension(wpSize.width - 4, 
						  (wpSize.height - titleBar.HEIGHT - filterPaneSize.height - nextPrevDim.height)));

	scrollPane.setMinimumSize(new Dimension(wpSize.width - scrollPane.getVerticalScrollBar().getMaximumSize().width - 5, 
						(wpSize.height - titleBar.HEIGHT - filterPaneSize.height - nextPrevDim.height)));
	multiList = new RSMultiList(listPane, scrollPane, listModel,
				    new Dimension(wpSize.width - scrollPane.getVerticalScrollBar().getMaximumSize().width - 5, 
						  (wpSize.height - titleBar.HEIGHT - filterPaneSize.height - nextPrevDim.height)),
				    nextPrevDim,
				    new Dimension((int)(0.15 * (wpSize.width - scrollPane.getVerticalScrollBar().getMaximumSize().width - 5)), 
						  (nextPrevDim.height)),
				    stationConfig){
		public void actionPerformed(ActionEvent e){
		    super.actionPerformed(e);
		    quickSearch.rerun();
		}
	    
	    };

	multiList.setNumItemsPerScreen(NUM_ENTRIES_PER_SCREEN);
	multiList.setBackground(componentBgColor);
	
	final Dimension cellSize = new Dimension(wpSize.width - 5, 
					 (wpSize.height - titleBar.HEIGHT - filterPaneSize.height - nextPrevDim.height)/NUM_ENTRIES_PER_SCREEN);
	
	listPane.setFixedCellWidth(cellSize.width);
	listPane.setFixedCellHeight(cellSize.height);

	final LogCellRenderer cellRenderer = new LogCellRenderer(cellSize, this);
	this.cellRenderer = cellRenderer;

	listPane.setCellRenderer(new ListCellRenderer(){
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasCellFocus){
		    cellRenderer.setCell(list, value, isSelected, index);
		    return cellRenderer;
		}
	    });
	
	listPane.addMouseListener(new MouseAdapter(){

		public void mouseClicked(MouseEvent e){
		    int index = e.getY()/cellSize.height;

		    if (index >= listModel.size())
			return;

		    selectedIndex = index;

		    cellRenderer.mouseClicked(e, (PlayoutHistory)listPane.getSelectedValue());
		    listPane.repaint();
		}


		public void mousePressed(MouseEvent e){
		    int index = e.getY()/cellSize.height;

		    if (index >= listModel.size())
			return;

		    selectedIndex = index;

		    cellRenderer.mousePressed(e, (PlayoutHistory)listPane.getSelectedValue());
		    listPane.repaint();
		}
		
		public void mouseReleased(MouseEvent e){
		    int index = e.getY()/cellSize.height;

		    if (index >= listModel.size())
			return;

		    selectedIndex = index;

		    cellRenderer.mouseReleased(e, (PlayoutHistory)listPane.getSelectedValue());
		    listPane.repaint();
		}

	    });

	previewProvider.registerWithProvider(this);
	metadataProvider.registerWithProvider(this);
	cacheProvider.registerWithProvider(this);

	if (previewProvider == null || !previewProvider.isActive())
	    previewProviderActive = false;
	else
	    previewProviderActive = true;

	if (metadataProvider == null || !metadataProvider.isActive())
	    metadataProviderActive = false;
	else
	    metadataProviderActive = true;

	if (cacheProvider == null || !cacheProvider.isActive())
	    cacheProviderActive = false;
	else
	    cacheProviderActive = true;
	
	
	wpComponent.add(multiList);
	wpComponent.validate();

	cellRenderer.activatePreview(previewProviderActive);
	cellRenderer.activateInfoButton(metadataProviderActive);

	if (cacheProviderActive)
	    loadPrograms();
	else
	    errorInference.displayServiceError("Unable to get log from database", new String[]{RSController.ERROR_SERVICE_DOWN});
	activateDateChooser(cacheProviderActive);

	reinitAspComponent();

	return true;
    }

    private void buildFilterPane(Color bgColor, Color componentBgColor){
	filterPaneSize = new Dimension(wpSize.width, wpSize.height*filterPaneVertFrac/100);

	filterPane = new JPanel();
	filterPane.setMinimumSize(filterPaneSize);
	filterPane.setMaximumSize(filterPaneSize);
	filterPane.setPreferredSize(filterPaneSize);
	
	filterPane.setBackground(componentBgColor);
	filterPane.setLayout(new GridBagLayout());

	Dimension filterSize = new Dimension(filterPaneSize.width/6, filterPaneSize.height/2);

	JPanel datePane = new JPanel();
	datePane.setLayout(new GridBagLayout());
	datePane.setBackground(componentBgColor);
	Dimension datePaneSize = new Dimension(filterPaneSize.width/2, filterPaneSize.height/2);
	datePane.setPreferredSize(datePaneSize);

	JLabel fromDateLabel = new JLabel("View Logs From: ");
	fromDateLabel.setHorizontalAlignment(fromDateLabel.RIGHT);

	datePane.add(fromDateLabel, getGbc(0, 0, weightx(0.5), gfillboth()));

	fromDateChooser = new JDateChooser(){
		public void propertyChange(java.beans.PropertyChangeEvent e){
		    super.propertyChange(e);
		    dateSelected();
		    onMinimize();
		    aspComponent.validate();
		}
	    };

	fromDateChooser.setBackground(componentBgColor);
	datePane.add(fromDateChooser, getGbc(1, 0, weightx(1.0), gfillboth()));

	JLabel toDateLabel = new JLabel("To: ");
	toDateLabel.setHorizontalAlignment(toDateLabel.RIGHT);
	
	datePane.add(toDateLabel, getGbc(2, 0, weightx(0.5), gfillboth()));

	toDateChooser = new JDateChooser(){
		public void propertyChange(java.beans.PropertyChangeEvent e){
		    super.propertyChange(e);
		    dateSelected();
		}
	    };

	toDateChooser.setBackground(componentBgColor);
	datePane.add(toDateChooser, getGbc(3, 0, weightx(1.0), gfillboth()));

	filterPane.add(datePane, getGbc(0, 0, gfillboth()));

	JPanel blankPanel = new JPanel();
	blankPanel.setPreferredSize(new Dimension(filterPaneSize.width/10, filterPaneSize.height/2));
	blankPanel.setBackground(componentBgColor);
	filterPane.add(blankPanel, getGbc(1, 0, gfillboth()));

	Dimension exportButtonSize = new Dimension((int)(filterPaneSize.width*1.5/10), filterPaneSize.height/2);
	Icon exportIcon = IconUtilities.getIcon(StationConfiguration.EXPORT_ICON, exportButtonSize.height, exportButtonSize.height);
	JButton exportAudioButton = new JButton("Export Audio", exportIcon);
	exportAudioButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    Thread t = new Thread("Export Files"){
			    public void run(){
				exportFiles();
			    }
			};
		    t.start();
		}
	    });
	exportAudioButton.setPreferredSize(exportButtonSize);
	exportAudioButton.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR));
	filterPane.add(exportAudioButton, getGbc(2, 0, gfillboth()));
	
	blankPanel = new JPanel();
	blankPanel.setPreferredSize(new Dimension((int)(filterPaneSize.width*0.5/10), filterPaneSize.height/2));
	blankPanel.setBackground(componentBgColor);
	filterPane.add(blankPanel, getGbc(3, 0, gfillboth()));

	Icon reportIcon = IconUtilities.getIcon(StationConfiguration.MAKE_REPORT_ICON, exportButtonSize.height, exportButtonSize.height);
	JButton reportButton = new JButton("Make Report", reportIcon);
	reportButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    Thread t = new Thread("Make Report"){
			    public void run(){
				makeReport();
			    }
			};
		    t.start();
		}
	    });
	reportButton.setPreferredSize(exportButtonSize);
	reportButton.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR));
	filterPane.add(reportButton, getGbc(4, 0, gfillboth()));
    }

    public void refresh(){
	logger.info("Refresh: Reloading playout data.");
	loadPrograms();
    }

    public void insertProgram(String programID, long telecastTime){
	String stationName = stationConfig.getStringParam(StationConfiguration.STATION_NAME);
	RadioProgramMetadata program = cacheProvider.getSingle(new RadioProgramMetadata(programID));
	PlayoutHistory history = new PlayoutHistory(programID, stationName, telecastTime);
	history.setProgram(program);
	multiList.add(0, history);
	multiList.setNumSearchResults(multiList.getNumSearchResults() + 1);
	cacheProvider.addCacheObjectListener(program, listModel);
	cellRenderer.programAdded();
    }

    protected synchronized void dateSelected(){
	if (fromDateChooser == null || toDateChooser == null)
	    return;

	Date fromDate = fromDateChooser.getDate();
	Date toDate   = toDateChooser.getDate();

	if (fromDate == null || toDate == null)
	    return;

	if (fromDate.getTime() == startDate.getTime() && endOfDay(toDate.getTime()) == endDate.getTime())
	    return;
	
	startDate = fromDate;
	endDate = new Date(endOfDay(toDate.getTime()));

	fromDateChooser.setMaxSelectableDate(endDate);
	toDateChooser.setMinSelectableDate(startDate);

	loadPrograms();
	quickSearch.rerun();
    }

    protected long endOfDay(long time){
	long day = ((time + TZ_OFFSET)/ MILLIS_PER_DAY)+1;
	return day * MILLIS_PER_DAY - 1 - TZ_OFFSET;
    }

    protected void setInitDates(){
	endDate = new Date(endOfDay(System.currentTimeMillis()));
	toDateChooser.setDate(endDate);
	startDate = new Date(endDate.getTime() - MILLIS_PER_DAY + 1);
	fromDateChooser.setDate(startDate);

	fromDateChooser.setMaxSelectableDate(endDate);
	toDateChooser.setMinSelectableDate(startDate);
    }

    protected void loadPrograms(){
	logger.debug("LoadPrograms: Loading PlayoutHistory information.");
	
	final PlayoutHistory[] playoutHistory = metadataProvider.get(new PlayoutHistory(),
								     null,
						       new String[] {PlayoutHistory.getTelecastTimeField()},
						       new String[] {MetadataProvider.ORDER_DESC},
						       new String[] {PlayoutHistory.getTelecastTimeField(), 
								     Long.toString(startDate.getTime()),
								     Long.toString(endDate.getTime())},
						       -1);
	Runnable loader = new Runnable(){
		public void run(){
		    multiList.clear();
		    int numItems = 0;
		    
		    for (PlayoutHistory item: playoutHistory){
			RadioProgramMetadata program = cacheProvider.getSingle(new RadioProgramMetadata(item.getItemID()));
			
			if (program != null){
			    item.setProgram(program);
			    cacheProvider.addCacheObjectListener(program, listModel);
			    multiList.add(item);
			    numItems++;
			} else {
			    logger.error("LoadPrograms: Got null metadata for program ID: "+item.getItemID());
			}
		    }

		    if (numItems > 0)
			multiList.setNumSearchResults(numItems);
		    scrollPane.validate();
		    scrollPane.repaint();
		}
	    };

	try{
	    if (SwingUtilities.isEventDispatchThread())
		loader.run();
	    else
		SwingUtilities.invokeLater(loader);
	} catch (Exception e){
	    logger.error("Add: Could not load programs.", e);
	}

	listPane.requestFocusInWindow();
    }

    protected void activateDateChooser(boolean activate){
	fromDateChooser.setEnabled(activate);
	toDateChooser.setEnabled(activate);
    }

    protected void selectIndices(HashSet<Integer> indices){
	int selected[] = new int[indices.size()];
	int i = 0;
	for (int index:indices)
	    selected[i++] = index;

	listPane.setSelectedIndices(selected);
    }

    public void onMaximize(){

    }

    public void onMinimize(){

    }

    public void reinitAspComponent() {
	if (startDate == null || endDate == null)
	    return;

	Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR);
	aspComponent.removeAll();

	aspComponent.setLayout(new GridBagLayout());

	String startDateString = DateFormat.getDateInstance(DateFormat.MEDIUM).format(startDate);
	RSLabel fromLabel = new RSLabel("From: " + startDateString);
	Dimension rowDim = new Dimension((int)(aspComponent.getWidth()) - 2, (int)(aspComponent.getHeight() / 8));
	fromLabel.setPreferredSize(rowDim);
	fromLabel.setMinimumSize(rowDim);
	fromLabel.setHorizontalAlignment(SwingConstants.LEFT);
	aspComponent.add(fromLabel, getGbc(0, 0));

	String endDateString = DateFormat.getDateInstance(DateFormat.MEDIUM).format(endDate);
	RSLabel toLabel = new RSLabel("To: " + endDateString);
	toLabel.setPreferredSize(rowDim);
	toLabel.setMinimumSize(rowDim);
	toLabel.setHorizontalAlignment(SwingConstants.LEFT);
	aspComponent.add(toLabel, getGbc(0, 1));

	statusLabel = new RSLabel("");
	aspComponent.add(statusLabel, getGbc(0, 2, weighty(100.0)));

	aspComponent.validate();
	aspComponent.repaint();
    }

    public void onUnload(){
	wpComponent.removeAll();

	previewProvider.unregisterWithProvider(getName());
	metadataProvider.unregisterWithProvider(getName());
	cacheProvider.unregisterWithProvider(getName());
    }

    protected void launchMetadataWidget(PlayoutHistory history){
	RadioProgramMetadata program = history.getProgram();

	if (program == null){
	    logger.error("LaunchMetadataWidget: Could not retrieve metadata for item: "+history.dump());
	    return;
	}

	if (metadataProviderActive) {
	    String programID = program.getItemID();
	    MetadataWidget metadataWidget = MetadataWidget.getMetadataWidget(rsApp, new String[]{programID}, false);
	    if (!metadataWidget.isLaunched()) {
		controlPanel.initWidget(metadataWidget, false);
		controlPanel.cpJFrame().launchWidget(metadataWidget);
	    } else {
		controlPanel.cpJFrame().setWidgetMaximized(metadataWidget);
	    }
	} else {
	    logger.error("LaunchMetadataWidget: Metadata provider inactive.");
	}
    }

    protected void startPreview(PlayoutHistory history){
	RadioProgramMetadata program = history.getProgram();

	if (program == null){
	    logger.error("StartPreview: Could not preview item: "+history.dump());
	    errorInference.displayPlaylistError("Program missing from database. Unable to preview", 
						new String[]{PlaylistController.PROGRAM_NOT_IN_DATABASE});
	    return;
	}

	String programID = program.getItemID();
	
	logger.debug("StartPreview: "+programID);

	if (previewProviderActive && previewProvider.isPlayable()){
	    if (previewProvider.play(programID, this) < 0)
		errorInference.displayPlaylistError("Unable to send preview-start command", new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
	}
    }

    protected void stopPreview(PlayoutHistory history){
	RadioProgramMetadata program = history.getProgram();

	if (program == null){
	    logger.error("StopPreview: Could not stop item: "+history.dump());
	    return;
	}

	String programID = program.getItemID();
	
	logger.debug("StopPreview: "+programID);

	if (previewProviderActive && previewProvider.isStoppable(programID, this)){
	    if (previewProvider.stop(programID, this) < 0)
		errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
	}
    }

    protected void setBackground(Component c, Color bgColor){
	c.setBackground(bgColor);
	if (c instanceof Container)
	    for (Component component: ((Container)c).getComponents())
		setBackground(component, bgColor);
    }

    String getDateStringFromChooser(JDateChooser chooser) {
	Date date = chooser.getDate();
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);
	
	String dd = (new Integer(calendar.get(Calendar.DAY_OF_MONTH))).toString();
	String mm = (new Integer(calendar.get(Calendar.MONTH) + 1)).toString();
	String yyyy = (new Integer(calendar.get(Calendar.YEAR))).toString();
	
	return dd + "-" + mm + "-" + yyyy;
    }

    protected synchronized void makeReport() {
	String separator = "\t";
	String[] entry = new String[4];
	ArrayList<String> entries = new ArrayList<String>();
	
	if(multiList.getObjects().size() == 0) {
	    JOptionPane.showMessageDialog(null, "No entries to export. Select dates to list playout history and then press 'Make Report'.", "information", JOptionPane.INFORMATION_MESSAGE);
	    return;
	}

	JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setBackground(componentBgColor);
	fileChooser.updateUI();
	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int retVal = fileChooser.showDialog(cpJFrame, "Make Report");

	if (retVal != JFileChooser.APPROVE_OPTION)
	    return;

	String destFolder = null;
	try{
	    destFolder = fileChooser.getSelectedFile().getCanonicalPath()+System.getProperty("file.separator");
	} catch (Exception e){
	    logger.error("MakeReport: Unable to find canonical path of selected folder.");
	    return;
	}

	String fromDateString = getDateStringFromChooser(fromDateChooser);
	String toDateString = getDateStringFromChooser(toDateChooser);
	String destFile = destFolder + fromDateString + "_to_" + toDateString + "." + LOG_FILE_EXTENSION;

	entries.add("Title" + separator + "Broadcast Time" + separator + "Type of Program"+ separator + "details");
	
	for (Object obj: multiList.getObjects()){
	    PlayoutHistory history = (PlayoutHistory) obj;
	    
	    String telecastTime = new Date(history.getTelecastTime()).toString();
	    String title = history.getProgram().getTitle();
	    title = StringUtilities.sanitize(title);
	    String type = history.getProgram().getHumanFriendlyTypeString();
	    String description = history.getProgram().getDescription();

	    entries.add(title + separator + telecastTime + separator + type + separator + description);
	}
	
	if(FileUtilities.writeToXLS(destFile, entries, separator, true)) {
	    FileUtilities.setPermissions(destFile, false);
	    JOptionPane.showMessageDialog(null, "Report created successfully at: " + destFile, "information", JOptionPane.INFORMATION_MESSAGE);
	} else {
	    JOptionPane.showMessageDialog(null, "Report creation failed!!", "information", JOptionPane.INFORMATION_MESSAGE);
	}
    }

    protected synchronized void exportFiles(){

	if(multiList.getObjects().size() == 0) {
	    JOptionPane.showMessageDialog(null, "No entries to export. Select dates to list playout history and then press export.", "information", JOptionPane.INFORMATION_MESSAGE);
	    return;
	}


	JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setBackground(componentBgColor);
	fileChooser.updateUI();
	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int retVal = fileChooser.showDialog(cpJFrame, "Export");

	if (retVal != JFileChooser.APPROVE_OPTION)
	    return;

	String destFolder = null;
	try{
	    destFolder = fileChooser.getSelectedFile().getCanonicalPath()+System.getProperty("file.separator");
	} catch (Exception e){
	    logger.error("ExportFiles: Unable to find canonical path of selected folder.");
	    return;
	}
	String fromDateString = getDateStringFromChooser(fromDateChooser);
	String toDateString = getDateStringFromChooser(toDateChooser);
	String audioFolderName = AUDIO + "_" + fromDateString + "_to_" + toDateString;
	String playlistName = fromDateString + "_to_" + toDateString + "." + PLAYLIST_FILE_EXTENSION;
	String audioFolder = destFolder + audioFolderName + System.getProperty("file.separator");

	if(!(new File(audioFolder).mkdir())) {
	    logger.error("ExportFiles: Could not create audio directory " + audioFolder);
	    return;
	}

	String srcFolder = stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR) + System.getProperty("file.separator");
	int numFiles = multiList.getObjects().size();
	FileStoreDownloader fsDownloader = new FileStoreDownloader(stationConfig);

	ArrayList<String> copiedFiles = new ArrayList<String>();

	for (Object obj: multiList.getObjects()){
	    PlayoutHistory history = (PlayoutHistory) obj;
	    File srcFile = null, destFile = null;
	    String destFileName = null;
	    try{
		statusLabel.setText(numFiles-- + " files remaining");

		String telecastTime = new Date(history.getTelecastTime()).toString();
		String baseName = telecastTime +"-"+ history.getProgram().getTitle();
		baseName = StringUtilities.sanitizeFilename(baseName);
		String extension = history.getItemID().substring(history.getItemID().lastIndexOf("."));
		destFileName =  baseName + extension;
		int i = 0;
		try{
		    while ((new File(audioFolder + destFileName)).exists()){
			destFileName = baseName + "-" + (i++) + extension;
		    }
		} catch (Exception e){
		    logger.error("ExportFiles: Error in checking existing files.", e);
		}

		String srcFilename = srcFolder + history.getItemID();
		String destFilename = audioFolder + destFileName;
		
		if (fsDownloader.getFile(history.getItemID(), audioFolder)) {
		    logger.debug("ExportFiles: Copied file "+srcFilename+" to "+destFilename);
		    FileUtilities.changeFilename(audioFolder + history.getItemID(), destFilename);
		    copiedFiles.add(audioFolderName + System.getProperty("file.separator") + destFileName);
		} else {
		    logger.error("ExportFiles: Could not copy file "+srcFilename+" to " +destFilename);
		}

	    } catch(Exception e) {
		logger.error("ExportFiles: Could not copy file "+srcFolder+history.getItemID()+" to " +audioFolder+destFileName, e);
	    }
	    statusLabel.setText("");
	}
	
	Collections.sort(copiedFiles);
	FileUtilities.writeToFile(destFolder + playlistName, copiedFiles.toArray(new String[copiedFiles.size()]), false);
	FileUtilities.setPermissions(audioFolder, true);
	FileUtilities.setPermissions(destFolder + playlistName, false);
	JOptionPane.showMessageDialog(null, "Export complete. " + copiedFiles.size() + " files copied.", "information", JOptionPane.INFORMATION_MESSAGE);
    }

    /* Callbacks from Providers */

    public synchronized void playActive(boolean success, String programID, String playType, long telecastTime, String[] error){
	if (success){
	    cellRenderer.playActive(programID);
	    listPane.repaint();
	}
	else{
	    logger.error("PlayActive: Provider unable to play.");
	    errorInference.displayServiceError("Unable to preview file", error);
	}
    }

    public synchronized void pauseActive(boolean success, String programID, String playType, String[] error){

    }

    public synchronized void stopActive(boolean success, String programID, String playType, String[] error){
	if (success){
	    cellRenderer.stopActive(programID);
	    listPane.repaint();
	}
	else{
	    logger.error("StopActive: Provider unable to stop.");
	    errorInference.displayServiceError("Unable to stop preview", error);
	}
    }

    public synchronized void playDone(boolean success, String programID, String playType){
	if (success){
	    cellRenderer.stopActive(programID);
	    listPane.repaint();
	}
	else{
	    logger.error("PlayDone: Error in playing.");
	    errorInference.displayServiceError("Error occured during playing", new String[]{});
	}
    }

    public void audioGstError(String programID, String error, String playType){
	errorInference.displayGstError("Error occured while playing file", error);
    }

    public void playoutPreviewError(String errorCode, String[] errorParams) {
	logger.error("PlayoutPreviewError: " + errorCode);
	
	errorInference.displayServiceError("Error occured during preview", errorCode, errorParams);
    }

    public synchronized void activateAudio(boolean activate, String playType, String[] error){
	if (playType.equals(AudioService.PREVIEW)){
	    previewProviderActive = activate;
	    cellRenderer.activatePreview(activate);
	    listPane.repaint();
	}

	if (!activate)
	    errorInference.displayServiceError("Unable to activate audio", error);
    }
    
    public synchronized void enableAudioUI(boolean enable, String playType, String[] error) {
	if (playType.equals(AudioService.PREVIEW)){
	    cellRenderer.enablePreviewUI(enable);
	    listPane.repaint();
	} 

	if (!enable)
	    errorInference.displayServiceError("Unable to activate audio service", error);
    }

    public synchronized void activateCache(boolean activate, String[] error) {
	cacheProviderActive = activate;
	activateDateChooser(activate);
	if (activate)
	    loadPrograms();
	else
	    errorInference.displayServiceError("Unable to activate cache service", error);
    }
    
    public void cacheObjectsDeleted(String objectType, String[] objectID){}
    public void cacheObjectsUpdated(String objectType, String[] objectID){}
    public void cacheObjectsInserted(String objectType, String[] objectID){}

    public synchronized void activateMetadataService(boolean activate, String[] error){
	metadataProviderActive = activate;
	cellRenderer.activateInfoButton(activate);
	listPane.repaint();

	if (!activate)
	    errorInference.displayServiceError("Unable to activate metadata service", error);
    }


    class LogViewModel extends DefaultListModel implements CacheObjectListener, MultiListModel {

	public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKey){
	    for (int j = 0; j < metadata.length; j++) {
		String newItemID = newKey[j].substring(0, newKey[j].lastIndexOf("."));
		RadioProgramMetadata program = (RadioProgramMetadata) metadata[j];
		RadioProgramMetadata dummyProgram, newProgram=null;

		if (!newItemID.equals("")) {
		    dummyProgram = RadioProgramMetadata.getDummyObject(newItemID);
		    newProgram = cacheProvider.getSingle(dummyProgram);
		    if (newProgram == null)
			return;
		}

		for (int i = 0; i < multiList.listSize(); i++){
		    PlayoutHistory playoutHistory = (PlayoutHistory) multiList.getElementAt(i);
		    if (program.getItemID().equals(playoutHistory.getProgram().getItemID())){
			if (!newItemID.equals("")){
			    playoutHistory.setProgram(newProgram);
			} else {
			    multiList.remove(i, true);
			}
		    }
		}

		if (!newItemID.equals("")) {
		    SwingUtilities.invokeLater(new Runnable() {
			    public void run() {
				fireContentsChanged(LogViewModel.this, 0, getSize());
			    }
			});
		}
	    }
	}

	public void cacheObjectValueChanged(Metadata metadata){

	    RadioProgramMetadata program = (RadioProgramMetadata) metadata;

	    for (int i = 0; i < multiList.listSize(); i++){
		PlayoutHistory playoutHistory = (PlayoutHistory)multiList.getElementAt(i);
		if (program.getItemID().equals(playoutHistory.getProgram().getItemID())){
		    playoutHistory.setProgram(program);
		}
	    }

	    SwingUtilities.invokeLater(new Runnable(){
		    public void run(){
			fireContentsChanged(LogViewModel.this, 0, getSize());
		    }
		});
	}

	public void add(Object obj) {
	    super.addElement(obj);
	}
	
	public void removeFromMultiList(int index){
	    remove(index);
	}
    }

    class LogCellRenderer extends JPanel {
	Dimension cellSize;
	JLabel titleLabel;
	JLabel playoutTimeLabel;
	JLabel typeLabel;

	Dimension typeLabelSize;

	Color bgColor;
	final int titleHorizFrac = 62;
	final int playoutTimeHorizFrac = 25;

	PlayoutHistory playingItem = null, infoItem = null;
	RSButton selectedButton = null;
	RSButton infoButton, previewButton, stopButton;
	
	boolean isMousePressed = false;
	boolean isPlayActive = false;
	boolean isPreviewEnabled = true;

	int infoIndex    = -1;
	int playingIndex = -1;

	Color selectedForeground = new Color(10, 24, 225);
	
	public LogCellRenderer(Dimension cellSize, final LogViewWidget widget){
	    super();
	    this.cellSize = cellSize;
	    setPreferredSize(cellSize);
	    setMinimumSize(cellSize);
	    setSize(cellSize);
	    setMaximumSize(cellSize);

	    int HORIZ_PADDING = cellSize.height/3;

	    setLayout(new GridBagLayout());
	    
	    titleLabel = new RSLabel("");
	    titleLabel.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_TITLE_COLOR));

	    Dimension titleSize = new Dimension(cellSize.width*titleHorizFrac/100, cellSize.height);
	    //titleLabel.setSize(titleSize);
	    titleLabel.setMinimumSize(titleSize);
	    add(titleLabel, getGbc(0, 0, gridheight(2), weightx(1.0), gfillboth()));

	    Dimension buttonSize = new Dimension(cellSize.width*(100-titleHorizFrac-playoutTimeHorizFrac)/(100 * 4), 
						     (int)(cellSize.height * 0.60));

	    typeLabelSize = buttonSize;
	    typeLabel = new RSLabel();
	    typeLabel.setPreferredSize(typeLabelSize);
	    typeLabel.setMinimumSize(typeLabelSize);
	    add(typeLabel, getGbc(1, 1, padx(HORIZ_PADDING)));


	    playoutTimeLabel = new RSLabel("");
	    Dimension playoutTimeSize = new Dimension(cellSize.width*playoutTimeHorizFrac/100, cellSize.height);
	    playoutTimeLabel.setSize(playoutTimeSize);
	    playoutTimeLabel.setMinimumSize(playoutTimeSize);
	    playoutTimeLabel.setPreferredSize(playoutTimeSize);
	    playoutTimeLabel.setHorizontalAlignment(SwingConstants.LEFT);
	    playoutTimeLabel.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_DETAILS_COLOR));
	    add(playoutTimeLabel, getGbc(2, 0, gridheight(2), padx(HORIZ_PADDING)));

	    Dimension previewButtonSize = buttonSize;

	    previewButton = new RSButton("");
	    previewButton.setSize(previewButtonSize);
	    previewButton.setMinimumSize(previewButtonSize);
	    previewButton.setPreferredSize(previewButtonSize);
	    previewButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (playingItem != null)
			    widget.startPreview(playingItem);
		    }
		});
	    previewButton.setToolTipText("Preview");
	    add(previewButton, getGbc(3, 1));


	    Dimension stopButtonSize = buttonSize;

	    stopButton = new RSButton ("");
	    stopButton.setSize(stopButtonSize);
	    stopButton.setMinimumSize(stopButtonSize);
	    stopButton.setPreferredSize(stopButtonSize);
	    stopButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (playingItem != null)
			    widget.stopPreview(playingItem);
		    }
		});
	    stopButton.setToolTipText("Stop");
	    add(stopButton, getGbc(4, 1));

	    infoButton = new RSButton("");
	    infoButton.setSize(previewButtonSize);
	    infoButton.setMinimumSize(previewButtonSize);
	    infoButton.setPreferredSize(previewButtonSize);
	    infoButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (infoItem != null)
			    widget.launchMetadataWidget(infoItem);
		    }
		});
	    infoButton.setToolTipText("Info");
	    add(infoButton, getGbc(5, 1));


	    bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_COLOR);
	    setBackground(bgColor);

	    Color buttonBgColor = stationConfig.getColor(StationConfiguration.SEARCHCLEAR_BUTTON_COLOR);
	    infoButton.setBackground(buttonBgColor);
	    previewButton.setBackground(buttonBgColor);
	    stopButton.setBackground(buttonBgColor);
	    
	    infoButton.setIcon(IconUtilities.getIcon(StationConfiguration.INFO_ICON, (int)(buttonSize.getWidth()), 
						     (int)(buttonSize.getHeight())));
	    previewButton.setIcon(IconUtilities.getIcon(StationConfiguration.PREVIEW_ICON, (int)(previewButtonSize.getWidth()), 
							(int)(previewButtonSize.getHeight())));
	    stopButton.setIcon(IconUtilities.getIcon(StationConfiguration.PREVIEW_STOP_ICON, (int)(stopButtonSize.getWidth()), 
						     (int)(stopButtonSize.getHeight())));

	    setBorder (new GradientBorder(stationConfig.getColor(StationConfiguration.LIST_ITEM_BORDER_COLOR)));
	}

	public void setCell(JList list, Object value, boolean isSelected, int index){
	    PlayoutHistory playoutHistory = (PlayoutHistory) value;
	    RadioProgramMetadata query = new RadioProgramMetadata(playoutHistory.getItemID());
	    RadioProgramMetadata program = cacheProvider.getSingle(query);

	    titleLabel.setText(" "+playoutHistory.getProgram().getTitle());
	    titleLabel.setToolTipText(playoutHistory.getProgram().getTitle());
	    playoutTimeLabel.setText(new Date(playoutHistory.getTelecastTime()).toString());

	    typeLabel.setIcon(GUIUtilities.getIcon(program, typeLabelSize.width, typeLabelSize.height));

	    if (isSelected) {
		//		titleLabel.setForeground(Color.blue);
		setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_SELECT_COLOR));
	    } else {
		//		titleLabel.setForeground(Color.black);
		setBackground(bgColor);
	    }
	    
	    previewButton.setPressed(false);
	    stopButton.setPressed(false);
	    infoButton.setPressed(false);

	    if (program == null || program.getPreviewStoreAttempts() != FileStoreManager.UPLOAD_DONE) {
		previewButton.setEnabled(false);
		stopButton.setEnabled(false);
	    } else if (isPreviewEnabled) {
		previewButton.setEnabled(true);
		stopButton.setEnabled(true);
	    }
		
	    if (isMousePressed){
		if (selectedButton == infoButton && index == infoIndex)
		    infoButton.setPressed(true);
		if (selectedButton == stopButton && playingIndex == index)
		    stopButton.setPressed(true);
		if (selectedButton == previewButton && playingIndex == index)
		    previewButton.setPressed(true);
	    }

	    //	    logger.debug("setCell: index = " + index + ": playingIndex = " + playingIndex + ": isSaveIndex = " + multiList.isSaveIndex(index));

	    if (isPlayActive && multiList.isSaveIndex(index))
		previewButton.setPressed(true);
	}

	public void programAdded(){
	    if (infoIndex != -1)
		infoIndex++;
	    if (playingIndex != -1)
		playingIndex++;
	}

	public void activateInfoButton(boolean activate){
	    infoButton.setEnabled(activate);
	}

	public void activatePreview(boolean activate){
	    enablePreviewUI(activate);
	    if (!activate)
		stopActive(null);
	}

	public void enablePreviewUI(boolean enable) {
	    isPreviewEnabled = enable;
	    previewButton.setEnabled(enable);
	    stopButton.setEnabled(enable);
	}

	protected RSButton getButtonAt(Point p){
	    setLocation(0, 0);
	    setSize(cellSize);

	    int y = (int)p.getY()%(int)getPreferredSize().getHeight();
	    int x = (int)p.getX();

	    Component button = getComponentAt(x, y);
	    if (button instanceof RSButton){
		return ((RSButton)button);
	    }
	    else
		return null;
	}

	public void mouseClicked(MouseEvent e, PlayoutHistory history){
	    if (selectedButton != null)
		selectedButton.doClick();
	    else if (e.getClickCount() == 2){
		infoItem = history;
		infoButton.doClick();
	    }
	}

	public void mousePressed(MouseEvent e, PlayoutHistory history){
	    RSButton tmp = getButtonAt(e.getPoint());

	    if ((tmp == null) ||
	       (isPlayActive  && tmp == previewButton) ||
	       (!isPlayActive && tmp == stopButton) ||
	       (isPlayActive  && tmp == stopButton && !multiList.isSaveIndex(selectedIndex))){

		selectedButton = null;
		return;
	    }

	    isMousePressed = true;

	    if (tmp == previewButton){
		playingIndex = selectedIndex;
		multiList.setSaveIndex(selectedIndex);
		playingItem = history;
	    } else if (tmp == infoButton) {
		infoIndex = selectedIndex;
		infoItem  = history;
	    }

	    selectedButton = tmp;
	}

	public void mouseReleased(MouseEvent e, PlayoutHistory history){
	    isMousePressed = false;
	}

	public void playActive(String programID){
	    isPlayActive = true;
	}

	public void stopActive(String programID){
	    isPlayActive = false;
	}

    }

}

