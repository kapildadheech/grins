package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.Category;

import java.util.ArrayList;

import java.awt.datatransfer.DataFlavor;

public class CategorySelection extends SelectionList {
    
    CategorySelection(ArrayList<Category> list){
	super(list);
    }

    public static DataFlavor getFlavor(){
	return SelectionList.getFlavor(CategorySelection.class);
    }
}