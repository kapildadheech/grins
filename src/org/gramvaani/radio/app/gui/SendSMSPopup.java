package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.providers.SMSProvider;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.utilities.IconUtilities;
import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.utilities.StringUtilities;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class SendSMSPopup extends JFrame {

    StationConfiguration stationConfig;
    JTextArea messageField, phoneNoField;
    RSApp rsApp;
    JPanel smsPanel;
    JComboBox smsLineBox;
    SendSMSPopupListener popupListener;

    public SendSMSPopup(RSApp rsApp, int width, int height, Color bgColor, Color titleColor, StationConfiguration stationConfig, SendSMSPopupListener listener) {
	this.rsApp = rsApp;
	this.stationConfig = stationConfig;
	popupListener = listener;

	smsPanel = new JPanel();
	Dimension size = new Dimension(width, height);
	smsPanel.setBorder(BorderFactory.createLineBorder(bgColor.darker()));
	smsPanel.setPreferredSize(size);
	smsPanel.setLayout(new BorderLayout());

	int titleVertFrac = 15;
	int buttonPanelVertFrac = 15;
	
	int titleHeight = titleVertFrac * height / 100;
	int buttonPanelHeight = buttonPanelVertFrac * height / 100;
	int fieldPanelHeight = size.height - titleHeight - buttonPanelHeight;

	Dimension titleSize = new Dimension(size.width, titleHeight);
	RSLabel titleLabel = new RSLabel("Send SMS");
	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	titleLabel.setPreferredSize(titleSize);
	titleLabel.setOpaque(true);
	titleLabel.setBackground(titleColor);
	smsPanel.add(titleLabel, BorderLayout.NORTH);

	Dimension fieldPanelSize = new Dimension(size.width, fieldPanelHeight);
	RSPanel fieldPanel = new RSPanel();
	fieldPanel.setBackground(bgColor);
	fieldPanel.setPreferredSize(fieldPanelSize);
	smsPanel.add(fieldPanel, BorderLayout.CENTER);

	Dimension buttonPanelSize = new Dimension(size.width, buttonPanelHeight);
	RSPanel buttonPanel = new RSPanel();
	buttonPanel.setBackground(bgColor);
	buttonPanel.setPreferredSize(buttonPanelSize);
	smsPanel.add(buttonPanel, BorderLayout.SOUTH);

	fieldPanel.setLayout(new GridBagLayout());
	
	int labelHorizFrac = 20;
	int labelVertFrac = 15;

	int labelWidth = labelHorizFrac * size.width/100;
	int fieldWidth = size.width - labelWidth - 5;
	int labelHeight = labelVertFrac * fieldPanelHeight/100;

	Dimension labelSize = new Dimension(labelWidth, labelHeight);
	Dimension comboBoxSize = new Dimension(fieldWidth, labelHeight);
	Dimension fieldSize = new Dimension(fieldWidth, labelHeight * 3);

	int vertGap = 4;
	Insets insets = new Insets(0, 0, vertGap, 0);

	RSLabel serviceLabel = new RSLabel("Service: ");
	serviceLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(serviceLabel, getGbc(0, 0, insets));

	smsLineBox = new JComboBox(stationConfig.getAllSMSLineIDs());
	smsLineBox.setPreferredSize(comboBoxSize);
	fieldPanel.add(smsLineBox, getGbc(1, 0, insets));

	RSLabel toLabel = new RSLabel("To: ");
	toLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(toLabel, getGbc(0, 1, insets, gfillboth()));

	phoneNoField = new JTextArea();
	phoneNoField.setPreferredSize(fieldSize);
	phoneNoField.setRows(3); //XXX Should readjust this as more phone numbers are entered.
	phoneNoField.setLineWrap(true);
	JScrollPane phoneNoScrollPane = new JScrollPane(phoneNoField);
	fieldPanel.add(phoneNoScrollPane, getGbc(1, 1, insets));

	RSLabel messageLabel = new RSLabel("Message: ");
	messageLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(messageLabel, getGbc(0, 2, insets, gfillboth()));

	messageField = new JTextArea();
	messageField.setPreferredSize(fieldSize);
	messageField.setRows(3);
	messageField.setLineWrap(true);
	messageField.setDocument(new LimitedLengthDocument(160));
	JScrollPane messageScrollPane = new JScrollPane(messageField);
	fieldPanel.add(messageScrollPane, getGbc(1, 2, insets));

	int buttonHorizFrac = 40;
	int buttonWidth = buttonHorizFrac * size.width/100 / 2;
	int buttonGap = 4;
	Dimension buttonSize = new Dimension(buttonWidth, buttonPanelSize.height - buttonGap * 2);
	int iconSize = (buttonSize.height * 8)/10;
	buttonPanel.setLayout(new GridBagLayout());

	RSButton sendButton = new RSButton("Send");
	sendButton.setPreferredSize(buttonSize);
	sendButton.setBackground(bgColor);
	sendButton.setIcon(IconUtilities.getIcon(StationConfiguration.SAVE_ICON, iconSize)); //XXXX change the icon
	sendButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    sendSMS();
		}
	    });
	buttonPanel.add(sendButton);
	
	RSButton cancelButton = new RSButton("Cancel");
	cancelButton.setPreferredSize(buttonSize);
	cancelButton.setBackground(bgColor);
	cancelButton.setIcon(IconUtilities.getIcon(StationConfiguration.CANCEL_ICON, iconSize));
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    cancelSMS();
		}
	    });
	buttonPanel.add(cancelButton);
	
	KeyListener escapeListener = new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			cancelSMS();
		    }
		}
	    };

	for (JComponent c: new JComponent[]{smsPanel, phoneNoField, messageField}){
	    c.addKeyListener(escapeListener);
	}

	addWindowFocusListener(new WindowAdapter(){
		public void windowLostFocus(WindowEvent e){
		    cancelSMS();
		}
	    });

	add(smsPanel);
	setUndecorated(true);
	pack();
    }
    
    public void showPopup(String[] phoneNos, String message, JComponent assocComponent) {
	String phoneNoStr = StringUtilities.getCSVFromArray(phoneNos);
	phoneNoField.setText(phoneNoStr);
	messageField.setText(message);

	Dimension panelSize = smsPanel.getPreferredSize();

	int xOffset = -(panelSize.width - assocComponent.getSize().width)/2;
	int yOffset = -(panelSize.height);
	Point p = assocComponent.getLocationOnScreen();

	int popupX = p.x + xOffset;
	int popupY = p.y + yOffset;

	setLocation(popupX, popupY);
	setVisible(true);
	smsPanel.requestFocusInWindow();
    }

    void sendSMS() {
	SMSProvider smsProvider = (SMSProvider) rsApp.getProvider(RSApp.SMS_PROVIDER);
	String[] phoneNos = StringUtilities.getArrayFromCSV(phoneNoField.getText());
	String message = messageField.getText();
	String smsLine = (smsLineBox.getSelectedItem()).toString();
	smsProvider.sendSMS(smsLine, message, phoneNos);
	popupListener.sendDone();
	setVisible(false);
    }

    void cancelSMS() {
	setVisible(false);
    }

}

class LimitedLengthDocument extends PlainDocument {
    int maxLength;

    public LimitedLengthDocument( int maxLength ) {
        this.maxLength = maxLength;
    }

    public void insertString( int offset, String str, AttributeSet a ) throws BadLocationException {
        int length = str.length();
	
        if ( offset + length > maxLength )
            length = maxLength - offset;
	
        super.insertString( offset, str.substring(0, length), a );
    }
}