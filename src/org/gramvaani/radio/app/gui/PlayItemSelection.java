package org.gramvaani.radio.app.gui;

import java.util.ArrayList;

import java.awt.datatransfer.DataFlavor;

public class PlayItemSelection extends SelectionList {

    PlayItemSelection(ArrayList<PlaylistWidget.PlayItemWidget> list){
	super(list);
    }

    public static DataFlavor getFlavor(){
	return SelectionList.getFlavor(PlayItemSelection.class);
    }
}