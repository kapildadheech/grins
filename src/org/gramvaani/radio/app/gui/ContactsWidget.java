package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.providers.CacheListenerCallback;
import org.gramvaani.radio.app.providers.CacheObjectListener;
import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.app.providers.MetadataListenerCallback;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.telephonylib.RSCallerID;

import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import java.util.Hashtable;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class ContactsWidget extends RSWidgetBase implements MetadataListenerCallback, CacheListenerCallback, EntityEditorPopup.EntityEditorListener {

    static final String CONTACTS_FILE_NAME = "contacts.xls";

    RSApp rsApp;

    CacheProvider cacheProvider;
    MetadataProvider metadataProvider;
    boolean isCacheActive, isMetadataActive;

    ErrorInference errorInference;
    StationConfiguration stationConfig;
    JTable contactsTable;
    ContactsTableModel tableModel;

    RSButton addButton, editButton, removeButton, exportButton;

    EntityEditorPopup editorPopup;

    RSLabel editorTitleLabel;
    Entity editedEntity;

    Color bgColor, titleColor, defaultTextFieldColor, errorTextFieldColor;

    public ContactsWidget(RSApp rsApp){
	super("Contacts");
	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.CONTACTS_ICON;
	bmpToolTip = "Contacts";

	this.rsApp = rsApp;
	stationConfig = rsApp.getStationConfiguration();
	
	cacheProvider    = (CacheProvider) rsApp.getProvider(RSApp.CACHE_PROVIDER);
	metadataProvider = (MetadataProvider) rsApp.getProvider(RSApp.METADATA_PROVIDER);

	errorInference = ErrorInference.getErrorInference();

    }

    protected boolean onLaunch(){
	wpComponent.setLayout(new BorderLayout());

	bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	titleColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);
	
	wpComponent.setBackground(bgColor);

	RSTitleBar titleBar = new RSTitleBar(name, (int)wpSize.getWidth());
	titleBar.setBackground(titleColor);
	wpComponent.add(titleBar, BorderLayout.PAGE_START);

	cacheProvider.registerWithProvider(this);
	metadataProvider.registerWithProvider(this);

	if (cacheProvider == null || !cacheProvider.isActive())
	    isCacheActive = false;
	else
	    isCacheActive = true;

	if (metadataProvider == null || !metadataProvider.isActive())
	    isMetadataActive = false;
	else
	    isMetadataActive = true;

	buildWorkspace();
	return true;
    }


    protected void onMaximize(){

    }

    protected void onMinimize(){
	if (editorPopup != null)
	    editorPopup.setVisible(false);
    }
    
    public void onUnload(){
	wpComponent.removeAll();
	tableModel.clearData();

	cacheProvider.unregisterWithProvider(getName());
	metadataProvider.unregisterWithProvider(getName());
    }

    void buildWorkspace(){
	RSPanel contactsPanel = new RSPanel();

	Dimension contactsPanelSize = new Dimension(wpSize.width, wpSize.height - RSTitleBar.HEIGHT);
	contactsPanel.setBackground(bgColor);
	contactsPanel.setPreferredSize(contactsPanelSize);

	wpComponent.add(contactsPanel, BorderLayout.CENTER);

	contactsPanel.setLayout(new GridBagLayout());
	
	int searchBarVertFrac = 5;
	int actionBarVertFrac = 5;

	int searchBarHeight = searchBarVertFrac * contactsPanelSize.height/100;
	int actionBarHeight = actionBarVertFrac * contactsPanelSize.height/100;
	int contactsTableHeight = contactsPanelSize.height - searchBarHeight - actionBarHeight;

	RSPanel searchPanel = new RSPanel();
	Dimension searchPanelSize = new Dimension(wpSize.width, searchBarHeight);
	searchPanel.setPreferredSize(searchPanelSize);
	searchPanel.setBackground(bgColor);
	searchPanel.setLayout(new GridBagLayout());

	contactsPanel.add(searchPanel, getGbc(0, 0, gfillboth()));

	RSLabel searchLabel = new RSLabel("Search: ");
	searchLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	searchPanel.add(searchLabel);
	
	final JTextField searchField = new JTextField();
	Dimension searchFieldDimension  = new Dimension(wpSize.width/3, searchPanelSize.height - 4);
	searchField.setPreferredSize(searchFieldDimension);
	searchField.getDocument().addDocumentListener(new DocumentAdapter(){
		public void documentUpdated(DocumentEvent e){
		    searchStringUpdated(searchField.getText());
		}

	    });
	searchField.addKeyListener(new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			searchField.setText("");
		    }
		}
	    });
	
	searchPanel.add(searchField);
	
	Icon searchIcon = IconUtilities.getIcon(StationConfiguration.SEARCH_ICON, searchFieldDimension.height, searchFieldDimension.height);
	RSLabel searchIconLabel = new RSLabel(" ", searchIcon, SwingConstants.LEFT);
	searchIconLabel.setHorizontalTextPosition(SwingConstants.LEFT);
	searchPanel.add(searchIconLabel);

	tableModel = new ContactsTableModel(metadataProvider, cacheProvider);
	contactsTable = new JTable(tableModel);
	contactsTable.setFillsViewportHeight(true);
	contactsTable.getTableHeader().setReorderingAllowed(false);
	Color headerColor = GUIUtilities.toSaturation(bgColor, 0.05f);
	contactsTable.getTableHeader().setBackground(headerColor);

	contactsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
		public void valueChanged(ListSelectionEvent e){
		    selectionChanged();
		}
	    });

	contactsTable.setDragEnabled(true);
	contactsTable.addKeyListener(new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_DELETE){
			removeEntities();
		    }
		}
	    });

	TransferHandler transferHandler = new TransferHandler(){
		
		public int getSourceActions(JComponent c){
		    return COPY;
		}
		
		public Transferable createTransferable (JComponent c){
		    ArrayList<Entity> list = new ArrayList<Entity>();
		    
		    for (Entity entity: getSelection()){
			list.add(entity);
		    }
		    
		    return new EntitySelection(list);
		}

	    };
	
	contactsTable.setTransferHandler(transferHandler);


	JScrollPane contactsScrollPane = new JScrollPane(contactsTable);

	Dimension tableSize = new Dimension(wpSize.width, contactsTableHeight);
	contactsScrollPane.setPreferredSize(tableSize);
	contactsScrollPane.setBackground(headerColor);
	contactsPanel.add(contactsScrollPane, getGbc(0, 1, gfillboth(), weighty(1.0)));
	contactsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	contactsScrollPane.getVerticalScrollBar().setBackground(bgColor);

	RSPanel actionPanel = new RSPanel();
	actionPanel.setPreferredSize(new Dimension(wpSize.width, actionBarHeight));
	actionPanel.setBackground(bgColor);
	actionPanel.setLayout(new GridBagLayout());
	contactsPanel.add(actionPanel, getGbc(0, 2, gfillboth()));
	
	int buttonsWidthHorizFrac = 50;
	int buttonWidth = buttonsWidthHorizFrac * wpSize.width/100 / 4;

	Dimension buttonSize = new Dimension(buttonWidth, actionBarHeight - 4);
	
	int iconSize = (buttonSize.height * 8)/10;
	
	addButton = new RSButton("Add");
	addButton.setBackground(bgColor);
	addButton.setPreferredSize(buttonSize);
	addButton.setIcon(IconUtilities.getIcon(StationConfiguration.NEW_CREATOR_ICON, iconSize));
	addButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    editorPopup.showEditor(false, null, addButton);
		}
	    });
	actionPanel.add(addButton);

	RSLabel emptyLabel = new RSLabel(" ");
	actionPanel.add(emptyLabel);

	editButton = new RSButton("Edit");
	editButton.setBackground(bgColor);
	editButton.setPreferredSize(buttonSize);
	editButton.setIcon(IconUtilities.getIcon(StationConfiguration.EDIT_CREATOR_ICON, iconSize));
	editButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    editorPopup.showEditor(true, getSelection()[0], editButton);
		}
	    });

	editButton.setEnabled(false);
	actionPanel.add(editButton);
	
	emptyLabel = new RSLabel(" ");
	actionPanel.add(emptyLabel);

	removeButton = new RSButton("Remove");
	removeButton.setBackground(bgColor);
	removeButton.setPreferredSize(buttonSize);
	removeButton.setIcon(IconUtilities.getIcon(StationConfiguration.REMOVE_CREATOR_ICON, iconSize));
	removeButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    removeEntities();
		}
	    });
	removeButton.setEnabled(false);
	actionPanel.add(removeButton);

	emptyLabel = new RSLabel(" ");
	actionPanel.add(emptyLabel);

	exportButton = new RSButton("Export All");
	exportButton.setBackground(bgColor);
	exportButton.setPreferredSize(buttonSize);
	exportButton.setIcon(IconUtilities.getIcon(StationConfiguration.MAKE_REPORT_ICON, iconSize));
	exportButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    exportEntities();
		}
	    });
	actionPanel.add(exportButton);
	
	int editorHorizFrac = 50;
	int editorVertFrac = 30;
	int editorWidth = wpSize.width * editorHorizFrac/100;
	int editorHeight = wpSize.height * editorVertFrac/100;

	editorPopup = new EntityEditorPopup(this, editorWidth, editorHeight, bgColor, titleColor, stationConfig, rsApp);

	tableModel.reloadTable();

	contactsTable.addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    if (e.getClickCount() == 2){
			editorPopup.showEditor(true, getSelection()[0], editButton);
		    }
		}
	    });
    }

    public void addEntity(String name, String url, String phoneType, String phoneNumber, String email, String location, String entityType){
	logger.info("GRINS_USAGE: ADD_CONTACT_CONTACTS_WIDGET");
	RSCallerID callerID = new RSCallerID(phoneType, phoneNumber);
	metadataProvider.addEntity(name, url, phoneType, callerID.toDBString(), email, location, entityType);
    }

    public void editEntity(int id, String name, String url, String phoneType, String phoneNumber, String email, String location, String entityType){
	logger.info("GRINS_USAGE: EDIT_CONTACT_CONTACTS_WIDGET");
	RSCallerID callerID = new RSCallerID(phoneType, phoneNumber);
	metadataProvider.editEntity(id, name, url, phoneType, callerID.toDBString(), email, location, entityType);
    }

    public void entitiesChanged(){
	tableModel.reloadTable();
    }

    Entity[] getSelection(){
	ListSelectionModel model = contactsTable.getSelectionModel();

	int minIndex = model.getMinSelectionIndex();
	int maxIndex = model.getMaxSelectionIndex();

	if (minIndex < 0 || maxIndex < 0)
	    return new Entity[]{};

	ArrayList<Entity> list = new ArrayList<Entity>();
	
	for (int i = minIndex; i <= maxIndex; i++){
	    if (model.isSelectedIndex(i))
		list.add(tableModel.getEntityAt(i));
	}

	return list.toArray(new Entity[list.size()]);
    }

    void selectionChanged(){
	Entity selection[] = getSelection();
	
	if (selection.length == 0){
	    editButton.setEnabled(false);
	    removeButton.setEnabled(false);
	} else if (selection.length == 1){
	    editButton.setEnabled(true);
	    removeButton.setEnabled(true);
	} else if (selection.length > 1){
	    editButton.setEnabled(false);
	    removeButton.setEnabled(true);
	}
    }

    void exportEntities() {
	Entity[] entities = tableModel.getAllEntities();
	if(entities.length == 0)
	    return;

	String separator = "\t";
	ArrayList<String> entityList = new ArrayList<String>();
	
	JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	fileChooser.updateUI();
	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int retVal = fileChooser.showDialog(rsApp.getControlPanel().cpJFrame(), "Export Contacts");

	if (retVal != JFileChooser.APPROVE_OPTION)
	    return;

	String destFolder = null;
	try{
	    destFolder = fileChooser.getSelectedFile().getCanonicalPath() + System.getProperty("file.separator");
	} catch (Exception e){
	    logger.error("ExportEntities: Unable to find canonical path of selected folder.");
	    return;
	}

	String destFile = destFolder + CONTACTS_FILE_NAME;
	entityList.add("Name" + separator + "Phone No" + separator + "Location" + separator + "Email");

	for(Entity e :entities)
	    entityList.add(e.getName() + separator + e.getPhone() + separator + e.getLocation() + separator + e.getEmail());

	if(FileUtilities.writeToXLS(destFile, entityList, separator, true)) {
	    FileUtilities.setPermissions(destFile, false);
	    JOptionPane.showMessageDialog(null, "Contacts exported to file: " + destFile, "information", JOptionPane.INFORMATION_MESSAGE);
	} else {
	    JOptionPane.showMessageDialog(null, "Contacts export failed!!", "information", JOptionPane.INFORMATION_MESSAGE);
	}
    }

    void removeEntities(){
	Entity selection[] = getSelection();

	if (selection.length == 0)
	    return;
	
	String message = "Are you sure you want to remove " + selection.length + " selected contact(s)?";
	String title = "Remove Selected Contacts";
	int retVal = JOptionPane.showConfirmDialog(rsApp.getControlPanel().cpJFrame(), message, title, JOptionPane.YES_NO_OPTION);
	if (retVal != JOptionPane.YES_OPTION){
	    return;
	}

	for (Entity entity: selection){
	    int entityID = entity.getEntityID();
	    
	    Creator query = new Creator();
	    query.setEntityID(entityID);
	    Creator creators[] = metadataProvider.get(query);

	    for (Creator creator: creators){
		metadataProvider.remove(creator);
	    }
	    
	    metadataProvider.remove(entity);
	}

	tableModel.reloadTable();
	
    }

    static final int MIN_QUERY_LENGTH = 3;
    void searchStringUpdated(String query){
	if (query == null)
	    return;

	if (query.length() < MIN_QUERY_LENGTH)
	    query = "";

	tableModel.search(query);
    }

    /* Callbacks from Providers */
    
    public void activateMetadataService(boolean activate, String[] error){
	isMetadataActive = activate;
    }
    
    public void activateCache(boolean activate, String[] errorCodeAndParams){
	isCacheActive = activate;
    }

    public void cacheObjectsDeleted(String objectType, String[] objectID){}
    public void cacheObjectsUpdated(String objectType, String[] objectID){}
    public void cacheObjectsInserted(String objectType, String[] objectID){}

    /* End of Callbacks */
    
}


