package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.rscontroller.pipeline.RSPipeline;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.telephonylib.RSCallerID;

import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class DiagnosticsController implements ActionListener, PlayoutPreviewListenerCallback, 
					      ArchiverListenerCallback, LibraryListenerCallback, 
					      MediaLibUploadListenerCallback, MonitorListenerCallback,
					      DiagnosticUtilitiesPingCallback, MicListenerCallback, TelephonyListenerCallback {


    public static long COMMAND_TIMEOUT = 1000;
    public static String DIAGNOSTICS_PLAYOUT_FILE = "test.mp3";
    public static String DIAGNOSTICS_PREVIEW_FILE = "testtest.mp3";

    public static String CONFIG_FILE_ERROR = "Config file error";
    public static String DEPENDENT_ERROR = "";
    public static String RSC_NETWORK_ERROR = "RS Controller network error";
    public static String SERVICE_NETWORK_ERROR = "Service network error";
    public static String SERVICE_INTERNAL_ERROR = "Service internal error";
    public static String SERVICE_RESOURCE_ERROR = "Service resource busy";
    public static String SERVICE_DOWN_ERROR = "Service down error";
    public static String NO_ERROR = "";
    public static String CABLE_CONNECTION_ERROR = "Cable connection fault";
    public static String DISK_WRITE_ERROR = "Disk write error";
    public static String DISKSPACE_ERROR = "Running out of diskspace";
    public static String SOME_ERROR = "Unknown error";
    public static String DIAGNOSTICS_CONTROLLER = "DIAGNOSTICS_CONTROLLER";

    public static String ERROR_UNMATCHED_FREQUENCY = "Unmatched frequency";
    public static String ERROR_LOW_AUDIO_LEVEL = "Low audio level";

    protected static final long MIN_DISK_SPACE_THRESHOLD_B = 1 * 1024L * 1024L * 1024L; // 1GB

    protected DiagnosticsWidget diagnosticsWidget;
    protected ControlPanel controlPanel;
    protected RSApp rsApp;
    protected LogUtilities logger;

    PlayoutProvider playoutProvider;
    PreviewProvider previewProvider;
    ArchiverProvider archiverProvider;
    LibraryProvider libraryProvider;
    MediaLibUploadProvider uploadProvider;
    MonitorProvider monitorProvider;
    MicProvider micProvider;
    TelephonyProvider telephonyProvider;

    StationConfiguration stationConfig;
    StationNetwork stationNetwork;
    DiagnosticUtilities diagUtil;

    boolean micEnabled = false;
    boolean isTelephonyEnabled = false;
    
    boolean playoutProviderActive, previewProviderActive, archiverProviderActive, libraryProviderActive, uploadProviderActive, monitorProviderActive, micProviderActive, telephonyProviderActive;

    boolean playoutActive = false, previewActive = false, archivingActive = false;
    boolean monitorInOk = false, monitorActive = false, micInOk = false, micActive = false;
    String playoutError = "", previewError = "", monitorError = "", archiverError = "", libraryError = "", uploadError = "", micError = "", telephonyError = "";
    String shortPlayoutError = "", shortPreviewError = "", shortMonitorError = "", shortArchiverError = "", shortLibraryError = "", shortUploadError = "", shortMicError = "", shortTelephonyError;
    String playoutErrorCode = "", previewErrorCode = "", monitorErrorCode = "", archiverErrorCode = "", libraryErrorCode = "", uploadErrorCode = "", micErrorCode = "", telephonyErrorCode ="";
    boolean archivingOk = false;
    boolean uploadOk = false;
    String uploadLibFilename = "";

    boolean telephonyOk = false;
    Hashtable<String, String> statusOpts = new Hashtable<String, String>();
    boolean receivedPlayoutGstError = false, receivedPreviewGstError = false, receivedArchiverGstError = false, receivedMonitorGstError = false, receivedMicGstError = false;
    int archivingLevel, micLevel;
    
    ArrayList<String> connectedLines = new ArrayList<String>();
    ArrayList<String> disconnectedLines = new ArrayList<String>();
    
    protected Hashtable<String, Boolean> pingRequests;
    
    public DiagnosticsController(DiagnosticsWidget diagnosticsWidget, ControlPanel controlPanel, RSApp rsApp) {
	this.diagnosticsWidget = diagnosticsWidget;
	this.controlPanel = controlPanel;
	this.rsApp = rsApp;
	this.logger = new LogUtilities("DiagnosticsController");
	this.stationConfig = rsApp.getStationConfiguration();
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(rsApp.getStationConfiguration(), rsApp.getUIService().getIPCServer());
	micEnabled = stationConfig.isMicServiceActive();
	isTelephonyEnabled = stationConfig.isServiceActive(RSController.ONLINE_TELEPHONY_SERVICE);

	stationNetwork = stationConfig.getStationNetwork();
	pingRequests = new Hashtable<String, Boolean>();
    }

    public void init() {
	playoutProvider = rsApp.getProvider(PlayoutProvider.class);
	previewProvider = rsApp.getProvider(PreviewProvider.class);
	archiverProvider = rsApp.getProvider(ArchiverProvider.class);
	libraryProvider = rsApp.getProvider(LibraryProvider.class);
	uploadProvider = rsApp.getProvider(MediaLibUploadProvider.class);
	monitorProvider = rsApp.getProvider(MonitorProvider.class);

	if (micEnabled)
	    micProvider = rsApp.getProvider(MicProvider.class);
	if (isTelephonyEnabled)
	    telephonyProvider = rsApp.getProvider(TelephonyProvider.class);

	playoutProvider.registerWithProvider(this);
	if (playoutProvider == null || !playoutProvider.isActive())
	    playoutProviderActive = false;
	else
	    playoutProviderActive = true;

	previewProvider.registerWithProvider(this);
	if (previewProvider == null || !previewProvider.isActive())
	    previewProviderActive = false;
	else
	    playoutProviderActive = true;

	archiverProvider.registerWithProvider(this);
	if (archiverProvider == null || !archiverProvider.isActive())
	    archiverProviderActive = false;
	else
	    archiverProviderActive = true;

	libraryProvider.registerWithProvider(this);
	if (libraryProvider == null || !libraryProvider.isActive())
	    libraryProviderActive = false;
	else
	    libraryProviderActive = true;

	uploadProvider.registerWithProvider(this);
	if (uploadProvider == null || !uploadProvider.isActive())
	    uploadProviderActive = false;
	else
	    uploadProviderActive = true;

	monitorProvider.registerWithProvider(this);
	if (monitorProvider == null || !monitorProvider.isActive())
	    monitorProviderActive = false;
	else
	    monitorProviderActive = true;

	if (micEnabled) {
	    micProvider.registerWithProvider(this);
	    if (micProvider == null || !micProvider.isActive())
		micProviderActive = false;
	    else
		micProviderActive = true;
	}
	
	if (isTelephonyEnabled){
	    telephonyProvider.registerWithProvider(this);
	    if (telephonyProvider == null || !telephonyProvider.isActive())
		telephonyProviderActive = false;
	    else
		telephonyProviderActive = true;
	}
	
	diagnosticsWidget.setAllDetailsEnabled(false);

	playoutActive = false; previewActive = false; archivingActive = false;
	monitorInOk = false;
	playoutError = ""; previewError = ""; monitorError = ""; archiverError = ""; libraryError = ""; uploadError = "";
	playoutErrorCode = ""; previewErrorCode = ""; monitorErrorCode = ""; archiverErrorCode = ""; libraryErrorCode = ""; uploadErrorCode = "";
	shortPlayoutError = ""; shortPreviewError = ""; shortMonitorError = ""; shortArchiverError = ""; shortLibraryError = ""; shortUploadError = "";
	archivingOk = false;
	uploadOk = false;
	uploadLibFilename = "";
	receivedPlayoutGstError = false; receivedPreviewGstError = false; receivedArchiverGstError = false;
    }

    public void deinit() {
	playoutProvider.unregisterWithProvider(getName());
	previewProvider.unregisterWithProvider(getName());
	libraryProvider.unregisterWithProvider(getName());
	archiverProvider.unregisterWithProvider(getName());
	uploadProvider.unregisterWithProvider(getName());
	monitorProvider.unregisterWithProvider(getName());
	telephonyProvider.unregisterWithProvider(getName());
    }

    protected void refreshDiagnostics() {
	diagnosticsWidget.resetAllStatus();
	diagnosticsWidget.getNetworkButton().setEnabled(false);
	diagnosticsWidget.getDiagnosticsButton().setEnabled(false);

	logger.info("GRINS_USAGE:DIAGNOSTICS:");

	playoutDiagnostics();
	recordingDiagnostics();
	libraryDiagnostics();
	if (micEnabled)
	    micDiagnostics();
	if (isTelephonyEnabled)
	    telephonyDiagnostics();

	if (diagnosticsWidget.requiresRestart()){
	    String message = "An error has been detected in the system. Please restart the system to fix this.";
	    JOptionPane.showMessageDialog(rsApp.getControlPanel().cpJFrame(), message, "Restart system!!", JOptionPane.ERROR_MESSAGE);
	}

	diagnosticsWidget.getNetworkButton().setEnabled(true);
	diagnosticsWidget.getDiagnosticsButton().setEnabled(true);
    }
    
    public void actionPerformed(final ActionEvent e) {
	
	Thread t = new Thread("DiagnosticsControllerWorker"){
		public void run(){
		    diagnosticsWidget.disableInput();

		    String command = e.getActionCommand();
		    if (command.equals(DiagnosticsWidget.DIAGNOSTICS)) {
			if (diagnosticsWidget.isNetworkDisplay()) {
			    diagnosticsWidget.displaySystemDiagnostics();
			} else {
			    refreshDiagnostics();
			    pingNetwork();
			}
		    } else if (command.equals(DiagnosticsWidget.NETWORK)) {
			if (!diagnosticsWidget.isNetworkDisplay()) {
			    diagnosticsWidget.displayNetworkDiagnostics();
			} else {
			    refreshDiagnostics();
			    pingNetwork();
			}
		    } else if (command.startsWith(DiagnosticsWidget.DETAILS)) {
			String checkItemID = command.split(":")[1];
			
			JOptionPane.showMessageDialog(controlPanel.cpJFrame(), diagnosticsWidget.getDetailsString(checkItemID),
						      "Error details", JOptionPane.WARNING_MESSAGE);
		    }
		    
		    diagnosticsWidget.enableInput();
		    diagnosticsWidget.stopProgressDisplay();
		}
	    };

	t.start();
    }

    protected void stopPlayout() {
	playoutProvider.forceStop(this);
    }

    protected void stopPreview() {
	previewProvider.forceStop(this);
    }

    protected void stopArchive() {
	archiverProvider.forceStop(true);
    }

    protected void playoutDiagnostics() {
	diagnosticsWidget.startProgressDisplay("Testing Playout");
	audioDiagnostics(AudioService.PLAYOUT);
	diagnosticsWidget.startProgressDisplay("Testing Preview");
	audioDiagnostics(AudioService.PREVIEW);
	diagnosticsWidget.startProgressDisplay("Testing Monitor");
	monitorDiagnostics();
	diagnosticsWidget.stopProgressDisplay();
    }

    protected void audioDiagnostics(String playType) {
	PlayoutPreviewProvider audioProvider;
	String checkTypeReachable;
	String checkTypeOk;
	String checkTypeName;
	String filename;
	if (playType.equals(AudioService.PLAYOUT)) {
	    audioProvider = playoutProvider;
	    checkTypeReachable = DiagnosticsWidget.PLAYOUT_REACHABLE;
	    checkTypeOk = DiagnosticsWidget.PLAYOUT_OK;
	    checkTypeName = "Playout";
	    filename = DIAGNOSTICS_PLAYOUT_FILE;
	}
	else {
	    audioProvider = previewProvider;
	    checkTypeReachable = DiagnosticsWidget.PREVIEW_REACHABLE;
	    checkTypeOk = DiagnosticsWidget.PREVIEW_OK;
	    checkTypeName = "Preview";
	    filename = DIAGNOSTICS_PREVIEW_FILE;
	}

	while (true) {                                  // playout
	    if (audioProvider == null) {
		diagnosticsWidget.setStatus(checkTypeReachable, DiagnosticsWidget.NOTOK, 
					    CONFIG_FILE_ERROR, checkTypeName + " provider was not initialized");
		diagnosticsWidget.setStatus(checkTypeOk, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }
	    else if (!audioProvider.isActive()) {
		if (audioProvider.activate() == -1) {
		    diagnosticsWidget.setStatus(checkTypeReachable, DiagnosticsWidget.NOTOK,
						RSC_NETWORK_ERROR, "RS controller is not reachable");
		    diagnosticsWidget.setStatus(checkTypeOk, DiagnosticsWidget.NOTOK,
						DEPENDENT_ERROR, "");
		    break;
		} else {
		    try {
			synchronized(this) {
			    wait(COMMAND_TIMEOUT);
			}
		    } catch(InterruptedException e) { }

		    if (!audioProvider.isActive()) {
			// check failure codes from activateAudio
			String errorCode, shortAudioError;
			if (playType.equals(AudioService.PLAYOUT)) {
			    if (playoutErrorCode.equals("")) {
				errorCode = SERVICE_NETWORK_ERROR;
				shortAudioError = checkTypeName + " service is not reachable";
				playoutError += "\nPlease check your network connections to " + 
				    stationNetwork.getMachineID(StationNetwork.AUDIO_PLAYOUT) + 
				    " and also consult a Gram Vaani engineer to check your config file";
			    } else {
				errorCode = playoutErrorCode;
				shortAudioError = shortPlayoutError;
			    }
			} else {
			    if (previewErrorCode.equals("")) {
				errorCode = SERVICE_NETWORK_ERROR;
				shortAudioError = checkTypeName + " service is not reachable";
				previewError += "\nPlease check your network connections to " +
				    stationNetwork.getMachineID(StationNetwork.AUDIO_PREVIEW) + 
				    " and also consult a Gram Vaani engineer to check your config file";
			    } else {
				errorCode = previewErrorCode;
				shortAudioError = shortPreviewError;
			    }
			}

			diagnosticsWidget.setStatus(checkTypeReachable, DiagnosticsWidget.NOTOK, errorCode, shortAudioError);
			diagnosticsWidget.addDetailsString(checkTypeReachable, playType.equals(AudioService.PLAYOUT) ? playoutError : previewError);
			diagnosticsWidget.setStatus(checkTypeOk, DiagnosticsWidget.NOTOK, DEPENDENT_ERROR, "");
			break;
		    }
		}
	    }

	    diagnosticsWidget.setStatus(checkTypeReachable, DiagnosticsWidget.OK,
					NO_ERROR, checkTypeName + " service is reachable");
	    
	    if (audioProvider.isPlaying()) {
		logger.debug("AudioDiagnostics: " + filename + ": not playable");
		int choice = JOptionPane.showConfirmDialog(controlPanel.cpJFrame(), "A " + checkTypeName + 
							   " is already in progress. Close it and run diagnostics?",
							   checkTypeName + " test", JOptionPane.YES_NO_OPTION);
		if (choice == JOptionPane.YES_OPTION) {
		    audioProvider.forceStop(this);
		    try {
			synchronized(this) {
			    wait(COMMAND_TIMEOUT);
			}
		    } catch(InterruptedException e) { }

		} else {
		    break;
		}
	    }
	    
	    if (audioProvider.play(filename, this) == 0) {
		try {
		    synchronized(this) {
			//Needed to prevent error in preview. XXX fix preview taking too long later
			wait(2 * COMMAND_TIMEOUT); 
		    }
		} catch(InterruptedException e) { }
	    } else {
		// check failure codes
		// XXX if network error. else, possibility that states are inconsistent
		diagnosticsWidget.setStatus(checkTypeReachable, DiagnosticsWidget.NOTOK,
					    RSC_NETWORK_ERROR, "RS Controller is not reachable");
		diagnosticsWidget.setStatus(checkTypeOk, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		logger.debug("AudioDiagnostics: " + filename + ": Unable to play local error");
		
		break;
	    }
		
	    if (playType.equals(AudioService.PLAYOUT) && playoutActive || playType.equals(AudioService.PREVIEW) && previewActive) {
		int choice = JOptionPane.showConfirmDialog(controlPanel.cpJFrame(), "Can you hear the " + checkTypeName + 
							   " through your " + (playType.equals(AudioService.PLAYOUT) ? "radio set" : "headphones") + "?",
							   checkTypeName + " test", JOptionPane.YES_NO_OPTION);
		if (choice == JOptionPane.YES_OPTION) {
		    diagnosticsWidget.setStatus(checkTypeOk, DiagnosticsWidget.OK,
						NO_ERROR, checkTypeName + " service is working Ok");
		} else {
		    diagnosticsWidget.setStatus(checkTypeOk, DiagnosticsWidget.CRITICAL,
						CABLE_CONNECTION_ERROR, "Something seems to be wrong with your audio connections");
		}

		logger.debug("AudioDiagnostics: " + filename + ": play successful");

		if (playType.equals(AudioService.PREVIEW)) {
		    previewProvider.stop(filename, this);
		    previewActive = false;
		}
	    } else {
		// check failure codes from playActive
		String errorCode, shortAudioError;
		if (playType.equals(AudioService.PLAYOUT)) {
		    if (playoutErrorCode.equals("")) {
			errorCode = SOME_ERROR;
			shortAudioError = "Some error occured";
		    } else {
			errorCode = playoutErrorCode;
			shortAudioError = shortPlayoutError;
		    }
		} else {
		    if (previewErrorCode.equals("")) {
			errorCode = SOME_ERROR;
			shortAudioError = "Some error occured";
		    } else {
			errorCode = previewErrorCode;
			shortAudioError = shortPreviewError;
		    }
		}
		receivedPlayoutGstError = false;
		receivedPreviewGstError = false;
		
		diagnosticsWidget.setStatus(checkTypeOk, DiagnosticsWidget.NOTOK,
					    errorCode, shortAudioError);
		diagnosticsWidget.addDetailsString(checkTypeOk, playType.equals(AudioService.PLAYOUT) ? playoutError : previewError);

		logger.debug("AudioDiagnostics: " + filename + ": unable to play remote error");
	    }

	    break;
	}
    }

    public void monitorDiagnostics() {
	monitorInOk = false;
	while (stationConfig.isMonitorServiceActive()) {                                  // monitor
	    if (monitorProvider == null) {
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_REACHABLE, DiagnosticsWidget.NOTOK, 
					    CONFIG_FILE_ERROR, "Monitor provider was not initialized");
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_IN, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_OUT, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }
	    else if (!monitorProvider.isActive()) {
		if (monitorProvider.activate() == -1) {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_REACHABLE, DiagnosticsWidget.NOTOK,
						RSC_NETWORK_ERROR, "RS controller is not reachable");
		    diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_IN, DiagnosticsWidget.NOTOK,
						DEPENDENT_ERROR, "");
		    diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_OUT, DiagnosticsWidget.NOTOK,
						DEPENDENT_ERROR, "");
		    break;
		} else {
		    try {
			synchronized(this) {
			    wait(COMMAND_TIMEOUT);
			}
		    } catch(InterruptedException e) { }

		    if (!monitorProvider.isActive()) {
			// check failure codes from activateMonitor callback
			// if network failure
			diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_REACHABLE, DiagnosticsWidget.NOTOK,
						    SERVICE_NETWORK_ERROR, "Monitor service is not reachable");

			diagnosticsWidget.addDetailsString(DiagnosticsWidget.MONITOR_REACHABLE, monitorError);
			diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_IN, DiagnosticsWidget.NOTOK,
						    DEPENDENT_ERROR, "");
			diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_OUT, DiagnosticsWidget.NOTOK,
						    DEPENDENT_ERROR, "");
			break;
		    } 
		}
	    }

	    diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_REACHABLE, DiagnosticsWidget.OK,
					NO_ERROR, "Monitor service is reachable");
	    
	    
	    if (!playoutActive) {
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_IN, DiagnosticsWidget.UNTESTED,
					    DEPENDENT_ERROR, "Unable to test monitor without Playout");
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_OUT, DiagnosticsWidget.UNTESTED,
					    DEPENDENT_ERROR, "");
		break;
	    }

	    //Stop the regular monitoring for diagnostics.
	    if (monitorProvider.isMonitoring()) {
		monitorProvider.stopMonitor();

		try {
		    synchronized(this) {
			wait(COMMAND_TIMEOUT);
		    }
		} catch(InterruptedException e) { }
	    }
	    
	    if (monitorProvider.startMonitorDiagnostics()  == 0) {
		try {
		    synchronized(this) {
			wait(2 * COMMAND_TIMEOUT);

		    }
		} catch(InterruptedException e) { }
	    } else {
		// check failure codes
		// XXX if network error. else, possibility that states are inconsistent
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_REACHABLE, DiagnosticsWidget.NOTOK,
					    RSC_NETWORK_ERROR, "RS Controller is not reachable");
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_IN, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_OUT, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }
	    
	    monitorProvider.stopMonitor();

	    try {
		synchronized(this) {
		    wait(2*COMMAND_TIMEOUT);
		}
	    } catch(InterruptedException e) { }

	    
	    if (!monitorInOk) {
		// check failure codes from callback
		// if cable fault
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_IN, DiagnosticsWidget.NOTOK,
					    CABLE_CONNECTION_ERROR, "Something is wrong with your monitor IN cable");
		diagnosticsWidget.addDetailsString(DiagnosticsWidget.MONITOR_IN, monitorError);
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_OUT, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }


	    diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_IN, DiagnosticsWidget.OK,
					NO_ERROR, "Your monitor IN connections are Ok");

	    // send command to monitorProvider to stop analyzing: XXX not sure what this comment is for

	    //restart the regular monitor
	    monitorProvider.startMonitor();
	    try {
		synchronized(this) {
		    wait(COMMAND_TIMEOUT);
		}
	    } catch(InterruptedException e) { }
	    
	    int choice = JOptionPane.showConfirmDialog(controlPanel.cpJFrame(), "Can you hear the playout through your headphones?",
						       "Monitor test", JOptionPane.YES_NO_OPTION);
	    if (choice == JOptionPane.YES_OPTION) {
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_OUT, DiagnosticsWidget.OK,
					    NO_ERROR, "Your monitor OUT connections are Ok");
	    } else {
		diagnosticsWidget.setStatus(DiagnosticsWidget.MONITOR_OUT, DiagnosticsWidget.NOTOK,
					    CABLE_CONNECTION_ERROR, "Something is wrong with your monitor OUT cable");
	    }
	    
	    break;
	}
	
	if (playoutActive) {
	    playoutProvider.stop(DIAGNOSTICS_PLAYOUT_FILE, this);
	    playoutActive = false;
	}

	//restart regular monitor in case we exited the loop before restarting regular monitor
	if (!monitorProvider.isMonitoring())
	    monitorProvider.startMonitor();

    }

    protected void recordingDiagnostics() {
	diagnosticsWidget.startProgressDisplay("Testing Archiving. Please speak into the Microphone");
	while (true) {
	    if (archiverProvider == null) {
		diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_REACHABLE, DiagnosticsWidget.NOTOK, 
					    CONFIG_FILE_ERROR, "Archiver provider was not initialized");
		diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_OK, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }
	    else if (!archiverProvider.isActive()) {
		if (archiverProvider.activate() == -1) {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_REACHABLE, DiagnosticsWidget.NOTOK,
						RSC_NETWORK_ERROR, "RS controller is not reachable");
		    diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_OK, DiagnosticsWidget.NOTOK,
						DEPENDENT_ERROR, "");
		    break;
		} else {
		    try {
			synchronized(this) {
			    wait(COMMAND_TIMEOUT);
			}
		    } catch(InterruptedException e) { }

		    if (!archiverProvider.isActive()) {
			// check failure codes from activateArchiver callback
			if (archiverErrorCode.equals("")) {
			    archiverErrorCode = SERVICE_NETWORK_ERROR;
			    shortArchiverError = "Archiver service is not reachable";
			    archiverError += "\nPlease check your network connections to " +
				stationNetwork.getMachineID(StationNetwork.ARCHIVER) +
				" and also consult a Gram Vaani engineer to check your config file";
			}

			diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_REACHABLE, DiagnosticsWidget.NOTOK,
						    archiverErrorCode, shortArchiverError);
			diagnosticsWidget.addDetailsString(DiagnosticsWidget.ARCHIVER_REACHABLE, archiverError);
			diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_OK, DiagnosticsWidget.NOTOK,
						    DEPENDENT_ERROR, "");
			break;
		    } 
		}
	    }

	    diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_REACHABLE, DiagnosticsWidget.OK,
					NO_ERROR, "Archiver service is reachable");

	    diagnosticsWidget.refreshDisplay();

	    if (!archiverProvider.isArchivable()) {
		int choice = JOptionPane.showConfirmDialog(controlPanel.cpJFrame(), "A recording is already in progress. Close it and run diagnostics? You may temporarily not be able to access this recording.",
							   "Archiving test", JOptionPane.YES_NO_OPTION);
		if (choice == JOptionPane.YES_OPTION) {
		    micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE);
		    archiverProvider.forceStop(true);
		} else {
		    break;
		}
	    }
	    
	    if (archiverProvider.startArchiverDiagnostics(ArchiverService.RECORDING, getName(), false) == 0) {
		try {
		    synchronized(this) {
			wait(2 * COMMAND_TIMEOUT);

		    }
		} catch(InterruptedException e) { }
	    } else {
		// check failure codes
		// XXX if network error. else, possibility that states are inconsistent
		diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_REACHABLE, DiagnosticsWidget.NOTOK,
					    RSC_NETWORK_ERROR, "RS Controller is not reachable");
		diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_OK, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }
	    
	    if (archivingActive) {
		// send command to archiverProvider to begin analyzing
		// XXX callback from archiverProvider will initialize archivingOk and archiverError

		archiverProvider.stopArchive(ArchiverService.RECORDING, false);

		try {
		    synchronized(this) {
			wait(COMMAND_TIMEOUT);
		    }
		} catch(InterruptedException e) { }
		
		if (archivingOk) {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_OK, DiagnosticsWidget.OK,
						NO_ERROR, "Archiver service is working Ok");
		    diagnosticsWidget.addDetailsString(DiagnosticsWidget.ARCHIVER_OK, "Audio level recorded: "+archivingLevel+"dB");
		} else {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_OK, DiagnosticsWidget.CRITICAL,
						CABLE_CONNECTION_ERROR, "Something seems to be wrong with your audio connections");
		    diagnosticsWidget.addDetailsString(DiagnosticsWidget.ARCHIVER_OK, "Audio level recorded: "+archivingLevel+"dB");
		}


	    } else {
		// check failure codes from archiveActive
		if (archiverErrorCode.equals("")) {
		    archiverErrorCode = SOME_ERROR;
		    shortArchiverError = "Some error occured";
		}
		
		receivedArchiverGstError = false;
		
		diagnosticsWidget.setStatus(DiagnosticsWidget.ARCHIVER_OK, DiagnosticsWidget.NOTOK,
					    archiverErrorCode, shortArchiverError);
		diagnosticsWidget.addDetailsString(DiagnosticsWidget.ARCHIVER_OK, archiverError);
	    }
	    
	    break;
	}

	diagnosticsWidget.refreshDisplay();
	diagnosticsWidget.stopProgressDisplay();
    }

    protected void libraryDiagnostics() {
	diagnosticsWidget.startProgressDisplay("Testing Searching");
	searchDiagnostics();
	diagnosticsWidget.startProgressDisplay("Testing Upload");
	uploadDiagnostics();
	diagnosticsWidget.startProgressDisplay("Checking Disk space");
	diskSpaceDiagnostics();
	diagnosticsWidget.stopProgressDisplay();
    }
    
    protected void searchDiagnostics() {
	while (true) {                                  // monitor
	    if (libraryProvider == null) {
		diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_REACHABLE, DiagnosticsWidget.NOTOK, 
					    CONFIG_FILE_ERROR, "Library provider was not initialized");
		diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_OK, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }
	    else if (!libraryProvider.isActive()) {
		if (libraryProvider.activate() == -1) {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_REACHABLE, DiagnosticsWidget.NOTOK,
						RSC_NETWORK_ERROR, "RS controller is not reachable");
		    diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_OK, DiagnosticsWidget.NOTOK,
						DEPENDENT_ERROR, "");
		    break;
		} else {
		    try {
			synchronized(this) {
			    wait(COMMAND_TIMEOUT);
			}
		    } catch(InterruptedException e) { }

		    if (!libraryProvider.isActive()) {
			// check failure codes from activateSearch callback
			if (libraryErrorCode.equals("")) {
			    libraryErrorCode = SERVICE_NETWORK_ERROR;
			    shortLibraryError = "Library service is not reachable";
			    libraryError += "\nPlease check your network connections to " + 
				stationNetwork.getMachineID(StationNetwork.INDEX) + 
				" and also consult a Gram Vaani engineer to check your config file";
			}
			diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_REACHABLE, DiagnosticsWidget.NOTOK, libraryErrorCode, shortLibraryError);
			diagnosticsWidget.addDetailsString(DiagnosticsWidget.LIBRARY_REACHABLE, libraryError);
			diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_OK, DiagnosticsWidget.NOTOK,
						    DEPENDENT_ERROR, "");
			break;
		    } 
		}
	    }

	    diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_REACHABLE, DiagnosticsWidget.OK,
					NO_ERROR, "Library service is reachable");
	    
	    LibraryProvider.SearchDetails searchDetails = libraryProvider.getMatchingPrograms("", "", "", 0, 1);
	    if (searchDetails == null) {
		// XXX do we need to check for other failure codes here?
		diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_OK, DiagnosticsWidget.NOTOK,
					    SERVICE_NETWORK_ERROR, "There seems to a problem with reaching your Index service");
		libraryError += "Index service not reachable. \nPlease check your network connections to " +
		    stationNetwork.getMachineID(StationNetwork.INDEX) + 
		    " and also consult a Gram Vaani engineer to check your config file";
	    } else {
		diagnosticsWidget.setStatus(DiagnosticsWidget.LIBRARY_OK, DiagnosticsWidget.OK,
					    NO_ERROR, "Library service is working Ok");
	    }

	    break;
	}
    }

    protected void uploadDiagnostics() {
	while (true) {
	    if (uploadProvider == null) {
		diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_REACHABLE, DiagnosticsWidget.NOTOK, 
					    CONFIG_FILE_ERROR, "Upload provider was not initialized");
		diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_OK, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }
	    else if (!uploadProvider.isActive()) {
		if (uploadProvider.activate() == -1) {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_REACHABLE, DiagnosticsWidget.NOTOK,
						RSC_NETWORK_ERROR, "RS controller is not reachable");
		    diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_OK, DiagnosticsWidget.NOTOK,
						DEPENDENT_ERROR, "");
		    break;
		} else {
		    try {
			synchronized(this) {
			    wait(COMMAND_TIMEOUT);
			}
		    } catch(InterruptedException e) { }

		    if (!uploadProvider.isActive()) {
			// check failure codes from activateSearch callback
			if (uploadErrorCode.equals("")) {
			    uploadErrorCode = SERVICE_NETWORK_ERROR;
			    shortUploadError = "Upload service is not reachable";
			    uploadError += "\nPlease check your network connections to " +
				stationNetwork.getMachineID(StationNetwork.UPLOAD) + 
				" and also consult a Gram Vaani engineer to check your config file";
			}
			diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_REACHABLE, DiagnosticsWidget.NOTOK,
						    uploadErrorCode, shortUploadError);
			diagnosticsWidget.addDetailsString(DiagnosticsWidget.UPLOAD_REACHABLE, uploadError);
			diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_OK, DiagnosticsWidget.NOTOK,
						    DEPENDENT_ERROR, "");
			break;
		    } 
		}
	    }

	    diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_REACHABLE, DiagnosticsWidget.OK,
					NO_ERROR, "Upload service is reachable");
	    
	    uploadProvider.uploadFile(stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR) + "/" + DIAGNOSTICS_PLAYOUT_FILE, getName());
	    try {
		synchronized(this) {
		    wait(COMMAND_TIMEOUT * 4);
		}
	    } catch(InterruptedException e) { }
	    
	    if (uploadOk) {
		diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_OK, DiagnosticsWidget.OK,
					    NO_ERROR, "Upload service is working Ok");		

		// XXX remove uploadLibFilename locally and from database

	    } else {
		// check failure codes
		if (uploadErrorCode.equals("")) {
		    uploadErrorCode = SERVICE_NETWORK_ERROR;
		    shortUploadError = "There seems to be a problem with reaching your Upload service";
		    uploadError += "\nPlease check your network connections to " +
			stationNetwork.getMachineID(StationNetwork.UPLOAD) +
			" and also consult a Gram Vaani engineer to check your config file";
		}
		diagnosticsWidget.setStatus(DiagnosticsWidget.UPLOAD_OK, DiagnosticsWidget.NOTOK,
					    uploadErrorCode, shortUploadError);
		diagnosticsWidget.addDetailsString(DiagnosticsWidget.UPLOAD_OK, uploadError);
	    }
	    
	    break;
	}
    }

    public int getNumUniqueStores(){
	return stationConfig.getStationNetwork().getNumUniqueStores();
    }

    public String[] getUniqueStores(){
	return stationConfig.getStationNetwork().getUniqueStores();
    }

    protected void diskSpaceDiagnostics() {
	StationNetwork network = stationConfig.getStationNetwork();
	String[] uniqueStores = network.getUniqueStores();
	String[] stores = network.getStores();

	for (String uniqueStore: uniqueStores){
	    ArrayList<String> services = new ArrayList<String>();
	    for (String store: stores)
		if ((network.getStoreMachine(store)+network.getStoreDir(store)).equals(uniqueStore))
		    services.add(store);

	    String store = services.get(0);
	    String machine = network.getStoreMachine(store);

	    diagnosticsWidget.setStatus(DiagnosticsWidget.DISKSPACE_OK + machine, DiagnosticsWidget.UNTESTED, 
					DEPENDENT_ERROR, "Checking diskspace on machine "+machine);	

	    diskCheck(machine, network.getStoreDir(store));
	}

    }

    public synchronized void diskSpaceChecked(String machineIP, String dirName, long usedSpace, long availableSpace){
	logger.debug("DiskSpaceChecked: Machine: "+machineIP+" Dir: "+dirName+" Used "+usedSpace+" Avail "+availableSpace);

	if (dirName.equals("")) {
	    logger.error("DiskSpaceChecked: File store not found on " + machineIP);
	    diagnosticsWidget.setStatus(DiagnosticsWidget.DISKSPACE_OK + machineIP, DiagnosticsWidget.NOTOK, DISKSPACE_ERROR, "Disk info not available for " + machineIP);
	    diagnosticsWidget.addDetailsString(DiagnosticsWidget.DISKSPACE_OK + machineIP, "Something seems to be worng with the configuration settings. Please verify the configuration files on local machine as well as on " + machineIP);
	    return;
	}

	if (availableSpace < MIN_DISK_SPACE_THRESHOLD_B){
	    diagnosticsWidget.setStatus(DiagnosticsWidget.DISKSPACE_OK + machineIP, DiagnosticsWidget.NOTOK, DISKSPACE_ERROR, "Insufficient disk space on " + machineIP);
	    diagnosticsWidget.addDetailsString(DiagnosticsWidget.DISKSPACE_OK + machineIP, "You have only " + StringUtilities.bytesToReadable(availableSpace) + " left in directory " + dirName + " at " + machineIP + ".");
	} else {
	    diagnosticsWidget.setStatus(DiagnosticsWidget.DISKSPACE_OK + machineIP, DiagnosticsWidget.OK, NO_ERROR, "You have " + StringUtilities.bytesToReadable(availableSpace) + " left in directory " + dirName + " at " + machineIP + ".");
	}

	diagnosticsWidget.refreshDisplay();
    }

    protected void diskCheck(String machineIP, String dirName){
	String serviceName = stationConfig.getStationNetwork().getServiceForIP(machineIP);
	logger.debug("DiskCheck: Checking space on "+dirName+" on machine: "+machineIP+" through "+serviceName);

	MachineStatusRequestMessage statusRequestMessage = new MachineStatusRequestMessage(RSController.UI_SERVICE, serviceName, RSApp.LIBRARY_PROVIDER, DiagnosticUtilities.DISK_SPACE_MSG, new String[] {dirName});
	
	rsApp.getUIService().sendCommand(statusRequestMessage);
    }

    protected void telephonyDiagnostics(){
	diagnosticsWidget.startProgressDisplay("Testing telephony");
	connectedLines.clear();
	disconnectedLines.clear();

	if (telephonyProvider == null){
	    diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_REACHABLE, DiagnosticsWidget.NOTOK,
					CONFIG_FILE_ERROR, "Telephony provider was not initialized.");
	    diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_OK, DiagnosticsWidget.NOTOK,
					DEPENDENT_ERROR, "");
	    return;
	} else if (!telephonyProvider.isActive()){
		if (telephonyProvider.activate() == -1) {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_REACHABLE, DiagnosticsWidget.NOTOK,
						RSC_NETWORK_ERROR, "RS controller is not reachable");
		    diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_OK, DiagnosticsWidget.NOTOK,
						DEPENDENT_ERROR, "");
		    return;
		} else {
		    try {
			synchronized(this){
			    wait(COMMAND_TIMEOUT);
			}
		    } catch (InterruptedException e){}

		    if (!telephonyProvider.isActive()){
			diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_REACHABLE, DiagnosticsWidget.NOTOK,
						    SERVICE_NETWORK_ERROR, "Telephony service is not reachable");

			diagnosticsWidget.addDetailsString(DiagnosticsWidget.ONLINETELEPHONY_REACHABLE, telephonyError);
			diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_OK, DiagnosticsWidget.NOTOK,
						    DEPENDENT_ERROR, "");
			return;
		    }
		}
	}
	    
	diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_REACHABLE, DiagnosticsWidget.OK,
				    NO_ERROR, "Telephony service is reachable");
	

	try{
	    synchronized(this){
		if (!telephonyProvider.checkStatus()){
		    diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_OK, DiagnosticsWidget.NOTOK,
						SERVICE_INTERNAL_ERROR, "Unable to check PBX.");
		    return;
		}

		wait(5 * DiagnosticUtilities.PING_TIMEOUT);
	    }
	} catch (Exception e) {}

	if (telephonyOk){
	    String status = "PBX is ok.";
	    /*
	    if (StringUtilities.getArrayFromCSV(statusOpts.get(TelephonyEventMessage.DAHDI_OK)).length > 0){
		status += " Connected line #: "+statusOpts.get(TelephonyEventMessage.DAHDI_OK) + ". ";
	    }
	    if (StringUtilities.getArrayFromCSV(statusOpts.get(TelephonyEventMessage.DAHDI_ERROR)).length > 0){
		status += " Disconnected line #: "+statusOpts.get(TelephonyEventMessage.DAHDI_ERROR);
	    }
	    if (StringUtilities.getArrayFromCSV(statusOpts.get(TelephonyEventMessage.ATA_REGISTERED)).length > 0){
		status += " Registered ATAs: "+statusOpts.get(TelephonyEventMessage.ATA_REGISTERED) + ". ";
	    }
	    if (StringUtilities.getArrayFromCSV(statusOpts.get(TelephonyEventMessage.ATA_UNREGISTERED)).length > 0){
		status += " Unregistered ATAs: "+statusOpts.get(TelephonyEventMessage.ATA_UNREGISTERED);
	    }
	    */

	    if (connectedLines.size() > 0) {
		status += " Connected Lines:";
		String[] arr = connectedLines.toArray(new String[] {});
		status += StringUtilities.getCSVFromArray(arr);
		status += ".";
	    }

	    if (disconnectedLines.size() > 0) {
		status += " Disconnected Lines:";
		String[] arr = disconnectedLines.toArray(new String[]{});
		status += StringUtilities.getCSVFromArray(arr);
		status += ".";
	    }

	    diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_OK, DiagnosticsWidget.OK,
					NO_ERROR, status);
	    
	} else {
	    if (telephonyErrorCode.equals("")){
		telephonyErrorCode = SOME_ERROR;
		shortTelephonyError = "Unable to contact PBX software.";
		telephonyError += "Please check your connections to "+ stationNetwork.getMachineID(StationNetwork.ONLINETELEPHONY) + ".";
	    }
	    diagnosticsWidget.setStatus(DiagnosticsWidget.ONLINETELEPHONY_OK, DiagnosticsWidget.NOTOK,
					telephonyErrorCode, shortTelephonyError);
	    diagnosticsWidget.addDetailsString(DiagnosticsWidget.ONLINETELEPHONY_OK, telephonyError);
	}

	diagnosticsWidget.stopProgressDisplay();

    }


    /* TelephonyListener Callbacks */
    
    public synchronized void activateTelephony(boolean activate, String[] error){
	telephonyProviderActive = activate;
	if (activate) {
	    telephonyError = "";
	    shortTelephonyError = "";
	    telephonyErrorCode = "";
	} else {
	    String[] a = ErrorInference.getServiceErrorString("Unable to activate telephony", error);
	    telephonyError = a[2];
	    shortTelephonyError = a[1];
	    telephonyErrorCode = a[0];
	}
	notifyAll();
    }

    public synchronized void pbxStatusUpdate(boolean status, String[] error) {
	telephonyOk = status;
	
	if (status){
	    telephonyError = "";
	    shortTelephonyError = "";
	    telephonyErrorCode = "";
	} else {
	    String[] a = ErrorInference.getTelephonyErrorString("Error in PBX", error);
	    telephonyError = a[2];
	    shortTelephonyError = a[1];
	    telephonyErrorCode = a[0];
	}

	notifyAll();
    }

    public synchronized void lineIDUpdate(String devicePortName, String status) {
	if (status.equals(TelephonyProvider.SUCCESS))
	    connectedLines.add(devicePortName);
	else
	    disconnectedLines.add(devicePortName);
    }

    public void enableTelephonyUI(boolean enable, String[] error){

    }

    public void telephonyOffline(boolean success, String[] error){

    }

    public void telephonyOnline(boolean success, String[] error){

    }

    public void callAccepted(boolean success, String callGUID, String programID, String[] error){

    }

    public void callHungup(boolean success, String callGUID, String[] error){

    }

    public void callerHungup(String callGUID, String reason){

    }

    public void newCall(String callGUID, RSCallerID callerID){

    }

    public void callOnAir(boolean success, String programID, long telecastTime, String[] error){

    }

    public void callOffAir(boolean success, String[] error){

    }

    public void callDialed(boolean success, String extension, String[] error){

    }

    public void confEnabled(boolean success, String monitorFile, String[] error){

    }

    public void confDisabled(boolean success, String[] error){

    }

    public void updateOutgoingCall(String callGUID, String newState, String... options){

    }



    /* End of TelephonyListener Callbacks */

    protected void micDiagnostics() {
	diagnosticsWidget.startProgressDisplay("Testing Microphone. Please speak into the microphone");

	while (true){
	    if (micProvider == null){
		diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_REACHABLE, DiagnosticsWidget.NOTOK,
					    CONFIG_FILE_ERROR, "Mic provider was not initialized.");
		diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_OK, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;

	    } else if (!micProvider.isActive()){
		if (micProvider.activate() == -1) {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_REACHABLE, DiagnosticsWidget.NOTOK,
						RSC_NETWORK_ERROR, "RS controller is not reachable");
		    diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_OK, DiagnosticsWidget.NOTOK,
						DEPENDENT_ERROR, "");
		    break;
		} else {
		    try {
			synchronized(this){
			    wait(COMMAND_TIMEOUT);
			}
		    } catch (InterruptedException e){}

		    if (!micProvider.isActive()){
			//check failure codes?
			diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_REACHABLE, DiagnosticsWidget.NOTOK,
						    SERVICE_NETWORK_ERROR, "Mic service is not reachable");

			diagnosticsWidget.addDetailsString(DiagnosticsWidget.MIC_REACHABLE, micError);
			diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_OK, DiagnosticsWidget.NOTOK,
						    DEPENDENT_ERROR, "");
			break;
		    }
		}
	    }
	    
	    diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_REACHABLE, DiagnosticsWidget.OK,
					NO_ERROR, "Mic service is reachable");
	    
	    if (!micProvider.isStartable()) {
		//skip the test as archiving is also on and user decided not to stop it.
		break;
	    }

	    if (micProvider.enableDiagnostics(getName()) == 0){
		try{
		    synchronized(this){
			wait(COMMAND_TIMEOUT);
		    }
		} catch (InterruptedException e){}
		
		int choice = JOptionPane.showConfirmDialog(controlPanel.cpJFrame(), "Can you hear what you are speaking into your microphone through your radio set?", "Mic test", JOptionPane.YES_NO_OPTION);
		if (choice == JOptionPane.YES_OPTION) {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_OK, DiagnosticsWidget.OK,
						NO_ERROR,  "Mic service is working Ok");
		    diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_IN, DiagnosticsWidget.OK, NO_ERROR,
						"Mic input is working OK");
		} else {
		    diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_OK, DiagnosticsWidget.CRITICAL,
						SOME_ERROR, "Something seems to be wrong with your audio connections");
		}
		
		micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE);

		// analysis starts now.

		//Do mic in status update only if user could not hear anything on his radio set
		if (choice != JOptionPane.YES_OPTION) {
		    try{
			synchronized(this){
			    wait(COMMAND_TIMEOUT);
			}
		    } catch (InterruptedException e){}
		
		    if (micInOk){
			diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_IN, DiagnosticsWidget.OK, NO_ERROR,
						"Mic input is working OK");
		    } else {
			diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_IN, DiagnosticsWidget.NOTOK, CABLE_CONNECTION_ERROR,
						    "Something is wrong with your Mic input");
			diagnosticsWidget.addDetailsString(DiagnosticsWidget.MIC_IN, micError);
			diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_OK, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		    }
		}
		
		break;
	    } else {
		//check failure codes
		//XXX
		diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_REACHABLE, DiagnosticsWidget.NOTOK,
					    RSC_NETWORK_ERROR, "RS Controller is not reachable");
		diagnosticsWidget.setStatus(DiagnosticsWidget.MIC_OK, DiagnosticsWidget.NOTOK,
					    DEPENDENT_ERROR, "");
		break;
	    }

	}

	diagnosticsWidget.stopProgressDisplay();
    }

    /* Mic Listener Callback */

    public void micDisabled(boolean success, String[] error) {
	micActive = false;
    }

    public void micEnabled(boolean success, String[] error) {
	micActive = success;

	if (!success){
	    if (!receivedMicGstError){
		String[] a = ErrorInference.getServiceErrorString("Unable to start mic", error);
		micError = a[2];
		shortMicError = a[1];
		micErrorCode = a[0];
	    }
	} else {
	    micError = "";
	    shortMicError = "";
	    micErrorCode = "";
	}

    }

    public void micLevelTested(boolean success, int level){
	micInOk = success;
	micLevel = level;
	
	if (!success){
	    String[] a = ErrorInference.getDiagnosticsErrorString("Unable to detect expected mic sound levels", new String[] {DiagnosticsController.ERROR_LOW_AUDIO_LEVEL});
	    micError = a[2];
	    shortMicError = a[1];
	    micErrorCode = a[0];
	} else {
	    micError = "";
	    shortMicError = "";
	    micErrorCode = "";
	}
    }

    public void micGstError(String error) {
	String[] a = ErrorInference.getGstErrorString("Unable to test mic", error);
	micError = a[2];
	shortMicError = a[1];
	micErrorCode = a[0];
	micActive = false;
	receivedMicGstError = true;
    }

    public void enableMicUI(boolean enable, String[] error) {
	
    }

    public synchronized void activateMic(boolean activate, String[] error) {
	micProviderActive = activate;
	if (activate) {
	    micError = "";
	    shortMicError = "";
	    micErrorCode = "";
	} else {
	    String[] a = ErrorInference.getServiceErrorString("Unable to activate mic", error);
	    micError = a[2];
	    shortMicError = a[1];
	    micErrorCode = a[0];
	}
	notifyAll();
    }

    /*End of Mic Listener callback */

    protected synchronized void pingNetwork() {
	String rscIP = stationConfig.getMachineIP(StationConfiguration.RSCONTROLLER_MACHINE);
	logger.debug("PingNetwork: Pinging machine: "+rscIP);
	pingRequests.remove(rscIP);
	diagUtil.initiatePing(rscIP, this);
	try {
	    wait(DiagnosticUtilities.PING_TIMEOUT);
	} catch(InterruptedException e) { }
	
	if (pingRequests.get(rscIP) != null && pingRequests.get(rscIP).booleanValue()) {
	    diagnosticsWidget.showNetworkLink(StationConfiguration.RSAPP_MACHINE, DiagnosticsWidget.OK);
	    
	    if (sendMachinePingRequest(stationNetwork.getMachines().size())) {
		for (String machineID: stationNetwork.getMachines())
		    if (!machineID.equals(StationConfiguration.RSAPP_MACHINE)) {
			if (pingRequests.get(stationConfig.getMachineIP(machineID)) != null &&
			   pingRequests.get(stationConfig.getMachineIP(machineID)).booleanValue())
			    diagnosticsWidget.showNetworkLink(machineID, DiagnosticsWidget.OK);
			else
			    diagnosticsWidget.showNetworkLink(machineID, DiagnosticsWidget.NOTOK);
		    }
		pingRequests.clear();
	    } else {
		for (String machineID: stationNetwork.getMachines())
		    if (!machineID.equals(StationConfiguration.RSAPP_MACHINE))
			diagnosticsWidget.showNetworkLink(machineID, DiagnosticsWidget.NOTOK);
	    }

	} else {
	    diagnosticsWidget.showNetworkLink(StationConfiguration.RSAPP_MACHINE, DiagnosticsWidget.NOTOK);
	    for (String machineID: stationNetwork.getMachines()) {
		if (!machineID.equals(StationConfiguration.RSAPP_MACHINE))
		    diagnosticsWidget.showNetworkLink(machineID, DiagnosticsWidget.NOTOK);
	    }
	}
	
	diagnosticsWidget.getNetworkDiagPanel().repaint();
    }

    public synchronized void pingResponse(String ipAddress, boolean status) {
	pingRequests.put(ipAddress, new Boolean(status));
	notifyAll();
    }

    protected boolean sendMachinePingRequest(int numMachines) {
	MachinePingRequestMessage pingRequestMessage = new MachinePingRequestMessage(IPCSyncMessage.SYNC_MESSAGE,
										     RSController.RESOURCE_MANAGER,
										     new String[]{});
	MachinePingResponseMessage pingResponseMessage = null;
	try {
	    pingResponseMessage = (MachinePingResponseMessage)(rsApp.getUIService().sendSyncCommand(pingRequestMessage,
												    DiagnosticUtilities.PING_TIMEOUT * numMachines));
	} catch(IPCSyncMessageTimeoutException e) {
	    logger.error("sendPingRequest: Caught sync message timeout exception", e);
	}
	
	if (pingResponseMessage != null) {
	    Hashtable<String, Boolean> serviceConnStatus = pingResponseMessage.getMachineConnStatus();
	    Enumeration<String> e = serviceConnStatus.keys();
	    while (e.hasMoreElements()) {
		String machineID = e.nextElement();
		Boolean status = serviceConnStatus.get(machineID);
		pingRequests.put(stationConfig.getMachineIP(machineID), status);
	    }

	    return true;
	}

	return false;
    }
    
    /* PlayoutPreviewListener callbacks */

    public synchronized void playActive(boolean success, String programID, String playType, long telecastTime, String[] error) {
	if (playType.equals(AudioService.PLAYOUT)) {
	    playoutActive = success;
	    if (!success) {
		if (!receivedPlayoutGstError) {
		    String[] a = ErrorInference.getServiceErrorString("Unable to play", error);
		    playoutError = a[2];
		    shortPlayoutError = a[1];
		    playoutErrorCode = a[0];
		}
	    } else {
		playoutError = "";
		shortPlayoutError = "";
		playoutErrorCode = "";
	    }
	} else {
	    previewActive = success;
	    if (!success) {
		if (!receivedPreviewGstError) {
		    String[] a = ErrorInference.getServiceErrorString("Unable to play", error);
		    previewError = a[2];
		    shortPreviewError = a[1];
		    previewErrorCode = a[0];
		}
	    } else {
		previewError = "";
		shortPreviewError = "";
		previewErrorCode = "";
	    }
	}
    }

    public synchronized void pauseActive(boolean success, String programID, String playType, String[] error) {

    }

    public synchronized void stopActive(boolean success, String programID, String playType, String[] error) {
	// error will show up in the next diagnostics test
    }

    public synchronized void playDone(boolean success, String programID, String playType) {
	if (playType.equals(AudioService.PLAYOUT)) {
	    playoutActive = false;
	} else {
	    previewActive = false;
	}
    }

    // for playout and preview
    public synchronized void audioGstError(String programID, String error, String playType) {
	if (playType.equals(AudioService.PLAYOUT)) {
	    String[] a = ErrorInference.getGstErrorString("Unable to test " + playType, error);
	    playoutError = a[2];
	    shortPlayoutError = a[1];
	    playoutErrorCode = a[0];
	    playoutActive = false;
	    receivedPlayoutGstError = true;
	} else {
	    String[] a = ErrorInference.getGstErrorString("Unable to test " + playType, error);
	    previewError = a[2];
	    shortPreviewError = a[1];
	    previewErrorCode = a[0];
	    previewActive = false;
	    receivedPreviewGstError = true;
	}
    }

    public synchronized void activateAudio(boolean activate, String playType, String[] error) {
	if (playType.equals(AudioService.PLAYOUT)) {
	    playoutProviderActive = activate;
	    if (activate) {
		playoutError = "";
		shortPlayoutError = "";
		playoutErrorCode = "";
	    } else {
		String[] a = ErrorInference.getServiceErrorString("Unable to activate playout", error);
		playoutError = a[2];
		shortPlayoutError = a[1];
		playoutErrorCode = a[0];
	    }
	    notifyAll();
	} else {
	    // if failure, then this should also initialize the error codes
	    previewProviderActive = activate;
	    if (activate) {
		previewError = "";
		shortPreviewError = "";
		previewErrorCode = "";
	    } else {
	        String[] a = ErrorInference.getServiceErrorString("Unable to activate preview", error);
		previewError = a[2];
		shortPreviewError = a[1];
		previewErrorCode = a[0];
	    }
	    notifyAll();
	}
    }

    public synchronized void enableAudioUI(boolean enable, String playType, String[] error) {
	if (!enable) {
	    if (playType.equals(AudioService.PLAYOUT)) {
		if (!receivedPlayoutGstError && error.length > 0) {
		    String[] a = ErrorInference.getServiceErrorString("Unable to do playout", error);
		    playoutError = a[2];
		    shortPlayoutError = a[1];
		    playoutErrorCode = a[0];
		}	    
		
		playoutActive = false;
	    } else {
		if (!receivedPreviewGstError && error.length > 0) {
		    String[] a = ErrorInference.getServiceErrorString("Unable to do preview", error);
		    previewError = a[2];
		    shortPreviewError = a[1];
		    previewErrorCode = a[0];
		}

		previewActive = false;
	    }
	}
    }

    public synchronized void playoutPreviewError(String errorCode, String[] errorParams) {
    	if (errorCode != null && errorParams != null && errorParams.length > 0 && !receivedPlayoutGstError && !receivedPreviewGstError) {
    	    String[] error = new String[errorParams.length + 1];
    	    error[0] = errorCode;
    	    for (int i = 1; i <= errorParams.length; i++)
    		error[i] = errorParams[i -1 ];
    	    String[] a = ErrorInference.getServiceErrorString("Error from playout/preview provider", error);
    	    playoutError = previewError = a[2];
    	    shortPlayoutError = shortPreviewError = a[1];
    	    playoutErrorCode = previewErrorCode = a[0];
	    
    	    playoutActive = false;	    
    	    previewActive = false;
    	}
    }


    /* End of PlayoutPreviewListener callbacks */


    /* MonitorListener Callback */

    public synchronized void activateMonitor(boolean activate, String[] error) {
	monitorProviderActive = activate;

	if (activate) {
	    monitorError = "";
	    shortMonitorError = "";
	    monitorErrorCode = "";
	} else {
	    String[] a = ErrorInference.getServiceErrorString("Unable to activate monitor", error);
	    monitorError = a[2];
	    shortMonitorError = a[1];
	    monitorErrorCode = a[0];
	}
	notifyAll();
    }

    public synchronized void monitorStarted(boolean success, String[] error) {
	monitorActive = success;

	if (!success){
	    if (!receivedMonitorGstError){
		String[] a = ErrorInference.getServiceErrorString("Unable to start monitor", error);
		monitorError = a[2];
		shortMonitorError = a[1];
		monitorErrorCode = a[0];
	    }
	} else {
	    monitorError = "";
	    shortMonitorError = "";
	    monitorErrorCode = "";
	}
	    
    }

    public synchronized void monitorStopped(boolean success, String[] error) {
	monitorActive = false;
    }

    public synchronized void monitorGstError(String error) {
	String[] a = ErrorInference.getGstErrorString("Unable to test monitor", error);
	monitorError = a[2];
	shortMonitorError = a[1];
	monitorErrorCode = a[0];
	monitorActive = false;
	receivedMonitorGstError = true;
    }

    public synchronized void monitorFrequencyTested(boolean success){
	monitorInOk = success;
	if (!success){
	    String[] a = ErrorInference.getDiagnosticsErrorString("Unable to match monitor frequency", new String[] {DiagnosticsController.ERROR_UNMATCHED_FREQUENCY});
	    monitorError = a[2];
	    shortMonitorError = a[1];
	    monitorErrorCode = a[0];
	}
    }
    /* End of Monitor Listener callbacks */


    /* ArchiverListener Callbacks */

    public synchronized void activateArchiver(boolean activate, String error[]) {
	archiverProviderActive = activate;
	if (!activate) {
	    String[] a = ErrorInference.getServiceErrorString("Unable to activate archiver", error);
	    archiverError = a[2];
	    shortArchiverError = a[1];
	    archiverErrorCode = a[0];
	} else {
	    archiverError = "";
	    shortArchiverError = "";
	    archiverErrorCode = "";
	}
    }

    public synchronized void enableArchiverUI(boolean enable, String[] error) { //used for setting accessibility/clickability of UI
	if (!enable) {
	    if (!receivedArchiverGstError && error.length > 0) {
		String[] a = ErrorInference.getServiceErrorString("Unable to do archiving", error);
		archiverError = a[2];
		shortArchiverError = a[1];
		archiverErrorCode = a[0];
	    }

	    archivingActive = false;
	}
    }

    public synchronized void archiveStarted(boolean activate, String[] error) {
	archivingActive = activate;
	if (!activate) {
	    if (!receivedArchiverGstError) {
		String[] a = ErrorInference.getServiceErrorString("Unable to start archiving", error);
		archiverError = a[2];
		shortArchiverError = a[1];
		archiverErrorCode = a[0];
	    }
	} else {
	    archiverError = "";
	    shortArchiverError = "";
	    archiverErrorCode = "";
	}
    }

    public synchronized void archiverLevelTested(boolean success, int level){
	archivingOk = success;
	archivingLevel = level;

	if (!success){
	    String[] a = ErrorInference.getDiagnosticsErrorString("Unable to detect expected archiver sound levels", new String[] {DiagnosticsController.ERROR_LOW_AUDIO_LEVEL});
	    archiverError = a[2];
	    shortArchiverError = a[1];
	    archiverErrorCode = a[0];
	}
    }

    public synchronized void archiveStopped(boolean activate, String[] error) {
	// error will show up in the next diagnostics test
    }

    public synchronized void archivingDone(String filename, boolean success, long startTime, String error) {
	// error will show up in the next diagnostics test
    }


    // for archiver
    public synchronized void archiverGstError(String error) {
	archivingActive = false;
	String[] a = ErrorInference.getGstErrorString("Unable to archive", error);
	archiverError = a[2];
	shortArchiverError = a[1];
	archiverErrorCode = a[0];
	receivedArchiverGstError = true;
    }

    /*End of ArchiverListener Callbacks */


    public synchronized void activateSearch(boolean activate, String[] error) {
	libraryProviderActive = activate;
	if (!activate) {
	    String[] a = ErrorInference.getServiceErrorString("Unable to activate search", error);
	    libraryError = a[2];
	    shortLibraryError = a[1];
	    libraryErrorCode = a[0];
	} else {
	    libraryError = "";
	    shortLibraryError = "";
	    libraryErrorCode = "";
	}
    }

    /* MediaLibUploadListener callbacks */

    public synchronized void activateMediaLibUpload(boolean activate, String[] error) {
	// if failure, then this should also initialize the error codes
	uploadProviderActive = activate;
	if (!activate) {
	    String[] a = ErrorInference.getServiceErrorString("Unable to activate upload", error);
	    uploadError = a[2];
	    shortUploadError = a[1];
	    uploadErrorCode = a[0];
	} else {
	    uploadError = "";
	    shortUploadError = "";
	    uploadErrorCode = "";
	}	
    }

    public void uploadGiveup(String fileName) {
    }

    public synchronized void uploadError(String fileName, String error, int numRemaining) {
	String[] a = ErrorInference.getUploadErrorString("Unable to upload file", new String[]{error});
	uploadError = a[2];
	shortUploadError = a[1];
	uploadErrorCode = a[0];
	uploadOk = false;
    }

    public synchronized void uploadSuccess(String fileName, String libFileName, int numRemaining, boolean isDuplicate) {
	uploadError = "";
	shortUploadError = "";
	uploadErrorCode = "";
	uploadLibFilename = libFileName;
	uploadOk = true;
    }

    public synchronized void updateProgress(String fileName, long uploadedLength, long totalLength) {
	
    }

    /* End of MediaLibUploadListener callbacks */
    

    public String getName() {
	return DIAGNOSTICS_CONTROLLER;
    }

}
