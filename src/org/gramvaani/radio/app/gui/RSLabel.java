package org.gramvaani.radio.app.gui;

import javax.swing.*;
import java.awt.*;

public class RSLabel extends JLabel {

    public RSLabel(){
	super();
    }

    public RSLabel(String str) {
	super(str);
    }

    public RSLabel(String text, Icon icon, int horizontalAlignment){
	super(text, icon, horizontalAlignment);
    }

}