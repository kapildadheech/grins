package org.gramvaani.radio.app.gui;

import org.gramvaani.utilities.IconUtilities;
import org.gramvaani.radio.stationconfig.StationConfiguration;

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.metal.MetalLookAndFeel;

public class CheckBoxIcon implements Icon {
    Color bgColor;
    Component c;
    ImageIcon checkIcon, autoCheckIcon;

    static final int boxInset = 3;
    Color boxColor;

    public CheckBoxIcon(boolean isEdit){
	float brightness;
	if (isEdit)
	    brightness = 0.5f;
	else
	    brightness = 0.8f;
	boxColor = GUIUtilities.toBrightness(Color.lightGray, brightness);
	checkIcon = IconUtilities.getIcon(StationConfiguration.CHECK_ICON, getControlSize(), getControlSize());
	autoCheckIcon = new ImageIcon(GrayFilter.createDisabledImage(checkIcon.getImage()));
    }

    protected int getControlSize(){
	return 26;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
	JCheckBox cb = (JCheckBox)c;
	cb.setFont(new Font("Lucida",Font.BOLD,10*getControlSize()/13));
	TagTree.TagCheckBoxButtonModel model = (TagTree.TagCheckBoxButtonModel) cb.getModel();
	int controlSize = getControlSize();
	
	Graphics2D g2 = (Graphics2D) g;
	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	g2.setColor(boxColor);
	int curve = 6;
	int boxSize = getControlSize() - 2*boxInset - 2;
	g2.drawRoundRect(x + boxInset, y + boxInset, boxSize, boxSize, curve, curve);

	if (model.isSelected()){
	    g2.drawImage(checkIcon.getImage(), 0, 0, cb);
	} else if (model.getAutoSelected()){
	    g2.drawImage(autoCheckIcon.getImage(), 0, 0, cb);
	}

    }
    
    public int getIconWidth(){return getControlSize();}
    public int getIconHeight(){return getControlSize();}
}
