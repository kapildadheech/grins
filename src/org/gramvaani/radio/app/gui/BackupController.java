package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MediaLibUploadProvider;
import org.gramvaani.radio.app.providers.MediaLibUploadListenerCallback;
import org.gramvaani.radio.rscontroller.services.UIService;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.RefreshIndexMessage;
import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.utilities.*;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.io.*;
import java.util.*;

import static org.gramvaani.utilities.FileUtilities.copyFile;

class BackupController implements MediaLibUploadListenerCallback {
    static BackupController backupController;

    UIService uiService;
    StationConfiguration stationConfig;
    ProgressListener progress;
    ErrorInference errorInference;
    MediaLibUploadProvider uploadProvider;
    FileStoreDownloader fileStoreDownloader;

    int lastBlockSize = -1;
    LogUtilities logger;
    ArrayList<BackupEntry> entries;
    ArrayList<Integer> volumes;
    Hashtable<Integer, ArrayList<String>> volumeDiffs;
    HashSet<String> uploadSuccessFiles, uploadFailFiles;

    MediaLib medialib;
    String configFileName;
    String importConf;
    String backupDirName;
    String libDirName;
    String localStoreName;
    String tempDir;
    int totalFilesToUpload = 0;

    String dbName, login, password, dbHost, dbPort;

    static final int NUM_DIFFS_PER_VOLUME = 3;
    static final String BACKUP_PREFIX = "GB_";
    static final String TEMP_DIR_STRING = "java.io.tmpdir";
    static final String BACKUP_CONTROLLER = "BackupController";
    static final int INTER_UPDATE_ATTEMPT_SLEEP_TIME = 500; //in millisecs
    static final int MAX_DB_UPDATE_ATTEMPTS = 3;
    static final String GRINS_PUB_KEY = "grins_rsa.pub";
    static final String GRINS_PRI_KEY = "grins_rsa";

    final String EXEC_MYSQLDUMP;
    final String EXEC_MYSQL;
    final String EXEC_DIFF;
    final String EXEC_PATCH;

    protected BackupController(RSApp rsApp, StationConfiguration stationConfig, ProgressListener progress){
	this.uiService = rsApp.getUIService();
	this.stationConfig = stationConfig;
	this.progress = progress;
	errorInference = ErrorInference.getErrorInference();

	logger = new LogUtilities(BACKUP_CONTROLLER);
	entries = new ArrayList<BackupEntry>();
	uploadSuccessFiles = new HashSet<String>();	
	uploadFailFiles = new HashSet<String>();

	backupDirName = stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR) + File.separator + "backupinfo";
	try {
	    File backup = new File(backupDirName);
	    if (!backup.exists()){
		backup.mkdir();
		new FileWriter(backupDirName + File.separator + "0.sql");
	    }
	} catch (Exception e){
	    logger.error("BackupController: Error initializing backup dir.", e);
	}

	volumes = new ArrayList<Integer>();
	volumeDiffs = new Hashtable<Integer, ArrayList<String>>();

	configFileName = backupDirName + File.separator + "backup.conf";
	importConf = backupDirName + File.separator + "import.conf";
	dbName = stationConfig.getStringParam(StationConfiguration.LIB_DB_NAME);
	login  = stationConfig.getStringParam(StationConfiguration.LIB_DB_LOGIN);
	password= stationConfig.getStringParam(StationConfiguration.LIB_DB_PASSWORD);
	String dbServer = stationConfig.getStringParam(StationConfiguration.LIB_DB_SERVER);
	libDirName = stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR);
	localStoreName = ((StationNetwork)stationConfig.getStationNetwork()).getALocalStoreName();
	tempDir = System.getProperty(TEMP_DIR_STRING) + File.separator + "grinsfiles";
	dbHost = dbServer.split(":")[0];
	dbPort = dbServer.split(":")[1];

	medialib = MediaLib.getMediaLib(stationConfig, null);

	uploadProvider = (MediaLibUploadProvider)rsApp.getProvider(RSApp.MEDIALIB_UPLOAD_PROVIDER);
	uploadProvider.registerWithProvider(this);
	fileStoreDownloader = new FileStoreDownloader(stationConfig);

	readConfigFile();
	
	if (stationConfig.getOperatingSystem().equals(StationConfiguration.LINUX)){
	    EXEC_MYSQLDUMP = "mysqldump";
	    EXEC_MYSQL = "mysql";
	    EXEC_DIFF = "diff";
	    EXEC_PATCH = "patch";
	} //else if (stationConfig.getOperatingSystem().equals(StationConfiguration.WINDOWS)){
	else {
	    //XXX: change this to appropriate paths in windows.
	    EXEC_MYSQLDUMP = "mysqldump";
	    EXEC_MYSQL = "mysql";
	    EXEC_DIFF = "diff";
	    EXEC_PATCH = "patch";
	}
    }

    public synchronized static BackupController getInstance(RSApp rsApp, StationConfiguration stationConfig, ProgressListener progress){
	if (backupController != null)
	    return backupController;
	else 
	    return (backupController = new BackupController(rsApp, stationConfig, progress));
    }

    public synchronized boolean hasMore(){
	long lastBackup = getLastDate(getNextVolume() - 2);
	RadioProgramMetadata query = RadioProgramMetadata.getDummyObject("");
	RadioProgramMetadata results[] = medialib.get(query,
						      null,
						      new String[] {RadioProgramMetadata.getCreationTimeFieldName()},
						      new String[] {MediaLib.ORDER_ASC},
						      new String[] {RadioProgramMetadata.getCreationTimeFieldName(), Long.toString(lastBackup), Long.toString(Long.MAX_VALUE)},
						      10);

	if (entries.size() == 0) {
	    return results.length > 0;
	}

	int lastIndex = -1;
	String lastID = entries.get(entries.size() - 1).endID;
	for (int i = 0; i < results.length; i++){
	    if (results[i].getItemID().equals(lastID)){
		lastIndex = i;
		break;
	    }
	}

	return lastIndex < results.length - 1;
    }

    public boolean requiresFullBlockSize(){
	//XXX fill in
	return true;
    }

    public synchronized long getLastDate(int volume){
	if (volume < 0){
	    RadioProgramMetadata query = RadioProgramMetadata.getDummyObject("");
	    RadioProgramMetadata results[] = medialib.get(query, 
							  null,
							  new String[] {RadioProgramMetadata.getCreationTimeFieldName()},
							  new String[] {MediaLib.ORDER_ASC},
							  null, 
							  1);
	    if (results != null && results.length > 0)
		return (results[0].getCreationTime() - 1);
	    else
		return 0L;
	} else
	    return (entries.get(volume).endTime);
    }

    String getLastID(int volume){
	if (volume < 0 || volume >= entries.size())
	    return null;
	else
	    return (entries.get(volume).endID);
    }
    
    public synchronized int getNextVolume(){
	return entries.size() + 1;
    }

    public synchronized int getLastBlockSize(){
	if (entries.size() == 0)
	    return -1;
	else 
	    return entries.get(entries.size() - 1).blockSize;
    }

    public synchronized String[] getVolumeInfo(){
	ArrayList<String> info = new ArrayList<String>();
	int i = 1;
	for (BackupEntry entry: entries){
	    info.add("Volume-"+(i++)+" "+entry.blockSize);
	}
	return info.toArray(new String[]{});
    }

    public synchronized Integer[] getMissingVolumes(){
	int i = 1;
	ArrayList<Integer> missing = new ArrayList<Integer>();
	for (int volume: volumes){
	    if (i == volume)
		i++;
	    else
		for (int j = i; j < volume; j++)
		    missing.add(j);
	}
	return missing.toArray(new Integer[]{});
    }

    public synchronized void recreate(final int volume, final String destDirName){
	backup(volume, entries.get(volume).blockSize, destDirName, true);
    }

    public synchronized void backup(final int volume, final int blockSizeMB, final String destDirName){
	backup(volume, blockSizeMB, destDirName, false);
    }

    void backup(int volume, int blockSizeMB, String destDirName, boolean recreate){
	logger.info("Backup: Backing up vol: " + volume + " to " + destDirName);
	long blockSizeBytes = 1024L * 1024L * blockSizeMB;
	long lastBackup = getLastDate(volume - 1);
	String lastID = getLastID(volume - 1);
	long startTime = 0L;
	long endTime = 0L;
	long sumSize = 0L;	
	String subDir, srcDirName;
	int offset = 0;
	ArrayList<RadioProgramMetadata> successPrograms = new ArrayList<RadioProgramMetadata>();
	ArrayList<RadioProgramMetadata> failPrograms = new ArrayList<RadioProgramMetadata>();

	subDir = BACKUP_PREFIX+stationConfig.getStringParam(StationConfiguration.STATION_NAME)+"_"+(volume+1);
	destDirName = destDirName + File.separator + subDir + File.separator;

	//Get radio program metadata
	RadioProgramMetadata query = RadioProgramMetadata.getDummyObject("");
	RadioProgramMetadata results[] = medialib.get(query,
						      null,
						      new String[] {RadioProgramMetadata.getCreationTimeFieldName(),
								    RadioProgramMetadata.getItemIDFieldName()},
						      new String[] {MediaLib.ORDER_ASC, MediaLib.ORDER_ASC},
						      new String[] {RadioProgramMetadata.getCreationTimeFieldName(), 
								    Long.toString(lastBackup), 
								    Long.toString(Long.MAX_VALUE)},
						      -1);

	//Start from the last file we backed up
	if (lastID != null){
	    while (!results[offset++].getItemID().equals(lastID)) {}
	}

	
	//Create the backup directory
	try{
	    File destDir = new File(destDirName);
	    if (destDir.exists()) {
		logger.error("Backup: Unable to back up. Directory " + destDirName + " already exists. Please remove/rename the directory and try again.");
		errorInference.displayServiceError("Unable to back up. Directory " + destDirName + " already exists. Please remove/rename the directory and try again.", new String[]{});

		return;
	    }

	    if (destDir.mkdirs() == false) {
		throw new Exception("Directory creation failed.");
	    }
	    
	    destDir.setReadable(true, false);
	    destDir.setWritable(true, false);

	} catch (Exception e){
	    logger.error("Backup: Encountered Exception:", e);
	    errorInference.displayServiceError("Unable to create backup directory:"+destDirName, new String[]{});
	    return;
	}

	//Fetch the file and put it in required directory
	progress.setMaxProgress(blockSizeMB);

	for (int i = offset; i < results.length; i++){
	    RadioProgramMetadata program = results[i];
	    if (fileStoreDownloader.getFile(program.getItemID(), destDirName)) {
		if (startTime == 0) {
		    startTime = program.getCreationTime();
		}
		
		long size = getSize(destDirName + program.getItemID());
		if (size < 0)
		    continue;

		if (size + sumSize > blockSizeBytes) {
		    (new File(destDirName + program.getItemID())).delete();
		    break;
		}
		
		sumSize += size;
		successPrograms.add(program);
		progress.setValue((int)(sumSize/1024/1024));
	    } else {
		failPrograms.add(program);
	    }
	}

	progress.finishProgress();
	
	//Copy non-content to be backed up.
	/*
	if (volume == 0){
	    String automationFilePath = stationConfig.getStringParam(StationConfiguration.CONFIGPATH);
	    String tmp[] = automationFilePath.split(File.separator.replace("\\", "\\\\"));
	    String automationFileName = tmp[tmp.length - 1];

	    if (copyFile(automationFilePath, destDirName + File.separator+automationFileName) < 0) {
		logger.error("Backup: Unable to backup automation file.");
		errorInference.displayServiceError("Backup: Unable to backup automation file.", new String[]{});
	    }
	    
	    if (copyFile(GRINS_PUB_KEY, destDirName + File.separator + GRINS_PUB_KEY) < 0){
		logger.error("Backup: Unable to backup GRINS public key.");
		errorInference.displayServiceError("Backup: Unable to backup GRINS public key.", new String[]{});
	    }

	    if (copyFile(GRINS_PRI_KEY, destDirName + File.separator + GRINS_PRI_KEY) < 0){
		logger.error("Backup: Unable to backup GRINS private key.");
		errorInference.displayServiceError("Backup: Unable to backup GRINS private key.", new String[]{});
	    }
	}
	*/

	if (successPrograms.size() != 0) {
	    endTime = successPrograms.get(successPrograms.size() - 1).getCreationTime();
	    
	    if (recreate){
		int start = Math.max(0, volume - NUM_DIFFS_PER_VOLUME + 1);
		for (int i = start; i <= volume; i++){
		    String fileName = entries.get(i).dbTime+".sql.diff";
		    copyFile(backupDirName+File.separator+fileName, destDirName+File.separator+fileName);
		}
	    
	    } else {
		long dbTime = dumpDatabase();
		createDBDiff(dbTime);
		copyDiffs(dbTime, destDirName);
		String startID = successPrograms.get(0).getItemID();
		String endID = successPrograms.get(successPrograms.size() - 1).getItemID();
	    
		try{
		    BufferedWriter out = new BufferedWriter(new FileWriter(configFileName, true));
		    out.write(startTime+":"+endTime+":"+dbTime+":"+startID+":"+endID+":"+blockSizeMB+"\n");
		    out.close();
		} catch (Exception e){
		    logger.error("Backup: Unable to update backup information.", e);
		    errorInference.displayServiceError("Unable to update backup information", new String[]{});
		}
	    
		readConfigFile();
	    }
	} else {
	    logger.error("Backup: Could not back up any file.");
	    errorInference.displayServiceError("Unable to fetch programs", new String[]{});
	}
    }

    public synchronized boolean readConfigFile(){
	boolean error = false;
	BufferedReader in = null;
	String line = null;

	try {
	    if (!(new File(configFileName)).exists())
		new FileWriter(configFileName);
	    in = new BufferedReader(new FileReader(configFileName));
	    entries.clear();
	    while ((line = in.readLine()) != null){
		String tokens[] = line.split(":");
		BackupEntry entry = new BackupEntry();
		entry.startTime = Long.parseLong(tokens[0]);
		entry.endTime = Long.parseLong(tokens[1]);
		entry.dbTime = Long.parseLong(tokens[2]);
		entry.startID = tokens[3];
		entry.endID = tokens[4];
		entry.blockSize = Integer.parseInt(tokens[5]);
		entries.add(entry);
	    }
	} catch (Exception e){
	    logger.error("ReadConfigFile: Encountered exception", e);
	    error = true;
	} finally {
	    try{
		in.close();
	    } catch (Exception e){}
	    if (error) {
		errorInference.displayServiceError("Unable to read backup config file.", new String[]{});
		return false;
	    }
	}
	
	return true;
    }

    long getSize(String fileName){
	File file = null;
	try {
	    file = new File(fileName);
	    if (file.exists()){
		return file.length();
	    } else
		return -1L;
	} catch (Exception e){
	    logger.error("GetSize: Exception:", e);
	    errorInference.displayServiceError("Unable to get file size.", new String[]{});
	    return -1L;
	}
    }

    long dumpDatabase(){
	long timestamp = System.currentTimeMillis();
	try {
	    Process process = Runtime.getRuntime().exec(EXEC_MYSQLDUMP + " -h " + dbHost + " -P " + dbPort + " -u "+login+" -p"+password+" "+dbName+" "+" -r "+backupDirName + File.separator + timestamp + ".sql");
	    process.waitFor ();
	    formatDump(timestamp+".sql");
	} catch (Exception e){
	    logger.error("DumpDatabase: Error in dumping database.", e);
	    errorInference.displayServiceError("Unable to dump database for backup.", new String[]{});
	    return -1L;
	}
	
	return timestamp;
    }

    public synchronized boolean reset(){
	try{
	    File backupDir = new File(backupDirName);
	    for (File file : backupDir.listFiles())
		file.delete();
	    new FileWriter(backupDirName + File.separator +"0.sql");
	} catch (Exception e){
	    logger.error("ShowReset: Encountered exception: ", e);
	    errorInference.displayServiceError("Unable to reset backup.", new String[]{});
	    return false;
	}
	return readConfigFile();
    }

    boolean createDBDiff(long curDBTime){
	long prevDBTime;
	if (entries.size() == 0)
	    prevDBTime = 0;
	else
	    prevDBTime = entries.get(entries.size() - 1).dbTime;
	
	String prevFileName = backupDirName + File.separator + prevDBTime + ".sql";
	String curFileName  = backupDirName + File.separator + curDBTime +".sql";
	String diffFileName = backupDirName + File.separator + curDBTime +".sql.diff";

	try{
	    Process p = Runtime.getRuntime().exec(EXEC_DIFF+" "+prevFileName+" "+curFileName);
	    
	    BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    BufferedWriter out = new BufferedWriter(new FileWriter(diffFileName));

	    String line;
	    while ((line = in.readLine()) != null){
		out.write(line+"\n");
	    }
	    out.close();
	    p.waitFor ();

	    (new File(prevFileName)).delete();
	} catch (Exception e){
	    logger.error("CreateDBDiff: Encountered exception.", e);
	    return false;
	}
	return true;
    }

    boolean copyDiffs(long curDBTime, String destDirName){
	ArrayList<Long> dbTimes = new ArrayList<Long>();
	dbTimes.add(curDBTime);
	
	for (int i = 0; i < NUM_DIFFS_PER_VOLUME - 1; i++){
	    int index = entries.size() - 1 - i;
	    if (index < 0)
		break;
	    dbTimes.add(entries.get(index).dbTime);
	}

	boolean error = false;
	for (long dbTime: dbTimes){
	    String fileName = dbTime + ".sql.diff";
	    if (copyFile(backupDirName+File.separator+fileName, destDirName+File.separator+fileName) < 0)
		error = true;
	}
	if (error)
	    errorInference.displayServiceError("Could not copy diffs for backup.", new String[]{});
	return !error;
    }

    boolean formatDump(String fileName){
	String fullFileName = backupDirName+File.separator+fileName;
	try{
	    BufferedReader in = new BufferedReader(new FileReader(fullFileName));
	    BufferedWriter out = new BufferedWriter(new FileWriter(fullFileName+".formatted"));

	    String line;
	    while ((line = in.readLine()) != null){
		if (line.startsWith("INSERT INTO")){
		    String subs[] = line.split("[)],");
		    for (int i = 0; i < subs.length - 1; i++)
			out.write(subs[i]+"),\n");
		    out.write(subs[subs.length - 1]+"\n");
		} else {
		    out.write(line +"\n");
		}
	    }
	    in.close();
	    out.close();

	    FileUtilities.changeFilename(fullFileName+".formatted", fullFileName);

	} catch (Exception e){
	    logger.error("FormatDump: Could not format:", e);
	    errorInference.displayServiceError("Unable to format database dump.", new String[]{});
	    return false;
	}
	return true;
    }

    public boolean importFromDir(File dir){
	String dirPrefix = BACKUP_PREFIX+stationConfig.getStringParam(StationConfiguration.STATION_NAME)+"_";

	if (!dir.exists()) {
	    logger.error("ImportFromDir: Specified directory " + dir + " does not exist.");
	    errorInference.displayServiceError("ImportFromDir: Specified directory " + dir + " does not exist.", new String[]{});
	    return false;
	} else if (!dir.isDirectory()) {
	    logger.error("ImportFromDir: Specified path is not a directory.");
	    errorInference.displayServiceError("ImportFromDir: Specified path " + dir + " is nto a directory.", new String[]{});
	    return false;
	}

	try{
	    for (File importDir: dir.listFiles()){
		if (importDir.isDirectory())
		    if (importDir.getName().startsWith(dirPrefix)){
			File[] files = importDir.listFiles();
			progress.setMaxProgress(files.length);
			int count = 0;
			ArrayList<String> diffs = new ArrayList<String>();
			for (File file: files){
			    String targetDir;
			    if (file.getName().endsWith(".sql.diff")){
				targetDir = backupDirName;
				diffs.add(file.getName());
			    } else if (!localStoreName.equals("")) {
				targetDir = libDirName;
			    } else {
				targetDir = tempDir;
			    }

			    try {
				File targetDirectory = new File(targetDir);
				targetDirectory.mkdir();
			    } catch(Exception e) {
				logger.error("ImportFromDir: Got error while trying to create directory: " + targetDir);
			    }

			    copyFile(file.getCanonicalPath(), targetDir+File.separator+file.getName());
			    progress.setValue(count++);
			}
			storeImportedInfo(importDir.getName().split(dirPrefix)[1], diffs);
			progress.finishProgress();
		    }
	    }
	} catch (Exception e){
	    logger.error("ImportFromDir: Failed to import.", e);
	    errorInference.displayServiceError("Unable to import from directory: "+dir, new String[]{});
	    return false;
	}
	return true;
    }

    boolean storeImportedInfo(String volume, ArrayList<String> diffs){
	try{
	    BufferedWriter out = new BufferedWriter(new FileWriter(importConf, true));
	    StringBuilder str = new StringBuilder();
	    str.append(volume);
	    for (String diff: diffs)
		str.append(":"+diff);
	    out.write(str.toString()+"\n");
	    out.close();
	} catch (Exception e){
	    logger.error("StoreImportedInfo: Encountered exception.", e);
	    return false;
	}
	return true;
    }

    public int canRestore(){
	volumes.clear();
	volumeDiffs.clear();

	ArrayList<String> diffs = new ArrayList<String>();
	try{
	    BufferedReader in = new BufferedReader(new FileReader(importConf));
	    String line = null;
	    while ((line = in.readLine()) != null){
		String toks[] = line.split(":");
		int volume = Integer.parseInt(toks[0]); 
		volumes.add(volume);
		for (int i = 1; i < toks.length; i++)
		    diffs.add(toks[i]);
		volumeDiffs.put(volume, diffs);
	    }
	    in.close();
	} catch (Exception e){
	    logger.error("CanRestore: Encountered error.", e);
	    return -1;
	}
	Collections.sort(volumes);
	int i = 1;
	for (int volume: volumes){
	    if (i == volume){
		i++;
	    } else if (volume - i < NUM_DIFFS_PER_VOLUME){
		i = volume + 1;
	    } else {
		return i - 1;
	    }
	}
	return i - 1;
    }
    
    public boolean restore(){
	int maxVolume = canRestore();
	if (maxVolume < 1)
	    return false;

	String targetDir;
	if (!localStoreName.equals("")) {
	    targetDir = libDirName;
	} else {
	    targetDir = tempDir;
	}

	HashSet<String> diffs = new HashSet<String>();
	for (int volume: volumes){
	    if (volume > maxVolume)
		break;
	    for (String diff: volumeDiffs.get(volume))
		diffs.add(diff);
	}
	ArrayList<String> diffList = new ArrayList<String>();
	for (String diff: diffs)
	    diffList.add(diff);
	Collections.sort(diffList);
	String restoreTimeStamp = diffList.get(diffList.size() - 1).split(".sql.diff")[0];
	String sqlName = backupDirName + File.separator + restoreTimeStamp+".sql";
	String placeHolderName = backupDirName + File.separator +"0.sql";
	try{
	    File file = new File(placeHolderName);
	    if (!file.exists()) {
		file.createNewFile();
	    }

	    for (String diff: diffList){
	    String patchString = EXEC_PATCH + " -o" + sqlName + " " + placeHolderName + " " + backupDirName + File.separator + diff;
		Process p = Runtime.getRuntime().exec(patchString);
		p.waitFor ();
	    }
	    
	    Process p = Runtime.getRuntime().exec(EXEC_MYSQL + " -h " + dbHost + " -P " + dbPort + " -u"+login+" -p"+password+" "+dbName);
	    BufferedReader in = new BufferedReader(new FileReader(sqlName));
	    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));

	    String line;
	    while ((line = in.readLine()) != null)
		out.write(line+"\n");
	    in.close();
	    out.close();
	    p.waitFor ();
	} catch (Exception e){
	    logger.error("Restore: Unable to restore:", e);
	    errorInference.displayServiceError("Unable to restore backup.", new String[]{});
	    return false;
	}
	
	

	//Update the database entries to indicate that none of the stores have the files
	RadioProgramMetadata whereMetadata = RadioProgramMetadata.getDummyObject("");
	RadioProgramMetadata updateMetadata = RadioProgramMetadata.getDummyObject("");
	FileStoreUploader.clearAllFileStoreFields(updateMetadata);
	int updateAttempts = 0;
	while (medialib.update(whereMetadata, updateMetadata, false) < 0) {
	    updateAttempts++;
	    if (updateAttempts > MAX_DB_UPDATE_ATTEMPTS) {
		logger.error("Restore: Unable to clear file store fields before starting update.");
		return false;
	    }
	    
	    try{
		Thread.sleep(INTER_UPDATE_ATTEMPT_SLEEP_TIME);
	    } catch(Exception e) {
		logger.warn("Restore: Sleep interrupted.");
	    }
	}

	HashSet<String> filesOnDisk = getMediaFilesOnDisk();

	RadioProgramMetadata query = RadioProgramMetadata.getDummyObject("");
	RadioProgramMetadata[] results = medialib.get(query);
	HashSet<String> filesOnDB = new HashSet<String>();
	for (RadioProgramMetadata program: results)
	    filesOnDB.add(program.getItemID());

	ArrayList<String> remFromDisk = new ArrayList<String>();
	for (String programID: filesOnDisk)
	    if (!filesOnDB.contains(programID))
		remFromDisk.add(programID);

	ArrayList<String> remFromDB = new ArrayList<String>();
	for (String programID: filesOnDB)
	    if (!filesOnDisk.contains(programID))
		remFromDB.add(programID);

	removeFromDisk(remFromDisk);
	removeFromDB(remFromDB, restoreTimeStamp);

	for (String fileToRemove : remFromDisk) {
	    filesOnDisk.remove(fileToRemove);
	}

	totalFilesToUpload = filesOnDisk.size();

	progress.setMaxProgress(totalFilesToUpload);
       
	//Actually upload the files
	uploadFailFiles.clear();
	uploadSuccessFiles.clear();
	for (String fileToUpload : filesOnDisk) 
	    uploadProvider.uploadFile(targetDir + File.separator + fileToUpload, getName(), fileToUpload, false, false);
	
	return true;
    }

    void resetState() {
	if (localStoreName.equals("")) { 
	    //i.e. there is no local file store and we have imported files to a temp directory
	    if (uploadFailFiles.size() > 0) {
		ArrayList<String> remFromDisk = new ArrayList<String>();
		for (String programID: uploadSuccessFiles)
		    remFromDisk.add(programID);
		
		removeFromDisk(remFromDisk);
	    } else {
		deleteDirectory(new File(tempDir));
	    }
	}
		

	EncodedFiles query = EncodedFiles.getDummyObject();
	medialib.remove(query);
	RefreshIndexMessage refreshMessage = new RefreshIndexMessage(RSController.UI_SERVICE, RSController.INDEX_SERVICE);
	refreshMessage.setPersistent(true);
	uiService.sendMessage(refreshMessage);
    }

    boolean deleteDirectory(File path) {
	if (path.exists()) {
	    for (File file : path.listFiles()) {
		if (file.isDirectory()) {
		    deleteDirectory(file);
		}
		else {
		    file.delete();
		}
	    }
	}
	return( path.delete() );
    }

    HashSet<String> getMediaFilesOnDisk(){
	String targetDir;
	if (!localStoreName.equals("")) {
	    targetDir = libDirName;
	} else {
	    targetDir = tempDir;
	}

	HashSet<String> mediaFiles = new HashSet<String>();
	try{
	    File libDir = new File(targetDir);
	    for (File file: libDir.listFiles()){
		if (!file.isDirectory() && FileUtilities.isSupported(file.getName())){
		    mediaFiles.add(file.getName());
		}
	    }
	} catch (Exception e){
	    logger.error("GetMediaFilesOnDisk: ", e);
	}
	return mediaFiles;
    }

    void removeFromDisk(ArrayList<String> programIDs) {
	String targetDir;
	if (!localStoreName.equals("")) {
	    targetDir = libDirName;
	} else {
	    targetDir = tempDir;
	}

	for (String programID: programIDs){
	    logger.info("RemoveFromDisk: Removing: "+programID);
	    try{
		(new File(targetDir+File.separator+programID)).delete();
	    } catch (Exception e){
		logger.error("RemoveFromDisk: Encountered Exception:", e);
	    }
	}
    }

    void removeFromDB(ArrayList<String> programIDs, String timeStamp){
	if (programIDs.size() == 0)
	    return;
	String logFileName = libDirName + File.separator + "deleteLog" + timeStamp + ".txt";
	JOptionPane.showMessageDialog(null, "Some audio files are missing. Information about them will be removed from GRINS. Details about these files can be found in file "+logFileName, "Missing Files", JOptionPane.WARNING_MESSAGE);

	try{
	    BufferedWriter out = new BufferedWriter(new FileWriter(logFileName));
	    int i = 1;
	    for (String programID: programIDs){
		RadioProgramMetadata query = new RadioProgramMetadata(programID);
		RadioProgramMetadata program = medialib.getSingle(query);
		program.populate__creators(medialib);
		program.populate__tags(medialib);
		program.populate__categories(medialib);
		out.write("file: "+(i++)+"\n");
		out.write(radioProgramToDetailedString(program));
		
		PlayoutHistory playout = PlayoutHistory.getDummyObject(programID);
		if (medialib.remove(playout, false) < 0) {
		    logger.error("RemoveFromDB: Could not remove playout history for: " + programID);
		}
		
		Creator creator = Creator.getDummyObject(programID);
		if (medialib.remove(creator, false) < 0) {
		    logger.error("RemoveFromDB: Could not remove creators for: " + programID);
		}
		
		ItemCategory category = ItemCategory.getDummyObject(programID);
		if (medialib.remove(category, false) < 0) {
		    logger.error("RemoveFromDB: Could not remove item categories for: " + programID);
		}
		
		Tags tag = Tags.getDummyObject(programID);
		if (medialib.remove(tag, false) < 0) {
		    logger.error("RemoveFromDB: Could not remove tags for: " + programID);
		}
		
		if (medialib.remove(program, false) < 0){
		    logger.error("RemoveFromDB: Could not remove program for: " + programID); 
		}
		
	    }
	    out.close();
	} catch (Exception e){
	    logger.error("RemoveFromDB: Encountered exception:",e);
	}
    }

    String radioProgramToDetailedString(RadioProgramMetadata program){
	StringBuilder str = new StringBuilder();
	str.append("Filename:\t"+program.getItemID()+"\n");
	str.append("Title:\t"+program.getTitle()+"\n");
	str.append("CreationTime:\t"+program.getCreationTime()+"("+(new Date(program.getCreationTime()))+")\n");
	str.append("Length:\t"+StringUtilities.durationStringFromMillis(program.getLength())+"\n");
	str.append("Language:\t"+program.getLanguage()+"\n");
	str.append("Description:\t"+program.getDescription()+"\n");
	str.append("Type:\t"+program.getType()+"\n");
	str.append("Tags:\t");
	Tags tags[];
	if ((tags = program.getTags()) != null)
	    for (Tags t: tags)
		str.append(t.getTagsCSV()+",");
	str.append("\n");
	str.append("Creators:\t");
	Creator creators[];
	if ((creators = program.getCreators()) != null)
	    for (Creator creator: creators){
		str.append(entityToString(creator.getEntity())+":"+creator.getAffiliation()+",");
	    }
	str.append("\n");
	str.append("Categories:\t");
	ItemCategory categories[];
	if ((categories = program.getCategories()) != null)
	    for (ItemCategory category: categories)
		str.append(category.getCategory().getNodeName()+",");
	str.append("\n");
	return str.toString();
    }

    String entityToString(Entity entity){
	StringBuilder str = new StringBuilder();
	str.append(entity.getName()+":"+entity.getUrl()+":"+entity.getPhone()+":"
		   +entity.getEmail()+":"+entity.getLocation()+":"+entity.getEntityType());
	return str.toString();
    }

    public void cleanup() {
	uploadProvider.cancelUpload();
	uploadProvider.unregisterWithProvider(getName());
    }

    //Call backs from upload provider
    public String getName() {
	return BACKUP_CONTROLLER;
    }

    public void activateMediaLibUpload(boolean activate, String[] error) {
	//XXX: enable/disable UI
    }

    public void uploadError(String fileName, String error, int numRemaining) {
	//Do nothing. We track the failed file on provider give up.
    }

    public synchronized void uploadGiveup(String fileName) {
	File file = new File(fileName);
	uploadFailFiles.add(file.getName());

	int completed = uploadSuccessFiles.size() + uploadFailFiles.size();
	progress.setValue(completed);

	if (completed >= totalFilesToUpload) {
	    resetState();
	    progress.finishProgress();
	}
    }

    public synchronized void uploadSuccess(String fileName, String libFileName, int numRemaining, boolean isDuplicate) {
	File file = new File(fileName);
	uploadSuccessFiles.add(file.getName());

	int completed = uploadSuccessFiles.size() + uploadFailFiles.size();
	progress.setValue(completed);

	if (completed >= totalFilesToUpload) {
	    resetState();
	    progress.finishProgress();
	}
    }

    public synchronized void updateProgress(String fileName, long uploadedLength, long totalLength) {
	//XXX: May be use this info to show which file is being restored right now.
    }
    //call backs from upload provider end here
    
    class BackupEntry {
	public long startTime, endTime, dbTime;
	public String startID, endID;
	public int blockSize;
    }

}