package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.utilities.IconUtilities;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SearchTextField extends JTextField {
    ImageIcon clearIcon = null;
    int iconX, iconY, iconSize;
    
    SearchTextField(){
	addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    Dimension size = getSize();
		    Rectangle rect = new Rectangle(size.width - size.height, 0, size.height, size.height);
		    if (rect.contains(e.getPoint()))
			setText("");
		}
		
	    });
    }

    public void paintComponent(Graphics g){
	Graphics2D g2 = (Graphics2D) g;
	int y = 0; //getY();
	int w = getWidth();
	int h = getHeight();
	int x = 0; //getX() - w/2;

	super.paintComponent(g);
	if (clearIcon == null)
	    loadIcon(x, y, w, h);
	g2.drawImage(clearIcon.getImage(), iconX, iconY, this);

    }
    
    void loadIcon(int x, int y, int w, int h){
	iconSize = h - 2;
	clearIcon = IconUtilities.getIcon(StationConfiguration.CLEAR_BAR_RTL_ICON, iconSize, iconSize);
	iconX = x + w - iconSize;
	iconY = y;
    }
    
}