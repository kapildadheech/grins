package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import org.gramvaani.radio.medialib.*;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;

import java.util.*;

public class RSMultiList extends JPanel implements ActionListener {
    public static String PREV = "PREV";
    public static String NEXT = "NEXT";

    JList list;
    JScrollPane scrollPane;
    MultiListModel listModel;
    Dimension scrollPaneDim, bottomPanelDim, buttonDim;

    ArrayList<Object> listObjects;
    int currentScreen;
    int numItemsPerScreen = 15;

    int numSearchResults;
    int maxScreens;

    RSButton prevButton, nextButton;
    RSLabel statsLabel, totalLabel;
    MultiListMoreItems moreItemsCallback = null;
    String moreItemsID = null;
    LogUtilities logger;

    protected StationConfiguration stationConfig;

    protected int saveIndex;

    public RSMultiList(JList list, JScrollPane scrollPane, MultiListModel listModel, 
		       Dimension scrollPaneDim, Dimension bottomPanelDim, Dimension buttonDim, StationConfiguration stationConfig) {
	this.list = list;
	this.scrollPane = scrollPane;
	this.listModel = listModel;

	this.scrollPaneDim = scrollPaneDim;
	this.bottomPanelDim = bottomPanelDim;
	this.buttonDim = buttonDim;

	this.listObjects = new ArrayList<Object>();
	this.currentScreen = 0;

	this.stationConfig = stationConfig;
	this.saveIndex = -1;
	
	logger = new LogUtilities("RSMultiList");
	init();
    }

    public synchronized void setNumSearchResults(int numSearchResults) {
	this.numSearchResults = numSearchResults;
	maxScreens = (numSearchResults / numItemsPerScreen) + (numSearchResults % numItemsPerScreen == 0 ? 0 : 1);
	if (maxScreens > 0 && list.getModel().getSize() == 0 && currentScreen >= maxScreens){
	    prevPage();
	}
	if (maxScreens > 0){
	    statsLabel.setText("Page " + (currentScreen + 1) + " of " + maxScreens);
	    totalLabel.setText("Results: "+numSearchResults);
	} else {
	    statsLabel.setText("");
	    totalLabel.setText("");
	}
    }

    public synchronized void setNumItemsPerScreen(int val) {
	this.numItemsPerScreen = val;
	maxScreens = (numSearchResults / numItemsPerScreen) + (numSearchResults % numItemsPerScreen == 0 ? 0 : 1);
	if (maxScreens > 0)
	    statsLabel.setText("Pg 1 of " + maxScreens);
	//System.out.println("** rsmultilist: " + numSearchResults + ":" + numItemsPerScreen + ":" + maxScreens);
    }

    public synchronized void addMoreItemsCallback(MultiListMoreItems moreItemsCallback, String moreItemsID) {
	this.moreItemsCallback = moreItemsCallback;
	this.moreItemsID = moreItemsID;
    }

    public synchronized int getNumSearchResults() {
	return numSearchResults;
    }

    public synchronized void setSaveIndex(int index) {
	this.saveIndex = currentScreen * numItemsPerScreen + index;
    }

    public synchronized boolean isSaveIndex(int index) {
	return (saveIndex == currentScreen * numItemsPerScreen + index);
    }

    protected void init() {
	setLayout(new GridBagLayout());
	
	GridBagConstraints c = new GridBagConstraints();
	c.gridx = 0; c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	add(scrollPane, c);

	JPanel bottomPane = new JPanel();
	bottomPane.setLayout(new GridBagLayout());
	bottomPane.setPreferredSize(bottomPanelDim);
	bottomPane.setMinimumSize(bottomPanelDim);

	prevButton = new RSButton("");
	prevButton.setPreferredSize(buttonDim);
	prevButton.setMinimumSize(buttonDim);
	prevButton.addActionListener(this);
	prevButton.setActionCommand(PREV);
	c = new GridBagConstraints();
	c.gridx = 1; c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	prevButton.setEnabled(false);
	prevButton.setToolTipText("Previous page");
	bottomPane.add(prevButton, c);

	totalLabel = new RSLabel("");
	totalLabel.setPreferredSize(new Dimension((int)(bottomPanelDim.getWidth() - buttonDim.getWidth()) / 2, (int)(buttonDim.getHeight())));
	totalLabel.setMinimumSize(new Dimension((int)(bottomPanelDim.getWidth() - buttonDim.getWidth()) / 2, (int)(buttonDim.getHeight())));
	totalLabel.setHorizontalAlignment(SwingConstants.CENTER);
	totalLabel.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_DETAILS_COLOR));
	c = new GridBagConstraints();
	c.gridx = 0; c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	c.weightx = 100;
	bottomPane.add(totalLabel, c);

	nextButton = new RSButton("");
	nextButton.setPreferredSize(buttonDim);
	nextButton.setMinimumSize(buttonDim);
	nextButton.addActionListener(this);
	nextButton.setActionCommand(NEXT);
	c = new GridBagConstraints();
	c.gridx = 2; c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	nextButton.setEnabled(false);
	nextButton.setToolTipText("Next page");
	bottomPane.add(nextButton, c);

	statsLabel = new RSLabel("");
	statsLabel.setPreferredSize(new Dimension((int)(bottomPanelDim.getWidth() - buttonDim.getWidth()) / 2, (int)(buttonDim.getHeight())));
	statsLabel.setMinimumSize(new Dimension((int)(bottomPanelDim.getWidth() - buttonDim.getWidth()) / 2, (int)(buttonDim.getHeight())));
	statsLabel.setHorizontalAlignment(SwingConstants.CENTER);
	statsLabel.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_DETAILS_COLOR));
	c = new GridBagConstraints();
	c.gridx = 3; c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	c.weightx = 100;
	bottomPane.add(statsLabel, c);

	c = new GridBagConstraints();
	c.gridx = 0; c.gridy = 1; c.gridwidth = 1; c.gridheight = 1;
	add(bottomPane, c);

	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	JComponent components[] = new JComponent[]{scrollPane, bottomPane, totalLabel, 
						   statsLabel, nextButton, prevButton};
	for (JComponent comp: components){
	    comp.setBackground(bgColor);
	    comp.setOpaque(true);
	}
	
	nextButton.setIcon(IconUtilities.getIcon(StationConfiguration.NEXT_ICON, (int)(buttonDim.getWidth()), (int)(buttonDim.getHeight())));
	prevButton.setIcon(IconUtilities.getIcon(StationConfiguration.PREV_ICON, (int)(buttonDim.getWidth()), (int)(buttonDim.getHeight())));

    }


    public synchronized Object[] getSelectedValues() {
	return list.getSelectedValues();
    }

    // used to add at beginning ONLY. Any other additions are only added at the end
    public synchronized void add(final int index, final Object obj) {
	Runnable adder = new Runnable (){
		public void run(){

		    if (index > 0) {
			add(obj);
			return;
		    }
		    
		    saveIndex++;
		    
		    if (currentScreen > 0) {
			listObjects.add(0, obj);
			Object scrollObj = listObjects.get(currentScreen * numItemsPerScreen);
			LogUtilities.getDefaultLogger().debug("RSMultilist: add: " + index + ":" + ((PlayoutHistory)obj).getItemID());
			listModel.add(0, scrollObj);
			if (listObjects.size() > (currentScreen + 1) * numItemsPerScreen) {
			    nextButton.setEnabled(true);
			    listModel.removeFromMultiList(numItemsPerScreen);
			}
		    } else {
			listObjects.add(0, obj);
			listModel.add(0, obj);
			if (listObjects.size() > numItemsPerScreen) {
			    nextButton.setEnabled(true);
			    listModel.removeFromMultiList(numItemsPerScreen);
			}
		    }
		}
	    };

	try{
	    if (SwingUtilities.isEventDispatchThread())
		adder.run();
	    else
		SwingUtilities.invokeLater(adder);
	} catch (Exception e){
	    logger.error("Add: Could not add object: "+obj, e);
	}
    }

    // used to add at end
    public synchronized void add(final Object obj) {
	Runnable adder = new Runnable() {
		public void run(){
		    boolean lastPage = !hasNextPages();
		    listObjects.add(obj);
		    if (lastPage && !hasNextPages()) {
			listModel.add(obj);
		    } else if (lastPage && hasNextPages()) {
			nextButton.setEnabled(true);
		    }
		}
	    };
	try{
	    if (SwingUtilities.isEventDispatchThread())
		adder.run();
	    else
		SwingUtilities.invokeLater(adder);
	} catch (Exception e){
	    logger.error("Add: Could not add object: "+obj, e);
	}
    }

    public synchronized void replace(Object oldObj, Object newObj) {
	int i = 0;
	for (Object obj: listObjects) {
	    if (obj == oldObj) {
		listObjects.set(i, newObj);
		break;
	    }
	    i++;
	}
    }

    public synchronized void clear() {
	Runnable r = new Runnable() {
		public void run() {
		    listModel.clear();
		    currentScreen = 0;
		    listObjects.clear();
		    setNumSearchResults(0);
		    prevButton.setEnabled(false);
		    nextButton.setEnabled(false);
		}
	    };

	try{
	    if (SwingUtilities.isEventDispatchThread())
		r.run();
	    else
		SwingUtilities.invokeLater(r);
	} catch (Exception e){
	    logger.error("Clear: Could not clear lists", e);
	}
    }

    public synchronized void remove(final int index, final boolean isAbsoluteIndex){
	Runnable remover = new Runnable (){
		public void run(){
		    if (isAbsoluteIndex){
			listObjects.remove(index);
			if (isIndexInCurrentScreen(index)){
			    listModel.removeFromMultiList(index % numItemsPerScreen);
			}
		    } else {
			listObjects.remove(index + (currentScreen * numItemsPerScreen));
			listModel.removeFromMultiList(index);
		    }

		    if ((currentScreen + 1) * numItemsPerScreen - 1 < listObjects.size()) {
			Object scrollObj = listObjects.get((currentScreen + 1) * numItemsPerScreen - 1);
			if (scrollObj != null) {
			    listModel.add(scrollObj);
			    if (hasNextPages())
				nextButton.setEnabled(true);
			    else {
				if (moreItemsCallback != null && moreItemsCallback.hasMoreItems(moreItemsID)) {
				    moreItemsCallback.populateMoreItems(moreItemsID);
				    nextButton.setEnabled(true);
				}
				else
				    nextButton.setEnabled(false);
			    }
			}
		    }
		    setNumSearchResults(getNumSearchResults() - 1);
		}
	    };

	try{
	    if (SwingUtilities.isEventDispatchThread())
		remover.run();
	    else
		SwingUtilities.invokeLater(remover);
	} catch (Exception e){
	    logger.error("Remove: Could not remove object at index: "+index, e);
	}

    }

    boolean isIndexInCurrentScreen(int index){
	if (index < currentScreen * numItemsPerScreen)
	    return false;
	if (index >= (currentScreen + 1) * numItemsPerScreen)
	    return false;
	return true;
    }

    public synchronized void actionPerformed(ActionEvent e) {
	if (e.getActionCommand().equals(NEXT)) {
	    currentScreen ++;
	} else {
	    currentScreen --;
	}
	
	listModel.clear();
	for (int i = currentScreen * numItemsPerScreen; i < Math.min((currentScreen + 1) * numItemsPerScreen, listObjects.size()); i++){
	    listModel.add(listObjects.get(i));
	}
	
	if (hasPrevPages())
	    prevButton.setEnabled(true);
	else
	    prevButton.setEnabled(false);
	
	if (hasNextPages())
	    nextButton.setEnabled(true);
	else {
	    if (moreItemsCallback != null && moreItemsCallback.hasMoreItems(moreItemsID)) {
		//System.out.println("** rsmultilist: " + moreItemsCallback.hasMoreItems(moreItemsID));
		moreItemsCallback.populateMoreItems(moreItemsID);
		nextButton.setEnabled(true);
		//System.out.println("new items populated");
	    }
	    else
		nextButton.setEnabled(false);
	}

	if (maxScreens > 0)
	    statsLabel.setText("Pg " + (currentScreen + 1) + " of " + maxScreens);

	// XXX dirty fix. there is actually a bug in calculating the total number of results in indexlib
	if (currentScreen + 1 >= maxScreens)
	    nextButton.setEnabled(false);
    }
   
    public void nextPage(){
	nextButton.doClick();
    }

    public void prevPage(){
	prevButton.doClick();
    }

    protected boolean hasPrevPages() {
	return (currentScreen > 0);
    }

    protected boolean hasNextPages() {
	return (listObjects.size() > (currentScreen + 1) * numItemsPerScreen);
    }

    public ArrayList<Object> getObjects(){
	return listObjects;
    }

    public int listSize(){
	return listObjects.size();
    }

    public Object getElementAt(int index){
	return listObjects.get(index);
    }

}