package org.gramvaani.radio.app.gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ListCellRenderer;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JLabel;

public class FilterCellRenderer implements ListCellRenderer {
    Color dummyColor;

    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

    public FilterCellRenderer (Color dummyColor){
	this.dummyColor = dummyColor;
    }

    public Component getListCellRendererComponent (JList list, Object value, int index, 
						   boolean isSelected, boolean cellHasFocus){
	JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	String text = (String) value;
	if (text.startsWith("Select"))
	    renderer.setForeground(dummyColor);
	else
	    renderer.setForeground(Color.black);
	return renderer;
    }

}