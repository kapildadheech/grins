package org.gramvaani.radio.app.gui;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

public class RSScrollList extends JPanel implements ListDataListener {
    public final static int HORIZONTAL_ORIENTATION = 0;
    public final static int VERTICAL_ORIENTATION = 1;
    
    ListModel listModel;
    RSLabel dummyLabel = new RSLabel("");
    int orientation;

    public RSScrollList(ListModel listModel, int orientation) {
	this.listModel = listModel;
	this.orientation = orientation;
	listModel.addListDataListener(this);
	setLayout(new GridBagLayout());
    }

    public void contentsChanged(ListDataEvent e) {

    }

    // always add at the end
    public void intervalAdded(ListDataEvent e) {
	if(dummyLabel != null)
	    remove(dummyLabel);
	else
	    dummyLabel = new RSLabel("");
	for(int i = e.getIndex0(); i <= e.getIndex1(); i++) {
	    GridBagConstraints c = new GridBagConstraints();
	    if(orientation == VERTICAL_ORIENTATION) {
		c.gridx = 0; c.gridy = i; c.gridwidth = 1; c.gridheight = 1;
	    } else {
		c.gridx = i; c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	    }
	    add((JComponent)(listModel.getElementAt(i)), c);
	}
	GridBagConstraints c = new GridBagConstraints();
	if(orientation == VERTICAL_ORIENTATION) {
	    c.gridx = 0; c.gridy = e.getIndex1() + 1; c.gridwidth = 1; c.gridheight = 1;
	    c.weighty = 100;
	} else {
	    c.gridx = e.getIndex1() + 1; c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	    c.weightx = 100;
	}
	add(dummyLabel, c);
	validate();
	repaint();
    }

    public void intervalRemoved(ListDataEvent e) {
	for(int i = e.getIndex0(); i <= e.getIndex1(); i++)
	    remove((JComponent)(listModel.getElementAt(i)));

	ArrayList<JComponent> tempComponents = new ArrayList<JComponent>();
	for(int i = e.getIndex1() + 1; i < listModel.getSize(); i++) {
	    tempComponents.add((JComponent)(listModel.getElementAt(i)));
	    remove((JComponent)(listModel.getElementAt(i)));
	}
	remove(dummyLabel);
	for(int i = 0; i < tempComponents.size(); i++) {
	    GridBagConstraints c = new GridBagConstraints();
	    if(orientation == VERTICAL_ORIENTATION) {
		c.gridx = 0; c.gridy = i + e.getIndex1(); c.gridwidth = 1; c.gridheight = 1;
	    } else {
		c.gridx = i + e.getIndex1(); c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	    }
	    add(tempComponents.get(i), c);
	}

	GridBagConstraints c = new GridBagConstraints();
	if(orientation == VERTICAL_ORIENTATION) {
	    c.gridx = 0; c.gridy = e.getIndex1() + tempComponents.size(); c.gridwidth = 1; c.gridheight = 1;
	    c.weighty = 100;
	} else {
	    c.gridx = e.getIndex1() + tempComponents.size(); c.gridy = 0; c.gridwidth = 1; c.gridheight = 1;
	    c.weightx = 100;
	}
	add(dummyLabel, c);

	validate();
	repaint();
    }

    
}