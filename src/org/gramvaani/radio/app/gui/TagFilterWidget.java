package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

import java.util.*;

public class TagFilterWidget extends ActiveFilterWidget implements TagTreeListener {
    MetadataProvider metadataProvider;
    StationConfiguration stationConfig;

    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;

    TagsFilter tagsFilter;
    Hashtable<Integer, Category> selectedTags = new Hashtable<Integer, Category>();
    JTextField tagsField;
    TagTree tagTree;

    public TagFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
			      LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	metadataProvider = libraryController.getMetadataProvider();
	this.stationConfig = libraryController.getStationConfiguration();

	tagsFilter = (TagsFilter) filter;
	init();
    }

    Color bgColor;
    protected void init(){
	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();

	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, 2 * filterVertSize + borderHeight));
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());
	titleLabel.setPreferredSize(new Dimension((int)(filterHorizSize * tlHorizFrac / 100.0), filterVertSize));
	add(titleLabel, getGbc(0, 0, weightx(100.0), gfillboth()));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));
	add(closeButton, getGbc(1, 0, gfillboth()));

	tagsFilter.activateAnchor(TagsFilter.TAGS_ANCHOR, "");
	SearchResult result = libraryController.activateFilter(sessionID, filter);
	libraryController.updateResults(result, sessionID);

	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	setBackground(bgColor);
	titleLabel.setBackground(bgColor);
	closeButton.setBackground(bgColor);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);

	closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON,
						  filterVertSize, filterVertSize));
	
	closeButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    TagTree.setTreeKeepAlive(tagTree, false);
		    tagsFilter.setCategories(new Integer[0][0]);
		}
	    });
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	RSPanel tagsPanel = new RSPanel();
	tagsPanel.setBackground(bgColor);
	add(tagsPanel, getGbc(0, 1, gfillboth(), gridwidth(2), weightx(1.0), weighty(1.0)));
	
	tagsPanel.setLayout(new BorderLayout());

	RSLabel tagsLabel = new RSLabel("Tags: ");
	tagsLabel.setBackground(bgColor);
	tagsPanel.add(tagsLabel, BorderLayout.WEST);

	tagsField = new JTextField();
	tagsPanel.add(tagsField, BorderLayout.CENTER);
	tagsField.setEditable(false);
	tagsField.setOpaque(true);
	tagsField.setBackground(Color.white);
	tagsField.addMouseListener(new MouseAdapter(){
		JFrame tagsPopup;

		public synchronized void mouseClicked(MouseEvent e){
		    if (tagsPopup == null){
			tagTree = new TagTree(TagFilterWidget.this, metadataProvider, new HashSet<Integer>(), stationConfig);
			tagsPopup = GUIUtilities.getTagsPopup(tagTree, tagsField, tagsField.getSize().height, bgColor);
			TagTree.setTreeKeepAlive(tagTree, true);
		    }
		    //tagsPopup.show(tagsField, 0, tagsField.getSize().height);
		    tagsPopup.setVisible(true);
		}

	    });
    }

    public void tagSelectionChanged(Category tag, boolean selected){
	if (selected){
	    selectedTags.put(tag.getNodeID(), tag);
	} else {
	    selectedTags.remove(tag.getNodeID());
	}

	updateTagField();

	Category[][] selectedCategories = tagTree.getSelectionClosure();
	
	ArrayList<Integer[]> selection = new ArrayList<Integer[]>();

	for (Category[] categorySet: selectedCategories){
	    ArrayList<Integer> list = new ArrayList<Integer>();
	    for (Category category: categorySet){
		list.add(category.getNodeID());
	    }
	    selection.add(list.toArray(new Integer[0]));
	}

	tagsFilter.setCategories(selection.toArray(new Integer[0][0]));
	SearchResult result = libraryController.activateFilter(sessionID, filter);
	libraryController.updateResults(result, sessionID);
    }

    public void tagEdited(Category tag){
	updateTagField();
    }

    void updateTagField(){
	ArrayList<Category> list = new ArrayList<Category>(selectedTags.values());
	Collections.sort(list);
	tagsField.setText(StringUtilities.joinFromIterable(list, ", "));
    }

    public void update(){

    }
}