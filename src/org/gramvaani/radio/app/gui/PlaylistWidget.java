package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.services.ArchiverService;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.metal.MetalSliderUI;
import javax.swing.border.MatteBorder;

import java.util.*;
import java.text.*;

import static org.gramvaani.utilities.IconUtilities.getIcon;
import static org.gramvaani.radio.app.gui.GBCUtilities.*;
import static org.gramvaani.utilities.StringUtilities.durationStringFromMillis;

public class PlaylistWidget implements ListDataListener {
    public final static String GOLIVE_ITEMID = "GOLIVEITEMID";

    public final static String GOLIVE 	= "GOLIVE";
    public final static String PLAY 	= "PLAY";
    public final static String PAUSE 	= "PAUSE";
    public final static String STOP 	= "STOP";
    public final static String VOLUMEUP = "VOLUMEUP";
    public final static String VOLUMEDN = "VOLUMEDN";

    public final static String CURRINFO 	= "CURRINFO";
    public final static String CURRPREVIEW 	= "CURRPREVIEW";
    public final static String CURRPREVIEWSTOP 	= "CURRPREVIEWSTOP";
    public final static String NEXTPROG 	= "NEXTPROG";

    public final static String LISTPLAY = "LISTPLAY";
    public final static String LISTPREVIEW = "LISTPREVIEW";
    public final static String LISTSTOP = "LISTSTOP";
    public final static String LISTINFO = "LISTINFO";
    public final static String LISTLIVE = "LISTLIVE";
    public final static String LISTFADE = "LISTFADE";

    public final static String CLEAR_LIST= "CLEAR_LIST";
    public final static String LOAD_LIST = "LOAD_LIST";
    public final static String SAVE_LIST = "SAVE_LIST";
    public final static String SETSTART_LIST = "SETSTART_LIST";

    public static long FADEOUT_DURATION = 500;         // 3 seconds
    final static int MIN_GAIN = -30;
    final static int MAX_GAIN = 30;

    protected final static int insetWidth = 5;   // pixels

    protected JPanel panel = null;

    protected JPanel topPane;
    protected JPanel volPane;
    protected VolumeDisplay volumeDisplay;
    protected JPanel livePane;
    protected JList listPane;
    protected ListModel listModel;
    protected JPanel listControlPane;
    protected JScrollPane scrollPane;
    protected RSToggleButton goLive;
    protected RSToggleButton play, pause;
    protected RSButton stop;
    protected JSlider seekSlider;
    protected RSLabel currentElementTimeLeftLabel, currentElementTitleLabel1, currentElementDurationLabel;
    protected long currentElementDuration;
    protected RSButton currentElementInfoButton, nextButton;
    protected RSButton currentElementPreviewStopButton;
    protected RSToggleButton currentElementPreviewButton;
    protected RSButton loadPlaylist, savePlaylist, setStartTimePlaylist;
    protected RSButton clearPlaylist;
    protected ArrayList<PlaylistWidget.PlayItemWidget> listElements;
    
    protected boolean currentElementPreviewPressed = false;

    protected ControlPanel controlPanel;
    protected PlaylistController playlistController;
    protected RSApp rsApp;
    protected LogUtilities logger;
    protected GUIPlaylist guiPlaylist;
    protected StationConfiguration stationConfig;

    protected int selectedIndex = -1;
    protected RSLabel fillLabel;
    protected boolean previewUIEnabled = false;
    protected boolean playoutUIEnabled = false;
    protected boolean livePreviewUIEnabled = false;

    Dimension liveDimension;
    Dimension playItemSize;
    Dimension goLiveDim;
    Dimension volDimension;
    String goLiveIconID;
    String goLiveActiveIconID;

    final int horizontalThumbSize = 20;
    final ImageIcon sliderIcon = getIcon(StationConfiguration.SLIDER_ICON, horizontalThumbSize, horizontalThumbSize);

    public PlaylistWidget(ControlPanel controlPanel, RSApp rsApp) {
	listElements = new ArrayList<PlaylistWidget.PlayItemWidget>();
	this.controlPanel = controlPanel;
	this.rsApp = rsApp;
	this.logger = controlPanel.getLogger();
	this.stationConfig = rsApp.getStationConfiguration();
	this.playlistController = new PlaylistController(this, controlPanel, rsApp);
	IconUtilities.setStationConfig(stationConfig);
    }

    public PlaylistController getController() {
	return playlistController;
    }

    public void setPanel(JPanel basePanel, Dimension dim) {
	basePanel.setLayout(new GridBagLayout());
	fixSize(basePanel, dim);
	if (panel == null) {
	    panel = new JPanel();
	    panel.setMaximumSize(dim);
	    panel.setPreferredSize(dim);
	    panel.setSize(dim);
	    init();
	    playlistController.init();
	    //fixSize(livePane, liveDimension);
	    basePanel.add(panel);
	    basePanel.validate();
	} else {
	    //fixSize(livePane, liveDimension);
	    basePanel.add(panel);
	    basePanel.validate();
	}
    }

    public void setGUIPlaylist(GUIPlaylist guiPlaylist) {
	this.guiPlaylist = guiPlaylist;
	guiPlaylist.addListDataListener(this);
    }


    public void activatePlayout(boolean activate) {
	enablePlayoutUI(activate);
	
	if (!activate){
	    play.setPressed(false);
	    pause.setPressed(false);
	}
	Iterable<PlaylistWidget.PlayItemWidget> listElements = guiPlaylist.getListElements();
	for (PlaylistWidget.PlayItemWidget itemWidget: listElements) {
	    if (!activate)
		itemWidget.getPlayButton().setPressed(false);
	}
	listPane.repaint();
    }

    public void changeGain (int gain){
	volumeDisplay.changeGain (gain);
    }

    //ListDataListener functions
    public void contentsChanged(ListDataEvent e) {
	//inefficient but cleaner code. a faster way would be to get
	//index of item with contents changed and do update for that entry only.
	enablePlayoutUI(playoutUIEnabled);
	enablePreviewUI(previewUIEnabled);
	enableLivePreviewUI(livePreviewUIEnabled);
    }

    public void intervalAdded(ListDataEvent e) { }

    public void intervalRemoved(ListDataEvent e) {}

    public void enablePlayoutUI(boolean enable) {
	playoutUIEnabled = enable;
	RadioProgramMetadata metadata = null;
	metadata = (RadioProgramMetadata)guiPlaylist.getFirstProgram();
	
	if (metadata != null && metadata.getPlayoutStoreAttempts() == FileStoreManager.UPLOAD_DONE) {
	    play.setEnabled(enable);
	    pause.setEnabled(enable);
	    stop.setEnabled(enable);
	    //seekSlider.setEnabled(enable);
	} else {
	    play.setEnabled(false);
	    pause.setEnabled(false);
	    stop.setEnabled(false);
	    seekSlider.setEnabled(false);
	}
	

	Iterable<PlaylistWidget.PlayItemWidget> listElements = guiPlaylist.getListElements();
	for (PlaylistWidget.PlayItemWidget itemWidget: listElements) {
	    metadata = (RadioProgramMetadata)itemWidget.getMetadata();
	    if (metadata.getPlayoutStoreAttempts() == FileStoreManager.UPLOAD_DONE) {
		itemWidget.getPlayButton().setEnabled(enable);
	    } else {
		itemWidget.getPlayButton().setEnabled(false);
	    }
	}
	listPane.repaint();
    }


    public void activatePreview(boolean activate) {
	enablePreviewUI(activate);
	if (!activate) {	
	    Iterable<PlaylistWidget.PlayItemWidget> listElements = guiPlaylist.getListElements();
	    for (PlaylistWidget.PlayItemWidget itemWidget: listElements) 
		itemWidget.getPreviewButton().setPressed(false);
	    listPane.repaint();	    
	}
    }

    public void enablePreviewUI(boolean enable) {
	previewUIEnabled = enable;
	RadioProgramMetadata metadata = null;

	Iterable<PlaylistWidget.PlayItemWidget> listElements = guiPlaylist.getListElements();
	for (PlaylistWidget.PlayItemWidget itemWidget: listElements) {
	    metadata = (RadioProgramMetadata)itemWidget.getMetadata();
	    if (metadata.getPreviewStoreAttempts() == FileStoreManager.UPLOAD_DONE) {
		itemWidget.getPreviewButton().setEnabled(enable);
		itemWidget.getStopButton().setEnabled(enable);
	    } else {
		itemWidget.getPreviewButton().setEnabled(false);
		itemWidget.getStopButton().setEnabled(false);
	    }
	}
	listPane.repaint();
    }

    public void activateLiveNext(boolean activate) {
	nextButton.setEnabled(activate);
    }

    public void activateLivePreview(boolean activate) {
	enableLivePreviewUI(activate);

	if (!activate){
	    currentElementPreviewButton.setPressed(false);
	    currentElementPreviewPressed = false;
	}
	
    }

    public void enableLivePreviewUI(boolean enable) {
	livePreviewUIEnabled = enable;
	RadioProgramMetadata metadata = null;
	metadata = (RadioProgramMetadata)guiPlaylist.getFirstProgram();
	
	if (metadata != null && metadata.getPreviewStoreAttempts() == FileStoreManager.UPLOAD_DONE) {	
	    currentElementPreviewStopButton.setEnabled(enable);
	    currentElementPreviewButton.setEnabled(enable);
	} else {
	    currentElementPreviewStopButton.setEnabled(false);
	    currentElementPreviewButton.setEnabled(false);
	}

    }

    public void activateLiveInfo(boolean activate) {
	currentElementInfoButton.setEnabled(activate);
    }

    public void activateInfo(boolean activate) {
	Iterable<PlaylistWidget.PlayItemWidget> listElements = guiPlaylist.getListElements();
	for (PlaylistWidget.PlayItemWidget itemWidget: listElements) {
	    itemWidget.getInfoButton().setEnabled(activate);
	}
	listPane.repaint();
    }

    public void activateArchiverButton(boolean activate) {
	goLive.setEnabled(activate);
    }

    // XXX duplicate method
    public void selectedArchiverButton(boolean pressed) {
	goLive.setSelected(pressed);
    }
    
    public void enablePlayFunctions(boolean enabled) {
	play.setEnabled(enabled);
	pause.setEnabled(enabled);
	stop.setEnabled(enabled);
	//seekSlider.setEnabled(enabled);
    }
    
    public void enableSeek(boolean enabled){
	seekSlider.setEnabled (enabled);
    }

    RadioProgramMetadata currentProgram = null;

    public boolean isCurrentLivePaneItem(String itemID) {
	if (currentProgram != null && currentProgram.getItemID().equals(itemID))
	    return true;
	else
	    return false;
    }

    public void initLivePane(RadioProgramMetadata program) {
	if (program != null) {
	    currentProgram = program;
	    currentElementTitleLabel1.setText(program.getTitle());
	    currentElementTitleLabel1.setToolTipText(program.getTitle());

	    PlayItemWidget playItemWidget = guiPlaylist.getPlayItemWidget(program.getItemID());
	    //currentElementDurationLabel.setText(getDurationString(playItemWidget.getGoliveDuration()));
	    currentElementDuration = playItemWidget.getGoliveDuration();
	    
	    livePane.setBackground(playItemWidget.getCurrBackground());

	    seekSlider.setMinimum(0);
	    int duration = (int)playItemWidget.getDuration();
	    seekSlider.setMaximum(duration);
	    seekSlider.setValue(0);
	    sliderLabel.setText(formatSliderLabel(0, duration));

	    Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();

	    sliderLabel.setForeground (new Color(128, 128, 128));
	    labelTable.put(duration/2, sliderLabel);

	    seekSlider.setLabelTable(labelTable);
	    seekSlider.setPaintLabels(true);
	    enableSeek(true);
	} else {
	    seekSlider.setPaintLabels(false);

	    currentElementTimeLeftLabel.setText("");
	    currentElementTitleLabel1.setText("");
	    currentElementTitleLabel1.setToolTipText("");
	    currentElementDurationLabel.setText("");
	    currentElementDuration = 0;

	    livePane.setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_LIVEPANE_COLOR));
	}
    }

    final static long SEC_MS 	= 1000L;
    final static long MINUTE_MS = 60L * SEC_MS;
    final static long HOUR_MS 	= 60L * MINUTE_MS;

    protected String formatSliderLabel(int value, int duration){
	if (duration < MINUTE_MS){
	    return String.format("%02d:%02d/%02d:%02d", 0, value/SEC_MS, 0, duration/SEC_MS);
	} else if (duration < HOUR_MS){
	    return String.format("%02d:%02d/%02d:%02d", value/MINUTE_MS, (value/SEC_MS)%60, duration/MINUTE_MS, (duration/SEC_MS)%60);
	} else {
	    return String.format("%d:%02d:%02d/%d:%02d:%02d", value/HOUR_MS, (value/MINUTE_MS)%60, (value/SEC_MS)%60,
				 duration/HOUR_MS, (duration/MINUTE_MS)%60, (duration/SEC_MS)%60);
	}
	//return String.format("%02d:%02d",duration/(60L*1000L),(duration/1000L)%60);
    }

    public void updateFirstProgram(RadioProgramMetadata program){
	currentElementTitleLabel1.setText(program.getTitle());
	currentElementTitleLabel1.setToolTipText(program.getTitle());
    }

    Object removeLock = new Object();
    public PlayItemWidget removePlaylistItem(int itemIndex){

	PlayItemWidget playItemWidget;

	synchronized(removeLock){
	    playItemWidget = guiPlaylist.removeItemAt(itemIndex);
	}

	scrollPane.validate();

	return playItemWidget;
    }

    public Dimension getPlayItemSize(){
	return playItemSize;
    }

    public int getListElemGoliveXoff(){
	return (int)(panel.getWidth() * (100 - liveItemHorizFrac) / 100.0);
    }

    public int getListElemGoliveYoff(){
	return (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2);
    }

    public int getSelectedIndex(){
	return selectedIndex+1 ;
    }

    public int getPanelWidth(){
	return panel.getWidth();
    }
        
    public void initPlaylist(GUIPlaylist guiPlaylist, Playlist playlist) {
	ArrayList<PlayItemWidget> itemWidgets = new ArrayList<PlayItemWidget>();

	int position = guiPlaylist.size() - playlist.size()-1;

	int i = position + 1;
	position = (position<0)?0:position;

	String[] programs = playlist.getPrograms();
	PlaylistItem[] playlistItems = playlist.getPlaylistItems();

	int j = 0;

	logger.debug("initPlaylist: programs = " + programs.length + ": i = " + i);
	for (String programID: programs) {
	    PlayItemWidget itemWidget;

	    logger.debug("initPlaylist: " + programID + ":" + position + ":" + i + ":" + j);

	    if (guiPlaylist.firstProgram() != null && i>0) {
		itemWidget = new PlaylistWidget.PlayItemWidget(playlist.getProgram(programID),
							       getPlayItemSize());
		itemWidgets.add(itemWidget);
		logger.debug("initplaylist: firstprogram not null-- " + programID + ":" + position);

	    } else {
		guiPlaylist.setFirstProgram(playlist.getProgram(programID));
		guiPlaylist.setFirstProgramItemWidget(itemWidget = new PlaylistWidget.PlayItemWidget(playlist.getProgram(programID),
												     getPlayItemSize()));
		logger.debug("initplaylist: firstprogram null-- " + programID);

	    }

	    if (programID.startsWith(SimulationWidget.GOLIVE_ITEMID) && playlistItems != null || 
	       playlistItems != null && playlistItems[j].getLiveItem()) {
		itemWidget.isLiveItem(playlistItems[j].getLiveItem());
		itemWidget.playBackgroundMusic(playlistItems[j].getPlayBackground());
		itemWidget.setGoliveDuration(playlistItems[j].getLiveDuration());
	    }
	    
	    if (playlistItems != null)
		itemWidget.setFadeoutDuration(playlistItems[j].getFadeout());

	    i++;
	    j++;
	}

	logger.debug("initPlaylist: leaving");

	addItemWidgetsAt(itemWidgets, position, false);
    }

    public void addPlayItems(ArrayList<PlayItemWidget> itemWidgets, int index){
	addItemWidgetsAt(itemWidgets, index, true);
    }

    public void addPrograms(ArrayList<RadioProgramMetadata> programList, int index){
	ArrayList <PlayItemWidget> itemWidgets = new ArrayList<PlayItemWidget>();
	for (RadioProgramMetadata program: programList){
	    itemWidgets.add(new PlayItemWidget(program, getPlayItemSize()));
	}
	addItemWidgetsAt(itemWidgets, index, false);
    }

    protected void addItemWidgetsAt(ArrayList<PlayItemWidget> itemWidgets, int index, boolean addToOrdering){
	long startTime = 0;
	if (index > 0) {
	    startTime = guiPlaylist.getItemAt(index).getTime() + guiPlaylist.getItemAt(index).getMetadata().getLength();
	} else {
	    /*
	    startTime = currentStartTime;
	    if (currentRemainingTime > 0)
		startTime += currentRemainingTime;
	    else
		startTime += currentElementDuration;
	    */
	}
	for (PlayItemWidget itemWidget: itemWidgets){
	    itemWidget.setTime(startTime);
	    startTime += itemWidget.getMetadata().getLength();
	    guiPlaylist.addItem(itemWidget, index++, addToOrdering);
	}
	scrollPane.validate();
    }

    public void clearPlaylist() {
	guiPlaylist.clear();
	scrollPane.validate();
	currentProgram = null;
	seekSlider.setValue(0);
    }

    public void pause(){
	pause.doClick();
    }

    public void activatePlay(boolean activate) {
	if (activate) {
	    play.setPressed(true);
	    pause.setPressed(false);
	    enableSeek(false);
	} else {
	    play.setPressed(false);
	}
    }

    public boolean isPauseActive() {
	return pause.isPressed();
    }

    public void activatePause(boolean activate) {
	if (activate) {
	    pause.setPressed(true);
	    play.setPressed(false);
	} else {
	    pause.setPressed(false);
	}
    }

    public void activateStop() {
	play.setPressed(false);
	pause.setPressed(false);
	enableSeek(true);
    }

    public void activateCurrentElementPreview(boolean activate) {
	currentElementPreviewPressed = activate;
	currentElementPreviewButton.setPressed(activate);
    }

    public void activateCurrentElementPreviewStop(boolean activate) {
	currentElementPreviewPressed = false;
	currentElementPreviewButton.setPressed(false);
    }

    public void activatePreview(int itemIndex, boolean activate) {
	PlaylistWidget.PlayItemWidget itemWidget = guiPlaylist.getItemAt(itemIndex);
	if (itemWidget != null)
	    itemWidget.setPreviewPressed(activate);
    }
    
    public void activateStop(int itemIndex) {
	PlaylistWidget.PlayItemWidget itemWidget = guiPlaylist.getItemAt(itemIndex);
	if (itemWidget != null)
	    //itemWidget.getPreviewButton().setPressed(false);
	    itemWidget.setPreviewPressed(false);
    }

    public void activateGoLive(boolean activate) {
	goLive.setSelected(activate);
	if (activate) {
	    goLive.setIcon(getIcon(goLiveActiveIconID, (int)goLiveDim.getWidth(), (int)goLiveDim.getHeight()));
	} else {
	    goLive.setIcon(getIcon(goLiveIconID, (int)goLiveDim.getWidth(), (int)goLiveDim.getHeight()));
	}
    }

    public void activatePlaylistClear(boolean activate) {
	clearPlaylist.setEnabled(activate);	
    }

    public void triggerPlay() {
	play.doClick();
    }

    public void setStartTime(long startTime, long remainingTime) {
	long currTime;
	//Color timeColor = stationConfig.getColor(StationConfiguration.
	//currentElementTimeLeftLabel.setText(GUIUtilities.getColoredString(getDateString(startTime), ));
	currentElementTimeLeftLabel.setText(getDateString(startTime));
	
	currTime = startTime;
	if (remainingTime < 0) {
	    if (guiPlaylist.size() > 0 && 
		guiPlaylist.getPlayItemWidget(guiPlaylist.firstProgram()).getGoliveDuration() >= 0)
		
		currTime += guiPlaylist.getPlayItemWidget(guiPlaylist.firstProgram()).getGoliveDuration();
	    
	} else {
	    currTime += remainingTime;
	}

	Iterable<PlaylistWidget.PlayItemWidget> listElements = guiPlaylist.getListElements();

	for (PlaylistWidget.PlayItemWidget itemWidget: listElements) {
	    itemWidget.setTime(currTime);
	    if (itemWidget.getGoliveDuration() >= 0)
		currTime += itemWidget.getGoliveDuration();
	}

	listPane.repaint();
    }

    public void setCurrentElementTimeRemaining(long remainingTime) {
	currentElementTimeLeftLabel.setText(durationStringFromMillis(remainingTime));
	seekSlider.setValue((int) (seekSlider.getMaximum() - remainingTime));
    }

    
    void init() {
	initTopLevelPanes();
	initTopPane();
	initVolPane();
	initLivePane();
	initListPane();
	initListControlPane();
    }

    void printComponents (Container container){
	for (Component c: container.getComponents()){
	    logger.info(c.getName()+":Preferred: "+c.getPreferredSize()+" Minimum: "+c.getMinimumSize()+" Current:"+c.getSize());
	}
    }
    
    void fixSize(Component c, Dimension dim){
	
	c.setSize(dim);
	c.setMinimumSize(dim);
	c.setPreferredSize(dim);
	
    }

    public RSButton getSaveButton() {
	return savePlaylist;
    }

    public RSButton getLoadButton() {
	return loadPlaylist;
    }

    public RSButton getStartTimeButton() {
	return setStartTimePlaylist;
    }

    public RSButton getClearButton() {
	return clearPlaylist;
    }

    public JList getListPane() {
	return listPane;
    }

    public void repaintList() {
	if (listPane != null)
	    listPane.repaint();
    }

    protected void initTopLevelPanes() {
	topPane = new JPanel();
	volPane = new JPanel();
	livePane = new JPanel();
	listPane = new JList(guiPlaylist);
	listModel = listPane.getModel();
	listPane.setCellRenderer(new ListCellRenderer(){
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus){
		    PlayItemWidget item = (PlayItemWidget) value;
		    if (isSelected)
			item.setSelected();
		    else
			item.setUnSelected();

		    return item;
		}
	    });
	scrollPane = new JScrollPane(listPane, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(0,0));
	scrollPane.getVerticalScrollBar().setUnitIncrement(10);

	listControlPane = new JPanel();

	Dimension topDimension = new Dimension(panel.getWidth(), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	fixSize(topPane, topDimension);
	volDimension = new Dimension(panel.getWidth(), (int)(panel.getHeight() * volPaneVertFrac / 100.0));
	fixSize(volPane, volDimension);
	liveDimension = new Dimension(panel.getWidth(), (int)(panel.getHeight() * livePaneVertFrac / 100.0));
	//fixSize(livePane, liveDimension);
	livePane.setMinimumSize(new Dimension(liveDimension.width, liveDimension.height + 13));
	livePane.setMaximumSize(new Dimension(liveDimension.width, Integer.MAX_VALUE));
	Dimension scrollDimension = new Dimension(panel.getWidth(), (int)(panel.getHeight() * listPaneVertFrac / 100.0));
	fixSize(scrollPane, scrollDimension);
	Dimension listControlDimension = new Dimension(panel.getWidth(), (int)(panel.getHeight() * listControlPaneVertFrac / 100.0));
	fixSize(listControlPane, listControlDimension);

	panel.setLayout(new GridBagLayout());
	panel.add(topPane, getGbc(0, 0, gfillboth()));
	panel.add(volPane, getGbc(0, 1, gfillboth()));
	panel.add(livePane, getGbc(0, 2, gfillboth()));
	panel.add(scrollPane, getGbc(0, 3, gfillboth(), weighty(1.0)));
	panel.add(listControlPane, getGbc(0, 4, gfillboth()));

	panel.validate();
    }

    protected void initTopPane() {

	goLive = new RSToggleButton("");
	goLive.setActionCommand(GOLIVE); goLive.addActionListener(playlistController);
	goLive.setToolTipText("Record Mic");

	play = new RSToggleButton(""){
		public void fireActionPerformed(ActionEvent e){
		    if (!isSelected())
			setSelected(true);
		    else
			super.fireActionPerformed(e);
		}
	    };
	final RSToggleButton playButton = play;

	play.setActionCommand(PLAY); play.addActionListener(playlistController);
	play.setToolTipText("Play");

	pause = new RSToggleButton(""){
		public void fireActionPerformed(ActionEvent e){
		    if (!playButton.isSelected() && isSelected()){
			setSelected(false);
			return;
		    }
		    if (!isSelected())
			setSelected(true);
		    else
			super.fireActionPerformed(e);
		}
	    };
	pause.setActionCommand(PAUSE); pause.addActionListener(playlistController);
	pause.setToolTipText("Pause");

	stop = new RSButton(""); 
	stop.setActionCommand(STOP); stop.addActionListener(playlistController);
	stop.setToolTipText("Stop");

	goLiveDim = new Dimension((int)(panel.getWidth() * goliveHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	goLive.setPreferredSize(goLiveDim);
	Dimension playDim = new Dimension((int)(panel.getWidth() * playHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	play.setPreferredSize(playDim);
	Dimension pauseDim = new Dimension((int)(panel.getWidth() * pauseHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	pause.setPreferredSize(pauseDim);
	Dimension stopDim = new Dimension((int)(panel.getWidth() * stopHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	stop.setPreferredSize(stopDim);

	if (stationConfig.isMicServiceActive()) {
	    goLiveIconID = StationConfiguration.GOLIVE_ICON;
	    goLiveActiveIconID = StationConfiguration.GOLIVE_ACTIVE_ICON;
	} else {
	    goLiveIconID = StationConfiguration.RECORD_ICON;
	    goLiveActiveIconID = StationConfiguration.RECORD_ACTIVE_ICON;
	}

	goLive.setIcon(getIcon(goLiveIconID, (int)goLiveDim.getWidth(), (int)goLiveDim.getHeight()));
	play.setIcon(getIcon(StationConfiguration.PLAY_ICON, (int)playDim.getWidth(), (int)playDim.getHeight()));
	pause.setIcon(getIcon(StationConfiguration.PAUSE_ICON, (int)pauseDim.getWidth(), (int)pauseDim.getHeight())); 
	stop.setIcon(getIcon(StationConfiguration.STOP_ICON, (int)stopDim.getWidth(), (int)stopDim.getHeight()));
	

	nextButton = new RSButton(""); nextButton.setActionCommand(NEXTPROG);
	nextButton.addActionListener(playlistController);
	nextButton.setToolTipText("Next item");

	nextButton.setPreferredSize(new Dimension((int)(panel.getWidth() * nextHorizFrac / 100.0),
								(int)(panel.getHeight() * livePaneVertFrac / 100.0)));
	nextButton.setIcon(getIcon(StationConfiguration.FWD_ICON,
							       (int)(panel.getWidth() * nextHorizFrac / 100.0),
							       (int)(panel.getHeight() * livePaneVertFrac / 100.0)));


	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	goLive.setBackground(bgColor);
	play.setBackground(bgColor);
	pause.setBackground(bgColor);
	stop.setBackground(bgColor);
	nextButton.setBackground(bgColor);

	topPane.setLayout(new GridBagLayout());
	topPane.add(goLive, getGbc(0, 0, gridheight(2), gfillboth()));
	topPane.add(play, getGbc(1, 0, gridheight(2), gfillboth()));
	topPane.add(pause, getGbc(2, 0, gridheight(2), gfillboth()));
	topPane.add(stop, getGbc(3, 0, gridheight(2), gfillboth()));
	topPane.add(nextButton, getGbc(4, 0, gridheight(2), gfillboth()));

	topPane.validate();
    }

    JLabel sliderLabel = new JLabel("");
    protected void initVolPane(){
	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);

	UIManager.put("Slider.horizontalThumbIcon", sliderIcon);
	seekSlider = new JSlider();
	Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
	seekSlider.setLabelTable(labelTable);

	volPane.setBackground(bgColor);
	volPane.setLayout (new GridBagLayout());
	
	volPane.add(seekSlider, getGbc(0, 0, gfillboth(), weightx(2.0)));

	seekSlider.setPaintLabels(false);
	seekSlider.setValue(0);
	seekSlider.setBackground(bgColor);

	seekSlider.addChangeListener(new ChangeListener(){
		@SuppressWarnings("unchecked")
		public void stateChanged(ChangeEvent e){
		    int value = seekSlider.getValue();
		    sliderLabel.setText(formatSliderLabel(value, (int)currentElementDuration));
		    seekSlider.repaint();

		    if (seekSlider.getValueIsAdjusting() && seekSlider.isEnabled()){
			return;
		    }
		    
		    playlistController.setSeekPosition(value);
		}
		
	    });

	seekSlider.addMouseListener(new MouseAdapter(){
		public void mousePressed(MouseEvent e){
		    if (!seekSlider.isEnabled())
			return;
		    
		    int leftOffset = seekSlider.getFontMetrics(seekSlider.getFont()).stringWidth(sliderLabel.getText())/2;
		    int max = seekSlider.getMaximum();
		    int value = (int) (max*(e.getPoint().getX() - leftOffset)/(seekSlider.getSize().width - 2 *leftOffset));
		    if (value < 0)
			value = 0;
		    if (value > max)
			value = max;

		    seekSlider.setValue(value);
		}

	    });

	volumeDisplay = new VolumeDisplay(playlistController, new Dimension((int) (panel.getWidth() * volHorizFrac / 100.0), volDimension.height), stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR), MIN_GAIN, MAX_GAIN);
	
	volPane.add(volumeDisplay);
    }

    public int getSliderPosition(){
	return seekSlider.getValue();
    }

    public void setGain(int gain){
	volumeDisplay.setGain(gain);
    }

    protected void initLivePane() {

	currentElementTimeLeftLabel = new RSLabel("");
	currentElementTimeLeftLabel.setFont(currentElementTimeLeftLabel.getFont().deriveFont(Font.BOLD));
	currentElementTitleLabel1 = new RSLabel("");
	currentElementTitleLabel1.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_TITLE_COLOR));
	currentElementTitleLabel1.setHorizontalAlignment(SwingConstants.CENTER);
	currentElementDurationLabel = new RSLabel("");
	currentElementDurationLabel.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_DETAILS_COLOR));
	currentElementInfoButton = new RSButton(""); currentElementInfoButton.setActionCommand(CURRINFO); 
	currentElementInfoButton.addActionListener(playlistController);
	currentElementInfoButton.setToolTipText("Info");

	currentElementPreviewButton = new RSToggleButton(""){
		public void doClick(){
		    if (isSelected())
			return;
		    else
			super.doClick();
		}
	    };
	
	currentElementPreviewButton.setActionCommand(CURRPREVIEW);
	currentElementPreviewButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    selectedIndex = -2;
		    playlistController.actionPerformed(e);
		    currentElementPreviewButton.setPressed(currentElementPreviewPressed);
		}
	    });
	currentElementPreviewButton.setToolTipText("Preview");
	//currentElementPreviewButton.addActionListener(playlistController);

	currentElementPreviewStopButton = new RSButton(""); currentElementPreviewStopButton.setActionCommand(CURRPREVIEWSTOP);
	currentElementPreviewStopButton.addActionListener(playlistController);
	currentElementPreviewStopButton.setToolTipText("Stop");
	
	
	currentElementInfoButton.setIcon(getIcon(StationConfiguration.INFO_ICON,
							       (int)(panel.getWidth() * currentElementInfoHorizFrac / 100.0),
							       (int)(panel.getHeight() * livePaneVertFrac / 100.0 / 2)));
	currentElementPreviewButton.setIcon(getIcon(StationConfiguration.PREVIEW_ICON,
								  (int)(panel.getWidth() * currentElementPreviewHorizFrac / 100.0),
								  (int)(panel.getHeight() * livePaneVertFrac / 100.0 / 2)));
	currentElementPreviewStopButton.setIcon(getIcon(StationConfiguration.PREVIEW_STOP_ICON,
								      (int)(panel.getWidth() * currentElementPreviewStopHorizFrac / 100.0),
								      (int)(panel.getHeight() * livePaneVertFrac / 100.0 / 2)));

	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_LIVEPANE_COLOR);
	livePane.setBackground(bgColor);
	//currentElementTimeLeftLabel.setBackground(bgColor);
	//currentElementTitleLabel1.setBackground(bgColor);
	//currentElementDurationLabel.setBackground(bgColor);
	currentElementInfoButton.setBackground(bgColor);
	currentElementPreviewButton.setBackground(bgColor);
	currentElementPreviewStopButton.setBackground(bgColor);

	livePane.setLayout(new GridBagLayout());
	livePane.add(currentElementTimeLeftLabel, getGbc(0, 0, gfillboth(), gridheight(2)));
	livePane.add(currentElementTitleLabel1, getGbc(1, 0, gfillboth(), weightx(1.0)));
	//livePane.add(currentElementDurationLabel, getGbc(1, 1, gfillboth(), weightx(1.0)));
	livePane.add(currentElementInfoButton, getGbc(3, 0, gfillboth(), gridwidth(2)));
	livePane.add(currentElementPreviewButton, getGbc(3, 1, gfillboth()));
	livePane.add(currentElementPreviewStopButton, getGbc(4, 1, gfillboth()));

	livePane.validate();

    }	
    
    protected void initListPane() {
	
	listPane.setDropMode(DropMode.INSERT);
	listPane.setDragEnabled(true);

	playItemSize = new Dimension(panel.getWidth(), (int)(panel.getHeight() * listElementVertFrac / 100.0));

	TransferHandler transferHandler = new TransferHandler(){
		int selectionIndices[];
		int importIndex;

		public boolean canImport (TransferHandler.TransferSupport support){
		    if (!support.isDrop())
			return false;
		    
		    if (!support.isDataFlavorSupported(ProgramSelection.getFlavor()) &&
		       !support.isDataFlavorSupported(PlayItemSelection.getFlavor())){

			return false;
		    }

		    return true;
		}

		@SuppressWarnings("unchecked") 
		public boolean importData(TransferHandler.TransferSupport info){
		    if (!info.isDrop())
			return false;

		    Transferable t = info.getTransferable();
		    ArrayList<RadioProgramMetadata> programList = null;
		    ArrayList<PlayItemWidget> playItemList = null;
		    
		    boolean isProgramSelection = info.isDataFlavorSupported(ProgramSelection.getFlavor());

		    try{

			if (isProgramSelection)
			    programList = (ArrayList<RadioProgramMetadata>)t.getTransferData(ProgramSelection.getFlavor());
			else
			    playItemList = (ArrayList<PlayItemWidget>)t.getTransferData(PlayItemSelection.getFlavor());

		    } catch(Exception e){
			logger.error("ImportData: Exception.", e);
			return false;
		    }
		    
		    JList.DropLocation dropLocation = (JList.DropLocation)info.getDropLocation();
		    int index = dropLocation.getIndex();
		    int numItems = 0;

		    if (isProgramSelection){
			for (RadioProgramMetadata m : programList) 
			    logger.info("GRINS_USAGE:DROP_TO_PLAYLIST:"+m.getItemID());
			playlistController.addPrograms(programList, index);
			numItems = programList.size();
		    } else {
			playlistController.addPlayItems(playItemList, index);
			numItems = playItemList.size();
		    }

		    importIndex = index;
		    listPane.ensureIndexIsVisible(index + numItems - 1);

		    return true;
		}

		public int getSourceActions(JComponent c){
		    return COPY_OR_MOVE;
		}
		
		public Transferable createTransferable (JComponent c){
		    ArrayList<PlayItemWidget> list = new ArrayList<PlayItemWidget>();

		    selectionIndices = ((JList)c).getSelectedIndices();

		    for (int i: selectionIndices){
			list.add((PlayItemWidget)guiPlaylist.getElementAt(i));
		    }

		    return new PlayItemSelection(list);
		}
		
		public void exportDone(JComponent c, Transferable t, int action){
		    switch(action){

		    case COPY:
			break;

		    case MOVE:
			if (c == listPane){
			    int previewIndex = guiPlaylist.getPreviewIndex();
			    if (importIndex < previewIndex)
				previewIndex -= selectionIndices.length;
			    previewIndex --;

			    boolean previewMoved = false;
			    for (int i: selectionIndices)
				if (i == previewIndex)
				    previewMoved = true;
			    
			    int lost = 0, offset = 0;
			    for (int index: selectionIndices){
				if (index < previewIndex)
				    offset++;
				if (index < importIndex)
				    lost++;
			    }
			    
			    int newPreviewIndex = -1;
			    if (previewMoved){
				newPreviewIndex = importIndex + offset - lost + 1;
			    }

			    if (selectionIndices.length > 0){
				for (int i = 0; i< selectionIndices.length; i++){
				    if (importIndex <= selectionIndices[i])
					selectionIndices[i] += selectionIndices.length;
				}
			    }

			    playlistController.removeProgramsAt(selectionIndices, false, newPreviewIndex);
			}
			break;
		    }
		}

	    };

	listPane.setTransferHandler(transferHandler);

	final JPopupMenu rightClickPopup = new JPopupMenu("Insert Live Item/Remove Item");
	final JMenuItem insertItem = new JMenuItem();
	final JMenuItem removeItem = new JMenuItem();
	
	int menuIconSize = playItemSize.height/2;

	Action insertAction = new AbstractAction("Insert Live Item", 
						 IconUtilities.getIcon(StationConfiguration.ADD_ICON, 
								       menuIconSize, menuIconSize)) {
		public void actionPerformed(ActionEvent e) {
		    int index = 0;
		    if (listPane.getSelectedIndices() != null && listPane.getSelectedIndices().length > 0)
			index = listPane.getSelectedIndices()[0];
		    RadioProgramMetadata goliveMetadata = RadioProgramMetadata.getDummyObject(GOLIVE_ITEMID + "-" + Math.random());
		    logger.info("GRINS_USAGE:INSERT_LIVE_ITEM");
		    goliveMetadata.setTitle("Go live!");
		    goliveMetadata.setPlayoutStoreAttempts(0);
		    goliveMetadata.setPreviewStoreAttempts(0);
		    goliveMetadata.setUploadStoreAttempts(0);
		    goliveMetadata.setArchiveStoreAttempts(0);

		    ArrayList<RadioProgramMetadata> goliveArr = new ArrayList<RadioProgramMetadata>();
		    goliveArr.add(goliveMetadata);

		    playlistController.addPrograms(goliveArr, index);
		    
		    listPane.getSelectionModel().setSelectionInterval(index, index);
		}
	    };

	Action removeAction = new AbstractAction("Remove Item(s)", 
						 IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, 
								       menuIconSize, menuIconSize)) {
		public void actionPerformed(ActionEvent e) {
		    int[] selectedIndices = listPane.getSelectedIndices();
		    playlistController.removeProgramsAt(selectedIndices, true, -1);
		}
	    };
	
	insertItem.setAction(insertAction);
	removeItem.setAction(removeAction);

	rightClickPopup.add(insertItem);
	rightClickPopup.add(removeItem);

	listPane.addMouseListener(new MouseAdapter(){
		Dimension cellSize = getPlayItemSize();
		
		public void mouseClicked(MouseEvent e){
		    int index = e.getY()/cellSize.height;
		    
		    if (index >= listModel.getSize())
			return;
		    
		    selectedIndex = index;

		    PlayItemWidget itemWidget = (PlayItemWidget) listModel.getElementAt(index);

		    if (itemWidget != null){
			itemWidget.mouseClicked(e, index);
			listPane.repaint();
		    }
		}
		
		public void mousePressed(MouseEvent e){
		    int index = e.getY()/cellSize.height;
		    
		    if (index >= listModel.getSize())
			return;

		    selectedIndex = index;

		    PlayItemWidget itemWidget = (PlayItemWidget) listModel.getElementAt(index);

		    if (itemWidget != null){
			itemWidget.mousePressed(e);
			listPane.repaint();
		    }
		    
		    if (e.getButton() == MouseEvent.BUTTON3){
			rightClickPopup.show(listPane, e.getX(), e.getY());
		    }
		}

		public void mouseReleased(MouseEvent e){

		    int index = e.getY()/cellSize.height;
		    
		    if (index >= listModel.getSize())
			return;

		    selectedIndex = index;

		    PlayItemWidget itemWidget = (PlayItemWidget) listModel.getElementAt(index);
		    
		    if (itemWidget != null){
			itemWidget.mouseReleased(e);
			listPane.repaint();
		    }
		    
		}
	    });
	
	listPane.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "removeitem");
	listPane.getActionMap().put("removeitem", removeAction);

	listPane.getInputMap().put(KeyStroke.getKeyStroke("INSERT"), "insertlive");
	listPane.getActionMap().put("insertlive", insertAction);
	
    }
    
    protected void initListControlPane() {
	
	clearPlaylist = new RSButton(""); 
	clearPlaylist.setActionCommand(CLEAR_LIST); 
	clearPlaylist.addActionListener(playlistController);
	clearPlaylist.setToolTipText("Clear playlist");

	loadPlaylist = new RSButton(""); 
	loadPlaylist.setActionCommand(LOAD_LIST); 
	loadPlaylist.addMouseListener(new MouseAdapter(){
		public void mousePressed(MouseEvent e){
		    playlistController.loadPlaylists();
		}
	    });
	loadPlaylist.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    playlistController.loadPlaylists();
		}
	    });
	loadPlaylist.setToolTipText("Load playlist");

	savePlaylist = new RSButton(""); 
	savePlaylist.setActionCommand(SAVE_LIST); 
	savePlaylist.addActionListener(playlistController);
	savePlaylist.setToolTipText("Save playlist");

	setStartTimePlaylist = new RSButton(""); 
	setStartTimePlaylist.setActionCommand(SETSTART_LIST); 
	setStartTimePlaylist.addActionListener(playlistController);
	setStartTimePlaylist.setToolTipText("Set start time");

	int buttonHeight = (int)(panel.getHeight() * listControlPaneVertFrac / 100.0);
	clearPlaylist.setPreferredSize(new Dimension((int)(panel.getWidth() * clearPlaylistHorizFrac / 100.0), 
						     buttonHeight));	
	loadPlaylist.setPreferredSize(new Dimension((int)(panel.getWidth() * loadPlaylistHorizFrac / 100.0), 
						    buttonHeight));
	savePlaylist.setPreferredSize(new Dimension((int)(panel.getWidth() * savePlaylistHorizFrac / 100.0), 
						    buttonHeight));
	setStartTimePlaylist.setPreferredSize(new Dimension((int)(panel.getWidth() * setStartTimePlaylistHorizFrac / 100.0),
							    buttonHeight));
	
	int iconSize = (int)(panel.getHeight() * listControlPaneVertFrac / 100.0);

	clearPlaylist.setIcon(getIcon(StationConfiguration.DELETE_ICON, iconSize));
	loadPlaylist.setIcon(getIcon(StationConfiguration.LOAD_PLAYLIST_ICON, iconSize));
	savePlaylist.setIcon(getIcon(StationConfiguration.SAVE_ICON, iconSize));
	setStartTimePlaylist.setIcon(getIcon(StationConfiguration.SETSTART_ICON, iconSize));
	
	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_CONTROLPANE_COLOR);
	clearPlaylist.setBackground(bgColor);
	loadPlaylist.setBackground(bgColor);
	savePlaylist.setBackground(bgColor);
	setStartTimePlaylist.setBackground(bgColor);

	listControlPane.setLayout(new GridBagLayout());
	listControlPane.add(clearPlaylist, getGbc(0, 0, gfillboth()));
	listControlPane.add(loadPlaylist, getGbc(1, 0, gfillboth()));
	listControlPane.add(savePlaylist, getGbc(2, 0, gfillboth()));
	listControlPane.add(setStartTimePlaylist, getGbc(3, 0, gfillboth()));
	
	listControlPane.validate();
    }

    public static String getDateString(long time) {
	if (time > -1) {
	    Date d = new Date(time);
	    DateFormat fmt = DateFormat.getTimeInstance(DateFormat.MEDIUM);
	    return fmt.format(d);
	} else {
	    return "00:00:00";
	}
    }

    protected final static int topPaneVertFrac = 7;
    protected final static int volPaneVertFrac = 6;
    protected final static int livePaneVertFrac = 7;
    protected final static int listPaneVertFrac = 77;
    protected final static int listElementVertFrac = 7;
    protected final static int listControlPaneVertFrac = 4;

    protected final static int goliveHorizFrac = 20;
    protected final static int playHorizFrac = 20;
    protected final static int pauseHorizFrac = 20;
    protected final static int stopHorizFrac = 20;
    protected final static int nextHorizFrac = 20;

    protected final static int volHorizFrac = 27;

    protected final static int currentElementTimeLeftHorizFrac = 25;
    protected final static int currentElementTitleHorizFrac = 35;
    protected final static int currentElementDurationHorizFrac = 25;
    protected final static int currentElementInfoHorizFrac = 20;
    protected final static int currentElementPreviewHorizFrac = 10;
    protected final static int currentElementPreviewStopHorizFrac = 10;

    protected final static int playItemHorizFrac = 12;
    protected final static int durationItemHorizFrac = 18;
    protected final static int titleItemHorizFrac = 30;
    protected final static int previewItemHorizFrac = 8;
    protected final static int stopItemHorizFrac = 8;
    protected final static int infoItemHorizFrac = 8;
    protected final static int liveItemHorizFrac = 8;
    protected final static int fadeItemHorizFrac = 8;

    protected final static int clearPlaylistHorizFrac = 25;
    protected final static int loadPlaylistHorizFrac = 25;
    protected final static int savePlaylistHorizFrac = 25;
    protected final static int setStartTimePlaylistHorizFrac = 25;

    public class PlayItemWidget extends JPanel {
	RadioProgramMetadata metadata;

	RSLabel timeLabel;
	RSButton play, stop;
	RSButton preview;
	RSLabel titleLabel1, titleLabel2, durationLabel;
	RSButton info, live;
	RSToggleButton fadeout;
	Color bgColor;
	Color origBgColor;
	Dimension size;

	boolean isLiveItem = false;
	long time;
	long liveDuration;
	long fadeoutDuration;
	boolean playBackgroundMusic = true;

	boolean previewPressed = false;
	boolean infoPressed = false;
	boolean stopPressed = false;

	public PlayItemWidget(RadioProgramMetadata metadata, Dimension dim) {
	    super();
	    this.metadata = metadata;
	    setLayout(new GridBagLayout());
	    setBorder(new GradientBorder(stationConfig.getColor(StationConfiguration.LIST_ITEM_BORDER_COLOR)));
	    size = dim;
	    fixSize(this, dim);
	    
	    if (metadata.getLength() > 0)
		bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_BGCOLOR);
	    else if (!metadata.getItemID().startsWith(GOLIVE_ITEMID))
		bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_ERROR_BGCOLOR);
		
	    origBgColor = bgColor;

	    if (metadata.getItemID().startsWith(GOLIVE_ITEMID)) {
		isLiveItem = true;
		playBackgroundMusic = false;
		liveDuration = (long)10 * 1000; // 10 seconds
		bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_GOLIVECOLOR);
		setBackground(bgColor);
	    } else {
		isLiveItem = false;
		liveDuration = metadata.getLength();
		setBackground(bgColor);
	    }

	    if (metadata.getType().equals(ArchiverService.BCAST_MIC))
		fadeoutDuration = 0;
	    else
		fadeoutDuration = FADEOUT_DURATION;

	    init();
	}
	
	public void setPreviewPressed(boolean pressed){
	    previewPressed = pressed;
	    preview.setPressed(pressed);
	}

	public void setInfoPressed(boolean pressed){
	    infoPressed = pressed;
	    info.setPressed(pressed);
	}

	public void setStopPressed(boolean pressed){
	    stopPressed = pressed;
	    stop.setPressed(pressed);
	}

	/*
	public void setFadeoutPressed(boolean pressed) {
	    fadeout.setSelected(pressed);
	}
	*/

	protected void init() {
	    timeLabel = new RSLabel("00:00:00");
	    play = new RSButton(""); 
	    play.setToolTipText("Play");
	    play.setActionCommand(LISTPLAY + "_" + metadata.getItemID()); 

	    play.addActionListener(playlistController);
	    preview = new RSButton(""); 
	    preview.setActionCommand(LISTPREVIEW + "_" + metadata.getItemID()); 
	    preview.setToolTipText("Preview");
	    preview.addActionListener(playlistController);
	    stop = new RSButton("");  
	    stop.setActionCommand(LISTSTOP + "_" + metadata.getItemID()); 
	    stop.setToolTipText("Stop");
	    stop.addActionListener(playlistController);
	    titleLabel1 = new RSLabel(metadata.getTitle()); 
	    titleLabel1.setVerticalAlignment(SwingConstants.CENTER);
	    titleLabel1.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_TITLE_COLOR));
	    titleLabel1.setToolTipText(metadata.getTitle());
	    titleLabel2 = new RSLabel(""); 
	    titleLabel2.setVerticalAlignment(SwingConstants.CENTER);
	    titleLabel2.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_DETAILS_COLOR));
	    durationLabel = new RSLabel(durationStringFromMillis(liveDuration));
	    durationLabel.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_DETAILS_COLOR));
	    info = new RSButton(""); 
	    info.setActionCommand(LISTINFO + "_" + metadata.getItemID()); 
	    info.setToolTipText("Info");
	    info.addActionListener(playlistController);
	    live = new RSButton(""); 
	    live.setActionCommand(LISTLIVE + "_" + metadata.getItemID());
	    live.setToolTipText("Go live!");
	    live.addActionListener(playlistController);
	    /*
	    //For fadeout button removal
	    fadeout = new RSToggleButton(""); 
	    fadeout.setActionCommand(LISTFADE + "_" + metadata.getItemID());
	    fadeout.setToolTipText("Fade out");
	    fadeout.addActionListener(playlistController);
	    
	    if (fadeoutDuration > 0)
		setFadeoutPressed(true);
	    else
		setFadeoutPressed(false);
	    */

	    play.setPreferredSize(new Dimension((int)(panel.getWidth() * playItemHorizFrac / 100.0), 
						(int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    durationLabel.setPreferredSize(new Dimension((int)(panel.getWidth() * durationItemHorizFrac/ 100.0), 
							 (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    preview.setPreferredSize(new Dimension((int)(panel.getWidth() * previewItemHorizFrac / 100.0), 
						   (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    stop.setPreferredSize(new Dimension((int)(panel.getWidth() * stopItemHorizFrac / 100.0), 
						(int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    info.setPreferredSize(new Dimension((int)(panel.getWidth() * (infoItemHorizFrac) / 100.0), 
						(int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    live.setPreferredSize(new Dimension((int)(panel.getWidth() * (liveItemHorizFrac) / 100.0), 
						(int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    /*
	    //For fadeout button removal
	    fadeout.setPreferredSize(new Dimension((int)(panel.getWidth() * (fadeItemHorizFrac) / 100.0), 
						   (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    */
	    titleLabel1.setPreferredSize(new Dimension((int)(panel.getWidth() * (titleItemHorizFrac + 4 * stopItemHorizFrac) / 100.0), 
						       (int)(panel.getHeight() * listElementVertFrac / 100.0) / 2));
	    //Added stopItemHorizFrac below to remove fadeout button
	    titleLabel2.setPreferredSize(new Dimension((int)(panel.getWidth() * (titleItemHorizFrac + stopItemHorizFrac) / 100.0), 
						       (int)(panel.getHeight() * listElementVertFrac / 100.0) / 2));


	    play.setMinimumSize(new Dimension((int)(panel.getWidth() * playItemHorizFrac / 100.0), 
					      (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    durationLabel.setMinimumSize(new Dimension((int)(panel.getWidth() * durationItemHorizFrac / 100.0), 
						       (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    preview.setMinimumSize(new Dimension((int)(panel.getWidth() * previewItemHorizFrac / 100.0), 
						 (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    stop.setMinimumSize(new Dimension((int)(panel.getWidth() * stopItemHorizFrac / 100.0), 
					      (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    info.setMinimumSize(new Dimension((int)(panel.getWidth() * (infoItemHorizFrac) / 100.0), 
						(int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    /*
	    //For fadeout button removal
	    fadeout.setMinimumSize(new Dimension((int)(panel.getWidth() * (fadeItemHorizFrac) / 100.0), 
						 (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    */
	    live.setMinimumSize(new Dimension((int)(panel.getWidth() * (liveItemHorizFrac) / 100.0), 
						(int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    titleLabel1.setMinimumSize(new Dimension((int)(panel.getWidth() * (titleItemHorizFrac + 4 * stopItemHorizFrac) / 100.0), 
						     (int)(panel.getHeight() * listElementVertFrac / 100.0) / 2));
	    //Added stopItemHorizFrac below to remove fadeout button
	    titleLabel2.setMinimumSize(new Dimension((int)(panel.getWidth() * (titleItemHorizFrac + stopItemHorizFrac) / 100.0), 
						     (int)(panel.getHeight() * listElementVertFrac / 100.0) / 2));

	    play.setIcon(getIcon(StationConfiguration.PLAY_ICON,
					       (int)(panel.getWidth() * playItemHorizFrac / 100.0), 
					       (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    preview.setIcon(getIcon(StationConfiguration.PREVIEW_ICON,
						  (int)(panel.getWidth() * previewItemHorizFrac / 100.0), 
						  (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    stop.setIcon(getIcon(StationConfiguration.PREVIEW_STOP_ICON,
					       (int)(panel.getWidth() * stopItemHorizFrac / 100.0), 
					       (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    info.setIcon(getIcon(StationConfiguration.INFO_ICON,
					       (int)(panel.getWidth() * (infoItemHorizFrac) / 100.0), 
					       (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    live.setIcon(getIcon(StationConfiguration.GOLIVE_ICON,
					       (int)(panel.getWidth() * (liveItemHorizFrac) / 100.0), 
					       (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    /*
	    //For fadeout button removal
	    fadeout.setIcon(getIcon(StationConfiguration.FADEOUT_ICON,
						  (int)(panel.getWidth() * (fadeItemHorizFrac) / 100.0), 
						  (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    */

	    setBackground(bgColor);
	    Color buttonColor = stationConfig.getColor(StationConfiguration.PLAYLIST_LIVEPANE_COLOR);
	    play.setBackground(buttonColor);
	    durationLabel.setBackground(bgColor);	    
	    preview.setBackground(buttonColor);	    
	    stop.setBackground(buttonColor);
	    info.setBackground(buttonColor);
	    live.setBackground(buttonColor);
	    /*
	    //For fadeout button removal
	    fadeout.setBackground(buttonColor);
	    */
	    titleLabel1.setBackground(bgColor);
	    titleLabel2.setBackground(bgColor);

	    add(timeLabel, getGbc(0, 0, gfillboth(), gridwidth(2)));
	    add(play, getGbc(0, 1, gfillboth()));
	    add(durationLabel, getGbc(1, 1, gfillboth()));
	    add(titleLabel1, getGbc(2, 0, gfillboth(), gridwidth(6)));
	    add(titleLabel2, getGbc(2, 1, gfillboth()));
	    add(preview, getGbc(3, 1, gfillboth()));
	    add(stop, getGbc(4, 1, gfillboth()));
	    add(info, getGbc(5, 1, gfillboth()));
	    add(live, getGbc(6, 1, gfillboth()));
	    /*
	    //For fadeout button removal
	    add(fadeout, getGbc(7, 1, gfillboth()));
	    */
	    validate();
	}

	public boolean isLiveItem() {
	    return isLiveItem;
	}

	public Color getCurrBackground() {
	    return bgColor;
	}

	public void isLiveItem(boolean isLiveItem) {
	    this.isLiveItem = isLiveItem;
	    if (isLiveItem)
		setBackground(bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_GOLIVECOLOR));
	    else
		setBackground(bgColor = origBgColor);
	}

	public boolean playBackgroundMusic() {
	    return playBackgroundMusic;
	}

	public void playBackgroundMusic(boolean playBackgroundMusic) {
	    this.playBackgroundMusic = playBackgroundMusic;
	    // XXX add some visual indication
	}

	public long getGoliveDuration() {
	    return liveDuration;
	}

	public long getDuration(){
	    return metadata.getLength();
	}

	public void setGoliveDuration(long liveDuration) {
	    this.liveDuration = liveDuration;
	    durationLabel.setText(durationStringFromMillis(liveDuration));
	}

	public long getFadeoutDuration() {
	    /*
	    //For fadeout button removal
	    if (!fadeout.isSelected() || fadeoutDuration < 0)
	    */
	    if(fadeoutDuration < 0)
		return 0;
	    else
		return fadeoutDuration;
	}

	public long getFadeoutStartTime(){
	    long duration = metadata.getLength();
	    long fadeoutDuration = getFadeoutDuration();
	    long startTime;

	    //XXX do we need to fadeout for small files?
	    if (fadeoutDuration == 0)
		return -1; // no fading
	    else 
		return Math.max(0, duration - fadeoutDuration);
	}

	public void setFadeoutDuration(long fadeoutDuration) {
	    this.fadeoutDuration = fadeoutDuration;
	    /*
	    //For fadeout button removal	      
	    if (fadeoutDuration > 0)
		setFadeoutPressed(true);
	    else
		setFadeoutPressed(false);
	    */
	}

	public RadioProgramMetadata getMetadata() {
	    return metadata;
	}

	public void setMetadata(RadioProgramMetadata metadata) {
	    this.metadata = metadata;
	    titleLabel1.setText(metadata.getTitle());
	    titleLabel1.setToolTipText(metadata.getTitle());
	    //	    titleLabel2.setText(SimulationWidget.additionalText(metadata));
	}

	public RSButton getPlayButton() {
	    return play;
	}

	public RSButton getPreviewButton() {
	    return preview;
	}

	public RSButton getStopButton() {
	    return stop;
	}

	public void setTime(long time){
	    this.time = time;
	    timeLabel.setText(getDateString(time));
	}
	
	public long getTime(){
	    return time;
	}

	public RSButton getInfoButton() {
	    return info;
	}

	public RSToggleButton getFadeoutButton() {
	    return fadeout;
	}

	public void setSelected(){
	    setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_SELECTCOLOR));
	}

	public void setUnSelected(){
	    setBackground(bgColor);
	}

	protected JComponent getButtonAt(Point p){
	    setLocation(0, 0);
	    setSize(size);

	    int y = (int)p.getY()%(int)getPreferredSize().getHeight();
	    int x = (int)p.getX();
		    
	    Component button = getComponentAt(x, y);
	    if (button instanceof RSButton)
		return ((RSButton)button);
	    else if (button instanceof RSToggleButton)
		return ((RSToggleButton)button);
	    else
		return null;
	}

	public void mouseClicked(MouseEvent e, int index){
	    JComponent button = getButtonAt(e.getPoint());
	    boolean pressed;
	    if (button != null && button instanceof RSButton) {
		RSButton rsButton = (RSButton) button;
		//if (!(rsButton == preview && previewPressed))
		//rsButton.doClick();
		//if (rsButton == preview)
		//rsButton.setPressed(previewPressed);
		ActionEvent ae = new ActionEvent(rsButton, 0, rsButton.getActionCommand());
		playlistController.actionPerformed(ae);
	    } else if (button != null && button instanceof RSToggleButton) {
		RSToggleButton rstButton = (RSToggleButton)button;
		rstButton.doClick();
	    }
	    
	}

	public void mousePressed(MouseEvent e){
	    JComponent button = getButtonAt(e.getPoint());
	    if (button != null && button instanceof RSButton) {
		//System.err.println("----------- set pressed true: "+button);
		((RSButton)button).setPressed(true);
	    } else if (button != null && button instanceof RSToggleButton) {
		//		((RSToggleButton)button).setPressed(true);
	    }
	}

	public void mouseReleased(MouseEvent e){
	    JComponent button = getButtonAt(e.getPoint());
	    if (button != null && button instanceof RSButton) {
		RSButton rsButton = (RSButton) button;
		if ( (rsButton == preview) && !previewPressed){
		    rsButton.setPressed(false);
		} else if (rsButton != preview) {
		    //System.err.println("----------- set pressed false: "+button);
		    rsButton.setPressed(false);
		}

	    } else if (button != null && button instanceof RSToggleButton) {
		//		((RSToggleButton)button).setPressed(false);
	    }
	}
    }
    
}