package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.providers.CacheObjectListener;
import org.gramvaani.radio.app.providers.CacheListenerCallback;
import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.rscontroller.services.UIService;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.utilities.StringUtilities;
import org.gramvaani.utilities.IconUtilities;
import org.gramvaani.radio.medialib.Category;
import org.gramvaani.radio.medialib.Entity;
import org.gramvaani.radio.telephonylib.*;
import org.gramvaani.radio.medialib.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.DataFlavor;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.TitledBorder;

import java.util.*;
import java.text.DateFormat;

import java.io.*;


public class TelephonyWidget extends RSWidgetBase implements TagTreeListener, CacheListenerCallback {

    RSApp rsApp;
    StationConfiguration stationConfig;
    Color bgColor, titleColor;
    
    TelephonyController telephonyController;

    static final int pickupButtonHorizFrac = 87;
    
    static final String SELECT_ADD_CALLER = "Select/Add Caller";
    static final String COMBO_EDITED 	= "comboBoxEdited";
    static final String COMBO_CHANGED 	= "comboBoxChanged";

    int scrollBarWidth;
    int incomingColumnWidth, activeColumnWidth;
    int slotWidth, slotHeight, slotTopInset;
    
    Hashtable<TelephonyController.CallInfo, IncomingCallWidget> incomingCallEntries;
    Hashtable<TelephonyController.CallInfo, ActiveCallWidget> activeCallEntries;
    Hashtable<TelephonyController.CallInfo, Integer> slotIndexes = new Hashtable<TelephonyController.CallInfo, Integer>();
    Hashtable<Integer, JComponent> incomingCallComponents = new Hashtable<Integer, JComponent>();
    Hashtable<Integer, JComponent> activeCallComponents = new Hashtable<Integer, JComponent>();

    PrevCallsTableModel prevCallsModel;

    JTextField idField, locationField, descField, tagsField;
    JComboBox nameField;
    RSButton newCallButton, dialButton, hangupButton;
    RSButton saveCallInfoButton, cancelEditsButton;
    RSToggleButton goOnlineButton, confButton, onairButton;
    
    static final int TAG_LIST_SIZE = 5;
    static final int NUM_DEFAULT_SLOTS = 5;

    DefaultListModel tagList = new DefaultListModel();
    JList tagListComp, entityListComp;

    Dimension metadataPanelSize, categoriesPanelSize, categoryComponentSize;
    Category[] categories = new Category[] {};
    HashSet<Category> tempCategories = new HashSet<Category>();
    RSPanel categoriesPanel;
    
    TelephonyController.CallInfo currentCallInfo, callInfoToSave;

    Dimension baseSize;
    JPanel basePanel;
    JPanel selectedCallPanel;
    JPanel slotPanel;
    JComboBox dialOutPortBox;
    JTextField dialIdField, dialNameField;
    RSLabel numberTypeLabel;
    Color defaultTextFieldColor, errorTextFieldColor;

    JTable prevCallsTable;
    boolean prevCallsEnabled = false;
    
    Color incomingColor, activeColor, selectedColor, metadataColor, dialColor;
    RSLabel allowDisplay, confDisplay, onairDisplay, incomingDisplay, activeDisplay;
    
    JPopupMenu entityMenu;
    DefaultListModel entitySearchList = new DefaultListModel();

    ErrorInference errorInference;
    CacheProvider cacheProvider;
    boolean cacheProviderActive = false;

    javax.swing.Timer bmpTimer;

    static final int BMP_RING_INTERVAL_MS = 500;
    Icon telephoneIcon, ringingTelephoneIcon;

    public TelephonyWidget(RSApp rsApp) {
	super("Telephony");
	this.rsApp = rsApp;

	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.TELEPHONY_ICON;
	bmpToolTip = "Telephony";
	
	stationConfig = rsApp.getStationConfiguration();
	cpJFrame = rsApp.getControlPanel().cpJFrame();

	incomingCallEntries = new Hashtable<TelephonyController.CallInfo, IncomingCallWidget>();
	activeCallEntries   = new Hashtable<TelephonyController.CallInfo, ActiveCallWidget>();

	errorInference = ErrorInference.getErrorInference();
	cacheProvider  = (CacheProvider) rsApp.getProvider(RSApp.CACHE_PROVIDER);

    }

    protected boolean onLaunch(){
	bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	titleColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);

	telephonyController = new TelephonyController(rsApp, this, stationConfig);

	wpComponent.setBackground(bgColor);
	wpComponent.setLayout(new BoxLayout(wpComponent, BoxLayout.PAGE_AXIS));
	
	RSTitleBar titleBar = new RSTitleBar(name, wpSize.width);
	titleBar.setBackground(titleColor);
	wpComponent.add(titleBar);

	basePanel = new JPanel();
	baseSize = new Dimension(wpSize.width, wpSize.height - RSTitleBar.HEIGHT);
	basePanel.setPreferredSize(baseSize);
	basePanel.setBackground(bgColor);
	wpComponent.add(basePanel);

	buildWorkspace();
	buildASP();
	telephonyController.init();
	updateGUIButtons();
	updateBMP();
	updateASP();

	cacheProvider.registerWithProvider(this);
	if (cacheProvider == null || !cacheProvider.isActive())
	    cacheProviderActive = false;
	else
	    cacheProviderActive = true;

	return true;

    }

    public void activateCache(boolean activate, String[] error){
	cacheProviderActive = activate;
	if (!activate)
	    errorInference.displayServiceError("Unable to activate cache service", error);
    }

    public void cacheObjectsDeleted(String objectType, String[] objectID){}
    public void cacheObjectsUpdated(String objectType, String[] objectID){}
    public void cacheObjectsInserted(String objectType, String[] objectID){}


    void buildASP() {
	aspComponent.setLayout(new GridBagLayout());

	RSLabel allowLabel = new RSLabel("Allow Incoming: ");
	allowLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	aspComponent.add(allowLabel, getGbc(0, 0, gfillboth()));
	allowDisplay = new RSLabel("");
	aspComponent.add(allowDisplay, getGbc(1, 0, gfillboth()));

	RSLabel onairLabel = new RSLabel("On Air: ");
	onairLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	aspComponent.add(onairLabel, getGbc(0, 1, gfillboth()));
	onairDisplay = new RSLabel("");
	aspComponent.add(onairDisplay, getGbc(1, 1, gfillboth()));

	RSLabel confLabel = new RSLabel("Conference: ");
	confLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	aspComponent.add(confLabel, getGbc(0, 2, gfillboth()));
	confDisplay = new RSLabel("");
	aspComponent.add(confDisplay, getGbc(1, 2, gfillboth()));

	RSLabel incomingLabel = new RSLabel("Incoming Calls: ");
	incomingLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	aspComponent.add(incomingLabel, getGbc(0, 3, gfillboth()));
	incomingDisplay = new RSLabel("");
	aspComponent.add(incomingDisplay, getGbc(1, 3, gfillboth()));

	RSLabel activeLabel = new RSLabel("Active Calls: ");
	activeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	aspComponent.add(activeLabel, getGbc(0, 4, gfillboth()));
	activeDisplay = new RSLabel("");
	aspComponent.add(activeDisplay, getGbc(1, 4, gfillboth()));

	aspComponent.repaint();
    }

    void updateASP(){
	if (goOnlineButton.isSelected())
	    allowDisplay.setText("Yes");
	else
	    allowDisplay.setText("No");

	if (onairButton.isSelected())
	    onairDisplay.setText("Yes");
	else
	    onairDisplay.setText("No");

	if (confButton.isSelected())
	    confDisplay.setText("On");
	else
	    confDisplay.setText("Off");

	int incoming = getNumIncomingCalls();
	incomingDisplay.setText(Integer.toString(incoming));
	activeDisplay.setText(Integer.toString(activeCallEntries.size()));

	aspComponent.repaint();
    }

    void updateBMP(){
	if (getNumIncomingCalls() == 0){
	    bmpTimer.stop();
	    setBMPIcon(false);
	} else {
	    bmpTimer.start();
	}
    }

    int getNumIncomingCalls(){
	int incoming = 0;
	for (TelephonyController.CallInfo info: incomingCallEntries.keySet())
	    if (!activeCallEntries.keySet().contains(info))
		incoming++;
	return incoming;
    }


    void setBMPIcon(final boolean isRinging){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    if (isRinging)
			bmpButton.setIcon(ringingTelephoneIcon);
		    else
			bmpButton.setIcon(telephoneIcon);
		}
	    });
    }

    void buildWorkspace(){

	int modeButtonVertFrac = 7;
	int columnLabelVertFrac = 4;
	int callRowVertFrac = 40;
	int dialLabelVertFrac = 4;

	int incomingColumnHorizFrac = 32;
	int activeColumnHorizFrac = 32;
	
	int modeButtonHeight = modeButtonVertFrac * baseSize.height/100;
	int columnLabelHeight = columnLabelVertFrac * baseSize.height/100;
	int callRowHeight = callRowVertFrac * baseSize.height/100;
	int dialRowHeight = baseSize.height - modeButtonHeight - columnLabelHeight - callRowHeight;
	int dialLabelHeight = dialLabelVertFrac * baseSize.height/100;

	incomingColumnWidth = incomingColumnHorizFrac * baseSize.width/100;
	activeColumnWidth = activeColumnHorizFrac * baseSize.width/100;
	
	int selectedColumnWidth = baseSize.width - incomingColumnWidth - activeColumnWidth;
	
	int slotVertFrac = 20;
	slotHeight = slotVertFrac * callRowHeight / 100;
	slotTopInset = slotHeight/5;
	
	Dimension goOnlineButtonSize = new Dimension(incomingColumnWidth, modeButtonHeight);
	Dimension confButtonSize = new Dimension(activeColumnWidth, modeButtonHeight);
	Dimension onairButtonSize = new Dimension(selectedColumnWidth, modeButtonHeight);
	
	basePanel.setLayout(new GridBagLayout());

	goOnlineButton = new RSToggleButton("Allow Incoming");
	goOnlineButton.addActionListener(new ActionListener(){
		public synchronized void actionPerformed(ActionEvent e){
		    goOnlineButton.setEnabled(false);
		    if (goOnlineButton.isSelected()){
			telephonyController.goOnline();
			goOnlineButton.setToolTipText("Calls will be picked up by GRINS");
		    } else {
			telephonyController.goOffline();
			goOnlineButton.setToolTipText("Calls will not be picked up by GRINS");
		    }
		    goOnlineButton.setEnabled(true);

		    updateASP();
		}

	    });
	goOnlineButton.setBackground(bgColor);
	goOnlineButton.setPreferredSize(goOnlineButtonSize);
	goOnlineButton.setToolTipText("Incoming Calls not picked up by GRINS");
	basePanel.add(goOnlineButton, getGbc(0, 0));
	
    	confButton = new RSToggleButton("Conference");
	confButton.addActionListener(new ActionListener(){
		public synchronized void actionPerformed(ActionEvent e){
		    if (confButton.isSelected() && ! isActiveCallPresent()){
			confButton.setSelected(false);
			return;
		    }
		    confButton.setEnabled(false);
		    telephonyController.enableConferenceMode(confButton.isSelected());
		    if (!confButton.isSelected())
			setDeselectedActiveCalls();
		    confButton.setEnabled(true);

		    if (confButton.isSelected())
			confButton.setToolTipText("Calls are in Conference");
		    else
			confButton.setToolTipText("Calls are not in Conference");

		    updateASP();
		}
	    });

	confButton.setBackground(bgColor);
	confButton.setPreferredSize(confButtonSize);
	confButton.setToolTipText("Conference Disabled");

	basePanel.add(confButton, getGbc(1, 0));

	onairButton = new RSToggleButton("Go Live");
	onairButton.addActionListener(new ActionListener(){
		public synchronized void actionPerformed(ActionEvent e){
		    if (!isActiveCallPresent() && onairButton.isSelected()){
			onairButton.setSelected(false);
			return;
		    }
		    onairButton.setEnabled(false);
		    if (onairButton.isSelected()) {
			telephonyController.goOnAir(confButton.isSelected());
			newCallButton.setEnabled(false);
		    } else {
			telephonyController.goOffAir(confButton.isSelected());
			newCallButton.setEnabled(true);
		    }
		    onairButton.setEnabled(true);
		    if (onairButton.isSelected())
			onairButton.setToolTipText("Calls on-air");
		    else
			onairButton.setToolTipText("Calls previewed");
		    updateASP();
		}
	    });
	onairButton.setPreferredSize(onairButtonSize);
	onairButton.setToolTipText("Calls previewed");
	onairButton.setBackground(bgColor);
	
	basePanel.add(onairButton, getGbc(2, 0));
	
	incomingColor = stationConfig.getColor(StationConfiguration.TELEPHONY_INCOMING_COLOR);
	RSLabel incomingLabel = getColumnLabel("Incoming Calls", incomingColor, incomingColumnWidth, columnLabelHeight);
	basePanel.add(incomingLabel, getGbc(0, 1));

	activeColor = stationConfig.getColor(StationConfiguration.TELEPHONY_ACTIVE_COLOR);
	RSLabel activeLabel = getColumnLabel("Active Calls", activeColor, activeColumnWidth, columnLabelHeight);
	basePanel.add(activeLabel, getGbc(1, 1));

	selectedColor = stationConfig.getColor(StationConfiguration.TELEPHONY_SELECTED_COLOR);
	RSLabel selectedLabel = getColumnLabel("Selected Call", selectedColor, selectedColumnWidth, columnLabelHeight);
	basePanel.add(selectedLabel, getGbc(2, 1));
	
	selectedCallPanel = getSelectedCallPanel(selectedColumnWidth, callRowHeight);
	basePanel.add(selectedCallPanel, getGbc(2, 2));
	
	JComponent callListPane = getCallListPane(incomingColumnWidth + activeColumnWidth, callRowHeight, bgColor);
	basePanel.add(callListPane, getGbc(0, 2, gridwidth(2)));
	
	dialColor = stationConfig.getColor(StationConfiguration.TELEPHONY_DIAL_COLOR);
	JComponent dialPane = getDialPane(incomingColumnWidth, dialRowHeight, bgColor, dialLabelHeight);
	basePanel.add(dialPane, getGbc(0, 3));

	JComponent prevCallsPane = getPrevCallsPane(activeColumnWidth + selectedColumnWidth, dialRowHeight, bgColor, dialLabelHeight);
	basePanel.add(prevCallsPane, getGbc(1, 3, gridwidth(2)));

	updateGUIButtons();

	telephoneIcon = bmpButton.getIcon();
	final int ringingIconInset = 0;
	ringingTelephoneIcon = IconUtilities.getIcon(StationConfiguration.RAW_TELEPHONY_ICON, telephoneIcon.getIconHeight() - ringingIconInset, telephoneIcon.getIconWidth() - ringingIconInset);

	bmpTimer = new javax.swing.Timer(BMP_RING_INTERVAL_MS, new ActionListener(){
		boolean isRinging = false;
		public void actionPerformed(ActionEvent e){
		    if (isMaximized()){
			setBMPIcon(false);
			return;
		    }
		    isRinging = !isRinging;
		    setBMPIcon(isRinging);
		}
	    });
    }

    JPanel getSelectedCallPanel(int width, int height){
	RSPanel panel = new RSPanel();
	panel.setPreferredSize(new Dimension(width, height));
	Color selectedCallBgColor = bgColor;
	panel.setBackground(selectedCallBgColor);
	panel.setLayout(new GridBagLayout());

	int callerVertFrac = 40;
	Dimension callerPaneSize = new Dimension(width, height* callerVertFrac/100);
	RSPanel callerPanel = new RSPanel();
	
	final Color borderColor = GUIUtilities.toSaturationBrightness(selectedColor, 0.1f, 1.0f);
	TitledBorder border = new TitledBorder(BorderFactory.createLineBorder(borderColor), "Caller Information");
	border.setTitleColor(GUIUtilities.toSaturationBrightness(selectedColor, 0.1f, 0.85f));
	callerPanel.setBorder(border);
	callerPanel.setPreferredSize(callerPaneSize);
	callerPanel.setBackground(selectedCallBgColor);
	callerPanel.setLayout(new GridBagLayout());
	panel.add(callerPanel, getGbc(0, 0));
	
	int labelHorizFrac = 25;
	int labelVertFrac = 10;
	
	Color labelForeground = selectedColor.darker();

	Dimension labelSize = new Dimension(width * labelHorizFrac/100, height * labelVertFrac/100);

	RSLabel idLabel = getRightLabel("#: ", labelSize, labelForeground);
	callerPanel.add(idLabel, getGbc(0, 0));

	RSPanel numberPanel = new RSPanel();
	numberPanel.setLayout(new BorderLayout());

	int numberTypeHorizFrac = 15;
	numberTypeLabel = new RSLabel("");
	numberTypeLabel.setBackground(bgColor);
	numberTypeLabel.setOpaque(true);
	numberTypeLabel.setPreferredSize(new Dimension(width * numberTypeHorizFrac/100, labelSize.height));
	numberTypeLabel.setHorizontalAlignment(SwingConstants.CENTER);

	idField = new JTextField();

	numberPanel.add(numberTypeLabel, BorderLayout.WEST);
	numberPanel.add(idField, BorderLayout.CENTER);

	callerPanel.add(numberPanel, getGbc(1, 0, weightx(1.0), gfillboth()));

	RSLabel nameLabel = getRightLabel("Name: ", labelSize, labelForeground);
	callerPanel.add(nameLabel, getGbc(0, 1));

	nameField = new JComboBox();
	nameField.setBackground(bgColor);
	callerPanel.add(nameField, getGbc(1, 1, weightx(1.0), gfillboth()));

	nameField.addActionListener(new ActionListener() {
		public synchronized void actionPerformed(ActionEvent e){
		    if (e.getActionCommand().equals(COMBO_CHANGED)) {
			if (callInfoToSave != null) {
			    if (nameField.getSelectedIndex() <= 0) {
				nameField.setEditable(true);
			    } else {
				callInfoToSave.setCaller(nameField.getSelectedIndex() - 1);
				updateLocationField(callInfoToSave);
				nameField.setEditable(false);
			    }
			}
		    }
		}
	    });
	

	RSLabel locationLabel = getRightLabel("Location: ", labelSize, labelForeground);
	callerPanel.add(locationLabel, getGbc(0, 2));
	
	locationField = new JTextField();
	callerPanel.add(locationField, getGbc(1, 2, weightx(1.0), gfillboth()));

	RSPanel callPanel = new RSPanel();
	Dimension callSize = new Dimension(width, height - callerPaneSize.height);
	callPanel.setBackground(bgColor);
	callPanel.setPreferredSize(callSize);
	
	panel.add(callPanel, getGbc(0, 1, weighty(1.0)));
	
	border = new TitledBorder(BorderFactory.createLineBorder(borderColor), "Call Information");
	border.setTitleColor(GUIUtilities.toSaturationBrightness(selectedColor, 0.1f, 0.85f));
	callPanel.setBorder(border);
	callPanel.setLayout(new GridBagLayout());
	
	RSPanel blankPanel = new RSPanel();
	blankPanel.setOpaque(false);
	callPanel.add(blankPanel, getGbc(0, 0, gfillboth(), weighty(0.5)));

	RSLabel descLabel = getRightLabel("Details: ", labelSize, labelForeground);
	callPanel.add(descLabel, getGbc(0, 1));
	
	descField = new JTextField();
	callPanel.add(descField, getGbc(1, 1, weightx(1.0), gfillboth()));


	RSLabel tagsLabel = getRightLabel("Tags: ", labelSize, labelForeground);
	callPanel.add(tagsLabel, getGbc(0, 2));

	tagsField = new JTextField();
	tagsField.setEditable(false);
	tagsField.setOpaque(true);
	tagsField.setBackground(Color.white);
	callPanel.add(tagsField, getGbc(1, 2, gfillboth(), weightx(1.0)));
	tagsField.addMouseListener(new MouseAdapter(){
		public synchronized void mouseClicked(MouseEvent e){
		    if (callInfoToSave == null)
			return;
		    HashSet<Integer> selected = new HashSet<Integer>();
		    for (Category category: callInfoToSave.getCategories())
			selected.add(category.getNodeID());
		    TagTree tagTree = new TagTree(TelephonyWidget.this, telephonyController.getMetadataProvider(), selected, stationConfig);
		    JFrame tagsPopup = GUIUtilities.getTagsPopup(tagTree, tagsField, tagsField.getSize().height, bgColor);
		    tagsPopup.setVisible(true);
		}

	    });

	prevCallsModel = new PrevCallsTableModel();
	prevCallsModel.setRowCount(MAX_PREV_CALLS);

	RSPanel buttonsPanel = new RSPanel();
	buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
	buttonsPanel.setBackground(bgColor);
	callPanel.add(buttonsPanel, getGbc(1, 3, weighty(1.0), gfillh()));

	Dimension buttonSize = new Dimension((width - labelSize.width)/3, labelSize.height);

	Icon saveIcon = IconUtilities.getIcon(StationConfiguration.SAVE_ICON, buttonSize.height, buttonSize.height);
	RSButton saveCallInfoButton = new RSButton(saveIcon);
	saveCallInfoButton.setBackground(bgColor);
	saveCallInfoButton.setPreferredSize(buttonSize);
	saveCallInfoButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    saveCallInfo(callInfoToSave);
		}
	    });
	saveCallInfoButton.setToolTipText("Save call information");

	buttonsPanel.add(saveCallInfoButton);

	Icon deleteIcon = IconUtilities.getIcon(StationConfiguration.DELETE_ICON, buttonSize.height, buttonSize.height);
	RSButton clearCallInfoButton = new RSButton(deleteIcon);
	clearCallInfoButton.setBackground(bgColor);
	clearCallInfoButton.setPreferredSize(buttonSize);
	clearCallInfoButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    clearCallInfo();
		}
	    });
	clearCallInfoButton.setToolTipText("Clear call information");
	buttonsPanel.add(clearCallInfoButton);

	return panel;
    }

    public void tagSelectionChanged(Category tag, boolean selection){
	if (callInfoToSave == null)
	    return;

	if (selection)
	    callInfoToSave.addCategory(tag);
	else
	    callInfoToSave.removeCategory(tag);
	tagsField.setText(callInfoToSave.getTagsString());
    }

    public void tagEdited(Category tag){
	if (callInfoToSave == null)
	    return;
	tagsField.setText(callInfoToSave.getTagsString());
    }

    RSLabel getRightLabel(String labelString, Dimension size, Color foreground){
	RSLabel label = new RSLabel(labelString);
	label.setHorizontalAlignment(SwingConstants.RIGHT);
	label.setPreferredSize(size);
	label.setMinimumSize(size);
	label.setForeground(foreground);
	return label;
    }

    JComponent getCallListPane(int width, int height, Color bgColor){
	slotPanel = new JPanel();
	slotPanel.setBackground(bgColor);
	JScrollPane scrollPane = new JScrollPane(slotPanel);
	scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	scrollPane.setBorder(null);
	int scrollBarWidth = scrollPane.getVerticalScrollBar().getMaximumSize().width;

	Dimension slotPaneSize = new Dimension(width - scrollBarWidth, height);
	slotPanel.setPreferredSize(slotPaneSize);
	slotPanel.setLayout(new GridBagLayout());
	slotWidth = slotPaneSize.width/2;

	for (int i = 0; i < NUM_DEFAULT_SLOTS; i++){
	    addBlankSlot(0, i);
	    addBlankSlot(1, i);
	}

	slotPanel.revalidate();

	return scrollPane;
    }
    
    void addBlankSlot(final int x, final int y){
	final JPanel blank = new JPanel();
	blank.setBackground(bgColor);
	blank.setPreferredSize(new Dimension(slotWidth, slotHeight));
	
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    JComponent previous;
		    if (x == 0){
			previous = incomingCallComponents.get(y);
			incomingCallComponents.put(y, blank);
		    } else if (x == 1) {
			previous = activeCallComponents.get(y);
			activeCallComponents.put(y, blank);
		    } else {
			previous = null;
			logger.error("AddBlankSlot: x: " + x);
		    }

		    if (previous != null)
			slotPanel.remove(previous);
		    slotPanel.add(blank, getGbc(x, y));
		    slotPanel.revalidate();
		}
	    });
    }

    void selectDialOutPort(String port){
	int index = -1;
	synchronized(dialOutPortBox){
	    if (port.equals(RSCallerID.SIP)){
		for (int i = 0; i < dialOutPortBox.getItemCount(); i++){
		    String item = (String) dialOutPortBox.getItemAt(i);
		    if (port.equals(item)){
			index = i;
			break;
		    }
		}
	    } else {
		for (int i = 0; i < dialOutPortBox.getItemCount(); i++){
		    String item = (String) dialOutPortBox.getItemAt(i);
		    if (!item.equals(RSCallerID.SIP)){
			index = i;
			break;
		    }
		}
	    }

	    if (index < 0){
		errorInference.displayServiceError("Unable to locate telephony interface:" + port, new String[]{});
	    } else {
		dialOutPortBox.setSelectedIndex(index);
	    }
	}
    }

    public void addDialOutPort(final String port) {
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    synchronized(dialOutPortBox) {
			for (int i = 0; i < dialOutPortBox.getItemCount(); i++) {
			    String item = (String)dialOutPortBox.getItemAt(i);
			    if (port.equals(item)) {
				return;
			    }
			}
			
			dialOutPortBox.addItem(port);
			if (!port.equals(RSCallerID.SIP)){
			    dialOutPortBox.setSelectedItem(port);
			}
		    }
		}
	    });
    }

    public void removeDialOutPort(final String port) {
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    synchronized(dialOutPortBox) {
			for (int i = 0; i < dialOutPortBox.getItemCount(); i++) {
			    String item = (String)dialOutPortBox.getItemAt(i);
			    if (port.equals(item)) {
				dialOutPortBox.removeItem(item);
				return;
			    }
			}
		    }
		}

	    });
    }

    JComponent getDialPane(int width, int height, Color bgColor, int labelHeight){
	JPanel panel = new JPanel();
	panel.setPreferredSize(new Dimension(width, height));
	panel.setBackground(bgColor);
	panel.setLayout(new GridBagLayout());
	
	RSLabel label = getColumnLabel("Dial", dialColor, width, labelHeight);
	panel.add(label, getGbc(0, 0));
	
	JPanel body = new JPanel();
	body.setBackground(bgColor);
	int labelGap = 5;
	Dimension bodySize = new Dimension(width, height - labelHeight - labelGap);
	body.setPreferredSize(bodySize);
	
	panel.add(body, getGbc(0, 1, new Insets(labelGap, 0, 0, 0)));
	body.setLayout(new GridBagLayout());
	
	int labelHorizFrac = 20;
	int labelVertFrac = 10;

	Dimension labelSize = new Dimension(width * labelHorizFrac/100, height * labelVertFrac/100);

	Color labelColor = dialColor.darker().darker();
	
	RSLabel nameLabel = getRightLabel("Name: ", labelSize, labelColor);
	nameLabel.setMinimumSize(labelSize);
	body.add(nameLabel, getGbc(0, 0));

	dialNameField = new JTextField();
	body.add(dialNameField, getGbc(1, 0, gridwidth(2), weightx(1.0),gfillboth(), new Insets(0, 0, 0, labelGap)));

	dialNameField.getDocument().addDocumentListener(new DocumentAdapter(){
		public void documentUpdated(DocumentEvent e){
		    setEntityFound(telephonyController.searchForEntity(dialNameField.getText()));
		}
	    });
	
	dialNameField.addKeyListener(new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (!entityMenu.isVisible())
			return;
		    int index = 0;
		    if (e.getKeyCode() == KeyEvent.VK_DOWN){
			index = entityListComp.getSelectedIndex() + 1;
			if (index >= entitySearchList.getSize())
			    index = 0;
		    }
		    if (e.getKeyCode() == KeyEvent.VK_UP){
			index = entityListComp.getSelectedIndex() - 1;
			if (index < 0)
			    index = entitySearchList.getSize() - 1;
		    }
		    entityListComp.setSelectedIndex(index);
		}
	    });
	dialNameField.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    if (entityMenu.isVisible()){
			Object selectedValue = entityListComp.getSelectedValue();
			if (selectedValue == null)
			    return;
			selectDialOutEntity(unwrapEntity(selectedValue));
		    } else {
			dial();
		    }
		}
	    });
	dialNameField.setToolTipText("Type name to search for contacts");

	RSLabel dialIdLabel = getRightLabel("#: ", labelSize, labelColor);
	dialIdLabel.setMinimumSize(labelSize);
	body.add(dialIdLabel, getGbc(0, 1));
	
	dialOutPortBox = new JComboBox();
	dialOutPortBox.setPreferredSize(labelSize);
	dialOutPortBox.setMinimumSize(labelSize);
	dialOutPortBox.setBackground(bgColor);
	dialOutPortBox.setForeground(labelColor);
	dialOutPortBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    validatePhone();
		}
	    });

	body.add(dialOutPortBox, getGbc(1, 1));

	//Removed to avoid confusion created when SIP is available as a dial out option
	//addDialOutPort(TelephonyLib.SIP);

	dialIdField = new JTextField();
	dialIdField.getDocument().addDocumentListener(new DocumentAdapter(){
		public void documentUpdated(DocumentEvent e){
		    validatePhone();
		}
	    });
	defaultTextFieldColor = dialIdField.getBackground();
	errorTextFieldColor = stationConfig.getColor(StationConfiguration.ERROR_FIELD_COLOR);

	body.add(dialIdField, getGbc(2, 1, /*weightx(1.0),*/ gfillboth(), new Insets(0, 0, 0, labelGap)));

	entityMenu = new JPopupMenu();
	entityMenu.setLayout(new BorderLayout());
	entityMenu.setBorder(BorderFactory.createLineBorder(bgColor.darker()));
	entityMenu.setBorderPainted(true);
	entityListComp = new JList(entitySearchList);
	entityListComp.addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    entityMenu.setVisible(false);
		    selectDialOutEntity(unwrapEntity(entityListComp.getSelectedValue()));
		}
	    });
	entityListComp.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	entityListComp.addMouseMotionListener(new MouseMotionAdapter(){
		public void mouseMoved(MouseEvent e){
		    entityListComp.setSelectedIndex(entityListComp.locationToIndex(e.getPoint()));
		}
	    });
	
	JScrollPane entityScrollPane = new JScrollPane(entityListComp);
	entityMenu.add(entityScrollPane, BorderLayout.CENTER);

	RSPanel dialPad = new RSPanel();
	body.add(dialPad, getGbc(1, 3, gridwidth(2), weighty(1.0), gfillboth()));

	dialPad.setBackground(bgColor);
	dialPad.setLayout(new GridBagLayout());

	int numButtonBorder = 5;
	int buttonWidth = (width - labelSize.width) / 3 - numButtonBorder;
	Dimension numButtonSize = new Dimension(buttonWidth, buttonWidth);
	
	Font keyFont = GUIUtilities.getDefaultFont().deriveFont(Font.BOLD, 16.0f);
	Insets numInsets = new Insets(numButtonBorder, 0, 0, numButtonBorder);
	for (int i = 0; i < 10; i++){ 
	    RSButton button = new RSButton(Integer.toString(i));
	    button.setBackground(bgColor);
	    button.setForeground(labelColor);
	    button.setFont(keyFont);
	    button.addActionListener(getDialButtonListener(Integer.toString(i)));

	    int row, col;
	    
	    if (i == 0){
		row = 3;
		col = 1;
	    } else {
		row = (i - 1)/3;
		col = (i - 1) % 3;
	    }
	    
	    dialPad.add(button, getGbc(col, row, gfillboth(), weightx(1.0), weighty(1.0), numInsets));
	}

	RSButton numButton = new RSButton("*");
	numButton.addActionListener(getDialButtonListener("*"));
	numButton.setBackground(bgColor);
	numButton.setForeground(labelColor.brighter());
	numButton.setFont(keyFont);
	dialPad.add(numButton, getGbc(0, 3, gfillboth(), weightx(1.0), weighty(1.0), numInsets));

	numButton = new RSButton("#");
	numButton.addActionListener(getDialButtonListener("#"));
	numButton.setBackground(bgColor);
	numButton.setForeground(labelColor.brighter());
	numButton.setFont(keyFont);
	dialPad.add(numButton, getGbc(2, 3, gfillboth(), weightx(1.0), weighty(1.0), numInsets));
	
	RSPanel buttonPane = new RSPanel();
	body.add(buttonPane, getGbc(1, 4, gridwidth(2), gfillboth(), new Insets(numButtonBorder, 0, numButtonBorder, 0)));

	buttonPane.setBackground(bgColor);
	buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER, numButtonBorder, 0));

	int iconSize = labelHeight;

	Icon dialIcon = IconUtilities.getIcon(StationConfiguration.PICKUP_CALL_ICON, iconSize, iconSize);
	Icon hangupIcon = IconUtilities.getIcon(StationConfiguration.HANGUP_CALL_ICON, iconSize, iconSize);

	dialButton = new RSButton(dialIcon);
	dialButton.setToolTipText("Dial");
	hangupButton = new RSButton(hangupIcon);
	hangupButton.setToolTipText("Hangup");

	newCallButton = new RSButton("New");

	int buttonVertFrac = 10;
	
	Dimension buttonSize = new Dimension(width/3, height*buttonVertFrac/100);
	dialButton.setPreferredSize(buttonSize);
	dialButton.setBackground(bgColor);
	dialButton.setForeground(labelColor);
	dialButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    dial();
		}
	    });

	hangupButton.setPreferredSize(buttonSize);
	hangupButton.setBackground(bgColor);
	hangupButton.setForeground(labelColor);
	hangupButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    telephonyController.hangupCall(currentCallInfo);
		}
	    });

	buttonPane.add(dialButton);
	buttonPane.add(hangupButton);

	return panel;
    }

    void dial(){
	if (RSCallerID.isValidID(RSCallerID.getIDType((String)dialOutPortBox.getSelectedItem()), dialIdField.getText()))
	    telephonyController.dial((String)dialOutPortBox.getSelectedItem(), dialIdField.getText(), dialNameField.getText(), "");
    }

    void validatePhone(){
	boolean phoneValid = false;
	String phoneType, phoneNumber;

	if (dialIdField == null)
	    return;

	phoneType = RSCallerID.getIDType((String)dialOutPortBox.getSelectedItem());
	phoneNumber = dialIdField.getText();
	if (phoneNumber == null || RSCallerID.isValidID(phoneType, phoneNumber))
	    phoneValid = true;
	
	if (phoneValid){
	    dialIdField.setBackground(defaultTextFieldColor);
	} else{
	    dialIdField.setBackground(errorTextFieldColor);
	}

	dialIdField.repaint();
    }

    ActionListener getDialButtonListener(final String str){
	return new ActionListener(){
	    public void actionPerformed(ActionEvent e){
		dialButtonPressed(str);
	    }
	};
    }

    void selectDialOutEntity(Entity entity){
	selectDialOutPort(entity.getPhoneType());
	dialNameField.setText(entity.getName());
	dialIdField.setText(entity.getPhone());
	entityMenu.setVisible(false);
    }

    void dialButtonPressed(String key){
	String str = dialIdField.getText().trim();
	dialIdField.setText(str+key);
    }

    JComponent getPrevCallsPane(int width, int height, Color bgColor, int labelHeight){
	JPanel panel = new JPanel();
	panel.setPreferredSize(new Dimension(width, height));
	panel.setBackground(bgColor);
	panel.setLayout(new GridBagLayout());
	Color prevCallsColor = stationConfig.getColor(StationConfiguration.TELEPHONY_METADATA_COLOR);
	RSLabel titleLabel = getColumnLabel("Recent Calls", prevCallsColor, width, labelHeight);
	panel.add(titleLabel, getGbc(0, 0));

	JPanel body = new JPanel();

	body.setBackground(bgColor);
	panel.add(body, getGbc(0, 1, gfillboth(), weighty(1.0), weightx(1.0)));

	body.setLayout(new GridBagLayout());
	int exportButtonHeight = labelHeight;
	height -= labelHeight + exportButtonHeight;

	prevCallsTable = new JTable(prevCallsModel);
	prevCallsTable.setFillsViewportHeight(true);
	prevCallsTable.getTableHeader().setReorderingAllowed(false);
	prevCallsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
		public void valueChanged(ListSelectionEvent e){
		    prevSelectionChanged();
		}
	    });

	prevCallsTable.addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    if (e.getClickCount() != 2)
			return;
		    TelephonyController.CallInfo call = prevCallsModel.getCall(prevCallsTable.rowAtPoint(e.getPoint()));
		    if (call == null)
			return;
		    telephonyController.launchMetadataWidget(call.getProgramID());
		}
	    });

	JScrollPane scrollPane = new JScrollPane(prevCallsTable);
	scrollPane.setPreferredSize(new Dimension(width, height));
	body.add(scrollPane, getGbc(0, 0));
	
	RSPanel actionPanel = new RSPanel();
	actionPanel.setBackground(bgColor);
	body.add(actionPanel, getGbc(0, 1));

	actionPanel.setLayout(new GridBagLayout());
	
	Dimension exportButtonSize = new Dimension(width/3, exportButtonHeight);
	Icon exportIcon = IconUtilities.getIcon(StationConfiguration.EXPORT_ICON, exportButtonSize.height, exportButtonSize.height);	
	RSButton exportButton = new RSButton("Export", exportIcon);
	exportButton.setPreferredSize(exportButtonSize);
	exportButton.setBackground(bgColor);
	exportButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    telephonyController.exportFiles(getSelectedPrevCalls());
		}
	    });
	exportButton.setToolTipText("Export selected calls as files.");
	actionPanel.add(exportButton);
	
	return panel;
    }

    RSLabel getColumnLabel(String labelString, Color bgColor, int width, int height){
	RSLabel label = new RSLabel(labelString);

	Color labelForeground = Color.white;
	Font labelFont = GUIUtilities.getDefaultFont().deriveFont(Font.BOLD);

	label.setForeground (labelForeground);
	label.setBackground(bgColor);
	label.setOpaque(true);
	label.setPreferredSize(new Dimension(width, height));
	//label.setFont(labelFont);
	label.setHorizontalAlignment(SwingConstants.CENTER);

	return label;
    }

    void addCategory(Category category, boolean forceAdd){
	boolean added = false;
	if (currentCallInfo != null){
	    added = currentCallInfo.addCategory(category);
	} else {
	    logger.error("AddCategory: Trying to add category to null callinfo.");
	    return;
	}
	
	if (!forceAdd && !added)
	    return;
    }

    public void setEntityFound(Entity entities[]){
	if (entities == null){
	    entityMenu.setVisible(false);
	    return;
	}
	entitySearchList.clear();
	for (Entity entity: entities){
	    entitySearchList.add(0, wrapEntity(entity));
	}
	
	entityListComp.setPreferredSize(dialNameField.getSize());

	if (entitySearchList.size() > 0)
	    entityMenu.show(dialNameField, 0, dialNameField.getSize().height);

	dialNameField.requestFocusInWindow();
    }


    EntityWrapper wrapEntity(Entity entity){
	return new EntityWrapper(entity);
    }

    Entity unwrapEntity(Object object){
	return ((EntityWrapper) object).getEntity();
    }

    Integer getMinSlot(){
	int i = 0;
	ArrayList<Integer> slotList = new ArrayList<Integer>(slotIndexes.values());
	Collections.sort(slotList);

	for (int index: slotList){
	    if (i < index)
		return i;
	    else
		i++;
	}

	return i;
    }
    
    public void addIncomingCall(TelephonyController.CallInfo callInfo){
	Integer slot = getMinSlot();
	IncomingCallWidget entryWidget = new IncomingCallWidget(callInfo);
	incomingCallEntries.put(callInfo, entryWidget);
	slotIndexes.put(callInfo, slot);
	addToIncomingList(entryWidget, slot);

	updateBMP();
	updateASP();
    }

    public void addOutgoingCall(TelephonyController.CallInfo callInfo){
	//Only one call can be dialed out.
	setButtonsEnableOutgoing(false);
	newCallButton.setEnabled(false);
	onairButton.setEnabled(false);

	//XXX bala 
	//currentCallInfo = callInfo;

	setCurrentCall(callInfo);

	ActiveCallWidget entryWidget = new ActiveCallWidget(callInfo, false);
	activeCallEntries.put(callInfo, entryWidget);
	Integer slot = getMinSlot();
	addToActiveList(entryWidget, slot);
	slotIndexes.put(callInfo, slot);

	setSelectedActiveCall(callInfo);
	updateGUIButtons();
	updateBMP();
	updateASP();
    }

    public void outgoingCallConnected(TelephonyController.CallInfo callInfo){
	setButtonsEnableOutgoing(true);
	newCallButton.getModel().setPressed(false);
	newCallButton.setEnabled(true);
	onairButton.setEnabled(true);
	ActiveCallWidget entryWidget = activeCallEntries.get(callInfo);
	if (entryWidget != null) {
	    entryWidget.setConnected();
	    setCurrentCall(callInfo);
	} else
	    logger.error("OutgoingCallConnected: Null callinfo");
    }

    public void outgoingCallRinging(TelephonyController.CallInfo callInfo){
	if (callInfo == null){
	    logger.error("OutgoingCallRinging: Null callinfo.");
	    return;
	}

	ActiveCallWidget entryWidget = activeCallEntries.get(callInfo);

	if (entryWidget != null)
	    entryWidget.setRinging();
	else
	    logger.error("OutgoingCallRinging: Null callinfo");
    }

    public void outgoingCallNoAnswer(TelephonyController.CallInfo callInfo){
	IncomingCallWidget entryWidget = incomingCallEntries.get(callInfo);
	if (entryWidget != null)
	    entryWidget.setNoAnswer();
	else
	    logger.error("OutgoingCallNoAnswer: Null callinfo");
    }

    public void outgoingCallBusy(TelephonyController.CallInfo callInfo){
	ActiveCallWidget entryWidget = activeCallEntries.get(callInfo);
	if (entryWidget != null)
	    entryWidget.setBusy();
	else
	    logger.error("OutgoingCallBusy: Null callinfo");
    }

    public void callAccepted(TelephonyController.CallInfo callInfo) {
	if (callInfo == null) {
	    logger.error("CallAccepted: Null callinfo.");
	    return;
	}
	
	IncomingCallWidget incomingWidget = incomingCallEntries.get(callInfo);
	incomingWidget.setAccepted();

	ActiveCallWidget entryWidget = new ActiveCallWidget(callInfo);
	activeCallEntries.put(callInfo, entryWidget);
	addToActiveList(entryWidget, slotIndexes.get(callInfo));
	setSelectedActiveCall(callInfo);

	updateGUIButtons();
	updateBMP();
	updateASP();
    }

    public void endCall(final TelephonyController.CallInfo callInfo){
	if (callInfo == currentCallInfo){
	    if (onairButton.isSelected() && !confButton.isSelected()) {
		telephonyController.goOffAir(confButton.isSelected());
		onairButton.setSelected(false);
	    }
	    clearCurrentCall();
	    setButtonsEnableOutgoing(true);
	    newCallButton.setEnabled(true);
	    onairButton.setEnabled(true);
	}

	if (callInfo == null){
	    logger.error("EndCall: Null callInfo.");
	    return;
	}

	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    IncomingCallWidget entryWidget = incomingCallEntries.remove(callInfo);
		    int slot = slotIndexes.remove(callInfo);
		    
		    if (entryWidget != null){
			slotPanel.remove(entryWidget);
			addBlankSlot(0, slot);
			entryWidget.delete();
		    } 
		    
		    ActiveCallWidget activeWidget = activeCallEntries.remove(callInfo);
		    if (activeWidget != null){
			slotPanel.remove(activeWidget);
			addBlankSlot(1, slot);
		    }
		    
		    if (activeWidget != null)
			addToPreviousCalls(callInfo);
		    
		    updateGUIButtons();
		    updateBMP();
		    updateASP();
		}
	    });
    }

    static final int MAX_PREV_CALLS = 10;
	
    
    void addToPreviousCalls(TelephonyController.CallInfo callInfo){
	prevCallsModel.add(callInfo);
    }

    public void goOffAir(){
	onairButton.setSelected(false);
    }

    public void updateGUIButtons(){
	if (isActiveCallPresent()){
	    prevCallsTable.setEnabled(false);
	} else {
	    prevCallsTable.setEnabled(true);
	}

    }

    // Called on activate.
    public void reInitButtons(){
	goOnlineButton.setSelected(false);
    }

    boolean isActiveCallPresent(){
	return activeCallEntries.size() > 0;
    }

    void addToIncomingList(final JComponent c, final int slot){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    Dimension dim = new Dimension(slotWidth, slotHeight - slotTopInset);
		    c.setPreferredSize(dim);
		    c.setMaximumSize(dim);
		    
		    JComponent blank = incomingCallComponents.get(slot);
		    if (blank != null)
			slotPanel.remove(blank);
		    
		    Insets insets = new Insets(slotTopInset, 0, 0, 0);
		    slotPanel.add(c, getGbc(0, slot, insets));
		    slotPanel.revalidate();
		}
	    });
    }

    void addToActiveList(final JComponent c, final int slot){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    JComponent blank = activeCallComponents.get(slot);
		    if (blank != null)
			slotPanel.remove(blank);
		    
		    Insets insets = new Insets(slotTopInset, 0, 0, 0);
		    slotPanel.add(c, getGbc(1, slot, insets));
		    slotPanel.revalidate();
		}
	    });
    }

    void prevSelectionChanged(){
	
	ArrayList<TelephonyController.CallInfo> list = getSelectedPrevCalls();

	if (list.size() > 1 || list.size() == 0){
	    clearCurrentCall();
	    return;
	}

	selectPrevCall(list.get(0));
    }

    ArrayList<TelephonyController.CallInfo> getSelectedPrevCalls(){
	ListSelectionModel selectionModel = prevCallsTable.getSelectionModel();
	int minIndex = selectionModel.getMinSelectionIndex();
	int maxIndex = selectionModel.getMaxSelectionIndex();

	ArrayList<TelephonyController.CallInfo> list = new ArrayList<TelephonyController.CallInfo> ();

	if (minIndex < 0 || maxIndex < 0)
	    return list;

	for (int i = minIndex; i <= maxIndex; i++){
	    if (selectionModel.isSelectedIndex(i) && prevCallsModel.getCall(i) != null)
		list.add(prevCallsModel.getCall(i));
	}

	return list;
    }

    void selectPrevCall(TelephonyController.CallInfo callInfo){
	if (callInfo == null)
	    return;

	callInfoToSave = callInfo;
	updateSelectedCallPanel(callInfo);
    }

    public void setCurrentCall(TelephonyController.CallInfo callInfo){
	currentCallInfo = callInfo;
	callInfoToSave = callInfo;
	updateSelectedCallPanel(callInfo);
	//setButtonsEnableOutgoing(false);
    }

    void updateSelectedCallPanel(TelephonyController.CallInfo callInfo){
	updateIDField(callInfo);
	populateNameField(callInfo);
	updateLocationField(callInfo);
	descField.setText(callInfo.getDescription());
	tagsField.setText(callInfo.getTagsString());
    }

    void clearCurrentCall(){
	currentCallInfo = null;
	idField.setText(null);
	locationField.setText(null);
	tagsField.setText(null);
	descField.setText(null);

	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    nameField.removeAllItems();
		}
	    });
    }

    void populateNameField(final TelephonyController.CallInfo callInfo) {
	if (callInfo == null)
	    return;
	
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    nameField.removeAllItems();
		    nameField.addItem(makeObject(SELECT_ADD_CALLER));
		    for (String name: callInfo.getPotentialCallerNames())
			nameField.addItem(makeObject(name));
		    
		    nameField.setSelectedIndex(callInfo.getCallerIndex() + 1);
		    
		    if (nameField.getSelectedIndex() == 0) 
			nameField.setEditable(true);
		    else
			nameField.setEditable(false);
		}
	    });
    }

    Object makeObject(final String item) {
	return new Object() {
	    public String toString() {
		return item;
	    }
	};
    }

    boolean isValidID(String lineID, String idString) {
	String idType = RSCallerID.getIDType(lineID);
	return RSCallerID.isValidID(idType, idString);
    }

    void saveCallInfo(TelephonyController.CallInfo callInfo) {
	boolean idValid;

	if (callInfo == null){
	    logger.warn("SaveCallInfo: Null callinfo.");
	    return;
	}

	idValid = isValidID(numberTypeLabel.getText(), idField.getText());
	if (!idValid) {
	    logger.warn("SaveCallInfo: Invalid caller ID:" + idField.getText());
	    errorInference.displayMetadataError("Invalid caller ID. Caller ID not saved.", new String[]{});
	}

	if (nameField.getSelectedIndex() <= 0) {
	    if (nameField.getSelectedItem() != null && !nameField.getSelectedItem().toString().equals(SELECT_ADD_CALLER)) {

		if (idValid) {
		    callInfo.addCaller(numberTypeLabel.getText(),
					      idField.getText(), 
					      nameField.getSelectedItem().toString(), 
					      locationField.getText());
		} else {
		    callInfo.addCaller(numberTypeLabel.getText(),
					      "", 
					      nameField.getSelectedItem().toString(), 
					      locationField.getText());
		}
		populateNameField(callInfo);

	    }
	} else {
	    if (idValid) {
		telephonyController.updateCallerInfo(callInfo.getCaller(), numberTypeLabel.getText(),
						     idField.getText(), 
						     nameField.getSelectedItem().toString(), locationField.getText());
	    } else {
		telephonyController.updateCallerInfo(callInfo.getCaller(), numberTypeLabel.getText(),
						     callInfo.getCaller().getPhone(), 
						     nameField.getSelectedItem().toString(), locationField.getText());
	    }
	    callInfo.saveCaller();
	}

	callInfo.setDescription(descField.getText());
	telephonyController.saveCallInfo(callInfo);

	prevCallsTable.repaint();
    }

    void clearCallInfo() {
	if (currentCallInfo == null){
	    return;
	}

	currentCallInfo.revertCallInfo();
	populateNameField(currentCallInfo);
	updateLocationField(currentCallInfo);
	updateIDField(currentCallInfo);
    }


    void updateIDField(TelephonyController.CallInfo callInfo) {
	if (callInfo.getCallerID() != null) {
	    idField.setText(callInfo.getCallerID().getCallerID());
	    numberTypeLabel.setText(callInfo.getCallerID().getLineID());
	} else {
	    idField.setText("");
	    numberTypeLabel.setText("");
	}
    }

    void updateLocationField(TelephonyController.CallInfo callInfo) {
	if (callInfo == null ||
	   callInfo.getCaller() == null ||
	   callInfo.getCaller().getLocation() == null)
	
	    locationField.setText("");
	else
	    locationField.setText(callInfo.getCaller().getLocation());

    }

    void setButtonsEnableOutgoing(boolean enabled){
	if (enabled){
	    dialButton.setEnabled(true);
	    hangupButton.setEnabled(false);
	} else {
	    dialButton.setEnabled(false);
	    hangupButton.setEnabled(true);
	}
	
    }
    
    void setSelectedActiveCall(TelephonyController.CallInfo callInfo){
	for (TelephonyController.CallInfo call: activeCallEntries.keySet()){
	    if (call == callInfo){
		activeCallEntries.get(callInfo).setSelected(true);
	    } else {
		activeCallEntries.get(call).setSelected(false);
	    }
	}
    }

    void setDeselectedActiveCalls(){
	for (TelephonyController.CallInfo call: activeCallEntries.keySet()){
	    activeCallEntries.get(call).setSelected(false);
	}
    }

    protected void onMaximize(){

    }

    protected void onMinimize(){

    }

    public void onUnload(){
	bmpTimer.stop();
	setBMPIcon(false);

	telephonyController.disconnect();
	telephonyController.unregisterWithProviders();
	wpComponent.removeAll();
	aspComponent.removeAll();
	slotIndexes.clear();
	incomingCallComponents.clear();
	activeCallComponents.clear();
	incomingCallEntries.clear();
	activeCallEntries.clear();
    }

    class EntityWrapper {
	Entity entity;
	public EntityWrapper(Entity entity){
	    this.entity = entity;
	}
	
	public Entity getEntity(){
	    return entity;
	}

	public String toString(){
	    return entity.getName()+" ("+entity.getPhone()+")" +", "+entity.getLocation();
	}
    }

    
    CallInfoWrapper wrapCallInfo(final TelephonyController.CallInfo callInfo){
	return (new CallInfoWrapper(callInfo));
    }

    TelephonyController.CallInfo unwrapCallInfo(CallInfoWrapper infoWrapper){
	return infoWrapper.getCallInfo();
    }

    static int callCounter = 0;
    class CallInfoWrapper {
	TelephonyController.CallInfo callInfo;
	int num;
	public CallInfoWrapper(TelephonyController.CallInfo callInfo){
	    this.callInfo = callInfo;
	    num = callCounter++;
	}

	public TelephonyController.CallInfo getCallInfo(){
	    return callInfo;
	    
	}

	public String toString(){
	    String callerString = null;
	    RSCallerID callerID = callInfo.getCallerID();
	    if (callerID != null){
		callerString = callerID.getCallerID();
	    }
	    if (callerString != null && !callerString.equals(""))
		return callerString;
	    else
		return Integer.toString(num);
	}

    }
	
    
    class ActiveCallWidget extends JPanel {

	RSToggleButton talkButton;
	RSButton dismissButton;

	boolean isIncoming;
	
	Color ringingColor;
	Color connectedColor;

	public ActiveCallWidget(final TelephonyController.CallInfo callInfo){
	    this(callInfo, true);
	}

	public ActiveCallWidget(final TelephonyController.CallInfo callInfo, final boolean isIncoming){
	    this.isIncoming = isIncoming;

	    ringingColor = stationConfig.getColor(StationConfiguration.CALL_RINGING_COLOR);
	    connectedColor = stationConfig.getColor(StationConfiguration.CALL_CONNECTED_COLOR);

	    setLayout(new BorderLayout());
	    int height = slotHeight - slotTopInset;
	    Dimension talkButtonSize = new Dimension(slotWidth - height, height);

	    int iconSize = height/2;
	    final Icon audioHighIcon = IconUtilities.getIcon(StationConfiguration.AUDIO_HIGH_ICON, iconSize, iconSize);
	    final Icon audioMutedIcon = IconUtilities.getIcon(StationConfiguration.AUDIO_MUTED_ICON, iconSize, iconSize);
	    talkButton = new RSToggleButton(callInfo.getCallerID().getCallerID(), audioHighIcon);

	    talkButton.setBackground(bgColor);
	    talkButton.setToolTipText("Press to Mute");
	    talkButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			talkButton.setEnabled(false);
			if (talkButton.isSelected()){
			    setSelectedActiveCall(callInfo);
			    if (confButton.isSelected())
				telephonyController.unmuteCall(callInfo);
			    else
				telephonyController.previewCall(callInfo);
			    talkButton.setToolTipText("Press to Mute");
			    talkButton.setIcon(audioHighIcon);
			} else {
			    if (confButton.isSelected())
				telephonyController.muteCall(callInfo);
			    else
				telephonyController.holdCall(callInfo);
			    talkButton.setToolTipText("Press to Unmute");
			    talkButton.setIcon(audioMutedIcon);
			}
			talkButton.setEnabled(true);
		    }
		});

	    talkButton.setPreferredSize(talkButtonSize);
	    talkButton.setMinimumSize(talkButtonSize);
	    
	    Icon hangupIcon = IconUtilities.getIcon(StationConfiguration.HANGUP_CALL_ICON, (int)(height/1.5), (int)(height/1.5));
	    dismissButton = new RSButton(hangupIcon);
	    dismissButton.setBackground(bgColor);
	    Dimension dismissButtonSize = new Dimension(slotWidth - talkButtonSize.width, height);
	    dismissButton.setPreferredSize(dismissButtonSize);
	    dismissButton.setMinimumSize(dismissButtonSize);
	    dismissButton.setToolTipText("Hangup Call");
	    dismissButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			telephonyController.hangupCall(callInfo);
		    }
		});

	    add(talkButton, BorderLayout.CENTER);
	    add(dismissButton, BorderLayout.WEST);

	    if (!isIncoming) {
		talkButton.setEnabled(false);
	    }
	}

	public void setSelected(boolean isSelected){
	    talkButton.setSelected(isSelected);
	}

	public void setRinging(){

	}

	public void setConnected(){
	    talkButton.setEnabled(true);
	}

	public void setBusy(){

	}

    }

    class IncomingCallWidget extends JPanel {

	RSToggleButton pickupButton;
	RSButton dismissButton;
	javax.swing.Timer timer;

	boolean isIncoming;
	
	static final int RING_UPDATE_INTERVAL = 500;
	
	Color ringingColor;
	Color connectedColor;

	Icon incomingArrowIcon;

	public IncomingCallWidget(final TelephonyController.CallInfo callInfo){
	    this(callInfo, true);
	}

	public IncomingCallWidget(final TelephonyController.CallInfo callInfo, final boolean isIncoming){
	    this.isIncoming = isIncoming;
	    
	    ringingColor = stationConfig.getColor(StationConfiguration.CALL_RINGING_COLOR);
	    connectedColor = stationConfig.getColor(StationConfiguration.CALL_CONNECTED_COLOR);
	    
	    int height = slotHeight - slotTopInset;
	    Dimension pickupButtonSize = new Dimension(slotWidth * pickupButtonHorizFrac/100, height);

	    Icon pickupIcon = IconUtilities.getIcon(StationConfiguration.PICKUP_CALL_ICON, height/2, height/2);
	    pickupButton = new RSToggleButton(callInfo.getCallerID().toString(), pickupIcon);
	    pickupButton.setHorizontalTextPosition(SwingConstants.LEADING);
	    pickupButton.setBackground(bgColor);
	    pickupButton.setToolTipText("Pickup Call");

	    pickupButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (!pickupButton.isSelected()) {
			    pickupButton.setSelected(true);
			    return;
			}
			telephonyController.acceptCall(callInfo);
			IncomingCallWidget entryWidget = incomingCallEntries.get(callInfo);
			if (entryWidget != null)
			    entryWidget.setConnected();
		    }
		});
	    setLayout(new BorderLayout());
	    pickupButton.setPreferredSize(pickupButtonSize);	
	    pickupButton.setMinimumSize(pickupButtonSize);
	    pickupButton.setSelected(!isIncoming);

	    Dimension enableTalkButtonSize = new Dimension(height, height);

	    Icon closeIcon = IconUtilities.getIcon(StationConfiguration.HANGUP_CALL_ICON, (int)(height/1.5), (int)(height/1.5));
	    dismissButton = new RSButton(closeIcon);
	    dismissButton.setPreferredSize(enableTalkButtonSize);
	    dismissButton.setMinimumSize(enableTalkButtonSize);
	    dismissButton.setBackground(bgColor);
	    dismissButton.setToolTipText("Hangup Call");
	    dismissButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			telephonyController.hangupCall(callInfo);
		    }
		});

	    add(pickupButton, BorderLayout.CENTER);
	    add(dismissButton, BorderLayout.WEST);

	    int iconSize = slotHeight * 75/100;
	    incomingArrowIcon = IconUtilities.getIcon(StationConfiguration.INCOMING_ARROW_ICON, iconSize, iconSize);
	    
	    setRinging();
	}

	
	public void setConnected(){
	    if (timer != null)
		timer.stop();

	    setColor(connectedColor);
	}

	void setRinging(){

	    timer = new javax.swing.Timer(RING_UPDATE_INTERVAL, new ActionListener(){
		    boolean on = true;
		    public void actionPerformed(ActionEvent e){
			Color color;
			if (on) {
			    color = bgColor;
			} else {
			    color = ringingColor;
			}
			setColor(color);
			on = !on;
		    }
		});
	    timer.start();
	}

	void setColor(Color color){
	    dismissButton.setBackground(color);
	    pickupButton.setBackground(color);
	}

	public void setBusy(){

	}
	
	void setAccepted(){
	    SwingUtilities.invokeLater(new Runnable(){
		    public void run(){
			remove(pickupButton);
			remove(dismissButton);
			setBackground(bgColor);
			RSLabel label = new RSLabel();
			label.setIcon(incomingArrowIcon);
			add(label, BorderLayout.EAST);
			repaint();
		    }
		});
	}

	public void setNoAnswer(){
	    dismissButton.setBackground(Color.red);
	}

	public void setMute(){

	}

	public void setUnMute(){

	}

	public void delete(){
	    //XXX remove buttons from buttongroup etc.
	}
    }

    class PrevCallsTableModel extends DefaultTableModel implements CacheObjectListener {
	ArrayList<TelephonyController.CallInfo> calls = new ArrayList<TelephonyController.CallInfo>();
	Hashtable<String, TelephonyController.CallInfo> callsById = new Hashtable<String, TelephonyController.CallInfo>();
	
	int prevCallsBuffer = 10;

	public void add(final TelephonyController.CallInfo callInfo){
	    if (callInfo.getProgramID() == null) {
		logger.warn("Add: Not adding callinfo with null program ID to Previous Calls table.");
		return;
	    }
	    cacheProvider.addCacheObjectListener(new RadioProgramMetadata(callInfo.getProgramID()), this);
	    callsById.put(callInfo.getProgramID(), callInfo);
	    SwingUtilities.invokeLater(new Runnable(){
		    public void run(){
			calls.add(0, callInfo);
			if (calls.size() == MAX_PREV_CALLS + prevCallsBuffer){
			    removeCall(calls.get(MAX_PREV_CALLS + prevCallsBuffer));
			}
			fireTableDataChanged();
		    }
		});
	}
	
	public int getColumnCount(){
	    return 3;
	}
	
	void removeCall(TelephonyController.CallInfo callInfo){
	    calls.remove(callInfo);
	    cacheProvider.removeCacheObjectListener(new RadioProgramMetadata(callInfo.getProgramID()), this);
	    callsById.remove(callInfo.getProgramID());
	    fireTableDataChanged();
	}

	final String columnNames[] = new String[] {"Name", "Location", "Phone"};

	public String getColumnName(int column){
	    return columnNames[column];
	}
	
	public Object getValueAt(int row, int column){
	    if (row >= calls.size())
		return null;
	    TelephonyController.CallInfo callInfo = calls.get(row);

	    switch(column){
	    case 0:
		if (callInfo.getCaller() != null)
		    return callInfo.getCaller().getName();
		else
		    return null;
	    case 1:
		if (callInfo.getCaller() != null)
		    return callInfo.getCaller().getLocation();
		else
		    return null;
	    case 2:
		return callInfo.getCallerID().getCallerID();
	    default:
		return null;
	    }
	}

	public boolean isCellEditable(int row, int col){
	    return false;
	}

	public TelephonyController.CallInfo getCall(int row){
	    if (row >= calls.size())
		return null;
	    else
		return calls.get(row);
	}

	public void cacheObjectValueChanged(Metadata metadata){
	    RadioProgramMetadata program = (RadioProgramMetadata) metadata;
	    TelephonyController.CallInfo callInfo = callsById.get(program.getItemID());
	    callInfo.setDescription(program.getDescription());
	    ItemCategory queryCategory = new ItemCategory(program.getItemID());
	    ItemCategory categories[] = telephonyController.getMetadataProvider().get(queryCategory);
	    callInfo.setItemCategories(categories);
	    prevSelectionChanged();
	}

	public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKeys){
	    for (int i = 0; i < metadata.length; i++){
		String oldKey = ((RadioProgramMetadata) metadata[i]).getItemID();
		String newKey = newKeys[i].substring(0, newKeys[i].lastIndexOf("."));

		if (newKey == null || newKey.equals("")){
		    removeCall(callsById.get(oldKey));
		} else {
		    TelephonyController.CallInfo callInfo = callsById.remove(oldKey);
		    callInfo.setProgramID(newKey);
		    callsById.put(newKey, callInfo);
		}
	    }
	}
    }
}
