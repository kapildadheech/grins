package org.gramvaani.radio.app.gui;

public interface MultiListMoreItems {

    public boolean hasMoreItems(String moreItemsID);
    public void populateMoreItems(String moreItemsID);

}