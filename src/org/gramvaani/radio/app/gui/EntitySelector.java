package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.medialib.Entity;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import java.util.ArrayList;

public class EntitySelector extends JFrame {
    int width, height;

    JTable contactsTable;
    ContactsTableModel tableModel;
    EntityListener listener;

    Color bgColor;

    EntitySelector(EntityListener listener, int width, int height, int labelHeight, JComponent assocComponent, MetadataProvider metadataProvider, CacheProvider cacheProvider, StationConfiguration stationConfig){
	this.listener = listener;

	int searchHeight = labelHeight;
	int searchWidth = width/2;

	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR);

	setLayout(new BorderLayout());
	((JComponent)getContentPane()).setBorder(BorderFactory.createLineBorder(Color.gray));

	JPanel searchPanel = new JPanel();
	searchPanel.setBackground(bgColor);
	searchPanel.setLayout(new GridBagLayout());
	JLabel searchLabel = new JLabel("Search: ");	

	Dimension labelSize = new Dimension((width - searchWidth)/2, searchHeight);
	searchLabel.setPreferredSize(labelSize);
	searchLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	searchPanel.add(searchLabel);

	Dimension fieldSize = new Dimension(searchWidth, searchHeight);
	final SearchTextField searchField = new SearchTextField();
	searchField.setPreferredSize(fieldSize);
	searchPanel.add(searchField);

	searchField.getDocument().addDocumentListener(new DocumentAdapter(){
		public void documentUpdated(DocumentEvent e){
		    searchStringUpdated(searchField.getText());
		}

	    });

	searchField.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    if (tableModel.getRowCount() == 1){
			returnEntities(new Entity[] {tableModel.getEntityAt(0)});
			setVisible(false);
		    }
		}
	    });

	JPanel blankPanel = new JPanel();
	blankPanel.setPreferredSize(labelSize);
	blankPanel.setOpaque(false);
	searchPanel.add(blankPanel);
	add(searchPanel, BorderLayout.NORTH);

	tableModel = new ContactsTableModel(metadataProvider, cacheProvider){
		public int getColumnCount(){
		    return 2;
		}
	    };
	contactsTable = new JTable(tableModel);
	contactsTable.setFillsViewportHeight(true);
	contactsTable.addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    if (e.getClickCount() == 2){
			returnEntities(getSelection());
			setVisible(false);
		    }
		}
	    });
	contactsTable.getTableHeader().setBackground(bgColor);
	contactsTable.addKeyListener(new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ENTER){
			returnEntities(getSelection());
			setVisible(false);
		    }

		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			setVisible(false);
		    }
		}
	    });
	JScrollPane scrollPane = new JScrollPane(contactsTable);

	add(scrollPane, BorderLayout.CENTER);

	Point p = assocComponent.getLocationOnScreen();

	int x = (int) p.getX();
	int y = (int) (p.getY() + assocComponent.getSize().height);

	setUndecorated(true);
	pack();
	setLocation(x, y);

	addWindowFocusListener(new WindowAdapter(){
		public void windowLostFocus(WindowEvent e){
		    if (e.getOppositeWindow() instanceof JDialog)
			return;
		    setVisible(false);
		}
		
		public void windowDeactivated(WindowEvent e){
		    setVisible(false);
		}
	    });

	KeyAdapter escapeAdapter = new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			setVisible(false);
		    }
		}
	    };

	addKeyListener(escapeAdapter);
	searchField.addKeyListener(escapeAdapter);
	tableModel.reloadTable();
    }

    static final int MIN_QUERY_LENGTH = 3;
    void searchStringUpdated(String query){
	if (query == null)
	    return;

	if (query.length() < MIN_QUERY_LENGTH)
	    query = "";

	tableModel.search(query);
    }

    void returnEntities(Entity[] entities){
	if (listener != null)
	    listener.entitiesSelected(entities);
    }

    Entity[] getSelection(){
	ListSelectionModel model = contactsTable.getSelectionModel();

	int minIndex = model.getMinSelectionIndex();
	int maxIndex = model.getMaxSelectionIndex();

	if (minIndex < 0 || maxIndex < 0)
	    return new Entity[]{};

	ArrayList<Entity> list = new ArrayList<Entity>();
	
	for (int i = minIndex; i <= maxIndex; i++){
	    if (model.isSelectedIndex(i))
		list.add(tableModel.getEntityAt(i));
	}

	return list.toArray(new Entity[list.size()]);
    }

    public interface EntityListener {
	void entitiesSelected(Entity[] entities);
    }
}