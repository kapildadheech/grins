package org.gramvaani.radio.app.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LinearGradientPaint;

import javax.swing.border.AbstractBorder;

public class GradientBorder extends AbstractBorder {
    int top = 0;
    int bottom = 1;
    int left = 0;
    int right = 0;

    Color color;
    Color colors[];
    float frac[];

    public GradientBorder(Color color){
	this(color, 64);
    }

    public GradientBorder(Color color, int alpha){
	this.color = color;
	Color transparent = new Color (color.getRed(), color.getGreen(), color.getBlue(), 0);
	Color highest = new Color (color.getRed(), color.getGreen(), color.getBlue(), alpha);
	colors = new Color[]{transparent, highest, transparent};
	frac = new float[] {0.0f, 0.5f, 1.0f};
    }

    public Insets getBorderInsets(Component c){
	return new Insets(top, left, bottom, right);
    }

    public boolean isBorderOpaque(){
	return false;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height){
	Graphics2D g2 = (Graphics2D) g;
	LinearGradientPaint gradient = new LinearGradientPaint((float)x, (float)y+height-1, (float) x+width, (float) y+height-1, frac, colors);
	g2.setPaint(gradient);
	g2.drawLine(x, y + height - 1, x+width, y+height - 1);
    }

}