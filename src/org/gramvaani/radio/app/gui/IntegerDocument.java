package org.gramvaani.radio.app.gui;

import javax.swing.text.*;

public class IntegerDocument extends PlainDocument {
    public void insertString (int offset, String str, AttributeSet aSet) throws BadLocationException {
	if (str == null) {
	    return;
	} else if (str.matches("[0-9]*")) {
	    super.insertString (offset, str, aSet);
	}
    }
}