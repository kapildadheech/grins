package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.telephonylib.RSCallerID;
import org.gramvaani.radio.medialib.Entity;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.utilities.IconUtilities;
import org.gramvaani.utilities.LogUtilities;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class EntityEditorPopup extends JFrame {
    Entity editedEntity;
    JComboBox phoneTypeBox;
    JTextField nameField, locationField, phoneField, emailField;

    ActionListener phoneTypeListener;
    DocumentListener phoneFieldListener;
    
    Color defaultTextFieldColor, errorTextFieldColor;
    boolean isEdit;
    EntityEditorListener listener;
    LogUtilities logger = new LogUtilities("EntityEditorPopup");
    JPanel editorPanel;
    JLabel editorTitleLabel;
    RSApp rsApp;

    public EntityEditorPopup(EntityEditorListener listener, int width, int height, Color bgColor, Color titleColor, StationConfiguration stationConfig, RSApp rsApp){
	this.listener = listener;
	this.rsApp    = rsApp;

	editorPanel = new JPanel();

	Dimension size = new Dimension(width, height);
	editorPanel.setBorder(BorderFactory.createLineBorder(bgColor.darker()));
	editorPanel.setPreferredSize(size);
	editorPanel.setLayout(new BorderLayout());

	int titleVertFrac = 15;
	int buttonPanelVertFrac = 15;
	
	int titleHeight = titleVertFrac * size.height/100;
	int buttonPanelHeight = buttonPanelVertFrac * size.height/100;
	int fieldPanelHeight = size.height - titleHeight - buttonPanelHeight;

	Dimension titleSize = new Dimension(size.width, titleHeight);
	editorTitleLabel = new RSLabel("");
	editorTitleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	editorTitleLabel.setPreferredSize(titleSize);
	editorTitleLabel.setOpaque(true);
	editorTitleLabel.setBackground(titleColor);
	editorPanel.add(editorTitleLabel, BorderLayout.NORTH);

	Dimension fieldPanelSize = new Dimension(size.width, fieldPanelHeight);
	RSPanel fieldPanel = new RSPanel();
	fieldPanel.setBackground(bgColor);
	fieldPanel.setPreferredSize(fieldPanelSize);
	
	editorPanel.add(fieldPanel, BorderLayout.CENTER);

	Dimension buttonPanelSize = new Dimension(size.width, buttonPanelHeight);
	RSPanel buttonPanel = new RSPanel();
	buttonPanel.setBackground(bgColor);
	buttonPanel.setPreferredSize(buttonPanelSize);
	
	editorPanel.add(buttonPanel, BorderLayout.SOUTH);

	fieldPanel.setLayout(new GridBagLayout());
	
	int labelHorizFrac = 20;
	int labelVertFrac = 15;

	int labelWidth = labelHorizFrac * size.width/100;
	int fieldWidth = size.width - labelWidth - 5;
	int labelHeight = labelVertFrac * fieldPanelHeight/100;

	Dimension labelSize = new Dimension(labelWidth, labelHeight);
	Dimension fieldSize = new Dimension(fieldWidth, labelHeight);

	int vertGap = 4;

	Insets insets = new Insets(0, 0, vertGap, 0);

	RSLabel nameLabel = new RSLabel("Name: ");
	nameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(nameLabel, getGbc(0, 0, insets, gfillboth()));

	nameField = new JTextField();
	nameField.setPreferredSize(fieldSize);
	fieldPanel.add(nameField, getGbc(1, 0, insets));

	RSLabel locationLabel = new RSLabel("Location: ");
	locationLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(locationLabel, getGbc(0, 1, insets, gfillboth()));

	locationField = new JTextField();
	locationField.setPreferredSize(fieldSize);
	fieldPanel.add(locationField, getGbc(1, 1, insets));

	RSLabel phoneLabel = new RSLabel("Phone: ");
	phoneLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(phoneLabel, getGbc(0, 2, insets, gfillboth()));

	RSPanel phonePanel = new RSPanel();
	phonePanel.setBackground(bgColor);
	phonePanel.setPreferredSize(fieldSize);
	phonePanel.setLayout(new GridBagLayout());
	phoneTypeBox = new JComboBox(RSCallerID.getIDTypes());
	
	int typeHorizFrac = 20;
	int typeWidth = typeHorizFrac * fieldSize.width/100;
	
	phoneTypeBox.setPreferredSize(new Dimension(typeWidth, fieldSize.height));
	phoneTypeBox.setBackground(bgColor);

	phonePanel.add(phoneTypeBox);

	phoneTypeListener = new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    validatePhone();
		}
	    };
		
	phoneField = new JTextField();
	phoneField.setPreferredSize(new Dimension(fieldSize.width - typeWidth, fieldSize.height));
	phoneFieldListener = new DocumentAdapter(){
		public void documentUpdated(DocumentEvent e){
		    validatePhone();
		}
	    };
	phonePanel.add(phoneField);
	fieldPanel.add(phonePanel, getGbc(1, 2, insets));
	
	defaultTextFieldColor = phoneField.getBackground();
	errorTextFieldColor = stationConfig.getColor(StationConfiguration.ERROR_FIELD_COLOR);

	RSLabel emailLabel = new RSLabel("Email: ");
	emailLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(emailLabel, getGbc(0, 3, insets, gfillboth()));

	emailField = new JTextField();
	emailField.setPreferredSize(fieldSize);
	fieldPanel.add(emailField, getGbc(1, 3, insets));

	int buttonHorizFrac = 40;
	int buttonWidth = buttonHorizFrac * size.width/100 / 2;

	int buttonGap = 4;
	Dimension buttonSize = new Dimension(buttonWidth, buttonPanelSize.height - buttonGap * 2);
	int iconSize = (buttonSize.height * 8)/10;
	buttonPanel.setLayout(new GridBagLayout());

	RSButton saveButton = new RSButton("Save");
	saveButton.setPreferredSize(buttonSize);
	saveButton.setBackground(bgColor);
	saveButton.setIcon(IconUtilities.getIcon(StationConfiguration.SAVE_ICON, iconSize));
	saveButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    saveEntity();
		}
	    });
	buttonPanel.add(saveButton);
	
	RSButton cancelButton = new RSButton("Cancel");
	cancelButton.setPreferredSize(buttonSize);
	cancelButton.setBackground(bgColor);
	cancelButton.setIcon(IconUtilities.getIcon(StationConfiguration.CANCEL_ICON, iconSize));
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    cancelEditor();
		}
	    });
							   
	buttonPanel.add(cancelButton);
	
	ActionListener submitListener = new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    saveEntity();
		}
	    };

	KeyListener escapeListener = new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			cancelEditor();
		    }
		}
	    };

	for (JComponent c: new JComponent[]{editorPanel, nameField, locationField, 
					    phoneField, phoneTypeBox, emailField}){
	    c.addKeyListener(escapeListener);
	}

	for (JTextField c: new JTextField[]{nameField, locationField, phoneField, emailField}){
	    c.addActionListener(submitListener);
	}

	addWindowFocusListener(new WindowAdapter(){
		public void windowLostFocus(WindowEvent e){
		    cancelEditor();
		}
	    });

	add(editorPanel);
	setUndecorated(true);
	pack();
    }

    void validatePhone(){
	String phoneType = (String) phoneTypeBox.getSelectedItem();
	String phoneNumber = phoneField.getText();

	if (phoneType == null || phoneNumber == null || RSCallerID.isValidID(phoneType, phoneNumber)){
	    phoneField.setBackground(defaultTextFieldColor);
	} else {
	    phoneField.setBackground(errorTextFieldColor);
	}
	phoneField.repaint();
    }

    void saveEntity(){
	if (isEdit){
	    listener.editEntity(editedEntity.getEntityID(), nameField.getText(), editedEntity.getUrl(), 
				(String)phoneTypeBox.getSelectedItem(), phoneField.getText(), 
				emailField.getText(), locationField.getText(), editedEntity.getEntityType());
	} else {
	    listener.addEntity(nameField.getText(), editedEntity.getUrl(), 
			       (String)phoneTypeBox.getSelectedItem(), phoneField.getText(), 
			       emailField.getText(), locationField.getText(), editedEntity.getEntityType());
	}
	rsApp.getControlPanel().cpJFrame().entitiesChanged();
	cancelEditor();
    }

    void cancelEditor(){
	setVisible(false);
    }

    void setEditedEntity(Entity entity){
	if (entity == null){
	    logger.error("SetEditedEntity: Setting null entity.");
	    return;
	}
	
	phoneField.getDocument().removeDocumentListener(phoneFieldListener);
	phoneTypeBox.removeActionListener(phoneTypeListener);
	phoneTypeBox.setSelectedItem(RSCallerID.PSTN);

	editedEntity = entity;
	nameField.setText(entity.getName());
	locationField.setText(entity.getLocation());
	if (entity.getPhoneType() != null)
	    phoneTypeBox.setSelectedItem(entity.getPhoneType());
	phoneField.setText(entity.getPhone());

	validatePhone();

	emailField.setText(entity.getEmail());

	phoneTypeBox.addActionListener(phoneTypeListener);
	phoneField.getDocument().addDocumentListener(phoneFieldListener);
    }

    void showEditor(boolean isEdit, Entity entity, JComponent assocComponent){
	this.isEdit = isEdit;

	Dimension editorSize = editorPanel.getPreferredSize();
	
	if (isEdit){
	    editorTitleLabel.setText("Edit Contact");
	    setEditedEntity(entity);
	    
	} else {
	    editorTitleLabel.setText("Add Contact");
	    if(entity != null)
		setEditedEntity(entity);
	    else
		setEditedEntity(new Entity());
	}


	int xOffset = -(editorSize.width - assocComponent.getSize().width)/2;
	int yOffset = -(editorSize.height);
	Point p = assocComponent.getLocationOnScreen();

	int popupX = p.x + xOffset;
	int popupY = p.y + yOffset;

	phoneField.setBackground(defaultTextFieldColor);
	phoneTypeBox.hidePopup();

	/*
	editorPopup = PopupFactory.getSharedInstance().getPopup(wpComponent, editorPanel, popupX, popupY);
	editorPopup.show();
	*/
	setLocation(popupX, popupY);
	setVisible(true);
	editorPanel.requestFocusInWindow();
	validatePhone();
    }

    public interface EntityEditorListener {
	public void addEntity(String name, String url, String phoneType, String phoneNumber, String email, String location, String entityType);
	public void editEntity(int id, String name, String url, String phoneType, String phoneNumber, String email, String location, String entityType);
    }
}