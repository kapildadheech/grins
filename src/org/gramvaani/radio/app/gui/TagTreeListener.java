package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.Category;

import java.util.HashSet;

public interface TagTreeListener {
    public void tagSelectionChanged(Category tag, boolean selection);
    public void tagEdited(Category tag);
}