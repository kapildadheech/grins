package org.gramvaani.radio.app.gui;

import java.awt.*;
import javax.swing.*;

public class RSTitleBar extends JPanel {
    String title;
    JLabel label;
    int width;
    Dimension size;

    //protected static get
    public static final int HEIGHT = 30;
    public RSTitleBar(String title, int width){
	label = new JLabel(title);
	this.width = width;
	size = new Dimension(width, HEIGHT);
	setSize(size);
	setPreferredSize(size);
	setMaximumSize(size);
	setMinimumSize(size);
	setLayout(new GridBagLayout());
	add(label);
    }

    public void setTitle(String title){
	this.title = title;
	label.setText(title);
	repaint();
    }
}