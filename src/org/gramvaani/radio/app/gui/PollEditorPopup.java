package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.medialib.Poll;
import org.gramvaani.radio.medialib.PollOption;
import org.gramvaani.utilities.IconUtilities;
import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.utilities.StringUtilities;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.Document;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class PollEditorPopup extends JFrame {
    static int MAX_POLLNAME_LENGTH = 256;
    static int MAX_KEYWORD_LENGTH = 8;
    static int MAX_OPTIONNAME_LENGTH = 32;
    static int MAX_OPTIONCODE_LENGTH = 8;

    Poll editedPoll;
    PollOption editedOptions[];
    String[] existingKeywords;

    JTextField nameField, keywordField;
    JTextField optionName1 = new JTextField();
    JTextField optionCode1 = new JTextField();
    JTextField optionName2 = new JTextField();
    JTextField optionCode2 = new JTextField();
    JTextField optionName3 = new JTextField();
    JTextField optionCode3 = new JTextField();
    JTextField optionName4 = new JTextField();
    JTextField optionCode4 = new JTextField();
    JTextField optionName5 = new JTextField();
    JTextField optionCode5 = new JTextField();

    JTextField optionNames[] = {optionName1, optionName2, optionName3, optionName4, optionName5};
    JTextField optionCodes[] = {optionCode1, optionCode2, optionCode3, optionCode4, optionCode5};

    String defaultName = "Name";
    String defaultCode = "Code";

    ButtonGroup optionsTypeGroup;
    JRadioButton fixedOptionsButton, openOptionsButton;

    DocumentListener pollNameListener;
    DocumentListener keywordFieldListener;
    DocumentListener optionNameListener;
    DocumentListener optionCodeListener;
    
    Color defaultTextFieldColor, errorTextFieldColor;
    boolean isEdit;
    PollEditorListener listener;
    LogUtilities logger = new LogUtilities("PollEditorPopup");
    JPanel pollPanel;
    JLabel titleLabel;
    RSApp rsApp;

    public PollEditorPopup(PollEditorListener listener, int width, int height, Color bgColor, Color titleColor, StationConfiguration stationConfig, RSApp rsApp){
	this.listener = listener;
	this.rsApp    = rsApp;

	pollPanel = new JPanel();

	Dimension size = new Dimension(width, height);
	pollPanel.setBorder(BorderFactory.createLineBorder(bgColor.darker()));
	pollPanel.setPreferredSize(size);
	pollPanel.setLayout(new BorderLayout());

	int titleVertFrac = 10;
	int buttonPanelVertFrac = 10;
	int labelHorizFrac = 30;
	int labelVertFrac = 10;

	int titleHeight = titleVertFrac * size.height/100;
	int buttonPanelHeight = buttonPanelVertFrac * size.height/100;
	int fieldPanelHeight = size.height - titleHeight - buttonPanelHeight;

	Dimension titleSize = new Dimension(size.width, titleHeight);
	titleLabel = new RSLabel("");
	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	titleLabel.setPreferredSize(titleSize);
	titleLabel.setOpaque(true);
	titleLabel.setBackground(titleColor);
	pollPanel.add(titleLabel, BorderLayout.NORTH);
	
	Dimension fieldPanelSize = new Dimension(size.width, fieldPanelHeight);
	RSPanel fieldPanel = new RSPanel();
	fieldPanel.setBackground(bgColor);
	fieldPanel.setPreferredSize(fieldPanelSize);
	pollPanel.add(fieldPanel, BorderLayout.CENTER);

	Dimension buttonPanelSize = new Dimension(size.width, buttonPanelHeight);
	RSPanel buttonPanel = new RSPanel();
	buttonPanel.setBackground(bgColor);
	buttonPanel.setPreferredSize(buttonPanelSize);
	pollPanel.add(buttonPanel, BorderLayout.SOUTH);

	fieldPanel.setLayout(new GridBagLayout());
	
	int labelWidth = labelHorizFrac * size.width/100;
	int fieldWidth = size.width - labelWidth - 5;
	int labelHeight = labelVertFrac * fieldPanelHeight/100;

	Dimension labelSize = new Dimension(labelWidth, labelHeight);
	Dimension fieldSize = new Dimension(fieldWidth, labelHeight);

	int vertGap = 4;

	Insets insets = new Insets(0, 0, vertGap, 0);

	RSLabel nameLabel = new RSLabel("Poll Name: ");
	nameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(nameLabel, getGbc(0, 0, insets, gfillboth()));

	nameField = new JTextField();
	nameField.setPreferredSize(fieldSize);
	fieldPanel.add(nameField, getGbc(1, 0, insets));

	pollNameListener = new DocumentAdapter() {
		public void documentUpdated(DocumentEvent e) {
		    validatePollName();
		}
	    };
	nameField.getDocument().addDocumentListener(pollNameListener);

	RSLabel keywordLabel = new RSLabel("Poll Keyword: ");
	keywordLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(keywordLabel, getGbc(0, 1, insets, gfillboth()));

	keywordField = new JTextField();
	keywordField.setPreferredSize(fieldSize);
	fieldPanel.add(keywordField, getGbc(1, 1, insets));

	keywordFieldListener = new DocumentAdapter() {
		public void documentUpdated(DocumentEvent e){
		    validateKeyword();
		}
	    };
	keywordField.getDocument().addDocumentListener(keywordFieldListener);

	RSLabel typeLabel = new RSLabel("Poll Options Type: ");
	typeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(typeLabel, getGbc(0, 2, insets, gfillboth()));

	JPanel optionsTypePanel = new JPanel();
	optionsTypePanel.setBackground(bgColor);
	optionsTypePanel.setPreferredSize(fieldSize);
	optionsTypePanel.setLayout(new BoxLayout(optionsTypePanel, BoxLayout.X_AXIS));
	fieldPanel.add(optionsTypePanel, getGbc(1, 2, insets, gfillboth()));

	RSLabel emptyLabel = new RSLabel("     ");
	optionsTypePanel.add(emptyLabel);

	optionsTypeGroup = new ButtonGroup();
	fixedOptionsButton = new JRadioButton("Fixed");
	fixedOptionsButton.setBackground(bgColor);
	fixedOptionsButton.setSelected(true);
	optionsTypeGroup.add(fixedOptionsButton);
	optionsTypePanel.add(fixedOptionsButton);

	emptyLabel = new RSLabel("     ");
	optionsTypePanel.add(emptyLabel);

	openOptionsButton = new JRadioButton("Open");
	openOptionsButton.setBackground(bgColor);
	optionsTypeGroup.add(openOptionsButton);
	optionsTypePanel.add(openOptionsButton);

	optionNameListener = new DocumentAdapter() {
		public void documentUpdated(DocumentEvent e) {
		    validateOptionNames();
		}
	    };

	optionCodeListener = new DocumentAdapter() {
		public void documentUpdated(DocumentEvent e) {
		    validateOptionCodes();
		}
	    };
	
	RSLabel optionLabel1 = new RSLabel("Option 1: ");
	optionLabel1.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(optionLabel1, getGbc(0, 3, insets, gfillboth()));
	JPanel optionPanel1 = makeOptionPanel(optionName1, optionCode1, fieldSize.width, fieldSize.height, bgColor, 0);
	fieldPanel.add(optionPanel1, getGbc(1, 3, insets));

	RSLabel optionLabel2 = new RSLabel("Option 2: ");
	optionLabel2.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(optionLabel2, getGbc(0, 4, insets, gfillboth()));
	JPanel optionPanel2 = makeOptionPanel(optionName2, optionCode2, fieldSize.width, fieldSize.height, bgColor, 1);
	fieldPanel.add(optionPanel2, getGbc(1, 4, insets));

	RSLabel optionLabel3 = new RSLabel("Option 3: ");
	optionLabel3.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(optionLabel3, getGbc(0, 5, insets, gfillboth()));
	JPanel optionPanel3 = makeOptionPanel(optionName3, optionCode3, fieldSize.width, fieldSize.height, bgColor, 2);
	fieldPanel.add(optionPanel3, getGbc(1, 5, insets));

	RSLabel optionLabel4 = new RSLabel("Option 4: ");
	optionLabel4.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(optionLabel4, getGbc(0, 6, insets, gfillboth()));
	JPanel optionPanel4 = makeOptionPanel(optionName4, optionCode4, fieldSize.width, fieldSize.height, bgColor, 3);
	fieldPanel.add(optionPanel4, getGbc(1, 6, insets));

	RSLabel optionLabel5 = new RSLabel("Option 5: ");
	optionLabel5.setHorizontalAlignment(SwingConstants.RIGHT);
	fieldPanel.add(optionLabel5, getGbc(0, 7, insets, gfillboth()));
	JPanel optionPanel5 = makeOptionPanel(optionName5, optionCode5, fieldSize.width, fieldSize.height, bgColor, 4);
	fieldPanel.add(optionPanel5, getGbc(1, 7, insets));

	int buttonHorizFrac = 40;
	int buttonWidth = buttonHorizFrac * size.width/100 / 2;
	int buttonGap = 4;
	Dimension buttonSize = new Dimension(buttonWidth, buttonPanelSize.height - buttonGap * 2);
	int iconSize = (buttonSize.height * 8)/10;
	buttonPanel.setLayout(new GridBagLayout());

	RSButton saveButton = new RSButton("Save");
	saveButton.setPreferredSize(buttonSize);
	saveButton.setBackground(bgColor);
	saveButton.setIcon(IconUtilities.getIcon(StationConfiguration.SAVE_ICON, iconSize));
	saveButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    savePoll();
		}
	    });
	buttonPanel.add(saveButton);
	
	RSButton cancelButton = new RSButton("Cancel");
	cancelButton.setPreferredSize(buttonSize);
	cancelButton.setBackground(bgColor);
	cancelButton.setIcon(IconUtilities.getIcon(StationConfiguration.CANCEL_ICON, iconSize));
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    cancelEditor();
		}
	    });
							   
	buttonPanel.add(cancelButton);
	
	ActionListener submitListener = new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    savePoll();
		}
	    };

	KeyListener escapeListener = new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			cancelEditor();
		    }
		}
	    };

	
	for (JComponent c: new JComponent[]{pollPanel}){
	    c.addKeyListener(escapeListener);
	}

	addWindowFocusListener(new WindowAdapter(){
		public void windowLostFocus(WindowEvent e){
		    cancelEditor();
		}
	    });

	add(pollPanel);

	defaultTextFieldColor = keywordField.getBackground();
	errorTextFieldColor = stationConfig.getColor(StationConfiguration.ERROR_FIELD_COLOR);
	
	addOptionFieldListeners();
	setUndecorated(true);
	pack();
    }

    void addOptionFieldListeners() {
	for(JTextField optionName :optionNames) 
	    optionName.getDocument().addDocumentListener(optionNameListener);

	for(JTextField optionCode :optionCodes)
	    optionCode.getDocument().addDocumentListener(optionCodeListener);
    }

    void removeOptionFieldListeners() {
	for(JTextField optionName :optionNames) 
	    optionName.getDocument().removeDocumentListener(optionNameListener);

	for(JTextField optionCode :optionCodes)
	    optionCode.getDocument().removeDocumentListener(optionCodeListener);
    }

    JPanel makeOptionPanel(JTextField optionName, JTextField optionCode, int width, int height, Color bgColor, int index) {
	Dimension optionNameSize = new Dimension(width/2, height);
	Dimension optionCodeSize = new Dimension(width/3, height);

	JPanel optionPanel = new JPanel();
	optionPanel.setBackground(bgColor);
	optionPanel.setLayout(new BoxLayout(optionPanel, BoxLayout.X_AXIS));
	
        optionName.setPreferredSize(optionNameSize);
	optionName.setText(defaultName);
	optionPanel.add(optionName);

	RSLabel emptyLabel = new RSLabel("     ");
	optionPanel.add(emptyLabel);

	optionCode.setPreferredSize(optionCodeSize);
	optionCode.setText(defaultCode);
	optionPanel.add(optionCode);

	return optionPanel;
    }

    boolean isValidKeyword() {
	return validateKeyword();
    }

    boolean isValidOptionCodes() {
	return validateOptionCodes();
    }

    boolean isValidOptionNames() {
	return validateOptionNames();
    }

    boolean validatePollName() {
	if(!validateAlphaNumericSpace(nameField))
	    return false;

	if(!validateLength(nameField, MAX_POLLNAME_LENGTH))
	    return false;

	String text = nameField.getText();
	if(text.equals("")) {
	    nameField.setBackground(errorTextFieldColor);
	    return false;
	}

	nameField.setBackground(defaultTextFieldColor);
	return true;
    }

    boolean validateLength(JTextField field, int maxLength) {
	if(field.getText().length() > maxLength) {
	    field.setBackground(errorTextFieldColor);
	    return false;
	} else {
	    field.setBackground(defaultTextFieldColor);
	    return true;
	}
    }

    boolean validateKeyword() {
	if(!validateAlphaNumeric(keywordField))
	    return false;

	if(!validateLength(keywordField, MAX_KEYWORD_LENGTH))
	    return false;

	String keyword = keywordField.getText();
	for(String existingKeyword : existingKeywords) {
	    if(keyword.startsWith(existingKeyword) || existingKeyword.startsWith(keyword)) {
		keywordField.setBackground(errorTextFieldColor);
		return false;
	    }
	}

	keywordField.setBackground(defaultTextFieldColor);	
	return true;
    }

    boolean validateAlphaNumeric(JTextField field) {
	String text = field.getText();
	if(!StringUtilities.isAlphaNumeric(text)) {
	    field.setBackground(errorTextFieldColor);
	    return false;
	} else {
	    field.setBackground(defaultTextFieldColor);
	    return true;
	}
    }

    boolean validateAlphaNumericSpace(JTextField field) {
	String text = field.getText();
	if(!StringUtilities.isAlphaNumericSpace(text)) {
	    field.setBackground(errorTextFieldColor);
	    return false;
	} else {
	    field.setBackground(defaultTextFieldColor);
	    return true;
	}
    }

    boolean validateOptionCode(JTextField code) {
	if(!validateAlphaNumeric(code))
	    return false;

	if(!validateLength(code, MAX_OPTIONCODE_LENGTH))
	    return false;

	String text = code.getText();
	if(text.equals("") || text.equals(defaultCode)) {
	    code.setBackground(defaultTextFieldColor);
	    return true;
	}

	for(JTextField matchCode : optionCodes) {
	    String matchText = matchCode.getText();
	    if(code != matchCode && text.equals(matchText)) {
		code.setBackground(errorTextFieldColor);
		matchCode.setBackground(errorTextFieldColor);
		return false;
	    }
	}
		
	code.setBackground(defaultTextFieldColor);
	return true;
    }

    boolean validateOptionName(JTextField name) {
	if(!validateAlphaNumericSpace(name))
	    return false;

	if(!validateLength(name, MAX_OPTIONNAME_LENGTH))
	    return false;

	String text = name.getText();
	if(text.equals("") || text.equals(defaultName)) {
	    name.setBackground(defaultTextFieldColor);
	    return true;
	}

	for(JTextField matchName : optionNames) {
	    String matchText = matchName.getText();
	    if(name != matchName && text.equals(matchText)) {
		name.setBackground(errorTextFieldColor);
		matchName.setBackground(errorTextFieldColor);
		return false;
	    }
	}
		
	name.setBackground(defaultTextFieldColor);
	return true;
    }

    boolean validateOptionCodes() {
	boolean retval = true;
	for(JTextField codeField : optionCodes) {
	    retval = retval && validateOptionCode(codeField);
	}

	return retval;
    }

    boolean validateOptionNames() {
	boolean retval = true;
	for(JTextField nameField : optionNames) {
	    retval = retval && validateOptionName(nameField);
	}

	return retval;
    }

    boolean pollOptionsChanged() {
	boolean changed;

	for(PollOption option : editedOptions) {
	    changed = true;
	    for(int i = 0; i < optionNames.length; i++) {
		String name = optionNames[i].getText();
		String code = optionCodes[i].getText();

		if(name.equals(option.getPollOptionName()) && code.equals(option.getPollOptionCode()))
		    changed = false;
	    }

	    if(changed)
		return true;
	}

	for(int i = 0; i < optionNames.length; i++) {
	    String name = optionNames[i].getText();
	    String code = optionCodes[i].getText();
	    changed = true;
	    
	    if(name.equals("") || name.equals(defaultName) || code.equals("") || code.equals(defaultCode)) {
		changed = false;
		continue;
	    } 

	    for(PollOption option : editedOptions) {
		if(name.equals(option.getPollOptionName()) && code.equals(option.getPollOptionCode())) {
		    changed = false;
		    break;
		}
	    }

	    if(changed)
		return true;
	}

	return false;
    }

    void savePoll() {
	if(!isValidKeyword() || !isValidOptionCodes() || !isValidOptionNames())
	    return;

	int pollType = getPollType();
	int pollID;
	if (isEdit) {
	    pollID = editedPoll.getID();
	    listener.editPoll(pollID, nameField.getText(), keywordField.getText(), pollType, editedPoll.getState());
	} else {
	    pollID = listener.addPoll(nameField.getText(), keywordField.getText(), pollType, Poll.NOT_RUNNING);
	}

	if(pollType == Poll.OPEN_OPTIONS) {
	    for(PollOption option :editedOptions)
		listener.removePollOption(option.getID());
	} else if(pollType == Poll.FIXED_OPTIONS) {
	    if(pollOptionsChanged()) {
		for(PollOption option :editedOptions)
		    listener.removePollOption(option.getID());
		for(int i = 0; i < optionNames.length; i++) {
		    String name = optionNames[i].getText();
		    String code = optionCodes[i].getText();
		    if(!name.equals("") && !name.equals(defaultName) && !code.equals("") && !code.equals(defaultCode))
			listener.addPollOption(pollID, name, code);
		}
	    }
	}

	listener.saveDone();
	cancelEditor();
    }

    int getPollType() {
	if(fixedOptionsButton.isSelected())
	    return Poll.FIXED_OPTIONS;
	else if(openOptionsButton.isSelected())
	    return Poll.OPEN_OPTIONS;
	else 
	    return -1;
    }

    void cancelEditor(){
	setVisible(false);
    }

    void setEditedPoll(Poll poll, PollOption[] options){
	if (poll == null){
	    logger.error("SetEditedPoll: Setting null poll.");
	    return;
	}
	
	nameField.getDocument().removeDocumentListener(pollNameListener);
	nameField.setText(poll.getPollName());
	nameField.getDocument().addDocumentListener(pollNameListener);

	keywordField.getDocument().removeDocumentListener(keywordFieldListener);
	keywordField.setText(poll.getKeyword());
	keywordField.getDocument().addDocumentListener(keywordFieldListener);

	removeOptionFieldListeners();
	if(poll.getPollType() == Poll.FIXED_OPTIONS) {
	    fixedOptionsButton.setSelected(true);
	    int i = 0;
	    for(PollOption option :options) {
		optionNames[i].setText(option.getPollOptionName());
		optionCodes[i].setText(option.getPollOptionCode());
		i++;
	    }
	    while(i < optionNames.length) {
		optionNames[i].setText(defaultName);
		optionCodes[i].setText(defaultCode);
		i++;
	    }		
	} else if(poll.getPollType() == Poll.OPEN_OPTIONS) {
	    openOptionsButton.setSelected(true);
	    for(int i = 0; i < optionNames.length; i++) {
		optionNames[i].setText(defaultName);
		optionCodes[i].setText(defaultCode);
	    }
	}

	addOptionFieldListeners();

	editedPoll = poll;
	editedOptions = options;

	validatePollName();
	validateKeyword();
	validateOptionNames();
	validateOptionCodes();
    }

    void showEditor(boolean isEdit, Poll poll, PollOption[] options, String[] existingKeywords, JComponent assocComponent){
	this.isEdit = isEdit;
	
	Dimension editorSize = pollPanel.getPreferredSize();
	if(options == null) 
	    options = new PollOption[0];

	if (isEdit){
	    titleLabel.setText("Edit Poll");
	    
	    ArrayList<String> keywords = new ArrayList<String>();
	    for(String keyword :existingKeywords)
		if(!keyword.equals(poll.getKeyword()))
		    keywords.add(keyword);
	    
	    this.existingKeywords = keywords.toArray(new String[keywords.size()]);
	    setEditedPoll(poll, options);
	} else {
	    titleLabel.setText("New Poll");
	    this.existingKeywords = existingKeywords;
	    setEditedPoll(new Poll(), new PollOption[0]);
	}

	int xOffset = -(editorSize.width - assocComponent.getSize().width)/2;
	int yOffset = -(editorSize.height);
	Point p = assocComponent.getLocationOnScreen();

	int popupX = p.x + xOffset;
	int popupY = p.y + yOffset;

	setLocation(popupX, popupY);
	setVisible(true);
	pollPanel.requestFocusInWindow();
    }

    public interface PollEditorListener {
	public void editPoll(int pollID, String name, String keyword, int pollType, int state);
	public int addPoll(String name, String keyword, int pollType, int state);
	public void addPollOption(int pollID, String name, String code);
	public void removePollOption(int pollOptionID);
	public void saveDone();
	//public String[] getAllKeywords
    }

    /*
    class OptionField extends JTextField {

	String defaultText;

	public OptionField(String defaultText) {
	    super();
	    this.defaultText = defaultText;
	    
	}
    }
    */
}