package org.gramvaani.radio.app.gui;

import org.gramvaani.utilities.LogUtilities;

import java.awt.Insets;
import java.awt.GridBagConstraints;

class GBCUtilities {
    public static GBCGridWidth gridwidth(int x){
	return new GBCGridWidth (x);
    }

    public static GBCGridHeight gridheight(int y){
	return new GBCGridHeight (y);
    }

    public static GBCFill gfillh(){
	return new GBCFill(GridBagConstraints.HORIZONTAL);
    }

    public static GBCFill gfillv(){
	return new GBCFill(GridBagConstraints.VERTICAL);
    }
    
    public static GBCFill gfillboth(){
	return new GBCFill (GridBagConstraints.BOTH);
    }

    public static GBCAnchor ganchor(int anchor) {
	return new GBCAnchor (anchor);
    }

    public static GBCWeightX weightx(double x){
	return new GBCWeightX (x);
    }

    public static GBCWeightY weighty(double y){
	return new GBCWeightY (y);
    }

    public static GBCPadX padx(int x){
	return new GBCPadX (x);
    }

    public static GBCPadY pady(int y){
	return new GBCPadY (y);
    }

    public static GridBagConstraints getGbc (int x, int y, Object... params){
	GridBagConstraints c = new GridBagConstraints();
	c.gridx = x;
	c.gridy = y;
	
	for (Object obj: params){
	    if (obj instanceof GBCGridWidth) {
		c.gridwidth = ((GBCGridWidth) obj).intValue();
	    } else if (obj instanceof GBCGridHeight){
		c.gridheight = ((GBCGridHeight) obj).intValue();
	    } else if (obj instanceof GBCFill) {
		c.fill = ((GBCFill) obj).intValue();
	    } else if (obj instanceof GBCAnchor) {
		c.anchor = ((GBCAnchor) obj).intValue();
	    } else if (obj instanceof GBCWeightX) {
		c.weightx = ((GBCWeightX) obj).doubleValue();
	    } else if (obj instanceof GBCWeightY) {
		c.weighty = ((GBCWeightY) obj).doubleValue();
	    } else if (obj instanceof GBCPadX) {
		c.ipadx = ((GBCPadX) obj).intValue();
	    } else if (obj instanceof GBCPadY) {
		c.ipady = ((GBCPadY) obj).intValue();
	    } else if (obj instanceof Insets) {
		c.insets = (Insets) obj;
	    } else {
		LogUtilities.getDefaultLogger().error("GetGBC: Unknown type:"+obj);
	    }
	}

	return c;
    }
    
}