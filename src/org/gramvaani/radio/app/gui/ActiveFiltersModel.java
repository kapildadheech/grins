package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

public class ActiveFiltersModel implements ListModel, ActionListener {
    ArrayList<ActiveFilterWidget> activeFilters;
    ArrayList<ListDataListener> listeners;

    protected String sessionID;
    protected LibraryWidget libraryWidget;
    protected LibraryController libraryController;
    
    public ActiveFiltersModel(String sessionID, LibraryWidget libraryWidget, LibraryController libraryController) {
	activeFilters = new ArrayList<ActiveFilterWidget>();
	listeners = new ArrayList<ListDataListener>();

	this.sessionID = sessionID;
	this.libraryWidget = libraryWidget;
	this.libraryController = libraryController;
    }

    public void actionPerformed(ActionEvent e) {
	String actionCommand = e.getActionCommand();
	
	if(actionCommand.startsWith(ActiveFilterWidget.CLOSE + "_")) {

	    String filterName = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    ActiveFilterWidget filterWidget = activeFilters.get(getActiveFilterIndex(filterName));
	    SearchResult result = libraryController.deactivateFilter(sessionID, filterWidget.getSearchFilter());
	    libraryController.updateResults(result, sessionID);
	    removeActiveFilter(filterName);
	    libraryWidget.activateDisplayFilter(sessionID, filterName, true);
	}
    }

    
    public void addActiveFilter(ActiveFilterWidget activeFilter) {
	activeFilters.add(activeFilter);
	//System.out.println("*********** active filter index = " + getActiveFilterIndex(activeFilter.getFilterName()) + ":" + activeFilters.size() + ": adding");

	for(ListDataListener listener: listeners){
	    listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, activeFilters.size() - 1, activeFilters.size() - 1));
	}
    }

    public int getActiveFilterIndex(String filterName) {
	int i = 0;
	for(; i < activeFilters.size(); i++) {
	    if(activeFilters.get(i).getFilterName().equals(filterName)) {
		break;
	    }
	}
	if(i < activeFilters.size())
	    return i;
	else
	    return -1;
    }

    public ActiveFilterWidget getActiveFilterWidget(String filterName) {
	int index = getActiveFilterIndex(filterName);
	if(index != -1)
	    return activeFilters.get(index);
	else
	    return null;
    }

    public void removeActiveFilter(String filterName) {
	int i = getActiveFilterIndex(filterName);
	//System.out.println("********* active filter index = " + i + ": removing");
	if(i > -1) {
	    for(ListDataListener listener: listeners){
		listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, i, i));
	    }
	    activeFilters.remove(i);
	}
    }

    public void activateAllActiveFilters(boolean activate) {
	for(ActiveFilterWidget activeFilterWidget: activeFilters) {
	    activeFilterWidget.setEnabled(activate);
	}
    }
	
    public void addListDataListener(ListDataListener listener){
	listeners.add(listener);
    }
    
	
    public Object getElementAt(int index){
	return activeFilters.get(index);
    }
    
    public int getSize(){
	    return activeFilters.size();
    }
    
    public void removeListDataListener(ListDataListener listener){
	    listeners.remove(listener);
    }
    
}
