package org.gramvaani.radio.app.gui;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class RSToggleButton extends JToggleButton {
    Color normalBgColor;
    Color disabledBgColor;

    public RSToggleButton(Icon icon) {
	super(icon);
	normalBgColor = getBackground();
	disabledBgColor = Color.decode("0xdcdcdc");
    }
    
    public RSToggleButton(String str) {
	super(str);
	normalBgColor = getBackground();
	disabledBgColor = Color.decode("0xdcdcdc");
    }

    public RSToggleButton(String str, Icon icon){
	super(str, icon);
	normalBgColor = getBackground();
	disabledBgColor = Color.decode("0xdcdcdc");
    }

    public void setPressed(boolean pressed) {
	super.setSelected(pressed);
    }

    // this does not work within lists for some reason
    public void forceEnabled(boolean enabled) {
	super.setEnabled(enabled);
    }

    public void setEnabled(boolean enabled) {
	super.setEnabled(enabled);
	if (!enabled)
	    super.setBackground(disabledBgColor);
	else
	    super.setBackground(normalBgColor);
    }

    public void setBackground(Color color) {
	super.setBackground(color);
	normalBgColor = color;
    }

    public boolean isPressed(){
	return isSelected();
    }

}