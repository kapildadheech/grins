package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.text.DateFormat;

import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

class BottomMenuPanel extends JPanel implements ActionListener{
    
    Hashtable<RSWidgetBase, JToggleButton> widgetButtons;
    Hashtable<JToggleButton, RSWidgetBase> widgets;

    protected int widgetCount = 0;
    protected ControlPanelJFrame cpJFrame;
    protected StationConfiguration stationConfig;

    JPanel buttonPanel = new JPanel();
    JPanel timePanel = new JPanel();
    RSLabel timeLabel = new RSLabel("");

    Color bgColor;
    
    int height;

    public BottomMenuPanel(ControlPanelJFrame cpJFrame, int height){
	super();
	this.cpJFrame = cpJFrame;
	this.height = height;

	widgetButtons = new Hashtable<RSWidgetBase, JToggleButton>();
	widgets = new Hashtable<JToggleButton, RSWidgetBase>();
	stationConfig = cpJFrame.getControlPanel().getRSApp().getStationConfiguration();
	IconUtilities.setStationConfig(stationConfig);
	
	setLayout(new GridBagLayout());
	add(buttonPanel, getGbc(0, 0, gfillboth(), weightx(1.0)));
	buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
	bgColor = stationConfig.getColor(StationConfiguration.BOTTOMPANE_COLOR);
	setBackground(bgColor);
	buttonPanel.setBackground(bgColor);
	
	add(timeLabel, getGbc(1, 0, gfillboth()));
	timeLabel.setPreferredSize(new Dimension(cpJFrame.getASPwidth(), height));
	timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
	Font font = timeLabel.getFont();
	timeLabel.setFont(new Font(font.getFontName(), font.getStyle(), 32));
	timeLabel.setForeground(bgColor.darker().darker());
	Timer timer = new Timer(1000, new ActionListener(){
		DateFormat format = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.US);
		public void actionPerformed(ActionEvent e){
		    timeLabel.setText(format.format(new Date(System.currentTimeMillis())).split(" ")[0]);
		}

	    });
	timer.start();
    }

    public void addWidget(final RSWidgetBase widget){
	JToggleButton widgetButton = widget.getBottomMenuButton();
	widgetButtons.put(widget, widgetButton);
	widgets.put(widgetButton, widget);
	Dimension size = new Dimension(height, height);
	widgetButton.setSize(size);
	widgetButton.setPreferredSize(size);
	widgetButton.setMaximumSize(size);
	widgetButton.setMinimumSize(size);
	widgetButton.setToolTipText(widget.getBottomMenuButtonToolTip());

	widgetButton.setIcon(IconUtilities.getIcon(widget.getBottomMenuButtonIconID(), (int)(size.getWidth() * 0.8), (int)(size.getHeight() * 0.8)));
	widgetButton.setBackground(bgColor);

	buttonPanel.add(widgetButton);
	widgetButton.addActionListener(this);
	widgetButton.addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    maximizeWidget(widget);
		}
	    });
	
	widgetCount++;
	
	validate();
    }

    public void actionPerformed(ActionEvent e){
	JToggleButton button = (JToggleButton)e.getSource();

	if (!button.isSelected()){
	    button.setSelected(true);
	    return;
	}

	RSWidgetBase widget = widgets.get(button);
	cpJFrame.launchWidget(widget);
    }
    
    public void unloadWidget(RSWidgetBase widget){
	JToggleButton button = widgetButtons.get(widget);
	if (button != null)
	    button.setSelected(false);
    }

    protected void maximizeWidget(RSWidgetBase widget){
	if(widget.isLaunched())
	    cpJFrame.setWidgetMaximized(widget);
    }
}
