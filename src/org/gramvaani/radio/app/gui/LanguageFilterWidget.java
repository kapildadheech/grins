package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class LanguageFilterWidget extends ActiveFilterWidget implements ActionListener {
    //MediaLib medialib;

    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;

    static final String nullItem = "Select language";

    JComboBox languageList;
    StationConfiguration stationConfig;
    boolean ignoreUpdate = false;
    Color bgColor;

    public LanguageFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
				LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	stationConfig = libraryController.getStationConfiguration();
	//medialib = MediaLib.getMediaLib(libraryController.getStationConfiguration(), libraryController.getTimeKeeper());
	init();
    }

    protected void init() {
	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();

	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, filterVertSize * 2 + borderHeight));
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());
	add(titleLabel, getGbc(0, 0, weightx(100.0), gfillboth()));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));
	add(closeButton, getGbc(1, 0, gfillboth()));


	String[] languages = ((LanguageFilter)filter).getLanguages(libraryController.getMetadataProvider());
	String[] languagesOption = new String[languages.length + 1];
	languagesOption[0] = nullItem;
	int i = 1;
	for(String language: languages)
	    languagesOption[i++] = language;

	languageList = new JComboBox(languagesOption);
	languageList.addActionListener(this);
	languageList.setPreferredSize(new Dimension(filterHorizSize, filterVertSize));
	add(languageList, getGbc(0, 1, gridwidth(2), weightx(100.0), gfillboth()));

	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	setBackground(bgColor);

	closeButton.setBackground(bgColor);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	languageList.setBackground(bgColor);

	closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON,
						  filterVertSize, filterVertSize));

	//languageList.setRenderer(new FilterCellRenderer(bgColor.darker().darker()));
    }

    public void actionPerformed(ActionEvent e) {

	if(ignoreUpdate){
	    ignoreUpdate = false;
	    return;
	}

	String languageSelection = (String)(languageList.getSelectedItem());
	if(!languageSelection.equals(nullItem)) {
	    ((LanguageFilter)filter).activateAnchor(LanguageFilter.LANGUAGE_ANCHOR, languageSelection);
	    SearchResult result = libraryController.activateFilter(sessionID, filter);
	    libraryController.updateResults(result, sessionID);
	} else {
	    ((LanguageFilter)filter).activateAnchor(LanguageFilter.LANGUAGE_ANCHOR, "");
	    SearchResult result = libraryController.activateFilter(sessionID, filter);
	    libraryController.updateResults(result, sessionID);
	}
	if(languageSelection.startsWith("Select "))
	    languageList.setBackground(bgColor);
	else
	    languageList.setBackground(bgColor.brighter());

    }

    public void setEnabled(boolean activate) {
	super.setEnabled(activate);
	languageList.setEnabled(activate);
    }

    public void update(){
	Object language = languageList.getSelectedItem();

	removeAll();
	init();
	
	ignoreUpdate = true;
	languageList.setSelectedItem(language);
    }
}
