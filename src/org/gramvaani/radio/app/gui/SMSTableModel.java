package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.utilities.StringUtilities;
import org.gramvaani.radio.telephonylib.RSCallerID;

import javax.swing.table.DefaultTableModel;

import java.util.Hashtable;

public class SMSTableModel extends DefaultTableModel implements CacheObjectListener {
    static final int NUM_PER_QUERY = 50;
    
    int totalMessages = 0;
    
    Hashtable<Integer, SMS> messages = new Hashtable<Integer, SMS>();
    Hashtable<Integer, Integer> entityRow = new Hashtable<Integer, Integer>();
    
    String query = "";
    MetadataProvider metadataProvider;
    CacheProvider cacheProvider;
    LogUtilities logger = new LogUtilities("SMSTableModel");
    
    SMSTableModel(MetadataProvider metadataProvider, CacheProvider cacheProvider){
	super();
	this.metadataProvider = metadataProvider;
	this.cacheProvider = cacheProvider;
    }
    
    public Class getColumnClass(int columnIndex) {
        return String.class;
    }

    public int getColumnCount(){
	return 5;
    }
    
    public int getRowCount(){
	return totalMessages;
    }
    
    final String columnNames[] = new String[] {"Name", "Number", "Message", "Date Time", "Status"};
    
    public String getColumnName(int column){
	return columnNames[column];
    }
    
    public String getContactName(SMS message) {
	RSCallerID callerID = new RSCallerID(RSCallerID.PSTN, message.getPhoneNo());
	Entity[] potentialContacts = RSCallerID.getPotentialContacts(callerID, metadataProvider);
	if(potentialContacts.length == 0) 
	    return "";
	else
	    return potentialContacts[0].getName();
    }

    public Object getValueAt(int row, int column){
	fetchBatch(row);
	SMS message = messages.get(row);
	switch(column) {
	case 0:
	    return getContactName(message);
	case 1:
	    return message.getPhoneNo();
	case 2:
	    return message.getMessage();
	case 3:
	    return StringUtilities.getDateTimeString(message.getDateTime());
	case 4:
	    if(message.getType() == SMS.INCOMING)
		return SMS.RECV_STATUS.get(message.getStatus());
	    else
		return SMS.SEND_STATUS.get(message.getStatus());
	default:
	    return "";
	}
    }

    public SMS getMessageAt(int row){
	fetchBatch(row);
	return messages.get(row);
    }
    
    public boolean isCellEditable(int row, int col){
	return false;
    }
    
    String getWhereClause(){
	if (query == null || query.equals(""))
	    return "";
	
	return SMS.getMessageFieldName() + " like '%" +query+ "%' " + " OR " + 
	    SMS.getPhoneNoFieldName() + " like '%" +query+ "%' ";

	//XXXX also query name if present in entity
    }

    public void reloadTable(){
	clearData();
	Long numResults = (Long) metadataProvider.mathOp(Metadata.getClassName(SMS.class), getWhereClause(), "1", MetadataProvider.COUNT_OP);
	totalMessages = numResults.intValue();
	fireTableDataChanged();
    }
    
    public void search(String query){
	this.query = query;
	reloadTable();
    }
    
    public void clearData(){
	messages.clear();
	entityRow.clear();
    }
    
    void fetchBatch(int row){
	int baseRow = (row - row % NUM_PER_QUERY);
	if (messages.get(baseRow) != null)
	    return;
	
	SMS results[] = metadataProvider.get(new SMS(), getWhereClause(), new String[] {SMS.getDateTimeFieldName()}, new String[] {MetadataProvider.ORDER_ASC}, null, baseRow, NUM_PER_QUERY);
	int i = baseRow;
	
	for (SMS message: results){
	    messages.put(i, message);
	    Entity entity = message.getEntity(cacheProvider);
	    if(entity != null) {
		cacheProvider.setMetadataCacheObject(entity);
		entityRow.put(entity.getEntityID(), i);
	    }
	    i++;
	}
    }

    public void cacheObjectValueChanged(Metadata metadata){
	//XXXX update the entity in SMS list

	/*
	if (metadata instanceof Entity){
	    Entity entity = (Entity) metadata;
	    
	    Integer row = entityRow.get(entity.getEntityID());
	    
	    if (row == null){
		logger.error("CacheObjectValueChanged: Null object.");
		return;
	    }
	    
	    entities.put(row, entity);
	    fireTableRowsUpdated(row, row);
	}
	*/
    }
    
    public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKey){
	
    }
}