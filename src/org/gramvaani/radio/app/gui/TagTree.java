package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.medialib.Category;
import org.gramvaani.radio.medialib.ItemCategory;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.dnd.*;
import java.awt.event.*;
import java.awt.datatransfer.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.plaf.metal.MetalLookAndFeel;

import java.util.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class TagTree extends JTree implements DragGestureListener {

    static Hashtable<Integer, Category> tags = null;
    static Hashtable<Integer, HashSet<Category>> tagChildren = new Hashtable<Integer, HashSet<Category>>();
    static HashSet<TagTree> trees = new HashSet<TagTree>();
    
    HashSet<Category> selectedTags = new HashSet<Category>();
    Hashtable<TreeNode, Integer> selectedPaths = new Hashtable<TreeNode, Integer>();

    TagTreeListener listener;
    MetadataProvider metadataProvider;
    TagModel model;
    StationConfiguration stationConfig;
    static LogUtilities logger = new LogUtilities("TagTree");
    static final int ROW_HEIGHT = 25;

    JPopupMenu menu;
    TreePath selectedPath;

    static final String NEW_TAG_LABEL = "...edit tag...";
    static boolean cleanedUp = false;

    TagEditor editor;

    public TagTree(TagTreeListener listener, MetadataProvider metadataProvider, HashSet<Integer> selectedTagsID, StationConfiguration stationConfig){
	super();
	this.metadataProvider = metadataProvider;
	this.stationConfig = stationConfig;
	this.listener = listener;

	if (!cleanedUp){
	    cleanUp();
	}

	if (tags == null){
	    tags = new Hashtable<Integer, Category>();
	    for (Category tag: metadataProvider.get(new Category())){
		tags.put(tag.getNodeID(), tag);
		setChild(tag.getParentID(), tag);
	    }
	}

	for (int i: selectedTagsID){
	    Category tag = tags.get(i);
	    if (tag != null)
		selectedTags.add(tag);
	}
	
	setEditable(true);
	TagRenderer renderer = new TagRenderer();
	setCellRenderer(renderer);
	editor = new TagEditor(this, renderer);
	setCellEditor(editor);
	
	model = new TagModel(new DefaultMutableTreeNode());
	setModel(model);
	model.init();
	addTreeExpansionListener(model);
	model.setNodesExpanded();

	setRootVisible(false);
	setShowsRootHandles(true);
	setRowHeight(ROW_HEIGHT);
	setDragEnabled(true);

	int menuHeight = ROW_HEIGHT * 4/5;
	final JPopupMenu addTagPopup = new JPopupMenu("Add/Remove");
	final JMenuItem addItem = new JMenuItem("Add Tag", IconUtilities.getIcon(StationConfiguration.ADD_ICON, menuHeight, menuHeight));
	final JMenuItem removeItem = new JMenuItem("Remove Tag", IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, menuHeight, menuHeight));

	addTagPopup.add(addItem);
	addTagPopup.add(removeItem);

	addItem.addActionListener(new ActionListener(){
		public synchronized void actionPerformed(ActionEvent e){
		    int parentID = Category.ROOT_PARENT_ID;
		    if (selectedPath != null)
			parentID = ((Category)((DefaultMutableTreeNode)selectedPath.getLastPathComponent()).getUserObject()).getNodeID();
		    addNewCategoryAt(parentID);
		}
	    });

	removeItem.addActionListener(new ActionListener(){
		public synchronized void actionPerformed(ActionEvent e){
		    if (selectedPath == null)
			return;
		    removeCategory((Category)((DefaultMutableTreeNode)selectedPath.getLastPathComponent()).getUserObject());
		}
	    });

	addMouseListener(new MouseAdapter(){
		public synchronized void mousePressed(MouseEvent e){
		    int row = getRowForLocation(e.getX(), e.getY());
		    selectedPath = getPathForLocation(e.getX(), e.getY());
		    startEditingAtPath(selectedPath);

		    if (e.getButton() == MouseEvent.BUTTON1){
			return;
		    } 
		    if (selectedPath == null)
			removeItem.setEnabled(false);
		    else
			removeItem.setEnabled(true);

		    addTagPopup.show(e.getComponent(), e.getX(), e.getY());
		}

		public synchronized void mouseClicked(MouseEvent e){
		    int row = getRowForLocation(e.getX(), e.getY());
		    TreePath path = getPathForLocation(e.getX(), e.getY());
		    
		    if (!isEditing())
			startEditingAtPath(path);

		}
	    });


	TransferHandler transferHandler = new TransferHandler(){
		public int getSourceActions(JComponent c){
		    return COPY;
		}

		public Transferable createTransferable(JComponent c){
		    ArrayList<Category> list = new ArrayList<Category>();

		    for (TreePath path: getSelectionPaths()){
			list.add((Category)((DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject());
		    }

		    return new CategorySelection(list);
		}

		public boolean canImport(TransferHandler.TransferSupport support){
		    if (!support.isDrop())
			return false;
		    JTree.DropLocation dropLocation = (JTree.DropLocation)support.getDropLocation();
		    setSelectionPath(dropLocation.getPath());
		    return support.isDataFlavorSupported(CategorySelection.getFlavor());
		}
		
		@SuppressWarnings("unchecked")
		public boolean importData(TransferHandler.TransferSupport support){
		    try{
			JTree.DropLocation dropLocation = (JTree.DropLocation)support.getDropLocation();
			TreePath path = dropLocation.getPath();
			int newParentID = Category.ROOT_PARENT_ID;
			if (path != null){
			    Category newParentCategory = (Category)((DefaultMutableTreeNode)path.getLastPathComponent()).getUserObject();
			    newParentID = newParentCategory.getNodeID();
			}
			ArrayList<Category> list = (ArrayList<Category>)support.getTransferable().getTransferData(CategorySelection.getFlavor());
			Category category = list.get(0);
			moveCategory(category, newParentID);
			return true;
		    } catch (Exception e){
			logger.error("ImportData: Exception", e);
			return false;
		    }
		}

	    };
	
	setTransferHandler(transferHandler);
	DragSource dragSource = new DragSource();
	DragGestureRecognizer dragRecognizer = dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY, this);
    }

    public void cleanUp(){
	Category query = new Category();
	query.setLabel(NEW_TAG_LABEL);
	metadataProvider.remove(query);
    }

    public void	dragGestureRecognized(DragGestureEvent e){

    }

    public void searchStringUpdated(String query){
	model.searchStringUpdated(query);
    }

    protected void setExpandedState(TreePath path, boolean value){
	if (!value && selectedPaths.containsKey(path.getLastPathComponent()))
	    return;
	super.setExpandedState(path, value);
    }

    public Category[][] getSelectionClosure(){
	HashSet<Category[]> result = new HashSet<Category[]>();
	
	for (Category category: selectedTags){
	    HashSet<Category> set = new HashSet<Category>();
	    getClosure(category, set);
	    result.add(set.toArray(new Category[0]));
	}

	return result.toArray(new Category[][]{});
    }

    HashSet<Category> getClosure(Category category, HashSet<Category> result){
	result.add(category);
	HashSet<Category> children = tagChildren.get(category.getNodeID());
	if (children != null)
	    for (Category child: children){
		getClosure(child, result);
	    }

	return result;
    }

    public synchronized static void setTreeKeepAlive(TagTree tree, boolean flag){
	if (tree == null){
	    logger.error("SetTreeKeepAlive: null tree");
	    return;
	}
	if (flag)
	    trees.add(tree);
	else
	    trees.remove(tree);
    }

    void updateCategory(Category category, String text){
	text = StringUtilities.sanitize(text.toLowerCase());

	if (category.getLabel().equals(text))
	    return;

	Category update = category.clone();
	update.setLabel(text);
	
	Category query = new Category();
	query.setLabel(text);
	Category results[] = metadataProvider.get(query);
	
	if (results.length == 0){
	    metadataProvider.update(category, update);
	    category.setLabel(text);
	}

	for (TagTree t: getAllTrees())
	    t.categoryUpdated(category);
    }

    public void categoryUpdated(Category category){
	model.categoryUpdated(category);
    }

    void moveCategory(Category category, int newParentID){
	HashSet<Category> subtree = getClosure(category, new HashSet<Category>());
	if (tags.get(newParentID) != null && subtree.contains(tags.get(newParentID))){
	    return;
	}

	Category query = category.clone();
	tagChildren.get(category.getParentID()).remove(category);
	setChild(newParentID, category);

	category.setParentID(newParentID);
	metadataProvider.update(query, category);

	for (TagTree t: getAllTrees()){
	    t.categoryMoved(newParentID, query.getParentID(), category.getNodeID());
	}
    }

    void addNewCategoryAt(int parentID){
	if (parentID != Category.ROOT_PARENT_ID && tags.get(parentID).getLabel().equals(NEW_TAG_LABEL))
	    return;

	Category category = new Category(Long.toString(System.currentTimeMillis()), NEW_TAG_LABEL, parentID);
	final int newNodeID = metadataProvider.insert(category);
	Category newCategory = metadataProvider.getSingle(new Category(newNodeID));
	tags.put(newNodeID, newCategory);
	setChild(parentID, newCategory);
	for (TagTree t: getAllTrees()){
	    newCategoryAddedAt(parentID);
	}
	
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    model.startEditing(newNodeID);
		}
	    });
    }

    void newCategoryAddedAt(int parentID){
	model.newCategoryAddedAt(parentID);
    }

    void removeCategory(Category category){
	HashSet<Category> list = getClosure(category, new HashSet<Category>());

	int numItems = 0;

	for (Category c: list){
	    ItemCategory items[] = metadataProvider.get(new ItemCategory(c.getNodeID()));
	    numItems += items.length;
	}
	
	JFrame cpJFrame = ControlPanelJFrame.getInstance();

	String message = "Are you sure you want to delete tag: "+category.getLabel();
	if (list.size() > 1)
	    message += " and " + (list.size() - 1) + " subtag(s)";
	message += " from the system?";

	if (numItems > 0)
	    message += "\n There are "+numItems+" file(s) tagged with them.";

	int retVal = JOptionPane.showConfirmDialog(this, message, "Deleting tag", JOptionPane.YES_NO_OPTION);

	if (retVal != JOptionPane.YES_OPTION)
	    return;

	for (Category c: list){
	    metadataProvider.remove(new ItemCategory(c.getNodeID()));
	}

	tagChildren.get(category.getParentID()).remove(category);

	for (Category c: list){
	    metadataProvider.remove(c);
	    tagChildren.remove(c.getNodeID());
	    tags.remove(c.getNodeID());
	}

	for (TagTree t: getAllTrees()){
	    t.categoriesRemoved(category.getParentID(), list);
	}
    }

    void setChild(int parentID, Category child){
	if (tagChildren.get(parentID) == null)
	    tagChildren.put(parentID, new HashSet<Category>());
	tagChildren.get(parentID).add(child);
    }

    void categoriesRemoved(int parentID, HashSet<Category> categories){
	model.categoriesRemoved(categories);
	refreshChildren(parentID);
    }

    public void refreshChildren(int id){
	model.refreshChildren(id);
    }

    public void categoryMoved(int newParentID, int oldParentID, int id){
	model.categoryMoved(newParentID, oldParentID, id);
    }

    HashSet<TagTree> getAllTrees(){
	HashSet<TagTree> list = new HashSet<TagTree>(trees);
	trees.add(this);
	return list;
    }

    static final int CHECK_BOX_SEP = 0;//ROW_HEIGHT/3;
    static final int LABEL_SEP = 7;

    class TagEditor extends DefaultTreeCellEditor implements CellEditorListener {
	RSRoundedPanel panel = new RSRoundedPanel();
	JCheckBox box = new JCheckBox(new CheckBoxIcon(true));
	TagCheckBoxButtonModel buttonModel = new TagCheckBoxButtonModel();
	JTextField textField = new JTextField();

	Category currentCategory;
	final int MIN_TAG_LENGTH = 3;

	public TagEditor(JTree tree, DefaultTreeCellRenderer renderer){
	    super(tree, renderer);
	
	    box.setModel(buttonModel);

	    panel.setLayout(new GridBagLayout());
	    panel.add(box, getGbc(0, 0, padx(CHECK_BOX_SEP)));
	    panel.add(textField, getGbc(1, 0, gfillboth(), padx(LABEL_SEP), weightx(1.0)));
	    
	    box.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			stopCellEditing();
		    }
		});
	    textField.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			stopCellEditing();
		    }
		});
	    textField.setBorder(null);

	    addCellEditorListener(this);
	    box.setOpaque(false);
	    textField.setOpaque(false);
	    panel.setBackground(stationConfig.getColor(StationConfiguration.EDITED_TAG_BG_COLOR));
	    textField.setBackground(stationConfig.getColor(StationConfiguration.EDITED_TAG_BG_COLOR));
	}

	public boolean stopCellEditing(){
	    if (StringUtilities.sanitize(textField.getText()).length() < MIN_TAG_LENGTH) {
		return false;
	    } else
		return super.stopCellEditing();
	}

	public Object getCellEditorValue(){
	    return currentCategory;
	}

	public void editingCanceled(ChangeEvent e){
	    textField.setText("");
	}

	public void editingStopped(ChangeEvent e){
	    if (currentCategory != null){
		model.tagSelected(currentCategory, box.isSelected());
		updateCategory(currentCategory, textField.getText());
	    }
	}

	public Component getTreeCellEditorComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row){
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

	    
	    if (node.getUserObject() == null){
		box.setSelected(false);
		textField.setText("");
	    } else {
		currentCategory = (Category) node.getUserObject();
		buttonModel.setAutoSelected(model.isChildSelected(currentCategory));
		box.setSelected(model.isSelected(currentCategory));
		    
		String label = currentCategory.getLabel();
		textField.setText(label);
	    }
	    return panel;
	}
    }
    

    class TagRenderer extends DefaultTreeCellRenderer {
	RSRoundedPanel panel = new RSRoundedPanel();
	JCheckBox box = new JCheckBox(new CheckBoxIcon(false));
	JLabel label  = new JLabel();
	TagCheckBoxButtonModel buttonModel = new TagCheckBoxButtonModel();

	Color selectedColor = stationConfig.getColor(StationConfiguration.SELECTED_TAG_BG_COLOR);
	Color childSelectedColor = stationConfig.getColor(StationConfiguration.SELECTEDCHILD_TAG_BG_COLOR);
	Color currentColor = stationConfig.getColor(StationConfiguration.CURRENT_TAG_BG_COLOR);

	public TagRenderer(){
	    box.setModel(buttonModel);

	    panel.setLayout(new GridBagLayout());
	    panel.add(box, getGbc(0, 0, padx(CHECK_BOX_SEP)));
	    panel.add(label, getGbc(1, 0, padx(LABEL_SEP)));
	    box.setBorder(null);
	    box.setOpaque(false);
	    label.setOpaque(false);
	    panel.setOpaque(false);
	}

	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus){
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

	    if (node.getUserObject() == null){
		box.setSelected(false);
		label.setText("");
 	    } else {
		Category category = (Category) node.getUserObject();
		box.setSelected(model.isSelected(category));

		//label.setOpaque(true);
		panel.setOpaque(true);

		buttonModel.setAutoSelected(model.isChildSelected(category));
		
		if (selected) {
		    label.setBackground(currentColor);
		    panel.setBackground(currentColor);
		} else if (model.isSelected(category)) {
		    label.setBackground(selectedColor);
		    panel.setBackground(selectedColor);
		} else if (model.isChildSelected(category)){
		    label.setBackground(childSelectedColor);
		    panel.setBackground(childSelectedColor); 
		} else {
		    //label.setOpaque(false);
		    panel.setOpaque(false);
		}
		
		label.setText(category.getLabel());
	    }
	    return panel;
	}
	
    }

    class TagModel extends DefaultTreeModel implements TreeExpansionListener {
	DefaultMutableTreeNode root;
	Hashtable<Category, DefaultMutableTreeNode> categoryNodes = new Hashtable<Category, DefaultMutableTreeNode>();
	Hashtable<Category, Boolean> expansionStatus = new Hashtable<Category, Boolean>();
	HashSet<DefaultMutableTreeNode> matchedNodes = new HashSet<DefaultMutableTreeNode>();

	String query = "";
	final int MIN_QUERY_LENGTH = 7;

	TagModel(DefaultMutableTreeNode root) {
	    super(root);
	    this.root = root;
	}

	void init(){
	    loadRoot(false);
	    for (Category category: selectedTags)
		setSelected(category, true);
	    setNodesExpanded();
	    reload();
	}

	void loadRoot(boolean filter){
	    if (tagChildren.size() == 0)
		return;
	    ArrayList<Category> list = new ArrayList<Category> (tagChildren.get(Category.ROOT_PARENT_ID));
	    Collections.sort(list);
	    for (Category category: list){
		addCategoryToNode(category, root, filter);
	    }	    
	}
	
	void buildTree(Category category, DefaultMutableTreeNode parentNode, boolean filter){
	    HashSet<Category> children = tagChildren.get(category.getNodeID());
	    if (children == null)
		return;
	    ArrayList<Category> list = new ArrayList<Category>(children);
	    Collections.sort(list);

	    for (Category subCategory: list){
		boolean match = true;
		boolean subFilter = filter;

		DefaultMutableTreeNode childNode = getCategoryNode(subCategory);
		childNode.removeAllChildren();
		parentNode.add(childNode);

		if (subCategory.getLabel().toLowerCase().matches(query)){
		    subFilter = false;
		    matchedNode(childNode);
		}
		
		buildTree(subCategory, childNode, subFilter);
		if (!(subFilter && childNode.getChildCount() > 0 || !subFilter))
		    parentNode.remove(childNode);

	    }
	}

	void categoryMoved(int newParentID, int oldParentID, int id){
	    refreshChildren(newParentID);
	    refreshChildren(oldParentID);
	    setExpansion(tags.get(newParentID), true);
	    DefaultMutableTreeNode newParentNode, oldParentNode;
	    Category tmp = tags.get(newParentID);
	    if (tmp == null)
		newParentNode = root;
	    else
		newParentNode = categoryNodes.get(tmp);

	    tmp = tags.get(oldParentID);
	    
	    if (tmp == null)
		oldParentNode = root;
	    else
		oldParentNode = categoryNodes.get(tmp);

	    if (oldParentNode.getChildCount() == 0)
		setExpansion(tags.get(oldParentID), false);

	    if (isChildSelected(tags.get(id)) || isSelected(tags.get(id))){
		addSelectedPath(new ArrayList<TreeNode>(Arrays.asList(newParentNode.getPath())));
		removeSelectedPath(new ArrayList<TreeNode>(Arrays.asList(oldParentNode.getPath())));
	    }
	    reload(newParentNode);
	    reload(oldParentNode);
	}

	void newCategoryAddedAt(int parentID){
	    refreshChildren(parentID);
	    setExpansion(tags.get(parentID), true);
	    Category parent = tags.get(parentID);
	    DefaultMutableTreeNode node;
	    if (parent == null)
		node = root;
	    else
		node = categoryNodes.get(parent);
	    reload(node);
	}

	void categoriesRemoved(HashSet<Category> categories){
	    for (Category category: categories){
		tagSelected(category, false);
		expansionStatus.remove(category);
		DefaultMutableTreeNode node = categoryNodes.get(category);
		DefaultMutableTreeNode parent = (DefaultMutableTreeNode)node.getParent();
		if (parent != null){
		    parent.remove(node);
		}
		matchedNodes.remove(node);
		categoryNodes.remove(category);
	    }
	}

	void startEditing(int id){
	    TreePath path = new TreePath(getCategoryNode(tags.get(id)).getPath());
	    startEditingAtPath(path);
	}

	public void reload(final TreeNode node){
	    SwingUtilities.invokeLater(new Runnable(){
		    public void run(){
			TagModel.super.reload(node);
			setNodesExpanded(node);
		    }
		});
	}

	void refreshChildren(int id){
	    DefaultMutableTreeNode node;
	    Category category = tags.get(id);
	    if (category == null)
		node = root;
	    else
		node = getCategoryNode(category);
	    node.removeAllChildren();
	    ArrayList<Category> list = new ArrayList<Category>(tagChildren.get(id));
	    Collections.sort(list);

	    for (Category cat: list){
		node.add(getCategoryNode(cat));
	    }
	    reload(node);
	}


	DefaultMutableTreeNode getCategoryNode(Category category){
	    DefaultMutableTreeNode node = categoryNodes.get(category);
	    if (node == null){
		node = new DefaultMutableTreeNode(category);
		categoryNodes.put(category, node);
		if (isSelected(category))
		    setSelected(category, true);
	    }
	    return node;
	}

	void addCategoryToNode(Category category, DefaultMutableTreeNode node, boolean filter){
	    DefaultMutableTreeNode childNode = getCategoryNode(category);
	    childNode.removeAllChildren();
	    node.add(childNode);

	    if (category.getLabel().toLowerCase().matches(query)){
		filter = false;
		matchedNode(childNode);
	    }

	    buildTree(category, childNode, filter);
	    if (!(filter && (childNode.getChildCount() > 0) || !filter)){
		node.remove(childNode);
	    }
	}
	
	void matchedNode(DefaultMutableTreeNode node){
	    for (TreeNode tn: node.getPath()){
		if (tn == null || tn == root)
		    continue;
		Category category = (Category)((DefaultMutableTreeNode) tn).getUserObject();
		if (expansionStatus.get(category) == null || !expansionStatus.get(category)){
		    matchedNodes.add((DefaultMutableTreeNode)tn);
		}
	    }
	}

	boolean isClosureSelected(Category category){
	    HashSet<Category> closure = getClosure(category, new HashSet<Category>());
	    for (Category subCategory: closure)
		if (isSelected(subCategory))
		    return true;
	    return false;
	}

	boolean isChildSelected(Category category){
	    return selectedPaths.containsKey(categoryNodes.get(category));
	}

	boolean isSelected(Category category){
	    return selectedTags.contains(category);
	}
	
	void tagSelected(Category category, boolean selected){
	    if (category.getLabel().equals(NEW_TAG_LABEL))
		return;
	    boolean oldSelection = isSelected(category);
	    setSelected(category, selected);
	    if (oldSelection != selected && listener != null)
		listener.tagSelectionChanged(category, selected);
	}

	void categoryUpdated(final Category category){
	    if (listener != null)
		listener.tagEdited(category);
	    refreshChildren(category.getParentID());
	    SwingUtilities.invokeLater(new Runnable(){
		    public void run(){
			for (TreeNode node: categoryNodes.get(category).getPath())
			    nodeChanged(node);
		    }
		});
	}

	void setSelected(Category category, boolean selected){
	    if (category == null){
		logger.error("SetSelected: Null category");
		return;
	    }
	    
	    if (categoryNodes.get(category) == null){
		logger.error("SetSelected: No category node");
		return;
	    }

	    if (selected) {
		selectedTags.add(category);
		ArrayList<TreeNode> list = new ArrayList<TreeNode>(Arrays.asList(categoryNodes.get(category).getPath()));
		list.remove(list.size() - 1);
		addSelectedPath(list);
	    } else {
		selectedTags.remove(category);
		ArrayList<TreeNode> list = new ArrayList<TreeNode>(Arrays.asList(categoryNodes.get(category).getPath()));
		list.remove(list.size() - 1);
		removeSelectedPath(list);
	    }
	}

	void addSelectedPath(ArrayList<TreeNode> list){
	    for (TreeNode node: list){
		if (selectedPaths.get(node) == null)
		    selectedPaths.put(node, 1);
		else
		    selectedPaths.put(node, selectedPaths.get(node) + 1);
	    }
	}

	void removeSelectedPath(ArrayList<TreeNode> list){
	    for (TreeNode node: list){
		if (selectedPaths.get(node) == null)
		    continue;
		if (selectedPaths.get(node) == 1){
		    selectedPaths.remove(node);
		} else {
		    selectedPaths.put(node, selectedPaths.get(node) - 1);
		}
	    }
	}

	void searchStringUpdated(String query){
	    query = ".*"+query.trim()+".*";
	    if (query.equals(this.query))
		return;
	    boolean filter = true;
	    if (query.length() < MIN_QUERY_LENGTH){
		filter = false;
		query = "";
		if (this.query.equals(""))
		    return;
	    }
	    this.query = query;

	    root.removeAllChildren();
	    for (DefaultMutableTreeNode node: matchedNodes){
		Category category = (Category)node.getUserObject();
		if (!isSelected(category) && !isChildSelected(category))
		    setExpansion(category, false);
	    }
	    matchedNodes.clear();
	    loadRoot(filter);
	    reload();
	    setNodesExpanded();
	}

	void setNodesExpanded(){
	    for (Category category: expansionStatus.keySet()){
		if (expansionStatus.get(category)){
		    setExpandedState(new TreePath(categoryNodes.get(category).getPath()), expansionStatus.get(category));
		}
	    }

	    for (DefaultMutableTreeNode node: matchedNodes){
		setExpandedState(new TreePath(node.getPath()), true);
	    }
	    
	    for (Category category: selectedTags){
		makeVisible(new TreePath(categoryNodes.get(category).getPath()));
	    }
	}

	void setNodesExpanded(TreeNode root){
	    Category rootCategory = (Category)((DefaultMutableTreeNode) root).getUserObject();
	    HashSet<Category> list = new HashSet<Category>();
	    if (rootCategory == null){
		list.addAll(tags.values());
	    } else {
		getClosure(rootCategory, list);
	    }

	    for (Category category: list){
		if (expansionStatus.get(category) != null && expansionStatus.get(category)){
		    setExpandedState(new TreePath(categoryNodes.get(category).getPath()), expansionStatus.get(category));
		}
	    }
	    
	    for (DefaultMutableTreeNode node: matchedNodes){
		Category category = (Category) node.getUserObject();
		if (list.contains(category)){
		    setExpandedState(new TreePath(node.getPath()), true);
		}
	    }
	    
	    
	    for (Category category: selectedTags){
		if (list.contains(category))
		    makeVisible(new TreePath(categoryNodes.get(category).getPath()));
	    }
	}

	void setExpansion(Category category, boolean flag){
	    if (category == null)
		return;
	    expansionStatus.put(category, flag);
	}

	public void treeExpanded(TreeExpansionEvent e){
	    setExpansion((Category)((DefaultMutableTreeNode) e.getPath().getLastPathComponent()).getUserObject(), true);
	}

	public void treeCollapsed(TreeExpansionEvent e){
	    Category category = (Category)((DefaultMutableTreeNode) e.getPath().getLastPathComponent()).getUserObject();
	    if (!isSelected(category))
		setExpansion(category, false);
	}

    }
    
    public class TagCheckBoxButtonModel extends JToggleButton.ToggleButtonModel {
	boolean isAutoSelected = false;

	public void setAutoSelected(boolean selected){
	    isAutoSelected = selected;
	}

	public boolean getAutoSelected(){
	    return isAutoSelected;
	}
    }

}