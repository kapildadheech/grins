package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.*;

import java.util.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.synth.*;

import java.io.*;

public class ControlPanel {
    protected ControlPanelJFrame cpJFrame;
    protected int screenX, screenY;

    protected LogUtilities logger;
    protected RSApp rsApp;

    protected PlaylistWidget playlistWidget = null;
    protected SimulationWidget simulationWidget = null;

    public static int SCREEN_X = 1920;
    public static int SCREEN_Y = 1200;
    public static int FONT_SIZE = 18;

    static final boolean isX11;
    
    static {
	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
	isX11 = (ge.getClass().getCanonicalName().equals("sun.awt.X11GraphicsEnvironment"));
    }

    public ControlPanel(LogUtilities logger, RSApp rsApp) {
	this.logger = logger;
	this.rsApp = rsApp;

	Dimension dimension = getScreenSize();

	screenX = dimension.width;
	screenY = dimension.height;

	ErrorInference.getErrorInference(rsApp.getStationConfiguration());

	init();
	//display();
    }

    Dimension getScreenSize() {
	
	if (isX11)
	    return getX11ScreenSize();
	else
	    return getDefaultScreenSize();
	
	//return new Dimension(1024, 600);
    }

    Dimension getDefaultScreenSize() {
	Insets insets = Toolkit.getDefaultToolkit().getScreenInsets((new JFrame()).getGraphicsConfiguration());
	Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
	
	size.width -= insets.left + insets.right;
	size.height -= insets.top + insets.bottom;

	return size;
    }

    Dimension getX11ScreenSize() {
	ProcessBuilder xpropProcess = new ProcessBuilder("xprop", "-root", "-notype", "_NET_WORKAREA");
	try {
	    Process process = xpropProcess.start();
	    BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));

	    String[] props = in.readLine().split("=")[1].split(",");
	    
	    int width = Integer.parseInt(props[2].trim());
	    int height = Integer.parseInt(props[3].trim());

	    return new Dimension(width, height);

	} catch (Exception e){

	    return getDefaultScreenSize();
	}

    }

    public RSApp getRSApp() {
	return rsApp;
    }

    public void init() {
	try{
	    
	    UIManager.setLookAndFeel("com.jgoodies.looks.plastic.PlasticXPLookAndFeel");
	    
	    UIManager.put("ScrollBar.width", 15);
	    UIManager.put("ToggleButton.select", rsApp.getStationConfiguration().getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR).darker());

	    int fontSize = (int)Math.min((double)screenX * FONT_SIZE / SCREEN_X, (double)screenY * FONT_SIZE / SCREEN_Y);
	    javax.swing.plaf.FontUIResource f = new javax.swing.plaf.FontUIResource("Tahoma", Font.PLAIN, fontSize);
	    
	    Enumeration e = UIManager.getDefaults().keys();
	    while (e.hasMoreElements()) {
		Object key = e.nextElement();
		Object value = UIManager.get(key);

		if (value instanceof javax.swing.plaf.FontUIResource) {
		    UIManager.put(key, f);
		}
	    }

	} catch (Exception e){
	    logger.error("Init: Could not initialize custom look and feel.", e);
	}

	cpJFrame = new ControlPanelJFrame(this);
    }

    public void display() {
	cpJFrame.setVisible(true);
    }

    public void bringToFront(){
	cpJFrame.toFront();
    }

    public int getScreenXsize() {
	return screenX;
    }

    public int getScreenYsize() {
	return screenY;
    }
    
    public LogUtilities getLogger() {
	return logger;
    }

    public void initPlaylistWidget(PlaylistWidget playlistWidget) {
	this.playlistWidget = playlistWidget;
	cpJFrame.getPlaylistPanel().removeAll();
	playlistWidget.setPanel(cpJFrame.getPlaylistPanel(), cpJFrame.getPlaylistPanelDim());
	cpJFrame.getPlaylistPanel().repaint();
	cpJFrame.setSimulationMode(false);
    }

    public PlaylistWidget getPlaylistWidget() {
	return playlistWidget;
    }

    public void initSimulationWidget(SimulationWidget simulationWidget) {
	this.simulationWidget = simulationWidget;
	cpJFrame.getPlaylistPanel().removeAll();
	simulationWidget.setPanel(cpJFrame.getPlaylistPanel(), cpJFrame.getPlaylistPanelDim());
	cpJFrame.getPlaylistPanel().repaint();
	cpJFrame.setSimulationMode(true);
    }

    public SimulationWidget getSimulationWidget() {
	return simulationWidget;
    }

    public void initWidget(RSWidgetBase widget){
	cpJFrame.setupWidget(widget, true);
    }

    public void initWidget(RSWidgetBase widget, boolean initBottomPanel){
	cpJFrame.setupWidget(widget, initBottomPanel);
    }

    public void setDefaultWidget(RSWidgetBase widget){
	cpJFrame.setDefaultWidget(widget);
    }

    public void displayError(String errorMessage) {
	// XXX
	logger.warn(errorMessage);
    }

    public ControlPanelJFrame cpJFrame() {
	return cpJFrame;
    }

}