package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public abstract class ActiveFilterWidget extends JPanel {
    public static final String CLOSE = "CLOSE";

    SearchFilter filter;
    ActiveFiltersModel activeFiltersModel;
    LibraryController libraryController;
    LibraryWidget libraryWidget;
    String sessionID;
    RSLabel titleLabel;
    RSButton closeButton;
    int borderHeight;

    public ActiveFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
			      LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	this.filter = filter;
	this.activeFiltersModel = activeFiltersModel;
	this.libraryController = libraryController;
	this.libraryWidget = libraryWidget;
	this.sessionID = sessionID;
	titleLabel = new RSLabel(filter.getFilterLabel());
	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	closeButton = new RSButton("");
	closeButton.setActionCommand(CLOSE + "_" + filter.getFilterName());
	closeButton.addActionListener(activeFiltersModel);
	setBorder(new GradientBorder(Color.black));
	borderHeight = 1;
    }

    public String getFilterName() {
	return filter.getFilterName();
    }

    public SearchFilter getSearchFilter() {
	return filter;
    }

    public void setEnabled(boolean activate) {
	closeButton.setEnabled(activate);
    }

    public abstract void update();
}