package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.telephonylib.RSCallerID;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;


import java.util.*;

import static org.gramvaani.utilities.IconUtilities.getIcon;
import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class MetadataWidget extends RSWidgetBase implements CacheObjectListener, TagTreeListener, EntitySelector.EntityListener {
    public static String EDIT = "EDIT";
    public static String LANGUAGE = "LANGUAGE";
    public static String ADDCATEGORY = "ADDCATEGORY";
    public static String REMOVECATEGORY = "REMOVECATEGORY";
    public static String SAVE = "SAVE";
    public static String NEXT = "NEXT";
    public static String PREV = "PREV";

    public static String UNABLE_TO_ADD_ITEM = "Unable to add item";

    MetadataController metadataController;
    StationConfiguration stationConfig;

    protected RSApp rsApp;
    protected String[] selectedPrograms;
    protected boolean editmode;
    protected boolean groupmode;

    protected JTextField titleField;
    protected JTextArea descField;
    protected JTextField tagsField;
    protected RSLabel durationField;
    protected RSLabel creationField;
    protected JComboBox languageDropDown;
    protected RSLabel typeField;
    protected JComboBox categoryDropDown;
    protected RSButton addCategoryButton;
    protected RSButton removeCategoryButton;
    protected JPanel buttonPane;
    protected RSLabel feedbackLabel;
    protected RSButton prevButton;
    protected RSButton nextButton;
    protected RSButton editButton;
    protected RSButton saveButton;

    protected JScrollPane creatorScrollPane;
    protected JScrollPane categoryScrollPane;

    JFrame tagsPopup;
    TagTree tagTree;
    JTable contactsTable;
    ContactsTableModel tableModel;
    EntityEditorPopup editorPopup;

    protected StatsDisplay longHistoryDisplay;
    protected StatsDisplay shortHistoryDisplay;
    protected CacheProvider cacheProvider;

    static Hashtable<String[], MetadataWidget> metadataWidgetInstances = new Hashtable<String[], MetadataWidget>();

    JComboBox entityTypeField;
    Entity selectedEntity = null;
    Creator selectedCreator = null;
    Entity entityResults[] = new Entity[0];
    int prevEntityIndex = -1;

    //String NULL_AFFILIATION = "<html><font color=c0c0c0>None</font></html>";
    
    RSPanel creatorListPanel;

    int creatorWidth, creatorHeight;
    Color bgColor, defaultTextFieldColor, errorTextFieldColor;

    static final String FIELD_NAME 	= "FIELD_NAME";
    static final String FIELD_LOCATION 	= "FIELD_LOCATION";
    static final String FIELD_PHONE 	= "FIELD_PHONE";
    static final String FIELD_PHONE_TYPE= "FIELD_PHONE_TYPE";
    static final String FIELD_AFFILIATION = "FIELD_AFFILIATION";
    

    CaretListener locationFieldCaretListener, numberFieldCaretListener;
    DocumentListener nameFieldDocumentListener;
    ActionListener nameFieldActionListener, numberTypeFieldActionListener, entityTypeFieldActionListener;

    public synchronized static MetadataWidget getMetadataWidget(RSApp rsApp, String[] selectedPrograms, boolean editmode) {

	for (String[] widgetArr: metadataWidgetInstances.keySet()){
	    if (arrayCompare(widgetArr, selectedPrograms))
		return metadataWidgetInstances.get(widgetArr);
	}

	MetadataWidget metadataWidget = new MetadataWidget(rsApp, selectedPrograms, editmode);
	metadataWidgetInstances.put(selectedPrograms, metadataWidget);
	return metadataWidget;
    }

    static boolean arrayCompare(String[] a, String[] b) {
	boolean testFailed = false;

	if (a.length == b.length) {
	    int i = 0;
	    for (String astr: a) {
		if (!astr.equals(b[i])) {
		    testFailed = true;
		    break;
		}
		i++;
	    }
	    return !testFailed;
	} else {
	    return false;
	}
    }

    public MetadataWidget(RSApp rsApp, String[] selectedPrograms, boolean editmode) {
	super("Metadata Information");
	
	this.rsApp = rsApp;
	this.selectedPrograms = selectedPrograms;
	this.editmode = editmode;
	if (selectedPrograms.length > 1)
	    groupmode = true;
	else
	    groupmode = false;
	
	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	metadataController = new MetadataController(this, rsApp.getControlPanel(), rsApp);	
	stationConfig = rsApp.getStationConfiguration();
	cacheProvider = (CacheProvider)rsApp.getProvider(RSApp.CACHE_PROVIDER);
    }

    public boolean onLaunch() {
	metadataController.init();
	metadataController.setCurrentActiveProgram(editmode, groupmode);

	buildWorkspace(editmode, groupmode);
	reinitAspComponent();
	metadataController.initFields(editmode, groupmode);
	metadataController.updateGUIButtons();
	registerCacheObjects();

	return true;
    }

    public DataFlavor[] getASPDataFlavors(){
	return new DataFlavor[] {EntitySelection.getFlavor()};
    }


    public DataFlavor[] getWPDataFlavors(){
	return new DataFlavor[] {EntitySelection.getFlavor()};
    }

    static final int leftColTitleHorizFrac = 10;
    static final int leftColTextHorizFrac = 45;
    static final int rightColTitleHorizFrac = 10;
    static final int rightColTextHorizFrac = 15;
    static final int extraColHorizFrac = 14;
    static final int middleColHorizFrac = 5;

    static final int defaultRowHeightFrac = 4;

    private void registerCacheObjects() {
	RadioProgramMetadata[] metadata = metadataController.getProgramsMetadata();
	if (metadata == null)
	    return;
	for (RadioProgramMetadata program : metadata) {
	    cacheProvider.addCacheObjectListener(program, this);
	}
    }

    private void unregisterCacheObjects() {
	RadioProgramMetadata[] metadata = metadataController.getProgramsMetadata();
	if (metadata == null)
	    return;
	for (RadioProgramMetadata program : metadata) {
	    cacheProvider.removeCacheObjectListener(program, this);
	}
    }

    public void cacheObjectValueChanged(Metadata metadata) {

	if (metadata instanceof Entity){
	    final Entity entity = (Entity) metadata;
	    
	    /* Hash, Selection, SearchResult, NameDropDown, CreatorList, MetadataController */
	    
	    for (int i = 0; i < tableModel.getRowCount(); i++){
		Creator c = tableModel.getCreatorAt(i);
		if (c.getEntity().getEntityID() == entity.getEntityID()){
		    c.setEntity(entity);
		    tableModel.refreshRow(i);
		}
	    }
	}
	

	if (metadata instanceof RadioProgramMetadata) {
	    RadioProgramMetadata newProgram = (RadioProgramMetadata) metadata;

	    if (!newProgram.getItemID().equals(metadataController.getCurrentProgramMetadata()))
		return;

	    //System.err.println("new: "+newProgram);
	    //System.err.println("old: "+metadataController.getCurrentProgramMetadata());

	    if (newProgram.equals(metadataController.getCurrentProgramMetadata()))
		return;

	    //System.err.println("--- prog changed.");
	}
	
    }

    public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKey) {

	for (Metadata m : metadata) {
	    RadioProgramMetadata programMetadata = (RadioProgramMetadata) m;
	    RadioProgramMetadata[] mdata = metadataController.getProgramsMetadata();
	    for (RadioProgramMetadata program : mdata) {
		if (program.getItemID().equals(programMetadata.getItemID())) {
		    rsApp.getControlPanel().cpJFrame().unloadWidget(this);
		    return;
		}
	    }
	}
    }

    private void buildWorkspace(boolean editmode, boolean groupmode) {
	GridBagLayout layout;
	wpComponent.setLayout(layout = new GridBagLayout());

	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR);
	Color buttonBgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	Color widgetBgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	Color controlBgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	Color controlButtonBgColor = stationConfig.getColor(StationConfiguration.NEXTPREV_BUTTON_COLOR);
	wpComponent.setBackground(bgColor);

	int leftColTitleWidth = (int)(wpSize.getWidth() * leftColTitleHorizFrac / 100.0);
	int leftColTextWidth = (int)(wpSize.getWidth() * leftColTextHorizFrac / 100.0);
	int rightColTitleWidth = (int)(wpSize.getWidth() * rightColTitleHorizFrac / 100.0);
	int rightColTextWidth = (int)(wpSize.getWidth() * rightColTextHorizFrac / 100.0);
	int extraColWidth = (int)(wpSize.getWidth() * extraColHorizFrac / 100.0);
	int middleColWidth = (int)(wpSize.getWidth() * middleColHorizFrac / 100.0);
	int rowWidth = leftColTitleWidth + leftColTextWidth + rightColTitleWidth + rightColTextWidth + extraColWidth + middleColWidth;
	int scrollBarHeight = (new JScrollBar(JScrollBar.HORIZONTAL)).getMaximumSize().height;
	int defaultRowHeight = (int)((wpSize.getHeight() - (2 * scrollBarHeight)) * defaultRowHeightFrac / 100.0);
	int leftOverHeight = (int)(wpSize.getHeight() - (2 * scrollBarHeight)) % (int)(100 / defaultRowHeightFrac);
	int smallButtonWidth = leftColTitleWidth / 2;

	layout.columnWidths = new int[]{leftColTitleWidth, leftColTextWidth, extraColWidth, middleColWidth, rightColTitleWidth, rightColTextWidth};

	// 0,0
	RSLabel titleLabel = new RSLabel("Title");
	titleLabel.setPreferredSize(new Dimension(leftColTitleWidth, defaultRowHeight));
	wpComponent.add(titleLabel, getGbc(0, 0));

	// 1,0
	titleField = new JTextField();
	titleField.setPreferredSize(new Dimension(leftColTextWidth + extraColWidth, defaultRowHeight));
	wpComponent.add(titleField, getGbc(1, 0, gridwidth(2)));

	// 0,1
	RSLabel descLabel = new RSLabel("Description");
	descLabel.setPreferredSize(new Dimension(leftColTitleWidth, defaultRowHeight * 2));
	wpComponent.add(descLabel, getGbc(0, 1, gridheight(2)));

	// 1,1
	descField = new JTextArea();
	descField.setRows(2); 
	descField.setLineWrap(true);
	
	JScrollPane descPane = new JScrollPane(descField, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	descPane.setPreferredSize(new Dimension(leftColTextWidth + extraColWidth, defaultRowHeight * 2));
	wpComponent.add(descPane, getGbc(1, 1, gridwidth(2), gridheight(2)));

	// 3,0
	RSLabel blankLabel = new RSLabel("");
	blankLabel.setPreferredSize(new Dimension(middleColWidth, defaultRowHeight * 4));
	wpComponent.add(blankLabel, getGbc(3, 0, gridheight(4)));
	
	
	// 4,0
	RSLabel durationLabel = new RSLabel("Duration");
	durationLabel.setPreferredSize(new Dimension(rightColTitleWidth, defaultRowHeight));
	wpComponent.add(durationLabel, getGbc(4, 0));

	// 5,0
	durationField = new RSLabel("");
	durationField.setPreferredSize(new Dimension(rightColTextWidth, defaultRowHeight));
	wpComponent.add(durationField, getGbc(5, 0));
	
	// 4,1
	RSLabel creationLabel = new RSLabel("Created");
	creationLabel.setPreferredSize(new Dimension(rightColTitleWidth, defaultRowHeight));
	wpComponent.add(creationLabel, getGbc(4, 1));

	// 5,1
	creationField = new RSLabel("");
	creationField.setPreferredSize(new Dimension(rightColTextWidth, defaultRowHeight));
	wpComponent.add(creationField, getGbc(5, 1));

	// 4,2
	RSLabel typeLabel = new RSLabel("Type");
	typeLabel.setPreferredSize(new Dimension(rightColTitleWidth, defaultRowHeight));
	wpComponent.add(typeLabel, getGbc(4, 2));

	// 5,2
	typeField = new RSLabel("");
	typeField.setPreferredSize(new Dimension(rightColTextWidth, defaultRowHeight));
	wpComponent.add(typeField, getGbc(5, 2));

	// 0,3
	RSLabel tagsLabel = new RSLabel("Tags");
	tagsLabel.setPreferredSize(new Dimension(leftColTitleWidth, defaultRowHeight));
	wpComponent.add(tagsLabel, getGbc(0, 3));

	// 1,3
	tagsField = new JTextField();
	tagsField.setPreferredSize(new Dimension(leftColTextWidth + extraColWidth, defaultRowHeight));
	wpComponent.add(tagsField, getGbc(1, 3, gridwidth(2)));
	tagsField.setEditable(false);
	tagsField.setOpaque(true);
	tagsField.setBackground(Color.white);
	tagsField.addMouseListener(new MouseAdapter(){
		public synchronized void mousePressed(MouseEvent e){
		    showTagPopup();
		}
	    });

	// 4,3
	RSLabel languageLabel = new RSLabel("Language");
	languageLabel.setPreferredSize(new Dimension(rightColTitleWidth, defaultRowHeight));
	wpComponent.add(languageLabel, getGbc(4, 3));

	// 5,3
	languageDropDown = new JComboBox();
	languageDropDown.setPreferredSize(new Dimension(rightColTextWidth, defaultRowHeight));
	wpComponent.add(languageDropDown, getGbc(5, 3));
	languageDropDown.setBackground(buttonBgColor);

	// 0,4
	JPanel historyPanel = new JPanel();
	wpComponent.add(historyPanel, getGbc(0, 4, gridwidth(6)));

	historyPanel.setLayout(new GridBagLayout());
	historyPanel.setBackground(bgColor);
	
	blankLabel = new RSLabel("");
	blankLabel.setPreferredSize(new Dimension(rowWidth, (int)(defaultRowHeight * 0.5)));
	historyPanel.add(blankLabel, getGbc(0, 0, weighty(100.0)));

	RSLabel historyLabel = new RSLabel("Playout history");
	historyLabel.setPreferredSize(new Dimension(rowWidth, defaultRowHeight));
	historyLabel.setBackground(widgetBgColor);
	historyLabel.setOpaque(true);
	historyPanel.add(historyLabel, getGbc(0, 1));


	Dimension statsDim;
	shortHistoryDisplay = new StatsDisplay(metadataController.getMetadataProvider(), null,
					       statsDim = new Dimension(rowWidth, (int)(defaultRowHeight * 10)));
	shortHistoryDisplay.setPreferredSize(statsDim);
	shortHistoryDisplay.setMinimumSize(statsDim);
	//historyPanel.add(shortHistoryDisplay, getGbc(0, 1));

	shortHistoryDisplay.setBackground(bgColor);
	shortHistoryDisplay.setBorder(BorderFactory.createLineBorder(stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR)));

	blankLabel = new RSLabel("");
	blankLabel.setPreferredSize(new Dimension(rowWidth, (int)(defaultRowHeight * 0.1)));
	historyPanel.add(blankLabel, getGbc(0, 2, weighty(100.0)));

	longHistoryDisplay = new StatsDisplay(metadataController.getMetadataProvider(), null,
					      statsDim);
	longHistoryDisplay.setPreferredSize(statsDim);
	longHistoryDisplay.setMinimumSize(statsDim);

	historyPanel.add(longHistoryDisplay, getGbc(0, 3));

	longHistoryDisplay.setBackground(bgColor);
	longHistoryDisplay.setBorder(BorderFactory.createLineBorder(stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR)));

	blankLabel = new RSLabel("");
	blankLabel.setPreferredSize(new Dimension(rowWidth, (int)(defaultRowHeight * 0.5)));
	historyPanel.add(blankLabel, getGbc(0, 4, weighty(100.0)));
	

	// 0,10
	JPanel contactsPanel = getContactsPanel(rowWidth, defaultRowHeight * 7, bgColor, widgetBgColor, defaultRowHeight, defaultRowHeight);
	wpComponent.add(contactsPanel, getGbc(0, 5, gridwidth(6)));

	// 0,15
	JPanel categoryPanel = new JPanel();
	categoryPanel.setMinimumSize(new Dimension(rowWidth, defaultRowHeight * 9));
	//wpComponent.add(categoryPanel, getGbc(0, 6, gridwidth(6)));
	categoryPanel.setLayout(new GridBagLayout());
	categoryPanel.setBackground(widgetBgColor);
	categoryPanel.setOpaque(true);

	// 0,15: 0,0
	RSLabel categoryLabel = new RSLabel("Categories");
	categoryLabel.setPreferredSize(new Dimension(leftColTitleWidth, defaultRowHeight));
	categoryPanel.add(categoryLabel, getGbc(0, 0));
	categoryLabel.setOpaque(true);
	categoryLabel.setBackground(widgetBgColor);
	
	// 0,15: 1,0
	categoryDropDown = new JComboBox();
	categoryDropDown.setPreferredSize(new Dimension(leftColTitleWidth * 3, defaultRowHeight));
	categoryPanel.add(categoryDropDown, getGbc(1, 0));
	categoryDropDown.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BUTTON_COLOR));

	// 0,15: 2,0
	addCategoryButton = new RSButton("");
	addCategoryButton.setIcon(getIcon(StationConfiguration.ADD_ICON, smallButtonWidth, defaultRowHeight));
	addCategoryButton.setPreferredSize(new Dimension(smallButtonWidth, defaultRowHeight));
	addCategoryButton.setActionCommand(ADDCATEGORY);
	addCategoryButton.addActionListener(metadataController);
	categoryPanel.add(addCategoryButton, getGbc(2, 0));
	addCategoryButton.setToolTipText("Add category");
	addCategoryButton.setBackground(widgetBgColor);

	// 0,15: 3,0
	blankLabel = new RSLabel("");
	blankLabel.setBackground(widgetBgColor);
	blankLabel.setOpaque(true);
	categoryPanel.add(blankLabel, getGbc(3, 0, weightx(100.0), gfillboth()));

	// 0,15: 4,0
	removeCategoryButton = new RSButton("");
	removeCategoryButton.setIcon(getIcon(StationConfiguration.DELETE_ICON, smallButtonWidth, defaultRowHeight));
	removeCategoryButton.setPreferredSize(new Dimension(smallButtonWidth, defaultRowHeight));
	removeCategoryButton.setActionCommand(REMOVECATEGORY);
	removeCategoryButton.addActionListener(metadataController);
	categoryPanel.add(removeCategoryButton, getGbc(4, 0));
	removeCategoryButton.setToolTipText("Remove category");
	removeCategoryButton.setBackground(widgetBgColor);

	// 0,15: 0,1

	// 0,23
	blankLabel = new RSLabel("");
	wpComponent.add(blankLabel, getGbc(0, 7, gridwidth(6), weighty(100.0)));

	// 0,24
	buttonPane = new JPanel();
	buttonPane.setPreferredSize(new Dimension(rowWidth, defaultRowHeight));
	wpComponent.add(buttonPane, getGbc(0, 8, gridwidth(6)));
	buttonPane.setLayout(new GridBagLayout());
	buttonPane.setBackground(controlBgColor);

	// 0,23
	prevButton = new RSButton("");
	prevButton.setIcon(getIcon(StationConfiguration.PREV_ICON, leftColTitleWidth, defaultRowHeight));
	prevButton.setPreferredSize(new Dimension(leftColTitleWidth, defaultRowHeight));
	prevButton.setActionCommand(PREV);
	prevButton.addActionListener(metadataController);
	//buttonPane.add(prevButton, getGbc(0, 0));
	prevButton.setToolTipText("Previous item");
	prevButton.setBackground(controlButtonBgColor);

	// 1,23
	nextButton = new RSButton("");
	nextButton.setIcon(getIcon(StationConfiguration.NEXT_ICON, leftColTitleWidth, defaultRowHeight));
	nextButton.setPreferredSize(new Dimension(leftColTitleWidth, defaultRowHeight));
	nextButton.setActionCommand(NEXT);
	nextButton.addActionListener(metadataController);
	//buttonPane.add(nextButton, getGbc(1, 0));
	nextButton.setToolTipText("Next item");
	nextButton.setBackground(controlButtonBgColor);

	// 2,23
	feedbackLabel = new RSLabel("");
	buttonPane.add(feedbackLabel, getGbc(2, 0, weightx(100.0)));

	// 3,23
	editButton = new RSButton("");
	editButton.setIcon(getIcon(StationConfiguration.EDIT_ICON, leftColTitleWidth, defaultRowHeight));
	editButton.setPreferredSize(new Dimension(leftColTitleWidth, defaultRowHeight));
	editButton.setActionCommand("EDIT");
	editButton.addActionListener(metadataController);
	buttonPane.add(editButton, getGbc(3, 0));
	editButton.setToolTipText("Edit individual items");
	editButton.setBackground(controlButtonBgColor);

	// 4,23
	saveButton = new RSButton("");
	saveButton.setIcon(getIcon(StationConfiguration.SAVE_ICON, leftColTitleWidth, defaultRowHeight));
	saveButton.setPreferredSize(new Dimension(leftColTitleWidth, defaultRowHeight));
	saveButton.setActionCommand(SAVE);
	saveButton.addActionListener(metadataController);
	buttonPane.add(saveButton, getGbc(4, 0));
	saveButton.setToolTipText("Save");
	saveButton.setBackground(controlButtonBgColor);

    }

    void showTagPopup(){
	if (tagsPopup == null){
	    HashSet<Integer> selectedCategories = new HashSet<Integer>();
	    for (ItemCategory item: metadataController.getCurrentProgramMetadata().getCategories()){
		selectedCategories.add(item.getCategoryNodeID());
	    }
	    
	    tagTree = new TagTree(this, metadataController.getMetadataProvider(), selectedCategories, stationConfig);
	    tagsPopup = GUIUtilities.getTagsPopup(tagTree, tagsField, tagsField.getSize().height, bgColor);
	    TagTree.setTreeKeepAlive(tagTree, true);
	}
	tagsPopup.setVisible(true);
	updateTags();
    }

    public void tagSelectionChanged(Category category, boolean selected){
	if (selected)
	    metadataController.addCategory(metadataController.getCurrentProgramMetadata().getItemID(), category);
	else 
	    metadataController.removeCategory(metadataController.getCurrentProgramMetadata().getItemID(), category);
	updateTags();
    }

    public void tagEdited(Category tag){
	boolean update = false;
	for (ItemCategory item: metadataController.getCurrentProgramMetadata().getCategories()){
	    if (tag.getNodeID() == item.getCategoryNodeID()){
		update = true;
		break;
	    }
	}

	for (Category cat: metadataController.getAddCategories(metadataController.getCurrentProgramMetadata().getItemID())){
	    if (tag.getNodeID() == cat.getNodeID()){
		update = true;
		break;
	    }
	}

	if (update){
	    updateTags();
	}
    }

    public void updateTags(){
	ArrayList<String> list = new ArrayList<String>();
	for (Category category: metadataController.getCategories(metadataController.getCurrentProgramMetadata().getItemID())){
	    list.add(category.getLabel());

	}
	Collections.sort(list);
	tagsField.setText(StringUtilities.joinFromIterable(list, ", "));
    }


    JPanel getContactsPanel(final int width, final int totalHeight, final Color bgColor, Color titleColor, final int titleHeight, int rowHeight){
	final int height = totalHeight - titleHeight;
	JPanel panel = new JPanel();
	panel.setPreferredSize(new Dimension(width, totalHeight));
	panel.setBackground(bgColor);
	
	panel.setLayout(new GridBagLayout());
	
	RSLabel contactsLabel = new RSLabel("Associated Contacts");
	contactsLabel.setMinimumSize(new Dimension(width, titleHeight));
	contactsLabel.setBackground(titleColor);
	contactsLabel.setOpaque(true);
	panel.add(contactsLabel, getGbc(0, 0, gridwidth(2)));

	tableModel = new ContactsTableModel();
	contactsTable = new JTable(tableModel);
	entityTypeField = new JComboBox(metadataController.getAffiliations());
	contactsTable.setDefaultEditor(Creator.class, new DefaultCellEditor(entityTypeField));

	final JLabel rendererLabel = new JLabel();
	rendererLabel.setOpaque(true);
	final Color defaultColor = Color.white;
	Component defaultComponent = (new DefaultTableCellRenderer()).getTableCellRendererComponent(contactsTable, null, true, false, 0, 0);
	final Color selectedColor = defaultComponent.getBackground();

	contactsTable.setDefaultRenderer(Object.class, new TableCellRenderer(){
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col){

		if (col == 2)
		    rendererLabel.setForeground(Color.black);
		else
		    rendererLabel.setForeground(Color.gray);

		if (isSelected){
		    rendererLabel.setBackground(selectedColor);
		} else {
		    rendererLabel.setBackground(defaultColor);
		}
		rendererLabel.setText((String) value);
		return rendererLabel;
	    }
	});
	
	
	final JPopupMenu removeMenu = new JPopupMenu("Remove Contact(s)");
	final Action removeAction = new AbstractAction("Remove Contacts(s)", IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, 20, 20)){
		public void actionPerformed(ActionEvent e){
		    removeSelectedCreators();
		}
	    };
		
	JMenuItem removeItem = new JMenuItem(removeAction);
	removeMenu.add(removeItem);
	
	contactsTable.addKeyListener(new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_DELETE){
			removeSelectedCreators();
		    }
		}
	    });

	contactsTable.addMouseListener(new MouseAdapter(){
		public void mousePressed(MouseEvent e){
		    if (e.getButton() == MouseEvent.BUTTON3){
			removeAction.setEnabled(contactsTable.getSelectedRows().length > 0);
			removeMenu.show(contactsTable, e.getX(), e.getY());
		    }
		}
	    });
	contactsTable.setFillsViewportHeight(true);
	contactsTable.getTableHeader().setReorderingAllowed(false);
	contactsTable.getTableHeader().setBackground(bgColor);

	JScrollPane tablePane = new JScrollPane(contactsTable);

	TransferHandler transferHandler = new TransferHandler(){
		public boolean canImport(TransferHandler.TransferSupport support){
		    if (!support.isDrop())
			return false;
		    
		    return support.isDataFlavorSupported(EntitySelection.getFlavor());
		}

		@SuppressWarnings("unchecked")
		public boolean importData(TransferHandler.TransferSupport support){
		    if (!support.isDrop())
			return false;

		    try{
			ArrayList<Entity> list = (ArrayList<Entity>)support.getTransferable().getTransferData(EntitySelection.getFlavor());
			for (Entity entity: list){
			    addCreatorFromEntity(entity);
			}
		    } catch (Exception e){
			logger.error("TransferHandler: Exception.", e);
			return false;
		    }
		    return true;
		}
	    };
	
	tablePane.setTransferHandler(transferHandler);

	panel.add(tablePane, getGbc(0, 1, gridwidth(2), gfillboth(), weightx(1.0), weighty(1.0)));

	RSPanel actionPanel = new RSPanel();
	actionPanel.setBackground(bgColor);
	actionPanel.setLayout(new GridBagLayout());

	panel.add(actionPanel, getGbc(0, 2, gridwidth(2), gfillboth()));

	int buttonWidthHorizFrac = 20;
	int buttonWidth = width * buttonWidthHorizFrac /100;
	int buttonHeight = titleHeight;

	Dimension buttonSize = new Dimension(buttonWidth, buttonHeight);
	Icon contactsIcon = getIcon(StationConfiguration.CONTACTS_ICON, buttonHeight-2, buttonHeight-2);
	final RSButton addExistingButton = new RSButton("Contact List", contactsIcon);
	addExistingButton.setMinimumSize(buttonSize);
	addExistingButton.setPreferredSize(buttonSize);
	addExistingButton.setBackground(bgColor);
	actionPanel.add(addExistingButton, getGbc(0, 0, gfillboth()));
	
	addExistingButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    EntitySelector entitySelector = new EntitySelector(MetadataWidget.this, width/2, height/2, titleHeight, addExistingButton, metadataController.getMetadataProvider(), cacheProvider, stationConfig);
		    entitySelector.setVisible(true);
		}
	    });

	EntityEditorPopup.EntityEditorListener entityEditorListener = new EntityEditorPopup.EntityEditorListener(){
		public void addEntity(String name, String url, String phoneType, String phoneNumber, String email, String location, String entityType){
		    logger.info("GRINS_USAGE: ADD_CONTACT_METADATA_WIDGET");
		    MetadataProvider metadataProvider = metadataController.getMetadataProvider();
		    int entityID = metadataProvider.addEntity(name, url, phoneType, phoneNumber, email, location, entityType);
		    if (entityID >= 0){
			Entity entity = metadataProvider.getSingle(new Entity(entityID));
			addCreatorFromEntity(entity);
		    }
		}
		public void editEntity(int id, String name, String url, String phoneType, String phoneNumber, String email, String location, String entityType){

		}
	    };

	editorPopup = new EntityEditorPopup(entityEditorListener, width/2, totalHeight, bgColor, titleColor, stationConfig, rsApp);
	Icon newCreatorIcon = getIcon(StationConfiguration.NEW_CREATOR_ICON, buttonHeight-2, buttonHeight-2);
	final RSButton addNewButton = new RSButton("Add New", newCreatorIcon);
	addNewButton.setMinimumSize(buttonSize);
	addNewButton.setPreferredSize(buttonSize);
	addNewButton.setBackground(bgColor);
	
	actionPanel.add(addNewButton, getGbc(1, 0, gfillboth()));
	
	addNewButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    editorPopup.showEditor(false, null, addNewButton);
		}
	    });

	populateCreators();
	return panel;
    }
    
    public void entitiesSelected(Entity[] entities){
	for (Entity entity: entities){
	    addCreatorFromEntity(entity);
	}
    }
    
    void addCreatorFromEntity(Entity entity){
	Creator creator = new Creator(metadataController.getCurrentProgramMetadata().getItemID(), entity.getEntityID(), (String) entityTypeField.getSelectedItem());
	creator.setEntity(entity);
	addCreator(creator);
    }
    
    public synchronized void resetCreators() {
	tableModel.clear();
	populateCreators();
    }

    void populateCreators() {
	if (metadataController == null ||
	    metadataController.getCurrentProgramMetadata() == null ||
	    metadataController.getCurrentProgramMetadata().getCreators() == null) {
	    
	    return;
	}

	for (Creator creator: metadataController.getCurrentProgramMetadata().getCreators()){
	    addCreator(creator);
	}
    }

    void addCreator(Creator creator){
	cacheProvider.getSingle(creator.getEntity());
	cacheProvider.addCacheObjectListener(creator.getEntity(), this);
	
	tableModel.addCreator(creator);
	metadataController.getCreatorListModel().addCreator(creator);

    }

    void removeSelectedCreators(){
	ListSelectionModel model = contactsTable.getSelectionModel();

	int minIndex = model.getMinSelectionIndex();
	int maxIndex = model.getMaxSelectionIndex();

	if (minIndex < 0 || maxIndex < 0)
	    return;

	ArrayList<Creator> list = new ArrayList<Creator>();
	
	for (int i = minIndex; i <= maxIndex; i++){
	    if (model.isSelectedIndex(i))
		list.add(tableModel.getCreatorAt(i));
	}

	for (Creator creator: list)
	    removeCreator(creator);

    }

    void removeCreator(Creator creator){
	metadataController.getCreatorListModel().removeCreator(creator);
	tableModel.removeCreator(creator);
	cacheProvider.removeCacheObjectListener(creator.getEntity(), this);
    }

    public void setFeedbackColor(Color color){
	buttonPane.setBackground(color);
	buttonPane.repaint();
    }

    public void setFeedbackText(String msg){
	feedbackLabel.setText(msg);
    }

    public String[] getSelectedPrograms() {
	return selectedPrograms;
    }

    public boolean getEditMode() {
	return editmode;
    }

    public void setEditMode(boolean editmode) {
	this.editmode = editmode;
    }

    public boolean getGroupMode() {
	return groupmode;
    }

    public JTextField getTitleField() {
	return titleField;
    }

    public JTextArea getDescField() {
	return descField;
    }
    
    public RSLabel getDurationField() {
	return durationField;
    }

    public RSLabel getCreationField() {
	return creationField;
    }

    public JComboBox getLanguageDropDown() {
	return languageDropDown;
    }

    public RSLabel getTypeField() {
	return typeField;
    }

    public JComboBox getCategoryDropDown() {
	return categoryDropDown;
    }

    public RSButton getAddCategoryButton() {
	return addCategoryButton;
    }

    public RSButton getRemoveCategoryButton() {
	return removeCategoryButton;
    }

    public RSButton getPrevButton() {
	return prevButton;
    }

    public RSButton getNextButton() {
	return nextButton;
    }

    public RSButton getEditButton() {
	return editButton;
    }

    public RSButton getSaveButton() {
	return saveButton;
    }

    public JScrollPane getCategoryScrollPane() {
	return categoryScrollPane;
    }

    public StatsDisplay getLongHistoryDisplay() {
	return longHistoryDisplay;
    }
    
    public StatsDisplay getShortHistoryDisplay() {
	return shortHistoryDisplay;
    }

    public void onMaximize(){
	    
    }

    public void onMinimize() {

    }

    protected void getProgramDisplayData(RadioProgramMetadata metadata, ArrayList<String> infoStrings) {
	infoStrings.add("Language: " + metadata.getLanguage());
	Creator[] creators = metadata.getCreators();
	if (creators != null && creators.length > 0) {
	    Entity entity = creators[0].getEntity();
	    if (entity != null){
		infoStrings.add("Creator: " + 
				(creators[0].getEntity().getName().equals("") ? "" :
				 creators[0].getEntity().getName() + ", ") +
				(creators[0].getEntity().getLocation().equals("") ? "" :
				 creators[0].getEntity().getLocation() + ", ") +
				(creators[0].getAffiliation().equals("") &&
				 creators[0].getEntity().getEntityType().equals("") ? "" :
				 "(" +
				 (creators[0].getEntity().getEntityType().equals("") ? "" :
				  creators[0].getEntity().getEntityType() + ", ") +
				 (creators[0].getAffiliation().equals("") ? "" :
				  creators[0].getAffiliation()) +
				 ")"));
	    }
	}

	/*
	Tags[] tags = metadata.getTags();
	if (tags != null && tags.length > 0)
	    infoStrings.add("Tags: " + tags[0].getTagsCSV());
	*/

	ItemCategory[] itemCategories = metadata.getCategories();
	if (itemCategories != null)
	    for (ItemCategory itemCategory: itemCategories) 
		infoStrings.add("Category: " + itemCategory.getCategory().getLabel());
	
    }

    public void reinitAspComponent() {

	aspComponent.removeAll();
	
	aspComponent.setLayout(new GridBagLayout());

	RSLabel stateLabel = null;
	ArrayList<String> infoStrings = new ArrayList<String>();
	if (editmode && !groupmode) {
	    stateLabel = new RSLabel("Individual edit ON");
	    infoStrings.add("Title: " + metadataController.getCurrentProgramMetadata().getTitle());
	    getProgramDisplayData(metadataController.getCurrentProgramMetadata(), infoStrings);
	} else if (!editmode && !groupmode) {
	    stateLabel = new RSLabel("Individual edit OFF");
	    infoStrings.add("Title: " + metadataController.getCurrentProgramMetadata().getTitle());
	    getProgramDisplayData(metadataController.getCurrentProgramMetadata(), infoStrings);
	} else if (editmode && groupmode) {
	    stateLabel = new RSLabel("Group edit ON");
	    for (RadioProgramMetadata program: metadataController.getProgramsMetadata()) {
		if (program.getItemID().equals(metadataController.getCurrentProgramMetadata().getItemID()))
		    infoStrings.add("Current: " + program.getTitle());
		else
		    infoStrings.add("Title: " + program.getTitle());
	    }
	} else if (!editmode && groupmode) {
	    stateLabel = new RSLabel("Group edit ON");
	    for (RadioProgramMetadata program: metadataController.getProgramsMetadata())
		infoStrings.add("Title: " + program.getTitle());
	}
	
	Dimension rowDim = new Dimension((int)(aspComponent.getWidth()) - 2, (int)(aspComponent.getHeight() / 8));
	stateLabel.setPreferredSize(rowDim);
	stateLabel.setMinimumSize(rowDim);
	stateLabel.setHorizontalAlignment(SwingConstants.CENTER);
	aspComponent.add(stateLabel, getGbc(0, 0));
	
	int i = 1;
	for (String info: infoStrings) {
	    if (i < 7) {
		RSLabel infoLabel = new RSLabel(info);
		infoLabel.setHorizontalAlignment(SwingConstants.LEFT);
		infoLabel.setPreferredSize(rowDim);
		infoLabel.setMinimumSize(rowDim);
		aspComponent.add(infoLabel, getGbc(0, i));
		i++;
	    } else
		break;
	}

	RSLabel blankLabel = new RSLabel("");
	aspComponent.add(blankLabel, getGbc(0, i, weighty(100.0)));

	aspComponent.validate();
	aspComponent.repaint();
    }

    public synchronized void onUnload(){
	Enumeration<String[]> e = metadataWidgetInstances.keys();
	while (e.hasMoreElements()) {
	    String[] widgetArr = e.nextElement();	    
	    MetadataWidget widget = metadataWidgetInstances.get(widgetArr);
	    if (widget == this) {
		metadataWidgetInstances.remove(widgetArr);
		break;
	    }
	}
	if (tagTree != null)
	    TagTree.setTreeKeepAlive(tagTree, false);
	unregisterCacheObjects();
	metadataController.unregisterWithProviders();
    }

    public boolean isBottomMenuActive() {
	return false;
    }

    class ContactsTableModel extends DefaultTableModel {
	
	final ArrayList<Creator> list = new ArrayList<Creator>();
	
	int size = 0;

	public int getRowCount(){
	    if (list == null)
		return 0;
	    else
		return list.size();
	}

	public int getColumnCount(){
	    return 3;
	}

	final String[] columnNames = new String[] {"Name", "Location", "Affiliation"};

	public String getColumnName(int col){
	    return columnNames[col];
	}

	public Object getValueAt(int row, int col){
	    Creator creator = list.get(row);
	    switch(col){
	    case 0:
		return creator.getEntity().getName();
	    case 1:
		return creator.getEntity().getLocation();
	    case 2:
		return creator.getAffiliation();
	    default:
		return "";
	    }
	}

	public synchronized void addCreator(Creator creator){
	    for (Creator c: list){
		if (c.getEntity().getEntityID() == creator.getEntity().getEntityID())
		    return;
	    }

	    list.add(creator);
	    int row = list.size() -1;
	    fireTableRowsInserted(row, row);
	}

	public synchronized void removeCreator(Creator creator){
	    list.remove(creator);
	    fireTableDataChanged();
	}

	public Creator getCreatorAt(int i){
	    return list.get(i);
	}

	public void refreshRow(int row){
	    fireTableRowsUpdated(row, row);
	}

	public void clear(){
	    list.clear();
	    fireTableDataChanged();
	}

	public boolean isCellEditable(int row, int col){
	    return col == 2;
	}

	public Class getColumnClass(int col){
	    if (col == 2)
		return Creator.class;
	    else
		return Object.class;
	}

	public void setValueAt(Object value, int row, int col){
	    list.get(row).setAffiliation((String)value);
	    metadataController.getCreatorListModel().editCreator(list.get(row));
	}

	public Creator[] getCreators(){
	    return list.toArray(new Creator[list.size()]);
	}
    }

}