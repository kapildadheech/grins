package org.gramvaani.radio.app.gui;

import java.util.ArrayList;

import java.awt.datatransfer.*;

public abstract class SelectionList implements Transferable {
    
    ArrayList<?> dataList;
    DataFlavor flavor;

    protected SelectionList(ArrayList<?> dataList){ 
	this.dataList = dataList;
	flavor = new DataFlavor(this.getClass(), this.getClass().getName());
    }

    public Object getTransferData(DataFlavor flavor){
	return dataList;
    }

    public DataFlavor[] getTransferDataFlavors(){
	return new DataFlavor []{flavor};
    }

    public boolean isDataFlavorSupported(DataFlavor flavor){
	return (flavor.equals(this.flavor));
    }

    protected static DataFlavor getFlavor(Class<? extends SelectionList> klass){
	return new DataFlavor(klass, klass.getName());
    }
    
}