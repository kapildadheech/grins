package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.utilities.StringUtilities;
import org.gramvaani.radio.telephonylib.RSCallerID;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.RSApp;

import javax.swing.table.DefaultTableModel;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

import java.util.Hashtable;


public class PollTableModel extends DefaultTableModel implements PollResultsPopup.PollResultsListener {
    static final int NUM_PER_QUERY = 50;
    
    int totalPolls = 0;
    
    Hashtable<Integer, Poll> polls = new Hashtable<Integer, Poll>();
    Hashtable<Integer, JPanel> buttonsPanels = new Hashtable<Integer, JPanel>();
    
    String query = "";
    MetadataProvider metadataProvider;
    StationConfiguration stationConfig;
    LogUtilities logger = new LogUtilities("PollTableModel");
    PollResultsPopup resultsPopup;

    PollTableModel(MetadataProvider metadataProvider, StationConfiguration stationConfig, int resultsWidth, int resultsHeight, Color bgColor, Color titleColor, RSApp rsApp) {
	super();
	this.stationConfig = stationConfig;
	this.metadataProvider = metadataProvider;
	resultsPopup = new PollResultsPopup(this, resultsWidth, resultsHeight, bgColor, titleColor, stationConfig, rsApp);
    }
    
    public Class getColumnClass(int columnIndex) {
	if(columnIndex == 4) 
	    return JPanel.class;
	else
	    return JLabel.class;
    }

    public int getColumnCount(){
	return 5;
    }
    
    public int getRowCount(){
	return totalPolls;
    }
    
    final String columnNames[] = new String[] {"Poll Name", "Keyword", "Status", "Votes", "Actions"};
    
    public String getColumnName(int column) {
	return columnNames[column];
    }
    
    public Object getValueAt(int row, int column){
	fetchBatch(row);
	Poll poll = polls.get(row);
	switch(column) {
	case 0:
	    return poll.getPollName();
	case 1:
	    return poll.getKeyword();
	case 2:
	    return Poll.STATUS_STR[poll.getState()];
	case 3:
	    Vote[] votes = metadataProvider.getVotesOfPoll(poll.getID());
	    return (votes.length);
	case 4:
	    return getButtonsPanel(row);
	default:
	    return "";
	}
    }

    JPanel getButtonsPanel(int row) {
	JPanel panel = buttonsPanels.get(row);

	if(panel == null) {
	    Poll poll = polls.get(row);
	    panel = makeButtonsPanel(poll);
	    buttonsPanels.put(new Integer(row), panel);
	}

	return panel;
    }

    void showResults(final Poll poll) {
	PollOption[] options = metadataProvider.getPollOptionsOfPoll(poll.getID());
	Vote[] votes = metadataProvider.getVotesOfPoll(poll.getID());
	
	resultsPopup.showPopup(poll, options, votes);
    }

    JPanel makeButtonsPanel(final Poll poll) {
	Color buttonBgColor = stationConfig.getColor(StationConfiguration.SEARCHCLEAR_BUTTON_COLOR);
	JPanel panel = new JPanel();
	
	panel.setLayout(new GridBagLayout());
	RSButton resultsButton = new RSButton("Results");
	resultsButton.setBackground(buttonBgColor);
	resultsButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    showResults(poll);
		}
	    });
	
	panel.add(resultsButton);
	
	RSLabel emptyLabel = new RSLabel(" ");
	panel.add(emptyLabel);
	
	RSButton startButton = new RSButton("Start");
	startButton.setBackground(buttonBgColor);
	startButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if(poll.getState() == Poll.RUNNING)
			return;

		    Poll dummyPoll = Poll.getDummyObject(poll.getID());
		    Poll updatePoll = Poll.getDummyObject(poll.getID());
		    updatePoll.setState(Poll.RUNNING);
		    metadataProvider.update(dummyPoll, updatePoll);
		    reloadTable();
		}
	    });
	panel.add(startButton);
	
	emptyLabel = new RSLabel(" ");
	panel.add(emptyLabel);
	
	RSButton stopButton = new RSButton("Stop");
	stopButton.setBackground(buttonBgColor);
	stopButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if(poll.getState() == Poll.NOT_RUNNING)
			return;

		    Poll dummyPoll = Poll.getDummyObject(poll.getID());
		    Poll updatePoll = Poll.getDummyObject(poll.getID());
		    updatePoll.setState(Poll.NOT_RUNNING);
		    metadataProvider.update(dummyPoll, updatePoll);
		    reloadTable();
		}
	    });
	panel.add(stopButton);
	
	return panel;
    }

    public Poll getPollAt(int row){
	return polls.get(row);
    }

    public void removeVotes(int pollID) {
	Vote[] toDeleteVotes = metadataProvider.getVotesOfPoll(pollID);
	for(Vote toDeleteVote :toDeleteVotes)
	    metadataProvider.remove(toDeleteVote);

	reloadTable();
    }
    
    public boolean isCellEditable(int row, int col){
	return false;
    }
    
    String getWhereClause(){
	if (query == null || query.equals(""))
	    return "";
	
	return Poll.getPollNameFieldName() + " like '%" +query+ "%' ";
    }

    public void reloadTable(){
	clearData();
	Long numResults = (Long) metadataProvider.mathOp(Metadata.getClassName(Poll.class), getWhereClause(), "1", MetadataProvider.COUNT_OP);
	totalPolls = numResults.intValue();
	fireTableDataChanged();
    }
    
    public void search(String query){
	this.query = query;
	reloadTable();
    }
    
    public void clearData(){
	polls.clear();
	buttonsPanels.clear();
    }
    
    void fetchBatch(int row){
	int baseRow = (row - row % NUM_PER_QUERY);
	if (polls.get(baseRow) != null)
	    return;
	
	Poll results[] = metadataProvider.get(new Poll(), getWhereClause(), new String[] {Poll.getPollNameFieldName()}, new String[] {MetadataProvider.ORDER_ASC}, null, baseRow, NUM_PER_QUERY);
	int i = baseRow;
	
	for (Poll poll: results){
	    polls.put(i, poll);
	    i++;
	}
    }

}