package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.pipeline.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;

import java.util.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import javax.swing.*;
import javax.swing.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class SimulationController implements ActionListener, PlayoutPreviewListenerCallback, ArchiverListenerCallback, 
					     MetadataListenerCallback, CacheListenerCallback, /*ChangeListener,*/ 
					     AdjustmentListener, MicListenerCallback, PlaylistPopup.PlaylistListener {

    public final static String SIMULATION_CONTROLLER = "SIMULATION_CONTROLLER";

    SimulationWidget simulationWidget;
    ControlPanel controlPanel;
    RSApp rsApp;

    SimulationPlaylist simulationPlaylist;

    boolean previewProviderActive;
    boolean archiverProviderActive;
    boolean micProviderActive;
    boolean metadataProviderActive;
    boolean cacheProviderActive;
    
    PreviewProvider previewProvider;
    ArchiverProvider archiverProvider;
    MicProvider micProvider;
    MetadataProvider metadataProvider;
    CacheProvider cacheProvider;

    StationConfiguration stationConfig;

    boolean previewActive, pauseActive;
    boolean goLiveActive;
    boolean audioUIEnabled;
    boolean archiverUIEnabled;

    String previewFilename = "";

    TimeKeeper timekeeper;
    ErrorInference errorInference;
    LogUtilities logger = new LogUtilities("SimulationController");

    long playStartTime;

    SavePlaylistPopup savePlaylistPopup = null;
    //LoadPlaylistPopup loadPlaylistPopup = null;
    PlaylistPopup listPopup = null;
    StartTimePlaylistPopup startTimePlaylistPopup = null;

    Hashtable<String, GoliveItemPopup> goliveItems;

    static final long TZ_OFFSET;

    static {
	Calendar calendar = GregorianCalendar.getInstance();
	TZ_OFFSET = calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET);
    }

    public SimulationController(SimulationWidget simulationWidget, ControlPanel controlPanel, RSApp rsApp) {
	this.simulationWidget = simulationWidget;
	this.controlPanel = controlPanel;
	this.rsApp = rsApp;
	
	simulationPlaylist = new SimulationPlaylist(this);

	simulationWidget.setSimulationPlaylist(simulationPlaylist);
	
	audioUIEnabled = false;
	previewProviderActive = false;
	goLiveActive = false;
	previewActive = false; pauseActive = false;

	archiverUIEnabled = false;
	archiverProviderActive = false;
	metadataProviderActive = false;
	cacheProviderActive = false;

	timekeeper = rsApp.getUIService().getTimeKeeper();
	playStartTime = defaultStartTime();		

	goliveItems = new Hashtable<String, GoliveItemPopup>();

	stationConfig = rsApp.getStationConfiguration();

	errorInference = ErrorInference.getErrorInference();
    }

    public SimulationWidget getSimulationWidget() {
	return simulationWidget;
    }

    public CacheProvider getCacheProvider() {
	return cacheProvider;
    }

    public void init() {
	this.previewProvider = rsApp.getProvider(PreviewProvider.class);
	this.metadataProvider = rsApp.getProvider(MetadataProvider.class);
	this.cacheProvider = rsApp.getProvider(CacheProvider.class);

	previewProvider.registerWithProvider(this);
	metadataProvider.registerWithProvider(this);
	cacheProvider.registerWithProvider(this);

	if (previewProvider == null || !previewProvider.isActive()) {
	    previewProviderActive = false;
	    audioUIEnabled = false;
	} else {
	    previewProviderActive = true;
	    audioUIEnabled = true;
	}

	if (metadataProvider == null || !metadataProvider.isActive())
	    metadataProviderActive = false;
	else
	    metadataProviderActive = true;
	if (cacheProvider == null || !cacheProvider.isActive())
	    cacheProviderActive = false;
	else {
	    cacheProviderActive = true;
	    simulationPlaylist.setCacheProvider(cacheProvider);
	    metadataProvider.setCacheProvider(cacheProvider);
	}
	
	this.archiverProvider = (ArchiverProvider)(rsApp.getProvider(RSApp.ARCHIVER_PROVIDER));
	if (archiverProvider == null || !archiverProvider.isActive()) {
	    archiverProviderActive = false;
	    archiverUIEnabled = false;
	} else {
	    archiverProviderActive = true;
	    archiverUIEnabled = true;
	    archiverProvider.registerWithProvider(this);
	}


	this.micProvider = (MicProvider)(rsApp.getProvider(RSApp.MIC_PROVIDER));
	micProvider.registerWithProvider(this);
	if (micProvider == null || !micProvider.isActive()) {
	    micProviderActive = false;
	} else {
	    micProviderActive = true;
	}

	updateGUIButtons();
	createPlaylistPopup();
    }

    protected long defaultStartTime() {
	return (long)(((timekeeper.getLocalTime() / 1000 / 60 / 60 / 24) + 1 ) * 24 * 60 * 60 * (long)1000 - TZ_OFFSET);
    }

    /*public void internalSliderChange(boolean b) {
	this.internalSliderChange = b;
    }

    long lastTimeSliderMoved = -1;
    boolean internalSliderChange = false;
    public void stateChanged(ChangeEvent e) {
	JSlider slider = (JSlider)(e.getSource());
	if (internalSliderChange) {
	    internalSliderChange = false;
	    return;
	}
	if (!slider.getValueIsAdjusting()) {
	    int val = (int)(slider.getValue());
	    simulationWidget.setSelectedItem(val);
	    lastTimeSliderMoved = -1;
	} else {
	    long currentTime = timekeeper.getLocalTime();
	    //	    System.out.println(currentTime - lastTimeSliderMoved + ":" + slider.getValue());

	    if (lastTimeSliderMoved == -1 || (currentTime - lastTimeSliderMoved < 3000) && (currentTime - lastTimeSliderMoved > 250)) {
		int val = (int)(slider.getValue());
		if (val > 0.03 * SimulationWidget.SLIDER_MAXVAL && val < 0.97 * SimulationWidget.SLIDER_MAXVAL) {
		    simulationWidget.setSelectedItem(val);
		    return;
		}
		
		if (val >= 0.97 * SimulationWidget.SLIDER_MAXVAL) {
		    //		    System.out.println("moving up");
		    int firstIndex = simulationWidget.getListPane().getFirstVisibleIndex();
		    if (firstIndex != -1 && firstIndex > 0) {
			firstIndex--;
			simulationWidget.getListPane().ensureIndexIsVisible(firstIndex);
		    }
		} else {
		    //		    System.out.println("moving down");
		    int lastIndex = simulationWidget.getListPane().getLastVisibleIndex();
		    if (lastIndex != -1 && lastIndex < simulationPlaylist.getSize() - 1) {
			lastIndex++;
			simulationWidget.getListPane().ensureIndexIsVisible(lastIndex);
		    }
		}

		lastTimeSliderMoved = currentTime;	    
	    } else {
		simulationWidget.setSelectedItem((int)(slider.getValue()));
	    }
	}
	}*/

    public void adjustmentValueChanged(AdjustmentEvent e) {
	JScrollBar scrollBar = (JScrollBar)(e.getSource());
	/*if (!scrollBar.getValueIsAdjusting()) {
	    simulationWidget.adjustSelection();
	    //simulationWidget.setSelectedItem((int)(simulationWidget.getSliderPane().getValue()));
	    }*/
    }

    public void actionPerformed(ActionEvent e) {
	String actionCommand = e.getActionCommand();
	if (actionCommand.equals(SimulationWidget.RECORD)) {

	    if (goLiveActive) { // archiving in progress. now send stop-archive command
		if (archiverProvider.isStoppable(SIMULATION_CONTROLLER)) {
		    if (archiverProvider.stopArchive(ArchiverService.RECORDING, true) < 0)
			errorInference.displayPlaylistError("Unable to send stop-record command", new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
		    goLiveActive = false;			
		}

		
		//micProvider.disable(MicProvider.PREVIEW_PORT_ROLE);
	    } else { // send start-archive command
		if (archiverProvider.isArchivable()) {
		    if (archiverProvider.startArchive(ArchiverService.RECORDING, SIMULATION_CONTROLLER, true) < 0) {
			errorInference.displayPlaylistError("Unable to send start-record command", new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
			simulationWidget.selectedArchiverButton(false);
		    }
		    else {
			goLiveActive = true;
			
			//micProvider.enable(SIMULATION_CONTROLLER, MicProvider.PREVIEW_PORT_ROLE);
		    }
		} else {
		    simulationWidget.selectedArchiverButton(false);
		}
	    }
	    updateGUIButtons();
	    
	} else if (actionCommand.equals(SimulationWidget.PLAY)) {
	    if (previewProviderActive && previewProvider.isPlayable()) {
		//		String programID = simulationWidget.getSliderPointingItem().getItemID();
		if (simulationWidget.getListPane().getSelectedIndices() != null && simulationWidget.getListPane().getSelectedIndices().length > 0) {
		    if (simulationWidget.getListPane().getSelectedValues().length == 0)
			return;

		    SimulationWidget.SimulationItemWidget itemWidget = (SimulationWidget.SimulationItemWidget) simulationWidget.getListPane().getSelectedValues()[0];
		    if (itemWidget == null || itemWidget.getMetadata() == null){
			logger.error("ActionPerformed: Null itemwidget.");
			return;
		    }
		    
		    String programID = ((SimulationWidget.SimulationItemWidget)(simulationWidget.getListPane().getSelectedValues()[0])).getMetadata().getItemID();
		    if (programID.startsWith(SimulationWidget.GOLIVE_ITEMID) || 
		       !((SimulationWidget.SimulationItemWidget)(simulationWidget.getListPane().getSelectedValues()[0])).playBackgroundMusic()) {
			simulationWidget.activatePlay(false);
			return;
		    }
		    
		    //System.out.println("!! about to play " + programID);
		    if (previewActive && !previewFilename.equals(programID)) {
			if (previewProvider.stop(previewFilename, this) < 0)
			    errorInference.displayPlaylistError("Unable to send preview-stop command", 
								new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
			previewActive = false;
			previewFilename = "";
		    }
		    if (previewProvider.play(programID, this) < 0)
			errorInference.displayPlaylistError("Unable to send preview-start command", 
							    new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
		    else {
			previewActive = true;
			previewFilename = programID;
		    }
		    if (previewActive) {
			pauseActive = false;
		    simulationWidget.activatePause(false);
		    }
		    updateGUIButtons();
		} else {
		    simulationWidget.activatePlay(false);
		}
	    }

	} else if (actionCommand.equals(SimulationWidget.PAUSE)) {

	    if (previewProviderActive && previewProvider.isPausable()) {
		String programID = previewFilename;
		//System.out.println("!! about to pause " + programID);
		if (previewProvider.pause(programID, this) < 0)
		    errorInference.displayPlaylistError("Unable to send preview-pause command",
							new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
		else {
		    pauseActive = true;
		}
		if (pauseActive) {
		    previewActive = false;
		    simulationWidget.activatePlay(false);
		}
		updateGUIButtons();
	    }

	} else if (actionCommand.equals(SimulationWidget.STOP)) {
	    String programID = previewFilename;

	    if (previewProviderActive && previewProvider.isStoppable(programID, this)) {
		//System.out.println("!! about to stop " + programID);
		if (previewProvider.stop(programID, this) < 0)
		    errorInference.displayPlaylistError("Unable to send preview-stop command", 
							new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
		previewActive = false;
		pauseActive = false;
		simulationWidget.activatePlay(false);
		simulationWidget.activatePause(false);
		previewFilename = "";
		updateGUIButtons();
	    }

	} else if (actionCommand.equals(SimulationWidget.VOLUMEUP)) {
	    if (previewProviderActive && previewProvider.isVolumeAdjustable()) {
		String programID = previewFilename;
		if (previewProvider.volumeUp(programID, this) < 0)
		    errorInference.displayPlaylistError("Unable to send preview-volume-Up command",
							new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
	    }

	} else if (actionCommand.equals(SimulationWidget.VOLUMEDN)) {
 
	    if (previewProviderActive && previewProvider.isVolumeAdjustable()) {
		String programID = previewFilename;
		if (previewProvider.volumeDn(programID, this) < 0)
		    errorInference.displayPlaylistError("Unable to send preview-volume-Dn command",
							new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
	    }

	} else if (actionCommand.equals(SimulationWidget.CLEAR_LIST)) {

	    simulationPlaylist.clearPlaylist();
	    updateGUIButtons();

	} else if (actionCommand.equals(SimulationWidget.LOAD_LIST)) {
	    
	    /*
	    if (loadPlaylistPopup == null || !loadPlaylistPopup.isVisible()) {
		loadPlaylistPopup = new LoadPlaylistPopup(simulationWidget.getLoadButton());
		loadPlaylistPopup.show(simulationWidget.getLoadButton(), 0, -(int)(loadPlaylistPopup.getPreferredSize().getHeight()));
		simulationWidget.getLoadButton().setPressed(true);
	    } else if (loadPlaylistPopup.isVisible()) {
		simulationWidget.getLoadButton().setPressed(false);
		loadPlaylistPopup.raiseNullEvent();
	    }
	    */

	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    listPopup.show(simulationWidget.getLoadButton(), 0, -(int)listPopup.getPreferredSize().getHeight());
		}
	    });


	} else if (actionCommand.equals(SimulationWidget.SAVE_LIST)) {
	    RSButton saveButton = simulationWidget.getSaveButton();
	    if (savePlaylistPopup == null || !savePlaylistPopup.isVisible()) {
		savePlaylistPopup = new SavePlaylistPopup((int)(saveButton.getWidth() * 3), (int)(saveButton.getHeight()));
		savePlaylistPopup.showPopup(saveButton);
		simulationWidget.getSaveButton().setPressed(true);
	    } else if (savePlaylistPopup.isVisible()) {
		simulationWidget.getSaveButton().setPressed(false);
		savePlaylistPopup.raiseNullEvent();
	    }

	} else if (actionCommand.equals(SimulationWidget.SETSTART_LIST)) {
	    /* not used any more
	    if (startTimePlaylistPopup == null || !startTimePlaylistPopup.isVisible()) {
		startTimePlaylistPopup = new StartTimePlaylistPopup(simulationWidget.getStartTimeButton());
		startTimePlaylistPopup.show(simulationWidget.getStartTimeButton(), 0, -(int)(startTimePlaylistPopup.getPreferredSize().getHeight()));
		simulationWidget.getStartTimeButton().setPressed(true);
	    } else if (startTimePlaylistPopup.isVisible()) {
		simulationWidget.getStartTimeButton().setPressed(false);
		startTimePlaylistPopup.raiseNullEvent();
	    }
	    */
	} else if (actionCommand.startsWith(SimulationWidget.LISTINFO)) {

	    String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    if (metadataProviderActive) {
		MetadataWidget metadataWidget = MetadataWidget.getMetadataWidget(rsApp, new String[]{programID}, false);
		if (!metadataWidget.isLaunched()) {
		    controlPanel.initWidget(metadataWidget, false);
		    controlPanel.cpJFrame().launchWidget(metadataWidget);
		} else {
		    controlPanel.cpJFrame().setWidgetMaximized(metadataWidget);
		}
	    }
	} else if (actionCommand.startsWith(SimulationWidget.LISTCLOSE)) {
	    
	    String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    simulationPlaylist.removeItem(programID);

	    //int val = (int)(simulationWidget.getSliderPane().getValue());
	    //simulationWidget.setSelectedItem(val);

	    updateGUIButtons();
	    
	} else if (actionCommand.startsWith(SimulationWidget.LISTLIVE)) {

	    String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    if (goliveItems.get(programID) == null) {
		int index = 0;
		if (simulationWidget.getListPane().getSelectedIndices() != null && simulationWidget.getListPane().getSelectedIndices().length > 0)
		    index = simulationWidget.getListPane().getSelectedIndices()[0];
		GoliveItemPopup goliveItemPopup = new GoliveItemPopup((RSButton)(e.getSource()), programID, index);
		goliveItemPopup.show(simulationWidget.getListPane(), simulationWidget.getListElemGoliveXoff() - 2, 
				     (int)(index * simulationWidget.getListElemDim().getHeight() +
					   simulationWidget.getListElemGoliveYoff()));
		((RSButton)(e.getSource())).setPressed(true);
		goliveItems.put(programID, goliveItemPopup);
	    } else {
		((RSButton)(e.getSource())).setPressed(false);
		goliveItems.get(programID).raiseNullEvent();
	    }

	} else if (actionCommand.startsWith(SimulationWidget.LISTFADE)) {

	    if (simulationWidget.getListPane().getSelectedIndices() != null && simulationWidget.getListPane().getSelectedIndices().length > 0) {
		String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);
		SimulationWidget.SimulationItemWidget simulationItemWidget = 
		    (SimulationWidget.SimulationItemWidget)simulationWidget.getListPane().getSelectedValues()[0];
		
		/*
		//For fadeout button removal
		if (simulationItemWidget != null)
		    if (simulationItemWidget.getFadeoutButton().isSelected()) {
			if (simulationItemWidget.getFadeoutDuration() <= 0) {
			    simulationItemWidget.setFadeoutDuration(SimulationWidget.FADEOUT_DURATION);
			} else {
			    simulationItemWidget.setFadeoutDuration(simulationItemWidget.getFadeoutDuration());
			}
		    } else {
			// do nothing
		    }
		*/
	    }
	}
	    
    }

    Object playlistLock = new Object();
    static final int loadPlaylistHorizFrac = 80;

    void createPlaylistPopup(){
	Thread t = new Thread(){
		public void run(){
		    synchronized(playlistLock){
			listPopup = new PlaylistPopup(SimulationController.this, metadataProvider, stationConfig, 
						      simulationWidget.getPanelWidth() * loadPlaylistHorizFrac/100);
			playlistLock.notifyAll();
		    }
		    
		}
	    };
	t.start();
    }
    
    public void loadPlaylists(){
	synchronized(playlistLock) {
	    while (listPopup == null) {
		try{
		    playlistLock.wait();
		} catch (Exception e){}
	    }
	}
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    listPopup.show(simulationWidget.getLoadButton(), 0, -(int)listPopup.getPreferredSize().getHeight());
		}
	    });
    }

    public void playActive(boolean success, String programID, String playType, long telecastTime, String[] error) {
	simulationWidget.activatePlay(success);

	if (!success) {
	    errorInference.displayServiceError("Unable to play file", error);
	    previewFilename = "";
	}
    }

    public void pauseActive(boolean success, String programID, String playType, String[] error) {
	simulationWidget.activatePause(success);

	if (!success) {
	    errorInference.displayServiceError("Unable to pause file", error);
	    previewFilename = "";
	}
    }

    public void stopActive(boolean success, String programID, String playType, String[] error) {
	simulationWidget.activateStop();

	if (!success) {
	    errorInference.displayServiceError("Unable to stop file", error);
	    previewFilename = "";
	}
    }

    public void playDone(boolean success, String programID, String playType) {
	playDoneHelper(success, programID, playType);
    }

    protected int playDoneHelper(boolean success, String programID, String playType) {
	simulationWidget.activateStop();
	if (simulationPlaylist.getSize() > 0) {
	    //	    int currIndex = simulationWidget.getSliderPointingIndex();
	    int currIndex = simulationWidget.getSelectedIndex();
	    if (currIndex != -1 && currIndex < simulationPlaylist.getSize() - 1) {
		currIndex++;
		simulationWidget.setSelectedIndex(currIndex);
	    } else {
		simulationWidget.activatePlay(false);
		previewActive = false;
		pauseActive = false;
		updateGUIButtons();
		return 0;
	    }
	    //System.err.println("=======currIndex=" + currIndex);
	    programID = simulationPlaylist.getProgramAt(currIndex);
	    if (programID.startsWith(SimulationWidget.GOLIVE_ITEMID)) {
		return playDoneHelper(true, programID, playType);
	    }
	    if (previewProvider.play(programID, this) < 0)
		errorInference.displayServiceError("Error occured during play", new String[]{});
	    else {
		previewActive = true;
		previewFilename = programID;
	    }
	} else
	    previewActive = false;
	if (previewActive)
	    pauseActive = false;
	updateGUIButtons();

	return 0;
    }

    public void audioGstError(String programID, String error, String playType){
	errorInference.displayGstError("Unable to play file", error);

	if (playType.equals(AudioService.PREVIEW)){
	    previewActive = false;
	    previewFilename = "";
	    simulationWidget.activatePlaylistClear(true);
	}
    }

    public void archiverGstError(String error){
	errorInference.displayGstError("Unable to Archive", error);
	goLiveActive = false;
    }

    public void playoutPreviewError(String errorCode, String[] errorParams) {
	simulationWidget.activateStop();
	previewActive = false;
	previewFilename = "";

	errorInference.displayServiceError("Preview error occured", errorCode, errorParams);
    }

    public void activateAudio(boolean activate, String playType, String[] error) {
	if (playType.equals(AudioService.PREVIEW))
	    previewProviderActive = activate;

	if (!activate)
	    errorInference.displayServiceError("Unable to activate " + playType, error);

	updateGUIButtons();
    }


    public void enableAudioUI(boolean enable, String playType, String[] error) {
	audioUIEnabled = enable;
	/*if (playType.equals(AudioService.PREVIEW)){
	    if (simulationPlaylist.getSize() > 0) {
		simulationWidget.enablePlayoutUI(enable);
	    }
	    }*/
	updateGUIButtons();
	if (!enable)
	    errorInference.displayServiceError("Unable to enable audio for " + playType, error);
    }

    public void activateMetadataService(boolean activate, String[] error) {
	metadataProviderActive = activate;

	updateGUIButtons();

	if (!activate)
	    errorInference.displayServiceError("Unable to enable metadata service", error);
	else if (listPopup != null)
	    listPopup.loadPlaylists();

    }

    public void activateCache(boolean activate, String[] error) {
	cacheProviderActive = activate;
	if (activate) {
	    simulationPlaylist.setCacheProvider(cacheProvider);
	    metadataProvider.setCacheProvider(cacheProvider);
	}

	if (!activate)
	    errorInference.displayServiceError("Unable to initialize the cache", error);
	updateGUIButtons();
    }

    public void cacheObjectsDeleted(String objectType, String[] objectID){}
    public void cacheObjectsUpdated(String objectType, String[] objectID){}
    public void cacheObjectsInserted(String objectType, String[] objectID){}

    public void activateArchiver(boolean activate, String[] error) {
	archiverProviderActive = activate;
	archiverUIEnabled = activate;
	updateGUIButtons();

	if (!activate)
	    errorInference.displayServiceError("Unable to enable archiver", error);

    }

    //Used for setting accessibility in UI. ActivateArchiver should change to reset UI state also 
    //apart from just making the UI inaccessible/accessible.
    public void enableArchiverUI(boolean enable, String[] error) {
	archiverUIEnabled = enable;
	//archiverProviderActive = enable;
	updateGUIButtons();

	if (!enable)
	    errorInference.displayServiceError("Unable to enable archiver", error);
    }

    public void archiveStarted(boolean activate, String[] error) {
	simulationWidget.activateGoLive(activate);

	if (!activate)
	    errorInference.displayServiceError("Unable to start archiver", error);
    }

    public void archiveStopped(boolean activate, String[] error) {
	simulationWidget.activateGoLive(false);

	if (!activate)
	    errorInference.displayServiceError("Unable to stop archiver", error);
    }

    public void archiverLevelTested(boolean success, int level){

    }

    public void archivingDone(String filename, boolean success, long startTime, String error) {
	if (success) {
	    RadioProgramMetadata[] dummy = metadataProvider.getProgramMetadata(new String[]{filename});
	    if (dummy == null || dummy.length == 0)
		return;
	    RadioProgramMetadata archivedMetadata = dummy[0];
	    //	    int currIndex = simulationWidget.getSliderPointingIndex() + 1;
	    int currIndex = simulationWidget.getSelectedIndex();
	    if (currIndex < 0)
		currIndex = 0;
	    if (currIndex >= simulationPlaylist.getSize() && simulationPlaylist.getSize() > 0)
		currIndex --;
	    simulationPlaylist.addPrograms(new RadioProgramMetadata[]{archivedMetadata}, currIndex);
	    simulationWidget.setSelectedIndex(currIndex);
	    //simulationWidget.setSelectedItem((int)(simulationWidget.getSliderPane().getValue()));
	    updateGUIButtons();
	} else
	    errorInference.displayServiceError("Error occured while archiving", new String[]{error});
    }

    public String getName() {
	return SIMULATION_CONTROLLER;
    }

    public void activateMic(boolean activate, String[] error){}
    public void enableMicUI(boolean enable, String[] error){}
    public void micGstError(String error){}
    public void micEnabled(boolean success, String[] error){}
    public void micDisabled(boolean success, String[] error){}

    public void micLevelTested(boolean success, int level){}

    public void updateGUIButtons() {
	if (previewProviderActive && simulationPlaylist.getSize() > 0) {
	    if (audioUIEnabled)
		simulationWidget.activatePlayout(true);
	    else
		simulationWidget.enablePlayoutUI(false);
	} else {
	    simulationWidget.activatePlayout(false);
	}

	if (metadataProviderActive)
	    simulationWidget.activateInfo(true);
	else
	    simulationWidget.activateInfo(false);

	if (archiverProviderActive && archiverUIEnabled) {
	    simulationWidget.activateArchiverButton(true);
	} else if (archiverProviderActive && !archiverUIEnabled) {
	    simulationWidget.activateArchiverButton(false);
	} else {
	    simulationWidget.activateArchiverButton(false);
	    simulationWidget.activateGoLive(false);
	}

	if (simulationPlaylist.getProgramAt(0) == null) {
	    /*
	    if (startTimePlaylistPopup != null && startTimePlaylistPopup.isVisible())
		startTimePlaylistPopup.raiseNullEvent();
	    simulationWidget.getStartTimeButton().setEnabled(false);
	    */

	    if (savePlaylistPopup != null && savePlaylistPopup.isVisible())
		savePlaylistPopup.raiseNullEvent();
	    simulationWidget.getSaveButton().setEnabled(false);

	    simulationWidget.getClearButton().setEnabled(false);
	} else if (simulationPlaylist.getProgramAt(0) != null) {
	    //simulationWidget.getStartTimeButton().setEnabled(true);
	    if (cacheProviderActive) {
		simulationWidget.getSaveButton().setEnabled(true);
	    } else {
		if (savePlaylistPopup != null && savePlaylistPopup.isVisible())
		    savePlaylistPopup.raiseNullEvent();
		simulationWidget.getSaveButton().setEnabled(true);
	    }
	    simulationWidget.getClearButton().setEnabled(true);
	}

	if (cacheProviderActive) {
	    simulationWidget.getLoadButton().setEnabled(true);
	} else {
	    /*
	    if (loadPlaylistPopup != null && loadPlaylistPopup.isVisible()) {
		loadPlaylistPopup.raiseNullEvent();
	    }
	    */
	    simulationWidget.getLoadButton().setEnabled(false);
	}
    }



    public void addPrograms(ArrayList<RadioProgramMetadata> programList, int index) {
	simulationPlaylist.addPrograms(programList.toArray(new RadioProgramMetadata[]{}), index);
	
	simulationWidget.setStartTime(playStartTime);
	updateGUIButtons();
    }

    public void addSimItems(ArrayList<SimulationWidget.SimulationItemWidget> itemList, int index){
	simulationPlaylist.addItems(itemList, index);
	simulationWidget.setStartTime(playStartTime);
	updateGUIButtons();
    }

    public void removeProgramsAt(int[] selectionIndices, boolean removeCacheListener) {
	if (selectionIndices.length == 0)
	    return;

	simulationPlaylist.removeProgramsAt(selectionIndices, removeCacheListener);

	simulationWidget.setStartTime(playStartTime);
	updateGUIButtons();
    }

    void newPlaylistAdded(Playlist playlist){
	if (listPopup != null)
	    listPopup.playlistAdded(playlist);
    }

    void playlistUpdated(int playlistID){
	if (listPopup != null)
	    listPopup.playlistUpdated(playlistID);
    }

    public class SavePlaylistPopup extends JDialog implements ActionListener {
	Playlist[] playlists;
	String nullItem = "Select playlist or Type new";
	JComboBox playlistDropDown;
	boolean closePopup = true;

	public SavePlaylistPopup(int width, int height) {
	    super(controlPanel.cpJFrame(), false);

	    playlists = metadataProvider.getPlaylists();

	    Dimension dim = new Dimension(width, height);
	    setPreferredSize(dim);

	    playlistDropDown = new JComboBox(playlists);
	    playlistDropDown.setPreferredSize(dim);
	    playlistDropDown.addItem(nullItem);
	    playlistDropDown.setSelectedItem(nullItem);
	    playlistDropDown.setEditable(true);
	    playlistDropDown.addActionListener(this);
	    playlistDropDown.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR));

	    add(playlistDropDown);

	    KeyListener escapeListener = new KeyAdapter(){
		    public void keyPressed(KeyEvent e){
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			    cancelSave();
			}
		    }
		};
	    
	    addWindowFocusListener(new WindowAdapter(){
		    public void windowLostFocus(WindowEvent e){
			cancelSave();
		    }
		});
	    
	    playlistDropDown.addKeyListener(escapeListener);

	    setUndecorated(true);
	    pack();
	    setVisible(false);
	}

	public void setVisible(boolean b) {
	    if (b || !closePopup)
		super.setVisible(true);
	    else
		super.setVisible(false);
	}
	
	void showPopup(JComponent assocComponent){
	    Dimension mySize = getPreferredSize();

	    int xOffset = -(mySize.width - assocComponent.getSize().width)/2;
	    int yOffset = -(mySize.height);
	    Point p = assocComponent.getLocationOnScreen();
	    
	    int popupX = p.x + xOffset;
	    int popupY = p.y + yOffset;
	    setLocation(popupX, popupY);
	    
	    playlistDropDown.hidePopup();

	    setVisible(true);
	    requestFocusInWindow();
	}

	public void cancelSave() {
	    setVisible(false);
	}

	public void actionPerformed(ActionEvent e) {
	    Object selectedObject = playlistDropDown.getSelectedItem();

	    if (e.getActionCommand().equals("comboBoxEdited") || selectedObject.toString().equals("")){
		return;
	    }

	    if (!selectedObject.toString().equals(nullItem)) {

		if (PlaylistController.newEntry(selectedObject, playlists)) {

		    Playlist playlist = new Playlist(-1);
		    String playlistName = StringUtilities.sanitize(selectedObject.toString());
		    playlist.setName(playlistName);
		    playlist.setStartTime(playStartTime);
		    int playlistID = metadataProvider.addPlaylist(playlist);
		    
		    if (playlistID != -1) {
			playlist.setPlaylistID(playlistID);
			playlistDropDown.addItem(playlist);
			playlists = metadataProvider.getPlaylists();
			
			RadioProgramMetadata[] programs = simulationPlaylist.getPrograms();
			int count = 0;
			for (RadioProgramMetadata program: programs) {
			    String programID = program.getItemID();
			    PlaylistItem playlistItem = new PlaylistItem(playlistID, programID, count);
			    SimulationWidget.SimulationItemWidget simulationItemWidget = 
				(SimulationWidget.SimulationItemWidget)(simulationPlaylist.getElementAt(count));
			    playlistItem.setLiveItem(simulationItemWidget.isLiveItem());
			    playlistItem.setPlayBackground(simulationItemWidget.playBackgroundMusic());
			    playlistItem.setLiveDuration(simulationItemWidget.getGoliveDuration());
			    playlistItem.setFadeout(simulationItemWidget.getFadeoutDuration());
			    metadataProvider.addPlaylistItem(playlistItem);
			    count++;
			}
			newPlaylistAdded(playlist);
		    } else {
			errorInference.displayPlaylistError("Unable to save", new String[]{PlaylistController.UNABLE_TO_SAVE});
		    }

		} else {
		    closePopup = false;
		    Playlist playlist = new Playlist(-1);
		    Playlist newPlaylist = new Playlist(-1);
		    
		    for (Playlist curr: playlists)
			if (curr.getName().equals(selectedObject.toString())) {

			    String message = "Are you sure you want to replace playlist: "+curr.getName()+"?";
			    String title = "Replace existing playlist";
			    int retVal = JOptionPane.showConfirmDialog(rsApp.getControlPanel().cpJFrame(), message, title, JOptionPane.YES_NO_OPTION);
			    if (retVal != JOptionPane.YES_OPTION){
				playlistDropDown.setSelectedItem(nullItem);
				closePopup = true;
				return;
			    }
			    

			    playlist.setPlaylistID(curr.getPlaylistID());
			    newPlaylist.setStartTime(playStartTime);
			    metadataProvider.updatePlaylist(playlist, newPlaylist);
			    
			    if (simulationPlaylist.getPrograms() != null && simulationPlaylist.getPrograms().length > 0) {
				metadataProvider.removePlaylistItems(PlaylistItem.getDummyObject(curr.getPlaylistID()));
			    }
			    
			    RadioProgramMetadata[] programs = simulationPlaylist.getPrograms();
			    int count = 0;
			    for (RadioProgramMetadata program: programs) {
				String programID = program.getItemID();
				PlaylistItem playlistItem = new PlaylistItem(curr.getPlaylistID(), programID, count);
				SimulationWidget.SimulationItemWidget simulationItemWidget = 
				    (SimulationWidget.SimulationItemWidget)(simulationPlaylist.getElementAt(count));
				playlistItem.setLiveItem(simulationItemWidget.isLiveItem());
				playlistItem.setPlayBackground(simulationItemWidget.playBackgroundMusic());
				playlistItem.setLiveDuration(simulationItemWidget.getGoliveDuration());
				playlistItem.setFadeout(simulationItemWidget.getFadeoutDuration());
				metadataProvider.addPlaylistItem(playlistItem);
				count++;
			    }
			    
			    playlistUpdated(playlist.getPlaylistID());
			    break;
			}
		    
		    closePopup = true;
		}

		setVisible(false);
		simulationWidget.getSaveButton().setPressed(false);
	    } 
	}

	public void raiseNullEvent() {
	    cancelSave();
	}
    }

    public void loadPlaylist(Playlist playlist){
	metadataProvider.getPlaylistItems(playlist);

	if (playlist.getStartTime() <= -1) {
	    if (playStartTime > timekeeper.getLocalTime())
		playlist.setStartTime(playStartTime);
	    else
		playlist.setStartTime(playStartTime = defaultStartTime());
	}

	simulationPlaylist.loadPlaylist(playlist);
	simulationWidget.setStartTime(playStartTime);
	updateGUIButtons();
    }
    
    public class StartTimePlaylistPopup extends JPopupMenu implements ActionListener {
	JSpinner hourSpinner;
	JSpinner minSpinner;
	JSpinner secSpinner;

	JSpinner fadeoutSpinner;

	RSButton okButton, cancelButton;
	RSButton fadeOkButton;

	boolean selectionDone = false;

	public StartTimePlaylistPopup(JComponent parentComponent) {
	    super();

	    Dimension dim = new Dimension((int)(parentComponent.getWidth() * 3) + 10, (int)(parentComponent.getHeight() * 4) + 10);
	    setPreferredSize(dim);

	    Calendar calendar = new GregorianCalendar();
	    calendar.setTimeInMillis(timekeeper.getLocalTime() + 10 * 60 * (long)1000);
	    hourSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.HOUR_OF_DAY), 0, 23, 1));
	    minSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.MINUTE), 0, 59, 1));
	    secSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.SECOND), 0, 59, 1));
	    fadeoutSpinner = new JSpinner(new SpinnerNumberModel(3, 0, 59, 1));

	    setLayout(new GridBagLayout());
	    JSpinner[] spinners = new JSpinner[]{hourSpinner, minSpinner, secSpinner, fadeoutSpinner};
	    String[] spinnerLabels = new String[]{"Hour", "Minute", "Second", "Fadeout"};
	    int i = 0;
	    for (JSpinner spinner: spinners) {
		RSLabel label = new RSLabel(spinnerLabels[i]);
		label.setPreferredSize(new Dimension(((int)(parentComponent.getWidth() * 2 / 3)), (int)(parentComponent.getHeight())));
		add(label, getGbc((i == 3 ? 2 : 0), (i == 3 ? 0 : i)));

		spinner.setPreferredSize(new Dimension(((int)(parentComponent.getWidth() * 2 / 3)), (int)(parentComponent.getHeight())));
		add(spinner, getGbc((i == 3 ? 3 : 1), (i == 3 ? 0 : i)));

		i++;
	    }

	    okButton = new RSButton("Set Start Time");
	    okButton.setActionCommand("OK");
	    okButton.addActionListener(this);
	    okButton.setPreferredSize(new Dimension(((int)(parentComponent.getWidth() * 1.5)), (int)(parentComponent.getHeight())));
	    add(okButton, getGbc(0, 3, gridwidth(2)));

	    fadeOkButton = new RSButton("Set Fadeout");
	    fadeOkButton.setActionCommand("OK_FADE");
	    fadeOkButton.addActionListener(this);
	    fadeOkButton.setPreferredSize(new Dimension(((int)(1.5 * parentComponent.getWidth())), (int)(parentComponent.getHeight())));
	    fadeOkButton.setMinimumSize(new Dimension(((int)(1.5 * parentComponent.getWidth())), (int)(parentComponent.getHeight())));
	    add(fadeOkButton, getGbc(2, 3, gridwidth(2)));
	    	    
	    Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	    setBackground(bgColor);
	    hourSpinner.setBackground(bgColor);
	    minSpinner.setBackground(bgColor);
	    secSpinner.setBackground(bgColor);
	    fadeoutSpinner.setBackground(bgColor);	    
	    bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	    okButton.setBackground(bgColor);
	    fadeOkButton.setBackground(bgColor);
	}

	public void setVisible(boolean b) {
	    if (b || !selectionDone)
		super.setVisible(true);
	    else
		super.setVisible(false);
	}

	protected void setSelection(boolean b) {
	    if (b) {
		selectionDone = true;
		setVisible(false);
		//simulationWidget.getStartTimeButton().setPressed(false);	    
	    } else {
		selectionDone = false;
	    }
	}

	public void actionPerformed(ActionEvent e) {
	    if (e.getActionCommand().equals("OK")) {
		int numHours = ((Integer)(hourSpinner.getValue())).intValue();
		int numMin = ((Integer)(minSpinner.getValue())).intValue();
		int numSec = ((Integer)(secSpinner.getValue())).intValue();
		
		playStartTime = (timekeeper.getLocalTime() / 1000 / 60 / 60 / 24) * 24 * 60 * 60 * (long)1000 +
		    (long)numHours * 60 * 60 * (long)1000 + (long)numMin * 60 * (long)1000 + (long)numSec * (long)1000 - TZ_OFFSET;
		//System.out.println("time selection: " + playStartTime + ":" + timekeeper.getLocalTime() + ":" + 
		//	   (timekeeper.getLocalTime() - playStartTime) + ":" + numHours + ":" + numMin + ":" + numSec);
		simulationWidget.setStartTime(playStartTime);
		updateGUIButtons();

		setSelection(true);
	    } else if (e.getActionCommand().equals("CANCEL")) {		
		setSelection(true);
	    } else if (e.getActionCommand().equals("OK_FADE")) {
		int fadeoutSec = ((Integer)(fadeoutSpinner.getValue())).intValue();
		simulationPlaylist.initFadeout(fadeoutSec * 1000);

		SimulationWidget.FADEOUT_DURATION = fadeoutSec * 1000;
		
		setSelection(true);
	    }	    

	}

	public void raiseNullEvent() {
	    //System.out.println("processing event");
	    //	    cancelButton.doClick();
	    setSelection(true);
	}

    }


    public class GoliveItemPopup extends JPopupMenu implements ActionListener {
	RSButton parentButton;
	String programID;
	int index;

	JSpinner minSpinner, secSpinner;
	
	RSButton okButton, cancelButton, resetButton;
	JCheckBox backgroundCheckBox;

	boolean selectionDone = false;

	SimulationWidget.SimulationItemWidget simulationItemWidget;

	public GoliveItemPopup(RSButton parentButton, String programID, int index) {
	    super();
	    
	    this.parentButton = parentButton;
	    this.programID = programID;
	    this.index = index;

	    this.simulationItemWidget = (SimulationWidget.SimulationItemWidget)(simulationPlaylist.getElementAt(index));

	    Dimension dim = new Dimension((int)(parentButton.getWidth() * 6) + 5, (int)(parentButton.getHeight() * 3) + 5);
	    setPreferredSize(dim);

	    setLayout(new GridBagLayout());

	    RSLabel durationLabel = new RSLabel("Time:");
	    durationLabel.setPreferredSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    durationLabel.setMinimumSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    add(durationLabel, getGbc(0, 0));
	    
	    Calendar calendar = new GregorianCalendar();
	    calendar.setTimeInMillis((long)10 * 1000 - TZ_OFFSET);
	    minSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.MINUTE), 0, 59, 1));
	    secSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.SECOND), 0, 59, 1));
	    minSpinner.setPreferredSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    minSpinner.setMinimumSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    secSpinner.setPreferredSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    secSpinner.setMinimumSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    
	    add(minSpinner, getGbc(1, 0));
	    
	    add(secSpinner, getGbc(2, 0));

	    backgroundCheckBox = new JCheckBox("Play BG music");
	    backgroundCheckBox.setPreferredSize(new Dimension((int)(dim.getWidth()), (int)(dim.getHeight() / 3)));
	    backgroundCheckBox.setMinimumSize(new Dimension((int)(dim.getWidth()), (int)(dim.getHeight() / 3)));
	    add(backgroundCheckBox, getGbc(0, 1, gridwidth(3)));

	    okButton = new RSButton("Ok");
	    okButton.setActionCommand("OK");
	    okButton.addActionListener(this);
	    okButton.setPreferredSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    okButton.setMinimumSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));

	    resetButton = new RSButton("Reset");
	    resetButton.setActionCommand("RESET");
	    resetButton.addActionListener(this);
	    resetButton.setPreferredSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    resetButton.setMinimumSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));

	    cancelButton = new RSButton("Cancel");
	    cancelButton.setActionCommand("CANCEL");
	    cancelButton.addActionListener(this);
	    cancelButton.setPreferredSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));
	    cancelButton.setMinimumSize(new Dimension((int)(dim.getWidth() / 3), (int)(dim.getHeight() / 3)));

	    add(okButton, getGbc(0, 2));
	    add(resetButton, getGbc(1, 2));
	    add(cancelButton, getGbc(2, 2));
	    
	    if (programID.startsWith(SimulationWidget.GOLIVE_ITEMID)) {
		backgroundCheckBox.setSelected(false);
		backgroundCheckBox.setEnabled(false);
	    } else {
		if (simulationItemWidget != null && simulationItemWidget.playBackgroundMusic())
		    backgroundCheckBox.setSelected(true);
		else
		    backgroundCheckBox.setSelected(false);
	    }

	    if (simulationItemWidget != null) {
		long duration = simulationItemWidget.getGoliveDuration();
		minSpinner.setValue((int)((duration / 1000) / 60));
		secSpinner.setValue((int)((duration / 1000) % 60));
	    }

	    Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	    setBackground(bgColor);
	    minSpinner.setBackground(bgColor);
	    secSpinner.setBackground(bgColor);
	    backgroundCheckBox.setBackground(bgColor);
	    
	    bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	    okButton.setBackground(bgColor);
	    cancelButton.setBackground(bgColor);
	    resetButton.setBackground(bgColor);
	}

	public void setVisible(boolean b) {
	    if (b || !selectionDone)
		super.setVisible(true);
	    else
		super.setVisible(false);
	}

	protected void setSelection(boolean b) {
	    if (b) {
		selectionDone = true;
		setVisible(false);
		parentButton.setPressed(false);	    
		goliveItems.remove(programID);
	    } else {
		selectionDone = false;
	    }
	}

	public void actionPerformed(ActionEvent e) {
	    if (e.getActionCommand().equals("OK")) {

		SimulationWidget.SimulationItemWidget simulationItemWidget = 
		    (SimulationWidget.SimulationItemWidget)simulationPlaylist.getElementAt(index);
		if (!programID.startsWith(SimulationWidget.GOLIVE_ITEMID)) {
		    simulationItemWidget.playBackgroundMusic(backgroundCheckBox.isSelected());
		    simulationWidget.getListPane().setSelectedIndex(index);
		}
		simulationItemWidget.setGoliveDuration(((long)((Integer)(minSpinner.getValue())).intValue() * 60 +
							(long)((Integer)(secSpinner.getValue())).intValue()) * 1000);
		simulationItemWidget.isLiveItem(true);
		setSelection(true);

	    } else if (e.getActionCommand().equals("RESET")) {

		SimulationWidget.SimulationItemWidget simulationItemWidget = 
		    (SimulationWidget.SimulationItemWidget)simulationPlaylist.getElementAt(index);

		if (!programID.startsWith(SimulationWidget.GOLIVE_ITEMID)) {
		    simulationItemWidget.isLiveItem(false);
		    simulationItemWidget.playBackgroundMusic(true);
		    simulationItemWidget.setGoliveDuration(simulationItemWidget.getMetadata().getLength());
		    setSelection(true);
		} else {
		    minSpinner.setValue(new Integer(0));
		    secSpinner.setValue(new Integer(10));
		}

	    } else if (e.getActionCommand().equals("CANCEL")) {		

		setSelection(true);

	    }
	}

	public void raiseNullEvent() {
	    //System.out.println("processing event");
	    cancelButton.doClick();
	}

    }


}