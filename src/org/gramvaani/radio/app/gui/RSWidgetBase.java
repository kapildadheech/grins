package org.gramvaani.radio.app.gui;

import org.gramvaani.utilities.LogUtilities;

import java.awt.*;
import java.awt.datatransfer.*;
import javax.swing.*;
import java.awt.event.*;

public abstract class RSWidgetBase {

    protected ControlPanelJFrame cpJFrame;
    protected JPanel wpComponent;
    protected RSPanel aspComponent;
    protected JToggleButton bmpButton = new JToggleButton(); 
    protected String bmpIconID;
    protected String bmpToolTip;
    protected String name;
    protected boolean isLaunched = false;
    protected boolean isMaximized = false;
    protected Dimension wpSize, aspItemSize;
    protected JComponent focusComponent = null;
    protected final LogUtilities logger;

    public RSWidgetBase(String name){
	this.name = name;
	logger = new LogUtilities(name);

	bmpButton.setContentAreaFilled(true);
	bmpButton.setOpaque(true);
    }

    public void setControlPanelJFrame(ControlPanelJFrame cpJFrame){
	this.cpJFrame = cpJFrame;
    }

    public JComponent getWorkspaceComponent(){
	return wpComponent;
    }
    
    public JComponent getActiveScrollComponent(){
	return aspComponent;
    }

    public JToggleButton getBottomMenuButton(){
	return bmpButton;
    }

    public String getBottomMenuButtonIconID() {
	return bmpIconID;
    }

    public String getBottomMenuButtonToolTip() {
	return bmpToolTip;
    }

    public String getName(){
	return name;
    }

    public boolean isLaunched(){
	return isLaunched;
    }

    public boolean launch(Dimension wpSize, Dimension aspItemSize){
	this.wpSize = wpSize;
	this.aspItemSize = aspItemSize;

	aspComponent.setPreferredSize(aspItemSize);
	aspComponent.setSize(aspItemSize);

	Action closeWidget = new AbstractAction(){
		public void actionPerformed(ActionEvent e){
		    cpJFrame.unloadWidget(RSWidgetBase.this);
		}

	    };

	if (wpComponent != null && !(this instanceof LogViewWidget)){
	    wpComponent.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("control W"), "closeWidget");
	    wpComponent.getActionMap().put("closeWidget", closeWidget);
	} 

	return (isLaunched = onLaunch());
    }

    protected abstract boolean onLaunch();

    public void maximize(){
	onMaximize();
	isMaximized = true;
    }
    
    protected abstract void onMaximize();

    public void minimize(){
	onMinimize();
	isMaximized = false;
    }

    protected abstract void onMinimize();

    public boolean isBottomMenuActive() {
	return true;  // default
    }

    public boolean isMaximized(){
	return isMaximized;
    }

    public void repaint() {
	wpComponent.repaint();
	aspComponent.repaint();
    }

    public DataFlavor[] getASPDataFlavors(){
	return new DataFlavor[] {};
    }

    public DataFlavor[] getWPDataFlavors(){
	return new DataFlavor[] {};
    }

    protected void setFocusComponent(JComponent component){
	focusComponent = component;
    }

    public void setFocus(){
	if (focusComponent != null){
	    focusComponent.requestFocus();
	}
    }

    public boolean canUnload(){
	return true;
    }

    public void unload(){
	onUnload();
	isLaunched = false;
    }

    public abstract void onUnload();
}