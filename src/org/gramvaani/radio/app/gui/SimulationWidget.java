package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.rscontroller.services.ArchiverService;

import java.util.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.MatteBorder;
import javax.swing.border.*;

import static org.gramvaani.utilities.IconUtilities.getIcon;
import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class SimulationWidget implements ListDataListener {
    public static String GOLIVE_ITEMID = "GOLIVEITEMID";

    public static long FADEOUT_DURATION = PlaylistWidget.FADEOUT_DURATION;

    public static int SLIDER_MAXVAL = 10000;

    public static final String PLAY = "PLAY";
    public static final String STOP = "STOP";
    public static final String PAUSE = "PAUSE";
    public static final String VOLUMEUP = "VOLUMEUP";
    public static final String VOLUMEDN = "VOLUMEDN";
    public static final String RECORD = "RECORD";
    
    public final static String CLEAR_LIST = "CLEAR_LIST";
    public final static String LOAD_LIST = "LOAD_LIST";
    public final static String SAVE_LIST = "SAVE_LIST";
    public final static String SETSTART_LIST = "SETSTART_LIST";

    public final static String LISTCLOSE = "LISTCLOSE";
    public final static String LISTINFO = "LISTINFO";
    public final static String LISTLIVE = "LISTLIVE";
    public final static String LISTFADE = "LISTFADE";

    protected ControlPanel controlPanel;
    protected RSApp rsApp;
    protected JPanel panel = null;

    JPanel topPane;
    //JSlider sliderPane;
    JList listPane;
    ListModel listModel;
    JScrollPane scrollPane;
    JPanel listControlPane;
    Dimension listElemDim;

    protected RSToggleButton goLive;
    protected RSToggleButton play, pause;
    protected RSButton stop;
    protected RSButton volumeUp, volumeDn;

    protected RSButton clearPlaylist;
    protected RSButton loadPlaylist, savePlaylist, setStartTimePlaylist;

    
    SimulationPlaylist simulationPlaylist;
    SimulationController simulationController;
    StationConfiguration stationConfig;

    protected boolean playoutUIEnabled = false;

    public SimulationWidget(ControlPanel controlPanel, RSApp rsApp) {
	this.controlPanel = controlPanel;
	this.rsApp = rsApp;

	this.simulationController = new SimulationController(this, controlPanel, rsApp);
	this.stationConfig = rsApp.getStationConfiguration();
    }

    public void setPanel(JPanel basePanel, Dimension dim) {
	if (panel == null) {
	    panel = new JPanel();
	    panel.setPreferredSize(dim);
	    panel.setSize(dim);
	    init();
	    simulationController.init();
	    basePanel.add(panel);
	    //	    basePanel.validate();
	} else {
	    basePanel.add(panel);
	    //	    basePanel.validate();
	}
    }

    protected final static int topPaneVertFrac = 7;
    protected final static int listPaneVertFrac = 89;
    protected final static int listElementVertFrac = 7;
    protected final static int listControlPaneVertFrac = 4;

    protected final static int goliveHorizFrac = 20;
    protected final static int playHorizFrac = 20;
    protected final static int pauseHorizFrac = 20;
    protected final static int stopHorizFrac = 20;
    protected final static int volHorizFrac = 20;

    protected final static int closeItemHorizFrac = 20;
    protected final static int durationItemHorizFrac = 25;
    protected final static int durationExtraHirozFrac = 5;
    protected final static int titleItemHorizFrac = 40;

    protected final static int clearPlaylistHorizFrac = 25;
    protected final static int loadPlaylistHorizFrac = 25;
    protected final static int savePlaylistHorizFrac = 25;
    protected final static int setStartTimePlaylistHorizFrac = 25;

    protected final static int sliderHorizFrac = 6;
    protected final static int listHorizFrac = 100;

    public void setSimulationPlaylist(SimulationPlaylist simulationPlaylist) {
	this.simulationPlaylist = simulationPlaylist;
	simulationPlaylist.addListDataListener(this);
    }

    protected void init() {
	initTopLevelPanes();
	initTopPane();
	//initSliderPane();
	initListPane();
	initListControlPane();

	listElemDim = new Dimension((int)(panel.getWidth() * listHorizFrac / 100.0)
				    - (int)(scrollPane.getVerticalScrollBar().getMaximumSize().getWidth()), 
				    (int)(panel.getHeight() * listElementVertFrac / 100.0));
    }

    protected void initTopLevelPanes() {
	topPane = new JPanel();

	listPane = new JList(simulationPlaylist);
	listModel = listPane.getModel();
	listPane.setCellRenderer(new ListCellRenderer(){
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus){
		    SimulationItemWidget item = (SimulationItemWidget) value;

		    if (isSelected) {
			item.setSelected();
			//simulationController.internalSliderChange(true);
			//sliderPane.setValue(getSliderPoint(index));
		    } else {
			item.setUnSelected();
		    }
		    return item;
		}
	    });
	scrollPane = new JScrollPane(listPane, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(0,0));
	scrollPane.getVerticalScrollBar().setUnitIncrement(10);
	scrollPane.getVerticalScrollBar().addAdjustmentListener(simulationController);

	listControlPane = new JPanel();

	Dimension topDimension = new Dimension(panel.getWidth(), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	fixSize(topPane, topDimension);
	Dimension scrollDimension = new Dimension((int)(panel.getWidth() * listHorizFrac / 100.0)
						  - (int)(scrollPane.getVerticalScrollBar().getMaximumSize().getWidth()), 
						  (int)(panel.getHeight() * listPaneVertFrac / 100.0));
	fixSize(scrollPane, scrollDimension);
	//Dimension sliderDimension = new Dimension((int)(panel.getWidth() * sliderHorizFrac / 100.0), (int)(panel.getHeight() * listPaneVertFrac / 100.0));
	//fixSize(sliderPane, sliderDimension);
	Dimension listControlDimension = new Dimension(panel.getWidth(), (int)(panel.getHeight() * listControlPaneVertFrac / 100.0));
	fixSize(listControlPane, listControlDimension);

	panel.setLayout(new GridBagLayout());
	panel.add(topPane, getGbc(0, 0, gridwidth(2), gfillboth()));

	panel.add(scrollPane, getGbc(1, 1, gfillboth()));
	panel.add(listControlPane, getGbc(0, 2, gridwidth(2), gfillboth()));

	panel.validate();
    }

    void fixSize(Component c, Dimension dim){
	c.setMinimumSize(dim);
	c.setPreferredSize(dim);
    }


    protected void initTopPane() {

	goLive = new RSToggleButton(""); goLive.setActionCommand(RECORD); goLive.addActionListener(simulationController);
	goLive.setToolTipText("Record Mic");

	play = new RSToggleButton(""){
		public void fireActionPerformed(ActionEvent e){
		    if (!isSelected())
			setSelected(true);
		    else
			super.fireActionPerformed(e);
		}
	    };
	final RSToggleButton playButton = play;

	play.setActionCommand(PLAY); play.addActionListener(simulationController);
	play.setToolTipText("Play");

	pause = new RSToggleButton(""){
		public void fireActionPerformed(ActionEvent e){
		    if (!playButton.isSelected() && isSelected()){
			setSelected(false);
			return;
		    }
		    if (!isSelected())
			setSelected(true);
		    else
			super.fireActionPerformed(e);
		}
	    };
	pause.setActionCommand(PAUSE); pause.addActionListener(simulationController);
	pause.setToolTipText("Pause");

	stop = new RSButton(""); stop.setActionCommand(STOP); stop.addActionListener(simulationController);
	stop.setToolTipText("Stop");

	volumeUp = new RSButton(""); volumeUp.setActionCommand(VOLUMEUP); volumeUp.addActionListener(simulationController);
	volumeUp.setToolTipText("Volume up");
	volumeDn = new RSButton(""); volumeDn.setActionCommand(VOLUMEDN); volumeDn.addActionListener(simulationController);
	volumeDn.setToolTipText("Volume down");

	Dimension goLiveDim = new Dimension((int)(panel.getWidth() * goliveHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	goLive.setPreferredSize(goLiveDim);
	goLive.setBackground(Color.YELLOW);
	Dimension playDim = new Dimension((int)(panel.getWidth() * playHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	play.setPreferredSize(playDim);
	Dimension pauseDim = new Dimension((int)(panel.getWidth() * pauseHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	pause.setPreferredSize(pauseDim);
	Dimension stopDim = new Dimension((int)(panel.getWidth() * stopHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0));
	stop.setPreferredSize(stopDim);
	Dimension volUpDim = new Dimension((int)(panel.getWidth() * volHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0 / 2));
	volumeUp.setPreferredSize(volUpDim);
	Dimension volDnDim = new Dimension((int)(panel.getWidth() * volHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0 / 2));
	volumeDn.setPreferredSize(volDnDim);

	topPane.setLayout(new GridBagLayout());
	topPane.add(goLive, getGbc(0, 0, gridheight(2), gfillboth()));
	topPane.add(play, getGbc(1, 0, gridheight(2), gfillboth()));
	topPane.add(pause, getGbc(2, 0, gridheight(2), gfillboth()));
	topPane.add(stop, getGbc(3, 0, gridheight(2), gfillboth()));
	topPane.add(volumeUp, getGbc(4, 0, gfillboth()));
	topPane.add(volumeDn, getGbc(4, 1, gfillboth()));

	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	topPane.setBackground(bgColor);
	goLive.setBackground(bgColor);
	play.setBackground(bgColor);
	pause.setBackground(bgColor);
	stop.setBackground(bgColor);
	volumeUp.setBackground(bgColor);
	volumeDn.setBackground(bgColor);


	//	if (stationConfig.isMicServiceActive()) {
	//	    goLive.setIcon(IconUtilities.getIcon(StationConfiguration.GOLIVE_ICON, 
	//						 (int)(panel.getWidth() * goliveHorizFrac / 100.0), 
	//						 (int)(panel.getHeight() * topPaneVertFrac / 100.0)));
	//	} else {
	goLive.setIcon(getIcon(StationConfiguration.RECORD_ICON, 
			       (int)(panel.getWidth() * goliveHorizFrac / 100.0), 
			       (int)(panel.getHeight() * topPaneVertFrac / 100.0)));
	    //	}

	play.setIcon(getIcon(StationConfiguration.PLAY_ICON, 
			     (int)(panel.getWidth() * playHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0)));
	pause.setIcon(getIcon(StationConfiguration.PAUSE_ICON, 
			      (int)(panel.getWidth() * pauseHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0)));
	stop.setIcon(getIcon(StationConfiguration.STOP_ICON, 
			     (int)(panel.getWidth() * stopHorizFrac / 100.0), (int)(panel.getHeight() * topPaneVertFrac / 100.0)));
	volumeUp.setIcon(getIcon(StationConfiguration.UP_ICON, 
				 (int)(panel.getWidth() * volHorizFrac / 100.0), 
				 (int)(panel.getHeight() * topPaneVertFrac / 100.0 / 2)));

	volumeDn.setIcon(getIcon(StationConfiguration.DOWN_ICON, 
				 (int)(panel.getWidth() * volHorizFrac / 100.0), 
				 (int)(panel.getHeight() * topPaneVertFrac / 100.0 / 2)));
	
	topPane.validate();
    }


    protected void initListPane() {

	listPane.setDropMode(DropMode.INSERT);
	listPane.setDragEnabled(true);

	TransferHandler transferHandler = new TransferHandler(){
		int selectionIndices[];
		int importIndex;

		public boolean canImport (TransferHandler.TransferSupport support){
		    if (!support.isDrop())
			return false;

		    if (!support.isDataFlavorSupported(ProgramSelection.getFlavor()) &&
		       !support.isDataFlavorSupported(SimulationItemSelection.getFlavor()) ){
			return false;
		    }

		    return true;
		}
		
		@SuppressWarnings("unchecked")
		public boolean importData(TransferHandler.TransferSupport info){
		    if (!info.isDrop())
			return false;
		    Transferable t = info.getTransferable();
		    ArrayList<RadioProgramMetadata> programList = null;
		    ArrayList<SimulationItemWidget> itemList = null;

		    boolean isProgramSelection = info.isDataFlavorSupported(ProgramSelection.getFlavor());
		    
		    try{
			if (isProgramSelection)
			    programList = (ArrayList<RadioProgramMetadata>)t.getTransferData(ProgramSelection.getFlavor());
			else
			    itemList = (ArrayList<SimulationItemWidget>)t.getTransferData(SimulationItemSelection.getFlavor());
		    }catch(Exception e){ 
			return false;
		    }
		    
		    JList.DropLocation dropLocation = (JList.DropLocation)info.getDropLocation();
		    int index = dropLocation.getIndex();

		    if (isProgramSelection)
			simulationController.addPrograms(programList, index);
		    else
			simulationController.addSimItems(itemList, index);

		    importIndex = index;

		    return true;
		}

		public int getSourceActions(JComponent c){
		    return COPY_OR_MOVE;
		}
		
		public Transferable createTransferable (JComponent c){
		    ArrayList<SimulationItemWidget> list = new ArrayList<SimulationItemWidget>();
		    
		    selectionIndices = ((JList)c).getSelectedIndices();

		    for (int i: selectionIndices){
			list.add((SimulationItemWidget)simulationPlaylist.getElementAt(i));
		    }
		    
		    return new SimulationItemSelection(list);
		}
		
		public void exportDone(JComponent c, Transferable t, int action){
		    switch(action){
		    case COPY:
			break;
		    case MOVE:
			if (c == listPane){
			    int[] origIndices = new int[selectionIndices.length];
			    for (int i = 0; i < selectionIndices.length; i++)
				origIndices[i] = importIndex + i;
			    if (importIndex < selectionIndices[0]) {
				for (int i = 0; i < selectionIndices.length; i++) {
				    selectionIndices[i] += selectionIndices.length;
				    origIndices[i] = importIndex + i;
				}
			    } else {
				for (int i = 0; i < selectionIndices.length; i++) {
				    origIndices[i] = importIndex + i - selectionIndices.length;
				}
			    }
			    simulationController.removeProgramsAt(selectionIndices, false);
			    listPane.setSelectedIndices(origIndices);
			}
			
			break;
		    }
		}

	    };

	listPane.setTransferHandler(transferHandler);


	listPane.addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    int index = e.getY()/listElemDim.height;
		    if (index >= listModel.getSize())
			return;
		    
		    SimulationItemWidget itemWidget = (SimulationItemWidget) listModel.getElementAt(index);

		    if (itemWidget != null){
			itemWidget.mouseClicked(e);
			listPane.repaint();
		    }
		}

		public void mousePressed(MouseEvent e){
		    int index = e.getY()/listElemDim.height;
		    if (index >= listModel.getSize())
			return;

		    SimulationItemWidget itemWidget = (SimulationItemWidget) listModel.getElementAt(index);

		    if (itemWidget != null){
			itemWidget.mousePressed(e);
			listPane.repaint();
		    }
		}

		public void mouseReleased(MouseEvent e){
		    int index = e.getY()/listElemDim.height;
		    if (index >= listModel.getSize())
			return;

		    SimulationItemWidget itemWidget = (SimulationItemWidget) listModel.getElementAt(index);

		    if (itemWidget != null){
			itemWidget.mouseReleased(e);
			listPane.repaint();
		    }
		}
	    });
	
	listPane.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "removeitem");
	listPane.getActionMap().put("removeitem", new AbstractAction() {
		public void actionPerformed(ActionEvent e) {
		    int[] selectedIndices = listPane.getSelectedIndices();
		    simulationController.removeProgramsAt(selectedIndices, true);

		    simulationController.updateGUIButtons();
		}
	    });
	listPane.getInputMap().put(KeyStroke.getKeyStroke("INSERT"), "insertlive");
	listPane.getActionMap().put("insertlive", new AbstractAction() {
		public void actionPerformed(ActionEvent e) {
		    int index = 0;
		    if (listPane.getSelectedIndices() != null && listPane.getSelectedIndices().length > 0)
			index = listPane.getSelectedIndices()[0];
		    RadioProgramMetadata goliveMetadata = RadioProgramMetadata.getDummyObject(GOLIVE_ITEMID + "-" + Math.random());
		    goliveMetadata.setTitle("Go live!");
		    ArrayList<RadioProgramMetadata> goliveArr = new ArrayList<RadioProgramMetadata>();
		    goliveArr.add(goliveMetadata);
		    simulationController.addPrograms(goliveArr, index);
		    
		    listPane.getSelectionModel().setSelectionInterval(index, index);
		    simulationController.updateGUIButtons();
		}
	    });
    }
    
    protected void initListControlPane() {
	clearPlaylist = new RSButton(""); clearPlaylist.setActionCommand(CLEAR_LIST); clearPlaylist.addActionListener(simulationController);
	clearPlaylist.setToolTipText("Clear playlist");
	loadPlaylist = new RSButton(""); 
	loadPlaylist.addMouseListener(new MouseAdapter(){
		public void mousePressed(MouseEvent e){
		    simulationController.loadPlaylists();
		}
	    });
	loadPlaylist.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    simulationController.loadPlaylists();
		}
	    });

	loadPlaylist.setToolTipText("Load playlist");

	savePlaylist = new RSButton(""); savePlaylist.setActionCommand(SAVE_LIST); savePlaylist.addActionListener(simulationController);
	savePlaylist.setToolTipText("Save playlist");
	setStartTimePlaylist = new RSButton(""); setStartTimePlaylist.setActionCommand(SETSTART_LIST); 
	setStartTimePlaylist.setToolTipText("Set start time");
	setStartTimePlaylist.addActionListener(simulationController);

	int buttonHeight = (int)(panel.getHeight() * listControlPaneVertFrac / 100.0);
	clearPlaylist.setPreferredSize(new Dimension((int)(panel.getWidth() * clearPlaylistHorizFrac / 100.0), 
						     buttonHeight));	
	loadPlaylist.setPreferredSize(new Dimension((int)(panel.getWidth() * loadPlaylistHorizFrac / 100.0), 
						    buttonHeight));
	savePlaylist.setPreferredSize(new Dimension((int)(panel.getWidth() * savePlaylistHorizFrac / 100.0), 
						    buttonHeight));
	setStartTimePlaylist.setPreferredSize(new Dimension((int)(panel.getWidth() * setStartTimePlaylistHorizFrac / 100.0),
							    buttonHeight));
	setStartTimePlaylist.setEnabled(false);

	listControlPane.setLayout(new GridBagLayout());
	listControlPane.add(clearPlaylist, getGbc(0, 0, gfillboth()));
	listControlPane.add(loadPlaylist, getGbc(1, 0, gfillboth()));
	listControlPane.add(savePlaylist, getGbc(2, 0, gfillboth()));
	listControlPane.add(setStartTimePlaylist, getGbc(3, 0, gfillboth()));
	
	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	listControlPane.setBackground(bgColor);
	clearPlaylist.setBackground(bgColor);
	loadPlaylist.setBackground(bgColor);
	savePlaylist.setBackground(bgColor);
	setStartTimePlaylist.setBackground(bgColor);	
	
	int iconSize = (int)(panel.getHeight() * listControlPaneVertFrac / 100.0);

	clearPlaylist.setIcon(getIcon(StationConfiguration.DELETE_ICON, iconSize));
	loadPlaylist.setIcon(getIcon(StationConfiguration.LOAD_PLAYLIST_ICON, iconSize));
	savePlaylist.setIcon(getIcon(StationConfiguration.SAVE_ICON, iconSize));
	setStartTimePlaylist.setIcon(getIcon(StationConfiguration.SETSTART_ICON, iconSize));

	listControlPane.validate();
    }

    public Dimension getListElemDim() {
	return listElemDim;
    }

    public int getListElemGoliveXoff() {
	return (int)(panel.getWidth() * (titleItemHorizFrac + durationItemHorizFrac) / 100.0);
    }

    public int getListElemGoliveYoff() {
	return (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2);
    }

    public SimulationItemWidget getNewSimulationItemWidget(RadioProgramMetadata program) {
	return new SimulationItemWidget(program, listElemDim);
    }

    public int getPanelWidth(){
	return panel.getWidth();
    }

    public RSButton getSaveButton() {
	return savePlaylist;
    }

    public RSButton getLoadButton() {
	return loadPlaylist;
    }

    public RSButton getStartTimeButton() {
	return setStartTimePlaylist;
    }

    public RSButton getClearButton() {
	return clearPlaylist;
    }

    public JScrollPane getScrollPane() {
	return scrollPane;
    }

    /*public JSlider getSliderPane() {
	return sliderPane;
	}*/

    public JList getListPane() {
	return listPane;
    }

    public int getSelectedIndex() {
	return listPane.getSelectedIndex();
    }

    public void setSelectedIndex(int index) {
	listPane.setSelectedIndex(index);
	listPane.ensureIndexIsVisible(index);
	listPane.repaint();
    }

    public void adjustSelection() {
	int firstIndex = listPane.getFirstVisibleIndex();
	int lastIndex = listPane.getLastVisibleIndex();
	int currIndex = listPane.getSelectedIndex();

	if (currIndex < firstIndex) {
	    currIndex = firstIndex;
	} else if (currIndex > lastIndex) {
	    currIndex = lastIndex;
	}

	listPane.setSelectedIndex(lastIndex);

    }

    //ListDataListener functions
    public void contentsChanged(ListDataEvent e) {
	enablePlayoutUI(playoutUIEnabled);
    }

    public void intervalAdded(ListDataEvent e) { }

    public void intervalRemoved(ListDataEvent e) {
	/*
	int lastRemovedIndex = e.getIndex1();
	int firstRemovedIndex = e.getIndex0();

	int indexToSet = lastRemovedIndex + 1;
	int removedCount = lastRemovedIndex - firstRemovedIndex + 1;

	if (indexToSet >= simulationPlaylist.getSize()) {
	    indexToSet = simulationPlaylist.getSize() -1; //firstRemovedIndex - 1;
	}

	//System.err.println("===lastremoved:" + lastRemovedIndex + " toset:" + indexToSet + " size:" + simulationPlaylist.getSize());
	listPane.setSelectedIndex(indexToSet);
	listPane.ensureIndexIsVisible(indexToSet);
	*/
    }

    public void activatePlayout(boolean activate) {
	enablePlayoutUI(activate);
	if (!activate){
	    play.setPressed(false);
	    pause.setPressed(false);
	}
    }

    public void enablePlayoutUI(boolean enable) {
	playoutUIEnabled = enable;
	RadioProgramMetadata metadata = null;
	SimulationWidget.SimulationItemWidget itemWidget;
	if (simulationPlaylist.getSize() > 0) {
	    itemWidget = (SimulationWidget.SimulationItemWidget)simulationPlaylist.getElementAt(0);
	    metadata = itemWidget.getMetadata();
	}

	if (metadata != null && metadata.getPreviewStoreAttempts() == FileStoreManager.UPLOAD_DONE) {
	    play.setEnabled(enable);
	    pause.setEnabled(enable);
	    stop.setEnabled(enable);
	    volumeUp.setEnabled(enable);
	    volumeDn.setEnabled(enable);
	} else {
	    play.setEnabled(false);
	    pause.setEnabled(false);
	    stop.setEnabled(false);
	    volumeUp.setEnabled(false);
	    volumeDn.setEnabled(false);
	}
    }

    public void activateInfo(boolean activate) {
	Iterable<SimulationWidget.SimulationItemWidget> listElements = simulationPlaylist.getElements();
	for (SimulationWidget.SimulationItemWidget itemWidget: listElements) {
	    itemWidget.getInfoButton().setEnabled(activate);
	}
	listPane.repaint();
    }

    public void activatePlay(boolean activate) {
	play.setPressed(activate);
    }

    public void activatePause(boolean activate) {
	pause.setPressed(activate);
    }

    public void activateStop() {

    }

    public void activateArchiverButton(boolean activate) {
	goLive.setEnabled(activate);
    }

    public void selectedArchiverButton(boolean pressed) {
	goLive.setSelected(pressed);
    }

    public void activateGoLive(boolean activate) {
	goLive.setPressed(activate);
	if (activate) {
	    goLive.setIcon(getIcon(StationConfiguration.RECORD_ACTIVE_ICON, 
				   (int)(panel.getWidth() * goliveHorizFrac / 100.0), 
				   (int)(panel.getHeight() * topPaneVertFrac / 100.0)));
	} else {
	    goLive.setIcon(getIcon(StationConfiguration.RECORD_ICON, 
				   (int)(panel.getWidth() * goliveHorizFrac / 100.0), 
				   (int)(panel.getHeight() * topPaneVertFrac / 100.0)));
	}
    }

    public void setStartTime(long startTime) {
	long currTime = startTime;
	Iterable<SimulationItemWidget> listElements = simulationPlaylist.getElements();
	for (SimulationItemWidget itemWidget: listElements) {
	    itemWidget.getTimeLabel().setText(PlaylistWidget.getDateString(currTime));
	    if (itemWidget.getGoliveDuration() >= 0)
		currTime += itemWidget.getGoliveDuration();
	}
	listPane.repaint();
    }

    public void activatePlaylistClear(boolean activate) {
	clearPlaylist.setEnabled(activate);
    }


    public static String additionalText(RadioProgramMetadata metadata) {
	if (metadata.getTags() != null && metadata.getTags().length > 0)
	    return metadata.getTags()[0].getTagsCSV();
	if (metadata.getCreators() != null && metadata.getCreators().length > 0) {
	    Entity entity = metadata.getCreators()[0].getEntity();
	    if (!entity.getName().equals(""))
		return entity.getName();
	    if (!entity.getLocation().equals(""))
		return entity.getLocation();		
	}
	if (metadata.getCategories() != null && metadata.getCategories().length > 0) {
	    Category cat = metadata.getCategories()[0].getCategory();
	    return cat.getLabel();
	}
	
	return "";
    }


    public class SimulationItemWidget extends JPanel {
	RadioProgramMetadata metadata;

	RSLabel timeLabel;
	RSLabel titleLabel1, titleLabel2, durationLabel;
	//	JSpinner minSpinner, secSpinner;
	RSButton close, golive;
	RSButton info;
	RSToggleButton fadeout;
	Color origBgColor;
	Color bgColor;
	Dimension size;
	boolean selected = false;

	boolean isLiveItem = false;
	long liveDuration;
	boolean playBackgroundMusic = true;
	long fadeoutDuration;

	GridBagLayout gridBagLayout;

	public SimulationItemWidget(RadioProgramMetadata metadata, Dimension dim) {
	    super();
	    this.metadata = metadata;
	    setLayout(gridBagLayout = new GridBagLayout());
	    //setBorder(new MatteBorder(0, 0, 1, 0, stationConfig.getColor(StationConfiguration.LIST_ITEM_BORDER_COLOR)));
	    setBorder(new GradientBorder(stationConfig.getColor(StationConfiguration.LIST_ITEM_BORDER_COLOR)));
	    size = dim;
	    fixSize(this, dim);
	    bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_BGCOLOR);
	    origBgColor = bgColor;

	    if (metadata.getItemID().startsWith(GOLIVE_ITEMID)) {
		isLiveItem = true;
		playBackgroundMusic = false;
		liveDuration = (long)10 * 1000; // 10 seconds
		bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_GOLIVECOLOR);
		setBackground(bgColor);
	    } else {
		isLiveItem = false;
		liveDuration = metadata.getLength();
	    }

	    if (metadata.getType().equals(ArchiverService.BCAST_MIC))
		fadeoutDuration = 0;
	    else
		fadeoutDuration = FADEOUT_DURATION;

	    init();
	}
	
	protected void init() {
	    timeLabel = new RSLabel("00:00:00");
	    durationLabel = new RSLabel(StringUtilities.durationStringFromMillis(liveDuration));

	    titleLabel1 = new RSLabel(metadata.getTitle()); titleLabel1.setVerticalAlignment(SwingConstants.CENTER);
	    titleLabel1.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_TITLE_COLOR));
	    titleLabel1.setToolTipText(metadata.getTitle());
	    titleLabel2 = new RSLabel(additionalText(metadata)); titleLabel2.setVerticalAlignment(SwingConstants.CENTER);
	    titleLabel2.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_DETAILS_COLOR));
	    info = new RSButton(""); info.setActionCommand(LISTINFO + "_" + metadata.getItemID()); 
	    info.addActionListener(simulationController);
	    info.setToolTipText("Info");

	    golive = new RSButton("");
	    golive.setActionCommand(LISTLIVE + "_" + metadata.getItemID());
	    golive.addActionListener(simulationController);
	    golive.setToolTipText("Go live!");
	    
	    /*
	    //For fadeout button removal
	    fadeout = new RSToggleButton(""); fadeout.setActionCommand(LISTFADE + "_" + metadata.getItemID());
	    fadeout.setToolTipText("Fade out");
	    fadeout.addActionListener(simulationController);

	    if (fadeoutDuration > 0)
		setFadeoutPressed(true);
	    else
		setFadeoutPressed(false);

	    */

	    info.setPreferredSize(new Dimension((int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2), 
						(int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    golive.setPreferredSize(new Dimension((int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2),
						  (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    /*
	    //For fadeout button removal
	    fadeout.setPreferredSize(new Dimension((int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2),
						   (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    */

	    titleLabel1.setPreferredSize(new Dimension((int)(panel.getWidth() * (titleItemHorizFrac + closeItemHorizFrac) / 100.0), 
						      (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    //added closeItemHorizFrac/2 for removing fadeout button
	    titleLabel2.setPreferredSize(new Dimension((int)(panel.getWidth() * (titleItemHorizFrac + closeItemHorizFrac / 2) / 100.0), 
						      (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    timeLabel.setPreferredSize(new Dimension((int)(panel.getWidth() * durationItemHorizFrac / 100.0), 
						     (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    durationLabel.setPreferredSize(new Dimension((int)(panel.getWidth() * (durationItemHorizFrac + durationExtraHirozFrac + closeItemHorizFrac / 2) / 100.0), 
							 (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));

	    info.setMinimumSize(new Dimension((int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2), 
					      (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    golive.setMinimumSize(new Dimension((int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2),
						(int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    /*
	    //For fadeout button removal
	    fadeout.setMinimumSize(new Dimension((int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2),
						 (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    */

	    titleLabel1.setMinimumSize(new Dimension((int)(panel.getWidth() * (titleItemHorizFrac + closeItemHorizFrac) / 100.0), 
						     (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));

	    //added closeItemHorizFrac/2 for removing fadeout button
	    titleLabel2.setMinimumSize(new Dimension((int)(panel.getWidth() * (titleItemHorizFrac + closeItemHorizFrac / 2) / 100.0), 
						     (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    timeLabel.setMinimumSize(new Dimension((int)(panel.getWidth() * durationItemHorizFrac / 100.0), 
						   (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    durationLabel.setMinimumSize(new Dimension((int)(panel.getWidth() * (durationItemHorizFrac + durationExtraHirozFrac) / 100.0), 
						       (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));

	    add(timeLabel, getGbc(0, 0, gfillboth()));
	    
	    add(durationLabel, getGbc(0, 1, gfillboth()));

	    add(titleLabel1, getGbc(1, 0, gridwidth(4), gfillboth()));

	    add(titleLabel2, getGbc(1, 1, gfillboth()));

	    add(info, getGbc(2, 1, gfillboth()));

	    add(golive, getGbc(3, 1, gfillboth()));

	    /*
	    //For fadeout button removal
	    add(fadeout, getGbc(4, 1, gfillboth()));
	    */

	    gridBagLayout.columnWidths = new int[]{(int)(panel.getWidth() * durationItemHorizFrac / 100.0),
						   (int)(panel.getWidth() * titleItemHorizFrac / 100.0),
						   (int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2),
						   (int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2)};
	    
	    setBackground(bgColor);

	    Color lightBgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	    
	    info.setBackground(lightBgColor);
	    golive.setBackground(lightBgColor);

	    /*
	    //For fadeout button removal  
	    fadeout.setBackground(lightBgColor);
	    */

	    info.setIcon(getIcon(StationConfiguration.INFO_ICON, (int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2), 
				 (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    golive.setIcon(getIcon(StationConfiguration.GOLIVE_ICON, (int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2), 
				   (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    /*
	    //For fadeout button removal
	    fadeout.setIcon(getIcon(StationConfiguration.FADEOUT_ICON, (int)(panel.getWidth() * closeItemHorizFrac / 100.0 / 2), 
				    (int)(panel.getHeight() * listElementVertFrac / 100.0 / 2)));
	    */
	    
	    validate();
	}


	public boolean isLiveItem() {
	    return isLiveItem;
	}

	public void isLiveItem(boolean isLiveItem) {
	    this.isLiveItem = isLiveItem;
	    if (isLiveItem)
		setBackground(bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_GOLIVECOLOR));
	    else
		setBackground(bgColor = origBgColor);
	}

	public boolean playBackgroundMusic() {
	    return playBackgroundMusic;
	}

	public void playBackgroundMusic(boolean playBackgroundMusic) {
	    this.playBackgroundMusic = playBackgroundMusic;
	    // XXX add some visual indication
	}

	public long getGoliveDuration() {
	    return liveDuration;
	}

	public void setGoliveDuration(long liveDuration) {
	    this.liveDuration = liveDuration;
	    durationLabel.setText(StringUtilities.durationStringFromMillis(liveDuration));
	}

	public RadioProgramMetadata getMetadata(){
	    return metadata;
	}

	public void setMetadata(RadioProgramMetadata metadata) {
	    this.metadata = metadata;
	    titleLabel1.setText(metadata.getTitle());
	    titleLabel1.setToolTipText(metadata.getTitle());
	    titleLabel2.setText(additionalText(metadata));
	}

	public RSButton getCloseButton() {
	    return close;
	}

	public RSLabel getTimeLabel() {
	    return timeLabel;
	}

	public RSButton getInfoButton() {
	    return info;
	}

	public void setSelected(){
	    setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_ITEM_SELECTCOLOR));
	    selected = true;
	}

	public boolean isSelected() {
	    return selected;
	}

	public void setUnSelected(){
	    setBackground(bgColor);
	    selected = false;
	}

	/*
	//For fadeout button removal
	public void setFadeoutPressed(boolean pressed) {
	    fadeout.setSelected(pressed);
	}
	*/
	
	public long getFadeoutDuration() {
	    /*
	    //For fadeout button removal
	    if (!fadeout.isSelected() || fadeoutDuration < 0)
	    */
	    if (fadeoutDuration < 0)
		return 0;
	    else
		return fadeoutDuration;
	}

	public void setFadeoutDuration(long fadeoutDuration) {
	    this.fadeoutDuration = fadeoutDuration;
	    /*
	    //For fadeout button removal
	    if (fadeoutDuration > 0)
		setFadeoutPressed(true);
	    else
		setFadeoutPressed(false);
	    */
	}

	/*
	//For fadeout button removal
	public RSToggleButton getFadeoutButton() {
	    return fadeout;
	}
	*/

	protected JComponent getButtonAt(Point p){
	    setLocation(0, 0);
	    setSize(size);

	    int y = (int)p.getY()%(int)getPreferredSize().getHeight();
	    int x = (int)p.getX();
		    
	    Component button = getComponentAt(x, y);
	    if (button instanceof RSToggleButton)
		return ((RSToggleButton)button);
	    else if (button instanceof RSButton)
		return ((RSButton)button);
	    else
		return null;
	}

	public void mouseClicked(MouseEvent e){
	    JComponent button = getButtonAt(e.getPoint());
	    if (button != null && button instanceof RSToggleButton){
		((RSToggleButton)button).doClick();
	    } else if (button != null && button instanceof RSButton) {
		((RSButton)button).doClick();
	    }
	}

	public void mousePressed(MouseEvent e){
	    JComponent button = getButtonAt(e.getPoint());
	    if (button != null && button instanceof RSToggleButton){
		//		((RSToggleButton)button).setPressed(true);
	    } else if (button != null && button instanceof RSButton) {
		((RSButton)button).setPressed(true);
	    }
	}

	public void mouseReleased(MouseEvent e){
	    JComponent button = getButtonAt(e.getPoint());
	    if (button != null && button instanceof RSToggleButton){
		//		((RSToggleButton)button).setPressed(false);
	    } else if (button != null && button instanceof RSButton) {
		((RSButton)button).setPressed(false);
	    }
	}
    }


}