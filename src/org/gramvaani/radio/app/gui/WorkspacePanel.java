package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.utilities.LogUtilities;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.Hashtable;

class WorkspacePanel extends JPanel {
    Hashtable<RSWidgetBase, JComponent> widgetComponents;
    JComponent currentComponent = null;
    protected ControlPanelJFrame cpJFrame;
    RSWidgetBase currentWidget = null;
    JComponent defaultComponent;
    LogUtilities logger = new LogUtilities("WorkspacePanel");
    Dimension componentSize;

    public WorkspacePanel(ControlPanelJFrame cpJFrame){
	super();
	this.cpJFrame = cpJFrame;
	widgetComponents = new Hashtable<RSWidgetBase, JComponent>();
	defaultComponent = new JPanel();
	defaultComponent.setBackground(cpJFrame.getStationConfiguration().getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	setLayout(new BorderLayout());
	showDefaultView();
    }

    public void setComponentSize(int w, int h){
	componentSize = new Dimension(w, h);
    }

    public void showDefaultView(){
	setComponent(defaultComponent);
    }

    public void setDefaultWidget(RSWidgetBase widget){
	defaultComponent = widget.getWorkspaceComponent();
	addWidget(widget);
	setComponent(defaultComponent);
    }

    public void addWidget(RSWidgetBase widget){
	JComponent widgetComponent = widget.getWorkspaceComponent();
	widgetComponents.put(widget, widgetComponent);
	widgetComponent.setPreferredSize(componentSize);
	widgetComponent.setMinimumSize(componentSize);
    }

    public void removeWidget(RSWidgetBase widget){
	widgetComponents.remove(widget);
    }

    public void setWidget(RSWidgetBase widget){
	if (widgetComponents.get(widget) == null) {
	    logger.error("SetWidget: Null widget component");
	    return;
	}

	currentWidget = widget;
	setComponent(widgetComponents.get(widget));
    }

    protected void setComponent(JComponent component){
	if (component == null) {
	    logger.error("SetComponent: Null component");
	    return;
	}

	if (currentComponent != null)
	    remove(currentComponent);
	currentComponent = component;
	add(component, BorderLayout.CENTER);
	validate(); 
	repaint();
    }

    public Dimension getComponentSize(){
	return componentSize;
    }
}
