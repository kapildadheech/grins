package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;

public class FilterWidgetFactory {

    public static ActiveFilterWidget getFilterWidget(SearchFilter searchFilter, ActiveFiltersModel activeFiltersModel, 
						     LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	if (searchFilter.getFilterName().equals(SearchFilter.CREATOR_FILTER)) {
	    return new CreatorFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().equals(SearchFilter.LENGTH_FILTER)) {
	    return new LengthFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().equals(SearchFilter.TYPE_FILTER)) {
	    return new TypeFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().equals(SearchFilter.LICENSE_FILTER)) {
	    return new LicenseFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().equals(SearchFilter.LANGUAGE_FILTER)) {
	    return new LanguageFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().startsWith(SearchFilter.CATEGORY_FILTER)) {
	    return new CategoryFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().equals(SearchFilter.PLAYOUTHISTORY_FILTER)) {
	    return new PlayoutHistoryFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().equals(SearchFilter.NEWITEM_FILTER)) {
	    return new NewItemFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().equals(SearchFilter.TAG_FILTER)) { 
	    return new TagFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else if (searchFilter.getFilterName().equals(SearchFilter.CREATION_TIME_FILTER)){
	    return new CreationTimeFilterWidget(searchFilter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	} else {
	    return null;
	}
    }

}