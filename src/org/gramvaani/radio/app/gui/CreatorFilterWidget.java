package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class CreatorFilterWidget extends ActiveFilterWidget implements ActionListener {
    //MediaLib medialib;
    
    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;

    static final String nullItemName = "Select name";
    static final String nullItemLocation = "Select location";
    static final String nullItemAffiliation = "Select affiliation";
    
    JComboBox affiliationsList, namesList, locationsList;
    StationConfiguration stationConfig;

    boolean ignoreUpdate = false;
    Color bgColor;

    public CreatorFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
			       LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	this.stationConfig = libraryController.getStationConfiguration();
	init();
    }
    
    protected void init() {
	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();

	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, filterVertSize * 4 + borderHeight));
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());
	add(titleLabel, getGbc(0, 0, weightx(100.0), gfillboth()));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));
	add(closeButton, getGbc(1, 0, gfillboth()));

	namesList = addDropDown(nullItemName, 
				//((CreatorFilter)filter).getNames(libraryController.getCacheProvider()), 1);
				((CreatorFilter)filter).getNames(libraryController.getMetadataProvider()), 1);

	locationsList = addDropDown(nullItemLocation,
				    //((CreatorFilter)filter).getLocations(libraryController.getCacheProvider()), 2);
				    ((CreatorFilter)filter).getLocations(libraryController.getMetadataProvider()), 2);
	affiliationsList = addDropDown(nullItemAffiliation,
				       //((CreatorFilter)filter).getAffiliations(libraryController.getCacheProvider()), 3);
				       ((CreatorFilter)filter).getAffiliations(libraryController.getMetadataProvider()), 3);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	setBackground(bgColor);
	titleLabel.setBackground(bgColor);
	closeButton.setBackground(bgColor);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	namesList.setBackground(bgColor);
	locationsList.setBackground(bgColor);
	affiliationsList.setBackground(bgColor);

	closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON,
						  filterVertSize, filterVertSize));

    }

    protected JComboBox addDropDown(String nullItem, String[] listEntries, int gridy) {

	String[] entriesOption = new String[listEntries.length + 1];
	entriesOption[0] = nullItem;
	int i = 1;
	for (String entry: listEntries)
	    entriesOption[i++] = entry;

	JComboBox combobox = new JComboBox(entriesOption);
	combobox.addActionListener(this);
	combobox.setPreferredSize(new Dimension((int)(libraryWidget.getFilterHorizSize() * (tlHorizFrac + cbHorizFrac) / 100.0), 
						(libraryWidget.getFilterVertSize())));

	add(combobox, getGbc(0, gridy, gridwidth(2), weightx(100.0), gfillboth()));
	return combobox;
    }


    public void actionPerformed(ActionEvent e) {
	JComboBox combobox = (JComboBox)(e.getSource());
	
	if (combobox == namesList) {
	    updateResults(CreatorFilter.NAME_ANCHOR, (String)(namesList.getSelectedItem()), nullItemName);
	} else if (combobox == locationsList) {
	    updateResults(CreatorFilter.LOCATION_ANCHOR, (String)(locationsList.getSelectedItem()), nullItemLocation);
	} else if (combobox == affiliationsList) {
	    updateResults(CreatorFilter.AFFILIATION_ANCHOR, (String)(affiliationsList.getSelectedItem()), nullItemAffiliation);
	} 
	
	ignoreUpdate = false;
	
	String text = (String) combobox.getSelectedItem();
	if (text.startsWith("Select "))
	    combobox.setBackground(bgColor);
	else
	    combobox.setBackground(bgColor.brighter());
    }

    protected void updateResults(String anchorName, String selectedText, String nullItem) {
	
	if (ignoreUpdate)
	    return;
	
	if (!selectedText.equals(nullItem)) {
	    ((CreatorFilter)filter).activateAnchor(anchorName, selectedText);
	    SearchResult result = libraryController.activateFilter(sessionID, filter);
	    libraryController.updateResults(result, sessionID);
	} else {
	    ((CreatorFilter)filter).activateAnchor(anchorName, "");
	    SearchResult result = libraryController.activateFilter(sessionID, filter);
	    libraryController.updateResults(result, sessionID);
	}
    }

    public void setEnabled(boolean activate) {
	super.setEnabled(activate);

	namesList.setEnabled(activate);
	locationsList.setEnabled(activate);
	affiliationsList.setEnabled(activate);
    }

    public void update(){
	Object name = namesList.getSelectedItem();
	Object location = locationsList.getSelectedItem();
	Object affiliation = affiliationsList.getSelectedItem();

	removeAll();
	init();

	ignoreUpdate = true;
	namesList.setSelectedItem(name);
	locationsList.setSelectedItem(location);
	affiliationsList.setSelectedItem(affiliation);
    }

}