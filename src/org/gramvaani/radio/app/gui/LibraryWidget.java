package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.rscontroller.services.LibService;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.awt.event.*;
import java.awt.*;
import java.awt.datatransfer.Transferable;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;

import java.io.*;
import java.util.*;

import static org.gramvaani.utilities.IconUtilities.getIcon;
import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class LibraryWidget extends RSWidgetBase {
    public final static String SEARCH = "SEARCH";
    public final static String FILTER = "FILTER";
    public final static String CLEAR = "CLEAR";

    public final static String INFO = "INFO";
    public final static String PREVIEW = "PREVIEW";
    public final static String STOP = "STOP";

    public final static String EDIT = "EDIT";
    public final static String STAT = "STAT";
    public final static String LIST = "LIST";

    static final String SEARCH_RESULTS_FILE = "search_results.xls";

    protected RSApp rsApp;
    protected LibraryController libraryController;
    protected StationConfiguration stationConfiguration;

    static int tpVertFrac = 4;         // title pane
    static double tabbedPaneVertFrac = 3; // tabs
    static int spVertFrac = 4;         // search pane
    static int dfVertFrac = 9;         // display filter
    static int bcVertFrac = 4;         // bottom control
    static int npVertFrac = 4;         // next-previous
    static int afVertFrac = (int)(100 - spVertFrac - dfVertFrac - tpVertFrac - tabbedPaneVertFrac); // active filter
    static int rpVertFrac = (int)(100 - spVertFrac - dfVertFrac - bcVertFrac - tabbedPaneVertFrac - npVertFrac - tpVertFrac);   // result pane

    static int dfScrollBarHeight;

    static final int rpHorizFrac = 65;
    static final int afHorizFrac = 100 - rpHorizFrac;

    static final int searchFieldHorizFrac = 74;
    static final int searchButtonHorizFrac = 13;
    static final int clearButtonHorizFrac = 13;

    static final int displayFilterHorizFrac = 15;
    
    static final int resultVertFrac = 9;

    static final int editHorizFrac = 15;

    static double numActiveFilterScrolls = 3;
    static double numResultScrolls = 1;
    static double numResultScreens;

    Hashtable<String, LibraryTab> libraryTabs;
    Hashtable<String, RSButton> displayFilters;
    ArrayList<String> sessionIndices;

    int filterHorizSize;
    Dimension activeFiltersScrollPaneSize;
    boolean previewActive = false;
    boolean previewUIEnabled = false;
    boolean infoActive = false;
    JTabbedPane tabbedPane;
    RSTitleBar titleBar;

    LogUtilities logger;
    ErrorInference errorInference;

    public LibraryWidget(RSApp rsApp){
	super("Library");

	this.rsApp = rsApp;
	this.stationConfiguration = rsApp.getStationConfiguration();
	IconUtilities.setStationConfig(stationConfiguration);

	wpComponent = new JPanel();
	wpComponent.setLayout(new GridBagLayout());

	aspComponent = new RSPanel();
	bmpIconID = StationConfiguration.LIBRARY_ICON;
	bmpToolTip = "Library";

	displayFilters = new Hashtable<String, RSButton>();

	libraryController = new LibraryController(this, rsApp.getControlPanel(), rsApp);
	logger = new LogUtilities("LibraryWidget");
	errorInference = ErrorInference.getErrorInference();
    }


    public void activatePreview(boolean activate) {
	previewActive = activate;
	previewUIEnabled = activate;
	if (libraryTabs != null) {
	    Collection<LibraryTab> libraryTabsArr = libraryTabs.values();
	    for (LibraryTab libraryTab: libraryTabsArr) {
		if (libraryTab.getResultSP() != null)
		    libraryTab.getResultSP().repaint();
	    }
	}
    }

    public void enablePreviewUI(boolean enable) {
	previewUIEnabled = enable;
	if (libraryTabs != null) {
	    Collection<LibraryTab> libraryTabsArr = libraryTabs.values();
	    for (LibraryTab libraryTab: libraryTabsArr) {
		if (libraryTab.getResultSP() != null)
		    libraryTab.getResultSP().repaint();
	    }
	}
    }

    public void activateInfo(boolean activate) {
	infoActive = activate;
	if (libraryTabs != null) {
	    Collection<LibraryTab> libraryTabArr = libraryTabs.values();
	    for (LibraryTab libraryTab: libraryTabArr) {
		if (libraryTab.getResultSP() != null)
		    libraryTab.getResultSP().repaint();
	    }
	}
    }
    
    public void activateSearch(boolean activate){
	for (LibraryTab libraryTab: libraryTabs.values()){
	    RSButton searchButton = libraryTab.getSearchButton();
	    if (searchButton != null) {
		searchButton.setEnabled(activate);
	    }
	}
    }
    
    public void setPressedInfoButton(String sessionID, boolean pressed) {
	if (sessionID != null)
	    getLibraryTab(sessionID).getCellRenderer().getInfoButton().setPressed(pressed);
    }

    public void setPressedPreviewButton(String sessionID, String programID, boolean pressed) {
	if (sessionID != null)
	    //getLibraryTab(sessionID).getCellRenderer().getPreviewButton().setPressed(pressed);
	    getLibraryTab(sessionID).getCellRenderer().setPreviewPressed(pressed, programID);
    }

    public void setPressedStopButton(String sessionID, boolean pressed) {
	if (sessionID != null)
	    getLibraryTab(sessionID).getCellRenderer().getStopButton().setPressed(pressed);
    }

    public void invokeSearch(String sessionID) {
	if (sessionID != null)
	    getLibraryTab(sessionID).getSearchButton().doClick();
    }

    public boolean onLaunch(){
	libraryTabs = new Hashtable<String, LibraryTab>();
	libraryController.init();

	for (int i = 0; i < stationConfiguration.getIntParam(StationConfiguration.MAX_SEARCH_SESSIONS); i++) {
	    String sessionID = libraryController.newSearchSession();
	    if (sessionID == null) {
		// XXX library service not working
		// XXX displayerror, close widget, and return
		logger.error("Could not initialize library.");
		errorInference.displayLibraryError("Library could not be initialized", new String[]{LibraryController.UNABLE_TO_SEND_COMMAND});
		return false;
	    } else {
		libraryTabs.put(sessionID, new LibraryTab(sessionID));
	    }
	}

	buildWorkspace();
	reinitAspComponent();
	return true;
    }

    protected void buildWorkspace(){
	final RSLabel titleLabel = new RSLabel(name);
	Dimension titleSize = new Dimension((int)(wpSize.getWidth()), (int)(wpSize.getHeight() * tpVertFrac / 100.0));
	titleLabel.setPreferredSize(titleSize);
	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	wpComponent.add(titleLabel, getGbc(0, 0));
	
	numResultScreens = (double)((int)(wpSize.getHeight() * rpVertFrac / 100.0 * resultVertFrac / 100.0)) * MetaSearchFilter.BUFFERED_SEARCH_RESULTS /
	    (int)(wpSize.getHeight()*rpVertFrac/100.0) / numResultScrolls;


	RSLabel blankLabel = new RSLabel("");

	UIManager.put("TabbedPane.selected", stationConfiguration.getColor(StationConfiguration.LIBRARY_DISPLAYFILTERPANE_COLOR));
	UIManager.put("TabbedPane.selectHighlight", stationConfiguration.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR));

	tabbedPane = new JTabbedPane();
	//tabbedPane.putClientProperty(Options.NO_CONTENT_BORDER_KEY, Boolean.TRUE);
	//tabbedPane.putClientProperty(Options.EMBEDDED_TABS_KEY, Boolean.TRUE);

	//tabbedPane.setPreferredSize(new Dimension((int)(wpSize.getWidth()), (int)(wpSize.getHeight() * (100 - tpVertFrac) / 100.0) - 5));
	tabbedPane.setPreferredSize(new Dimension((int)(wpSize.getWidth()), wpSize.height - titleSize.height));
	tabbedPane.addChangeListener(libraryController);
	wpComponent.add(tabbedPane, getGbc(0, 1, weighty(100.0), gfillboth()));

	sessionIndices = new ArrayList<String>();

	int i = 0;
	Enumeration<String> e = libraryTabs.keys();

	while (e.hasMoreElements()) {
	    final String sessionID = e.nextElement();
	    final JPanel tabPanel = new JPanel();
	    tabbedPane.addTab("Search " + (i + 1), tabPanel);
	    //wpComponent.add(tabPanel, getGbc(0, 1, weighty(100.0), gfillboth()));
	    tabPanel.setLayout(new GridBagLayout());

	    LibraryTab libraryTab = libraryTabs.get(sessionID);
	    sessionIndices.add(sessionID);


	    JPanel searchPanel = new JPanel();
	    Dimension spSize = new Dimension((int)wpSize.getWidth(), (int)(wpSize.getHeight()*spVertFrac/100.0));
	    searchPanel.setPreferredSize(spSize);
	    searchPanel.setMinimumSize(spSize);
	    searchPanel.setLayout(new GridBagLayout());

	    JTextField searchField = new JTextField();
	    final RSButton searchButton = new RSButton("Search");
	    RSButton clearButton = new RSButton("Clear");

	    searchField.addActionListener(new ActionListener(){
		    public synchronized void actionPerformed(ActionEvent e){
			searchButton.doClick();
		    }
		});
	    
	    searchButton.setToolTipText("Search");
	    clearButton.setToolTipText("Clear");

	    searchField.setPreferredSize(new Dimension((int)(spSize.getWidth()*searchFieldHorizFrac/100.0),(int) (spSize.getHeight())));
	    searchPanel.add(searchField, getGbc(0, 0, weightx(1.0), gfillboth()));

	    searchButton.setPreferredSize(new Dimension((int)(spSize.getWidth()*searchButtonHorizFrac/100.0), (int)(spSize.getHeight())));
	    searchButton.setMinimumSize(new Dimension((int)(spSize.getWidth()*searchButtonHorizFrac/100.0), (int)(spSize.getHeight())));
	    searchPanel.add(searchButton, getGbc(1, 0, gfillboth()));
	    clearButton.setPreferredSize(new Dimension((int)(spSize.getWidth()*clearButtonHorizFrac/100.0), (int)(spSize.getHeight())));
	    clearButton.setMinimumSize(new Dimension((int)(spSize.getWidth()*clearButtonHorizFrac/100.0), (int)(spSize.getHeight())));
	    searchPanel.add(clearButton, getGbc(2, 0, gfillboth()));

	    setFocusComponent(searchField);

	    tabPanel.add(searchPanel, getGbc(0, 0, gridwidth(2)));

	    blankLabel = new RSLabel("");
	    blankLabel.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_DISPLAYFILTERPANE_COLOR));
	    blankLabel.setOpaque(true);
	    tabPanel.add(blankLabel, getGbc(0, 1, gridwidth(2), weighty(100.0)));
	    
	    JPanel displayFilterPanel = new JPanel();
	    JScrollPane displayFilterPane = new JScrollPane(displayFilterPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER, 
							    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    displayFilterPane.setPreferredSize(new Dimension((int)wpSize.getWidth(), (int)(wpSize.getHeight() * dfVertFrac / 100.0)));
	    displayFilterPane.setMinimumSize(new Dimension((int)wpSize.getWidth(), (int)(wpSize.getHeight() * dfVertFrac / 100.0)));
	    displayFilterPane.getHorizontalScrollBar().setUnitIncrement(10);
	    //dfScrollBarHeight = displayFilterPane.getHorizontalScrollBar().getMaximumSize().height;
	    dfScrollBarHeight = 0;

	    tabPanel.add(displayFilterPane, getGbc(0, 2, gridwidth(2)));

	    blankLabel = new RSLabel("");
	    blankLabel.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_DISPLAYFILTERPANE_COLOR));
	    blankLabel.setOpaque(true);
	    tabPanel.add(blankLabel, getGbc(0, 3, gridwidth(2), weighty(100.0)));
	    
	    RSScrollList activeFilterList = new RSScrollList(libraryController.getActiveFiltersModel(sessionID), RSScrollList.VERTICAL_ORIENTATION);
	    JScrollPane activeFilterPane = new JScrollPane(activeFilterList, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
							   ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    activeFilterPane.setPreferredSize(activeFiltersScrollPaneSize = new Dimension((int)(wpSize.getWidth() * afHorizFrac / 100.0), 
											  (int)(wpSize.getHeight() * afVertFrac / 100.0)));
	    activeFilterPane.setMinimumSize(activeFiltersScrollPaneSize = new Dimension((int)(wpSize.getWidth() * afHorizFrac / 100.0), 
											(int)(wpSize.getHeight() * afVertFrac / 100.0)));
	    activeFilterPane.getVerticalScrollBar().setUnitIncrement(10);

	    activeFilterList.setPreferredSize(new Dimension(activeFiltersScrollPaneSize.width - 
							    activeFilterPane.getVerticalScrollBar().getMaximumSize().width - 10, 
							    (int)(activeFiltersScrollPaneSize.height * numActiveFilterScrolls)));
	    //	    activeFilterList.setMaximumSize(new Dimension(activeFiltersScrollPaneSize.width - activeFilterPane.getVerticalScrollBar().getMaximumSize().width - 10, activeFiltersScrollPaneSize.height));


	    filterHorizSize = activeFiltersScrollPaneSize.width - activeFilterPane.getVerticalScrollBar().getMaximumSize().width - 1;
	    
	    tabPanel.add(activeFilterPane, getGbc(1, 4, gridheight(2)));

	    
	    ArrayList<RadioProgramMetadata> dataList = new ArrayList<RadioProgramMetadata>();

	    final SearchTableModel model = new SearchTableModel(dataList);
	    final JList resultList = new JList(model);
	    model.setList(resultList);
	    model.setWidget(this);
	    JScrollPane resultSP = new JScrollPane(resultList, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
						   ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

	    resultList.setDragEnabled(true);
	    Dimension resultSPDim;
	    resultSP.setPreferredSize(resultSPDim = new Dimension((int)(wpSize.getWidth() * rpHorizFrac / 100.0), 
								  (int)(wpSize.getHeight()*rpVertFrac/100.0)));
	    resultSP.setMinimumSize(resultSPDim = new Dimension((int)(wpSize.getWidth() * rpHorizFrac / 100.0), 
								  (int)(wpSize.getHeight()*rpVertFrac/100.0)));
	    resultList.setPreferredSize(new Dimension((int)(wpSize.getWidth() * rpHorizFrac / 100.0), 
						      (int)((int)(wpSize.getHeight()*rpVertFrac/100.0) * numResultScrolls)));
	    resultList.setMinimumSize(new Dimension((int)(wpSize.getWidth() * rpHorizFrac / 100.0), 
						    (int)((int)(wpSize.getHeight()*rpVertFrac/100.0) * numResultScrolls)));
	    //	    resultList.setRowHeight(((int)(wpSize.getHeight() * rpVertFrac / 100.0 * resultVertFrac / 100.0)));
	    model.setRowHeight(((int)(wpSize.getHeight() * rpVertFrac / 100.0 * resultVertFrac / 100.0)));
	    model.setRowWidth((int)(wpSize.getWidth() * rpHorizFrac / 100.0));

	    SearchTransferHandler transferHandler = new SearchTransferHandler(dataList);
	    resultList.setTransferHandler(transferHandler);

	    final Dimension cellSize = new Dimension(((int)(wpSize.getWidth() * rpHorizFrac / 100.0)),
						     ((int)(wpSize.getHeight() * rpVertFrac / 100.0 * resultVertFrac / 100.0)));
	    final ResultCellRenderer cellRenderer = new ResultCellRenderer(cellSize, sessionID);
	    resultList.setCellRenderer(cellRenderer);
	    

	    final JPopupMenu removeMenu = new JPopupMenu("Remove File");
	    JMenuItem removeItem = new JMenuItem();
	    
	    Action removeAction = new AbstractAction("Delete File(s)", IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, 20, 20)) {
		    public void actionPerformed(ActionEvent e) {
			int[] selectedIndices = resultList.getSelectedIndices();
			if (selectedIndices.length > 0) {
			    String highlightColor = GUIUtilities.colorToHtmlString(stationConfiguration.getColor(StationConfiguration.CONFIRM_HIGHLIGHT_COLOR));
			    String message = "<html>Are you sure you want to remove the selected " +
				"<font color=" + highlightColor + "><b>" + selectedIndices.length + 
				" file(s)</b></font> permanently from GRINS?</html>";
			    
			    int retVal = JOptionPane.showConfirmDialog(rsApp.getControlPanel().cpJFrame(), 
								       message, "Remove File(s)", 
								       JOptionPane.YES_NO_OPTION);
			    
			    if (retVal != JOptionPane.YES_OPTION) 
				return;
			    
			    String[] itemsToDelete = new String[selectedIndices.length];
			    for (int i = 0 ; i < selectedIndices.length; i++) {
				RadioProgramMetadata program = (RadioProgramMetadata)resultList.getModel().getElementAt(selectedIndices[i]);
				itemsToDelete[i] = program.getItemID();
			    }
			    libraryController.deletePrograms(itemsToDelete);
			}
		    }
		};
	    
	    removeItem.setAction(removeAction);
	    removeMenu.add(removeItem);

	    resultList.addMouseListener(new MouseAdapter(){
		    public void mouseClicked(MouseEvent e){
			if (e.getY() > resultList.getModel().getSize()*cellSize.height)
			    return;
			else
			    cellRenderer.mouseClicked(e, model);
		    }
		    
		    public void mousePressed(MouseEvent e){
			int index = e.getY()/cellSize.height;

			if (index >= resultList.getModel().getSize())
			    return;
			else
			    cellRenderer.mousePressed(e, index);

			if (e.getButton() == MouseEvent.BUTTON3 && ! resultList.isSelectionEmpty()){
			    removeMenu.show(resultList, e.getX(), e.getY());
			}

			resultList.repaint();
		    }

		    public void mouseReleased(MouseEvent e){
			int index = e.getY()/cellSize.height;

			if (index > resultList.getModel().getSize())
			    return;
			else
			    cellRenderer.mouseReleased(e, index);
			resultList.repaint();
		    }
		});
	    
	    resultList.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "removeitem");
	    resultList.getActionMap().put("removeitem", removeAction);
	    
	    final RSMultiList multiList = new RSMultiList(resultList, resultSP, model, resultSPDim, 
							  new Dimension((int)(wpSize.getWidth() * rpHorizFrac / 100.0), 
									(int)(wpSize.getHeight() * npVertFrac / 100.0)),
							  new Dimension((int)(wpSize.getWidth() * editHorizFrac / 100.0), 
									(int)(wpSize.getHeight() * npVertFrac / 100.0)),
							  stationConfiguration);
	    multiList.addMoreItemsCallback(libraryController, sessionID);
	    multiList.setNumItemsPerScreen(100 / resultVertFrac);
	    model.setMultiList(multiList);
	    
	    resultList.addKeyListener(new KeyAdapter(){
		    public void keyPressed(KeyEvent e){
			int keyCode = e.getKeyCode();
			if (keyCode == KeyEvent.VK_RIGHT)
			    multiList.nextPage();
			else if (keyCode == KeyEvent.VK_LEFT)
			    multiList.prevPage();
		    }

		});

	    tabPanel.add(multiList, getGbc(0, 4, weightx(100.0)));
	    libraryTab.setResultSPGridConstraints(getGbc(0, 4, weightx(100.0)));
	    libraryTab.setResultSP(resultSP);
	    libraryTab.setResultMultiList(multiList);
	    

	    JPanel statsPanel = new JPanel();
	    Dimension statsPanelSize;
	    statsPanel.setPreferredSize(statsPanelSize = new Dimension((int)(wpSize.getWidth() * rpHorizFrac / 100.0), 
								       (int)(wpSize.getHeight()*rpVertFrac / 100.0) + 
								       (int)(wpSize.getHeight()*npVertFrac / 100.0)));
	    statsPanel.setMinimumSize(statsPanelSize);
	    libraryTab.setStatsPanelGridConstraints(getGbc(0, 4, weightx(100.0), weighty(100.0)));
	    libraryTab.setStatsPanel(statsPanel);
	    libraryTab.setStatsPanelSize(statsPanelSize);
	    //statsPanel.setBorder(BorderFactory.createLineBorder(Color.blue));

	    JPanel bottomCommandPane = new JPanel();
	    bottomCommandPane.setPreferredSize(new Dimension((int)(wpSize.getWidth() * rpHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    bottomCommandPane.setMinimumSize(new Dimension((int)(wpSize.getWidth() * rpHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	   
	    tabPanel.add(bottomCommandPane, getGbc(0, 5));
	    bottomCommandPane.setLayout(new GridBagLayout());

	    RSButton statsButton = new RSButton("");
	    statsButton.setPreferredSize(new Dimension((int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    statsButton.setMinimumSize(new Dimension((int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    statsButton.setActionCommand(STAT + "_" + sessionID);
	    statsButton.addActionListener(libraryController);
	    statsButton.setToolTipText("Graph mode");
	    bottomCommandPane.add(statsButton, getGbc(0, 0));

	    RSButton listButton = new RSButton("");
	    listButton.setPreferredSize(new Dimension((int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    listButton.setMinimumSize(new Dimension((int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    listButton.setActionCommand(LIST + "_" + sessionID);
	    listButton.addActionListener(libraryController);
	    bottomCommandPane.add(listButton, getGbc(1, 0));
	    listButton.setToolTipText("List mode");
	    listButton.setEnabled(false);

	    blankLabel = new RSLabel("");
	    bottomCommandPane.add(blankLabel, getGbc(2, 0, weightx(100.0)));

	    Dimension exportButtonSize = new Dimension((int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0));
	    Icon exportIcon = IconUtilities.getIcon(StationConfiguration.EXPORT_ICON, exportButtonSize.height, exportButtonSize.height);

	    RSButton exportFilesButton = new RSButton("Export Files", exportIcon);
	    exportFilesButton.setToolTipText("Export selected files.");
	    exportFilesButton.setPreferredSize(exportButtonSize);
	    exportFilesButton.setMinimumSize(exportButtonSize);

	    bottomCommandPane.add(exportFilesButton, getGbc(3, 0, gfillboth()));
	    
	    exportFilesButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			exportFiles(sessionID);
		    }
		});

	    Icon reportIcon = IconUtilities.getIcon(StationConfiguration.MAKE_REPORT_ICON, exportButtonSize.height, exportButtonSize.height);
	    RSButton makeReportButton = new RSButton("Make Report", reportIcon);
	    makeReportButton.setToolTipText("Export search results to a spreadsheet.");
	    makeReportButton.setPreferredSize(exportButtonSize);
	    makeReportButton.setMinimumSize(exportButtonSize);

	    bottomCommandPane.add(makeReportButton, getGbc(4, 0, gfillboth()));
	    
	    makeReportButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			makeReport(sessionID);
		    }
		});
		
	    

	    RSButton editButton = new RSButton("");
	    editButton.setPreferredSize(new Dimension((int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    editButton.setMinimumSize(new Dimension((int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    editButton.setActionCommand(EDIT + "_" + sessionID);
	    editButton.addActionListener(libraryController);
	    editButton.setToolTipText("Group edit");
	    //XXX: group edit inactive currently.
	    //bottomCommandPane.add(editButton, getGbc(4, 0, gfillboth()));

	    //bottomCommandPane.setBorder(BorderFactory.createLineBorder(Color.red));

	    libraryTab.setPosition(i);
	    libraryTab.setPanel(tabPanel);
	    libraryTab.setResultList(resultList);
	    libraryTab.setSearchTableModel(model);
	    libraryTab.setSearchField(searchField);
	    libraryTab.setSearchButton(searchButton);
	    libraryTab.setCellRenderer(cellRenderer);
	    libraryTab.setDataList(dataList);
	    libraryTab.setDisplayFilterPanel(displayFilterPanel);
	    libraryTab.setActiveFilterList(activeFilterList);
	    libraryTab.setEditButton(editButton);
	    libraryTab.setStatsButton(statsButton);
	    libraryTab.setListButton(listButton);

	    searchButton.setActionCommand(SEARCH + "_" + sessionID);
	    searchButton.addActionListener(libraryController);
	    final int index = i;
	    searchButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (getSearchText(sessionID).equals(""))
			    tabbedPane.setToolTipTextAt(index, null);
			else
			    tabbedPane.setToolTipTextAt(index, getSearchText(sessionID));
		    }
		    
		});

	    clearButton.setActionCommand(CLEAR + "_" + sessionID);
	    clearButton.addActionListener(libraryController);

	    displayFilterPanel.setLayout(new GridBagLayout());
	    SearchFilter[] filters = libraryController.getAllFilters(sessionID);
	    Color filterBgColor = stationConfiguration.getColor(StationConfiguration.LIBRARY_DISPLAYFILTER_BUTTON_COLOR);
	    int j = 0;
	    for (SearchFilter filter: filters) {
		//System.out.println("******** returning filter " + filter + ":" + sessionID);
		addDisplayFilter(filter.getFilterName(), filter.getFilterLabel(), sessionID, j, displayFilterPanel, filterBgColor);
		j++;
	    }
	    SearchFilterThin[] categoryFilters = libraryController.getAllCategoryFilters(sessionID);
	    for (SearchFilterThin categoryFilter: categoryFilters) {
		addDisplayFilter(categoryFilter.getFilterName(), categoryFilter.getFilterLabel(), sessionID, j, displayFilterPanel,
				 filterBgColor);
		j++;		
	    }

	    displayFilterPanel.add(new RSLabel(""), getGbc((j+1)/2, 0, gridheight(2), weightx(100.0)));

	    libraryController.initDefaultDisplayFilters(sessionID);

	    Color buttonBgColor = stationConfiguration.getColor(StationConfiguration.SEARCHCLEAR_BUTTON_COLOR);
	    searchButton.setBackground(buttonBgColor);
	    clearButton.setBackground(buttonBgColor);
	    statsButton.setBackground(buttonBgColor);
	    listButton.setBackground(buttonBgColor);
	    editButton.setBackground(buttonBgColor);
	    exportFilesButton.setBackground(buttonBgColor);
	    makeReportButton.setBackground(buttonBgColor);

	    searchButton.setIcon(getIcon(StationConfiguration.SEARCH_ICON,
						       (int)(spSize.getWidth()*searchButtonHorizFrac/100.0), (int)(spSize.getHeight())));
	    clearButton.setIcon(getIcon(StationConfiguration.DELETE_ICON,
						      (int)(spSize.getWidth()*clearButtonHorizFrac/100.0), (int)(spSize.getHeight())));
	    statsButton.setIcon(getIcon(StationConfiguration.STATS_ICON,
						      (int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    listButton.setIcon(getIcon(StationConfiguration.LIST_ICON,
						      (int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    editButton.setIcon(getIcon(StationConfiguration.EDIT_ICON,
						      (int)(wpSize.getWidth() * editHorizFrac / 100.0), (int)(wpSize.getHeight() * bcVertFrac / 100.0)));
	    
	    tabPanel.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR));
	    searchPanel.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR));
	    displayFilterPanel.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_DISPLAYFILTERPANE_COLOR));
	    displayFilterPane.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_DISPLAYFILTERPANE_COLOR));
	    activeFilterList.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR));
	    activeFilterPane.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR));
	    resultSP.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_LISTITEM_COLOR));
	    bottomCommandPane.setBackground(stationConfiguration.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	    statsPanel.setBackground(stationConfiguration.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));

	    // blankLabel; ?

	    i++;
	}
	tabbedPane.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR));
	wpComponent.setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR));
    }

    protected void addDisplayFilter(String filterName, String filterLabel, String sessionID, int j, JPanel displayFilterPanel, Color bgColor) {
	RSButton filterButton = new RSButton(filterLabel);
	filterButton.setToolTipText(filterLabel);
	filterButton.setBackground(bgColor);
	filterButton.setPreferredSize(new Dimension((int)(wpSize.getWidth() * displayFilterHorizFrac / 100.0), 
						    //(int)((wpSize.getHeight() * dfVertFrac / 100.0) / 2)));
						    (int)((wpSize.getHeight() * dfVertFrac / 100.0 - dfScrollBarHeight) / 2)));
	//XXXX
	filterButton.setActionCommand(FILTER + "_" + sessionID + "_" + filterName);
	filterButton.addActionListener(libraryController);
	displayFilterPanel.add(filterButton, getGbc(j/2, j%2));
	displayFilters.put(sessionID + "_" + filterName, filterButton);
    }

    public void activateDisplayFilter(String sessionID, String filterName, boolean activate) {
	RSButton filterButton = displayFilters.get(sessionID + "_" + filterName);
	if (filterButton != null)
	    filterButton.setEnabled(activate);

	if (getLibraryTab(sessionID) == null){
	    logger.error("ActivateDisplayFilter: No tab for sessionID: "+sessionID);
	    return;
	}
	
	if (getLibraryTab(sessionID).getDisplayFilterPanel() == null){
	    logger.error("ActivateDisplayFilter: No filter panel found for session: "+sessionID);
	    return;
	}

	getLibraryTab(sessionID).getDisplayFilterPanel().repaint();
    }

    public void activateAllDisplayFilters(boolean activate, Hashtable<String, ActiveFiltersModel> activeFiltersModels) {
	if (!activate) {
	    for (RSButton filterButton: displayFilters.values()) {
		filterButton.setEnabled(false);
	    }
	} else {
	    Enumeration<String> e = displayFilters.keys();
	    while (e.hasMoreElements()) {
		String filterKey = e.nextElement();
		String sessionID = filterKey.split("_")[0];
		RSButton filterButton = displayFilters.get(filterKey);
		ActiveFiltersModel activeFiltersModel = activeFiltersModels.get(sessionID);
		if (activeFiltersModel.getActiveFilterIndex(filterKey.substring(filterKey.indexOf("_") + 1)) == -1)
		    filterButton.setEnabled(true);
		else
		    filterButton.setEnabled(false);
	    }
	}
	// XXX get all displayFilters and setEnabled(activate);
	// XXX need to look at current state as well, or get it from activeFilterSessions
    }

    public int getFilterHorizSize() {
	//return (int)(wpSize.getWidth() * afHorizFrac / 100.0);
	return filterHorizSize;
    }

    public int getFilterVertSize() {
	return (int)((wpSize.getHeight() * dfVertFrac / 100.0 - dfScrollBarHeight)/ 2);
	//return (int)((wpSize.getHeight() * dfVertFrac / 100.0)/ 2);
    }

    public Dimension getActiveFiltersScrollPaneSize() {
	return activeFiltersScrollPaneSize;
    }
    
    public void onMaximize(){

    }

    public void onMinimize() {

    }

    public void reinitAspComponent() {

	if (aspComponent.getComponents() != null && aspComponent.getComponents().length > 0)
	    aspComponent.removeAll();

	int selectedTab = tabbedPane.getSelectedIndex();
	String sessionID = sessionIndices.get(selectedTab);

	ArrayList<String> infoStrings = StatsDisplay.getInfoStrings(libraryController.getSearchSessions().get(sessionID).getActiveFilters(), 
								    libraryController.getMetadataProvider());
	aspComponent.setLayout(new GridBagLayout());

	RSLabel tabLabel = new RSLabel("Search tab: " + (selectedTab + 1));
	Dimension rowDim = new Dimension((int)(aspComponent.getWidth()) - 2, (int)(aspComponent.getHeight() / 8));
	tabLabel.setPreferredSize(rowDim);
	tabLabel.setHorizontalAlignment(SwingConstants.CENTER);
	aspComponent.add(tabLabel, getGbc(0, 0));

	int i = 1;
	for (String info: infoStrings) {
	    if (i < 7) {
		RSLabel infoLabel = new RSLabel(info);
		infoLabel.setHorizontalAlignment(SwingConstants.LEFT);
		infoLabel.setPreferredSize(rowDim);
		aspComponent.add(infoLabel, getGbc(0, i));

		i++;
	    } else
		break;
	}

	RSLabel blankLabel = new RSLabel("");
	aspComponent.add(blankLabel, getGbc(0, i, weighty(100.0)));

	aspComponent.validate();
	aspComponent.repaint();
    }

    public void onUnload(){
	for (LibraryTab libraryTab: libraryTabs.values()) {
	    libraryController.closeSession(libraryTab.getSessionID());
	    if (libraryTab.getSearchTableModel() != null)
		libraryTab.getSearchTableModel().removeCacheObjectListeners();
	}
	libraryTabs = null;
	wpComponent.removeAll();
	libraryController.unregisterWithProviders();
    }

    public LibraryTab getLibraryTab(String sessionID) {
	if (sessionID == null) {
	    logger.error("GetLibraryTab: Null sessionID");
	    return null;
	}
	return libraryTabs.get(sessionID);
    }

    public String getSearchText(String sessionID) {
	return libraryTabs.get(sessionID).getSearchText();
    }

    public void clearSearchText(String sessionID) {
	libraryTabs.get(sessionID).clearSearchText();
    }

    public void refreshResults(String sessionID) {
	LibraryTab libraryTab = libraryTabs.get(sessionID);
	libraryTab.getResultList().repaint();
	libraryTab.getResultSP().repaint();
    }

    public void removeProgramFromAllSessions(String itemID) {
	for (String sessionID : libraryTabs.keySet()) {
	    libraryController.removeProgramFromSession(sessionID, itemID);
	}
    }

    public void displayList(String sessionID) {
	getLibraryTab(sessionID).getPanel().remove(getLibraryTab(sessionID).getStatsPanel());
	getLibraryTab(sessionID).getPanel().add(getLibraryTab(sessionID).getResultMultiList(), getLibraryTab(sessionID).getResultSPGridConstraints());
	getLibraryTab(sessionID).getPanel().validate();
	getLibraryTab(sessionID).getPanel().repaint();
	getLibraryTab(sessionID).getStatsPanel().repaint();
    }

    public void displayStats(String sessionID) {
	getLibraryTab(sessionID).getPanel().remove(getLibraryTab(sessionID).getResultMultiList());
	getLibraryTab(sessionID).getPanel().add(getLibraryTab(sessionID).getStatsPanel(), getLibraryTab(sessionID).getStatsPanelGridConstraints());
	getLibraryTab(sessionID).getPanel().validate();
	getLibraryTab(sessionID).getPanel().repaint();
    }

    public void activateListStats(boolean editActive, boolean statsActive, boolean listActive) {
	if (libraryTabs != null) {
	    Collection<LibraryTab> libraryTabsArr = libraryTabs.values();
	    for (LibraryTab libraryTab: libraryTabsArr) {
		RSButton button;
		button = libraryTab.getEditButton();
		if (button != null)
		    button.setEnabled(editActive);
		
		button = libraryTab.getStatsButton();
		if (button != null)
		    button.setEnabled(statsActive);
		
		button = libraryTab.getListButton();
		if (button != null)
		    button.setEnabled(listActive);
	    }
	}
    }

    protected void makeReport(String sessionID) {
	String separator = "\t";
	String[] entry = new String[3];
	ArrayList<String> entries = new ArrayList<String>();
	
	if(getLibraryTab(sessionID).getResultList().getModel().getSize() == 0) {
	    JOptionPane.showMessageDialog(null, "No entries to create report for.", "information", JOptionPane.INFORMATION_MESSAGE);
	    return;
	}

	JFileChooser fileChooser = new JFileChooser(stationConfiguration.getUserDesktopPath());
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setBackground(stationConfiguration.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	fileChooser.updateUI();
	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int retVal = fileChooser.showDialog(cpJFrame, "Make Report");

	if (retVal != JFileChooser.APPROVE_OPTION)
	    return;

	String destFolder = null;
	try{
	    destFolder = fileChooser.getSelectedFile().getCanonicalPath()+System.getProperty("file.separator");
	} catch (Exception e){
	    logger.error("MakeReport: Unable to find canonical path of selected folder.");
	    return;
	}

	String destFile = destFolder + SEARCH_RESULTS_FILE;

	entries.add("Title" + separator + "Broadcast Count" + separator + "Type of Program" + separator + "Tags" + separator + "Contacts");
	
	SearchSession session = libraryController.getSearchSessions().get(sessionID);
	SearchResult result = session.getAllResults();
	
	
	for (RadioProgramMetadata metadata: result.getPrograms()){
	    String title = metadata.getTitle();	    
	    title = StringUtilities.sanitize(title);
	    int count = (metadata.getHistory()).length;
	    String type = metadata.getHumanFriendlyTypeString();

	    String[] tagsArr = metadata.getCategoryNames(libraryController.getMetadataProvider().getMediaLib());
	    StringUtilities.sanitize(tagsArr);
	    String tags = StringUtilities.getCSVFromArray(tagsArr);

	    String[] contactsArr = metadata.getCreatorNames(libraryController.getMetadataProvider().getMediaLib());
	    StringUtilities.sanitize(contactsArr);
	    String contacts = StringUtilities.getCSVFromArray(contactsArr);

	    entries.add(title + separator + count + separator + type + separator + tags + separator + contacts);
	}
	

	if(FileUtilities.writeToXLS(destFile, entries, separator, true)) {
	    FileUtilities.setPermissions(destFile, false);
	    JOptionPane.showMessageDialog(null, "Report created successfully at: " + destFile, "information", JOptionPane.INFORMATION_MESSAGE);
	} else {
	    JOptionPane.showMessageDialog(null, "Report creation failed!!", "information", JOptionPane.INFORMATION_MESSAGE);
	}
    }

    protected void exportFiles(final String sessionID){
	if (getLibraryTab(sessionID).getResultList().getSelectedValues().length == 0) {
	    JOptionPane.showMessageDialog(rsApp.getControlPanel().cpJFrame(), 
					  "No files selected for export!!", "Export File(s)", 
					  JOptionPane.WARNING_MESSAGE);
	    return;
	}

	final JFileChooser fileChooser = new JFileChooser(stationConfiguration.getUserDesktopPath());
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setBackground(stationConfiguration.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	fileChooser.updateUI();

	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int retVal = fileChooser.showDialog(cpJFrame, "Export");

	if (retVal != JFileChooser.APPROVE_OPTION)
	    return;

	String destFolder = null;
	try{
	    destFolder = fileChooser.getSelectedFile().getCanonicalPath()+System.getProperty("file.separator");
	} catch (Exception e){
	    logger.error("ExportFiles: Unable to find canonical path of selected folder.");
	    return;
	}
	
	final String destFolderFinal = destFolder;
	final ArrayList<RadioProgramMetadata> programs = new ArrayList<RadioProgramMetadata>();

	for (Object obj: getLibraryTab(sessionID).getResultList().getSelectedValues())
	    programs.add((RadioProgramMetadata) obj);
		
	
	Thread t = new Thread("LibraryExport"){
		public void run(){
		    FileStoreDownloader fsDownloader = new FileStoreDownloader(stationConfiguration);
		    fsDownloader.exportFiles(programs.toArray(new RadioProgramMetadata[0]), destFolderFinal);
		}
	    };
	t.start();
    }

    public class LibraryTab {
	String sessionID;
	int position;
	JPanel tabPanel;
	JList resultList;
	SearchTableModel searchTableModel;
	JScrollPane resultSP;
	JTextField searchField;
	JPanel displayFilterPanel;
	RSScrollList activeFilterList;
	ResultCellRenderer cellRenderer;
	RSButton searchButton;
	RSButton editButton, statsButton, listButton;
	JPanel statsPanel;
	GridBagConstraints statsPanelGridConstraints, resultSPGridConstraints;
	Dimension statsPanelSize;
	RSMultiList resultMultiList;
	ArrayList<RadioProgramMetadata> dataList;
	
	public LibraryTab(String sessionID) {
	    this.sessionID = sessionID;
	}

	public void setSessionID(String sessionID) {
	    this.sessionID = sessionID;
	}

	public String getSessionID() {
	    return sessionID;
	}

	public void setPosition(int position) {
	    this.position = position;
	}

	public int getPosition() {
	    return position;
	}

	public void setPanel(JPanel tabPanel) {
	    this.tabPanel = tabPanel;
	}

	public JPanel getPanel() {
	    return tabPanel;
	}

	public JList getResultList() {
	    return resultList;
	}

	public SearchTableModel getSearchTableModel() {
	    return searchTableModel;
	}

	public void setSearchTableModel(SearchTableModel searchTableModel) {
	    this.searchTableModel = searchTableModel;
	}

	public void setResultList(JList resultList) {
	    this.resultList = resultList;
	}

	public JScrollPane getResultSP() {
	    return resultSP;
	}

	public void setResultSP(JScrollPane resultSP) {
	    this.resultSP = resultSP;
	}

	public GridBagConstraints getResultSPGridConstraints() {
	    return resultSPGridConstraints;
	}

	public void setResultSPGridConstraints(GridBagConstraints c) {
	    resultSPGridConstraints = c;
	}

	public JPanel getStatsPanel() {
	    return statsPanel;
	}

	public void setStatsPanel(JPanel statsPanel) {
	    this.statsPanel = statsPanel;
	}

	public GridBagConstraints getStatsPanelGridConstraints() {
	    return statsPanelGridConstraints;
	}

	public void setStatsPanelGridConstraints(GridBagConstraints c) {
	    statsPanelGridConstraints = c;
	}

	public void setStatsPanelSize(Dimension statsPanelSize) {
	    this.statsPanelSize = statsPanelSize;
	}

	public Dimension getStatsPanelSize() {
	    return statsPanelSize;
	}

	public void setSearchField(JTextField searchField) {
	    this.searchField = searchField;
	}

	public JTextField getSearchField() {
	    return searchField;
	}

	public String getSearchText() {
	    return searchField.getText();
	}

	public void clearSearchText() {
	    searchField.setText("");
	}

	public ArrayList<RadioProgramMetadata> getDataList() {
	    return dataList;
	}

	public void setDataList(ArrayList<RadioProgramMetadata> dataList) {
	    this.dataList = dataList;
	}	

	public void setDisplayFilterPanel(JPanel dfPanel) {
	    this.displayFilterPanel = dfPanel;
	}

	public JPanel getDisplayFilterPanel() {
	    return displayFilterPanel;
	}

	public void setActiveFilterList(RSScrollList afList) {
	    this.activeFilterList = afList;
	}

	public RSScrollList getActiveFilterList() {
	    return activeFilterList;
	}

	public ResultCellRenderer getCellRenderer() {
	    return cellRenderer;
	}

	public void setCellRenderer(ResultCellRenderer cellRenderer) {
	    this.cellRenderer = cellRenderer;
	}

	public RSButton getSearchButton() {
	    return searchButton;
	}

	public void setSearchButton(RSButton searchButton) {
	    this.searchButton = searchButton;
	}

	public void setEditButton(RSButton editButton) {
	    this.editButton = editButton;
	}

	public RSButton getEditButton() {
	    return editButton;
	}

	public void setStatsButton(RSButton statsButton) {
	    this.statsButton = statsButton;
	}

	public RSButton getStatsButton() {
	    return statsButton;
	}

	public void setListButton(RSButton listButton) {
	    this.listButton = listButton;
	}

	public RSButton getListButton() {
	    return listButton;
	}

	public void setResultMultiList(RSMultiList resultMultiList) {
	    this.resultMultiList = resultMultiList;
	}

	public RSMultiList getResultMultiList() {
	    return resultMultiList;
	}

    }

    public class SearchTableModel implements CacheObjectListener, ListModel, MultiListModel {
	protected ArrayList<RadioProgramMetadata> dataList;
	protected ArrayList<ListDataListener> listeners;
	protected JList list;
	protected RSMultiList multiList;
	protected int height;
	protected int width;
	protected LibraryWidget widget;

	public SearchTableModel(ArrayList<RadioProgramMetadata> dataList) {
	    this.dataList = dataList;
	    for (RadioProgramMetadata program: dataList)
		libraryController.getCacheProvider().addCacheObjectListener(program, this);
	    listeners = new ArrayList<ListDataListener>();
	}

	public void setMultiList(RSMultiList multiList) {
	    this.multiList = multiList;
	}

	public void setList(JList list) {
	    this.list = list;
	}

	public void setWidget(LibraryWidget widget) {
	    this.widget = widget;
	}

	public void setRowHeight(int height) {
	    this.height = height;
	}

	public void setRowWidth(int width) {
	    this.width = width;
	}

	public synchronized void cacheObjectValueChanged(Metadata metadata) {
	    RadioProgramMetadata program = (RadioProgramMetadata)metadata;
	    int i = 0;
	    //System.out.println("---- libraryWidget: value changed: " + metadata);
	    for (RadioProgramMetadata data: dataList) {
		if (data.getItemID().equals(program.getItemID())) {
		    for (ListDataListener listener: listeners) {
			listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, i, i));
		    }
		    RadioProgramMetadata oldProgram = dataList.remove(i);

		    dataList.add(i, program);

		    multiList.replace(oldProgram, program);

		    for (ListDataListener listener: listeners) {
			listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, i, i));
		    }

		    //		    table.tableChanged(new TableModelEvent(this, i, i));
		    break;
		}
		i++;
	    }
	}

	//Assuming either all are key updates or all are key deletes
	//whether it is update or delete is identified by the first key
	//Rework this method if and when the assumption does not hold anymore
	public synchronized void cacheObjectKeyChanged(Metadata[] metadata, String[] newKeys) {
	    String firstNewItem = newKeys[0].substring(0, newKeys[0].lastIndexOf("."));
	    if (firstNewItem.equals(""))
		cacheObjectKeysDeleted(metadata);
	    else
		cacheObjectKeysUpdated(metadata, newKeys);
	}
	
	public synchronized void cacheObjectKeysDeleted(Metadata[] metadata) {
	    for (Metadata m : metadata) {
		RadioProgramMetadata program = (RadioProgramMetadata)m;
		widget.removeProgramFromAllSessions(program.getItemID());
	    }

	    for (int j = 0; j < metadata.length; j++) {
		final RadioProgramMetadata program = (RadioProgramMetadata)metadata[j];

		logger.debug("CacheObjectKeyDeleted: old object: "+metadata[j]);

		Runnable r = new Runnable(){
			public void run(){

			    int i = 0;
			    for (RadioProgramMetadata data: dataList) {
				if (data.getItemID().equals(program.getItemID())) {
				    for (ListDataListener listener: listeners) {
					listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, i, i));
				    }
				    RadioProgramMetadata oldProgram = dataList.remove(i);
				    //multiList.remove(i, oldProgram);
				    multiList.remove(i, false);
				    break;
				}
				i++;
			    }
			}
		    };
		
		try{
		    SwingUtilities.invokeLater(r);
		} catch (Exception e){
		    logger.error("CacheObjectKeyChanged: Could not remove from list.", e);
		}
	    }
	}

	public synchronized void cacheObjectKeysUpdated(Metadata[] metadata, String[] newKeys) {
	    for (int j = 0; j < metadata.length; j++) {
		final RadioProgramMetadata program = (RadioProgramMetadata)metadata[j];
		final String newItemID = newKeys[j].substring(0, newKeys[j].lastIndexOf("."));
		RadioProgramMetadata dummyProgram, newProgram = null;
		final Metadata mdata;

		logger.debug("CacheObjectKeyUpdated: old object: "+metadata[j]+" new key: "+newKeys[j]);

		dummyProgram = RadioProgramMetadata.getDummyObject(newItemID);
		mdata = libraryController.getCacheProvider().getSingle(dummyProgram);
		if (mdata == null)
		    return;
		newProgram = (RadioProgramMetadata)mdata;

		final RadioProgramMetadata nProgram = newProgram;
		
		Runnable r = new Runnable(){
			public void run(){

			    int i = 0;
			    for (RadioProgramMetadata data: dataList) {
				if (data.getItemID().equals(program.getItemID())) {
				    for (ListDataListener listener: listeners) {
					listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, i, i));
				    }
				    RadioProgramMetadata oldProgram = dataList.remove(i);
				    dataList.add(i, nProgram);
				    multiList.replace(oldProgram, nProgram);
				    
				    for (ListDataListener listener: listeners) {
					listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, i, i));
				    }
				    break;
				}
				i++;
			    }
			    
			}
		    };
		
		try {
		    SwingUtilities.invokeLater(r);
		} catch (Exception e){
		    logger.error("CacheObjectKeyChanged: Could not remove from list.", e);
		}
	    }
	}

	public void removeCacheObjectListeners() {
	    for (RadioProgramMetadata data: dataList) {
		libraryController.getCacheProvider().removeCacheObjectListener(data, this);
	    }
	}

	public void clear() {
	    int size = getSize();
	    removeCacheObjectListeners();
	    dataList.clear();
	    for (ListDataListener listener: listeners){
		listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, 0, size));
	    }
	}

	public void add(Object obj) {
	    RadioProgramMetadata data = (RadioProgramMetadata)obj;
	    dataList.add(data);
	    libraryController.getCacheProvider().addCacheObjectListener(data, this);

	    list.setPreferredSize(new Dimension(width, height * dataList.size()));

	    for (ListDataListener listener: listeners) {
		listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, dataList.size() - 1, dataList.size() - 1));
	    }
	}

	public void add(int index, Object obj) {
	    RadioProgramMetadata data = (RadioProgramMetadata)obj;
	    dataList.add(index, data);
	    libraryController.getCacheProvider().addCacheObjectListener(data, this);

	    list.setPreferredSize(new Dimension(width, height * dataList.size()));

	    for (ListDataListener listener: listeners) {
		listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, index, index));
	    }
	}

	// Invoked by rsmultilist but no action required
	public void removeFromMultiList(int index) {

	}

	public int getSize() {
	    return dataList.size();
	}

	public Object getElementAt(int row) {
	    return dataList.get(row);
	}

	public void addListDataListener(ListDataListener listener) {
	    listeners.add(listener);
	}

	public void removeListDataListener(ListDataListener listener) {
	    listeners.remove(listener);
	}

    }

    public class SearchTransferHandler extends TransferHandler {
	ArrayList<RadioProgramMetadata> dataList;

	public SearchTransferHandler(ArrayList<RadioProgramMetadata> dataList) {
	    this.dataList = dataList;
	}

	public int getSourceActions(JComponent c){
	    return COPY;
	}
	
	public Transferable createTransferable (JComponent c){
	    ArrayList<RadioProgramMetadata> list = new ArrayList<RadioProgramMetadata>();
	    RadioProgramMetadata rpm;

	    for (Object program: ((JList)c).getSelectedValues()) {
		rpm = (RadioProgramMetadata)program;
		logger.info("GRINS_USAGE:LIBRARY_DRAG:" + rpm.getItemID());
		list.add(rpm);
	    }
	    
	    return new ProgramSelection(list);
	}
	
	public void exportDone(JComponent c, Transferable t, int action){
	    
	}
    }

    static final int resultTitleHF = 50;
    static final int resultTagsHF = 20;
    static final int resultDurationHF = 30;
    static final int resultInfoHF = 14;

    static final int resultSnippetHF = 79;
    static final int resultPreviewHF = 7;
    static final int resultStopHF = 7;

    public class ResultCellRenderer extends JPanel implements ListCellRenderer {
	Dimension resultSize, typeLabelSize;
	Color bgColor;
	String sessionID;
	String currentItemID = "";
	RSLabel titleLabel, tagsLabel, durationLabel, typeLabel;
	RSLabel resultLabel;
	RSButton infoButton, previewStopButton;
	RSButton previewStartButton;

	boolean infoClicked = false;
	boolean previewClicked = false;
	boolean stopClicked = false;

	RSButton selectedButton = null;
	int selectedIndex = -1;

	public ResultCellRenderer(Dimension resultSize, String sessionID) {
	    this.resultSize = resultSize;
	    this.sessionID = sessionID;

	    setPreferredSize(resultSize);
	    setMinimumSize(resultSize);
	    setSize(resultSize);

	    setLayout(new GridBagLayout());

	    titleLabel = new RSLabel("");
	    titleLabel.setForeground(stationConfiguration.getColor(StationConfiguration.LISTITEM_TITLE_COLOR));
	    titleLabel.setPreferredSize(new Dimension((int)(resultSize.getWidth() * resultTitleHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    titleLabel.setMinimumSize(new Dimension((int)(resultSize.getWidth() * resultTitleHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    add(titleLabel, getGbc(0, 0));

	    tagsLabel = new RSLabel("");
	    tagsLabel.setPreferredSize(new Dimension((int)(resultSize.getWidth() * resultTagsHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    tagsLabel.setMinimumSize(new Dimension((int)(resultSize.getWidth() * resultTagsHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    add(tagsLabel, getGbc(1, 0));
	    
	    durationLabel = new RSLabel("");
	    durationLabel.setPreferredSize(new Dimension((int)(resultSize.getWidth() * resultDurationHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    durationLabel.setMinimumSize(new Dimension((int)(resultSize.getWidth() * resultDurationHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    add(durationLabel, getGbc(2, 0, gridwidth(4)));

	    typeLabel = new RSLabel();
	    typeLabelSize = new Dimension((int)(resultSize.getWidth() * resultPreviewHF / 100.0),
							      (int)(resultSize.getHeight() / 2));
	    typeLabel.setPreferredSize(typeLabelSize);
	    typeLabel.setMinimumSize(typeLabelSize);
	    add(typeLabel, getGbc(2, 1));

	    infoButton = new RSButton("");
	    infoButton.setPreferredSize(new Dimension((int)(resultSize.getWidth() * resultInfoHF / 100.0 / 2), (int)(resultSize.getHeight() / 2)));
	    infoButton.setMinimumSize(new Dimension((int)(resultSize.getWidth() * resultInfoHF / 100.0 / 2), (int)(resultSize.getHeight() / 2)));
	    infoButton.addActionListener(libraryController);
	    infoButton.setToolTipText("Info");
	    add(infoButton, getGbc(5, 1));
	    
	    resultLabel = new RSLabel("");
	    resultLabel.setForeground(stationConfiguration.getColor(StationConfiguration.LISTITEM_DETAILS_COLOR));
	    resultLabel.setPreferredSize(new Dimension((int)(resultSize.getWidth() * resultSnippetHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    resultLabel.setMinimumSize(new Dimension((int)(resultSize.getWidth() * resultSnippetHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    add(resultLabel, getGbc(0, 1, gridwidth(3)));
	    
	    previewStartButton = new RSButton("");
	    previewStartButton.setPreferredSize(new Dimension((int)(resultSize.getWidth() * resultPreviewHF / 100.0),
							      (int)(resultSize.getHeight() / 2)));
	    previewStartButton.setMinimumSize(new Dimension((int)(resultSize.getWidth() * resultPreviewHF / 100.0),
	    						      (int)(resultSize.getHeight() / 2)));
	    previewStartButton.addActionListener(libraryController);
	    previewStartButton.setToolTipText("Preview");
	    add(previewStartButton, getGbc(3, 1));

	    previewStopButton = new RSButton("");
	    previewStopButton.setPreferredSize(new Dimension((int)(resultSize.getWidth() * resultStopHF / 100.0),
							     (int)(resultSize.getHeight() / 2)));
	    previewStopButton.setMinimumSize(new Dimension((int)(resultSize.getWidth() * resultStopHF / 100.0),
	    						     (int)(resultSize.getHeight() / 2)));
	    previewStopButton.addActionListener(libraryController);
	    previewStopButton.setToolTipText("Stop");
	    add(previewStopButton, getGbc(4, 1));

	    bgColor = stationConfiguration.getColor(StationConfiguration.LIBRARY_LISTITEM_COLOR);
	    setBackground(bgColor);
	    titleLabel.setBackground(bgColor);
	    tagsLabel.setBackground(bgColor);
	    durationLabel.setBackground(bgColor);
	    resultLabel.setBackground(bgColor);
	    
	    Color buttonBgColor = stationConfiguration.getColor(StationConfiguration.SEARCHCLEAR_BUTTON_COLOR);
	    infoButton.setBackground(buttonBgColor);
	    previewStartButton.setBackground(buttonBgColor);
	    previewStopButton.setBackground(buttonBgColor);

	    infoButton.setIcon(getIcon(StationConfiguration.INFO_ICON,
						     (int)(resultSize.getWidth() * resultInfoHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    previewStartButton.setIcon(getIcon(StationConfiguration.PREVIEW_ICON,
							     (int)(resultSize.getWidth() * resultPreviewHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    previewStopButton.setIcon(getIcon(StationConfiguration.PREVIEW_STOP_ICON,
							    (int)(resultSize.getWidth() * resultStopHF / 100.0), (int)(resultSize.getHeight() / 2)));
	    
	    setBorder(new GradientBorder(stationConfiguration.getColor(StationConfiguration.LIST_ITEM_BORDER_COLOR)));
	}

	public Component getListCellRendererComponent(JList list, Object metadata,int row, boolean isSelected, boolean hasFocus) {
	    RadioProgramMetadata program = (RadioProgramMetadata)metadata;

	    if (isSelected) {
		//currentItemID = program.getItemID();
		setSelected();
	    } else {
		setUnSelected();
	    }
	    
	    titleLabel.setText(" "+program.getTitle());
	    titleLabel.setToolTipText(program.getTitle());
	    durationLabel.setText(StringUtilities.durationStringFromMillis(program.getLength()));

	    if (program.getTags() != null && program.getTags().length > 0){
		tagsLabel.setText(program.getTags()[0].getTagsCSV());
	    } else {
		tagsLabel.setText("");
	    }

	    Creator[] creators = program.getCreators();
	    StringBuilder str = new StringBuilder();
	    if (creators != null && creators.length > 0) {
		for (int i = 0; i < Math.min(creators.length, 2); i++) {
		    if (!creators[i].getEntity().getName().equals(""))
			str.append(creators[i].getEntity().getName() + ", ");
		    if (!creators[i].getEntity().getLocation().equals(""))
			str.append(" (" + creators[i].getEntity().getLocation() + "), ");
		}
	    }
	    
	    if (program.getCategories() != null && program.getCategories().length > 0) {
		str.append("Categories: ");
		for (int i = 0; i < program.getCategories().length; i++) {
		    Category category = program.getCategories()[i].getCategory();
		    if (category != null)
			str.append(category.getLabel() + ", ");
		}
	    }

	    if (program.getLanguage() != null) {
		str.append("Language: ");
		str.append(program.getLanguage() + ", ");
	    }

	    String strstr = str.toString();
	    if (strstr.endsWith(", ")) {
		resultLabel.setText("  "+strstr.substring(0, strstr.length() - 2));
		resultLabel.setToolTipText(strstr.substring(0, strstr.length() - 2));
	    } else
		resultLabel.setText("");

	    typeLabel.setIcon(GUIUtilities.getIcon(program, typeLabelSize.width, typeLabelSize.height));

	    if (program.getPreviewStoreAttempts() != FileStoreManager.UPLOAD_DONE) {
		previewStartButton.setEnabled(false);
		previewStopButton.setEnabled(false);
	    } else {
		previewStartButton.setEnabled(previewActive && previewUIEnabled);
		previewStopButton.setEnabled(previewActive && previewUIEnabled);
	    }
	    infoButton.setEnabled(infoActive);
	    
	    infoButton.setPressed(row == selectedIndex && selectedButton == infoButton);
	    previewStopButton.setPressed(previewClicked && 
					 row == selectedIndex && selectedButton == previewStopButton &&
					 currentItemID.equals(program.getItemID()));

	    if (currentItemID.equals(program.getItemID())){
		previewStartButton.setPressed(previewClicked);
	    } else {
		previewStartButton.setPressed(!previewClicked && row == selectedIndex && selectedButton == previewStartButton);
	    }
		
	    return this;
	}

	protected void setSelected(){
	    setBackground(stationConfiguration.getColor(StationConfiguration.LIBRARY_LISTITEM_SELECT_COLOR));
	}

	protected void setUnSelected(){
	    setBackground(bgColor);
	}

	public void setPreviewPressed(boolean pressed, String programID){
	    previewClicked = pressed;
	    currentItemID = programID;
	}

	protected JComponent getButtonAt(Point p){
	    setLocation(0, 0);
	    setSize(resultSize);

	    int y = (int)p.getY()%(int)getPreferredSize().getHeight();
	    int x = (int)p.getX();
		    
	    Component button = getComponentAt(x, y);
	    if (button instanceof RSButton)
		return ((RSButton)button);
	    else if (button instanceof RSToggleButton)
		return ((RSToggleButton)button);
	    else
		return null;
	}

	public void mouseClicked(MouseEvent e, SearchTableModel model){
	    int index = e.getY()/resultSize.height;
	    //currentItemID = (String) ((RadioProgramMetadata)model.getElementAt(index)).getItemID();
	    
	    JComponent button = getButtonAt(e.getPoint());
	    if (button instanceof RSButton && button == infoButton){
		//infoClicked = true;
		String currentItemID = (String) ((RadioProgramMetadata)model.getElementAt(index)).getItemID();
		infoButton.setActionCommand(INFO + "_" + sessionID + "_" + currentItemID);
		infoButton.doClick();
	    } else if (button instanceof RSButton && button == previewStartButton){
		//previewClicked = true;
		String currentItemID = (String) ((RadioProgramMetadata)model.getElementAt(index)).getItemID();
		previewStartButton.setActionCommand(PREVIEW + "_" + sessionID + "_" + currentItemID);
		previewStartButton.doClick();
	    } else if (button instanceof RSButton && button == previewStopButton){
		//stopClicked = true;
		String currentItemID = (String) ((RadioProgramMetadata)model.getElementAt(index)).getItemID();
		previewStopButton.setActionCommand(STOP + "_" + sessionID + "_" + currentItemID);
		previewStopButton.doClick();
	    } else if (button == null){
		if (e.getClickCount() == 2){
		    String currentItemID = (String) ((RadioProgramMetadata)model.getElementAt(index)).getItemID();
		    infoButton.setActionCommand(INFO + "_" + sessionID + "_" + currentItemID);
		    infoButton.doClick();
		}
	    }
	    
	}

	public void mousePressed(MouseEvent e, int index){
	    selectedIndex = index;
	    selectedButton = (RSButton) getButtonAt(e.getPoint());
	}

	public void mouseReleased(MouseEvent e, int index){
	    selectedButton = null;
	}

	public void mouseExited(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {
	    
	}

	public RSButton getInfoButton() {
	    return infoButton;
	}

	public RSButton getPreviewButton() {
	    return previewStartButton;
	}

	public RSButton getStopButton() {
	    return previewStopButton;
	}

    }
    
}


