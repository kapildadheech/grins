package org.gramvaani.radio.app.gui;

import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.radio.stationconfig.*;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.image.RGBImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageObserver;

import java.util.ArrayList;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class ControlPanelJFrame extends JFrame {
    public static int bottomMenuVertFrac = 10;
    public static int topMenuVertFrac = 5;
    public static int playlistPanelHorizFrac = 25;
    public static int workspacePanelHorizFrac = 60;

    protected ControlPanel controlPanel;
    protected StationConfiguration stationConfig;

    protected BottomMenuPanel bottomMenuPanel;
    protected TopMenuPanel topMenuPanel;
    protected JPanel  playlistPanel;
    protected WorkspacePanel  workspacePanel;
    protected ActiveScrollPanel activeScrollPanel;

    protected ArrayList<RSWidgetBase> widgetList;
    protected Dimension wpSize, aspItemSize, playlistPanelDim;

    protected LogUtilities logger;
    protected RSWidgetBase defaultWidget;
    protected static ControlPanelJFrame cpJFrame = null;
    
    public ControlPanelJFrame(ControlPanel controlPanel) {
	super("Gramin Radio Inter Networking System v0.1");
	this.controlPanel = controlPanel;
	this.stationConfig = controlPanel.getRSApp().getStationConfiguration();
	logger = new LogUtilities("ControlPanelJFrame");

	setUndecorated(true);
	setBounds(0, 0, controlPanel.getScreenXsize(), controlPanel.getScreenYsize());
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setResizable(false);
	setLayout(new GridBagLayout());

	widgetList = new ArrayList<RSWidgetBase>();
	topMenuPanel = new TopMenuPanel(this);
	topMenuPanel.init(new Dimension(controlPanel.getScreenXsize(), (int)(controlPanel.getScreenYsize() * topMenuVertFrac / 100.0)));
	add(topMenuPanel, getGbc(0, 0, gridwidth(3), gfillboth()));

	
	playlistPanel = new JPanel();
	playlistPanel.setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	playlistPanelDim = new Dimension((int)(controlPanel.getScreenXsize() * playlistPanelHorizFrac / 100.0),
					 (int)(controlPanel.getScreenYsize() * (100 - bottomMenuVertFrac - topMenuVertFrac) / 100.0));
					 //(int)(controlPanel.getScreenYsize() * (100 - topMenuVertFrac) / 100.0));
	add(playlistPanel, getGbc(0, 1, gfillboth()));
	//add(playlistPanel, getGbc(0, 1, gridheight(2), gfillboth()));
	
	workspacePanel = new WorkspacePanel(this);
	workspacePanel.setComponentSize((int)(controlPanel.getScreenXsize() * workspacePanelHorizFrac / 100.0), 
					(int)(controlPanel.getScreenYsize() * (100 - bottomMenuVertFrac - topMenuVertFrac) / 100.0));
	add(workspacePanel, getGbc(1, 1, gfillboth()));
	
	activeScrollPanel = new ActiveScrollPanel(this);
	activeScrollPanel.calculateComponentSize((int)(controlPanel.getScreenXsize() * (100 - playlistPanelHorizFrac - workspacePanelHorizFrac) / 100.0), 
						 (int)(controlPanel.getScreenYsize() * (100 - bottomMenuVertFrac - topMenuVertFrac) / 100.0));
	
	add(activeScrollPanel, getGbc(2, 1, gfillboth()));
	
	wpSize = workspacePanel.getComponentSize();
	aspItemSize = activeScrollPanel.getComponentSize();
	
	bottomMenuPanel = new BottomMenuPanel(this, (int)(controlPanel.getScreenYsize() * bottomMenuVertFrac / 100.0));
	add(bottomMenuPanel, getGbc(0, 2, gridwidth(3), gfillboth()));
	//add(bottomMenuPanel, getGbc(1, 2, gridwidth(2), gfillboth()));
    

	setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));

	Runtime.getRuntime().addShutdownHook(new Thread() {
		public void run() {
		    unloadWidgets();
		}
	    });

	cpJFrame = this;
    }

    public static ControlPanelJFrame getInstance(){
	return cpJFrame;
    }

    public StationConfiguration getStationConfiguration(){
	return stationConfig;
    }

    public ControlPanel getControlPanel() {
	return controlPanel;
    }

    public BottomMenuPanel getBottomMenuPanel() {
	return bottomMenuPanel;
    }

    public TopMenuPanel getTopMenuPanel() {
	return topMenuPanel;
    }

    public JPanel getPlaylistPanel() {
	return playlistPanel;
    }

    public Dimension getPlaylistPanelDim() {
	return playlistPanelDim;
    }

    public int getASPwidth() {
	return (int)(controlPanel.getScreenXsize() * (100 - playlistPanelHorizFrac - workspacePanelHorizFrac) / 100.0);
    }

    public WorkspacePanel getWorkspacePanel() {
	return workspacePanel;
    }
    
    public ActiveScrollPanel getActiveScrollPanel() {
	return activeScrollPanel;
    }

    public void minimizeWindow(){
	setExtendedState(Frame.ICONIFIED);
    }

    @SuppressWarnings("unchecked")
    void unloadWidgets() {
	ArrayList<RSWidgetBase> list = (ArrayList<RSWidgetBase>) widgetList.clone();
	for (RSWidgetBase widget: list) {
	    unloadWidget(widget);
	}
    }
	
    public void closeApplication(){
	unloadWidgets();
	System.exit(0);
    }

    boolean isSimulationMode;
    public synchronized void setSimulationMode(boolean isSimulationMode){
	this.isSimulationMode = isSimulationMode;
	for (RSWidgetBase widget: widgetList){
	    if (widget instanceof HotkeyWidget){
		((HotkeyWidget)widget).setSimulationMode(isSimulationMode);
	    }
	}
    }

    public synchronized boolean getSimulationMode(){
	return isSimulationMode;
    }

    public void setupWidget(RSWidgetBase widget, boolean initBottomPanel){
	try{
	    widget.setControlPanelJFrame(this);

	    if (initBottomPanel)
		bottomMenuPanel.addWidget(widget);	
	} catch (Exception e){
	    logger.error("SetupWidget: Encountered exception:", e);
	}
    }

    protected RSWidgetBase currentWidget(){
	if (widgetList.size() == 0)
	    return null;
	else
	    return widgetList.get(widgetList.size() - 1);
    }

    protected RSWidgetBase previousWidget(){
	if (widgetList.size() > 1)
	    return widgetList.get(widgetList.size() - 2);
	else
	    return null;
    }

    protected void addToOrdering(RSWidgetBase widget){
	widgetList.add(widget);
    }

    protected void removeFromOrdering(RSWidgetBase widget){
	widgetList.remove(widget);
    }

    protected void bringToFront(RSWidgetBase widget){
	widgetList.remove(widget);
	widgetList.add(widget);
    }

    protected void setDefaultWidget(RSWidgetBase widget){
	try{
	    widget.setControlPanelJFrame(this);
	    widget.launch(wpSize, aspItemSize);
	    workspacePanel.setDefaultWidget(widget);
	    activeScrollPanel.setDefaultWidget(widget);
	    activeScrollPanel.widgetLaunched(widget);
	    setWidgetMaximized(widget);

	    defaultWidget = widget;
	} catch (Exception e){
	    logger.error("SetDefaultWidget: Unable to initialize default widget: "+widget.getName(), e);
	}
    }

    public void launchWidget(RSWidgetBase widget){
	if (widget.isLaunched())
	    return;
	boolean status = false;
	try{
	    status = widget.launch(wpSize, aspItemSize);
	} catch (Exception e) {
	    logger.error("LaunchWidget: Unable to launch widget: "+widget.getName(), e);
	} finally {
	    if (!status)
		if (widget.canUnload()){
		    widget.unload();
		    if (widget.isBottomMenuActive())
			bottomMenuPanel.unloadWidget(widget);
		}	    
	}

	if (status){
	    workspacePanel.addWidget(widget);
	    activeScrollPanel.addWidget(widget);
	    activeScrollPanel.widgetLaunched(widget);
	    
	    minimizeWidget(currentWidget());
	    addToOrdering(widget);
	    setWidgetMaximized(widget);
	}
    }

    private void maximizeWidget(RSWidgetBase widget){
	if (!widget.isLaunched()){
	    logger.error("MaximizeWidget: Trying to maximize unlaunched widget: "+widget.getName());
	    return;
	}
	    
	bringToFront(widget);
	widget.maximize();
	workspacePanel.setWidget(widget);
	activeScrollPanel.maximizeWidget(widget);
    }

    private void minimizeWidget(RSWidgetBase widget){
	if (widget == null || !widget.isMaximized())
	    return;
	widget.minimize();
	activeScrollPanel.minimizeWidget(widget);
    }

    public void setWidgetMaximized(RSWidgetBase widget){
	if (widget == null){
	    logger.error("SetWidgetMaximized: Received null widget.");
	    return;
	}
	if (!widget.isMaximized()){
	    minimizeWidget(currentWidget());
	    maximizeWidget(widget);
	} 
    }
    
    public void setWidgetMinimized(RSWidgetBase widget){
	if (widget.isMaximized())
	    if (previousWidget() != null){
		minimizeWidget(widget);
		maximizeWidget(previousWidget());
	    }
    }

    public void unloadWidget(RSWidgetBase widget){
	if (!(widget.isLaunched() && widget.canUnload()))
	    return;

	if (currentWidget() == widget){
	    if (activeScrollPanel.getWidgetCount() > 1)
		setWidgetMaximized(previousWidget());
	    else
		workspacePanel.showDefaultView();
	} 
	
	minimizeWidget(widget);
	workspacePanel.removeWidget(widget);
	activeScrollPanel.removeWidget(widget);
	if (widget.isBottomMenuActive())
	    bottomMenuPanel.unloadWidget(widget);
	widget.unload();
	removeFromOrdering(widget);
    }

    public void displayError(final String errorType, final String errorMessage){
	topMenuPanel.addErrorMessage(errorType, errorMessage);
    }

    public void notifyPlayout(String programID, long startTime){
	if (defaultWidget instanceof LogViewWidget){
	    LogViewWidget logView = (LogViewWidget) defaultWidget;
	    logView.insertProgram(programID, startTime);
	    workspacePanel.validate();
	    workspacePanel.repaint();
	}
    }

    public void entitiesChanged(){
	for (RSWidgetBase widget: widgetList){
	    if (widget instanceof ContactsWidget){
		((ContactsWidget) widget).entitiesChanged();
	    }
	}
    }

    Image image;
    BufferedImage frameImage;
    boolean isDisabled = false;
    Container cp;
    public void setDisabled(){
	if (isDisabled)
	    return;
	Dimension size = getSize();
	frameImage = new BufferedImage((int)size.getWidth(), (int)size.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
	Graphics g = frameImage.createGraphics();

	paint(g);

	RGBImageFilter filter = new RGBImageFilter(){
		public int filterRGB(int x, int y, int rgb){
		    int r = (rgb & 0xff0000) >> 17;
		    int g = (rgb & 0xff00) >> 9;
		    int b = (rgb & 0xff);

		    //return b << 16 | g << 8 | r;
		    //return 0xffffffff;
		    //System.out.println(rgb+","+b);
		    return rgb;
		}
		
	    };
	image = createImage(new FilteredImageSource(frameImage.getSource(),
						    filter));
	isDisabled = true;

	cp = getContentPane();
	ImagePane ip = new ImagePane(image);
	ip.setSize(controlPanel.getScreenXsize(), controlPanel.getScreenYsize());
	//ip.repaint();
	setContentPane(ip);
	repaint();
    }

    public void setEnabled(){
	if (!isDisabled)
	    return;
	setContentPane(cp);
	repaint();
    }
}

class ImagePane extends JPanel {
    Image image;
    public ImagePane(Image image){
	this.image = image;
    }

    public void paintComponent(Graphics g){
	Graphics2D g2 = (Graphics2D)g;
	g2.drawImage(image, 0, 0, this);
	g2.drawString("asdf", 100, 100);
    }
    
}