package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

class TopMenuPanel extends JPanel implements ActionListener, ErrorDisplayListener {
    public final static String TOP_MENU_PANEL = "TOP_MENU_PANEL";

    static String SIMULATION = "SIMULATION";
    static String NEXT_ERROR = "NEXT_ERROR";
    static String PREV_ERROR = "PREV_ERROR";

    protected ControlPanelJFrame cpJFrame;
    protected StationConfiguration stationConfig;

    boolean simulationActive = false;
    RSLabel simulationLabel;
    RSButton simulationButton;
    JPanel errorPanel;
    RSLabel blankLabel;

    RSLabel errorLabel;
    RSButton nextError;
    RSButton prevError;

    ErrorListModel errorListModel;
    int currError = -1;

    ErrorInference errorInference;

    public TopMenuPanel (ControlPanelJFrame cpJFrame){
	super();
	this.cpJFrame = cpJFrame;
	this.stationConfig = cpJFrame.getControlPanel().getRSApp().getStationConfiguration();
	IconUtilities.setStationConfig(stationConfig);

	errorListModel = new ErrorListModel();
	errorInference = ErrorInference.getErrorInference();
	errorInference.registerErrorDisplay(this);
    }

    static int smButtonFrac = 10;
    static int smLabelFrac = 15;
    static int errorPanelFrac = 60;
    static int windowButtonFrac = 2;
    static int blankLabelFrac = 96 - smButtonFrac - smLabelFrac - errorPanelFrac - 2*windowButtonFrac;

    static final String liveModeLabelText = "<html><center><b>Live Mode</b></center></html>";
    static final String simulationModeLabelText = "<html>   <b>Simulation Mode</b></html>";

    Color bgColor, liveModeColor;

    public void init(Dimension dim) {
	setLayout(new GridBagLayout());

	simulationLabel = new RSLabel(liveModeLabelText);
	Dimension smLabelDim;
	simulationLabel.setPreferredSize(smLabelDim = new Dimension((int)(dim.getWidth() * smLabelFrac / 100.0), (int)(dim.getHeight())));
	simulationLabel.setMinimumSize(smLabelDim);
	simulationLabel.setHorizontalAlignment(SwingConstants.CENTER);
	add(simulationLabel, getGbc(0, 0, gfillboth()));

	simulationButton = new RSButton("Switch modes");
	Dimension smButtonDim;
	simulationButton.setPreferredSize(smButtonDim = new Dimension((int)(dim.getWidth() * smButtonFrac / 100.0), (int)(dim.getHeight())));
	simulationButton.setMinimumSize(smButtonDim);
	simulationButton.setActionCommand(SIMULATION);
	simulationButton.addActionListener(this);
	Icon switchIcon = IconUtilities.getIcon(StationConfiguration.SWITCH_ICON, (int)smLabelDim.getHeight()/2, (int)smLabelDim.getHeight()/2);
	simulationButton.setIcon(switchIcon);

	add(simulationButton, getGbc(1, 0));

	errorPanel = new JPanel();
	errorPanel.setLayout(new GridBagLayout());
	Dimension errorDim;
	errorPanel.setPreferredSize(errorDim = new Dimension((int)(dim.getWidth() * errorPanelFrac / 100.0), (int)(dim.getHeight())));
	errorPanel.setMinimumSize(errorDim);
	add(errorPanel, getGbc(2, 0));

	errorLabel = new RSLabel("");
	errorLabel.setPreferredSize(new Dimension((int)(dim.getWidth() * errorPanelFrac / 100.0 * 0.87), (int)(dim.getHeight())));
	errorPanel.add(errorLabel, getGbc(0, 0, weightx(100.0)));
	
	prevError = new RSButton("");
	prevError.setPreferredSize(new Dimension((int)(dim.getWidth() * errorPanelFrac / 100.0 * 0.06), (int)(dim.getHeight())));
	prevError.setActionCommand(PREV_ERROR);
	prevError.addActionListener(this);
	prevError.setEnabled(false);
	errorPanel.add(prevError, getGbc(1, 0, gfillboth()));

	nextError = new RSButton("");
	nextError.setPreferredSize(new Dimension((int)(dim.getWidth() * errorPanelFrac / 100.0 * 0.06), (int)(dim.getHeight())));
	nextError.setActionCommand(NEXT_ERROR);
	nextError.addActionListener(this);
	nextError.setEnabled(false);
	errorPanel.add(nextError, getGbc(2, 0));

	bgColor = stationConfig.getColor(StationConfiguration.TOPPANE_COLOR);
	liveModeColor = bgColor;

	RSLabel blankLabel = new RSLabel("");
	Dimension blankDim;
	blankLabel.setPreferredSize(blankDim = new Dimension((int)(dim.getWidth() * blankLabelFrac / 100.0),
							     (int)(dim.getHeight()) - 3));

	blankLabel.setHorizontalAlignment(SwingConstants.CENTER);
	blankLabel.setForeground(bgColor.darker());
	blankLabel.setText("GRINS");
	blankLabel.setIcon(IconUtilities.getIcon(StationConfiguration.GRINS_ICON, (int)(blankDim.getWidth() * 0.5), (int)(blankDim.getHeight())));
	blankLabel.setToolTipText("Gramin Radio Inter Networking System v0.1");

	add(blankLabel, getGbc(3, 0, weightx(1.5)));

	
	RSButton appMinimizeButton = new RSButton("");
	Dimension buttonDim = new Dimension( dim.height, dim.height);
	appMinimizeButton.setPreferredSize(buttonDim);
	appMinimizeButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    cpJFrame.minimizeWindow();
		}
	    });
	appMinimizeButton.setIcon(IconUtilities.getIcon(StationConfiguration.MINIMIZE_ICON, (int)buttonDim.width, (int)buttonDim.height));

	add(appMinimizeButton, getGbc(4, 0));
       

	RSButton appCloseButton = new RSButton("");
	appCloseButton.setPreferredSize(buttonDim);
	appCloseButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    cpJFrame.closeApplication();
		}
	    });
	appCloseButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON, (int)buttonDim.width, (int)buttonDim.height));

	add(appCloseButton, getGbc(5, 0));
       
	nextError.setIcon(IconUtilities.getIcon(StationConfiguration.NEXT_ICON, 
						(int)(dim.getWidth() * errorPanelFrac / 100.0 * 0.04), (int)(dim.getHeight())));
	prevError.setIcon(IconUtilities.getIcon(StationConfiguration.PREV_ICON, 
						(int)(dim.getWidth() * errorPanelFrac / 100.0 * 0.04), (int)(dim.getHeight())));
	

	setBackground(bgColor);
	simulationLabel.setBackground(liveModeColor);
	simulationLabel.setOpaque(true);
	simulationButton.setBackground(stationConfig.getColor(StationConfiguration.SIMULATION_BUTTON_COLOR));
	//simulationButton.setBackground(bgColor.darker());
	errorPanel.setBackground(bgColor);
	errorLabel.setBackground(bgColor);
	prevError.setBackground(stationConfig.getColor(StationConfiguration.NEXTPREV_BUTTON_COLOR));
	nextError.setBackground(stationConfig.getColor(StationConfiguration.NEXTPREV_BUTTON_COLOR));
	blankLabel.setBackground(bgColor);
	
    }

    public void actionPerformed(ActionEvent e) {
	if(e.getActionCommand().equals(SIMULATION)) {
	    if(!simulationActive) {
		SimulationWidget simulationWidget = cpJFrame.getControlPanel().getSimulationWidget();
		if(simulationWidget == null) {
		    simulationWidget = new SimulationWidget(cpJFrame.getControlPanel(),
							    cpJFrame.getControlPanel().getRSApp());
		}
		cpJFrame.getControlPanel().initSimulationWidget(simulationWidget);
		//simulationButton.setText("Go to live mode");
		simulationLabel.setText(simulationModeLabelText);
		simulationLabel.setBackground(bgColor);
		simulationActive = true;
	    } else {
		PlaylistWidget playlistWidget = cpJFrame.getControlPanel().getPlaylistWidget();
		if(playlistWidget == null) {
		    playlistWidget = new PlaylistWidget(cpJFrame.getControlPanel(),
							cpJFrame.getControlPanel().getRSApp());
		}
		cpJFrame.getControlPanel().initPlaylistWidget(playlistWidget);
		//simulationButton.setText("Go to simulation mode");
		simulationLabel.setText(liveModeLabelText);
		simulationLabel.setBackground(liveModeColor);
		simulationActive = false;
	    }
	} else if(e.getActionCommand().equals(NEXT_ERROR) || e.getActionCommand().equals(PREV_ERROR)) {
	    if(e.getActionCommand().equals(NEXT_ERROR)) {
		currError ++;
	    } else {
		currError --;
	    }
	    
	    errorLabel.setText((String)(errorListModel.getElementAt(currError)));
	    errorLabel.setToolTipText((String)(errorListModel.getElementAt(currError)));

	    if(currError > 0)
		prevError.setEnabled(true);
	    else
		prevError.setEnabled(false);

	    if(currError < errorListModel.getSize() - 1)
		nextError.setEnabled(true);
	    else
		nextError.setEnabled(false);
	}
    }

    public String getName() {
	return TOP_MENU_PANEL;
    }

    public void displayError(String errorString) {
	addErrorMessage("", errorString);
    }

    public void addErrorMessage(String errorType, String errorMessage) {
	errorListModel.addErrorMessage(errorType.replaceAll("\n", ""), errorMessage.replaceAll("\n", ""));
    }


    boolean onDisplay;
    int numTimesFlash;
    Timer timer;
    public class ErrorListModel implements ListModel {
	ArrayList<ListDataListener> listeners;
	ArrayList<String> errorMessages;

	Timer clearTimer;
	final int clearErrorDelay = 10000;

	public ErrorListModel() {
	    listeners = new ArrayList<ListDataListener>();
	    errorMessages = new ArrayList<String>();
	    clearTimer = new Timer(clearErrorDelay, new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			clearTimer.stop();
			errorLabel.setText("");
			errorLabel.setToolTipText("");
			currError = errorMessages.size();
			prevError.setEnabled(true);
			nextError.setEnabled(false);
		    }
		});
	}
	
	public void addErrorMessage(String errorType, String errorMessage) {
	    String time = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date(System.currentTimeMillis()));
	    errorMessages.add((errorMessages.size() + 1) + ". " + time+": "+ (errorType.equals("") ? "" : errorType + " -- ") + errorMessage);
	    for(ListDataListener listener: listeners) {
		listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, errorMessages.size() - 1, errorMessages.size() - 1));
	    }

	    errorLabel.setText(errorMessages.get(currError = errorMessages.size() - 1));
	    errorLabel.setToolTipText(errorMessages.get(currError = errorMessages.size() - 1));

	    nextError.setEnabled(false);
	    if(currError > 0)
		prevError.setEnabled(true);

	    if(timer == null) {
		onDisplay = true;
		numTimesFlash = 0;
		timer = new Timer(500, new ErrorTimerFlash());
		timer.start();
	    }

	    clearTimer.restart();
	}

	public void addListDataListener(ListDataListener listener){
	    listeners.add(listener);
	}
	
	public Object getElementAt(int index){
	    if (index < errorMessages.size())
		return errorMessages.get(index);
	    else
		return null;
	}

	public int getSize(){
	    return errorMessages.size();
	}
	
	public void removeListDataListener(ListDataListener listener){
	    listeners.remove(listener);
	}

    }

    public class ErrorTimerFlash implements ActionListener {

	public void actionPerformed(ActionEvent e) {
	    if (onDisplay) {
		errorLabel.setText((String)(errorListModel.getElementAt(currError)));
	    } else {
		errorLabel.setText("");
	    }
	    onDisplay = !onDisplay;
	    numTimesFlash++;
	    if (numTimesFlash > 10 && timer != null) {
		timer.stop();
		timer = null;
	    }
	}
    }
    
}
