package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class TypeFilterWidget extends ActiveFilterWidget implements ActionListener {
    //MediaLib medialib;
    
    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;

    static final String nullItem = "Select resource type";

    JComboBox typeList;
    StationConfiguration stationConfig;
    Color bgColor;

    public TypeFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
			    LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	this.stationConfig = libraryController.getStationConfiguration();
	init();
    }

    protected void init() {
	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();
							  
	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, filterVertSize * 2 + borderHeight));	
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());

	add(titleLabel, getGbc(0, 0, weightx(100.0), gfillboth()));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));
	add(closeButton, getGbc(1, 0, gfillboth()));

	String[] types = ((TypeFilter)filter).getTypes(libraryController.getMetadataProvider());
	String[] typesOption = new String[types.length + 1];
	typesOption[0] = nullItem;
	int i = 1;
	for(String type: types)
	    typesOption[i++] = type;
	typeList = new JComboBox(typesOption);
	typeList.addActionListener(this);
	typeList.setPreferredSize(new Dimension((int)(libraryWidget.getFilterHorizSize() * (tlHorizFrac + cbHorizFrac) / 100.0), 
						(libraryWidget.getFilterVertSize())));

	add(typeList, getGbc(0, 1, gridwidth(2), weightx(100.0), gfillboth()));

	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	setBackground(bgColor);
	titleLabel.setBackground(bgColor);
	closeButton.setBackground(bgColor);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	typeList.setBackground(bgColor);

	closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON,
						  filterVertSize, filterVertSize));
    }

    public void actionPerformed(ActionEvent e) {
	String typeSelection = (String)(typeList.getSelectedItem());
	if(!typeSelection.equals(nullItem)) {
	    ((TypeFilter)filter).activateAnchor(TypeFilter.TYPE_ANCHOR, typeSelection);
	    SearchResult result = libraryController.activateFilter(sessionID, filter);
	    libraryController.updateResults(result, sessionID);
	} else {
	    ((TypeFilter)filter).activateAnchor(TypeFilter.TYPE_ANCHOR, "");
	    SearchResult result = libraryController.activateFilter(sessionID, filter);
	    libraryController.updateResults(result, sessionID);
	}

	if (typeSelection.startsWith("Select "))
	    typeList.setBackground(bgColor);
	else
	    typeList.setBackground(bgColor.brighter());
    }

    public void setEnabled(boolean activate) {
	super.setEnabled(activate);
	typeList.setEnabled(activate);
    }

    public void update(){
	Object type = typeList.getSelectedItem();
	removeAll();
	init();
	typeList.setSelectedItem(type);
    }

}