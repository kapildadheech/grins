package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.*;

import java.util.*;

import java.awt.*;
import java.awt.geom.*;

public class PointGraph extends BarGraph {

    int pointSize;

    public PointGraph(String title, long xMin, long xMax, String[] xLabels, long[] yDataItems, 
		      long binInterval, long maxDataItem, String[] infoStrings, Dimension displayDim, int yTics,
		      StationConfiguration stationConfig, int pointSize) {
	super(title, xMin, xMax, xLabels, yDataItems,
	      binInterval, maxDataItem, infoStrings, displayDim, yTics, stationConfig);	
	this.pointSize = pointSize;
    }
    
    protected void plotData(Graphics g, FontMetrics fm, int graphTopX, int graphTopY, int graphVert, int yPadVert, double xScale, double yScale) {
	Calendar calendar = new GregorianCalendar();
	g.setColor(stationConfig.getColor(StationConfiguration.GRAPH_COLOR));
	for(int i = 0; i < yDataItems.length; i++) {
	    int xVal = (int)((yDataItems[i] - xMin) / xScale);
	    calendar.setTimeInMillis(yDataItems[i]);
	    int yVal = (int)(calendar.get(Calendar.HOUR_OF_DAY) / yScale);
	    //	    g.fillOval(graphTopX + xVal - pointSize/2, graphTopY + graphVert - yVal, pointSize, pointSize);
	    
	    for(int j = -1; j <= 1; j++) {
		g.drawLine(graphTopX + xVal + j, graphTopY + graphVert - yVal - pointSize / 2,
			   graphTopX + xVal + j, graphTopY + graphVert - yVal + pointSize / 2);
		g.drawLine(graphTopX + xVal - pointSize / 2, graphTopY + graphVert - yVal + j,
			   graphTopX + xVal + pointSize / 2, graphTopY + graphVert - yVal + j);
	    }
	}
    }
}
