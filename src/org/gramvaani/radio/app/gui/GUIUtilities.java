package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.rscontroller.services.LibService;
import org.gramvaani.radio.medialib.RadioProgramMetadata;
import org.gramvaani.radio.medialib.Category;
import org.gramvaani.radio.stationconfig.StationConfiguration;

import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import java.util.ArrayList;
import java.util.HashSet;

public class GUIUtilities {
    
    static Font defaultFont = null;
    static LogUtilities logger = new LogUtilities("GUIUtilities");

    public static Icon getIcon(RadioProgramMetadata program, int width, int height){
	
	if (program == null)
	    return null;

	String iconStr = null;
	
	if (program.getType().equals(RadioProgramMetadata.TELEPHONY_TYPE))
	    iconStr = StationConfiguration.RAW_TELEPHONY_ICON;
	else if (program.getType().equals(RadioProgramMetadata.IMPORTED_TELEPHONY_TYPE))
	    iconStr = StationConfiguration.IMPORTED_TELEPHONY_ICON;
	else if (program.getType().equals(RadioProgramMetadata.MIC_TYPE))
	    iconStr = StationConfiguration.RECORD_ICON;
	
	if (iconStr == null)
	    return null;
	else
	    return IconUtilities.getIcon(iconStr, width, height);
    }

    //Make sure that the child nodes have been populated. metadataProvider.populateChildren
    public static JPanel getCategoryWidget(Category category, Dimension size, Color bgColor){
	if (category == null){
	    LogUtilities.getDefaultLogger().error("BuildCategorySubTree: null category");
	    return null;
	}

	JPanel widget = new JPanel();
	widget.setPreferredSize(size);
	widget.setMaximumSize(size);
	widget.setMinimumSize(size);
	widget.setBackground(bgColor);
	widget.setLayout(new BorderLayout());
	DefaultMutableTreeNode root = new DefaultMutableTreeNode (category);

	buildCategorySubTree (root, category, category);

	final JTree categoryTree = new JTree(root);
	categoryTree.setExpandsSelectedPaths(true);

	JScrollPane treePane = new JScrollPane(categoryTree, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, 
					       ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	
	treePane.setPreferredSize(size);
	treePane.setMaximumSize(size);
	treePane.setMinimumSize(size);
	treePane.setBackground(bgColor);

	categoryTree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);

	categoryTree.setDragEnabled(true);
	categoryTree.setToolTipText("<html><i>Ctrl/Shift</i>+click for multiple selections.</html>");

	TransferHandler transferHandler = new TransferHandler(){
		public int getSourceActions(JComponent c){
		    return COPY;
		}

		public Transferable createTransferable(JComponent c){
		    ArrayList<Category> list = new ArrayList<Category>();

		    for (TreePath path: categoryTree.getSelectionPaths()){
			list.add((Category)((DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject());
		    }

		    return new CategorySelection(list);
		}
	    };
	
	categoryTree.setTransferHandler(transferHandler);

	widget.add(treePane, BorderLayout.CENTER);
	return widget;
    }

    static void buildCategorySubTree(DefaultMutableTreeNode node, Category category, Category rootCategory){
	if (category == null){
	    LogUtilities.getDefaultLogger().error("BuildCategorySubTree: null category");
	    return;
	}

	for (Category subCategory: category.getChildren()){
	    DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(subCategory);
	    node.add(childNode);
	    buildCategorySubTree(childNode, subCategory, rootCategory);
	}
    }

    public static Font getDefaultFont(){
	if (defaultFont != null)
	    return defaultFont;

	return (defaultFont = (Font)UIManager.get("TextArea.font"));
    }

    public static Color toSaturation(Color c, float s){
	float hsb[] = new float[3];
	Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
	return new Color(Color.HSBtoRGB(hsb[0], s, hsb[2]));
    }

    public static Color toBrightness(Color c, float b){
	float hsb[] = new float[3];
	Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
	return new Color(Color.HSBtoRGB(hsb[0], hsb[1], b));
    }

    public static Color toSaturationBrightness(Color c, float s, float b){
	float hsb[] = new float[3];
	Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsb);
	return new Color(Color.HSBtoRGB(hsb[0], s, b));
    }

    public static Color interpolateColor(Color start, Color end, double blend){
	int red = (int) (start.getRed() * (1.0 - blend) + end.getRed() * blend);
	int green = (int)(start.getGreen() * (1.0 - blend) + end.getGreen() * blend);
	int blue = (int )(start.getBlue() * (1.0 - blend) + end.getBlue() * blend);

	return new Color(red, green, blue);
    }

    static int scrollBarHeight = -1;
    static int scrollBarWidth = -1;

    public static int getScrollBarHeight(){
	if (scrollBarHeight > -1)
	    return scrollBarHeight;

	scrollBarHeight = (new JScrollBar(JScrollBar.HORIZONTAL)).getMaximumSize().height;
	return scrollBarHeight;
    }

    public static int getScrollBarWidth(){
	if (scrollBarWidth > -1)
	    return scrollBarWidth;
	
	scrollBarWidth = (new JScrollBar(JScrollBar.VERTICAL)).getMaximumSize().width;
	return scrollBarWidth;
    }
    
    /*
    public static void debug(int maxFrames){
	StackTraceElement elements[] = (new Exception()).getStackTrace();
	int max = Math.min(maxFrames, elements.length);
	for (int i = 1; i <= maxFrames; i++)
	    //System.err.println("\t"+elements[i]);
    }
    */
    
    public static JFrame getTagsPopup(final TagTree tagTree, JComponent assocComponent, int labelHeight, Color bgColor){
	if (assocComponent == null){
	    logger.error("GetTagsPopup: Null assocComponent");
	    return null;
	}

	//final JPopupMenu tagsPopup = new JPopupMenu();
	RSPanel tagsPopup = new RSPanel();
	
	tagsPopup.setLayout(new BorderLayout());
	tagsPopup.setBorder(BorderFactory.createLineBorder(Color.gray));

	int width = assocComponent.getSize().width;
	int height = width;

	int fieldWidth = getTagSearchFieldWidth(width);
	Dimension fieldSize = new Dimension(fieldWidth, labelHeight);

	JPanel searchPanel = new JPanel();
	searchPanel.setBackground(bgColor);
	searchPanel.setLayout(new GridBagLayout());
	JLabel searchLabel = new JLabel("Search: ");	
	Dimension labelSize = new Dimension((width - fieldWidth)/2, labelHeight);
	searchLabel.setPreferredSize(labelSize);
	searchLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	searchPanel.add(searchLabel);

	final JTextField searchField = new SearchTextField();
	searchField.setPreferredSize(fieldSize);
	searchPanel.add(searchField);
	JPanel blankPanel = new JPanel();
	blankPanel.setOpaque(false);
	blankPanel.setPreferredSize(labelSize);
	searchPanel.add(blankPanel);
	tagsPopup.add(searchPanel, BorderLayout.NORTH);

	JPanel addPanel = new JPanel();
	addPanel.setLayout(new GridBagLayout());
	JLabel addLabel = new JLabel("Add: ");
	addLabel.setPreferredSize(labelSize);
	addLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	addPanel.add(addLabel);
	JTextField addField = new JTextField();
	addField.setPreferredSize(fieldSize);
	addPanel.add(addField);
	blankPanel = new JPanel();
	blankPanel.setPreferredSize(labelSize);
	addPanel.add(blankPanel);
	//tagsPopup.add(addPanel, BorderLayout.SOUTH);
	
	JScrollPane scrollPane = new JScrollPane(tagTree);
	tagsPopup.add(scrollPane, BorderLayout.CENTER);
	
	searchField.getDocument().addDocumentListener(new DocumentAdapter(){
		public void documentUpdated(DocumentEvent e){
		    tagTree.searchStringUpdated(searchField.getText());
		}
	    });

	Point p = assocComponent.getLocationOnScreen();

	int x = (int) p.getX();
	int y = (int) (p.getY() + assocComponent.getSize().height);

	final JFrame window = new JFrame();
	window.setUndecorated(true);
	window.add(tagsPopup);
	window.pack();
	window.setLocation(x, y);

	window.addWindowFocusListener(new WindowAdapter(){
		public void windowLostFocus(WindowEvent e){
		    if (e.getOppositeWindow() instanceof JDialog)
			return;
		    window.setVisible(false);
		}

		public void windowDeactivated(WindowEvent e){
		    window.setVisible(false);
		}
	    });

	KeyAdapter escapeAdapter = new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			window.setVisible(false);
		    }
		}
	    };

	window.addKeyListener(escapeAdapter);
	tagTree.addKeyListener(escapeAdapter);
	searchField.addKeyListener(escapeAdapter);

	return window;
    }

    static int getTagSearchFieldWidth(int width){
	return width/2;
    }

    public static String colorToHtmlString(Color color) {
	if (color == null)
	    return null;

	String htmlString = Integer.toHexString(color.getRGB());
	htmlString = "#" + htmlString.substring(2, htmlString.length());

	return htmlString;
    }

    public static String getColoredString(String string, Color color){
	return "<html><font color="+colorToHtmlString(color)+">"+string+"</font></html>";
    }

    public static Color getColorFromString(String colorString) {
	if (colorString.startsWith("0x"))
	    return Color.decode(colorString);
	else if (colorString.startsWith("#")) 
	    return Color.getColor(colorString.substring(1, colorString.length()));
	else
	    return Color.getColor(colorString);
    }

}