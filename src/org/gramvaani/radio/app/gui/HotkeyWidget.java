package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.rscontroller.services.AudioService;
import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.providers.CacheObjectListener;
import org.gramvaani.radio.app.providers.CacheListenerCallback;
import org.gramvaani.radio.app.providers.PlayoutProvider;
import org.gramvaani.radio.app.providers.PlayoutPreviewListenerCallback;
import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.app.providers.MetadataListenerCallback;
import org.gramvaani.radio.rscontroller.RSController;

import org.gramvaani.radio.medialib.Metadata;
import org.gramvaani.radio.medialib.RadioProgramMetadata;
import org.gramvaani.radio.medialib.Hotkey;
import org.gramvaani.radio.medialib.FileStoreManager;
import org.gramvaani.radio.app.*;

import org.gramvaani.utilities.IconUtilities;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureEvent;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class HotkeyWidget extends RSWidgetBase implements PlayoutPreviewListenerCallback, CacheListenerCallback, MetadataListenerCallback {
    
    CacheProvider cacheProvider;
    MetadataProvider metadataProvider;
    PlayoutProvider playoutProvider;
    
    boolean cacheProviderActive = false;
    boolean metadataProviderActive = false;
    boolean playoutProviderActive = false;
    boolean uiEnabled = false;

    ArrayList<HotkeyButton> hotkeys;
    static final int bottomBarVertFrac = 5;

    RSTitleBar titleBar;
    ButtonPane buttonPane;
    JPanel bottomBar;
    RSButton addNewButton;
    RSToggleButton removeButton;
    RSToggleButton colorChooserButton;
    JToggleButton.ToggleButtonModel colorButtonModel;
    boolean popupVisible = false;
    long colorPopupCloseTime = 0;
    JTextArea aspText;

    StationConfiguration stationConfig;
    //static final int BUTTON_COLOR = Color.blue.getRGB();
    Hashtable<String, HotkeyButton> hotkeyButtons;
    boolean isRemoveButtonMode = false;
    boolean isSimulationMode;
    Color bgColor;
    Color titleColor;
    
    Color selectedColor;
    DataFlavor colorFlavor;

    ErrorInference errorInference;

    public HotkeyWidget(RSApp rsApp){
	super("Hotkeys");
	
	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.HOTKEYS_ICON;
	bmpToolTip = "Hotkeys";
	
	stationConfig = rsApp.getStationConfiguration();
	
	cacheProvider    = (CacheProvider) rsApp.getProvider(RSApp.CACHE_PROVIDER);
	metadataProvider = (MetadataProvider) rsApp.getProvider(RSApp.METADATA_PROVIDER);
	playoutProvider  = (PlayoutProvider) rsApp.getProvider(RSApp.PLAYOUT_PROVIDER);

	errorInference = ErrorInference.getErrorInference();
    }

    protected void initAspComponent(){

	aspText = new JTextArea();
	aspText.setEditable(false);
	aspText.setLineWrap(true);
	aspText.setWrapStyleWord(true);
	aspText.setBackground(bgColor);
	aspComponent.setLayout(new BorderLayout());
	aspComponent.add(aspText, BorderLayout.CENTER);
	
	aspText.addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    e.setSource(aspComponent.getParent());
		    aspComponent.getParent().dispatchEvent(e);
		}
	    });

	aspText.setTransferHandler(null);
    }

    public DataFlavor[] getASPDataFlavors(){
	return new DataFlavor[] {ProgramSelection.getFlavor()};
    }


    public DataFlavor[] getWPDataFlavors(){
	return new DataFlavor[] {ProgramSelection.getFlavor()};
    }

    public boolean onLaunch(){
	hotkeys = new ArrayList<HotkeyButton> ();
	hotkeyButtons = new Hashtable<String, HotkeyButton>();

	bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	titleColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);

	initAspComponent();
	
	wpComponent.setBackground(bgColor);

	wpComponent.setLayout(new BorderLayout());
	titleBar = new RSTitleBar(name, (int)wpSize.getWidth());
	titleBar.setBackground(titleColor);
	wpComponent.add(titleBar, BorderLayout.PAGE_START);

	playoutProvider.registerWithProvider(this);
	cacheProvider.registerWithProvider(this);
	metadataProvider.registerWithProvider(this);

	if (playoutProvider == null || !playoutProvider.isActive())
	    playoutProviderActive = false;
	else
	    playoutProviderActive = true;

	if (cacheProvider == null || !cacheProvider.isActive())
	    cacheProviderActive = false;
	else
	    cacheProviderActive = true;

	if (metadataProvider == null || !metadataProvider.isActive())
	    metadataProviderActive = false;
	else
	    metadataProviderActive = true;

	if (!cacheProviderActive)
	    errorInference.displayServiceError("Unable to activate cache service", new String[]{RSController.ERROR_SERVICE_DOWN});

	buttonPane = new ButtonPane(new Dimension(wpSize.width, 
						  wpSize.height - titleBar.HEIGHT 
						  - wpSize.height*bottomBarVertFrac/100),
				    metadataProvider, this);
	

	wpComponent.add(buttonPane, BorderLayout.CENTER);

	buildBottomBar();
	wpComponent.add(bottomBar, BorderLayout.SOUTH);
	wpComponent.validate();
	
	isSimulationMode = cpJFrame.getSimulationMode();
	activateMetadataService(metadataProviderActive, new String[]{});

	return true;
    }

    public void onMaximize(){

    }

    public void onMinimize(){

    }

    public void onUnload(){
	for (HotkeyButton hotkeyButton: hotkeys) {
	    hotkeyButton.removeCacheObjectListener();
	}

	wpComponent.removeAll();

	playoutProvider.unregisterWithProvider(getName());
	cacheProvider.unregisterWithProvider(getName());
	metadataProvider.unregisterWithProvider(getName());

	isRemoveButtonMode = false;
    }

    protected void buildBottomBar(){

	bottomBar = new JPanel();
	Dimension bbSize = new Dimension(wpSize.width, wpSize.height*bottomBarVertFrac/100);
	
	bottomBar.setBackground(titleColor);
	bottomBar.setSize(bbSize);
	bottomBar.setMinimumSize(bbSize);
	bottomBar.setPreferredSize(bbSize);
	bottomBar.setLayout(new GridBagLayout());

	Dimension buttonSize = new Dimension((int) bbSize.width/6, (int)bbSize.height);
	int iconSize = (buttonSize.height * 8)/10;

	addNewButton = new RSButton("Add Hotkey");
	addNewButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    buttonPane.addNewButton();
		}
	    });

	addNewButton.setBackground(bgColor);
	addNewButton.setPreferredSize(buttonSize);
	addNewButton.setMinimumSize(buttonSize);
	addNewButton.setIcon(IconUtilities.getIcon(StationConfiguration.ADD_ICON, iconSize));

	removeButton = new RSToggleButton("Remove Hotkey");
	final ButtonModel removeButtonModel = removeButton.getModel();

	removeButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    if (!isPlaying())
			buttonPane.removeButtonMode(removeButtonModel.isSelected());
		    else
			removeButton.setSelected(false);
		}
	    });

	removeButton.setBackground(bgColor);
	removeButton.setPreferredSize(buttonSize);
	removeButton.setMinimumSize(buttonSize);
	removeButton.setIcon(IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, iconSize));

	bottomBar.add(addNewButton, getGbc(1, 0));

	bottomBar.add(removeButton, getGbc(2, 0));

	final JPopupMenu colorPopup = new JPopupMenu() {
		protected void firePopupMenuCanceled(){
		    if (colorButtonModel.isSelected())
			colorChooserButton.doClick();
		}
	    };

						       
	final JColorChooser colorChooser = new JColorChooser();
	colorChooser.setDragEnabled(true);
	selectedColor = bgColor;
	colorChooser.setColor(bgColor);
	colorPopup.add(colorChooser);
	
	colorChooserButton = new RSToggleButton("Select Color");

	colorButtonModel = (JToggleButton.ToggleButtonModel) colorChooserButton.getModel();

	
	colorChooserButton.addActionListener(new ActionListener(){
		int x = 1, y = 1;
		public void actionPerformed(ActionEvent e){
		    if (x > 0){
			colorPopup.show(colorChooserButton, 0, 0);
			colorPopup.setVisible(false);
			x = -(colorPopup.getSize().width - colorChooserButton.getSize().width)/2;
			y = -(colorPopup.getSize().height);
		    }
		    
		    if (!colorChooserButton.isSelected()){
			colorPopup.setVisible(false);
		    } else { 
			colorPopup.show(colorChooserButton, x, y);
		    }

		}
	    });
	

	colorChooserButton.setBackground(bgColor);
	colorChooserButton.setPreferredSize(buttonSize);
	colorChooserButton.setMinimumSize(buttonSize);
	colorChooserButton.setIcon(IconUtilities.getIcon(StationConfiguration.COLOR_PICKER_ICON, iconSize));

	bottomBar.add(colorChooserButton, getGbc(3, 0));

	final JPanel colorPanel = new JPanel(){
		public void paintComponent(Graphics g){
		    Graphics2D g2 = (Graphics2D) g;
		    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		    g2.setColor(getBackground());
		    g2.fillRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 20, 15);
		    g2.setColor(Color.darkGray);
		    g2.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 20, 15);
		}
		
	    };
	
	try{
	    colorFlavor = new DataFlavor("application/x-java-jvm-local-objectref; class=java.awt.Color");
	} catch (Exception e){
	    logger.error("BuildBottomBar: Could not create data flavor: ", e);
	}
	

	DragGestureListener gestureListener = new DragGestureListener(){
		public void dragGestureRecognized(DragGestureEvent dge){
		    Transferable transferable = new Transferable() {
			    public Object getTransferData(DataFlavor flavor){
				return selectedColor;
			    }
			    
			    public DataFlavor[] getTransferDataFlavors(){
				return new DataFlavor[] {colorFlavor};
			    }
			    
			    public boolean isDataFlavorSupported(DataFlavor flavor){
				return flavor.equals(colorFlavor);
			    }
			    
			};

		    dge.startDrag(null, transferable);
		}
	    };

	DragSource.getDefaultDragSource().createDefaultDragGestureRecognizer(colorPanel, DnDConstants.ACTION_COPY,
								     gestureListener);


	colorPanel.setBackground(bgColor);
	colorPanel.setPreferredSize(buttonSize);
	colorPanel.setMinimumSize(buttonSize);

	bottomBar.add(colorPanel, getGbc(4, 0));
	
	colorChooser.getSelectionModel().addChangeListener(new ChangeListener(){
		public void stateChanged(ChangeEvent e){
		    Color color = colorChooser.getColor();
		    colorPanel.setBackground(color);
		    selectedColor = color;
		}
	    });

    }

    public void removeButtonMode(boolean enabled){
	isRemoveButtonMode = enabled;
	removeButton.setSelected(enabled);
	addNewButton.setEnabled(!enabled);
    }

    public void setSimulationMode(boolean isSimulationMode){
	this.isSimulationMode = isSimulationMode;
	buttonPane.activate();
    }

    protected boolean isPlaying(){
	for (HotkeyButton hotkey: hotkeyButtons.values())
	    if (hotkey.isPlaying())
		return true;
	return false;
    }

    protected void play(String programID, HotkeyButton hotkey){
	logger.debug("Play: Playing "+programID);
	
	boolean providerActive = playoutProviderActive;
	boolean isPlayable = playoutProvider.isPlayable();
	boolean alreadyPlaying = hotkeyButtons.get(programID) != null;

	if (providerActive && isPlayable && !alreadyPlaying){
	    hotkeyButtons.put(programID, hotkey);
	    if (playoutProvider.play(programID, this, false) < 0)
		errorInference.displayPlaylistError("Unable to play hotkey", new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
	    else
		hotkey.setPlaying(true);
	} else {
	    //hotkey.playDone();
	    logger.error("Play: Unable to play. Provider active: "+providerActive+" playable: "+isPlayable+" alreadyPlaying: "+alreadyPlaying);
	}
	
    }

    protected void stop(String programID, HotkeyButton hotkey){
	logger.debug("Stop: Stopping "+programID);
	
	boolean providerActive = playoutProviderActive;
	boolean isStoppable = playoutProvider.isStoppable();

	if (providerActive && isStoppable){
	    if (playoutProvider.stop(programID, this) < 0)
		errorInference.displayPlaylistError("Unable to stop hotkey", new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
	    else {
		hotkey.setPlaying(false);
	    }
	} else {
	    logger.error("Stop: Unable to stop. Provider active: "+providerActive+" stoppable: "+isStoppable);
	}
    }

    /* Callbacks from providers */

    public void playActive(boolean success, String programID, String playType, long telecastTime, String[] error){
	if (success){
	    //cpJFrame.notifyPlayout(programID, telecastTime);
	} else {
	    clearProgram(programID);
	    errorInference.displayServiceError("Unable to play hotkey", error);
	}
    }

    public void pauseActive(boolean success, String programID, String playType, String[] error){

    }

    public void stopActive(boolean success, String programID, String playType, String[] error){
	if (success)
	    clearProgram(programID);
	else
	    errorInference.displayServiceError("Unable to stop hotkey", error);
    }

    public void playDone(boolean success, String programID, String playType){
	clearProgram(programID);
	if (!success)
	    errorInference.displayServiceError("Error occured while playing hotkey", new String[]{});
    }

    public void audioGstError(String programID, String error, String playType){
	clearProgram(programID);
	errorInference.displayGstError("Error occured while playing hotkey", error);
    }

    protected void clearProgram(String programID){
	hotkeyButtons.get(programID).playDone();
	hotkeyButtons.get(programID).setPlaying(false);
	hotkeyButtons.remove(programID);
    }

    public void playoutPreviewError(String errorCode, String[] errorParams) {
	errorInference.displayServiceError("Error occured while playing file", errorCode, errorParams);
    }

    public void activateAudio(boolean activate, String playType, String[] error){
	if (playType.equals(AudioService.PLAYOUT)){
	    playoutProviderActive = activate;
	    buttonPane.activate();
	}

	if (!activate)
	    errorInference.displayServiceError("Unable to activate audio service", error);
    }

    public void enableAudioUI(boolean enable , String playType, String[] error) {
	if (playType.equals(AudioService.PLAYOUT)){
	    buttonPane.enableUI(enable); 
	}

	if (!enable)
	    errorInference.displayServiceError("Unable to activate audio service", error);
    }

    public void activateCache(boolean activate, String[] error){
	cacheProviderActive = activate;
	if (activate){
	    buttonPane.loadButtons();
	} else
	    errorInference.displayServiceError("Unable to activate cache service", error);
    }

    public void cacheObjectsDeleted(String objectType, String[] objectID){}
    public void cacheObjectsUpdated(String objectType, String[] objectID){}
    public void cacheObjectsInserted(String objectType, String[] objectID){}

    public void activateMetadataService(boolean activate, String[] error){
	metadataProviderActive = activate;
	buttonPane.setEditMode(activate && !isRemoveButtonMode);
	addNewButton.setEnabled(activate && !isRemoveButtonMode);
	removeButton.setEnabled(activate);

	if (!activate)
	    errorInference.displayServiceError("Unable to activate metadata service", error);
    }

    protected void allButtonsFilled(boolean filled){
	String text;
	if (filled)
	    text = "";
	else
	    text = "Drag and drop files on newly created buttons to use them.";
	aspText.setText(text);
    }

    class ButtonPane extends JScrollPane {
	static final int ROW_SIZE = 4;
	
	static final int HBORDER = 100;
	static final int VBORDER = 0;

	//static final int buttonsPerPage = ROW_SIZE * COL_SIZE;

	int currentPage = 0, currentRow = -1, currentCol = -1;//, currentIndex;
	Dimension size, buttonSize;
	JPanel buttonPanel;
	MetadataProvider metadataProvider;
	HashSet<String> itemIDs;
	HotkeyWidget widget;
	//Boolean uiEnabled = true;
	
	public ButtonPane(Dimension size, MetadataProvider metadataProvider, HotkeyWidget widget){
	    this.size = size;
	    this.metadataProvider = metadataProvider;
	    this.widget = widget;

	    setSize(size);
	    setPreferredSize(size);
	    setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    getVerticalScrollBar().setUnitIncrement(10);
	    buttonSize = new Dimension((size.width - HBORDER)/ROW_SIZE, (size.width - HBORDER)/ROW_SIZE);
	    buttonPanel = new JPanel();
	    buttonPanel.setLayout(new GridBagLayout());
	    buttonPanel.setBackground(bgColor);

	    itemIDs = new HashSet<String> ();
	    if (cacheProviderActive)
		loadButtons();
	    activate();
	    setEditMode(metadataProviderActive);
	    setViewportView(buttonPanel);
	}

	public void loadButtons(){
	    Hotkey[] hotkeyMetadata = cacheProvider.getAll(new Hotkey(), 
							   new String[] {Hotkey.getKeyIDField()},
							   new String[] {CacheProvider.ORDER_ASC},
							   null,
							   -1);
	    hotkeys.clear();
	    for (Hotkey hotkey: hotkeyMetadata){
		hotkeys.add(new HotkeyButton(hotkey, metadataProvider, this, true));
	    }
	    addButtons();	    
	}

	protected void addButtons(){
	    
	    int row = -1;
	    int col = -1;

	    for (int i = 0; i < hotkeys.size(); i++){
		col = i % ROW_SIZE;
		if (col == 0)
		    row++;
		addButtonAt(hotkeys.get(i), row, col);
	    }
	    currentRow = row;
	    currentCol = col;

	    buttonPanel.validate();
	}

	public void addNewButton(){
	    currentCol = (++currentCol) % ROW_SIZE;
	    if (currentCol == 0)
		currentRow++;
	    
	    Hotkey hotkey = new Hotkey(bgColor.getRGB());
	    HotkeyButton button = new HotkeyButton(hotkey, metadataProvider, this, false);
	    hotkeys.add(button);
	    addButtonAt(button, currentRow, currentCol);
	    buttonPanel.setSize(buttonPanel.getSize().width, (currentRow+1)*buttonSize.height);
	    
	    buttonPanel.validate();
	    allButtonsFilled(!hasEmptyButton());
	}

	protected void addButtonAt(JComponent button, int row, int col){
	    logger.debug("ButtonPane:AddButtonAt: Row: "+row+" Col: "+col);

	    button.setSize(buttonSize);
	    button.setPreferredSize(buttonSize);
	    button.setMinimumSize(buttonSize);
	    buttonPanel.add(button, getGbc(col, row, gfillboth()));

	    if (button instanceof HotkeyButton){
		HotkeyButton hotkeyButton = (HotkeyButton) button;
		itemIDs.add(hotkeyButton.getItemID());
	    }
	}

	public boolean isDuplicate(String itemID){
	    return itemIDs.contains(itemID);
	}

	public void addItemID(String itemID){
	    itemIDs.add(itemID);
	    allButtonsFilled(!hasEmptyButton());
	}

	public void updateItemID(String oldItemID, String newItemID){
	    itemIDs.remove(oldItemID);
	    addItemID(newItemID);
	}

	protected boolean hasEmptyButton(){
	    for (HotkeyButton hotkey: hotkeys)
		if (hotkey.isEmpty())
		    return true;
	    return false;
	}

	public void removeButtonMode(boolean enabled){
	    widget.removeButtonMode(enabled);
	    activate();
	}

	public void removeButton(HotkeyButton hotkeyButton){
	    logger.info("ButtonPane: RemoveButton: Removing button: "+hotkeyButton.getItemID());
	    itemIDs.remove(hotkeyButton.getItemID());
	    HotkeyButton tmp = null;
	    for (HotkeyButton button: hotkeys) {
		if (button.getItemID().equals(hotkeyButton.getItemID()))
		    tmp = button;
	    }
	    if (tmp != null)
		hotkeys.remove(tmp);
	    reLayout();
	    
	    allButtonsFilled(!hasEmptyButton());
	}

	protected void reLayout(){
	    buttonPanel.removeAll();
	    addButtons();
	    buttonPanel.validate();
	    buttonPanel.repaint();
	    updateButtonOrder();
	}

	protected void updateButtonOrder(){
	    int keyID = 0;
	    
	    for (HotkeyButton hotkeyButton: hotkeys){
		Hotkey hotkey = hotkeyButton.getHotkey();
		Hotkey query = new Hotkey(hotkey.getItemID());

		hotkey.setKeyID(keyID);
		
		if (hotkey.getItemID().equals(""))
		    continue;
		if (metadataProvider.update(query, hotkey) < 0)
		    logger.error("UpdateButtonOrder: Could not update button: "+hotkey.getItemID());
		else
		    logger.debug("UpdateButtonOrder: Updated button: "+hotkey.getItemID());

		keyID++;
	    }
	}

	public void activate(){
	    enableUI(playoutProviderActive && !isRemoveButtonMode && !isSimulationMode);
	    if (!playoutProviderActive)
		for (HotkeyButton hotkeyButton: hotkeys)
		    hotkeyButton.playDone();
	}

	public void enableUI(boolean enabled) {
	    uiEnabled = enabled;
	    if (enabled)
		for (HotkeyButton hotkeyButton: hotkeys)
		    hotkeyButton.activate();
	    else
		for (HotkeyButton hotkeyButton: hotkeys)
		    hotkeyButton.deactivate();
	}

	public void setEditMode(boolean enabled){
	    for (HotkeyButton hotkey: hotkeys){
		hotkey.setEditMode(enabled);
	    }
	}

    }


    class HotkeyButton extends JToggleButton implements ActionListener, CacheObjectListener {
	Hotkey hotkey;
	RadioProgramMetadata program;
	Dimension size;
	MetadataProvider metadataProvider;
	Color buttonColor;

	final ButtonPane buttonPane;
	boolean inDatabase;
	boolean isButtonSelected = false;
	boolean isEditable = false;
	boolean isPlaying = false;

	public HotkeyButton(Hotkey hotkey, MetadataProvider metadataProvider, 
			    final ButtonPane buttonPane, boolean inDatabase){
	    super("");
	    this.hotkey = hotkey;
	    this.metadataProvider = metadataProvider;
	    this.buttonPane = buttonPane;
	    this.inDatabase = inDatabase;

	    buttonColor = new Color(hotkey.getColor());
	    if (!inDatabase)
		setToolTipText("Drag and drop programs.");

	    program = hotkey.getProgram();
	    if (program != null)
		cacheProvider.addCacheObjectListener(program, this);
	    addActionListener(this);
	    updateDisplay();
	    
	    @SuppressWarnings("unchecked") 
	    TransferHandler transferHandler = new TransferHandler(){
		    int selectionIndices[];
		    int importIndex;

		    public boolean canImport (TransferHandler.TransferSupport support){
			if (!support.isDrop() || !isEditable)
			    return false;
			
			if (support.isDataFlavorSupported(colorFlavor))
			    return true;
			
			if (!support.isDataFlavorSupported(ProgramSelection.getFlavor()))
			    return false;
			
			
			boolean copySupported = (COPY & support.getSourceDropActions()) == COPY;
			if (copySupported) {
			    
			    Transferable t = support.getTransferable();
			    ArrayList<RadioProgramMetadata> list = null;
			    try{
				list = (ArrayList<RadioProgramMetadata>)t.getTransferData(ProgramSelection.getFlavor());
			    }catch(Exception e){ 
				return false;
			    }
			    
			    if (!buttonPane.isDuplicate(list.get(0).getItemID())){
				support.setDropAction(COPY);
				return true;
			    }
			}
			return false;
		    }
		    
		    public boolean importData(TransferHandler.TransferSupport info){
			if (!info.isDrop())
			    return false;
			Transferable t = info.getTransferable();

			if (info.isDataFlavorSupported(colorFlavor)){
			    try{
				Color color = (Color) t.getTransferData(colorFlavor);
				setColor(color);
				return true;
			    } catch (Exception e){
				logger.error("ImportData: Exception.", e);
				return false;
			    }
			} else {
			    ArrayList<RadioProgramMetadata> list = null;
			    try{
				list = (ArrayList<RadioProgramMetadata>)t.getTransferData(ProgramSelection.getFlavor());
			    }catch(Exception e){ 
				logger.error("ImportData: Exception.", e);
				return false;
			    }
			    
			    setProgram(list.get(0));
			    updateDisplay();
			    return true;
			}
		    }
		    
		};
	    
	    setTransferHandler(transferHandler);

	    addMouseListener(new MouseAdapter(){
		    public void mouseEntered(MouseEvent e){
			setButtonSelected();
		    }

		    public void mouseExited(MouseEvent e){
			setButtonUnselected();
		    }
		    
		    public void mouseClicked(MouseEvent e){
			if (isButtonSelected && isRemoveButtonMode) {
			    deleteButton();
			    //buttonPane.removeButtonMode(false);
			}
		    }
		});

	    isEditable = metadataProviderActive;
	}
	
	public void setEditMode(boolean enabled){
	    isEditable = enabled;
	}

	public void setPlaying(boolean isPlaying){
	    this.isPlaying = isPlaying;
	}

	public boolean isPlaying(){
	    return isPlaying;
	}

	protected void updateDisplay() {
	    setBackground(buttonColor);
	    if (program == null){
		setEnabled(false);
		setText("Add File");
	    }
	    else {
		if (playoutProviderActive && !isSimulationMode 
		    && uiEnabled && program.getPlayoutStoreAttempts() == FileStoreManager.UPLOAD_DONE){
		    setEnabled(true);
		} else {
		    setEnabled(false);
		}
		setText(program.getTitle());
	    }
	}

	protected void setColor(Color color){
	    Hotkey query = new Hotkey(hotkey.getKeyID());
	    hotkey.setColor(color.getRGB());
	    buttonColor = color;
	    setBackground(color);
	    if (program != null){
		if (metadataProvider.update(query, hotkey) < 0)
		    errorInference.displayServiceError("Unable to update hotkey color",
						       new String[]{RSController.ERROR_DB_FAILED});
		else 
		    logger.debug("HotkeyButton: SetColor: Updated color: "+hotkey.getItemID()+" color: "+color.getRGB());
	    }
	}
	
	protected void setProgram(RadioProgramMetadata program){
	    String oldItemID = hotkey.getItemID();	    
	    if (this.program != null)
		removeCacheObjectListener();
	    this.program = program;
	    cacheProvider.addCacheObjectListener(this.program, this);

	    //Setting non-null dummy keyID, it will be corrected by updateButtonOrder
	    hotkey.setKeyID(0);
	    hotkey.setProgram(program);
	    hotkey.setItemID(program.getItemID());
	    buttonPane.updateItemID(oldItemID, program.getItemID());
	    
	    if (inDatabase){
		if (metadataProvider.update(new Hotkey(oldItemID), hotkey) < 0)
		    errorInference.displayServiceError("Unable to update hotkey in database",
						       new String[]{RSController.ERROR_DB_FAILED});
		else {
		    logger.debug("HotkeyButton: SetProgram: Updated Hotkey: " + hotkey.getItemID());
		}
	    } else {
		if (metadataProvider.insert(hotkey) < 0)
		    errorInference.displayServiceError("Unable to Insert Hotkey in database",
						       new String[]{RSController.ERROR_DB_FAILED});
		else {
		    inDatabase = true;
		    buttonPane.updateButtonOrder();
		    logger.debug("HotkeyButton: SetProgram: Inserted Hotkey: " + hotkey.getItemID());
		}
	    }

	    setToolTipText(null);
	}

	public Hotkey getHotkey(){
	    return hotkey;
	}

	public boolean isEmpty(){
	    return (program == null);
	}

	protected void deleteButton(){
	    if (hotkey.getItemID() != null && !hotkey.getItemID().equals(""))
		metadataProvider.remove(hotkey);
	    buttonPane.removeButton(this);
	    removeCacheObjectListener();
	}
	
	protected RadioProgramMetadata getProgram(){
	    return program;
	}
	
	protected void playDone(){
	    if (isSelected()) {
		setSelected(false);
	    }
	}
	
	public void actionPerformed(ActionEvent e){
	    if (isSelected())
		play(program.getItemID(), this);
	    else if (playoutProviderActive && playoutProvider.isStoppable())
		stop(program.getItemID(), this);
	    else {
		setSelected(true);
	    }
	}
	
	public String getItemID(){
	    if (hotkey != null)
		return hotkey.getItemID();
	    else
		return null;
	}

	protected void displayError(String error){
	    logger.error(error);
	}

	public void deactivate(){
	    setEnabled(false);
	}

	public void activate(){
	    if ((program != null) && (program.getPlayoutStoreAttempts() == FileStoreManager.UPLOAD_DONE)) {
		setEnabled(uiEnabled);
	    } else {
		setEnabled(false);
	    }
	}

	public void setButtonSelected(){
	    isButtonSelected = true;
	    setBorder(BorderFactory.createLineBorder(Color.blue));
	    //setBackground(Color.blue);
	}

	public void setButtonUnselected(){
	    isButtonSelected = false;
	    setBorder(BorderFactory.createLineBorder(Color.lightGray));
	    //setBackground(buttonColor);
	}

	public void removeCacheObjectListener() {
	    if (program != null)
		cacheProvider.removeCacheObjectListener(program, this);
	}
	
	public void cacheObjectValueChanged(Metadata metadata) {
	    program = (RadioProgramMetadata)metadata;
	    hotkey.setProgram(program);
	    updateDisplay();
	}

	public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKeys) {
	    //here metadata.length will always be 1
	    for (int j = 0; j < metadata.length; j++) {
		String newItemID = newKeys[j].substring(0, newKeys[j].lastIndexOf("."));
		RadioProgramMetadata dummyProgram, newProgram=null;
		Metadata mdata;
		if (!newItemID.equals("")) {
		    dummyProgram = RadioProgramMetadata.getDummyObject(newItemID);
		    mdata = cacheProvider.getSingle(dummyProgram);
		    if (mdata == null){
			logger.warn("CacheObjectKeyChanged: Metadata not present in cache: "+newItemID);
			return;
		    }
		    newProgram = (RadioProgramMetadata)mdata;
		}
	    
		if (!newItemID.equals("")) {
		    hotkey.setProgram(newProgram);
		    updateDisplay();
		} else {
		    if (playoutProviderActive && playoutProvider.isStoppable() && isPlaying)
			stop(program.getItemID(), this);
		    deleteButton();
		}
	    }
	}
    }
}


