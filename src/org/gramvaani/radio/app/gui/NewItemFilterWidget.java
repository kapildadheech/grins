package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class NewItemFilterWidget extends ActiveFilterWidget {
    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;
    
    protected StationConfiguration stationConfig;

    public NewItemFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
			       LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	stationConfig = libraryController.getStationConfiguration();
	init();
    }

    protected void init() {
	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();
							  
	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, filterVertSize + borderHeight));
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());
	titleLabel.setPreferredSize(new Dimension((int)(filterHorizSize * tlHorizFrac / 100.0), filterVertSize));
	add(titleLabel, getGbc(0, 0, weightx(100.0), gfillboth()));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));
	add(closeButton, getGbc(1, 0, gfillboth()));

	((NewItemFilter)filter).activateAnchor(NewItemFilter.NEWITEM_ANCHOR, "");
	SearchResult result = libraryController.activateFilter(sessionID, filter);
	libraryController.updateResults(result, sessionID);

	Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	setBackground(bgColor);
	titleLabel.setBackground(bgColor);
	closeButton.setBackground(bgColor);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);

	closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON,
						  filterVertSize, filterVertSize));

    }


    public void update(){
	
    }
}