package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.app.providers.MetadataListenerCallback;
import org.gramvaani.radio.app.providers.CacheProvider;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.telephonylib.RSCallerID;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.*;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class PollWidget extends RSWidgetBase implements MetadataListenerCallback, PollEditorPopup.PollEditorListener {

    static int POLL_TABLE_ROW_HEIGHT = 25;

    RSApp rsApp;
    MetadataProvider metadataProvider;
    CacheProvider cacheProvider;
    boolean isMetadataActive;

    StationConfiguration stationConfig;
    Color bgColor, titleColor;

    JTable pollTable;
    PollTableModel tableModel;
    RSButton newPollButton, editPollButton, deletePollButton, refreshButton;
    PollEditorPopup editorPopup;
    
    int nameColHorFrac = 35;
    int keywordColHorFrac = 10;
    int statusColHorFrac = 12;
    int votesColHorFrac = 12;
    

    int editorPopupHorizFrac = 70;
    int editorPopupVertFrac = 50;
    int resultsPopupHorizFrac = 80;
    int resultsPopupVertFrac = 65;

    public PollWidget(RSApp rsApp){
	super("Polls");
	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.POLL_ICON;
	bmpToolTip = "Polls";

	this.rsApp = rsApp;
	stationConfig = rsApp.getStationConfiguration();
	
	cacheProvider    = (CacheProvider) rsApp.getProvider(RSApp.CACHE_PROVIDER);
	metadataProvider = (MetadataProvider) rsApp.getProvider(RSApp.METADATA_PROVIDER);
    }

    protected boolean onLaunch(){
	wpComponent.setLayout(new BorderLayout());

	bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	titleColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);
	
	wpComponent.setBackground(bgColor);

	RSTitleBar titleBar = new RSTitleBar(name, (int)wpSize.getWidth());
	titleBar.setBackground(titleColor);
	wpComponent.add(titleBar, BorderLayout.PAGE_START);

	metadataProvider.registerWithProvider(this);
	if (metadataProvider == null || !metadataProvider.isActive())
	    isMetadataActive = false;
	else
	    isMetadataActive = true;

	buildWorkspace();
	return true;
    }


    protected void onMaximize(){

    }

    protected void onMinimize(){
    }
    
    public void onUnload(){
	wpComponent.removeAll();
	if(tableModel != null)
	    tableModel.clearData();
	metadataProvider.unregisterWithProvider(getName());
    }

    void buildWorkspace(){
	RSPanel pollPanel = new RSPanel();

	Dimension pollPanelSize = new Dimension(wpSize.width, wpSize.height - RSTitleBar.HEIGHT);
	pollPanel.setBackground(bgColor);
	pollPanel.setPreferredSize(pollPanelSize);

	wpComponent.add(pollPanel, BorderLayout.CENTER);

	pollPanel.setLayout(new GridBagLayout());
	
	int searchBarVertFrac = 5;
	int actionBarVertFrac = 5;

	int searchBarHeight = searchBarVertFrac * pollPanelSize.height/100;
	int actionBarHeight = actionBarVertFrac * pollPanelSize.height/100;
	int pollTableHeight = pollPanelSize.height - searchBarHeight - actionBarHeight;

	RSPanel searchPanel = new RSPanel();
	Dimension searchPanelSize = new Dimension(wpSize.width, searchBarHeight);
	searchPanel.setPreferredSize(searchPanelSize);
	searchPanel.setBackground(bgColor);
	searchPanel.setLayout(new GridBagLayout());

	pollPanel.add(searchPanel, getGbc(0, 0, gfillboth()));

	RSLabel searchLabel = new RSLabel("Search Poll: ");
	searchLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	searchPanel.add(searchLabel);
	
	final JTextField searchField = new JTextField();
	Dimension searchFieldDimension  = new Dimension(wpSize.width/3, searchPanelSize.height - 4);
	searchField.setPreferredSize(searchFieldDimension);
	searchField.getDocument().addDocumentListener(new DocumentAdapter(){
		public void documentUpdated(DocumentEvent e){
		    searchStringUpdated(searchField.getText());
		}

	    });
	searchField.addKeyListener(new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			searchField.setText("");
		    }
		}
	    });
	
	searchPanel.add(searchField);

	RSLabel emptyLabel = new RSLabel(" ");
	searchPanel.add(emptyLabel);

	int refreshButtonWidth = 12 * wpSize.width/100;
	Dimension refreshButtonSize = new Dimension(refreshButtonWidth, searchBarHeight - 4);
	int refreshIconSize = (refreshButtonSize.height * 8)/10;

	refreshButton = new RSButton("Refresh");
	refreshButton.setBackground(bgColor);
	refreshButton.setPreferredSize(refreshButtonSize);
	refreshButton.setIcon(IconUtilities.getIcon(StationConfiguration.REDO_ICON, refreshIconSize));
	refreshButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    tableModel.reloadTable();
		}
	    });
	searchPanel.add(refreshButton);


	int resultsPopupWidth = wpSize.width * resultsPopupHorizFrac / 100;
	int resultsPopupHeight = wpSize.height * resultsPopupVertFrac / 100;

	tableModel = new PollTableModel(metadataProvider, stationConfig, resultsPopupWidth, resultsPopupHeight, bgColor, titleColor, rsApp);

	pollTable = new JTable(tableModel);
	pollTable.setFillsViewportHeight(true);
	pollTable.getTableHeader().setReorderingAllowed(false);
	pollTable.setRowHeight(POLL_TABLE_ROW_HEIGHT);
	Color headerColor = GUIUtilities.toSaturation(bgColor, 0.05f);
	pollTable.getTableHeader().setBackground(headerColor);

	pollTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
		public void valueChanged(ListSelectionEvent e){
		    selectionChanged();
		}
	    });
	
	TableColumn col = pollTable.getColumnModel().getColumn(0);
	col.setPreferredWidth(wpSize.width * nameColHorFrac / 100);
	col = pollTable.getColumnModel().getColumn(1);
	col.setPreferredWidth(wpSize.width * keywordColHorFrac / 100);
	col = pollTable.getColumnModel().getColumn(2);
	col.setPreferredWidth(wpSize.width * statusColHorFrac / 100);
	col = pollTable.getColumnModel().getColumn(3);
	col.setPreferredWidth(wpSize.width * votesColHorFrac / 100);
	col = pollTable.getColumnModel().getColumn(4);
	col.setPreferredWidth(wpSize.width * (100 - nameColHorFrac - keywordColHorFrac - statusColHorFrac - votesColHorFrac) / 100);

	pollTable.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_DELETE){
			deletePolls();
		    }
		}
	    });
		
	pollTable.setDefaultRenderer(JLabel.class, new LabelRenderer());
	TableColumn keywordColumn = pollTable.getColumnModel().getColumn(1);
	keywordColumn.setCellRenderer(new CenterLabelRenderer());
	TableColumn stateColumn = pollTable.getColumnModel().getColumn(2);
	stateColumn.setCellRenderer(new CenterLabelRenderer());
	TableColumn voteColumn = pollTable.getColumnModel().getColumn(3);
	voteColumn.setCellRenderer(new CenterLabelRenderer());
	TableColumn buttonColumn = pollTable.getColumnModel().getColumn(4);
	buttonColumn.setCellRenderer(new PanelRenderer());

	JScrollPane pollScrollPane = new JScrollPane(pollTable);

	Dimension tableSize = new Dimension(wpSize.width, pollTableHeight);
	pollScrollPane.setPreferredSize(tableSize);
	pollScrollPane.setBackground(headerColor);
	pollPanel.add(pollScrollPane, getGbc(0, 1, gfillboth(), weighty(1.0)));
	pollScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	pollScrollPane.getVerticalScrollBar().setBackground(bgColor);

	RSPanel actionPanel = new RSPanel();
	Dimension actionPanelSize = new Dimension(wpSize.width, actionBarHeight);
	actionPanel.setPreferredSize(actionPanelSize);
	actionPanel.setBackground(bgColor);
	actionPanel.setLayout(new GridBagLayout());
	pollPanel.add(actionPanel, getGbc(0, 2, gfillboth()));

	int buttonsWidthHorizFrac = 70;
	int buttonWidth = buttonsWidthHorizFrac * wpSize.width/100 / 4;
	Dimension buttonSize = new Dimension(buttonWidth, actionBarHeight - 4);
	int iconSize = (buttonSize.height * 8)/10;
	
	newPollButton = new RSButton("New Poll");
	newPollButton.setBackground(bgColor);
	newPollButton.setPreferredSize(buttonSize);
	newPollButton.setIcon(IconUtilities.getIcon(StationConfiguration.NEW_POLL_ICON, iconSize));
	newPollButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
		    String[] keywords = metadataProvider.getAllPollKeywords();
		    editorPopup.showEditor(false, null, null, keywords, newPollButton);
		}
	    });
	actionPanel.add(newPollButton);

	emptyLabel = new RSLabel(" ");
	emptyLabel.setBackground(bgColor);
	actionPanel.add(emptyLabel);

	editPollButton = new RSButton("Edit Poll");
	editPollButton.setBackground(bgColor);
	editPollButton.setPreferredSize(buttonSize);
	editPollButton.setIcon(IconUtilities.getIcon(StationConfiguration.EDIT_POLL_ICON, iconSize));
	editPollButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    editPoll();
		}
	    });
	editPollButton.setEnabled(false);
	actionPanel.add(editPollButton);
	
	emptyLabel = new RSLabel(" ");
	emptyLabel.setBackground(bgColor);
	actionPanel.add(emptyLabel);

	deletePollButton = new RSButton("Delete Poll");
	deletePollButton.setBackground(bgColor);
	deletePollButton.setPreferredSize(buttonSize);
	deletePollButton.setIcon(IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, iconSize));
	deletePollButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    deletePolls();
		}
	    });
	
	deletePollButton.setEnabled(false);
	actionPanel.add(deletePollButton);

	int editorPopupWidth = wpSize.width * editorPopupHorizFrac / 100;
	int editorPopupHeight = wpSize.height * editorPopupVertFrac / 100;
	editorPopup = new PollEditorPopup(this, editorPopupWidth, editorPopupHeight, bgColor, titleColor, stationConfig, rsApp);

	PollButtonsMouseListener buttonsListener = new PollButtonsMouseListener(pollTable);
	pollTable.addMouseListener(buttonsListener);

	tableModel.reloadTable();
    }

    static final int MIN_QUERY_LENGTH = 3;
    void searchStringUpdated(String query){
	if (query == null)
	    return;

	if (query.length() < MIN_QUERY_LENGTH)
	    query = "";

	tableModel.search(query);
    }

    Poll[] getSelection(){
	ListSelectionModel model = pollTable.getSelectionModel();
	int minIndex = model.getMinSelectionIndex();
	int maxIndex = model.getMaxSelectionIndex();

	if (minIndex < 0 || maxIndex < 0)
	    return new Poll[]{};

	ArrayList<Poll> list = new ArrayList<Poll>();
	for (int i = minIndex; i <= maxIndex; i++){
	    if (model.isSelectedIndex(i))
		list.add(tableModel.getPollAt(i));
	}

	return list.toArray(new Poll[list.size()]);
    }

    void selectionChanged(){
	Poll selection[] = getSelection();
	
	if (selection.length == 0){
	    deletePollButton.setEnabled(false);
	    editPollButton.setEnabled(false);
	} else if(selection.length == 1) {
	    deletePollButton.setEnabled(true);
	    editPollButton.setEnabled(true);
	} else if(selection.length > 1) {
	    deletePollButton.setEnabled(true);
	    editPollButton.setEnabled(false);
	}
    }

    void editPoll() {
	Poll[] polls = getSelection();
	
	if(polls.length == 0)
	    return;

	Vote[] votes = metadataProvider.getVotesOfPoll(polls[0].getID());

	if(votes.length != 0) {
	    String confirm = "Warning!! Editing Poll Options in a Poll will delete ALL the votes. Are you sure you want to continue?";
	    String title = "Edit Poll";
	    int retVal = JOptionPane.showConfirmDialog(rsApp.getControlPanel().cpJFrame(), confirm, title, JOptionPane.YES_NO_OPTION);
	    if (retVal != JOptionPane.YES_OPTION)
		return;
	}

	String[] keywords = metadataProvider.getAllPollKeywords();
	PollOption[] options = metadataProvider.getPollOptionsOfPoll(polls[0].getID());
	editorPopup.showEditor(true, polls[0], options, keywords, editPollButton);
    }

    void deletePolls(){
	Poll selection[] = getSelection();

	if (selection.length == 0)
	    return;
	
	String confirm = "Are you sure you want to delete " + selection.length + " selected Poll(s)?";
	String title = "Delete Selected Polls";
	int retVal = JOptionPane.showConfirmDialog(rsApp.getControlPanel().cpJFrame(), confirm, title, JOptionPane.YES_NO_OPTION);
	if (retVal != JOptionPane.YES_OPTION)
	    return;

	for (Poll poll: selection) {
	    PollOption[] toDeleteOptions = metadataProvider.getPollOptionsOfPoll(poll.getID());
	    for(PollOption toDeleteOption :toDeleteOptions)
		metadataProvider.remove(toDeleteOption);

	    Vote[] toDeleteVotes = metadataProvider.getVotesOfPoll(poll.getID());
	    for(Vote toDeleteVote :toDeleteVotes)
		metadataProvider.remove(toDeleteVote);

	    metadataProvider.remove(poll);
	}

	tableModel.reloadTable();
    }

    /*Callbacks from PollEditorPopup*/
    public void editPoll(int pollID, String name, String keyword, int pollType, int state) {
	metadataProvider.editPoll(pollID, name, keyword, pollType, state);
    }

    public int addPoll(String name, String keyword, int pollType, int state) {
	return metadataProvider.addPoll(name, keyword, pollType, state);
    }

    public void addPollOption(int pollID, String name, String code) {
	metadataProvider.addPollOption(pollID, name, code);
    }

    public void removePollOption(int pollOptionID) {
	metadataProvider.removePollOption(pollOptionID);
    }

    public void saveDone() {
	tableModel.reloadTable();
    }
    /* End of callbacks*/
    

    /* Callbacks from Providers */
    
    public void activateMetadataService(boolean activate, String[] error){
	isMetadataActive = activate;
    }
    
    /* End of Callbacks */
    
    class PanelRenderer extends JPanel implements TableCellRenderer {  
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {  
	    JPanel panel = (JPanel)value;

	    if(isSelected)
		panel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_SELECT_COLOR));
	    else
		panel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_COLOR));

	    int width = table.getColumnModel().getColumn(column).getPreferredWidth();
	    int height = table.getRowHeight(row);
	    Dimension buttonSize = new Dimension(width/3 - 3, height - 1);

	    for(Component component : panel.getComponents()) {
		if(component instanceof JButton) {
		    JButton button = (JButton)component;
		    button.setPreferredSize(buttonSize);
		    button.setMinimumSize(buttonSize);
		}
	    }

	    return panel;
	}  
    }  

    class LabelRenderer extends DefaultTableCellRenderer {
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	    JLabel renderedLabel = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	    renderedLabel.setVerticalAlignment(SwingConstants.CENTER);

	    if(isSelected)
		renderedLabel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_SELECT_COLOR));
	    else
		renderedLabel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_COLOR));

	    return renderedLabel;
	}
    }

    class CenterLabelRenderer extends DefaultTableCellRenderer {
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	    JLabel renderedLabel = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	    renderedLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    renderedLabel.setVerticalAlignment(SwingConstants.CENTER);

	    if(isSelected)
		renderedLabel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_SELECT_COLOR));
	    else 
		renderedLabel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_COLOR));

	    return renderedLabel;
	}
    }

    public class PollButtonsMouseListener extends MouseAdapter {
	private final JTable table;
	
	public PollButtonsMouseListener(JTable table) {
	    this.table = table;
	}

	JButton getButtonAt(JPanel panel, Point p) {
	    panel.setLocation(0, 0);
	    panel.setSize(new Dimension(table.getColumnModel().getColumn(4).getPreferredWidth(), table.getRowHeight()));
	    int y = (int)p.getY();
	    int x = (int)p.getX();
		    
	    Component button = panel.getComponentAt(x, y);
	    if (button instanceof RSButton)
		return ((RSButton)button);
	    else
		return null;
	}

	@Override public void mouseClicked(MouseEvent e) {
	    int column = table.getColumnModel().getColumnIndexAtX(e.getX());
	    int row    = e.getY()/table.getRowHeight(); 
	    
	    if (row < table.getRowCount() && row >= 0 && column < table.getColumnCount() && column >= 0) {
		Object value = table.getValueAt(row, column);

		int relativeY = e.getY() - (row * table.getRowHeight());
		int relativeX = e.getX();
		for(int i = 0; i < column; i++) {
		    relativeX -= table.getColumnModel().getColumn(i).getWidth();
		}
		
		if (value instanceof JPanel) {
		    JPanel panel = (JPanel)value;
		    JButton button = getButtonAt(panel, new Point(relativeX, relativeY));
		    if(button != null)			
			button.doClick();
		}
	    }
	}
    }

}


