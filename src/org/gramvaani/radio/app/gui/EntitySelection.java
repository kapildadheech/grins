package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.Entity;

import java.util.ArrayList;

import java.awt.datatransfer.DataFlavor;

public class EntitySelection extends SelectionList {
    
    EntitySelection(ArrayList<Entity> list){
	super(list);
    }

    public static DataFlavor getFlavor(){
 	return SelectionList.getFlavor(EntitySelection.class);
    }
}