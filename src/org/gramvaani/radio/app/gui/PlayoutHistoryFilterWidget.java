package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import com.toedter.calendar.JDateChooser;

import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class PlayoutHistoryFilterWidget extends ActiveFilterWidget {
    MetadataProvider metadataProvider;

    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;
    static final int dlHorizFrac = 20;
    static final int dcHorizFrac = 90 - dlHorizFrac;

    JDateChooser fromDateChooser, toDateChooser;
    Date startDate, endDate;
    StationConfiguration stationConfig;

    static final long MILLIS_PER_DAY = 1000L * 60L * 60L * 24L;
    static final long TZ_OFFSET;

    PlayoutHistoryFilter filter;
    boolean runOnce = false;

    static {
	Calendar calendar = GregorianCalendar.getInstance();
	TZ_OFFSET = calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET);
    }
    
    public PlayoutHistoryFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
				      LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	this.filter = (PlayoutHistoryFilter)filter;
	metadataProvider = libraryController.getMetadataProvider();
	this.stationConfig = libraryController.getStationConfiguration();
	init();
    }

    protected void init() {
	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();
							  
	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, filterVertSize * 3 + borderHeight));	
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());
	add(titleLabel, getGbc(0, 0, gridwidth(2), weightx(100.0), gfillboth()));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));

	add(closeButton, getGbc(2, 0, gfillboth()));

	RSLabel fromLabel = new RSLabel("From:");
	fromLabel.setPreferredSize(new Dimension((int)(filterHorizSize * dlHorizFrac / 100.0), filterVertSize));
	add(fromLabel, getGbc(0, 1, gfillboth()));
	
	fromDateChooser = new JDateChooser(){
		public void propertyChange(java.beans.PropertyChangeEvent e){
		    super.propertyChange(e);
		    dateSelected();
		}
	    };

	fromDateChooser.setPreferredSize(new Dimension((int)(filterHorizSize * dcHorizFrac / 100.0), filterVertSize));
	add(fromDateChooser, getGbc(1, 1, gridwidth(2), gfillboth()));
	
	RSLabel toLabel = new RSLabel("To:");
	toLabel.setPreferredSize(new Dimension((int)(filterHorizSize * dlHorizFrac / 100.0), filterVertSize));

	add(toLabel, getGbc(0, 2, gfillboth()));
	
	toDateChooser = new JDateChooser(){
		public void propertyChange(java.beans.PropertyChangeEvent e){
		    super.propertyChange(e);
		    dateSelected();
		}
	    };

	toDateChooser.setPreferredSize(new Dimension((int)(filterHorizSize * dcHorizFrac / 100.0), filterVertSize));

	add(toDateChooser, getGbc(1, 2, gridwidth(2), gfillboth()));

	Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	setBackground(bgColor);
	titleLabel.setBackground(bgColor);
	closeButton.setBackground(bgColor);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	fromDateChooser.setBackground(bgColor);
	toDateChooser.setBackground(bgColor);
	fromLabel.setBackground(bgColor); fromLabel.setOpaque(true);
	toLabel.setBackground(bgColor);	toLabel.setOpaque(true);

	closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON,
						  filterVertSize, filterVertSize));

	setInitDates();
    }

    protected long endOfDay(long time){
	long day = ((time + TZ_OFFSET)/ MILLIS_PER_DAY)+1;
	return day * MILLIS_PER_DAY - 1 - TZ_OFFSET;
    }

    protected synchronized void dateSelected(){
	if (fromDateChooser == null ||toDateChooser == null)
	    return;

	Date fromDate = fromDateChooser.getDate();
	Date toDate   = toDateChooser.getDate();

	if (fromDate == null || toDate == null)
	    return;

	if (fromDate.equals(startDate) && toDate.equals(endDate) && runOnce)
	    return;
	
	runOnce = true;

	startDate = fromDate;
	endDate = toDate;

	fromDateChooser.setMaxSelectableDate(endDate);
	toDateChooser.setMinSelectableDate(startDate);

	filter.activateAnchor(PlayoutHistoryFilter.PLAYOUTHISTORY_ANCHOR, null);
	filter.setStartTime(fromDate.getTime());
	filter.setEndTime(endOfDay(toDate.getTime()));

	SearchResult result = libraryController.activateFilter(sessionID, filter);
	libraryController.updateResults(result, sessionID);
    }

    
    protected void setInitDates(){
	endDate = new Date();
	Calendar cal = Calendar.getInstance();
	cal.setTime(endDate);
	cal.add(Calendar.DATE, -1);
	startDate = cal.getTime();
	fromDateChooser.setDate(startDate);
	toDateChooser.setDate(endDate);
    }
    
    public void update(){
	
    }
}