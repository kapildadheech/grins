package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.services.UIService;
import org.gramvaani.radio.telephonylib.RSCallerID;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import java.text.DateFormat;

import java.util.*;

import static org.gramvaani.utilities.StringUtilities.sanitize;

public class MetadataController implements ActionListener, MetadataListenerCallback, 
					   CacheListenerCallback, CacheObjectListener {

    public final static String METADATA_CONTROLLER = "METADATA_CONTROLLER";

    protected MetadataProvider metadataProvider;
    protected boolean metadataProviderActive;

    protected CacheProvider cacheProvider;
    protected boolean cacheProviderActive;

    protected RSApp rsApp;
    protected MetadataWidget metadataWidget;
    protected ControlPanel controlPanel;
    protected StationConfiguration stationConfig;
    protected UIService uiService;

    protected RadioProgramMetadata[] selectedProgramsMetadata;
    protected int selectedProgramIndex;
    protected RadioProgramMetadata currentActiveProgram;
    protected CreatorListModel creatorListModel;
    protected String[] languageList, roleList, affiliationList, nameList, locationList;
    protected Category[] categoryList;

    protected boolean languageUpdated;
    protected String nullLanguageItem = "Select language";
    
    protected LogUtilities logger;
    protected ErrorInference errorInference;

    Hashtable<String, HashSet<Category>> addCategories = new Hashtable<String, HashSet<Category>>();
    Hashtable<String, HashSet<Category>> removeCategories = new Hashtable<String, HashSet<Category>>();
    Hashtable<String, HashSet<Category>> currentCategories = new Hashtable<String, HashSet<Category>>();

    public MetadataController(MetadataWidget metadataWidget, ControlPanel controlPanel, RSApp rsApp) {
	this.metadataWidget = metadataWidget;
	this.rsApp = rsApp;
	this.controlPanel = controlPanel;
	this.selectedProgramIndex = 0;
	this.uiService = rsApp.getUIService();

	logger = new LogUtilities("MetadataController");
	metadataProviderActive = false;
	cacheProviderActive = false;

	this.stationConfig = rsApp.getStationConfiguration();

	errorInference = ErrorInference.getErrorInference();
    }

    public void init() {
	this.cacheProvider = (CacheProvider)(rsApp.getProvider(RSApp.CACHE_PROVIDER));

	cacheProvider.registerWithProvider(this);
	if (cacheProvider == null || !cacheProvider.isActive()) {
	    cacheProviderActive = false;
	} else {
	    cacheProviderActive = true;
	}

	this.metadataProvider = (MetadataProvider)(rsApp.getProvider(RSApp.METADATA_PROVIDER));
	metadataProvider.registerWithProvider(this);
	metadataProvider.setCacheProvider(cacheProvider);

	if (metadataProvider == null || !metadataProvider.isActive())
	    metadataProviderActive = false;
	else
	    metadataProviderActive = true;

	if (metadataProviderActive)
	    selectedProgramsMetadata = metadataProvider.getProgramMetadata(metadataWidget.getSelectedPrograms());
	else
	    selectedProgramsMetadata = new RadioProgramMetadata[]{};
	
	creatorListModel = new CreatorListModel();

	if (metadataProviderActive) {
	    languageList = metadataProvider.getLanguages();
	    categoryList = metadataProvider.getCategories();
	    roleList = metadataProvider.getRoles();
	    affiliationList = metadataProvider.getAffiliations();
	    locationList = metadataProvider.getLocations();
	    nameList = metadataProvider.getNames();

	    for (Category category: categoryList)
		metadataProvider.populateChildren(category);
	} else {
	    languageList = new String[]{};
	    categoryList = new Category[]{};
	    roleList = new String[]{};
	    affiliationList = new String[]{};
	    locationList = new String[]{};
	    nameList = new String[]{};
	}
    }

    public void actionPerformed(ActionEvent evt) {
	String actionCommand = evt.getActionCommand();
       	if (actionCommand.equals(MetadataWidget.EDIT)) {
	    metadataWidget.setEditMode(true);
	    //	    setEnabled(true, metadataWidget.getGroupMode());
	    updateGUIButtons();
	    metadataWidget.reinitAspComponent();
	    if (metadataWidget.getGroupMode()) {
		setCurrentActiveProgram(metadataWidget.getEditMode(), metadataWidget.getGroupMode());
		initFields(metadataWidget.getEditMode(), metadataWidget.getGroupMode());
	    }
	    
	} else if (actionCommand.equals(MetadataWidget.SAVE)) {
	    
	    if (!metadataWidget.getGroupMode()) {
		save(currentActiveProgram, false);
	    } else {
		if (!metadataWidget.getEditMode()) {
		    for (RadioProgramMetadata program: selectedProgramsMetadata) {
			//System.out.println("************* group saving " + program.getItemID());
			save(program, true);
		    }
		} else {
		    save(currentActiveProgram, false);
		}
	    }
	    int retVal = metadataProvider.flushProgramIndex();
	    if (retVal < 0)
		if (retVal == IndexLib.ERROR_NO_SPACE_CODE){
		    errorInference.displayIndexLibError("Unable to save", new String[]{IndexLib.ERROR_NO_SPACE});
		} else {
		    errorInference.displayIndexLibError("Unable to save", new String[]{IndexLib.ERROR_UNKNOWN});
		}
	    
	    selectedProgramsMetadata = metadataProvider.getProgramMetadata(metadataWidget.getSelectedPrograms());
	    setCurrentActiveProgram(metadataWidget.getEditMode(), metadataWidget.getGroupMode());
	    initFields(metadataWidget.getEditMode(), metadataWidget.getGroupMode());
	    metadataWidget.reinitAspComponent();
	    metadataWidget.resetCreators();
	    LibraryController libraryController = null;
	    if ((libraryController = uiService.getController(LibraryController.class)) != null){
		libraryController.updateFilters();
	    }

	} else if (actionCommand.equals(MetadataWidget.NEXT)) {

	    selectedProgramIndex++;
	    setCurrentActiveProgram(metadataWidget.getEditMode(), metadataWidget.getGroupMode());
	    initFields(metadataWidget.getEditMode(), metadataWidget.getGroupMode());
	    metadataWidget.reinitAspComponent();

	} else if (actionCommand.equals(MetadataWidget.PREV)) {

	    selectedProgramIndex--;
	    setCurrentActiveProgram(metadataWidget.getEditMode(), metadataWidget.getGroupMode());
	    initFields(metadataWidget.getEditMode(), metadataWidget.getGroupMode());	    
	    metadataWidget.reinitAspComponent();

	}
    }

    public String[] getAffiliations(){
	return affiliationList;
    }

    protected void save(RadioProgramMetadata program, boolean groupmode) {
	boolean phoneValid;
	String title = "";
	if (!groupmode)
	    title = sanitize(metadataWidget.getTitleField().getText());
	String desc = sanitize(metadataWidget.getDescField().getText());
	String language = "";
	if (languageUpdated && !((String)(metadataWidget.getLanguageDropDown().getSelectedItem())).equals(nullLanguageItem))
	    language = sanitize((String)(metadataWidget.getLanguageDropDown().getSelectedItem()));

	metadataProvider.editProgram(program.getItemID(), title, desc, language, "");  // XXX license missed out

	for (Category category: removeCategories.get(program.getItemID()))
	    metadataProvider.removeProgramCategory(program.getItemID(), category.getNodeID());	    

	for (Category category: addCategories.get(program.getItemID()))
	    metadataProvider.addProgramCategory(program.getItemID(), category.getNodeID());	    

	Creator[] newCreators = creatorListModel.newCreators();
	Creator[] removeCreators = creatorListModel.removeCreators();
	Creator[] editCreators = creatorListModel.editCreators();

	for (Creator creator: newCreators) {
	    metadataProvider.addProgramCreator(program.getItemID(), creator.getEntityID(), creator.getAffiliation());
	}

	for (Creator creator: editCreators) {
	    Creator queryCreator = new Creator(creator.getItemID());
	    queryCreator.setEntityID(creator.getEntityID());
	    
	    metadataProvider.update(queryCreator, creator);
	}

	for (Creator creator: removeCreators) {
	    if (creator.getEntity() != null && creator.getEntity().getName() != null && !creator.getEntity().getName().equals("")
	       && creator.getEntity().getEntityID() != -1) {
		metadataProvider.removeProgramCreator(program.getItemID(), creator.getEntityID());
	    }
	}
	
	creatorListModel.clear();
	creatorListModel.initProgram(currentActiveProgram);
	
	int retVal = metadataProvider.updateProgramIndex(program.getItemID());
	if (retVal < 0)
	    if (retVal == IndexLib.ERROR_NO_SPACE_CODE){
		errorInference.displayIndexLibError("Unable to save", new String[]{IndexLib.ERROR_NO_SPACE});
	    } else {
		errorInference.displayIndexLibError("Unable to save", new String[]{IndexLib.ERROR_UNKNOWN});
	    }
	
	saveFeedback();
    }

    public void displayError(String error) {
	errorInference.displayMetadataError(error, new String[]{});
    }

    private void saveFeedback(){
	final Color feedbackColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	final Color defaultColor  = stationConfig.getColor(StationConfiguration.NEXTPREV_BUTTON_COLOR);
	final int duration = 700;
	final int steps = 20;
	final int sleepTime = duration / steps;
	final int extraTextTime = 300;

	Thread t = new Thread(){
		public void run(){
		    metadataWidget.setFeedbackText("Saved Metadata.");
		    for (int i = 0; i < steps + 1; i++){
			Color color = GUIUtilities.interpolateColor(feedbackColor, defaultColor, 1.0 * i/steps);
			metadataWidget.setFeedbackColor(color);
			try{
			    Thread.sleep(sleepTime);
			}catch(Exception e){}
		    }
		    try{
			Thread.sleep(extraTextTime);
		    }catch(Exception e){}
		    
		    metadataWidget.setFeedbackText("");
		}
	    };
	t.start();
    }

    public MetadataProvider getMetadataProvider() {
	return metadataProvider;
    }

    public RadioProgramMetadata[] getProgramsMetadata() {
	return selectedProgramsMetadata;
    }

    public RadioProgramMetadata getCurrentProgramMetadata() {
	return currentActiveProgram;
    }

    public Set<Category> getCategories(String itemID) {
	return currentCategories.get(itemID);
    }

    public Set<Category> getAddCategories(String itemID){
	return addCategories.get(itemID);
    }

    public void addCategory(String itemID, Category category){
	addCategories.get(itemID).add(category);
	currentCategories.get(itemID).add(category);

	Category tmp = category;
	for (Category c: currentCategories.get(itemID)){
	    if (c.getNodeID() == category.getNodeID()){
		tmp = c;
		break;
	    }
	}

	removeCategories.get(itemID).remove(tmp);
    }

    public void removeCategory(String itemID, Category category){
	Category tmp = category;

	for (Category c: currentCategories.get(itemID)){
	    if (c.getNodeID() == category.getNodeID()){
		tmp = c;
		break;
	    }
	}

	addCategories.get(itemID).remove(tmp);
	currentCategories.get(itemID).remove(tmp);
	removeCategories.get(itemID).add(tmp);
    }

    public void setCurrentActiveProgram(boolean editmode, boolean groupmode) {
	if (!groupmode){
	    if (selectedProgramsMetadata.length > 0)
		currentActiveProgram = selectedProgramsMetadata[0];
	    else
		logger.error("SetCurrentActiveProgram: No selected programs available.");
	} else {

	    if (!editmode)
		currentActiveProgram = initCommonFields(selectedProgramsMetadata);
	    else {
		currentActiveProgram = selectedProgramsMetadata[selectedProgramIndex];
	    }
	}
    }

    protected RadioProgramMetadata initCommonFields(RadioProgramMetadata[] selectedProgramsMetadata) {
	RadioProgramMetadata commonMetadata = new RadioProgramMetadata("");
	commonMetadata.setLanguage(commonLanguage(selectedProgramsMetadata));
	commonMetadata.setLicenseType(commonLicenseType(selectedProgramsMetadata));
	commonMetadata.setCreators(commonCreators(selectedProgramsMetadata));
	commonMetadata.setCategories(commonCategories(selectedProgramsMetadata));
	commonMetadata.setTags(new Tags[]{new Tags("", 0, -1, StringUtilities.getCSVFromArray(commonTags(selectedProgramsMetadata)))});
	
	return commonMetadata;
    }
    
    protected String commonLanguage(RadioProgramMetadata[] programs) {
	String language = programs[0].getLanguage();
	for (int i = 1; i < programs.length; i++) {
	    RadioProgramMetadata program = programs[i];
	    if (!program.getLanguage().equals(language))
		return "";
	}
	return language;
    }

    protected String commonLicenseType(RadioProgramMetadata[] programs) {
	String licenseType = programs[0].getLicenseType();
	for (int i = 1; i < programs.length; i++) {
	    RadioProgramMetadata program = programs[i];
	    if (!program.getLicenseType().equals(licenseType))
		return "";
	}
	return licenseType;
    }

    protected Creator[] commonCreators(RadioProgramMetadata[] programs) {
	ArrayList<Creator> creators = new ArrayList<Creator>();
	int i = 0;
	for (Creator creator: programs[0].getCreators()) {
	    creators.add(creator); i++;
	    for (int j = 1; j < programs.length; j++) {
		RadioProgramMetadata program = programs[j];
		Creator[] creatorArr = program.getCreators();
		boolean found = false;
		for (Creator creatorArrCurr: creatorArr) {
		    if (creatorArrCurr.getEntityID() == creator.getEntityID()) {
			found = true;
			break;
		    }
		}
		if (!found) {		   
		    creators.remove(--i);
		    break;
		}
	    }
	}
	return creators.toArray(new Creator[]{});
    }

    protected ItemCategory[] commonCategories(RadioProgramMetadata[] programs) {
	ArrayList<ItemCategory> categories = new ArrayList<ItemCategory>();
	int i = 0;
	for (ItemCategory category: programs[0].getCategories()) {
	    categories.add(category); i++;
	    for (int j = 1; j < programs.length; j++) {
		RadioProgramMetadata program = programs[j];
		ItemCategory[] categoryArr = program.getCategories();
		boolean found = false;
		for (ItemCategory categoryArrCurr: categoryArr) {
		    if (categoryArrCurr.getCategoryNodeID() == category.getCategoryNodeID()) {
			found = true;
			break;
		    }
		}
		if (!found) {		   
		    categories.remove(--i);
		    break;
		}
	    }
	}
	return categories.toArray(new ItemCategory[]{});
    }

    protected String[] commonTags(RadioProgramMetadata[] programs) {
	ArrayList<String> tags = new ArrayList<String>();
	int i = 0;
	for (Tags thisTags: programs[0].getTags()) {
	    String[] thisTagsArr = StringUtilities.getArrayFromCSV(thisTags.getTagsCSV());
	    for (String thisTag: thisTagsArr) {
		thisTag = sanitize(thisTag);
		tags.add(thisTag); i++;
		for (int j = 0; j < programs.length; j++) {
		    RadioProgramMetadata program = programs[j];
		    Tags[] tagsArr = program.getTags();
		    boolean found = false;
		    for (Tags tagsArrCurr: tagsArr) {
			String[] tagStrArr = StringUtilities.getArrayFromCSV(tagsArrCurr.getTagsCSV());
			for (String tagStr: tagStrArr) {
			    tagStr = sanitize(tagStr);
			    if (thisTag.equals(tagStr)) {
				found = true;
				break;
			    }
			}
			if (found)
			    break;
		    }
		    if (!found) {		   
			tags.remove(--i);
			break;
		    }
		}
	    }
	}
	return tags.toArray(new String[]{});
    }
    
    public void initFields(boolean editmode, boolean groupmode) {
	metadataWidget.getTitleField().setText(currentActiveProgram.getTitle());
	metadataWidget.getDescField().setText(currentActiveProgram.getDescription());
        metadataWidget.getDurationField().setText(StringUtilities.durationStringFromMillis(currentActiveProgram.getLength()));
	String dateString = DateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date(currentActiveProgram.getCreationTime()));
	metadataWidget.getCreationField().setText(dateString);
	metadataWidget.getTypeField().setText(currentActiveProgram.getType());

	clearLanguageDropDown();
	initLanguageDropDown();

	metadataWidget.getCategoryDropDown().removeAllItems();
	for (Category category: categoryList)
	    metadataWidget.getCategoryDropDown().addItem(category.getLabel());

	creatorListModel.clear();
	creatorListModel.initProgram(currentActiveProgram);

	addCategories.put(currentActiveProgram.getItemID(), new HashSet<Category>());
	removeCategories.put(currentActiveProgram.getItemID(), new HashSet<Category>());
	HashSet<Category> categories = new HashSet<Category>();
	currentCategories.put(currentActiveProgram.getItemID(), categories);
	for (ItemCategory itemCategory: currentActiveProgram.getCategories()){
	    Category query = new Category();
	    query.setNodeID(itemCategory.getCategoryNodeID());
	    Category category = metadataProvider.getSingle(query);
	    if (category != null){
		categories.add(category);
		cacheProvider.addCacheObjectListener(category, this);
	    }
	}

	metadataWidget.updateTags();

	if (!groupmode && !editmode) {
	    metadataWidget.getShortHistoryDisplay().setDisplayTitle(false);
	    metadataWidget.getShortHistoryDisplay().setGraphType(StatsDisplay.POINT_GRAPH);
	    metadataWidget.getShortHistoryDisplay().refresh(new String[]{currentActiveProgram.getItemID()},
							    System.currentTimeMillis() - (long)30 * 24 * 60 * 60 * 1000L, System.currentTimeMillis(), 4);
	    
	    metadataWidget.getLongHistoryDisplay().setDisplayTitle(false);
	    metadataWidget.getLongHistoryDisplay().refresh(new String[]{currentActiveProgram.getItemID()}, -1, -1, 4);
	    // assume that this will be called on just one item, not a group
	    //	    setEnabled(false, false);
	    updateGUIButtons();
	} else if (!groupmode && editmode) {
	    metadataWidget.getShortHistoryDisplay().setDisplayTitle(false);
	    metadataWidget.getShortHistoryDisplay().setGraphType(StatsDisplay.POINT_GRAPH);
	    metadataWidget.getShortHistoryDisplay().refresh(new String[]{currentActiveProgram.getItemID()},
							    System.currentTimeMillis() - (long)30 * 24 * 60 * 60 * 1000L, System.currentTimeMillis(), 4);

	    metadataWidget.getLongHistoryDisplay().setDisplayTitle(false);
	    metadataWidget.getLongHistoryDisplay().refresh(new String[]{currentActiveProgram.getItemID()}, -1, -1, 4);
	    // enable all buttons and lists except next, prev, and edit
	    //	    setEnabled(true, false);
	    updateGUIButtons();
	} else if (groupmode) {
	    if (editmode) {
		metadataWidget.getShortHistoryDisplay().setDisplayTitle(false);
		metadataWidget.getShortHistoryDisplay().setGraphType(StatsDisplay.POINT_GRAPH);
		metadataWidget.getShortHistoryDisplay().refresh(selectedProgramsMetadata,
								System.currentTimeMillis() - (long)30 * 24 * 60 * 60 * 1000L, System.currentTimeMillis(), 4);

		metadataWidget.getLongHistoryDisplay().setDisplayTitle(false);
		metadataWidget.getLongHistoryDisplay().refresh(selectedProgramsMetadata, -1, -1, 4);
		// inidividual item edit mode. all buttons are active
		//		setEnabled(true, true);
		updateGUIButtons();
	    } else {
		metadataWidget.getShortHistoryDisplay().setDisplayTitle(false);
		metadataWidget.getShortHistoryDisplay().setGraphType(StatsDisplay.POINT_GRAPH);
		metadataWidget.getShortHistoryDisplay().refresh(selectedProgramsMetadata,
								System.currentTimeMillis() - (long)30 * 24 * 60 * 60 * 1000L, System.currentTimeMillis(), 4);

		metadataWidget.getLongHistoryDisplay().setDisplayTitle(false);
		metadataWidget.getLongHistoryDisplay().refresh(selectedProgramsMetadata, -1, -1, 4);
		// group edit mode. all buttons except title, etc are active
		//		setEnabled(false, true);
		updateGUIButtons();
	    }
	}

    }

    public void updateGUIButtons() {
	if (!metadataWidget.isLaunched()) 
	    return;

	if (metadataProviderActive && cacheProviderActive) {
	    setEnabled(metadataWidget.getEditMode(), metadataWidget.getGroupMode(), true);	    
	} else {
	    setEnabled(false, metadataWidget.getGroupMode(), false);
	    ///XXX null pointer exception here when DB down.
	    metadataWidget.getEditButton().setEnabled(false);
	    metadataWidget.getSaveButton().setEnabled(false);
	    metadataWidget.getNextButton().setEnabled(false);
	    metadataWidget.getPrevButton().setEnabled(false);
	}
    }

    protected void setEnabled(boolean editable, boolean isgroup, boolean guiEnabled) {
	//XXX NullPointer Exception here when DB down.
	if (!metadataWidget.isLaunched())
	    return;
	metadataWidget.getTitleField().setEnabled(guiEnabled && editable);
	metadataWidget.getDescField().setEnabled(guiEnabled && (isgroup || editable));
	metadataWidget.getDurationField().setEnabled(false);
	metadataWidget.getCreationField().setEnabled(false);
	metadataWidget.getTypeField().setEnabled(false);
	metadataWidget.getLanguageDropDown().setEnabled(guiEnabled && (isgroup || editable));

	setLanguageDropDownEnabled(guiEnabled && (isgroup || editable));
	
	metadataWidget.getAddCategoryButton().setEnabled(guiEnabled && (isgroup || editable));
	metadataWidget.getRemoveCategoryButton().setEnabled(guiEnabled && (isgroup || editable));
	metadataWidget.getCategoryDropDown().setEnabled(guiEnabled && (isgroup || editable));
	
	metadataWidget.getPrevButton().setEnabled(isgroup && editable && selectedProgramsMetadata.length > 0 && 
						  selectedProgramIndex > 0);
	metadataWidget.getNextButton().setEnabled(isgroup && editable && selectedProgramsMetadata.length > 0 && 
						  (selectedProgramIndex < selectedProgramsMetadata.length - 1));
	metadataWidget.getEditButton().setEnabled(!editable);
	metadataWidget.getSaveButton().setEnabled(isgroup || editable);

	//creatorListModel.setEnabled(guiEnabled && (isgroup || editable));
	//categoryListModel.setEnabled(guiEnabled && (isgroup || editable));
    }

    public CreatorListModel getCreatorListModel() {
	return creatorListModel;
    }
    
    public void unregisterWithProviders() {
	metadataProvider.unregisterWithProvider(getName());
	cacheProvider.unregisterWithProvider(getName());
    }

    public String getName() {
	return METADATA_CONTROLLER;
    }

    public void metadataError(String errorCode, String[] errorParams) {
	errorInference.displayServiceError("Network failure", errorCode, errorParams);
    }

    public void activateMetadataService(boolean activate, String[] error) {
	metadataProviderActive = activate;
	//XXX disable self.
	
	updateGUIButtons();

	if (!activate)
	    errorInference.displayServiceError("Unable to activate metadata service", error);
    }

    public void activateCache(boolean activate, String[] error) {
	cacheProviderActive = activate;
	updateGUIButtons();

	if (!activate)
	    errorInference.displayServiceError("Unable to activate cache", error);
    }

    public void cacheObjectsDeleted(String objectType, String[] objectID){}
    public void cacheObjectsUpdated(String objectType, String[] objectID){}
    public void cacheObjectsInserted(String objectType, String[] objectID){}


    public synchronized void cacheObjectValueChanged(Metadata metadata){
	/*
	String itemID = ((RadioProgramMetadata) metadata).getItemID();
	for (int i = 0; i < selectedProgramsMetadata.length; i++)
	    if (selectedProgramsMetadata[i].getItemID().equals(itemID))
		selectedProgramsMetadata[i] = (RadioProgramMetadata)metadata;

	if (currentActiveProgram.getItemID().equals(itemID))
	    currentActiveProgram = (RadioProgramMetadata) metadata;
	*/
	if (metadata instanceof Category){
	    Category category = (Category) metadata;
	    ArrayList<HashSet<Category>> list = new ArrayList<HashSet<Category>>();
	    list.addAll(addCategories.values());
	    list.addAll(removeCategories.values());
	    list.addAll(currentCategories.values());

	    for (HashSet<Category> set: list){
		for (Category old: set){
		    if (old.getNodeID() == category.getNodeID()){
			set.remove(old);
			set.add(category);
		    }
		}
	    }

	    metadataWidget.updateTags();
	}

    }

    public void cacheObjectKeyChanged(Metadata[] metadata, String newKeys[]){
	for (int i = 0; i < metadata.length; i++){
	    Metadata data = metadata[i];
	    String newKey = newKeys[i];
	    if (data instanceof Category){
		String newName = newKey.substring(0, newKey.lastIndexOf("."));
		if (newName.equals("")){

		}
	    }
	}
    }
    

    public Category getParentCategory(int categoryNodeID) {
	for (Category parent: categoryList) {
	    if (parent.getNodeID() == categoryNodeID)
		return parent;

	    if (!cacheProvider.assocFieldsInitialized(parent)) {
		metadataProvider.populateChildren(parent);
		cacheProvider.setMetadataCacheObject(parent);
		cacheProvider.setAssocFieldsInitialized(parent, true);
	    }

	    Category[] children = parent.getAllChildren();
	    for (Category category: children) {
		if (category.getNodeID() == categoryNodeID)
		    return parent;
	    }
	}
	return null;
    }

    public Category getCategory(int categoryNodeID) {
	for (Category category: categoryList) {
	    if (category.getNodeID() == categoryNodeID)
		return category;

	    if (!cacheProvider.assocFieldsInitialized(category)) {
		metadataProvider.populateChildren(category);
		cacheProvider.setMetadataCacheObject(category);
		cacheProvider.setAssocFieldsInitialized(category, true);
	    }
	    Category[] children = category.getAllChildren();
	    for (Category child: children) {
		if (child.getNodeID() == categoryNodeID)
		    return child;
	    }
	}

	return null;
    }


    public class MetadataListModel implements ListModel {
	ArrayList<ListDataListener> listeners;
	ArrayList<Object> elements;
	Dimension widgetDim;

	public MetadataListModel() {
	    listeners = new ArrayList<ListDataListener>();
	    elements = new ArrayList<Object>();
	}
	
	
	public void addListDataListener(ListDataListener listener){
	    listeners.add(listener);
	}
	
	
	public Object getElementAt(int index){
	    return elements.get(index);
	}
	
	public int getSize(){
	    return elements.size();
	}
	
	public void removeListDataListener(ListDataListener listener){
	    listeners.remove(listener);
	}

	public void setEnabled(boolean enabled) {
	    for (int i = 0; i < elements.size(); i++) {
		Object element = elements.get(i);
		if (element instanceof JComponent)
		    ((JComponent)element).setEnabled(enabled);
	    }
	}

	public void setPreferredWidgetSize(Dimension widgetDim) {
	    this.widgetDim = widgetDim;
	}
    }

    public class CreatorListModel  { 
	ArrayList<Creator> addedCreators;
	ArrayList<Creator> removedCreators;
	HashSet<Creator> editedCreators;

	Creator selectedCreator = null;
	
	Hashtable<Integer, Creator> originalCreators = new Hashtable<Integer, Creator>();

	public CreatorListModel() {
	    super();
	    addedCreators = new ArrayList<Creator>();
	    removedCreators = new ArrayList<Creator>();
	    editedCreators = new HashSet<Creator>();
	}

	public void clear() {
	    addedCreators = new ArrayList<Creator>();
	    removedCreators = new ArrayList<Creator>();
	    editedCreators = new HashSet<Creator>();
	    originalCreators = new Hashtable<Integer, Creator>();
	}

	public void initProgram(RadioProgramMetadata program) {
	    Creator[] creators = program.getCreators();
	    for (Creator creator: creators) {
		originalCreators.put(creator.getEntityID(), creator);
	    }

	}

	public void addCreator(Creator creator) {
	    for (Creator c: removedCreators){
		if (c.getEntity().getEntityID() == creator.getEntity().getEntityID()){
		    removedCreators.remove(c);
		    return;
		}
	    }

	    for (Creator c: originalCreators.values()){
		if (c.getEntity().getEntityID() == creator.getEntity().getEntityID()){
		    return;
		}
	    }

	    for (Creator c: addedCreators){
		if (c.getEntity().getEntityID() == creator.getEntity().getEntityID()){
		    return;
		}
	    }

	    addedCreators.add(creator);
	}

	public void removeCreator(Creator creator) {
	    removedCreators.add(creator);
	    
	    for (int j = 0; j < addedCreators.size(); j++)
		if (addedCreators.get(j) == creator) {
		    addedCreators.remove(j);
		}
	}
	
	public void editCreator(Creator creator){
	    editedCreators.add(creator);
	}
	
	boolean creatorChanged(Creator creator){
	    return editedCreators.contains(creator);
	}

	public Creator[] newCreators() {
	    return addedCreators.toArray(new Creator[]{});
	}

	public Creator[] editCreators() {
	    ArrayList<Creator> editedCreators = new ArrayList<Creator>();
	    
	    for (Creator c: originalCreators.values()){
		int entityID = c.getEntity().getEntityID();
		boolean skip = false;
		for (Creator added: addedCreators)
		    if (added.getEntity().getEntityID() == entityID){
			skip = true;
			break;
		    }
		for (Creator removed: removedCreators)
		    if (removed.getEntity().getEntityID() == entityID){
			skip = true;
			break;
		    }
		if (skip){
		    continue;
		} else {
		    if (creatorChanged(c))
			editedCreators.add(c);
		}
	    }

	    return editedCreators.toArray(new Creator[]{});
	}

	public Creator[] removeCreators() {
	    return removedCreators.toArray(new Creator[]{});
	}
	
    }

    public interface SelectionListener {
	public void unSelect();
	public void setSelectionListenerCallback(SelectionListenerCallback listenerCallback);
    }

    public interface SelectionListenerCallback {
	public void isSelected(JPanel widgetPanel);
    }

    protected void clearLanguageDropDown() {
	metadataWidget.getLanguageDropDown().removeAllItems();
    }

    protected void initLanguageDropDown() {
	metadataWidget.getLanguageDropDown().addItem(nullLanguageItem);
	metadataWidget.getLanguageDropDown().setSelectedItem(nullLanguageItem);
	for (String language: languageList) {
	    //System.out.println("adding " + language);
	    metadataWidget.getLanguageDropDown().addItem(language);
	    if (language.equals(currentActiveProgram.getLanguage()))
		metadataWidget.getLanguageDropDown().setSelectedItem(language);
	}
	metadataWidget.getLanguageDropDown().setActionCommand(MetadataWidget.LANGUAGE);
	metadataWidget.getLanguageDropDown().addActionListener(new LanguageDropDownListener());
    }

    protected void setLanguageDropDownEnabled(boolean editable) {
	metadataWidget.getLanguageDropDown().setEditable(editable);
	//metadataWidget.getTagsField().setEnabled(editable);
    }

    public class LanguageDropDownListener implements ActionListener {
	
	public LanguageDropDownListener() {
	}

	public void actionPerformed(ActionEvent e) {
	    JComboBox combobox = (JComboBox)(e.getSource());
	    String selectedItem = (String)(combobox.getSelectedItem());

	    if (selectedItem != null) {
		if (selectedItem.equals(currentActiveProgram.getLanguage()))
		    languageUpdated = false;
		else {
		    if (!selectedItem.equals(nullLanguageItem)) {
			languageUpdated = true;
			if (newEntry(selectedItem, languageList)) {
			    selectedItem = sanitize(selectedItem);
			    if (metadataProvider.addLanguage(selectedItem) != -1)
				languageList = addElement(selectedItem, metadataWidget.getLanguageDropDown(), languageList, "");
			    else
				errorInference.displayMetadataError("Unable to save language", new String[]{MetadataWidget.UNABLE_TO_ADD_ITEM});
			}
		    }
		}
	    }
	}
	
    }

    protected boolean newEntry(String searchItem, String[] searchList) {
	for (int i = 0; i < searchList.length; i++) {
	    //System.out.print(searchList[i] + " ");
	    if (searchList[i].equals(searchItem))
		return false;
	}
	//System.out.println();
	return true;
    }
    
    
    protected String[] addElement(String newItem, JComboBox combobox, String[] currItems, String nullItem) {
	//System.out.println("adding " + newItem);
	if (!nullItem.equals(""))
	    combobox.removeItem(nullItem);
	combobox.addItem(newItem);
	if (!nullItem.equals(""))
	    combobox.addItem(nullItem);
	ArrayList<String> arr = new ArrayList<String>();
	arr.add(newItem);
	for (String currItem: currItems)
	    arr.add(currItem);
	return arr.toArray(new String[]{});
    }
    
}






