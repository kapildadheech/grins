package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.*;
import org.gramvaani.radio.stationconfig.StationConfiguration;

import javax.swing.*;
import java.awt.Color;

public class BlankWidget extends RSWidgetBase {
    Color componentBgColor;
    StationConfiguration stationConfig;

    public BlankWidget(String name, RSApp rsApp, ControlPanel controlPanel) {
	super("GRINS");
	wpComponent = new JPanel();
	aspComponent = new RSPanel();
	stationConfig = rsApp.getStationConfiguration();
    }

    public boolean onLaunch() {
	componentBgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	wpComponent.setBackground(componentBgColor);

	return true;
    }
    
    public void onUnload() {}
    
    public void onMinimize() {}
    
    public void onMaximize() {}
}