package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.app.providers.MetadataListenerCallback;
import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.providers.SMSProvider;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.telephonylib.RSCallerID;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.border.EmptyBorder;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class SMSWidget extends RSWidgetBase implements MetadataListenerCallback, SendSMSPopupListener, EntityEditorPopup.EntityEditorListener {
    static String SMS_EXPORT_FILE_NAME = "sms.xls";
    RSApp rsApp;
    MetadataProvider metadataProvider;
    CacheProvider cacheProvider;
    boolean isMetadataActive;

    ErrorInference errorInference;
    StationConfiguration stationConfig;
    JTable smsTable;
    SMSTableModel tableModel;
    RSButton addContactButton, removeSMSButton, refreshButton, exportButton, sendButton, cancelButton;

    Color bgColor, titleColor;

    SendSMSPopup sendSMSPopup;
    EntityEditorPopup editorPopup;

    public SMSWidget(RSApp rsApp){
	super("SMS");
	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.SMS_ICON;
	bmpToolTip = "SMS";

	this.rsApp = rsApp;
	stationConfig = rsApp.getStationConfiguration();
	
	cacheProvider    = (CacheProvider) rsApp.getProvider(RSApp.CACHE_PROVIDER);
	metadataProvider = (MetadataProvider) rsApp.getProvider(RSApp.METADATA_PROVIDER);
	
	errorInference = ErrorInference.getErrorInference();
    }

    protected boolean onLaunch(){
	wpComponent.setLayout(new BorderLayout());

	bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	titleColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);
	
	wpComponent.setBackground(bgColor);

	RSTitleBar titleBar = new RSTitleBar(name, (int)wpSize.getWidth());
	titleBar.setBackground(titleColor);
	wpComponent.add(titleBar, BorderLayout.PAGE_START);

	metadataProvider.registerWithProvider(this);
	if (metadataProvider == null || !metadataProvider.isActive())
	    isMetadataActive = false;
	else
	    isMetadataActive = true;

	buildWorkspace();
	return true;
    }


    protected void onMaximize(){

    }

    protected void onMinimize(){
	if (sendSMSPopup != null)
	    sendSMSPopup.setVisible(false);

	if (editorPopup != null)
	    editorPopup.setVisible(false);
    }
    
    public void onUnload(){
	wpComponent.removeAll();
	tableModel.clearData();
	metadataProvider.unregisterWithProvider(getName());
    }

    void buildWorkspace(){
	RSPanel smsPanel = new RSPanel();

	Dimension smsPanelSize = new Dimension(wpSize.width, wpSize.height - RSTitleBar.HEIGHT);
	smsPanel.setBackground(bgColor);
	smsPanel.setPreferredSize(smsPanelSize);

	wpComponent.add(smsPanel, BorderLayout.CENTER);

	smsPanel.setLayout(new GridBagLayout());
	
	int searchBarVertFrac = 5;
	int actionBarVertFrac = 5;

	int searchBarHeight = searchBarVertFrac * smsPanelSize.height/100;
	int actionBarHeight = actionBarVertFrac * smsPanelSize.height/100;
	int smsTableHeight = smsPanelSize.height - searchBarHeight - actionBarHeight;

	RSPanel searchPanel = new RSPanel();
	Dimension searchPanelSize = new Dimension(wpSize.width, searchBarHeight);
	searchPanel.setPreferredSize(searchPanelSize);
	searchPanel.setBackground(bgColor);
	searchPanel.setLayout(new GridBagLayout());

	smsPanel.add(searchPanel, getGbc(0, 0, gfillboth()));

	RSLabel searchLabel = new RSLabel("Search SMS: ");
	searchLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	searchPanel.add(searchLabel);
	
	final JTextField searchField = new JTextField();
	Dimension searchFieldDimension  = new Dimension(wpSize.width/3, searchPanelSize.height - 4);
	searchField.setPreferredSize(searchFieldDimension);
	searchField.getDocument().addDocumentListener(new DocumentAdapter(){
		public void documentUpdated(DocumentEvent e){
		    searchStringUpdated(searchField.getText());
		}

	    });
	searchField.addKeyListener(new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			searchField.setText("");
		    }
		}
	    });
	
	searchPanel.add(searchField);

	RSLabel emptyLabel = new RSLabel(" ");
	searchPanel.add(emptyLabel);

	int buttonWidth = 12 * wpSize.width/100;
	Dimension buttonSize = new Dimension(buttonWidth, searchBarHeight - 4);
	int iconSize = (buttonSize.height * 8)/10;

	refreshButton = new RSButton("Refresh");
	refreshButton.setBackground(bgColor);
	refreshButton.setPreferredSize(buttonSize);
	refreshButton.setIcon(IconUtilities.getIcon(StationConfiguration.REDO_ICON, iconSize));
	refreshButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    tableModel.reloadTable();
		}
	    });

	exportButton = new RSButton("Export");
	exportButton.setBackground(bgColor);
	exportButton.setPreferredSize(buttonSize);
	exportButton.setIcon(IconUtilities.getIcon(StationConfiguration.MAKE_REPORT_ICON, iconSize));
	exportButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    exportMessages();
		}
	    });
	
	searchPanel.add(exportButton);
	searchPanel.add(refreshButton);


	tableModel = new SMSTableModel(metadataProvider, cacheProvider);
	smsTable = new JTable(tableModel);
	smsTable.setFillsViewportHeight(true);
	smsTable.getTableHeader().setReorderingAllowed(false);
	Color headerColor = GUIUtilities.toSaturation(bgColor, 0.05f);
	smsTable.getTableHeader().setBackground(headerColor);

	smsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
		public void valueChanged(ListSelectionEvent e){
		    selectionChanged();
		}
	    });
	
	TableColumn col = smsTable.getColumnModel().getColumn(0);
	col.setPreferredWidth(wpSize.width*12/100);
	col = smsTable.getColumnModel().getColumn(1);
	col.setPreferredWidth(wpSize.width*12/100);
	col = smsTable.getColumnModel().getColumn(2);
	col.setPreferredWidth(wpSize.width*51/100);
	col = smsTable.getColumnModel().getColumn(3);
	col.setPreferredWidth(wpSize.width*15/100);
	col = smsTable.getColumnModel().getColumn(4);
	col.setPreferredWidth(wpSize.width*10/100);

	smsTable.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_DELETE){
			removeMessages();
		    }
		}
	    });
		
	smsTable.setDefaultRenderer(String.class, new MultiLineCellRenderer());

	JScrollPane smsScrollPane = new JScrollPane(smsTable);

	Dimension tableSize = new Dimension(wpSize.width, smsTableHeight);
	smsScrollPane.setPreferredSize(tableSize);
	smsScrollPane.setBackground(headerColor);
	smsPanel.add(smsScrollPane, getGbc(0, 1, gfillboth(), weighty(1.0)));
	smsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	smsScrollPane.getVerticalScrollBar().setBackground(bgColor);

	RSPanel actionPanel = new RSPanel();
	Dimension actionPanelSize = new Dimension(wpSize.width, actionBarHeight);
	actionPanel.setPreferredSize(actionPanelSize);
	actionPanel.setBackground(bgColor);
	actionPanel.setLayout(new GridBagLayout());
	smsPanel.add(actionPanel, getGbc(0, 2, gfillboth()));

	int buttonsWidthHorizFrac = 70;
	buttonWidth = buttonsWidthHorizFrac * wpSize.width/100 / 4;
	buttonSize = new Dimension(buttonWidth, actionBarHeight - 4);
	iconSize = (buttonSize.height * 8)/10;
	
	addContactButton = new RSButton("Add Contact");
	addContactButton.setBackground(bgColor);
	addContactButton.setPreferredSize(buttonSize);
	addContactButton.setIcon(IconUtilities.getIcon(StationConfiguration.NEW_CREATOR_ICON, iconSize));
	addContactButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
		    SMS selection[] = getSelection();
		    Entity contact = new Entity();
		    contact.setPhoneType(RSCallerID.PSTN);
		    contact.setPhone(selection[0].getPhoneNo());
		    editorPopup.showEditor(false, contact, addContactButton);
		}
	    });
	addContactButton.setEnabled(false);
	actionPanel.add(addContactButton);

	removeSMSButton = new RSButton("Remove SMS");
	removeSMSButton.setBackground(bgColor);
	removeSMSButton.setPreferredSize(buttonSize);
	removeSMSButton.setIcon(IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, iconSize));
	removeSMSButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    removeMessages();
		}
	    });
	
	removeSMSButton.setEnabled(false);
	actionPanel.add(removeSMSButton);

	sendButton = new RSButton("Send SMS");
	sendButton.setBackground(bgColor);
	sendButton.setPreferredSize(buttonSize);
	sendButton.setIcon(IconUtilities.getIcon(StationConfiguration.SEND_SMS_ICON, iconSize));
	sendButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    sendSMSToSelection();
		}
	    });
	actionPanel.add(sendButton);

	cancelButton = new RSButton("Cancel Send");
	cancelButton.setBackground(bgColor);
	cancelButton.setPreferredSize(buttonSize);
	cancelButton.setIcon(IconUtilities.getIcon(StationConfiguration.CANCEL_ICON, iconSize));
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    cancelSend();
		}
	    });
	actionPanel.add(cancelButton);

	int popupHorizFrac = 50;
	int popupVertFrac = 35;
	
	int smsPopupWidth = wpSize.width * popupHorizFrac / 100;
	int smsPopupHeight = wpSize.height * popupVertFrac / 100;
	sendSMSPopup = new SendSMSPopup(rsApp, smsPopupWidth, smsPopupHeight, bgColor, titleColor, stationConfig, this);

	int editorPopupWidth = wpSize.width * popupHorizFrac / 100;
	int editorPopupHeight = wpSize.height * popupVertFrac / 100;
	editorPopup = new EntityEditorPopup(this, editorPopupWidth, editorPopupHeight, bgColor, titleColor, stationConfig, rsApp);
	tableModel.reloadTable();
    }

    static final int MIN_QUERY_LENGTH = 3;
    void searchStringUpdated(String query){
	if (query == null)
	    return;

	if (query.length() < MIN_QUERY_LENGTH)
	    query = "";

	tableModel.search(query);
    }

    SMS[] getSelection(){
	ListSelectionModel model = smsTable.getSelectionModel();
	int minIndex = model.getMinSelectionIndex();
	int maxIndex = model.getMaxSelectionIndex();

	if (minIndex < 0 || maxIndex < 0)
	    return new SMS[]{};

	ArrayList<SMS> list = new ArrayList<SMS>();
	for (int i = minIndex; i <= maxIndex; i++){
	    if (model.isSelectedIndex(i))
		list.add(tableModel.getMessageAt(i));
	}

	return list.toArray(new SMS[list.size()]);
    }

    void selectionChanged(){
	SMS selection[] = getSelection();
	
	if (selection.length == 0){
	    removeSMSButton.setEnabled(false);
	    addContactButton.setEnabled(false);
	} else if(selection.length >= 1) {
	    boolean enableAddContact = false;
	    if(selection.length == 1) {
		ListSelectionModel model = smsTable.getSelectionModel();
		int selectedIndex = model.getMinSelectionIndex();
		String name = (String)tableModel.getValueAt(0, selectedIndex);

		if(name.equals(""))
		    enableAddContact = true;
	    }
	    
	    addContactButton.setEnabled(enableAddContact);
	    removeSMSButton.setEnabled(true);
	} 
    }

    void sendSMSToSelection() {
	HashSet<String> phoneNos = new HashSet<String>();
	SMS selection[] = getSelection();

	for(SMS message : selection) {
	    String phoneNo = message.getPhoneNo();
	    phoneNos.add(phoneNo);
	}

	sendSMSPopup.showPopup(phoneNos.toArray(new String[phoneNos.size()]), "", sendButton);
    }

    void cancelSend() {
	SMSProvider smsProvider = (SMSProvider) rsApp.getProvider(RSApp.SMS_PROVIDER);
	SMS selection[] = getSelection();
	ArrayList<String> toCancel = new ArrayList<String>();

	for(SMS message : selection) {
	    logger.info("GRINS_USAGE:CANCEL_SMS:" + message.getPhoneNo() + ":" + message.getMessage());
	    toCancel.add((new Integer(message.getID())).toString());
	}

	smsProvider.cancelSend(toCancel.toArray(new String[toCancel.size()]));

	try{
	    Thread.sleep(300);
	} catch(Exception e){}
	tableModel.reloadTable();
    }

    void exportMessages(){
	SMS selection[] = getSelection();

	if (selection.length == 0) {
	    JOptionPane.showMessageDialog(rsApp.getControlPanel().cpJFrame(), 
					  "No SMS selected for export. Please select SMS you want to export and then press the export button.", "Export SMS", 
					  JOptionPane.WARNING_MESSAGE);
	    return;
	}

	JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	fileChooser.updateUI();

	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int retVal = fileChooser.showDialog(cpJFrame, "Export");

	if (retVal != JFileChooser.APPROVE_OPTION)
	    return;

	String destFolder = null;
	try{
	    destFolder = fileChooser.getSelectedFile().getCanonicalPath()+System.getProperty("file.separator");
	} catch (Exception e){
	    logger.error("ExportFiles: Unable to find canonical path of selected folder.");
	    return;
	}
	String destFile = destFolder + SMS_EXPORT_FILE_NAME;
	String separator = "\t";
	ArrayList<String> entries = new ArrayList<String>();
	entries.add("Date/Time" + separator + "Number" + separator + "Name" + separator + "Message" + separator + "Status");
	
	String status;
	for(SMS sms :selection) {
	    if(sms.getType() == SMS.INCOMING)
		status = SMS.RECV_STATUS.get(sms.getStatus());
	    else
		status = SMS.SEND_STATUS.get(sms.getStatus());

	    entries.add(sms.getDateTime() + separator + sms.getPhoneNo() + separator + 
			tableModel.getContactName(sms) + separator + sms.getMessage() + 
			separator + status);
	}

	if(FileUtilities.writeToXLS(destFile, entries, separator, true)) {
	    FileUtilities.setPermissions(destFile, false);
	    JOptionPane.showMessageDialog(null, "SMS exported successfully at: " + destFile, 
					  "information", JOptionPane.INFORMATION_MESSAGE);
	} else {
	    JOptionPane.showMessageDialog(null, "SMS export failed!!", "information", JOptionPane.INFORMATION_MESSAGE);
	}
    }

    void removeMessages(){
	SMS selection[] = getSelection();
	
	if (selection.length == 0)
	    return;
	
	String confirm = "Are you sure you want to remove " + selection.length + " selected SMS(es)?";
	String title = "Remove Selected SMS";
	int retVal = JOptionPane.showConfirmDialog(rsApp.getControlPanel().cpJFrame(), confirm, title, JOptionPane.YES_NO_OPTION);
	if (retVal != JOptionPane.YES_OPTION)
	    return;
	
	for (SMS message: selection) {
	    SMS dummyMessage = new SMS(message.getID());
	    metadataProvider.remove(dummyMessage, false);
	}

	tableModel.reloadTable();
    }

    public void sendDone() {
	try{
	    Thread.sleep(300);
	} catch(Exception e){}	
	tableModel.reloadTable();
    }

    /* Callbacks from entity editor popup */

    public void addEntity(String name, String url, String phoneType, String phoneNumber, String email, String location, String entityType){
	logger.info("GRINS_USAGE: ADD_CONTACT_CONTACTS_WIDGET");
	RSCallerID callerID = new RSCallerID(phoneType, phoneNumber);
	metadataProvider.addEntity(name, url, phoneType, callerID.toDBString(), email, location, entityType);
	tableModel.reloadTable();
    }

    public void editEntity(int id, String name, String url, String phoneType, String phoneNumber, String email, String location, String entityType){
	logger.info("GRINS_USAGE: EDIT_CONTACT_CONTACTS_WIDGET");
	RSCallerID callerID = new RSCallerID(phoneType, phoneNumber);
	metadataProvider.editEntity(id, name, url, phoneType, callerID.toDBString(), email, location, entityType);
	tableModel.reloadTable();
    }
    
    /*End of callbacks*/

    /* Callbacks from Providers */
    
    public void activateMetadataService(boolean activate, String[] error){
	isMetadataActive = activate;
    }
    
    /* End of Callbacks */
    
    class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {
	List<List<Integer>> rowColHeight = new ArrayList<List<Integer>>();
	
	public MultiLineCellRenderer() {
	    setLineWrap(true);
	    setWrapStyleWord(true);
	    setOpaque(true);
	}
	
	public Component getTableCellRendererComponent(JTable table, Object value,
						       boolean isSelected, boolean hasFocus, int row, int column) {
	    if (isSelected) {
		setForeground(table.getSelectionForeground());
		setBackground(table.getSelectionBackground());
	    } else {
		setForeground(table.getForeground());
		setBackground(table.getBackground());
	    }
	    setFont(table.getFont());
	    if (hasFocus) {
		setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
		if (table.isCellEditable(row, column)) {
		    setForeground(UIManager.getColor("Table.focusCellForeground"));
		    setBackground(UIManager.getColor("Table.focusCellBackground"));
		}
	    } else {
		setBorder(new EmptyBorder(1, 2, 1, 2));
	    }
	    setText((value == null) ? "" : value.toString());
	    
	    adjustRowHeight(table, row, column);
	    return this;
	}

	void adjustRowHeight(JTable table, int row, int column) {
	    int cWidth = table.getTableHeader().getColumnModel().getColumn(column).getWidth();
	    setSize(new Dimension(cWidth, 1000));
	    int prefH = getPreferredSize().height;
	    while (rowColHeight.size() <= row) {
		rowColHeight.add(new ArrayList<Integer>(column));
	    }
	    List<Integer> colHeights = rowColHeight.get(row);
	    while (colHeights.size() <= column) {
		colHeights.add(0);
	    }
	    colHeights.set(column, prefH);
	    int maxH = prefH;
	    for (Integer colHeight : colHeights) {
		if (colHeight > maxH) {
		    maxH = colHeight;
		}
	    }
	    if (table.getRowHeight(row) != maxH) {
		table.setRowHeight(row, maxH);
	    }
	}
	
    }
}


