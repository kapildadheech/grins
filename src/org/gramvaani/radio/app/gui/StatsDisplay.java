package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.medialib.CategoryFilter;
import org.gramvaani.radio.medialib.CreatorFilter;
import org.gramvaani.radio.medialib.LanguageFilter;
import org.gramvaani.radio.medialib.LengthFilter;
import org.gramvaani.radio.medialib.LicenseFilter;
import org.gramvaani.radio.medialib.PlayoutHistoryFilter;
import org.gramvaani.radio.medialib.Metadata;
import org.gramvaani.radio.medialib.PlayoutHistory;
import org.gramvaani.radio.medialib.RadioProgramMetadata;
import org.gramvaani.radio.medialib.SearchFilter;
import org.gramvaani.radio.medialib.TypeFilter;
import org.gramvaani.radio.medialib.GraphStats;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;

import java.util.*;
import java.text.DateFormat;

public class StatsDisplay extends JPanel {
    public static int GRAPH_HEIGHT = 126;

    public static int BAR_GRAPH = 0;
    public static int POINT_GRAPH = 1;

    public static int MAX_BINS = 10;
    public static String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    SearchFilter[] activeFilters;
    Dimension displayDim;
    MetadataProvider metadataProvider;
    LibraryProvider libraryProvider;

    BarGraph graph;
    int graphType;
    Canvas canvas;

    boolean freshInit = false;
    boolean displayTitle = true;

    StationConfiguration stationConfig;

    public StatsDisplay(MetadataProvider metadataProvider, LibraryProvider libraryProvider, Dimension displayDim) {
	this.displayDim = displayDim;
	this.activeFilters = null;
	this.metadataProvider = metadataProvider;
	this.libraryProvider = libraryProvider;
	this.graphType = BAR_GRAPH;

	setSize((int)(displayDim.getWidth()), (int)(displayDim.getHeight()));
	setOpaque(false);

	RSLabel blankLabel = new RSLabel("");
	blankLabel.setPreferredSize(displayDim);
	add(blankLabel);

	freshInit = true;
	
	if(libraryProvider != null)
	    stationConfig = libraryProvider.getStationConfig();
	else
	    stationConfig = metadataProvider.getStationConfig();
    }

    public static ArrayList<String> getInfoStrings(SearchFilter[] activeFilters, MetadataProvider metadataProvider) {
	//logger.debug("GetInfoStrings: getInfoStrings: " + activeFilters.length);
	ArrayList<String> infoStrings = new ArrayList<String>();
	if(activeFilters != null) {
	    for(SearchFilter filter: activeFilters) {
		String filterName = filter.getFilterName();

		if(filterName.equals(SearchFilter.LANGUAGE_FILTER)) {
		    infoStrings.add(filter.getFilterLabel() + ": " + filter.getSelectedText(LanguageFilter.LANGUAGE_ANCHOR));
		} else if(filterName.equals(SearchFilter.CREATOR_FILTER)) {
		    infoStrings.add(filter.getFilterLabel() + ": " + 
				    (filter.getSelectedText(CreatorFilter.NAME_ANCHOR).equals("") ? "" :
				     filter.getSelectedText(CreatorFilter.NAME_ANCHOR) + ", ") +
				    (filter.getSelectedText(CreatorFilter.LOCATION_ANCHOR).equals("") ? "" :
				     filter.getSelectedText(CreatorFilter.LOCATION_ANCHOR) + ", ") +
				    (filter.getSelectedText(CreatorFilter.ROLE_ANCHOR).equals("") &&
				     filter.getSelectedText(CreatorFilter.AFFILIATION_ANCHOR).equals("") ? "" :
				     "(" +
				     (filter.getSelectedText(CreatorFilter.ROLE_ANCHOR).equals("") ? "" :
				      filter.getSelectedText(CreatorFilter.ROLE_ANCHOR) + ", ") +
				     (filter.getSelectedText(CreatorFilter.AFFILIATION_ANCHOR).equals("") ? "" :
				      filter.getSelectedText(CreatorFilter.AFFILIATION_ANCHOR)) +
				     ")"));
		} else if(filterName.equals(SearchFilter.TYPE_FILTER)) {
		    infoStrings.add(filter.getFilterLabel() + ": " + filter.getSelectedText(TypeFilter.TYPE_ANCHOR));
		} else if(filterName.equals(SearchFilter.LICENSE_FILTER)) {
		    infoStrings.add(filter.getFilterLabel() + ": " + filter.getSelectedText(LicenseFilter.LICENSE_ANCHOR));
		} else if(filterName.equals(SearchFilter.LENGTH_FILTER)) {
		    long length = ((LengthFilter)filter).getSelectedLength();
		    int hours = (int)(length / 1000 / 60 / 60);
		    int min = (int)(length / 1000 / 60 - hours * 60);
		    int sec = (int)(length / 1000 - hours * 60 * 60 - min * 60);
		    infoStrings.add(filter.getFilterLabel() + ": " + hours + ":" + min + ":" + sec);
		} else if(filterName.startsWith(SearchFilter.CATEGORY_FILTER)) {
		    infoStrings.add(filter.getFilterLabel() + ": " + filter.getSelectedText(CategoryFilter.CATEGORY_ANCHOR));
		} else if(filterName.equals(SearchFilter.NEWITEM_FILTER)) {
		    infoStrings.add(filter.getFilterLabel());
		} else if(filterName.equals(SearchFilter.PLAYOUTHISTORY_FILTER)) {
		    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
		    //infoStrings.add(filter.getFilterLabel() + ": "  + df.format(new Date(metadataProvider.getMinTelecastTime())) + " to " +
		    //df.format(new Date(metadataProvider.getMaxTelecastTime())));
		    PlayoutHistoryFilter historyFilter = (PlayoutHistoryFilter) filter;
		    infoStrings.add(filter.getFilterLabel() + ": "  + df.format(new Date(historyFilter.getStartTime())) + " to " +
				    df.format(new Date(historyFilter.getEndTime())));
		}
	    }
	}

	return infoStrings;
    }

    protected void drawGraph(GraphStats graphStats, int yTics, ArrayList<String> infoStrings) {
	String title;
	if(displayTitle) {
	    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
	    title = "Playout history: "  + df.format(new Date(graphStats.getMinTelecastTime())) + " to " +
		df.format(new Date(graphStats.getMaxTelecastTime()));
	} else {
	    title = "";
	}

	title += " Total: " + graphStats.getSumDataItems();

	if(graphType == BAR_GRAPH) {	    
	    graph = new BarGraph(title,
				 graphStats.getMinTelecastTime(), graphStats.getMaxTelecastTime(), 
				 graphStats.getXLabels(), graphStats.getDataItems(), 
				 graphStats.getApproxBinInterval(), graphStats.getMaxDataItem(), 
				 (infoStrings == null ? new String[]{} : infoStrings.toArray(new String[]{})), displayDim, yTics, stationConfig);
	} else {
	    graph = new PointGraph(title,
				   graphStats.getMinTelecastTime(), graphStats.getMaxTelecastTime(), graphStats.getXLabels(),
				   graphStats.getDataItems(), graphStats.getApproxBinInterval(), graphStats.getMaxDataItem(), 
				   (infoStrings == null ? new String[]{} : infoStrings.toArray(new String[]{})), displayDim, yTics, stationConfig,
				   (int)((double)20 / GRAPH_HEIGHT * (double)(displayDim.getHeight())));
	}

	if(!freshInit) {
	    repaint();
	}
	
	freshInit = false;
    }

    public void refresh(SearchSession searchSession, long minTelecastTime, long maxTelecastTime, int yTics) {
	ArrayList<String> infoStrings = getInfoStrings(activeFilters, metadataProvider);
	if(graphType == BAR_GRAPH) {
	    GraphStats barGraphStats = searchSession.getBarGraphStats(minTelecastTime, maxTelecastTime);
	    drawGraph(barGraphStats, yTics, infoStrings);
	} else {
	    GraphStats pointGraphStats = searchSession.getPointGraphStats(minTelecastTime, maxTelecastTime);
	    drawGraph(pointGraphStats, yTics, infoStrings);
	}
    }

    public void refresh(RadioProgramMetadata[] programs, long minTelecastTime, long maxTelecastTime, int yTics) {
	String[] programIDs = new String[programs.length];
	int i = 0;
	for(RadioProgramMetadata program: programs) {
	    programIDs[i] = program.getItemID();
	    i++;
	}
	refresh(programIDs, minTelecastTime, maxTelecastTime, yTics);
    }

    public void refresh(String[] programIDs, long minTelecastTime, long maxTelecastTime, int yTics) {
	//logger.debug("Refresh: called with " + minTelecastTime + ":" + maxTelecastTime + ":" + System.currentTimeMillis());

	StringBuilder str = new StringBuilder();
	for(String programID: programIDs) {
	    str.append(PlayoutHistory.getItemIDFieldName());
	    str.append("='");
	    str.append(programID);
	    str.append("' or ");
	}

	String whereClause;
	if(str.length() > 0)
	    whereClause = str.substring(0, str.length() - 3);
	else
	    whereClause = "";
	
	if(graphType == BAR_GRAPH) {
	    GraphStats barGraphStats = metadataProvider.getBarGraphStats(whereClause, minTelecastTime, maxTelecastTime);
	    drawGraph(barGraphStats, yTics, null);
	} else {
	    GraphStats pointGraphStats = metadataProvider.getPointGraphStats(whereClause, minTelecastTime, maxTelecastTime);
	    drawGraph(pointGraphStats, yTics, null);
	}

    }

    public void setActiveFilters(SearchFilter[] activeFilters) {
	this.activeFilters = activeFilters;

    }

    public void setDisplayTitle(boolean displayTitle) {
	this.displayTitle = displayTitle;
    }

    public void setGraphType(int graphType) {
	this.graphType = graphType;
    }

    protected void paintComponent(Graphics g) {
	super.paintComponent(g);
	graph.drawGraph((Graphics2D)g, getFontMetrics(getFont()));

    }

}
