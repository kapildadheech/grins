package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.app.providers.*;

import org.gramvaani.radio.medialib.LibConnectionPool;
import org.gramvaani.radio.medialib.SearchFilter;
import org.gramvaani.radio.medialib.SearchFilterThin;
import org.gramvaani.radio.medialib.SearchResult;
import org.gramvaani.radio.medialib.RadioProgramMetadata;
import org.gramvaani.radio.medialib.PlayoutHistoryFilter;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

public class LibraryController implements ActionListener, LibraryListenerCallback, 
					  PlayoutPreviewListenerCallback, MetadataListenerCallback,
					  CacheListenerCallback, MultiListMoreItems, ChangeListener {
    public final static String UNKNOWN_ERROR = "Unknown error";
    public final static String UNABLE_TO_SEND_COMMAND = "Unable to send command";
    public final static String UNABLE_TO_PLAY = "Unable to play";
    public final static String UNABLE_TO_STOP = "Unable to stop";

    public final static String LIBRARY_CONTROLLER = "LIBRARY_CONTROLLER";

    LibraryWidget libraryWidget;
    RSApp rsApp;
    ControlPanel controlPanel;
    LibraryProvider libraryProvider;
    PreviewProvider previewProvider;
    MetadataProvider metadataProvider;
    CacheProvider cacheProvider;
    StationConfiguration stationConfiguration;
    LogUtilities logger;
    
    LibConnectionPool connectionPool;
    Hashtable<String, SearchSession> searchSessions;
    Hashtable<String, ActiveFiltersModel> activeFiltersSessions;
    boolean libraryProviderActive;
    boolean previewProviderActive;
    boolean previewUIEnabled;
    boolean metadataProviderActive;
    boolean cacheProviderActive;
    TimeKeeper timekeeper;

    boolean listMode;
    StatsDisplay statsDisplay;
    ErrorInference errorInference;

    Hashtable<String, String> previewProgramSessions;

    public LibraryController(LibraryWidget libraryWidget, ControlPanel controlPanel, RSApp rsApp) {
	this.libraryWidget = libraryWidget;
	this.controlPanel = controlPanel;
	this.rsApp = rsApp;
	this.stationConfiguration = rsApp.getStationConfiguration();
	this.logger = new LogUtilities("LibraryController");
	
	timekeeper = rsApp.getUIService().getTimeKeeper();
	connectionPool = new LibConnectionPool(rsApp.getUIService().getStationConfiguration(), timekeeper);
	searchSessions = new Hashtable<String, SearchSession>();
	activeFiltersSessions = new Hashtable<String, ActiveFiltersModel>();
	
	previewProviderActive = false;
	previewUIEnabled = false;
	previewProgramSessions = new Hashtable<String, String>();

	metadataProviderActive = false;

	listMode = true;

	errorInference = ErrorInference.getErrorInference();
	rsApp.getUIService().registerController(this);
    }

    public void init() {
	this.libraryProvider = (LibraryProvider)(rsApp.getProvider(RSApp.LIBRARY_PROVIDER));
	this.previewProvider = (PreviewProvider)(rsApp.getProvider(RSApp.PREVIEW_PROVIDER));
	this.metadataProvider = (MetadataProvider)(rsApp.getProvider(RSApp.METADATA_PROVIDER));
	this.cacheProvider = (CacheProvider)(rsApp.getProvider(RSApp.CACHE_PROVIDER));

	libraryProvider.registerWithProvider(this);
	previewProvider.registerWithProvider(this);
	metadataProvider.registerWithProvider(this);
	cacheProvider.registerWithProvider(this);

	if (libraryProvider == null || !libraryProvider.isActive())
 	    libraryProviderActive = false;
	else
	    libraryProviderActive = true;

	if (previewProvider == null || !previewProvider.isActive())
	    activateAudio(false, AudioService.PLAYOUT, new String[]{});
	else
	    activateAudio(true, AudioService.PREVIEW, new String[]{});

	if (metadataProvider == null || !metadataProvider.isActive())
	    metadataProviderActive = false;
	else
	    metadataProviderActive = true;
	
	if (cacheProvider == null || !cacheProvider.isActive()) {
	    cacheProviderActive = false;
	} else {
	    cacheProviderActive = true;
	}

	updateGUIButtons();
    }

    public void setUI(boolean active){
	if (active)
	    rsApp.getControlPanel().cpJFrame().setEnabled();
	else
	    rsApp.getControlPanel().cpJFrame().setDisabled();
    }

    public void unregisterWithProviders() {
	libraryProvider.unregisterWithProvider(getName());
	previewProvider.unregisterWithProvider(getName());
	metadataProvider.unregisterWithProvider(getName());
	cacheProvider.unregisterWithProvider(getName());
    }

    public RSApp getRSApp() {
	return rsApp;
    }

    
    public void stateChanged(ChangeEvent e) {
	if (e.getSource() instanceof JTabbedPane) {
	    logger.debug("StateChanged: Num tabs = " + (((JTabbedPane)(e.getSource())).getTabCount()));
	    if (((JTabbedPane)(e.getSource())).getTabCount() >= stationConfiguration.getIntParam(StationConfiguration.MAX_SEARCH_SESSIONS)) {
		libraryWidget.reinitAspComponent();
	    }
	}
    }
    

    public void deletePrograms(String[] programs) {
	libraryProvider.deletePrograms(programs);
    }

    public void removeProgramFromSession(String sessionID, String itemID) {
	SearchSession session = searchSessions.get(sessionID);
	session.removeProgramFromSearchResult(itemID);
    }

    public void actionPerformed(ActionEvent e) {
	String actionCommand = e.getActionCommand();
	
	if (actionCommand.startsWith(LibraryWidget.SEARCH + "_")) {

	    String sessionID = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    search(libraryWidget.getSearchText(sessionID), sessionID);

	    logger.info("GRINS_USAGE:LIBRARY_ACTION:SEARCH:"+libraryWidget.getSearchText(sessionID));

	} else if (actionCommand.startsWith(LibraryWidget.CLEAR + "_")) {

	    String sessionID = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    libraryWidget.clearSearchText(sessionID);
	    libraryWidget.invokeSearch(sessionID);

	} else if (actionCommand.startsWith(LibraryWidget.FILTER + "_")) {

	    String dummy = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    String sessionID = dummy.substring(0, dummy.indexOf("_"));
	    String filterName = dummy.substring(dummy.indexOf("_") + 1);
	    SearchFilter filter = null;
	    if (!filterName.startsWith(SearchFilter.CATEGORY_FILTER))
	       filter = searchSessions.get(sessionID).getFilter(filterName);
	    else
		filter = searchSessions.get(sessionID).getCategoryFilter(filterName);
	    searchSessions.get(sessionID).displayFilter(filter);
	    ActiveFilterWidget filterWidget = FilterWidgetFactory.getFilterWidget(filter, activeFiltersSessions.get(sessionID), 
										  this, libraryWidget, sessionID);
	    activeFiltersSessions.get(sessionID).addActiveFilter(filterWidget);
	    libraryWidget.activateDisplayFilter(sessionID, filterName, false);
	    
	} else if (actionCommand.startsWith(LibraryWidget.INFO)) {

	    //System.err.println("********** info action: " + actionCommand + ":" + metadataProviderActive);
	    String dummy = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    String sessionID = dummy.substring(0, dummy.indexOf("_"));
	    String programID = dummy.substring(dummy.indexOf("_") + 1);
	    if (metadataProviderActive) {
		MetadataWidget metadataWidget = MetadataWidget.getMetadataWidget(rsApp, new String[]{programID}, false);
		if (!metadataWidget.isLaunched()) {
		    controlPanel.initWidget(metadataWidget, false);
		    controlPanel.cpJFrame().launchWidget(metadataWidget);
		} else {
		    controlPanel.cpJFrame().setWidgetMaximized(metadataWidget);
		}
	    }

	} else if (actionCommand.startsWith(LibraryWidget.PREVIEW)) {

	    String dummy = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    String sessionID = dummy.substring(0, dummy.indexOf("_"));
	    String programID = dummy.substring(dummy.indexOf("_") + 1);

	    if (previewProviderActive && previewProvider.isPlayable()) {
		if (previewProvider.play(programID, this) < 0)
		    errorInference.displayLibraryError("Unable to send preview-start command", new String[]{UNABLE_TO_SEND_COMMAND});
		else
		    previewProgramSessions.put(programID, sessionID);
	    }
	    //System.out.println("************** preview action: " + sessionID + ":" + programID);

	} else if (actionCommand.startsWith(LibraryWidget.STOP)) {
	    
	    String dummy = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    String sessionID = dummy.substring(0, dummy.indexOf("_"));
	    String programID = dummy.substring(dummy.indexOf("_") + 1);

	    if (previewProviderActive && previewProvider.isStoppable(programID, this)) {
		if (previewProvider.stop(programID, this) < 0)
		    errorInference.displayLibraryError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
	    }
	    //System.out.println("************** stop action: " + sessionID + ":" + programID);
	    
	} else if (actionCommand.startsWith(LibraryWidget.EDIT)) {

	    String sessionID = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    Object[] selectedRows = libraryWidget.getLibraryTab(sessionID).getResultMultiList().getSelectedValues();
	    if (selectedRows == null || selectedRows.length == 0)
		return;

	    String[] selectedProgramIDs = new String[selectedRows.length];
	    int i = 0;
	    for (Object selectedRow: selectedRows) {
		selectedProgramIDs[i++] = ((RadioProgramMetadata)selectedRow).getItemID();
	    }
	    
	    if (selectedProgramIDs.length > 0) {
		MetadataWidget metadataWidget = MetadataWidget.getMetadataWidget(rsApp, selectedProgramIDs, false);
		if (!metadataWidget.isLaunched()) {
		    controlPanel.initWidget(metadataWidget, false);
		    controlPanel.cpJFrame().launchWidget(metadataWidget);
		} else {
		    controlPanel.cpJFrame().setWidgetMaximized(metadataWidget);
		}		
	    }

	} else if (actionCommand.startsWith(LibraryWidget.STAT)) {

	    String sessionID = actionCommand.substring(actionCommand.indexOf("_") + 1);	    
	    
	    libraryWidget.getLibraryTab(sessionID).getStatsPanel().removeAll();
	    SearchFilter[] activeFilters = searchSessions.get(sessionID).getActiveFilters();
	    long minTelecastTime = -1;
	    long maxTelecastTime = -1;
	    for (SearchFilter activeFilter: activeFilters) {
		if (activeFilter.getFilterName().equals(SearchFilter.PLAYOUTHISTORY_FILTER)) {
		    if ((minTelecastTime = ((PlayoutHistoryFilter)activeFilter).getStartTime()) < 0)
			minTelecastTime = metadataProvider.getMinTelecastTime();
		    if ((maxTelecastTime = ((PlayoutHistoryFilter)activeFilter).getEndTime()) < 0)
			maxTelecastTime = metadataProvider.getMaxTelecastTime();
		}
	    }
	    libraryWidget.getLibraryTab(sessionID).getStatsPanel().add(statsDisplay = 
								       new StatsDisplay(metadataProvider, libraryProvider,
											libraryWidget.getLibraryTab(sessionID).getStatsPanelSize()));
	    statsDisplay.setActiveFilters(activeFilters);
	    statsDisplay.refresh(searchSessions.get(sessionID), minTelecastTime, maxTelecastTime, -1);
	    
	    listMode = false;
	    updateGUIButtons();
	    libraryWidget.displayStats(sessionID);

	} else if (actionCommand.startsWith(LibraryWidget.LIST)) {

	    String sessionID = actionCommand.substring(actionCommand.indexOf("_") + 1);	    
	    listMode = true;
	    updateGUIButtons();
	    libraryWidget.displayList(sessionID);
	}
    }

    public void updateFilters(){
	for (ActiveFiltersModel filtersModel: activeFiltersSessions.values()){
	    for (int i = 0; i < filtersModel.getSize(); i++){
		ActiveFilterWidget filterWidget = (ActiveFilterWidget) filtersModel.getElementAt(i);
		filterWidget.update();
	    }
	}
    }

    public void playActive(boolean success, String programID, String playType, long telecastTime, String[] error) {
	libraryWidget.setPressedPreviewButton(previewProgramSessions.get(programID), programID, success);
	if (!success)
	    errorInference.displayServiceError("Unable to play file", error);
	updateGUIButtons();
    }

    public void pauseActive(boolean success, String programID, String playType, String[] error) {
	// do nothing
    }

    public void stopActive(boolean success, String programID, String playType, String[] error) {
	//libraryWidget.setPressedStopButton(previewProgramSessions.get(programID), success);
	if (success)
	    libraryWidget.setPressedPreviewButton(previewProgramSessions.get(programID), programID, false);
	String sessionID = previewProgramSessions.remove(programID);
	if (!success)
	    errorInference.displayServiceError("Unable to stop file", error);
	libraryWidget.getLibraryTab(sessionID).getResultList().repaint();
    }

    public void playDone(boolean success, String programID, String playType) {
	//libraryWidget.setPressedStopButton(previewProgramSessions.get(programID), success);
	if (success)
	    libraryWidget.setPressedPreviewButton(previewProgramSessions.get(programID), programID, false);
	String sessionID = previewProgramSessions.remove(programID);
	if (!success)
	    errorInference.displayServiceError("Error occured while playing file", new String[]{});
	libraryWidget.getLibraryTab(sessionID).getResultList().repaint();
    }

    public void audioGstError(String programID, String error, String playType){
        errorInference.displayGstError("Error encountered while playing file", error);
    }

    public void playoutPreviewError(String errorCode, String[] errorParams) {
	String programID = errorParams[0];
	libraryWidget.setPressedStopButton(previewProgramSessions.get(programID), false);
	previewProgramSessions.remove(programID);

	errorInference.displayServiceError("Error occured during preview", errorCode, errorParams);
    }

    public void activateAudio(boolean activate, String playType, String[] error) {
	previewProviderActive = activate;
	previewUIEnabled = activate;
	if (!activate) {
	    Enumeration<String> e = previewProgramSessions.keys();
	    while (e.hasMoreElements()) {
		String program = e.nextElement();
		libraryWidget.setPressedPreviewButton(previewProgramSessions.get(program), program, false);
	    }
	    previewProgramSessions.clear();
	}
	updateGUIButtons();

	if (!activate)
	    errorInference.displayServiceError("Unable to activate audio service", error);
    }

    public void enableAudioUI(boolean enable, String playType, String[] error) {
	previewUIEnabled = enable;
	//libraryWidget.enablePreviewUI(enable);
	updateGUIButtons();
	if (!enable)
	    errorInference.displayServiceError("Unable to enable preview", error);
    }

    public void activateMetadataService(boolean activate, String[] error) {
	metadataProviderActive = activate;
	updateGUIButtons();

	if (!activate)
	    errorInference.displayServiceError("Unable to activate metadata service", error);
    }

    protected void updateGUIButtons() {
	//System.out.println("---------- called updateGUIButtons: " + libraryProviderActive);
	if (previewProviderActive && previewUIEnabled) {
	    libraryWidget.activatePreview(true);
	} else if (previewProviderActive && !previewUIEnabled) {
	    libraryWidget.enablePreviewUI(false);
	} else {
	    libraryWidget.activatePreview(false);
	}

	if (metadataProviderActive && cacheProviderActive)
	    libraryWidget.activateInfo(true);
	else
	    libraryWidget.activateInfo(false);

	if (libraryProviderActive && cacheProviderActive) {
	    //System.out.println("updateGUI: " + libraryProviderActive + ":" + listMode);
	    libraryWidget.activateSearch(true);
	    libraryWidget.activateAllDisplayFilters(true, activeFiltersSessions);

	    for (ActiveFiltersModel activeFiltersModel: activeFiltersSessions.values()) {
		activeFiltersModel.activateAllActiveFilters(true);
	    }

	    if (listMode) {
		libraryWidget.activateListStats(true, true, false);
	    } else {
		libraryWidget.activateListStats(false, false, true);
	    }
	} else {
	    libraryWidget.activateSearch(false);
	    libraryWidget.activateAllDisplayFilters(false, activeFiltersSessions);

	    for (ActiveFiltersModel activeFiltersModel: activeFiltersSessions.values()) {
		activeFiltersModel.activateAllActiveFilters(false);
	    }

	    libraryWidget.activateListStats(false, false, false);
	}
    }

    public String getName() {
	return LIBRARY_CONTROLLER;
    }


    protected void search(String query, String sessionID){

	SearchResult result = textSearch(sessionID, query);
	updateResults(result, sessionID);

	SearchFilter[] displayFilters = result.getDisplayFilters();
	for (SearchFilter displayFilter: displayFilters) {
	    if (activeFiltersSessions.get(sessionID).getActiveFilterIndex(displayFilter.getFilterName()) == -1 &&
	       !displayFilter.getFilterName().equals(SearchFilter.TEXT_FILTER)) {
		ActiveFilterWidget filterWidget = FilterWidgetFactory.getFilterWidget(displayFilter, activeFiltersSessions.get(sessionID), 
										      this, libraryWidget, sessionID);
		activeFiltersSessions.get(sessionID).addActiveFilter(filterWidget);
		libraryWidget.activateDisplayFilter(sessionID, displayFilter.getFilterName(), false);
	    }
	}
    }

    public void updateResults(SearchResult result, String sessionID) {
	if (result == null){
	    logger.error("UpdateResults: Null searchresult found.");
	    return;
	}

	LibraryWidget.LibraryTab libraryTab = libraryWidget.getLibraryTab(sessionID);
	libraryTab.getResultList().clearSelection();
	RadioProgramMetadata[] data = result.getPrograms();

	RSMultiList tableList = libraryTab.getResultMultiList();
	tableList.clear();
	
	if (data != null) {
	    tableList.setNumSearchResults(result.numSearchResults());
	    for (RadioProgramMetadata i: data){
		if (i == null)
		    logger.error("UpdateResults: Null metadata found.");
		else {
		    tableList.add(i);
		    logger.debug("DUMP: " + i.dump());
		}
	    }
	    libraryWidget.refreshResults(sessionID);
	}
	
	libraryWidget.reinitAspComponent();

	if (!listMode) {
	    SearchFilter[] activeFilters = searchSessions.get(sessionID).getActiveFilters();
	    long minTelecastTime = -1;
	    long maxTelecastTime = -1;
	    for (SearchFilter activeFilter: activeFilters) {
		if (activeFilter.getFilterName().equals(SearchFilter.PLAYOUTHISTORY_FILTER)) {
		    if ((minTelecastTime = ((PlayoutHistoryFilter)activeFilter).getStartTime()) < 0)
			minTelecastTime = metadataProvider.getMinTelecastTime();
		    if ((maxTelecastTime = ((PlayoutHistoryFilter)activeFilter).getEndTime()) < 0)
			maxTelecastTime = metadataProvider.getMaxTelecastTime();
		}
	    }
	    statsDisplay.setActiveFilters(activeFilters);
	    statsDisplay.refresh(searchSessions.get(sessionID), minTelecastTime, maxTelecastTime, -1);
	}
    }

    public void activateSearch(boolean success, String[] error) {
	libraryProviderActive = success;
	updateGUIButtons();

	if (!success)
	    errorInference.displayServiceError("Unable to activate library search", error);
    }

    public void diskSpaceChecked(String machineIP, String dirName, long usedSpace, long availableSpace){

    }

    public void activateCache(boolean activate, String[] error) {
	cacheProviderActive = activate;
	updateGUIButtons();

	if (!activate)
	    errorInference.displayServiceError("Unable to activate cache service", error);
    }

    public void cacheObjectsDeleted(String objectType, String[] objectID){}
    public void cacheObjectsUpdated(String objectType, String[] objectID){}
    public void cacheObjectsInserted(String objectType, String[] objectID){}

    public CacheProvider getCacheProvider() {
	return cacheProvider;
    }

    public MetadataProvider getMetadataProvider(){
	return metadataProvider;
    }

    public ActiveFiltersModel getActiveFiltersModel(String sessionID) {
	return activeFiltersSessions.get(sessionID);
    }

    public SearchFilter[] getAllFilters(String sessionID) {
	return searchSessions.get(sessionID).getAllFilters();
    }

    public SearchFilterThin[] getAllCategoryFilters(String sessionID) {
	return searchSessions.get(sessionID).getAllCategoryFilters();
    }

    public void initDefaultDisplayFilters(String sessionID) {
	SearchFilter[] defaultFilters = searchSessions.get(sessionID).getDefaultDisplayFilters();
	for (SearchFilter defaultFilter: defaultFilters) {
	    ActiveFilterWidget filterWidget = FilterWidgetFactory.getFilterWidget(defaultFilter, 
										  activeFiltersSessions.get(sessionID), this, libraryWidget, sessionID);
	    activeFiltersSessions.get(sessionID).addActiveFilter(filterWidget);
	    libraryWidget.activateDisplayFilter(sessionID, defaultFilter.getFilterName(), false);
	}
    }

    public StationConfiguration getStationConfiguration() {
	return stationConfiguration;
    }

    public TimeKeeper getTimeKeeper() {
	return timekeeper;
    }
    
    public String newSearchSession() {
	if (libraryProvider.isSearchable() && searchSessions.size() < stationConfiguration.getIntParam(StationConfiguration.MAX_SEARCH_SESSIONS)) {
	    String sessionID = GUIDUtils.getGUID();
	    SearchSession searchSession = new SearchSession(stationConfiguration, timekeeper, connectionPool, libraryProvider, cacheProvider, sessionID);
	    ActiveFiltersModel activeFiltersModel = new ActiveFiltersModel(sessionID, libraryWidget, this);
	    if (searchSession.isActive()) {
		searchSessions.put(sessionID, searchSession);
		activeFiltersSessions.put(sessionID, activeFiltersModel);
		return sessionID;
	    } else {
		logger.warn("NewSearchSession: Session inactive.");
		return null;
	    }
	} else {
	    if (!libraryProvider.isSearchable())
		logger.error("NewSearchSession: State not LISTENING");
	    else
		logger.error("NewSearchSession: Exceeded max search Sessions: "+searchSessions.size());
	    return null;
	}
    }

    public void closeSession(String sessionID) {
	SearchSession searchSession = searchSessions.get(sessionID);
	if (searchSession != null) {
	    searchSession.close();
	    searchSessions.remove(sessionID);
	}
    }
    
    public Hashtable<String, SearchSession> getSearchSessions() {
	return searchSessions;
    }

    public SearchResult activateFilter(String sessionID, SearchFilter activeFilter) {
	if (!libraryProvider.isSearchable())
	    return null;
	
	SearchSession searchSession = searchSessions.get(sessionID);
	if (searchSession != null && searchSession.isActive()) {
	    logger.info("GRINS_USAGE:LIBRARY_ACTION:ACTIVATE_FILTER:" + activeFilter.getFilterName() + ":" + sessionID);
	    return searchSession.activateFilter(activeFilter);
	} else
	    return null;
    }

    public SearchResult deactivateFilter(String sessionID, SearchFilter inactiveFilter) {
	if (!libraryProvider.isSearchable())
	    return null;

	SearchSession searchSession = searchSessions.get(sessionID);
	if (searchSession != null && searchSession.isActive()) {
	    logger.info("GRINS_USAGE:LIBRARY_ACTION:DEACTIVATE_FILTER:" + inactiveFilter.getFilterName() + ":" + sessionID);
	    return searchSession.deactivateFilter(inactiveFilter);
	} else 
	    return null;
    }

    public SearchResult textSearch(String sessionID, String text) {
	if (!libraryProvider.isSearchable())
	    return null;

	SearchSession searchSession = searchSessions.get(sessionID);
	if (searchSession != null && searchSession.isActive())
	    return searchSession.textSearch(text);
	else
	    return null;
    }

    public SearchResult textSearch(String sessionID, int goBackN) {
	if (!libraryProvider.isSearchable())
	    return null;

	SearchSession searchSession = searchSessions.get(sessionID);
	if (searchSession != null && searchSession.isActive())
	    return searchSession.textSearch(goBackN);
	else
	    return null;
    }

    public boolean hasMoreItems(String moreItemsID) {
	return searchSessions.get(moreItemsID).hasMoreResults();
    }

    public void populateMoreItems(String moreItemsID) {
	SearchResult searchResult = searchSessions.get(moreItemsID).getMoreResults();
	addResults(searchResult, moreItemsID, searchSessions.get(moreItemsID).getSearchOffset());
    }

    public void addResults(SearchResult result, String sessionID, int searchOffset) {
	if (result == null)
	    return;
	LibraryWidget.LibraryTab libraryTab = libraryWidget.getLibraryTab(sessionID); 	
	RadioProgramMetadata[] data = result.getPrograms();
	if (data != null) {
	    RSMultiList tableList = libraryTab.getResultMultiList();
	    for (int i = searchOffset; i < data.length; i++){
		if (data[i] == null)
		    logger.error("AddResults: Null metadata found.");
		else {
		    tableList.add(data[i]);
		    logger.debug("AddResults: added: "+data[i]);
		}
	    }
	    libraryWidget.refreshResults(sessionID);
	}
    }

}
