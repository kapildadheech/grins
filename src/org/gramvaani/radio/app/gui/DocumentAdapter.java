package org.gramvaani.radio.app.gui;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public abstract class DocumentAdapter implements DocumentListener {
    public void changedUpdate(DocumentEvent e){
	documentUpdated(e);
    }

    public void insertUpdate(DocumentEvent e){
	documentUpdated(e);
    }

    public void removeUpdate(DocumentEvent e){
	documentUpdated(e);
    }

    public abstract void documentUpdated(DocumentEvent e);
}