package org.gramvaani.radio.app.gui;

import java.util.ArrayList;

import java.awt.datatransfer.DataFlavor;

public class SimulationItemSelection extends SelectionList {
    SimulationItemSelection(ArrayList<SimulationWidget.SimulationItemWidget> list){
	super(list);
    }

    public static DataFlavor getFlavor(){
	return SelectionList.getFlavor(SimulationItemSelection.class);
    }
}