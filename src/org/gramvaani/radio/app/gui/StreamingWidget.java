package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.rscontroller.services.StreamingService;
import org.gramvaani.radio.app.providers.StreamingProvider;
import org.gramvaani.radio.app.providers.StreamingListener;
import org.gramvaani.radio.rscontroller.messages.StreamingCommandMessage;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.RSApp;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;


import java.util.Hashtable;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;
import static org.gramvaani.utilities.IconUtilities.getIcon;

import com.jgoodies.forms.layout.*;
import com.jgoodies.forms.util.UnitConverter;
import com.jgoodies.forms.builder.DefaultFormBuilder;

import static org.gramvaani.radio.rscontroller.services.StreamingService.*;

public class StreamingWidget extends RSWidgetBase {
    RSApp rsApp;
    StationConfiguration stationConfig;
    Color bgColor, titleColor;

    ErrorInference errorInference;
    StreamingController streamingController;

    Dimension baseSize;
    JPanel basePanel;
    int rowHeight;

    static final String ERROR_STREAMING_NOT_CONFIGURED = "GRINS is not configured for streaming.";

    public StreamingWidget(RSApp rsApp){
	super("Network Streaming");
	this.rsApp = rsApp;

	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.STREAMING_ICON;
	bmpToolTip = "Network Streaming";
	
	stationConfig = rsApp.getStationConfiguration();
	cpJFrame = rsApp.getControlPanel().cpJFrame();

	errorInference = ErrorInference.getErrorInference();
    }

    public boolean onLaunch(){
	bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	titleColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);

	streamingController = new StreamingController(rsApp, this, stationConfig);

	buildWorkSpace();
	addValidators();
	initDefaults();
	activate(streamingController.isActive());
	return true;
    }

    JTextField nameField, descField, hostField, mountField, portField, usernameField;
    JPasswordField passwordField;
    JComboBox protocolBox, sampleRateBox, bitrateBox;
    JRadioButton mp3Button, oggVorbisButton, oggSpeexButton;
    JSlider qualitySlider;
    JCheckBox stereoCheckBox;
    JButton applySettings;
    JToggleButton playButton;
    JLabel statusLabel;

    ButtonGroup buttonGroup;

    static final String PROTOCOL_ICY = "ICY";
    static final String PROTOCOL_HTTP= "HTTP";
    Integer sampleRateList[] = new Integer[] {8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000};
    Integer speexSampleRateList[] = new Integer[] {8000, 16000, 32000};
    Integer bitrateList[] = new Integer[]{8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320};

    void buildWorkSpace(){
	wpComponent.setBackground(bgColor);
	wpComponent.setLayout(new BoxLayout(wpComponent, BoxLayout.PAGE_AXIS));
	
	RSTitleBar titleBar = new RSTitleBar(name, wpSize.width);
	titleBar.setBackground(titleColor);
	wpComponent.add(titleBar);

	basePanel = new JPanel();
	baseSize = new Dimension(wpSize.width, wpSize.height - RSTitleBar.HEIGHT);
	basePanel.setPreferredSize(baseSize);
	basePanel.setBackground(bgColor);
	wpComponent.add(basePanel);

	basePanel.setLayout(new BorderLayout());
	JPanel form = getSettingsPanel();
	basePanel.add(form, BorderLayout.CENTER);
    }

    Icon connectingIcon, streamingIcon, errorIcon;
    JPanel getSettingsPanel(){
	
	/*
	int heightRatios[][] = { 
	    {30, 20},
	    {30, 20, 20, 20},
	    {30, 20, 20, 20, 20},
	    {36, 30},
	    {36}
	};

	int pixelPerDluY = Sizes.dialogUnitYAsPixel(1, rsApp.getControlPanel().cpJFrame());
	
	int ratioSum = 0;
	for (int[] i: heightRatios)
	    for (int j: i)
		ratioSum += j;
	
	double ratioToPixel = baseSize.height/(ratioSum);
	*/

	FormLayout layout = new FormLayout("right:max(75dlu;p), 5dlu, 125dlu, 7dlu, "+
					   "right:max(75dlu;p), 5dlu, 125dlu",
					   "30dlu, 20dlu, " + //Stream
					   "30dlu, 20dlu, 20dlu, 20dlu,"+ //Connection
					   "30dlu, 20dlu, 20dlu, 20dlu, 20dlu,"+ //Format
					   "30dlu, 20dlu,"+ //Apply Settings
					   "p"); //Play controls
	
	DefaultFormBuilder builder = new DefaultFormBuilder(layout);
	builder.setDefaultDialogBorder();
	builder.setParagraphGapSize(Sizes.dluY(14));
	
	builder.appendSeparator("Stream Information");
	builder.append("Stream Name:", nameField = new JTextField());
	builder.append("Description:", descField = new JTextField());
	
	builder.appendSeparator("Connection Information");
	builder.append("Host IP:", hostField = new JTextField());
	builder.append("Mount Point:", mountField = new JTextField());
	builder.append("Port:", portField = new JTextField());
	builder.append("Username:", usernameField = new JTextField());
	builder.append("Protocol:", protocolBox = new JComboBox(new Object[] {PROTOCOL_HTTP, PROTOCOL_ICY}));
	builder.append("Password:", passwordField = new JPasswordField());
	builder.appendParagraphGapRow();
	builder.nextLine();
	
	builder.addSeparator("Format", 3);
	builder.nextColumn(3);
	builder.addSeparator("Codec", 4);
	builder.nextLine();

	builder.append("Sample Rate (Hz):", sampleRateBox = new JComboBox(sampleRateList));
	builder.append("MP3:", mp3Button = new JRadioButton());
	builder.append("Quality:", qualitySlider = new JSlider());
	builder.append("Ogg/Vorbis:", oggVorbisButton = new JRadioButton());
	builder.append("Stereo:", stereoCheckBox = new JCheckBox());
	builder.append("Ogg/Speex:", oggSpeexButton = new JRadioButton());
	builder.append(""); builder.append("");
	builder.append("Bitrate (kbps):", bitrateBox = new JComboBox(bitrateList));

	builder.append(""); builder.append(""); builder.append("");
	builder.append(applySettings = new JButton("Apply Settings"));
	builder.appendSeparator();

	builder.setAlignment(CellConstraints.FILL, CellConstraints.FILL);
	builder.append(playButton = new JToggleButton("Start"));
	builder.setAlignment(CellConstraints.FILL, CellConstraints.FILL);
	builder.append(statusLabel = new JLabel(""), 5);
	builder.setAlignment(CellConstraints.FILL, CellConstraints.FILL);

	ConstantSize size = (ConstantSize)layout.getRowSpec(13).getSize();
	rowHeight = size.getPixelSize(wpComponent);

	statusLabel.setHorizontalTextPosition(JLabel.LEADING);
	statusLabel.setHorizontalAlignment(SwingConstants.CENTER);
	statusLabel.setIconTextGap(rowHeight);
	clearStatus();

	connectingIcon = getIcon(StationConfiguration.CONNECTING_ICON, rowHeight, rowHeight);
	streamingIcon  = getIcon(StationConfiguration.STREAMING_ICON, rowHeight, rowHeight);
	errorIcon      = getIcon(StationConfiguration.CRITICAL_ICON, rowHeight, rowHeight);

	playButton.setIcon(getIcon(StationConfiguration.PLAY_ICON, rowHeight, rowHeight));
	
	playButton.addActionListener(new ActionListener(){
		public synchronized void actionPerformed(ActionEvent e){
		    if (playButton.isSelected()){
			if (stationConfig.isStreamingServiceActive()) {
			    streamingController.startStreaming(getStreamingConfig());
			    setButtonPlaying(true);
			} else {
			    streamError(ERROR_STREAMING_NOT_CONFIGURED);
			}
		    } else {
			streamingController.stopStreaming();
			setButtonPlaying(false);
		    }
		}
	    });

	if (!stationConfig.isStreamingServiceActive())
	    streamError(ERROR_STREAMING_NOT_CONFIGURED);

	buttonGroup = new ButtonGroup();
	buttonGroup.add(mp3Button);
	buttonGroup.add(oggVorbisButton);
	buttonGroup.add(oggSpeexButton);
	mp3Button.setActionCommand(CODEC_MP3);
	oggVorbisButton.setActionCommand(CODEC_OGV);
	oggSpeexButton.setActionCommand(CODEC_OGS);

	mp3Button.addActionListener(getSampleRateListener(sampleRateList));
	oggVorbisButton.addActionListener(getSampleRateListener(sampleRateList));
	oggSpeexButton.addActionListener(getSampleRateListener(speexSampleRateList));

	mp3Button.addActionListener(getEnableSliderListener(true));
	oggVorbisButton.addActionListener(getEnableSliderListener(false));
	oggSpeexButton.addActionListener(getEnableSliderListener(false));

	qualitySlider.setMinimum(0);
	qualitySlider.setMaximum(9);
	qualitySlider.setValue(5);
	Font sliderFont = qualitySlider.getFont();
	Font labelFont = sliderFont.deriveFont(sliderFont.getSize() * (3.0f/4.0f));
	Hashtable<Integer, JLabel> labels = new Hashtable<Integer, JLabel>();
	labels.put(0, new JLabel("lowest"));
	labels.put(9, new JLabel("highest"));
	for (JComponent c: labels.values())
	    c.setFont(labelFont);
	qualitySlider.setLabelTable(labels);
	qualitySlider.setPaintLabels(true);
	qualitySlider.setSnapToTicks(true);
	
	applySettings.addActionListener(new ActionListener(){
		public synchronized void actionPerformed(ActionEvent e){
		    if(stationConfig.isStreamingServiceActive())
			streamingController.updateStream(getStreamingConfig());
		    else
			streamError(ERROR_STREAMING_NOT_CONFIGURED);
		}
	    });

	JComponent[] components = new JComponent[] {protocolBox, sampleRateBox, qualitySlider, 
						    stereoCheckBox, mp3Button, oggVorbisButton,
						    oggSpeexButton, bitrateBox, applySettings,
						    playButton};
	for (JComponent c: components)
	    c.setBackground(bgColor);

	JPanel form = builder.getPanel();
	form.setBackground(bgColor);
	return form;
    }

    ActionListener getEnableSliderListener(final boolean enable){
	return new ActionListener(){
	    public void actionPerformed(ActionEvent e){
		qualitySlider.setEnabled(enable);
	    }
	};
    }

    ActionListener getSampleRateListener(final Integer[] list){
	ActionListener listener = new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    if (sampleRateBox.getItemCount() == list.length)
			return;
		    int oldValue = (Integer) sampleRateBox.getSelectedItem();
		    sampleRateBox.removeAllItems();
		    for (Integer i: list)
			sampleRateBox.addItem(i);
		    sampleRateBox.setSelectedItem(getClosestValue(oldValue, list));
		}
	    };
	return listener;
    }

    Integer getClosestValue(int value, Integer[] list){
	Integer closestValue = null;
	int minDifference = Integer.MAX_VALUE;

	for (Integer i: list){
	    if (Math.abs(i - value) < minDifference){
		closestValue = i;
		minDifference = Math.abs(i - value);
	    }
	}
	
	return closestValue;
    }

    void addValidators(){
	portField.setDocument(new PlainDocument(){
		public void insertString(int offs, String str, AttributeSet a){
		    str = str.trim();
		    String oldString = portField.getText();
		    String newString = oldString.substring(0, offs) + str + oldString.substring(offs);
		    try{
			int port = Integer.parseInt(newString);
			if (port >= 0 && port < 65536)
			    super.insertString(offs, str, a);
		    } catch (Exception e){}
		}
	    });
	
	mountField.setDocument(new PlainDocument(){
		public void insertString(int offs, String str, AttributeSet a){
		    str = str.replaceAll(" ", "");
		    str = str.replaceAll("#", "");
		    try {
			super.insertString(offs, str, a);
		    } catch (Exception e){}
		}
	    });
    }

    void initDefaults(){
	bitrateBox.setSelectedItem(new Integer(128));
	sampleRateBox.setSelectedItem(new Integer(32000));
	mp3Button.setSelected(true);
	mountField.setText(stationConfig.getStringParam(StationConfiguration.STATION_NAME));
	nameField.setText(stationConfig.getStringParam(StationConfiguration.STREAMING_NAME));
	hostField.setText(stationConfig.getStringParam(StationConfiguration.STREAMING_HOST));
	usernameField.setText(stationConfig.getStringParam(StationConfiguration.STREAMING_USER));
	portField.setText(stationConfig.getStringParam(StationConfiguration.STREAMING_PORT));	
	passwordField.setText(stationConfig.getStringParam(StationConfiguration.STREAMING_PASSWORD));
	
	descField.setText("GRINS stream");
    }

    void setButtonPlaying(boolean isPlaying){
	if (isPlaying){
	    playButton.setText("Stop");
	    playButton.setIcon(getIcon(StationConfiguration.STOP_ICON, rowHeight, rowHeight));
	    playButton.setSelected(true);
	} else {
	    playButton.setText("Start");
	    playButton.setIcon(getIcon(StationConfiguration.PLAY_ICON, rowHeight, rowHeight));
	    playButton.setSelected(false);
	}
    }

    public void clearStatus(){
	statusLabel.setText("Ready");
	statusLabel.setIcon(null);
    }

    JPanel getControlBar(){
	JPanel panel = new JPanel();
	panel.setLayout(new BorderLayout());
	panel.setBackground(bgColor);
	panel.add(new JButton("play"), BorderLayout.WEST);
	panel.add(new JLabel("status"),BorderLayout.CENTER);
	return panel;
    }

    public void onMaximize(){

    }

    public void onMinimize(){

    }

    public void onUnload(){
	wpComponent.removeAll();
	streamingController.stopStreaming();
	streamingController.unregister();
    }

    Hashtable<String,String> getStreamingConfig(){
	Hashtable<String, String> options = new Hashtable<String, String>();
	
	options.put(StreamingCommandMessage.OPT_NAME, nameField.getText());
	options.put(StreamingCommandMessage.OPT_DESC, descField.getText());
	options.put(StreamingCommandMessage.OPT_HOST, hostField.getText());
	options.put(StreamingCommandMessage.OPT_PORT, portField.getText());
	options.put(StreamingCommandMessage.OPT_PROTOCOL, (String) protocolBox.getSelectedItem());
	options.put(StreamingCommandMessage.OPT_MOUNTPOINT, mountField.getText());
	options.put(StreamingCommandMessage.OPT_USERNAME, usernameField.getText());
	options.put(StreamingCommandMessage.OPT_PASSWORD, new String(passwordField.getPassword()));
	options.put(StreamingCommandMessage.OPT_SAMPLERATE, ((Integer) sampleRateBox.getSelectedItem()).toString());
	options.put(StreamingCommandMessage.OPT_QUALITY, Integer.toString(qualitySlider.getValue()));
	options.put(StreamingCommandMessage.OPT_BITRATE, ((Integer) bitrateBox.getSelectedItem()).toString());
	options.put(StreamingCommandMessage.OPT_STEREO, Boolean.toString(stereoCheckBox.isSelected()));
	options.put(StreamingCommandMessage.OPT_CODEC, buttonGroup.getSelection().getActionCommand());

	return options;
    }

    public void activate(boolean activate){
	//playButton.setEnabled(activate);
	//applySettings.setEnabled(activate);
    }

    public void streamError(String error){
	statusLabel.setText(error);
	statusLabel.setIcon(errorIcon);
	setButtonPlaying(false);
    }

    public void streamingStarted(){
	statusLabel.setText("Streaming");
	statusLabel.setIcon(streamingIcon);
	setButtonPlaying(true);
    }

    public void streamConnecting(){
	statusLabel.setText("Connecting...");
	statusLabel.setIcon(connectingIcon);
    }
}

class StreamingController implements StreamingListener {

    StreamingProvider streamingProvider;
    boolean isStreamingProviderActive = false;
    StreamingWidget widget;
    RSApp rsApp;
    StationConfiguration stationConfig;

    StreamingController(RSApp rsApp, StreamingWidget widget, StationConfiguration stationConfig){
	this.widget = widget;
	this.rsApp = rsApp;
	this.stationConfig = stationConfig;

	streamingProvider = (StreamingProvider) rsApp.getProvider(RSApp.STREAMING_PROVIDER);
	streamingProvider.registerWithProvider(this);
    }
    
    public boolean isActive(){
	return streamingProvider.isActive();
    }

    public void startStreaming(Hashtable<String, String> config){
	widget.streamConnecting();
	streamingProvider.startStreaming(config);
    }

    public void updateStream(Hashtable<String, String> config){
	streamingProvider.updateStream(config);
    }

    public void stopStreaming(){
	streamingProvider.stopStreaming();
    }

    public void unregister(){
	streamingProvider.unregisterWithProvider(widget.getName());
    }

    /* Streaming Listener API */
    public void activateStreaming(boolean activate, String[] error){
	widget.activate(activate);
	if (activate)
	    widget.clearStatus();
	else
	    widget.streamError("Connection lost to Streaming Service. Check network.");
    }

    public void streamingGstError(String error){
	if (error.equals(StreamingService.STREAMING_ERROR_CONNECTION)){
	    widget.streamError("Could not connect to server. Please check your network.");
	} else if (error.equals(StreamingService.STREAMING_ERROR_LOGIN)){
	    widget.streamError("Unable to login to server. Please check username/password.");
	} else if (error.equals(StreamingService.STREAMING_ERROR_SOCKET)){
	    widget.streamError("Error occured in talking to Server.");
	} else if (error.equals(StreamingService.STREAMING_ERROR_RECONNECTION)){
	    widget.streamError("Error occured while trying to reconnect.");
	} else {
	    widget.streamError("Unknown error.");
	}
    }

    public void streamingStarted(boolean success, String[] error){
	if (success)
	    widget.streamingStarted();
	else
	    widget.streamError("Unknown error.");
    }

    public void streamingStopped(boolean success, String[] error){
	if (success)
	    widget.clearStatus();
    }

    public String getName(){
	return widget.getName();
    }
    /* End of Streaming Listener */

    StreamingConfiguration newConfig(){
	return new StreamingConfiguration();
    }

    public class StreamingConfiguration {
	public String name, desc, host, port, protocol, mountpoint, username, password;
	public int sampleRate, quality, bitrate;
	public boolean stereo;
	public String codec;
    }

}