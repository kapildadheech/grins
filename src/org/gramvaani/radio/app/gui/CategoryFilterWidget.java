package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class CategoryFilterWidget extends ActiveFilterWidget implements TreeSelectionListener {
    //MediaLib medialib;

    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;

    JTree categoryTree;
    DefaultMutableTreeNode topNode;
    int treeHeight = 0;
    Category lastSelectedNode = null;
    StationConfiguration stationConfig;

    public CategoryFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
				LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	//medialib = MediaLib.getMediaLib(libraryController.getStationConfiguration(), libraryController.getTimeKeeper());
	this.stationConfig = libraryController.getStationConfiguration();
	init();
    }

    protected void init() {
	initTree();
	int paneHeight = (treeHeight > 8 ? 8 : treeHeight) + 1;

	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();

	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, filterVertSize * paneHeight + borderHeight));
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());
	add(titleLabel, getGbc(0, 0, weightx(100.0), gfillboth()));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));
	add(closeButton, getGbc(1, 0, gfillboth()));

	categoryTree = new JTree(topNode);
	JScrollPane treePane = new JScrollPane(categoryTree, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, 
					       ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	treePane.setPreferredSize(new Dimension((int)(libraryWidget.getFilterHorizSize() * (tlHorizFrac + cbHorizFrac) / 100.0), 
						   (libraryWidget.getFilterVertSize()) * (paneHeight - 1)));
	categoryTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	categoryTree.addTreeSelectionListener(this);

	add(treePane, getGbc(0, 1, gridwidth(2), weightx(100.0), gfillboth()));

	Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	setBackground(bgColor);
	titleLabel.setBackground(bgColor);
	closeButton.setBackground(bgColor);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	treePane.setBackground(bgColor);

	closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON,
						  filterVertSize, filterVertSize));
    }

    private void initTree() {
	Category parentNode = ((CategoryFilter)filter).getParentNode();
	topNode = new DefaultMutableTreeNode(parentNode);
	treeHeight++;
	browseTree(parentNode, topNode);
    }

    private void browseTree(Category parentCategoryNode, DefaultMutableTreeNode parentTreeNode) {
	if(parentCategoryNode.getChildren().length > 0) {
	    treeHeight++;
	    for(Category child: parentCategoryNode.getChildren()) {
		DefaultMutableTreeNode childTreeNode;
		parentTreeNode.add(childTreeNode = new DefaultMutableTreeNode(child));
		browseTree(child, childTreeNode);
	    }
	}
    }

    public void valueChanged(TreeSelectionEvent e) {
	DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode)(categoryTree.getLastSelectedPathComponent());
	if(treeNode == null)
	    return;
	Category selectedNode = (Category)(treeNode.getUserObject());
	if(lastSelectedNode != null) {
	    // do nothing
	}
	((CategoryFilter)filter).setSelectedNode(selectedNode);
	((CategoryFilter)filter).setAllChildren(selectedNode.getAllChildren());
	((CategoryFilter)filter).activateAnchor(CategoryFilter.CATEGORY_ANCHOR + "_" + selectedNode.getNodeName(), null);
	SearchResult result = libraryController.activateFilter(sessionID, filter);
	libraryController.updateResults(result, sessionID);
	lastSelectedNode = selectedNode;
    }

    public void setEnabled(boolean activate) {
	super.setEnabled(activate);
	categoryTree.setEnabled(activate);
    }
    
    public void update(){
	//init();
    }
}