package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class PlaylistPopup extends JPopupMenu implements ListCellRenderer {
    PlaylistModel listModel;
    JList list;
    RSPanel panel = new RSPanel();
    JLabel label = new JLabel();
    final int ICON_SIZE = 20;
    Dimension iconSize = new Dimension(ICON_SIZE, ICON_SIZE);
    
    JLabel iconLabel = new JLabel();
    Icon removeIcon = IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, ICON_SIZE, ICON_SIZE);
    
    Color bgColor, selectedColor, removeColor;
    boolean isRemove = false;
    
    final String detailsColorString;
    
    final PlaylistListener listener;
    final MetadataProvider metadataProvider;
    final StationConfiguration stationConfig;

    javax.swing.Timer infoLoadTimer;
    static final int INFO_LOAD_DELAY_MS = 350;
    static final int NUM_LISTS_VISIBLE = 10;
    
    static ArrayList<PlaylistPopup> instances = new ArrayList<PlaylistPopup>();

    PlaylistPopup(final PlaylistListener listener, MetadataProvider metadataProvider, 
		  StationConfiguration stationConfig, int width){

	this.listener = listener;
	this.metadataProvider = metadataProvider;
	this.stationConfig = stationConfig;

	listModel = new PlaylistModel(metadataProvider, NUM_LISTS_VISIBLE);
	list = new JList(listModel);

	JScrollPane scrollPane = new JScrollPane(list);
	add(scrollPane);

	infoLoadTimer = new javax.swing.Timer(INFO_LOAD_DELAY_MS, new ActionListener(){
		public synchronized void actionPerformed(ActionEvent e){
		    int minIndex = list.getFirstVisibleIndex();

		    if (minIndex >= listModel.getSize())
			minIndex = listModel.getSize() - 1;
		    if (minIndex < 0)
			minIndex = 0;

		    int maxIndex = minIndex + NUM_LISTS_VISIBLE;

		    if (maxIndex >= listModel.getSize())
			maxIndex = listModel.getSize() - 1;

		    listModel.loadInfoBetween(minIndex, maxIndex);
		    infoLoadTimer.stop();
		}
	    });

	scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener(){
		public void adjustmentValueChanged(AdjustmentEvent e){
		    infoLoadTimer.restart();
		}
	    });

	list.addMouseListener(new MouseAdapter(){
		public synchronized void mouseClicked(MouseEvent e){
		    int index = list.locationToIndex(e.getPoint());
		    
		    if (index < 0)
			return;
		    
		    int lastElement = list.getModel().getSize() - 1;
		    int cellHeight = list.getCellBounds(0, lastElement).height;
		    
		    if (e.getPoint().getY() > cellHeight)
			return;
		    
		    if (!(listModel.get(index) instanceof Playlist))
			return;

		    Playlist playlist = (Playlist)listModel.get(index);
		    
		    if (e.getX() > list.getSize().width - ICON_SIZE){
			removePlaylist(playlist);
		    } else {
			listener.loadPlaylist(playlist);
		    }
		    
		    setVisible(false);
		}
		
	    });
	list.addMouseMotionListener(new MouseMotionAdapter(){
		public synchronized void mouseMoved(MouseEvent e){
		    int index = list.locationToIndex(e.getPoint());
		    boolean oldStatus = isRemove;
		    if (e.getX() > list.getSize().width - ICON_SIZE){
			isRemove = true;
			list.setToolTipText("Delete Playlist");
		    } else {
			isRemove = false;
			list.setToolTipText("Load Playlist");
		    }
		    list.setSelectedIndex(index);
		    if (oldStatus != isRemove){
			list.repaint();
		    }
		}
		
	    });
	
	list.setBackground(bgColor = stationConfig.getColor(StationConfiguration.LOADSAVE_POPUP_COLOR));
	selectedColor = bgColor.brighter();
	removeColor = stationConfig.getColor(StationConfiguration.ERROR_FIELD_COLOR);
	
	panel.setOpaque(false);
	panel.setLayout(new GridBagLayout());
	panel.add(label, getGbc(0, 0, gfillboth(), weightx(1.0)));
	iconLabel.setMinimumSize(iconSize);
	iconLabel.setPreferredSize(iconSize);
	panel.add(iconLabel, getGbc(1, 0, new Insets(0, 2*ICON_SIZE, 0, 0)));
	panel.setBackground(selectedColor);

	detailsColorString = GUIUtilities.colorToHtmlString(Color.lightGray);

	list.setCellRenderer(this);

	list.setVisibleRowCount(NUM_LISTS_VISIBLE);
	list.setPrototypeCellValue("dummy");
	list.setFixedCellWidth(width);

	instances.add(this);
	infoLoadTimer.start();
    }

    public void loadPlaylists() {
	listModel.loadLists();
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus){
	iconLabel.setIcon(null);

	if (value instanceof Playlist){
	    Playlist playlist = (Playlist) value;
	    int id = playlist.getPlaylistID();

	    if (listModel.isInfoLoaded(playlist)){
		label.setText("<html>"+playlist.getName() 
			      + " <font color="+detailsColorString+">("+listModel.getNumFiles(playlist)+"), <i>" 
			      + StringUtilities.durationStringFromMillis(listModel.getDuration(playlist)) + "</i></font></html>");
	    } else {
		label.setText(playlist.getName());
	    }

	    panel.setOpaque(isSelected);
	    if (isRemove)
		panel.setBackground(removeColor);
	    else
		panel.setBackground(selectedColor);
	    
	    if (isSelected)
		iconLabel.setIcon(removeIcon);

	} else {
	    label.setText((String) value);
	}
	
	return panel;
    }

    protected void removePlaylist(Playlist playlist){
	String title = "Delete Playlist";
	String message = "Are you sure you want to delete playlist: "+playlist.getName();
	int retVal = JOptionPane.showConfirmDialog(ControlPanelJFrame.getInstance(), message, title, JOptionPane.YES_NO_OPTION);
	if (retVal != JOptionPane.YES_OPTION)
	    return;

	for (PlaylistPopup p: instances)
	    p._remove(playlist);
    }
    
    protected void _remove(Playlist playlist){
	listModel.remove(playlist);
    }

    public void playlistUpdated(int playlistID){
	for (PlaylistPopup p: instances)
	    p._playlistUpdated(playlistID);
    }

    protected void _playlistUpdated(int playlistID){
	listModel.updatePlaylist(playlistID);
    }

    public void playlistAdded(Playlist playlist){
	for (PlaylistPopup p: instances)
	    p._playlistAdded(playlist);
    }

    protected void _playlistAdded(Playlist playlist){
	listModel.addPlaylist(playlist);
    }

    public void refreshPlaylists(String[] playlistIDs) {
	for (PlaylistPopup p: instances)
	    p._refreshPlaylists(playlistIDs);
    }

    public void reload(){
	for (PlaylistPopup p: instances)
	    p._reload();
    }

    public void _refreshPlaylists(String[] playlistIDs) {
	listModel.refreshPlaylists(playlistIDs);
	infoLoadTimer.restart();
    }

    protected void _reload(){
	listModel.reload();
	infoLoadTimer.restart();
    }

    public interface PlaylistListener {
	void loadPlaylist(Playlist playlist);
    }
}

class PlaylistModel extends DefaultListModel {

    Hashtable<Integer, Boolean> infoLoadedFlag = new Hashtable<Integer, Boolean>();
    Hashtable<Integer, Integer> numFiles = new Hashtable<Integer, Integer>();
    Hashtable<Integer, Long> totalDuration = new Hashtable<Integer, Long>();
    
    MetadataProvider metadataProvider;
    int numVisibleLists;
    static final String NO_PLAYLISTS = "No playlists available.";


    PriorityBlockingQueue<InfoRequest> queue = new PriorityBlockingQueue<InfoRequest>();

    public PlaylistModel(MetadataProvider metadataProvider, int numVisibleLists){
	this.metadataProvider = metadataProvider;
	this.numVisibleLists = numVisibleLists;

	loadLists();
	watchForLoadInfo();
    }

    public void loadLists(){
	clear();
	ArrayList<Playlist> array = new ArrayList<Playlist>();
	for (Playlist playlist: metadataProvider.getPlaylists()){
	    array.add(playlist);
	}

	if (array.size() > 0){
	    Collections.sort(array);
	    for (Playlist playlist: array){
		addElement(playlist);
		infoLoadedFlag.put(playlist.getPlaylistID(), false);
	    }
	} else {
	    addElement(NO_PLAYLISTS);
	}

    }

    public void reload(){
	for (Integer i: infoLoadedFlag.keySet()){
	    infoLoadedFlag.put(i, false);
	}
    }

    public void refreshPlaylists(String[] playlistIDs) {
	for (String id: playlistIDs)
	    infoLoadedFlag.put(new Integer(id), false);
    }

    void watchForLoadInfo(){
	Thread t = new Thread("LoadInfoWorker"){
		public void run(){
		    while (true){
			InfoRequest request = null;
			while (request == null){
			    try{
				request = queue.take();
			    } catch (InterruptedException e){
				
			    }
			}
			loadInfoFromMinIndex(request.getMinIndex(), request.getMaxIndex());
		    }
		}
	    };
	t.start();
    }

    public void loadInfoBetween(int minIndex, int maxIndex){
	queue.put(new InfoRequest(minIndex, maxIndex));
    }

    void loadInfoFromMinIndex(int minIndex, int maxIndex){
	if (!playlistsAvailable())
	    return;

	for (int i = minIndex; i <= maxIndex; i++){
	    if(i >= getSize())
		break;

	    Playlist playlist = (Playlist) getElementAt(i);

	    if (isInfoLoaded(playlist))
		continue;
	    
	    int id = playlist.getPlaylistID();
	    PlaylistItem items[] = metadataProvider.get(new PlaylistItem(id));
	    numFiles.put(id, items.length);
	    
	    long length = 0;
	    RadioProgramMetadata dummy = new RadioProgramMetadata("");
	    
	    for (PlaylistItem item: items){
		if (item.getLiveItem()){
		    length += item.getLiveDuration();
		} else {
		    dummy.setItemID(item.getItemID());
		    RadioProgramMetadata program = metadataProvider.getSingle(dummy);
		    if (program != null)
			length += program.getLength();
		}
	    }
	    
	    if (length < 0)
		length = 0L;
	    
	    totalDuration.put(id, length);
	    
	    infoLoadedFlag.put(playlist.getPlaylistID(), true);
	    final int index = i;
	    
	    SwingUtilities.invokeLater(new Runnable(){
		    public void run(){
			fireContentsChanged(this, index, index);
		    }
		});
	}
    }

    public void addPlaylist(final Playlist playlist){
	int i = 0;
	if (!playlistsAvailable())
	    removeElementAt(0);

	for (i = 0; i < getSize(); i++){
	    Playlist list = (Playlist) getElementAt(i);
	    if (list.getName().compareTo(playlist.getName()) > 0)
		break;
	}

	infoLoadedFlag.put(playlist.getPlaylistID(), false);
	//loadInfoBetween(i, i);
	final int index = i;
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    add(index, playlist);
		    loadInfoBetween(index, index);
		}
	    });
    }

    public void updatePlaylist(int playlistID){
	infoLoadedFlag.put(playlistID, false);
	int index = 0;
	for (int i = 0; i < getSize(); i++){
	    if (getElementAt(i) instanceof String)
		continue;
	    Playlist list = (Playlist) getElementAt(i);
	    if (list.getPlaylistID() == playlistID){
		list.clear__playlistItems();
		index = i;
		break;
	    }
	}
	loadInfoBetween(index, index);
    }

    public boolean isInfoLoaded(Playlist playlist){
	return infoLoadedFlag.get(playlist.getPlaylistID());
    }

    public int getNumFiles(Playlist playlist){
	return numFiles.get(playlist.getPlaylistID());
    }

    public long getDuration(Playlist playlist){
	return totalDuration.get(playlist.getPlaylistID());
    }

    boolean playlistsAvailable(){
	if (getSize() == 0)
	    return false;
	else
	    return (getElementAt(0) instanceof Playlist);
    }

    public void remove(Playlist playlist){
	metadataProvider.remove(new PlaylistItem(playlist.getPlaylistID()));
	metadataProvider.remove(playlist);

	if (playlistsAvailable()){

	    int index = -1;
	    for (int i = 0; i < getSize(); i++){
		Playlist tmp = (Playlist) getElementAt(i);
		if (tmp.getPlaylistID() == playlist.getPlaylistID()){
		    index = i;
		    break;
		}
	    }
	    
	    if (index > -1)
		remove(index);

	    if (isEmpty()) {
		addElement(NO_PLAYLISTS);
	    }
	}
    }

    class InfoRequest implements Comparable<InfoRequest>{
	int minIndex, maxIndex;
	long time;
	
	InfoRequest(int minIndex, int maxIndex){
	    this.minIndex = minIndex;
	    this.maxIndex = maxIndex;
	    time = System.currentTimeMillis();
	}

	public long getTime(){
	    return time;
	}

	public int getMinIndex(){
	    return minIndex;
	}

	public int getMaxIndex(){
	    return maxIndex;
	}

	public int compareTo(InfoRequest r){
	    return (int)(time - r.getTime());
	}
    }

}