package org.gramvaani.radio.app.gui;

public interface ErrorDisplayListener {

    public String getName();
    public void displayError(String error);

}