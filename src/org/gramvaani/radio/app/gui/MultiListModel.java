package org.gramvaani.radio.app.gui;

public interface MultiListModel {

    public void clear();
    public void add(Object obj);
    public void add(int index, Object obj);
    public void removeFromMultiList(int index);

}