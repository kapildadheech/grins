package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.pipeline.RSPipeline;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;


import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Collections;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;


public class PlaylistController implements ActionListener, PlayoutPreviewListenerCallback, 
					   ArchiverListenerCallback, MetadataListenerCallback,
					   CacheListenerCallback, MicListenerCallback, VolumeDisplay.VolumeChangeListener,
					   PlaylistPopup.PlaylistListener {

    public final static String PLAYLIST_CONTROLLER = "PLAYLIST_CONTROLLER";

    public final static String UNABLE_TO_SEND_COMMAND  = "Unable to send command";
    public final static String PROGRAM_NOT_IN_DATABASE = "Program not in database";
    public final static String UNABLE_TO_PLAY = "Unable to play";
    public final static String UNABLE_TO_PAUSE= "Unable to pause";
    public final static String UNABLE_TO_STOP = "Unable to stop";
    public final static String UNABLE_TO_SAVE = "Unable to save";
    public final static String UNKNOWN_ERROR  = "Unknown error";
    public final static String DATABASE_ERROR = "Database error";

    public static long RESTART_WAIT_TIME = 120000;  

    PlaylistWidget playlistWidget;
    RSApp rsApp;
    ControlPanel controlPanel;
    PlayoutProvider playoutProvider;
    PreviewProvider previewProvider;
    ArchiverProvider archiverProvider;
    MetadataProvider metadataProvider;
    CacheProvider cacheProvider;
    MonitorProvider monitorProvider;
    MicProvider micProvider;
    LogUtilities logger;

    GUIPlaylist guiPlaylist;
    boolean playoutProviderActive, previewProviderActive;
    boolean archiverProviderActive;
    boolean metadataProviderActive;
    boolean cacheProviderActive;
    boolean micProviderActive;

    boolean liveActive;
    boolean playingActive, pauseActive;
    boolean previewActive; String previewFilename;
    boolean goliveActive;
    boolean realGoliveActive;
    boolean micEnabled;
    boolean previewUIEnabled;
    boolean playoutUIEnabled;
    boolean archiverUIEnabled;
    long playStartTime, remainingTime, currentElementDuration;
    boolean micServiceActive = false;
    int seekPosition;

    TimeKeeper timekeeper;
    Timer startTimekeeper, countdownTimekeeper;
    StationConfiguration stationConfig;

    SavePlaylistPopup savePlaylistPopup = null;
    StartTimePlaylistPopup startTimePlaylistPopup = null;
    
    GoliveItemPopup goliveItemPopup;

    ErrorInference errorInference;
    PlaylistPopup listPopup;

    static final long TZ_OFFSET;

    static {
	Calendar calendar = GregorianCalendar.getInstance();
	TZ_OFFSET = calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET);
    }

    public PlaylistController(PlaylistWidget playlistWidget, ControlPanel controlPanel, RSApp rsApp) {
	this.playlistWidget = playlistWidget;
	this.controlPanel = controlPanel;
	this.rsApp = rsApp;
	
	guiPlaylist = new GUIPlaylist(this);
	playoutProviderActive = previewProviderActive = false;
	liveActive = false;
	playingActive = false; pauseActive = false;
	previewActive = false; previewFilename = "";
	micEnabled = false;
	
	playoutUIEnabled = false; 
	previewUIEnabled = false;
	archiverUIEnabled = false;
	archiverProviderActive = false;
	metadataProviderActive = false;
	cacheProviderActive = false;

	playlistWidget.setGUIPlaylist(guiPlaylist);

	timekeeper = rsApp.getUIService().getTimeKeeper();

	// default time -- midnight today
	playStartTime = defaultStartTime();

	stationConfig = rsApp.getStationConfiguration();

	errorInference = ErrorInference.getErrorInference();
	micServiceActive = stationConfig.isMicServiceActive();
	logger = new LogUtilities("PlaylistController");
    }
    
    public void init() {
	this.playoutProvider = (PlayoutProvider)(rsApp.getProvider(RSApp.PLAYOUT_PROVIDER));
	this.previewProvider = (PreviewProvider)(rsApp.getProvider(RSApp.PREVIEW_PROVIDER));
	this.metadataProvider = (MetadataProvider)(rsApp.getProvider(RSApp.METADATA_PROVIDER));
	this.cacheProvider = (CacheProvider)(rsApp.getProvider(RSApp.CACHE_PROVIDER));
	this.monitorProvider = (MonitorProvider)(rsApp.getProvider(RSApp.MONITOR_PROVIDER));
	this.archiverProvider = (ArchiverProvider)(rsApp.getProvider(RSApp.ARCHIVER_PROVIDER));
	this.micProvider   = (MicProvider) rsApp.getProvider(RSApp.MIC_PROVIDER);

	playoutProvider.registerWithProvider(this);
	previewProvider.registerWithProvider(this);
	metadataProvider.registerWithProvider(this);
	cacheProvider.registerWithProvider(this);
	archiverProvider.registerWithProvider(this);
	micProvider.registerWithProvider(this);

	if (playoutProvider == null || !playoutProvider.isActive()) {
	    playoutProviderActive = false;
	    playoutUIEnabled = false;
	}
	else {
	    playoutProviderActive = true;
	    playoutUIEnabled = true;
	}
	if (previewProvider == null || !previewProvider.isActive()) {
	    previewProviderActive = false;
	    previewUIEnabled = false;
	}
	else {
	    previewProviderActive = true;
	    previewUIEnabled = true;
	}
	if (metadataProvider == null || !metadataProvider.isActive())
	    metadataProviderActive = false;
	else
	    metadataProviderActive = true;
	if (cacheProvider == null || !cacheProvider.isActive())
	    cacheProviderActive = false;
	else {
	    cacheProviderActive = true;
	    guiPlaylist.setCacheProvider(cacheProvider);
	    metadataProvider.setCacheProvider(cacheProvider);
	}
	if (archiverProvider == null || !archiverProvider.isActive()) {
	    archiverUIEnabled = false;
	    archiverProviderActive = false;
	} else {
	    archiverUIEnabled = true;
	    archiverProviderActive = true;
	}

	if (micServiceActive) {
	    if (micProvider == null || !micProvider.isActive()) {
		micProviderActive = false;
		archiverUIEnabled = false;
	    } else {
		micProviderActive = true;
	    }
	} else {
	    micProviderActive = false;
	}

	updateGUIButtons();
	createPlaylistPopup();
    }

    protected long defaultStartTime() {
	return (long)(((timekeeper.getLocalTime() / 1000 / 60 / 60 / 24) + 1.25 ) * 24 * 60 * 60 * (long)1000 - TZ_OFFSET);
    }

    PlaylistWidget.PlayItemWidget playItemWidget;

    void stopGoLive() {
	if (archiverProvider.isStoppable(PLAYLIST_CONTROLLER)) {
	    if (archiverProvider.stopArchive(ArchiverService.PLAYOUT, true) < 0){
		errorInference.displayPlaylistError("Unable to send stop-record command", new String[]{UNABLE_TO_SEND_COMMAND});
		logger.error("ActionPerformed: GOLIVE archive: Could not stop archiving.");
	    } else {
		goliveActive = false;
	    }
	} else {
	    errorInference.displayPlaylistError("Unable to send stop-record command", new String[]{UNABLE_TO_SEND_COMMAND});
	    logger.error("ActionPerformed: GOLIVE archive: ArchiverProvider is not stoppable.");
	}
	
	if (micServiceActive && micProvider != null && micProvider.isActive() && micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE) == 0){
	    goliveActive = false;
	    micEnabled = false;
	} else if (micServiceActive) {
	    errorInference.displayPlaylistError("Unable to send stop-mic command", new String[]{UNABLE_TO_SEND_COMMAND});
	    logger.error("ActionPerformed: GOLIVE mic: Could not stop mic.");
	    goliveActive = false;
	    micEnabled = false;
	}
	
	String programID = guiPlaylist.firstProgram();
	PlaylistWidget.PlayItemWidget playItemWidget = guiPlaylist.getPlayItemWidget(programID);

	String id = (guiPlaylist.firstProgram() == null ? PlaylistWidget.GOLIVE_ITEMID : guiPlaylist.firstProgram());
	logger.info("GRINS_USAGE:STOP_GOLIVE:"+id);

	if (playingActive) {
	    if (programID != null && playoutProvider != null && playoutProvider.isActive())
		playoutProvider.setGoLiveActive(programID, false, this);
	    playlistWidget.activatePlay(true);
	} else {
	    realGoliveActive = false;
	    if (playItemWidget != null && playItemWidget.isLiveItem() && countdownTimekeeper != null)
		countdownTimekeeper.stop();
	}
    }

    public synchronized boolean startGoLive() {
	micEnabled = false;
	boolean archiverStatus = false;
	
	if (micServiceActive && micProvider != null && micProvider.isActive() && archiverProvider.isArchivable()){
	    micEnabled = (micProvider.enable(PLAYLIST_CONTROLLER, MicProvider.PLAYOUT_PORT_ROLE) == 0);
	    if (micEnabled) {
		archiverStatus = archiverProvider.startArchive(ArchiverService.PLAYOUT, PLAYLIST_CONTROLLER, true) == 0;
	    }
	    
	} else if (!micServiceActive && archiverProvider.isArchivable()){
	    archiverStatus = archiverProvider.startArchive(ArchiverService.PLAYOUT, PLAYLIST_CONTROLLER, true) == 0;
	}
	
	if (archiverStatus) {
	    goliveActive = true;
	    String programID = (guiPlaylist.firstProgram() == null ? PlaylistWidget.GOLIVE_ITEMID : guiPlaylist.firstProgram());
	    logger.info("GRINS_USAGE:START_GOLIVE:"+programID);
	    
	    if (!playingActive && !pauseActive && micEnabled) {
		//initCountdownTimer(programID);
	    } else if ((playingActive || pauseActive)) { // && micEnabled) {
		playoutProvider.setGoLiveActive(programID, true, this);
	    }
	    logger.info("ActionPerformed: GOLIVE: Started archiving.");
	} else {
	    errorInference.displayPlaylistError("Unable to send start-record command", new String[]{UNABLE_TO_SEND_COMMAND});
	    playlistWidget.selectedArchiverButton(false);
	    logger.error("ActionPerformed: GOLIVE: Failed to start archiving. MicEnabled: " + micEnabled + " Archiver: " + archiverStatus);
	}

	return archiverStatus;
    }

    private void handleGRINSRestartItem() {
	Thread t = new Thread() {
		public void run() {
		    String programID = guiPlaylist.firstProgram();
		    RadioProgramMetadata removedProgram = removeFromPlaylistScroll(programID);
		    playlistWidget.activateStop();
		    guiPlaylist.setFirstProgram(removedProgram);
		    rsApp.getUIService().restartMachines();
		    try {
			Thread.sleep(RESTART_WAIT_TIME);
		    } catch(Exception e){}

		    if (guiPlaylist.size() == 1) {
			guiPlaylist.removeFirstProgram();
			playingActive = false;
		    } else {
			advancePlaylist();
		    }
		    updateLivePane();
		    updateGUIButtons();
		}
	    };
	
	t.start();
    }
    
    public synchronized void actionPerformed(ActionEvent e) {
	String actionCommand = e.getActionCommand();
	if (actionCommand.equals(PlaylistWidget.GOLIVE)) {

	    if (goliveActive) { // archiving in progress. now send stop-archive command
		stopGoLive();
	    } else { // send start-archive command
		startGoLive();
	    }
	    updateGUIButtons();
	    
	} else if (actionCommand.equals(PlaylistWidget.PLAY)) {

	    if (playoutProviderActive && playoutProvider.isPlayable()) {
		String programID = guiPlaylist.firstProgram();

		PlaylistWidget.PlayItemWidget playItemWidget = guiPlaylist.getPlayItemWidget(programID);
		
		if (/*micServiceActive &&*/ !goliveActive && !realGoliveActive && 
		   //(programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
		   // bala 15 dec
		   ( playItemWidget.isLiveItem())) {
		    //startMic();
		    if (!pauseActive) {
			initCountdownTimer(programID);
		    }
		}

		if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID)) {
		    playlistWidget.activatePlay(false);
		    return;
		}

		if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) ||
		   playItemWidget.isLiveItem() && !playItemWidget.playBackgroundMusic()) {

		    realGoliveActive = true;
		    playingActive = false;
		    playlistWidget.activatePlay(false);
		    if (pauseActive) {
			countdownTimekeeper.restart();
			pauseActive = false;
		    } else {
			initCountdownTimer(programID);
			//			playlistWidget.activatePlay(true);
		    }
		    updateGUIButtons();
		    return;
		}
		if (previewActive && previewFilename.equals(programID)) {
		    // XXX there is a race condition here. need to test for previewProvider.isStoppable. 
		    // if not stoppable right now, then ideally we should wait
		    //		    if (!previewProvider.isStoppable(programID, this)) {
		    //			try { Thread.sleep(200); } catch(InterruptedException exp) { }
		    //		    }
		    if (previewProvider.stop(previewFilename, this) < 0) {
			errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
			guiPlaylist.unsetPreviewIndex();
		    }
		    previewActive = false;
		    previewFilename = "";
		}

		boolean isGoLiveBackground = (goliveActive || playItemWidget.isLiveItem() && playItemWidget.playBackgroundMusic());// && micServiceActive;
		
		// FADE duration passed here
		int retVal = -1;
		//playoutProvider.setGoLiveActive(programID, false, this);
		if (pauseActive)
		    retVal = playoutProvider.replay(programID, playItemWidget.getFadeoutStartTime(), isGoLiveBackground, this);
		else
		    retVal = playoutProvider.play(programID, seekPosition, playItemWidget.getFadeoutStartTime(), isGoLiveBackground, this);
		if (retVal < 0) {
		    errorInference.displayPlaylistError("Unable to send playout-start command", new String[]{UNABLE_TO_SEND_COMMAND});

		    if (/*micServiceActive &&*/ goliveActive && (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
			stopGolive();
		    }
		    advancePlaylist();
		} else {
		    playingActive = true;
		    if (pauseActive) {
			countdownTimekeeper.restart();
		    } else {
			initCountdownTimer(programID);
		    }
		}

		restartTimer = !pauseActive;

		if (playingActive) {
		    pauseActive = false;
		    realGoliveActive = false;
		}

		playlistWidget.setStartTime(timekeeper.getLocalTime(), remainingTime);

		updateGUIButtons();
		
		//guiPlaylist.dump();
	    }

	} else if (actionCommand.equals(PlaylistWidget.PAUSE)) {
	    String programID = guiPlaylist.firstProgram();
	    
	    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID)) {
		playlistWidget.activatePause(false);
		return;
	    }
	    
	    if (realGoliveActive) {
		realGoliveActive = false;
		playlistWidget.activatePause(true);
	    }

	    if (playoutProviderActive && playoutProvider.isPausable()) {

		if (playoutProvider.pause(programID, this) < 0)
		    errorInference.displayPlaylistError("Unable to send playout-pause command", new String[]{UNABLE_TO_SEND_COMMAND});
		else {
		    pauseActive = true;
		    //if (!micServiceActive || !goliveActive)
		    countdownTimekeeper.stop();
		}
		if (pauseActive)
		    playingActive = false;
		updateGUIButtons();
	    }

	} else if (actionCommand.equals(PlaylistWidget.STOP)) {

	    String programID = guiPlaylist.firstProgram();
	    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID)){
		countdownTimekeeper.stop();
		playlistWidget.activateStop();
		playingActive = false;
		pauseActive = false;
		goliveActive = false;
		realGoliveActive = false;
		return;
	    }

	    if (realGoliveActive || pauseActive && (guiPlaylist.firstProgram().startsWith(PlaylistWidget.GOLIVE_ITEMID) ||
						   guiPlaylist.getPlayItemWidget(guiPlaylist.firstProgram()).isLiveItem() &&
						   !guiPlaylist.getPlayItemWidget(guiPlaylist.firstProgram()).playBackgroundMusic())) {
		realGoliveActive = false;
		playlistWidget.activateStop();
	    }

	    if (playoutProviderActive && playoutProvider.isStoppable()) {
		if (playoutProvider.stop(programID, this) < 0)
		    errorInference.displayPlaylistError("Unable to send playout-stop command", new String[]{UNABLE_TO_SEND_COMMAND});

		//if (!micServiceActive || !goliveActive)
		countdownTimekeeper.stop();

		playingActive = false;
		pauseActive = false;
		updateGUIButtons();

		//guiPlaylist.dump();
	    }

	    
	} else if (actionCommand.equals(PlaylistWidget.VOLUMEUP)) {
	    String programID = guiPlaylist.firstProgram();
	    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
		return;
	    if (!playoutProvider.isPlaying (programID, this))
		return;

	    if (playoutProviderActive && playoutProvider.isVolumeAdjustable()) {
		if (playoutProvider.volumeUp(programID, this) < 0)
		    errorInference.displayPlaylistError("Unable to send playout-volume-Up command", new String[]{UNABLE_TO_SEND_COMMAND});
		else
		    playlistWidget.changeGain((int)AudioService.DELTA_GAIN);
	    }

	} else if (actionCommand.equals(PlaylistWidget.VOLUMEDN)) {
	    String programID = guiPlaylist.firstProgram();
	    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
		return;
	    if (!playoutProvider.isPlaying (programID, this))
		return;
	    if (playoutProviderActive && playoutProvider.isVolumeAdjustable()) {
		if (playoutProvider.volumeDn(programID, this) < 0)
		    errorInference.displayPlaylistError("Unable to send playout-volume-Dn command", new String[]{UNABLE_TO_SEND_COMMAND});
		else
		    playlistWidget.changeGain((int)-AudioService.DELTA_GAIN);
	    }
	    

	} else if (actionCommand.equals(PlaylistWidget.CURRINFO)) {
	    
	    if (metadataProviderActive) {
		String programID = guiPlaylist.firstProgram();
		
		if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
		    return;

		MetadataWidget metadataWidget = MetadataWidget.getMetadataWidget(rsApp, new String[]{programID}, false);
		if (!metadataWidget.isLaunched()) {
		    controlPanel.initWidget(metadataWidget, false);
		    controlPanel.cpJFrame().launchWidget(metadataWidget);
		} else {
		    controlPanel.cpJFrame().setWidgetMaximized(metadataWidget);
		}
	    }

	} else if (actionCommand.equals(PlaylistWidget.NEXTPROG)) {

	    //System.err.println("prov: "+playoutProviderActive+" playing "+playingActive+" pause "+pauseActive+" stoppable "+playoutProvider.isStoppable());
	    if (playoutProvider.isCreatingSession()){
		return;
	    }

	    if (playoutProviderActive && ((playingActive || pauseActive) && playoutProvider.isStoppable() 
					 || !playingActive && !pauseActive)) {

		String programID = guiPlaylist.firstProgram();
		boolean wasGoliveActive = false;

		guiPlaylist.removeFirstProgram();
		if (playingActive || pauseActive) {

		    playoutProvider.stop(programID, this);
		    playlistWidget.activateStop();
		    countdownTimekeeper.stop();
		    pauseActive = false;

		    //XXX: bala: confirm:
		    //playingActive = false;
		}

		if (/*micServiceActive &&*/ goliveActive) {
		    stopGolive();
		    wasGoliveActive = true;
		}

		if (realGoliveActive)
		    countdownTimekeeper.stop();

		if (guiPlaylist.size() > 0) {
		    programID = guiPlaylist.firstProgram();
		    final PlaylistWidget.PlayItemWidget playItemWidget = guiPlaylist.getPlayItemWidget(programID);

		    if (/*micServiceActive &&*/ (playingActive || wasGoliveActive) && !goliveActive &&  
		       //(programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
		       // bala 15 dec
		       (playItemWidget.isLiveItem())) {
			//startMic();
			initCountdownTimer(programID);
		    }
		    
		    if (!playItemWidget.isLiveItem() || playItemWidget.playBackgroundMusic()) {
			
			if (playingActive || realGoliveActive || wasGoliveActive) {
			    if (previewActive && previewFilename.equals(programID)) {
				// XXX there is a race condition here. need to test for previewProvider.isStoppable. 
				// if not stoppable right now, then ideally we should wait
				//				if (!previewProvider.isStoppable(programID, this)) {
				//				    try { Thread.sleep(200); } catch(InterruptedException exp) { }
				//				}

				if (previewProvider.stop(programID, this) < 0) {
				    errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
				    guiPlaylist.unsetPreviewIndex();
				}
				previewActive = false;
				previewFilename = "";
			    }
			    
			    //playoutProvider.setGoLiveActive(programID, false, this);

			    boolean isGoLiveItem = (goliveActive || 
						    playItemWidget.isLiveItem() && 
						    playItemWidget.playBackgroundMusic());// && micServiceActive;
			    
			    seekPosition = 0;

			    if (playoutProvider.play(programID, seekPosition, playItemWidget.getFadeoutStartTime(), isGoLiveItem, PlaylistController.this) < 0) {
				errorInference.displayPlaylistError("Unable to send playout-start command", new String[]{UNABLE_TO_SEND_COMMAND});
				
				if (/*micServiceActive && */goliveActive && (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || 
									playItemWidget.isLiveItem())) {
				    stopGolive();
				}
				advancePlaylist();

			    } else {
				//XXX: bala
				playingActive = true;
				initCountdownTimer(programID);
			    }
			}
		    } else {
			if (playingActive || realGoliveActive) {
			    realGoliveActive = true;
			    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
				playlistWidget.activatePlay(false);
			    initCountdownTimer(programID);
			}
			playingActive = false;
		    }

		    // XXX this is a bad call, but should work here because the next programID will match always
		    RadioProgramMetadata removedProgram = removeFromPlaylistScroll(programID);
		    guiPlaylist.setFirstProgram(removedProgram);
		    guiPlaylist.setFirstProgramItemWidget(playItemWidget);
		    playlistWidget.setStartTime(playStartTime, -1);

		    //		    remainingTime = removedProgram.getLength();

		} else {
		    playingActive = false;
		}

		if (playingActive) {
		    pauseActive = false;
		    realGoliveActive = false;
		}
		updateLivePane();
		updateGUIButtons();

		//guiPlaylist.dump();
	    }

	} else if (actionCommand.equals(PlaylistWidget.CURRPREVIEW)) {

	    if (previewProviderActive && previewProvider.isPlayable() && !previewActive) {
		String programID = guiPlaylist.firstProgram();
		if (previewProvider.play(programID, seekPosition, this) < 0) {
		    errorInference.displayPlaylistError("Unable to send preview-start command", new String[]{UNABLE_TO_SEND_COMMAND});
		    guiPlaylist.unsetPreviewIndex();
		} else {
		    previewActive = true;
		    previewFilename = programID;
		    guiPlaylist.setPreviewIndex(0);
		}
	    }

	} else if (actionCommand.equals(PlaylistWidget.CURRPREVIEWSTOP)) {
	    String programID = guiPlaylist.firstProgram();

	    if (previewProviderActive && previewProvider.isStoppable(programID, this) 
	       && previewFilename.equals(programID) && guiPlaylist.getPreviewIndex() == 0) {
		if (previewProvider.stop(programID, this) < 0) {
		    errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
		    guiPlaylist.unsetPreviewIndex();
		}
		previewActive = false;
		previewFilename = "";
	    }

	} else if (actionCommand.equals(PlaylistWidget.CLEAR_LIST)) {
	    if (playingActive){
		/*
		int choice = JOptionPane.showConfirmDialog(controlPanel.cpJFrame(), "Are you sure you want to clear current playlist?", "Clear Playlist", JOptionPane.YES_NO_OPTION);
		if (choice != JOptionPane.YES_OPTION)
		    return;
		*/
		return;
	    }

	    playlistWidget.clearPlaylist();
	    updateLivePane();
	    updateGUIButtons();

	} else if (actionCommand.equals(PlaylistWidget.SAVE_LIST)) {
	    RSButton saveButton = playlistWidget.getSaveButton();

	    if (savePlaylistPopup == null || !savePlaylistPopup.isVisible()) {
		savePlaylistPopup = new SavePlaylistPopup((int)(saveButton.getWidth() * 3), (int)(saveButton.getHeight()));
		savePlaylistPopup.showPopup(saveButton);
		saveButton.setPressed(true);
	    } else if (savePlaylistPopup.isVisible()) {
		saveButton.setPressed(false);
		savePlaylistPopup.raiseNullEvent();
	    }

	} else if (actionCommand.equals(PlaylistWidget.SETSTART_LIST)) {

	    //System.out.println("----- set start play list: " + startTimePlaylistPopup + ":" + 
	    //       (startTimePlaylistPopup == null ? "null" : startTimePlaylistPopup.isVisible()));
	    if (startTimePlaylistPopup == null || !startTimePlaylistPopup.isVisible()) {
		startTimePlaylistPopup = new StartTimePlaylistPopup(playlistWidget.getStartTimeButton());
		startTimePlaylistPopup.show(playlistWidget.getStartTimeButton(), 0, -(int)(startTimePlaylistPopup.getPreferredSize().getHeight()));
		playlistWidget.getStartTimeButton().setPressed(true);
	    } else if (startTimePlaylistPopup.isVisible()) {
		playlistWidget.getStartTimeButton().setPressed(false);
		startTimePlaylistPopup.raiseNullEvent();
	    }

	} else if (actionCommand.startsWith(PlaylistWidget.LISTPLAY)) {

	    if ((playoutProviderActive && (playingActive || pauseActive) && playoutProvider.isStoppable() || 
		!playingActive && !pauseActive && playoutProvider.isPlayable())) {

		String programID = guiPlaylist.firstProgram();
		String newProgramID = actionCommand.substring(actionCommand.indexOf("_") + 1);
		
		if (goliveItemPopup != null)
		    goliveItemPopup.closePopup();

		guiPlaylist.removeFirstProgram();

		final int oldIndex = playlistWidget.getSelectedIndex() - 1;

		guiPlaylist.moveToBeginning(oldIndex);
		PlaylistWidget.PlayItemWidget playItemWidget = playlistWidget.removePlaylistItem(oldIndex);

		RadioProgramMetadata programMetadata = playItemWidget.getMetadata();

		guiPlaylist.setFirstProgram(programMetadata);
		guiPlaylist.setFirstProgramItemWidget(playItemWidget);
		updateLivePane();
		if (playingActive || pauseActive) {
		    playlistWidget.activateStop();
		    playoutProvider.stop(programID, this);
		    countdownTimekeeper.stop();
		}

		boolean wasGoliveActive = false;
		if (/*micServiceActive &&*/ goliveActive) {
		    stopGolive();
		    wasGoliveActive = true;
		}

		if (realGoliveActive)
		    countdownTimekeeper.stop();

		// XXX there is a race condition here. need to test for previewProvider.isStoppable
		// if not stoppable right now, then ideally we should wait
		if (previewActive && previewFilename.equals(newProgramID)) {
		    //		    if (!previewProvider.isStoppable(newProgramID, this)) {
		    //			try { Thread.sleep(200); } catch(InterruptedException exp) { }
		    //		    }

		    if (previewProvider.stop(newProgramID, this) < 0) {
			errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
			guiPlaylist.unsetPreviewIndex();
		    }
		    previewActive = false;
		    previewFilename = "";
		}

		if (/*micServiceActive &&*/ !goliveActive &&
		   //(newProgramID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
		   (playItemWidget.isLiveItem())) {
		    //startMic();
		    initCountdownTimer(newProgramID);
		}

		// FADE duration passed here
		if (newProgramID.startsWith(PlaylistWidget.GOLIVE_ITEMID) ||
		   playItemWidget.isLiveItem() && !playItemWidget.playBackgroundMusic()) {
		    if (playingActive || realGoliveActive) {
			realGoliveActive = true;
			playlistWidget.activatePlay(false);
			initCountdownTimer(newProgramID);
		    }
		    playingActive = false;
		    pauseActive = false;

		    updateGUIButtons();
		    return;
		}

		//playoutProvider.setGoLiveActive(newProgramID, false, this);
		boolean isGoLiveBackground = (goliveActive || playItemWidget.isLiveItem() && playItemWidget.playBackgroundMusic());// && micServiceActive;

		if (playoutProvider.play(newProgramID, seekPosition, playItemWidget.getFadeoutStartTime(), isGoLiveBackground, this) < 0) {
		    errorInference.displayPlaylistError("Unable to send playout-start command", new String[]{UNABLE_TO_SEND_COMMAND});

		    if (/*micServiceActive &&*/ goliveActive && (newProgramID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
			stopGolive();
		    }
		    advancePlaylist();

		} else {
		    playingActive = true;
		    initCountdownTimer(newProgramID);
		}

		restartTimer = !pauseActive;

		if (playingActive) {
		    pauseActive = false;
		    realGoliveActive = false;
		}
		updateGUIButtons();
	    }

	} else if (actionCommand.startsWith(PlaylistWidget.LISTPREVIEW)) {
	    if (previewProviderActive && previewProvider.isPlayable() && !previewActive) {
		String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);
		if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
		    return;

		if (previewProvider.play(programID, this) < 0) {
		    errorInference.displayPlaylistError("Unable to send preview-start command", new String[]{UNABLE_TO_SEND_COMMAND});
		    guiPlaylist.unsetPreviewIndex();
		} else {
		    previewActive = true;
		    previewFilename = programID;
		    guiPlaylist.setPreviewIndex(playlistWidget.getSelectedIndex());
		}
	    }
	} else if (actionCommand.startsWith(PlaylistWidget.LISTSTOP)) {
	    String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);

	    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
		return;

	    if (previewProviderActive && previewProvider.isStoppable(programID, this) && previewFilename.equals(programID)
	       && guiPlaylist.getPreviewIndex() == playlistWidget.getSelectedIndex()) {
		if (previewProvider.stop(programID, this) < 0) {
		    errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
		    guiPlaylist.unsetPreviewIndex();
		}
		previewActive = false;
		previewFilename = "";
	    }

	} else if (actionCommand.startsWith(PlaylistWidget.LISTINFO)) {

	    String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);

	    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
		return;

	    if (metadataProviderActive) {
		MetadataWidget metadataWidget = MetadataWidget.getMetadataWidget(rsApp, new String[]{programID}, false);
		if (!metadataWidget.isLaunched()) {
		    controlPanel.initWidget(metadataWidget, false);
		    controlPanel.cpJFrame().launchWidget(metadataWidget);
		} else {
		    controlPanel.cpJFrame().setWidgetMaximized(metadataWidget);
		}
	    }

	} else if (actionCommand.startsWith(SimulationWidget.LISTLIVE)) {

	    String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);
	    int index = 0;
	    if (playlistWidget.getListPane().getSelectedIndices() != null 
		&& playlistWidget.getListPane().getSelectedIndices().length > 0) {
		
		index = playlistWidget.getListPane().getSelectedIndices()[0];
	    }

	    goliveItemPopup = new GoliveItemPopup((RSButton)(e.getSource()), programID, index);
	    goliveItemPopup.showPopup(playlistWidget.getListPane(), playlistWidget.getListElemGoliveXoff() - 2, 
				      (int)(index * playlistWidget.getPlayItemSize().getHeight() +
					    playlistWidget.getListElemGoliveYoff()));

	} else if (actionCommand.startsWith(PlaylistWidget.LISTFADE)) {

	    String programID = actionCommand.substring(actionCommand.indexOf("_") + 1);

	    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
		return;

	    if (playlistWidget.getListPane().getSelectedIndices() != null && playlistWidget.getListPane().getSelectedIndices().length > 0) {
		PlaylistWidget.PlayItemWidget playItemWidget = (PlaylistWidget.PlayItemWidget)(playlistWidget.getListPane().getSelectedValues()[0]);
		/*
		//For fadeout button removal
		if (playItemWidget != null)
		    if (playItemWidget.getFadeoutButton().isSelected()) {
			if (playItemWidget.getFadeoutDuration() <= 0) {
			    playItemWidget.setFadeoutDuration(PlaylistWidget.FADEOUT_DURATION);
			} else {
			    playItemWidget.setFadeoutDuration(playItemWidget.getFadeoutDuration());
			}
		    } else {
			// do nothing
		    }
		*/
	    }
	}
	updateGUIButtons();
    }

    Object playlistLock = new Object();
    static final int loadPlaylistHorizFrac = 80;

    void createPlaylistPopup(){
	Thread t = new Thread(){
		public void run(){
		    synchronized(playlistLock){
			listPopup = new PlaylistPopup(PlaylistController.this, metadataProvider, stationConfig, 
						      playlistWidget.getPanelWidth() * loadPlaylistHorizFrac/100);
			playlistLock.notifyAll();
		    }

		    SwingUtilities.invokeLater(new Runnable(){
			    public void run(){
				listPopup.setVisible(true);
				listPopup.setVisible(false);
			    }
			});
		}
	    };
	t.start();
    }

    public void loadPlaylists(){
	synchronized(playlistLock) {
	    while (listPopup == null) {
		try{
		    playlistLock.wait();
		} catch (Exception e){}
	    }
	}
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    listPopup.show(playlistWidget.getLoadButton(), 0, -(int)listPopup.getPreferredSize().getHeight());
		}
	    });
    }

    public boolean isPlayingActive(){
	return playingActive;
    }

    public long getCurrentTime(){
	return timekeeper.getLocalTime();
    }

    public void setSeekPosition(int position){

	if (position + remainingTime == currentElementDuration)
	    return;
	
	if (guiPlaylist.size() == 0)
	    return;

	String programID = guiPlaylist.firstProgram();
	if (previewProvider.isStoppable (programID, this))
	    previewProvider.seekToPosition (programID, position, this);
	else if (playoutProvider.isPlaying (programID, this))
	    return;
	
	seekPosition = position;

    }

    public void volumeChanged(int gain){
	String programID = guiPlaylist.firstProgram();
	if (programID == null || programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
	    return;
	if (!playoutProvider.isPlaying (programID, this))
	    return;

	if (playoutProviderActive && playoutProvider.isVolumeAdjustable()) {
	    if (playoutProvider.volumeChanged(programID, this, gain) < 0)
		errorInference.displayPlaylistError("Unable to send playout-volume-changed command", new String[]{UNABLE_TO_SEND_COMMAND});
	    else
		playlistWidget.setGain(gain);
	}
    }

    protected void initCountdownTimer(String programID) {
	playStartTime = timekeeper.getLocalTime();
	playlistWidget.setStartTime(playStartTime, -1);

	int delay = 1000;
	ActionListener startTask = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    remainingTime -= 1000;
		    if (remainingTime > 0) {
			playlistWidget.setCurrentElementTimeRemaining(remainingTime);
		    } else {
			playStartTime = timekeeper.getLocalTime();
			playlistWidget.setStartTime(playStartTime, 0);
		    }
		}
	    };
	if (countdownTimekeeper != null && countdownTimekeeper.isRunning())
	    countdownTimekeeper.stop();
	countdownTimekeeper = new Timer(delay, startTask);
	countdownTimekeeper.setRepeats(true);

	if (guiPlaylist.size() > 0){
	    currentElementDuration = guiPlaylist.getPlayItemWidget(programID).getGoliveDuration();
	    if (seekPosition > 0)
		remainingTime = currentElementDuration - seekPosition;
	    else
		remainingTime = currentElementDuration;
	} else
	    remainingTime = (long)(10 * 1000);

	countdownTimekeeper.start();
    }

    boolean restartTimer;
    public synchronized void playActive(boolean success, String programID, String playType, long telecastTime, String[] error) {
	if (playType.equals(AudioService.PLAYOUT)){
	    if (success && !playlistWidget.isPauseActive()){
		controlPanel.cpJFrame().notifyPlayout(programID, telecastTime);
	    }
	    playlistWidget.activatePlay(success);
	    playingActive = success;
	    
	    if (!success){
		advancePlaylist();
	    }

	} else {
	    int index = guiPlaylist.getPreviewIndex();
	    if (index > 0)
		playlistWidget.activatePreview(index, success);
	    else
		playlistWidget.activateCurrentElementPreview(success);
	}
	if (!success)
	    errorInference.displayServiceError("Unable to play file", error);
	if (playType.equals(AudioService.PLAYOUT) && success){
	    //System.out.println("starting cd "+programID);
	    if (restartTimer || countdownTimekeeper == null) {
		initCountdownTimer(programID);
	    }
	    else {
		countdownTimekeeper.start();
	    }
	}

	updateGUIButtons();
    }


    public void removeFirstProgram() {
	String programID;
	String oldProgramID;

	if (guiPlaylist.size() <= 0)
	    return;

	oldProgramID = guiPlaylist.firstProgram();
	guiPlaylist.removeFirstProgram();

	if (playingActive || pauseActive) {
	    playlistWidget.activateStop();
	    playoutProvider.stop(oldProgramID, this);
	    countdownTimekeeper.stop();
	}
	
	if (previewActive && previewFilename.equals(oldProgramID)) {
	    if (previewProvider.stop(oldProgramID, this) < 0) {
		errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
		guiPlaylist.unsetPreviewIndex();
	    }
	    previewActive = false;
	    previewFilename = "";
	}

	if (guiPlaylist.size() > 0) {
	    programID = guiPlaylist.firstProgram();
	    PlaylistWidget.PlayItemWidget playItemWidget = guiPlaylist.getPlayItemWidget(programID);
	    
	    if (previewActive && previewFilename.equals(programID)) {
		playlistWidget.activateCurrentElementPreview(true);
	    } 

	    // bad call, but should work because the next item is the one found
	    RadioProgramMetadata removedProgram = removeFromPlaylistScroll(programID);
	    guiPlaylist.setFirstProgram(removedProgram);
	    guiPlaylist.setFirstProgramItemWidget(playItemWidget);
	} else {
	    playlistWidget.clearPlaylist();
	    updateLivePane();
	}

	updateGUIButtons();
    }
	
    protected boolean advancePlaylist(){
	String programID;
	seekPosition = 0;
	if (guiPlaylist.size() <= 0)
	    return true; 

	guiPlaylist.removeFirstProgram();

	if (countdownTimekeeper != null)
	    countdownTimekeeper.stop();
	playlistWidget.activateStop();
	if (guiPlaylist.size() > 0) {
	    programID = guiPlaylist.firstProgram();

	    PlaylistWidget.PlayItemWidget playItemWidget = guiPlaylist.getPlayItemWidget(programID);
	    
	    if (previewActive && previewFilename.equals(programID)) {
		// XXX potential preview race condition
		if (previewProvider.stop(programID, this) < 0) {
		    errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
		    guiPlaylist.unsetPreviewIndex();
		}
		previewActive = false;
		previewFilename = "";
	    }

	    if (/*micServiceActive &&*/ !goliveActive && !realGoliveActive &&
	       //(programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
	       //bala 15 dec
	       (playItemWidget.isLiveItem())) {
		//startMic();
		initCountdownTimer(programID);
	    }

	    // FADE duration passed here
	    if (!playItemWidget.isLiveItem() || playItemWidget.playBackgroundMusic()) {
		//playoutProvider.setGoLiveActive(programID, false, this);

		boolean isGoLiveBackground = (goliveActive || playItemWidget.isLiveItem() && playItemWidget.playBackgroundMusic());// && micServiceActive;
		if (playoutProvider.play(programID, seekPosition, playItemWidget.getFadeoutStartTime(), isGoLiveBackground, this) < 0) {
		    errorInference.displayPlaylistError("Unable to send playout-start command", new String[]{UNABLE_TO_SEND_COMMAND});
		    
		    if (/*micServiceActive &&*/ goliveActive && (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
			stopGolive();
		    }
		    return advancePlaylist();

		} else {
		    playingActive = true;
		    initCountdownTimer(programID);
		}
	    } else {
		if (playingActive) {
		    realGoliveActive = true;
		    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
			playlistWidget.activatePlay(false);
		    initCountdownTimer(programID);
		}
		playingActive = false;
	    }

	    // bad call, but should work because the next item is the one found
	    RadioProgramMetadata removedProgram = removeFromPlaylistScroll(programID);
	    guiPlaylist.setFirstProgram(removedProgram);
	    guiPlaylist.setFirstProgramItemWidget(playItemWidget);
	
	    return true;
	}

	return false;
    }
    
    public synchronized void pauseActive(boolean success, String programID, String playType, String[] error) {

	if (playType.equals(AudioService.PLAYOUT))
	    playlistWidget.activatePause(success);
	else {
	    // will not come here
	    //	    int index = guiPlaylist.getItemIndex(programID);
	    //	    playlistWidget.activatePause(index, success);
	}
	if (!success)
	    errorInference.displayServiceError("Unable to pause file", error);
    }

    public synchronized void stopActive(boolean success, String programID, String playType, String[] error) {
	if (playType.equals(AudioService.PLAYOUT)){
	    logger.debug("stopActive: Stopping playout: " + success + ":" + programID);

	    playlistWidget.activateStop();
	    playingActive = false;
	    if (countdownTimekeeper != null)
		countdownTimekeeper.stop();

	} else {
	    int index = guiPlaylist.getPreviewIndex();
	    previewActive = false;
	    if (index > 0 && index < guiPlaylist.size()){
		playlistWidget.activateStop(index);
		//guiPlaylist.dump();
	    } else if (index == 0)
		playlistWidget.activateCurrentElementPreviewStop(success);
	    guiPlaylist.unsetPreviewIndex();
	}


	notifyAll();


	if (!success)
	    errorInference.displayServiceError("Unable to stop file", error);
	updateGUIButtons();
    }

    public synchronized void playDone(boolean success, String programID, String playType) {
	seekPosition = 0;

	if (playType.equals(AudioService.PLAYOUT)) {
	    guiPlaylist.removeFirstProgram();
	    countdownTimekeeper.stop();
	    playlistWidget.activateStop();

	    boolean wasGoliveActive = false;
	    if (/*micServiceActive &&*/ goliveActive) {
		stopGolive();
		wasGoliveActive = true;
	    }
	    
	    if (guiPlaylist.size() > 0) {
		programID = guiPlaylist.firstProgram();
		PlaylistWidget.PlayItemWidget playItemWidget = guiPlaylist.getPlayItemWidget(programID);

		RadioProgramMetadata metadata = playItemWidget.getMetadata();
		if (metadata.getTitle().equals("restart-grins-now")) {
		    logger.info("PlayDone: Restarting GRINS");
		    handleGRINSRestartItem();
		    return;
		}

		if (previewActive && previewFilename.equals(programID)) {
		    // XXX potential preview race condition
		    if (previewProvider.stop(programID, this) < 0) {
			errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
			guiPlaylist.unsetPreviewIndex();
		    }
		    previewActive = false;
		    previewFilename = "";
		}

		if (/*micServiceActive &&*/ !goliveActive &&
		   //(programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
		   // bala 15 dec
		   (playItemWidget.isLiveItem())) {
		    //startMic();
		    initCountdownTimer(programID);
		}

		// FADE duration passed here
		if (!playItemWidget.isLiveItem() || playItemWidget.playBackgroundMusic()) {
		    //playoutProvider.setGoLiveActive(programID, false, this);

		    boolean isGoLiveBackground = (goliveActive || playItemWidget.isLiveItem() && playItemWidget.playBackgroundMusic());// && micServiceActive;

		    if (playoutProvider.play(programID, seekPosition, playItemWidget.getFadeoutStartTime(), isGoLiveBackground, this) < 0) {
			errorInference.displayPlaylistError("Unable to send playout-start command", new String[]{UNABLE_TO_SEND_COMMAND});

			if (/*micServiceActive &&*/ goliveActive && (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
			    stopGolive();
			}
			advancePlaylist();

		    } else {
			playingActive = true;
			initCountdownTimer(programID);
		    }
		} else {
		    if (playingActive) {
			realGoliveActive = true;
			if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID))
			    playlistWidget.activatePlay(false);
			initCountdownTimer(programID);
		    }
		    playingActive = false;
		}

		// XXX bad call, but should work
		RadioProgramMetadata removedProgram = removeFromPlaylistScroll(programID);

		guiPlaylist.setFirstProgram(removedProgram);
		guiPlaylist.setFirstProgramItemWidget(playItemWidget);
	    } else
		playingActive = false;
	    if (playingActive) {
		pauseActive = false;
		realGoliveActive = false;
	    }
	    updateLivePane();
	} else {
	    previewActive = false;
	    previewFilename = "";
	    int index = guiPlaylist.getPreviewIndex();
	    if (index > 0 && index < guiPlaylist.size())
		playlistWidget.activateStop(index);
	    else if (index == 0)
		playlistWidget.activateCurrentElementPreviewStop(success);
	    guiPlaylist.unsetPreviewIndex();
	}
	if (!success)
	    errorInference.displayServiceError("Problem occured during play", new String[]{});
	
	updateGUIButtons();

    }

    // callback from guiplaylist, if the item is removed
    public void stopPreview(int index) {
	if (previewActive && previewFilename != null && !previewFilename.equals("")) {
	    int retVal;
	    if ((retVal = previewProvider.stop(previewFilename, this)) < 0)
		errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
	    previewActive = false;
	    previewFilename = "";
	    if (index > 0 && index < guiPlaylist.size())
		playlistWidget.activateStop(index);
	    else if (index == 0)
		playlistWidget.activateCurrentElementPreviewStop(retVal >= 0);
	}
    }

    // callback from guiplaylist, if the item is moved up to the live panel
    public void activatePreview(int index) {
	if (index == 0)
	    playlistWidget.activateCurrentElementPreview(previewActive);
    }

    // callback from guiplaylist, if current item is updated
    public void updateFirstProgram(RadioProgramMetadata program){
	playlistWidget.updateFirstProgram(program);
    }

    //Error from playout/preview
    public synchronized void audioGstError(String programID, String error, String playType){
	errorInference.displayGstError("Unable to play", error);

	if (playType.equals(AudioService.PLAYOUT)){
	    //playingActive = false;
	    //playlistWidget.activatePlaylistClear(true);

	    guiPlaylist.removeFirstProgram();
	    //	    playoutProvider.stop(programID, this);
	    if (countdownTimekeeper != null)
		countdownTimekeeper.stop();
	    playlistWidget.activateStop();
	    

	    if (guiPlaylist.size() > 0) {
		programID = guiPlaylist.firstProgram();
		PlaylistWidget.PlayItemWidget playItemWidget = guiPlaylist.getPlayItemWidget(programID);

		if (previewActive && previewFilename.equals(programID)) {
		    // XXX potential preview race condition
		    if (previewProvider.stop(programID, this) < 0) {
			errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{UNABLE_TO_SEND_COMMAND});
			guiPlaylist.unsetPreviewIndex();
		    }
		    previewActive = false;
		    previewFilename = "";
		}

		if (/*micServiceActive &&*/ !goliveActive && !realGoliveActive && 
		   //(programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
		   // bala 15 dec
		   (playItemWidget.isLiveItem())) {
		    //startMic();
		    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID)) {
			initCountdownTimer(programID);
		    }
		}

		// FADE duration passed here
		if (!playItemWidget.isLiveItem() || playItemWidget.playBackgroundMusic()) {
		    //playoutProvider.setGoLiveActive(programID, false, this);

		    boolean isGoLiveBackground = (goliveActive || playItemWidget.isLiveItem() && playItemWidget.playBackgroundMusic());// && micServiceActive;

		    if (playoutProvider.play(programID, seekPosition, playItemWidget.getFadeoutStartTime(), isGoLiveBackground, this) < 0) {
			errorInference.displayPlaylistError("Unable to send playout-start command", new String[]{UNABLE_TO_SEND_COMMAND});

			if (/*micServiceActive &&*/ goliveActive && (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID) || playItemWidget.isLiveItem())) {
			    stopGolive();
			}
			advancePlaylist();

		    } else {
			playingActive = true;
			initCountdownTimer(programID);
		    }
		} else {
		    realGoliveActive = true;
		    playingActive = false;
		    playlistWidget.activatePlay(false);
		    initCountdownTimer(programID);

		    //XXX:30June09: Verify that the following lines are indeed not needed.
		    /*if (playingActive) {
			realGoliveActive = true;
			playlistWidget.activatePlay(true);
			initCountdownTimer(programID);
			}
			playingActive = false;*/
		}

		// XXX bad call, but should work
		RadioProgramMetadata removedProgram = removeFromPlaylistScroll(programID);
		guiPlaylist.setFirstProgram(removedProgram);
		guiPlaylist.setFirstProgramItemWidget(playItemWidget);
	    } else {
		playingActive = false;
		playlistWidget.activateStop();

	    }
	    if (playingActive) {
		pauseActive = false;
		realGoliveActive = false;
	    }
	    updateLivePane();

	} else {
	    previewActive = false;
	    previewFilename = "";
	    int index = guiPlaylist.getPreviewIndex();
	    if (index > 0 && index < guiPlaylist.size())
		playlistWidget.activateStop(index);
	    else if (index == 0)
		playlistWidget.activateCurrentElementPreviewStop(false);
	    guiPlaylist.unsetPreviewIndex();
	}
	
	//playlistWidget.activateStop();
		
	updateGUIButtons();
    }

    //Error from archiver,
    /* These errors are seen when the RJ is live on-air.
     */
    public synchronized void archiverGstError(String error){
	errorInference.displayGstError("Unable to archive", error);
	
	if (goliveActive && micServiceActive) {
	    playingActive = false;
	    pauseActive = false;
	    realGoliveActive = false;
	}

	goliveActive = false;

	playlistWidget.activateGoLive(false);

	if (micServiceActive && micEnabled)
	    if (micProvider != null && micProvider.isActive() && micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE) != 0) {
		errorInference.displayServiceError("Unable to send stop-mic command", new String[] {UNABLE_TO_SEND_COMMAND});
	    } else {
		//MicDisabled call back sets golive active so no need to do that when mic.disable() call succeeded
		String programID = guiPlaylist.firstProgram();
		if (programID != null && (playingActive || pauseActive))
		    playoutProvider.setGoLiveActive(programID, false, this);
	    }
    }

    public synchronized void playoutPreviewError(String errorCode, String[] errorParams) {
	String programID = errorParams[0];
	String playType = errorParams[1];
	if (playType.equals(AudioService.PLAYOUT)) {
	    if (playingActive) {
		playlistWidget.activateStop();
		countdownTimekeeper.stop();
	    }
	    playingActive = false;
	    pauseActive = false;
	} else {
	    int index = guiPlaylist.getPreviewIndex();
	    if (index > 0 && index < guiPlaylist.size())
		playlistWidget.activateStop(index);
	    else if (index == 0)
		playlistWidget.activateCurrentElementPreviewStop(false);		
	    previewActive = false;
	    previewFilename = "";
	    guiPlaylist.unsetPreviewIndex();
	}

	errorInference.displayServiceError("Playout/preview error occured", errorCode, errorParams);
    }

    public synchronized void activateAudio(boolean activate, String playType, String[] error) {
	if (playType.equals(AudioService.PREVIEW)){
	    previewProviderActive = activate;
	    previewUIEnabled = activate;
	    if (!activate)
		previewActive = false;
	} else {
	    playoutUIEnabled = activate;
	    playoutProviderActive = activate;
	    if (!activate){
		playingActive = false;
		pauseActive = false;
		if (countdownTimekeeper != null)
		    countdownTimekeeper.stop();
		seekPosition = playlistWidget.getSliderPosition();
	    }
	}

	if (!activate)
	    errorInference.displayServiceError("Unable to activate " + playType, error);

	updateGUIButtons();
    }

    public synchronized void enableAudioUI(boolean enable, String playType, String[] error) {

	if (playType.equals(AudioService.PREVIEW)){
	    previewUIEnabled = enable;
	} else {
	    playoutUIEnabled = enable;
	    /*
	    if (!enable) {
		if (countdownTimekeeper != null)
		    countdownTimekeeper.stop();
	    } else {
		if (countdownTimekeeper != null)
		    countdownTimekeeper.start();
	    }
	    */
	}

	if (!enable) {
	    errorInference.displayServiceError("Unable to activate audio for " + playType, error);
	}

	updateGUIButtons();
    }

    public synchronized void activateMetadataService(boolean activate, String[] error) {
	metadataProviderActive = activate;

	if (!activate)
	    errorInference.displayServiceError("Unable to activate metadata service", error);
	else if (listPopup != null)
	    listPopup.loadPlaylists();

	updateGUIButtons();
    }

    public String getName() {
	return PLAYLIST_CONTROLLER;
    }

    protected void updateGUIButtons() {
	if (playoutProviderActive && playoutUIEnabled && liveActive) {
	    playlistWidget.activatePlayout(true);

	    if (guiPlaylist.size() > 1){
		playlistWidget.activateLiveNext(true);
	    }
	    else
		playlistWidget.activateLiveNext(false);

	    if (playingActive)
		playlistWidget.activatePlaylistClear(false);
	    else
		playlistWidget.activatePlaylistClear(true);
	
	} else if (playoutProviderActive && !playoutUIEnabled && liveActive) {
	    
	    playlistWidget.enablePlayoutUI(false);
	    playlistWidget.activateLiveNext(false);
	    
	    if (playingActive)
		playlistWidget.activatePlaylistClear(false);
	    else
		playlistWidget.activatePlaylistClear(true);
	}
	else {
	    playlistWidget.activatePlayout(false);
	    //playlistWidget.activateLivePreview(false); //Commented to remove a bug.
	    playlistWidget.activateLiveNext(false);
	}

	if (metadataProviderActive && liveActive)
	    playlistWidget.activateLiveInfo(true);
	else
	    playlistWidget.activateLiveInfo(false);
	
	if (metadataProviderActive)
	    playlistWidget.activateInfo(true);
	else
	    playlistWidget.activateInfo(false);

	/*
	if (archiverProviderActive)
	    playlistWidget.activateArchiverButton(true);
	else
	    playlistWidget.activateArchiverButton(false);
	*/

	if (micServiceActive)
	    playlistWidget.activateArchiverButton(micProviderActive && archiverProviderActive && archiverUIEnabled);
	else
	    playlistWidget.activateArchiverButton(archiverProviderActive && archiverUIEnabled);

	if (previewProviderActive && previewUIEnabled) {
	    playlistWidget.activatePreview(true);

	    if (liveActive && !playingActive && !pauseActive)
		playlistWidget.activateLivePreview(true);
	    else 
		playlistWidget.activateLivePreview(false);
	} else if (previewProviderActive && !previewUIEnabled){
	    playlistWidget.enablePreviewUI(false);

	    if (liveActive && !playingActive && !pauseActive)
		playlistWidget.enableLivePreviewUI(false);
	    //lines below are not really needed
	    //else 
	    //playlistWidget.activateLivePreview(false);

	} else {
	    playlistWidget.activatePreview(false);
	    playlistWidget.activateLivePreview(false);	    
	}

	if (guiPlaylist.firstProgram() == null) {
	    if (startTimePlaylistPopup != null && startTimePlaylistPopup.isVisible())
		startTimePlaylistPopup.raiseNullEvent();
	    playlistWidget.getStartTimeButton().setEnabled(false);

	    if (savePlaylistPopup != null && savePlaylistPopup.isVisible())
		savePlaylistPopup.raiseNullEvent();
	    playlistWidget.getSaveButton().setEnabled(false);

	    playlistWidget.getClearButton().setEnabled(false);
	} else {
	    playlistWidget.getStartTimeButton().setEnabled(true);
	    if (cacheProviderActive) {
		playlistWidget.getSaveButton().setEnabled(true);
	    } else {
		if (savePlaylistPopup != null && savePlaylistPopup.isVisible())
		    savePlaylistPopup.raiseNullEvent();
		playlistWidget.getSaveButton().setEnabled(false);
	    }		
	    playlistWidget.getClearButton().setEnabled(true);
	}

	if (cacheProviderActive) {
	    playlistWidget.getLoadButton().setEnabled(true);
	} else {
	    playlistWidget.getLoadButton().setEnabled(false);
	}
	
    }

    protected void updateLivePane() {

	String programID = guiPlaylist.firstProgram();

	if (programID != null) {
	    RadioProgramMetadata metadata = guiPlaylist.getProgram(programID);

	    playlistWidget.initLivePane(metadata);
	    liveActive = true;
	    
	    remainingTime = guiPlaylist.getPlayItemWidget(programID).getGoliveDuration();
	    seekPosition = 0;
	    
	    if (programID.startsWith(PlaylistWidget.GOLIVE_ITEMID)) {
		playlistWidget.enablePlayFunctions(false);
	    } else {
		playlistWidget.enablePlayFunctions(true);
	    }

	} else {
	    playlistWidget.initLivePane(null);
	    liveActive = false;
	}
    }

    protected void setStartTimer() {
	int delay = (int)(playStartTime - timekeeper.getLocalTime());
	logger.debug("setStartTimer: delay = " + delay + ": playStartTime = " + playStartTime);
	if (delay > 0) {
	    ActionListener startTask = new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
			startTimekeeper.stop();
			if (liveActive)
			    playlistWidget.triggerPlay();
		    }
		};
	    if (startTimekeeper != null && !startTimekeeper.isRunning())
		startTimekeeper.stop();
	    playlistWidget.setStartTime(playStartTime, -1);
	    startTimekeeper = new Timer(delay, startTask);
	    startTimekeeper.setRepeats(false);
	    startTimekeeper.start();
	}
    }

    public void addPrograms(ArrayList<RadioProgramMetadata> programList, int index){
	Playlist playlist = new Playlist(-1);
	playlist.addPrograms(programList);

	logger.debug("addPrograms: called on index = " + index + ": guiPlaylist size = " + guiPlaylist.size());
	
	if (guiPlaylist.size() > 0) {
	    guiPlaylist.init(playlist, index);
	    playlistWidget.addPrograms(programList, index);
	} else {
	    guiPlaylist.init(playlist, index);
	    playlistWidget.initPlaylist(guiPlaylist, playlist);
	}

	ArrayList<String> guiPlaylistPrograms = guiPlaylist.getPrograms();
	for (int i = 0; i < guiPlaylistPrograms.size(); i++)
	    logger.debug("addPrograms: after add programs  -- ordering: " + guiPlaylistPrograms.get(i) + ":" + i);

	playlistWidget.setStartTime(playStartTime, -1);

	if (!playingActive && !pauseActive)
	    updateLivePane();
	updateGUIButtons();
    }

    public void addPlayItems(ArrayList<PlaylistWidget.PlayItemWidget> itemList, int index){
	playlistWidget.addPlayItems(itemList, index);
	playlistWidget.setStartTime(playStartTime, -1);

	if (!playingActive && !pauseActive)
	    updateLivePane();
	updateGUIButtons();
    }

    public void removeProgramsAt(int indices[], boolean removeCacheListener, int newPreviewIndex) {
	guiPlaylist.removeProgramsAt(indices, removeCacheListener, newPreviewIndex);
	playlistWidget.setStartTime(playStartTime, -1);

	if (!playingActive && !pauseActive)
	    updateLivePane();
	updateGUIButtons();
    }

    protected RadioProgramMetadata removeFromPlaylistScroll(String programID) {
	int index = guiPlaylist.getItemIndex(programID);
	PlaylistWidget.PlayItemWidget itemWidget = playlistWidget.removePlaylistItem(index);

	if (itemWidget != null)
	    return itemWidget.getMetadata();
	else
	    return null;
    }

    public void loadPlaylist(Playlist playlist){
	metadataProvider.getPlaylistItems(playlist);
	
	
	if (playlist.getStartTime() <= timekeeper.getLocalTime()) {
	    if (playStartTime > timekeeper.getLocalTime()){
		playlist.setStartTime(playStartTime);
	    } else {
		if (!(playingActive || pauseActive || goliveActive || realGoliveActive)){
		    playlist.setStartTime(playStartTime = defaultStartTime());
		} 
	    }
	}

	guiPlaylist.init(playlist, guiPlaylist.size()-1);
	guiPlaylist.initDetails(playlist);
	playlistWidget.initPlaylist(guiPlaylist, playlist);

	if (!(playingActive || pauseActive || goliveActive || realGoliveActive))
	    setStartTimer();

	if (!playingActive && !pauseActive)
	    updateLivePane();
	updateGUIButtons();
    }
    

    /* Call backs */

    public void activateCache(boolean activate, String[] error) {
	cacheProviderActive = activate;
	if (activate) {
	    guiPlaylist.setCacheProvider(cacheProvider);
	    metadataProvider.setCacheProvider(cacheProvider);
	} else {
	    errorInference.displayServiceError("Unable to activate cache service", error);
	}
	updateGUIButtons();
    }

    public void cacheObjectsDeleted(String objectType, String[] objectID){
	/*
	if (objectType.equals(Metadata.getClassName(RadioProgramMetadata.class)))
	    if (listPopup != null)
		listPopup.refreshPlaylists(objectID);
	*/
    }

    public void cacheObjectsUpdated(String objectType, String[] objectID){
	if (objectType.equals(Metadata.getClassName(Playlist.class)))
	    if (listPopup != null)
		listPopup.refreshPlaylists(objectID);
    }
    
    public void cacheObjectsInserted(String objectType, String[] objectID){}

    public synchronized void activateMic(boolean activate, String[] error){
	if (!micServiceActive)
	    return;

	micProviderActive = activate;

	if (!activate){
	    if (archiverProvider.isStoppable(PLAYLIST_CONTROLLER)){
		if (archiverProvider.stopArchive(ArchiverService.PLAYOUT, true) == 0){

		} else {
		    errorInference.displayServiceError("Unable to send stop-record command", new String[] {UNABLE_TO_SEND_COMMAND});
		}
	    }

	    if (goliveActive) {
		playingActive = false;
		pauseActive = false;
		realGoliveActive = false;
	    }

	    goliveActive = false;
	    micEnabled = false;
	    playlistWidget.activateGoLive(false);
	    errorInference.displayServiceError("Unable to activate mic service", error);
	} else {
	    playlistWidget.activateArchiverButton(activate);
	    playlistWidget.activateGoLive(false);
	}
	//updateGUIButtons();
    }

    public synchronized void enableMicUI(boolean enable, String[] error){
	if (micServiceActive){
	    //playlistWidget.activateArchiverButton(enable);
	    if (!enable)
		errorInference.displayServiceError("Unable to start live audio", error);
	    updateGUIButtons();
	}

    }

    public synchronized void micGstError(String error){
	errorInference.displayGstError("Unable to go live", error);

	if (micServiceActive){
	    if (archiverProvider.isStoppable(PLAYLIST_CONTROLLER)){
		if (archiverProvider.stopArchive(ArchiverService.PLAYOUT, true) == 0){

		} else {
		    errorInference.displayServiceError("Unable to send stop-record command", new String[] {UNABLE_TO_SEND_COMMAND});
		}
	    }

	    if (goliveActive) {
		playingActive = false;
		pauseActive = false;
		realGoliveActive = false;
	    }

	    goliveActive = false;
	    micEnabled = false;
	    playlistWidget.activateGoLive(false);

	    String programID = guiPlaylist.firstProgram();
	    if (programID != null && (playingActive || pauseActive))
		playoutProvider.setGoLiveActive(programID, false, this);
	}
    }

    public synchronized void micEnabled(boolean success, String[] error){
	if (!micServiceActive)
	    return;

	//bala 16aug begin
	//playlistWidget.activateGoLive(success && goliveActive);
	playlistWidget.activateGoLive(success);
	goliveActive = success;
	//bala 16aug end

	if (!success) {
	    micEnabled = false;

	    if (goliveActive) {
		playingActive = false;
		pauseActive = false;
		realGoliveActive = false;
	    }

	    goliveActive = false;
	    playlistWidget.activateGoLive(false);
	    errorInference.displayServiceError("Unable to enable mic", error);

	    if (archiverProvider.isStoppable(PLAYLIST_CONTROLLER) && archiverProvider.stopArchive(ArchiverService.PLAYOUT, true) == 0){

	    } else {
		errorInference.displayServiceError("Unable to send stop-record command", new String[] {UNABLE_TO_SEND_COMMAND});
	    }
	} else {
	    /*
	    String programID = guiPlaylist.firstProgram();
	    if (programID != null && playingActive)
	    playoutProvider.setGoLiveActive(programID, true, this);
	    if (countdownTimekeeper != null)
		countdownTimekeeper.start();
	    */
	}
    }

    protected void startMic() {
	boolean micStatus = false;

	if (!stationConfig.isMicServiceActive()){
	    micStatus = true;
	} else if (micProvider != null && micProvider.isActive() && !micEnabled){
	    micStatus = micEnabled = (micProvider.enable(PLAYLIST_CONTROLLER, MicProvider.PLAYOUT_PORT_ROLE) == 0);
	}

	boolean archiverStatus = false;

	/* If mic service is configured to run, connect mic before starting to archive */

	if (micStatus && archiverProvider.isArchivable()) {
	    archiverStatus = archiverProvider.startArchive(ArchiverService.PLAYOUT, PLAYLIST_CONTROLLER, true) == 0;
	}
		
	if (!archiverStatus && micEnabled) {
	    micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE);
	    micEnabled = false;
	}

	//System.err.println("---- STARTMIC: " + archiverStatus + ":" + micStatus);
		
	if (archiverStatus && micStatus){
	    goliveActive = true;
	    logger.info("ActionPerformed: GOLIVE: Started archiving.");
	} else {
	    errorInference.displayPlaylistError("Unable to send start-record command", new String[]{UNABLE_TO_SEND_COMMAND});
	    playlistWidget.selectedArchiverButton(false);
	    logger.error("ActionPerformed: GOLIVE: Failed to start archiving. Mic: "+micStatus+" Archiver: "+archiverStatus);
	}
    }

    protected void stopGolive() {
	if (goliveActive) {
	    playingActive = false;
	    pauseActive = false;
	    realGoliveActive = false;
	}

	goliveActive = false;
	playlistWidget.activateGoLive(false);

	if (archiverProvider.isStoppable(PLAYLIST_CONTROLLER) && archiverProvider.stopArchive(ArchiverService.PLAYOUT, true) == 0){
	    
	} else {
	    errorInference.displayServiceError("Unable to send stop-record command", new String[] {UNABLE_TO_SEND_COMMAND});
	}
	
	if (micServiceActive && micEnabled && micProvider != null && micProvider.isActive()){
	    if (micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE) == 0) {
		
	    } else {
		errorInference.displayServiceError("Unable to send stop-mic command", new String[] {UNABLE_TO_SEND_COMMAND});
	    }
	}

	micEnabled = false;
    }

    public synchronized void micDisabled(boolean success, String[] error){
	playlistWidget.activateGoLive(false);
	if (!success) {
	    errorInference.displayServiceError("Unable to disable mic", error);
	}
	
	String programID = guiPlaylist.firstProgram();
	if (programID != null && (playingActive || pauseActive))
	    playoutProvider.setGoLiveActive(programID, false, this);
    }
    
    public void micLevelTested(boolean success, int level){

    }

    public synchronized void activateArchiver(boolean activate, String[] error) {
	archiverProviderActive = activate;
	archiverUIEnabled = activate;
	//playlistWidget.activateArchiverButton(activate);
	if (!activate){
	    if (goliveActive && micServiceActive) {
		playingActive = false;
		pauseActive = false;
		realGoliveActive = false;
	    }

	    goliveActive = false;  //change archiving state
	    playlistWidget.activateGoLive(false);
	    errorInference.displayServiceError("Unable to start archiver", error);

	    if (micServiceActive && micEnabled && micProvider != null && micProvider.isActive()){
		if (micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE) != 0) {
		    errorInference.displayServiceError("Unable to send stop-mic command", new String[] {UNABLE_TO_SEND_COMMAND});
		}
	    }
	}
	updateGUIButtons();
    }
    
    //for changing UI accessibility/clickability
    public synchronized void enableArchiverUI(boolean enable, String[] error){
	archiverUIEnabled = enable;
	//playlistWidget.activateArchiverButton(enable);
	if (!enable)
	    errorInference.displayServiceError("Unable to start archiver", error);

	updateGUIButtons();
    }

    public synchronized void archiveStarted(boolean activate, String[] error) {

	playlistWidget.activateGoLive(activate && goliveActive);

	if (!activate){
	    errorInference.displayServiceError("Unable to start archiving", error);
	    
	    if (goliveActive && micServiceActive) {
		playingActive = false;
		pauseActive = false;
		realGoliveActive = false;
	    }

	    goliveActive = false;
	    playlistWidget.activateGoLive(false);

	    if (micServiceActive && micEnabled && micProvider != null && micProvider.isActive())
		if (micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE) != 0) {
		    errorInference.displayServiceError("Unable to send stop-mic command", new String[] {UNABLE_TO_SEND_COMMAND});
		}
	}
    }

    public synchronized void archiveStopped(boolean activate, String[] error) {
	playlistWidget.activateGoLive(false);
	if (!activate) 
	    errorInference.displayServiceError("Unable to stop archiving", error);

	String programID = guiPlaylist.firstProgram();
	if (programID != null && (playingActive || pauseActive))
	    playoutProvider.setGoLiveActive(programID, false, this);
    }
    
    public void archiverLevelTested(boolean success, int level){

    }

    public void archivingDone(String filename, boolean success, long startTime, String error) {
	// XXX get metadata object from database and push to log view widget
	if (success){
	    controlPanel.cpJFrame().notifyPlayout(filename, startTime);
	} else {
	    errorInference.displayServiceError("Unable to archive properly. Some problem occured", new String[]{error});
	}
    }

    void newPlaylistAdded(Playlist playlist){
	if (listPopup != null)
	    listPopup.playlistAdded(playlist);
    }

    void playlistUpdated(int playlistID){
	if (listPopup != null)
	    listPopup.playlistUpdated(playlistID);
    }

    public class SavePlaylistPopup extends JDialog implements ActionListener {
	Playlist[] playlists;
	String nullItem = "Select playlist or Type new";
	JComboBox playlistDropDown;
	boolean closePopup = true;

	public SavePlaylistPopup(int width, int height) {
	    super(controlPanel.cpJFrame(), false);

	    playlists = metadataProvider.getPlaylists();

	    Dimension dim = new Dimension(width, height);
	    setPreferredSize(dim);

	    playlistDropDown = new JComboBox(playlists);
	    playlistDropDown.setPreferredSize(dim);
	    playlistDropDown.addItem(nullItem);
	    playlistDropDown.setSelectedItem(nullItem);
	    playlistDropDown.setEditable(true);
	    playlistDropDown.addActionListener(this);

	    Color bgColor;
	    playlistDropDown.setBackground(bgColor = stationConfig.getColor(StationConfiguration.LOADSAVE_POPUP_COLOR));
	    setBackground(bgColor);
	    
	    add(playlistDropDown);

	    KeyListener escapeListener = new KeyAdapter(){
		    public void keyPressed(KeyEvent e){
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			    cancelSave();
			}
		    }
		};
	    
	    addWindowFocusListener(new WindowAdapter(){
		    public void windowLostFocus(WindowEvent e){
			cancelSave();
		    }
		});
	    
	    playlistDropDown.addKeyListener(escapeListener);

	    setUndecorated(true);
	    pack();
	    setVisible(false);
	}
	
	
	public void setVisible(boolean b) {
	    if (b || !closePopup)
		super.setVisible(true);
	    else
		super.setVisible(false);
	}

	void showPopup(JComponent assocComponent){
	    Dimension mySize = getPreferredSize();

	    int xOffset = -(mySize.width - assocComponent.getSize().width)/2;
	    int yOffset = -(mySize.height);
	    Point p = assocComponent.getLocationOnScreen();
	    
	    int popupX = p.x + xOffset;
	    int popupY = p.y + yOffset;
	    setLocation(popupX, popupY);
	    
	    playlistDropDown.hidePopup();

	    setVisible(true);
	    requestFocusInWindow();
	}

	public void cancelSave() {
	    setVisible(false);
	}

	public synchronized void actionPerformed(ActionEvent e) {
	    Object selectedObject = playlistDropDown.getSelectedItem();
	    
	    if (e.getActionCommand().equals("comboBoxEdited") || selectedObject.toString().equals("")){
		return;
	    }
	    
	    if (!selectedObject.toString().equals(nullItem)) {

		if (newEntry(selectedObject, playlists)) {

		    Playlist playlist = new Playlist(-1);
		    String playlistName = StringUtilities.sanitize(selectedObject.toString());
		    playlist.setName(playlistName);
		    playlist.setStartTime(playStartTime);
		    int playlistID = metadataProvider.addPlaylist(playlist);

		    if (playlistID != -1) {
			playlist.setPlaylistID(playlistID);
			playlistDropDown.addItem(playlist);
			playlists = metadataProvider.getPlaylists();
			
			Iterator<String> coll = guiPlaylist.getPrograms().iterator();
			int count = 0;
			while (coll.hasNext()) {
			    String programID = coll.next();
			    PlaylistItem playlistItem = new PlaylistItem(playlistID, programID, count);

			    PlaylistWidget.PlayItemWidget playlistItemWidget = guiPlaylist.getItemAtNotNull(count);
			    playlistItem.setLiveItem(playlistItemWidget.isLiveItem());
			    playlistItem.setPlayBackground(playlistItemWidget.playBackgroundMusic());
			    playlistItem.setLiveDuration(playlistItemWidget.getGoliveDuration());
			    playlistItem.setFadeout(playlistItemWidget.getFadeoutDuration());
			    
			    metadataProvider.addPlaylistItem(playlistItem);
			    count++;
			}
			newPlaylistAdded(playlist);
		    } else {
			errorInference.displayPlaylistError("Unable to save playlist", new String[]{UNABLE_TO_SAVE});
		    }

		} else {
		    closePopup = false;
		    Playlist playlist = new Playlist(-1);
		    Playlist newPlaylist = new Playlist(-1);
		    
		    for (Playlist curr: playlists)
			if (curr.getName().equals(selectedObject.toString())) {
			    
			    playlistDropDown.hidePopup();
			    String message = "Are you sure you want to replace playlist: "+curr.getName()+"?";
			    String title = "Replace existing playlist";
			    int retVal = JOptionPane.showConfirmDialog(rsApp.getControlPanel().cpJFrame(), message, title, JOptionPane.YES_NO_OPTION);
			    if (retVal != JOptionPane.YES_OPTION){
				playlistDropDown.setSelectedItem(nullItem);
				closePopup = true;
				return;
			    }
			    
			    playlist.setPlaylistID(curr.getPlaylistID());
			    newPlaylist.setStartTime(playStartTime);

			    if (guiPlaylist.getPrograms().iterator().hasNext()) {
				metadataProvider.removePlaylistItems(PlaylistItem.getDummyObject(curr.getPlaylistID()));
			    }
			    
			    Iterator<String> coll = guiPlaylist.getPrograms().iterator();
			    int count = 0;
			    while (coll.hasNext()) {
				String programID = coll.next();
				PlaylistItem playlistItem = new PlaylistItem(curr.getPlaylistID(), programID, count);

				PlaylistWidget.PlayItemWidget playlistItemWidget = guiPlaylist.getItemAtNotNull(count);
				playlistItem.setLiveItem(playlistItemWidget.isLiveItem());
				playlistItem.setPlayBackground(playlistItemWidget.playBackgroundMusic());
				playlistItem.setLiveDuration(playlistItemWidget.getGoliveDuration());
				playlistItem.setFadeout(playlistItemWidget.getFadeoutDuration());

				metadataProvider.addPlaylistItem(playlistItem);
				count++;
			    }
			    
			    metadataProvider.reloadPlaylistItems(playlist);
			    playlistUpdated(playlist.getPlaylistID());
			    break;
			}
		    
		    closePopup = true;
		}

		setVisible(false);
		playlistWidget.getSaveButton().setPressed(false);
	    } 
	}

	public void raiseNullEvent() {
	    cancelSave();
	}
    }
    
    public class StartTimePlaylistPopup extends JPopupMenu implements ActionListener {
	JSpinner hourSpinner;
	JSpinner minSpinner;
	JSpinner secSpinner;

	//JSpinner fadeoutSpinner;

	RSButton okButton, cancelButton;
	RSButton fadeOkButton;

	boolean selectionDone = false;

	public StartTimePlaylistPopup(JComponent parentComponent) {
	    super();

	    Dimension dim = new Dimension((int)(parentComponent.getWidth() * 2/*3*/) + 10, (int)(parentComponent.getHeight() * 3 + 10));
	    setPreferredSize(dim);

	    Calendar calendar = new GregorianCalendar();
	    //calendar.setTimeInMillis(timekeeper.getLocalTime() + 10 * 60 * (long)1000);
	    calendar.setTimeInMillis(playStartTime);
	    hourSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.HOUR_OF_DAY), 0, 23, 1));
	    minSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.MINUTE), 0, 59, 1));
	    secSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.SECOND), 0, 59, 1));
	    //fadeoutSpinner = new JSpinner(new SpinnerNumberModel(3, 0, 59, 1));

	    setLayout(new GridBagLayout());
	    JSpinner[] spinners = new JSpinner[] {hourSpinner, minSpinner /*, secSpinner, fadeoutSpinner*/};
	    String[] spinnerLabels = new String[] {"Hour", "Minute" /*, "Second", "Fadeout"*/};
	    int i = 0;
	    /*
	    for (JSpinner spinner: spinners) {

		RSLabel label = new RSLabel(spinnerLabels[i]);
		label.setPreferredSize(new Dimension(((int)(parentComponent.getWidth())), (int)(parentComponent.getHeight())));
		label.setMinimumSize(new Dimension(((int)(parentComponent.getWidth())), (int)(parentComponent.getHeight())));
		add(label, getGbc((i == 3 ? 2 : 0), (i == 3 ? 0 : i)));

		spinner.setPreferredSize(new Dimension(((int)(parentComponent.getWidth()) / 2), (int)(parentComponent.getHeight())));
		spinner.setMinimumSize(new Dimension(((int)(parentComponent.getWidth()) / 2), (int)(parentComponent.getHeight())));
		add(spinner, getGbc((i == 3 ? 3 : 1), (i == 3 ? 0 : i)));

		i++;
	    }
	    */
	    RSLabel timeLabel = new RSLabel(GUIUtilities.getColoredString("Hour:Min", Color.lightGray));
	    timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    add(timeLabel, getGbc(0, 0, weightx(1.0), gfillboth()));
	    add(hourSpinner, getGbc(1, 0));
	    add(new RSLabel(" : "), getGbc(2, 0));
	    add(minSpinner, getGbc(3, 0, weighty(0.5)));

	    okButton = new RSButton("Set Start Time");
	    okButton.setActionCommand("OK");
	    okButton.addActionListener(this);
	    okButton.setPreferredSize(new Dimension(((int)(1.5 * parentComponent.getWidth())), (int)(parentComponent.getHeight())));
	    okButton.setMinimumSize(new Dimension(((int)(1.5 * parentComponent.getWidth())), (int)(parentComponent.getHeight())));
	    //add(okButton, getGbc(0, 3, gridwidth(2)));
	    add(okButton, getGbc(0, 1, gridwidth(4), weighty(0.5)));

	    /*
	    fadeOkButton = new RSButton("Set Fadeout");
	    fadeOkButton.setActionCommand("OK_FADE");
	    fadeOkButton.addActionListener(this);
	    fadeOkButton.setPreferredSize(new Dimension(((int)(1.5 * parentComponent.getWidth())), (int)(parentComponent.getHeight())));
	    fadeOkButton.setMinimumSize(new Dimension(((int)(1.5 * parentComponent.getWidth())), (int)(parentComponent.getHeight())));
	    add(fadeOkButton, getGbc(2, 3, gridwidth(2)));
	    */

	    Color bgColor = stationConfig.getColor(StationConfiguration.LOADSAVE_POPUP_COLOR);
	    setBackground(bgColor);
	    hourSpinner.setBackground(bgColor);
	    minSpinner.setBackground(bgColor);	    
	    secSpinner.setBackground(bgColor);
	    //fadeoutSpinner.setBackground(bgColor);
	    bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	    okButton.setBackground(bgColor);
	    //fadeOkButton.setBackground(bgColor);
	}

	/*
	public void setVisible(boolean b) {
	    if (b || !selectionDone)
		super.setVisible(true);
	    else
		super.setVisible(false);
	}
	*/

	protected void setSelection(boolean b) {
	    if (b) {
		selectionDone = true;
		setVisible(false);
		playlistWidget.getStartTimeButton().setPressed(false);	    
	    } else {
		selectionDone = false;
	    }
	}

	public void actionPerformed(ActionEvent e) {
	    if (e.getActionCommand().equals("OK")) {
		int numHours = ((Integer)(hourSpinner.getValue())).intValue();
		int numMin = ((Integer)(minSpinner.getValue())).intValue();
		//int numSec = ((Integer)(secSpinner.getValue())).intValue();
		int numSec = 0;
		
		if (!(playingActive || pauseActive || goliveActive || realGoliveActive)) {

		    long currentTime = timekeeper.getLocalTime() + TZ_OFFSET;
		    long schedTime = numHours * 60L * 60L * 1000L + numMin * 60L * 1000L + numSec * 1000L;
		    long millisInDay = (24L * 60L * 60L * 1000L);
		    long offset = currentTime % millisInDay;

		    playStartTime = currentTime/millisInDay;
		    playStartTime *= millisInDay;
		    playStartTime += schedTime;

		    if (schedTime < offset)
			playStartTime += millisInDay;

		    playStartTime -= TZ_OFFSET;
		    
		    logger.info("GRINS_USAGE:SET_START_TIME:time selection: " + playStartTime + ":" + 
				timekeeper.getLocalTime() + ":" + (timekeeper.getLocalTime() - playStartTime) + 
				":" + numHours + ":" + numMin + ":" + numSec);
		    //guiPlaylist.setStartTime(playStartTime);
		    setStartTimer();
		}
		updateLivePane();
		updateGUIButtons();

		setSelection(true);
	    } else if (e.getActionCommand().equals("CANCEL")) {		
		setSelection(true);
	    } else if (e.getActionCommand().equals("OK_FADE")) {
		/*
		int fadeoutSec = ((Integer)(fadeoutSpinner.getValue())).intValue();
		guiPlaylist.initFadeout(fadeoutSec * 1000);

		PlaylistWidget.FADEOUT_DURATION = fadeoutSec * 1000;
		
		setSelection(true);
		*/
	    }	    
	}

	public void raiseNullEvent() {
	    //	    cancelButton.doClick();
	    setSelection(true);
	}

    }

    public static boolean newEntry(Object obj, Object[] list) {
	for (Object listObj: list)
	    if (listObj.toString().equals(obj.toString()))
		return false;
	return true;
    }

    public class GoliveItemPopup extends JDialog implements ActionListener {
	RSButton parentButton;
	String programID;
	PlaylistWidget.PlayItemWidget playItemWidget;
	int index;

	JSpinner minSpinner, secSpinner;
	JCheckBox backgroundCheckBox;
	JButton okButton;

	public GoliveItemPopup(RSButton parentButton, final String programID, int index) {
	    super(controlPanel.cpJFrame(), false);
	    
	    this.parentButton = parentButton;
	    this.programID = programID;
	    this.index = index;

	    this.playItemWidget = guiPlaylist.getItemAtNotNull(index + 1);

	    Dimension dim = new Dimension((int)(parentButton.getWidth() * 6) + 5, (int)(parentButton.getHeight() * 3) + 5);
	    setPreferredSize(dim);

	    setLayout(new GridBagLayout());

	    RSLabel durationLabel = new RSLabel("Length:");
	    durationLabel.setOpaque(false);
	    add(durationLabel, getGbc(0, 0, gfillboth()));
	    
	    Calendar calendar = new GregorianCalendar();
	    calendar.setTimeInMillis((long)10 * 1000 - TZ_OFFSET);
	    minSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.MINUTE), 0, 59, 1));
	    secSpinner = new JSpinner(new SpinnerNumberModel(calendar.get(Calendar.SECOND), 0, 59, 1));
	    add(minSpinner, getGbc(1, 0, gfillboth()));
	    add(secSpinner, getGbc(2, 0, gfillboth()));

	    backgroundCheckBox = new JCheckBox("Play as Background");
	    add(backgroundCheckBox, getGbc(0, 1, gridwidth(3), gfillboth()));

	    okButton = new RSButton("Apply");
	    okButton.addActionListener(this);
	    add(okButton, getGbc(0, 2, gridwidth(3), gfillboth()));
	    
	    backgroundCheckBox.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			setSelected(backgroundCheckBox.isSelected());
		    }
		});

	    if (isLiveItemInsert()) {
		backgroundCheckBox.setSelected(false);
		backgroundCheckBox.setEnabled(false);
	    } else {
		if (playItemWidget != null && playItemWidget.isLiveItem())
		    setSelected(true);
		else
		    setSelected(false);
	    }

	    if (playItemWidget != null) {
		setDuration(playItemWidget.getGoliveDuration());
	    }

	    Color bgColor = stationConfig.getColor(StationConfiguration.LOADSAVE_POPUP_COLOR);
	    getRootPane().getContentPane().setBackground(bgColor);
	    backgroundCheckBox.setBackground(bgColor);
	    bgColor = stationConfig.getColor(StationConfiguration.NEXTPREV_BUTTON_COLOR);
	    okButton.setBackground(bgColor);

	    ChangeListener durationChangeListener = new ChangeListener(){
		    public void stateChanged(ChangeEvent e){
			if (isLiveItemInsert() || playItemWidget == null || playItemWidget.getMetadata() == null)
			    return;
			if (currentDuration() > playItemWidget.getMetadata().getLength()){
			    setDuration(playItemWidget.getMetadata().getLength());
			}
		    }
		};

	    KeyListener escapeListener = new KeyAdapter(){
		    public void keyPressed(KeyEvent e){
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			    closePopup();
			}
		    }
		};
	    
	    addWindowFocusListener(new WindowAdapter(){
		    public void windowLostFocus(WindowEvent e){
			closePopup();
		    }
		});
	    
	    for (JSpinner spinner : new JSpinner[] {minSpinner, secSpinner}){
		((JSpinner.NumberEditor)spinner.getEditor()).getTextField().addKeyListener(escapeListener);
		spinner.addChangeListener(durationChangeListener);
	    }

	    setUndecorated(true);
	    pack();
	}

	void setSelected(boolean value) {
	    backgroundCheckBox.setSelected(value);

	    boolean enabled = true;
	    if (!value && !isLiveItemInsert())
		enabled = false;
	    minSpinner.setEnabled(enabled);
	    secSpinner.setEnabled(enabled);
	}

	long currentDuration() {
	    long duration = ((Integer)minSpinner.getValue()) * 1000 * 60;
	    duration += ((Integer)secSpinner.getValue()) * 1000;

	    return duration;
	}

	void setDuration(long duration) {
	    minSpinner.setValue((int)((duration / 1000) / 60));
	    secSpinner.setValue((int)((duration / 1000) % 60));
	}

	void closePopup() {
	    parentButton.setPressed(false);
	    setVisible(false);
	    playlistWidget.repaintList();
	}
	
	boolean isLiveItemInsert() {
	    return programID.startsWith(PlaylistWidget.GOLIVE_ITEMID);
	}

	public void actionPerformed(ActionEvent e) {
	    parentButton.setPressed(false);
	    
	    boolean isBackgroundMusic = backgroundCheckBox.isSelected();
	    
	    PlaylistWidget.PlayItemWidget playlistItemWidget = 
		(PlaylistWidget.PlayItemWidget)guiPlaylist.getElementAt(index);
	    
	    long goliveDuration = 10 * 1000L;

	    if (isBackgroundMusic || isLiveItemInsert()) {
		goliveDuration = ((long)((Integer)(minSpinner.getValue())).intValue() * 60 +
				  (long)((Integer)(secSpinner.getValue())).intValue()) * 1000;
	    } else {
		goliveDuration = playlistItemWidget.getMetadata().getLength();
	    }

	    playlistItemWidget.setGoliveDuration(goliveDuration);

	    if (!isLiveItemInsert()) {
		playlistItemWidget.playBackgroundMusic(isBackgroundMusic);
		playlistItemWidget.isLiveItem(isBackgroundMusic);
	    }

	    playlistWidget.getListPane().setSelectedIndex(index);
	    playlistWidget.setStartTime(playStartTime, -1);
	    closePopup();
	}

	/*
	public void actionPerformed(ActionEvent e) {
	    parentButton.setPressed(false);
	    if (e.getActionCommand().equals("OK")) {

		PlaylistWidget.PlayItemWidget playlistItemWidget = 
		    (PlaylistWidget.PlayItemWidget)guiPlaylist.getElementAt(index);
		if (!programID.startsWith(PlaylistWidget.GOLIVE_ITEMID)) {
		    playlistItemWidget.playBackgroundMusic(backgroundCheckBox.isSelected());
		    playlistWidget.getListPane().setSelectedIndex(index);
		}
		playlistItemWidget.setGoliveDuration(((long)((Integer)(minSpinner.getValue())).intValue() * 60 +
						      (long)((Integer)(secSpinner.getValue())).intValue()) * 1000);
		playlistItemWidget.isLiveItem(true);
		setSelection(true);
		playlistWidget.setStartTime(playStartTime, -1);

	    } else if (e.getActionCommand().equals("RESET")) {

		PlaylistWidget.PlayItemWidget playlistItemWidget = 
		    (PlaylistWidget.PlayItemWidget)guiPlaylist.getElementAt(index);

		if (!programID.startsWith(PlaylistWidget.GOLIVE_ITEMID)) {
		    playlistItemWidget.isLiveItem(false);
		    playlistItemWidget.playBackgroundMusic(true);
		    playlistItemWidget.setGoliveDuration(playlistItemWidget.getMetadata().getLength());
		    setSelection(true);
		} else {
		    minSpinner.setValue(new Integer(0));
		    secSpinner.setValue(new Integer(10));
		}

	    
	    
	    } else if (e.getActionCommand().equals("CANCEL")) {

		setSelection(true);

	    }
	    
	}
	*/
	
	public void showPopup(JComponent c, int x, int y) {
	    Point p = c.getLocationOnScreen();

	    int popupX = p.x + x;
	    int popupY = p.y + y;

	    setLocation(popupX, popupY);
	    
	    setVisible(true);
	}
    }

}