package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.rscontroller.services.AudioService;
import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.app.providers.PreviewProvider;
import org.gramvaani.radio.app.providers.MetadataListenerCallback;
import org.gramvaani.radio.app.providers.PlayoutPreviewListenerCallback;
import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.providers.CacheListenerCallback;
import org.gramvaani.radio.app.providers.CacheObjectListener;
import org.gramvaani.radio.app.providers.MediaLibUploadProvider;
import org.gramvaani.radio.app.providers.MediaLibUploadListenerCallback;
import org.gramvaani.radio.rscontroller.pipeline.RSPipeline;
import org.gramvaani.radio.stationconfig.StationConfiguration;

import org.gramvaani.radio.medialib.Metadata;
import org.gramvaani.radio.medialib.RadioProgramMetadata;
import org.gramvaani.radio.medialib.PlayoutHistory;
import org.gramvaani.radio.medialib.FileStoreManager;

import org.gramvaani.radio.app.*;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;

import static org.gramvaani.utilities.IconUtilities.getIcon;
import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class UploadMediaWidget extends RSWidgetBase 
    implements MediaLibUploadListenerCallback, PlayoutPreviewListenerCallback, 
	       MetadataListenerCallback, CacheListenerCallback {

    RSApp rsApp;
    ControlPanel controlPanel;

    RSTitleBar titleBar;
    JFileChooser fileChooser;
    FileNameExtensionFilter filter;
    JProgressBar progressBar;
    RSLabel aspStatusLabel;
    
    UploadMediaModel listModel;
    JList listPane;
    JScrollPane scrollPane;
    UploadCellRenderer cellRenderer;
    Dimension cellSize;
    QuickSearch quickSearch;
    int count = 0;
    int focusedIndex = -1;

    protected int selectedIndex = -1;

    MetadataProvider metadataProvider;
    PreviewProvider previewProvider;
    CacheProvider cacheProvider;
    MediaLibUploadProvider uploadProvider;
    StationConfiguration stationConfig;
    ErrorInference errorInference;

    boolean metadataProviderActive = false;
    boolean previewProviderActive  = false;
    boolean cacheProviderActive    = false;
    boolean uploadProviderActive   = false;

    static int fileChooserVertFrac = 47;
    static int progressVertFrac = 3;

    public static final String[] SUPPORTED_EXTNS = FileUtilities.SUPPORTED_EXTNS;

    HashSet<RadioProgramMetadata> duplicates = new HashSet<RadioProgramMetadata>();

    public UploadMediaWidget(RSApp rsApp, ControlPanel controlPanel){
	super("Upload Audio");
	this.rsApp = rsApp;
	this.controlPanel = controlPanel;

	wpComponent = new JPanel();
	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.UPLOAD_ICON;
	bmpToolTip = "Upload Audio";

	metadataProvider = (MetadataProvider)(rsApp.getProvider(RSApp.METADATA_PROVIDER));
	previewProvider  = (PreviewProvider)(rsApp.getProvider(RSApp.PREVIEW_PROVIDER));
	cacheProvider    = (CacheProvider) (rsApp.getProvider(RSApp.CACHE_PROVIDER));
	uploadProvider   = (MediaLibUploadProvider) rsApp.getProvider(RSApp.MEDIALIB_UPLOAD_PROVIDER);

	stationConfig = rsApp.getStationConfiguration();
	errorInference = ErrorInference.getErrorInference();
    }

    public boolean onLaunch(){
	Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);
	Color componentBgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);

	wpComponent.setLayout(new BoxLayout(wpComponent, BoxLayout.PAGE_AXIS));
	titleBar = new RSTitleBar(name, (int)wpSize.getWidth());
	wpComponent.add(titleBar);

	titleBar.setBackground(bgColor);
	titleBar.setOpaque(true);

	fileChooser = new JFileChooser(stationConfig.getUserDesktopPath()){
		public void approveSelection(){
		    uploadSelection();
		    setListFocused();
		}
		
		public void cancelSelection(){
		    cancelUpload();
		}

		public void setEnabled(boolean enabled){
		    super.setEnabled(enabled);
		    Container buttonPanel = (Container)((Container)getComponent(3)).getComponent(3);
		    for (Component c: buttonPanel.getComponents())
		    	c.setEnabled(enabled);
		    
		}
	    };
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setApproveButtonText("Upload");
	fileChooser.setMultiSelectionEnabled(true);
	//fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

	filter = new FileNameExtensionFilter("Audio Files", SUPPORTED_EXTNS);
	fileChooser.setFileFilter(filter);

	fileChooser.setBackground(componentBgColor);
	fileChooser.setForeground(componentBgColor);

	Dimension fileChooserSize = new Dimension(wpSize.width, wpSize.height*fileChooserVertFrac/100);
	fileChooser.setPreferredSize(fileChooserSize);
	fileChooser.setMaximumSize(fileChooserSize);

	wpComponent.add(fileChooser);

	Dimension progressSize = new Dimension(wpSize.width, wpSize.height*progressVertFrac/100);

	progressBar = new JProgressBar();
	progressBar.setPreferredSize(progressSize);
	progressBar.setMaximumSize(progressSize);
	progressBar.setSize(progressSize);
	progressBar.setMaximum(100);

	progressBar.setString("");
	progressBar.setStringPainted(true);
	progressBar.setBackground(bgColor);
	progressBar.setForeground(Color.white);

	wpComponent.add(progressBar);
	
	listModel = new UploadMediaModel();
	listPane = new JList(listModel);

	scrollPane = new JScrollPane(listPane);
	scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

	listPane.setBackground(componentBgColor);
	scrollPane.setBackground(componentBgColor);

	int numEntriesPerScreen = 8;
	cellSize = new Dimension(wpSize.width - scrollPane.getVerticalScrollBar().getMaximumSize().width - 5, 
						 (wpSize.height - titleBar.HEIGHT - fileChooserSize.height - progressSize.height)/numEntriesPerScreen);
	
	final UploadCellRenderer cellRenderer = new UploadCellRenderer(cellSize, this);
	this.cellRenderer = cellRenderer;

	listPane.setCellRenderer(new ListCellRenderer(){
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasCellFocus){
		    cellRenderer.setCell(list, value, isSelected, index);
		    return cellRenderer;
		}
	    });
	
	listPane.addKeyListener(new KeyAdapter(){
		public void keyTyped(KeyEvent e){
		    if (e.getModifiers() > 0)
			return;
		    quickSearch.keyTyped(e.getKeyChar());
		}

		public void keyPressed(KeyEvent e){
		    int keyCode = e.getKeyCode();
		    switch (keyCode){
		    case KeyEvent.VK_RIGHT:
			showNextItem();
			break;
		    case KeyEvent.VK_LEFT:
			showPrevItem();
			break;
		    }
		}
	    });

	listPane.addListSelectionListener(new ListSelectionListener(){
		public void valueChanged(ListSelectionEvent e){
		    if (!e.getValueIsAdjusting())
			setNewSelection(listPane.getSelectedIndices());
		}
	    });

	listPane.addMouseListener(new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    int index = e.getY()/cellSize.height;

		    if (index >= listModel.size())
			return;

		    selectedIndex = index;

		    cellRenderer.mouseClicked(e, (RadioProgramMetadata)listPane.getSelectedValue());
		    listPane.repaint();
		}

		public void mousePressed(MouseEvent e){
		    int index = e.getY()/cellSize.height;

		    if (index >= listModel.size())
			return;

		    selectedIndex = index;

		    cellRenderer.mousePressed(e, (RadioProgramMetadata)listPane.getSelectedValue());
		    listPane.repaint();
		}
		
		public void mouseReleased(MouseEvent e){
		    int index = e.getY()/cellSize.height;

		    if (index >= listModel.size())
			return;

		    selectedIndex = index;

		    cellRenderer.mouseReleased(e, (RadioProgramMetadata)listPane.getSelectedValue());
		    listPane.repaint();
		}

	    });
	
	listPane.setDragEnabled(true);

	TransferHandler transferHandler = new TransferHandler(){
		public boolean canImport(TransferHandler.TransferSupport support){
		    if (!(cacheProviderActive && uploadProviderActive))
			return false;
		    DataFlavor uriFlavor = null;
		    DataFlavor fileListFlavor = null;

		    try{
			 uriFlavor = new DataFlavor("text/uri-list;class=java.lang.String");
			 fileListFlavor = new DataFlavor("application/x-java-file-list;class=java.util.List");
		    } catch (Exception e){
			logger.error("TransferHandler: CanImport: Exception.", e);
		    }
		    
		    if (support.isDataFlavorSupported(uriFlavor) || support.isDataFlavorSupported(fileListFlavor))
			return true;

		    return false;
		}

		public boolean importData(TransferHandler.TransferSupport info){
		    if (!(cacheProviderActive && uploadProviderActive))
			return false;
		    if (!info.isDrop())
			return false;
		    Transferable t = info.getTransferable();

		    DataFlavor fileListFlavor = null;
		
		    List fileList = null;
		    ArrayList<String> fileURLs = new ArrayList<String>();
		    boolean uriFound = false;

		    if (info.isDataFlavorSupported(DataFlavor.javaFileListFlavor)){
			try{
			    fileList = (List)t.getTransferData(DataFlavor.javaFileListFlavor);
			    Iterator iter = fileList.iterator();
			    int numFiles = fileList.size();
			    setStatus(numFiles + " file(s) remaining.");
			    while (iter.hasNext()){
				File file = (File) iter.next();
				if (isSupportedFile(file.getName())){
				    uploadProvider.uploadFile(file.getCanonicalPath(), getName());
				    numFiles--;
				} else {
				    logger.error("ImportData: Unsupported file extension: "+file.getName());
				    uploadError(file.getName(), MediaLibUploadProvider.ERROR_NOT_SUPPORTED, --numFiles);
				}
			    }

			    return true;
			} catch (Exception e){
			    logger.error("ImportData: Exception.", e);
			    return false;
			}
		    }

		    DataFlavor uriFlavor = null;
		    String uriList = null;

		    try{
			uriFlavor = new DataFlavor("text/uri-list;class=java.lang.String");
			uriList = (String) t.getTransferData(uriFlavor);
		    } catch (Exception e){
			logger.error("TransferHandler: ", e);
		    }
		    
		    String[] uris = uriList.split("\n");
		    int numFiles = uris.length;

		    setStatus(numFiles + " file(s) remaining.");

		    for (String uri: uris){
			try{
			    String fileName = (new URI(uri.trim())).toURL().getFile();
			    String decodedName = URLDecoder.decode(fileName, "UTF-8");
			    if (isSupportedFile(decodedName)){
				uploadProvider.uploadFile(decodedName, getName());
				numFiles--;
			    } else {
				logger.error("ImportData: Unsupported file extension: "+decodedName);
				uploadError(fileName, MediaLibUploadProvider.ERROR_NOT_SUPPORTED, --numFiles);
			    }
			} catch (Exception e){
			    logger.error("TransferHandler: ", e);
			}
		    }
		    
		    return true;
		}
		
		public int getSourceActions(JComponent c){
		    return COPY;
		}
		
		public Transferable createTransferable (JComponent c){
		    ArrayList<RadioProgramMetadata> list = new ArrayList<RadioProgramMetadata>();
		    RadioProgramMetadata rpm;
		    int[] selectionIndices = ((JList)c).getSelectedIndices();
		    
		    for (int i: selectionIndices){
			rpm = (RadioProgramMetadata)listModel.getElementAt(i);
			logger.info("GRINS_USAGE:UPLOAD_MEDIA_DRAG:" + rpm.getItemID());
			list.add(rpm);
		    }
		    
		    return new ProgramSelection(list);
		}
	    };

	listPane.setTransferHandler(transferHandler);
	
	quickSearch = new QuickSearch(new QuickSearch.QuickSearchListener(){
		public void matchedIndices(HashSet<Integer> indices){
		    selectIndices(indices);
		}
	    });

	count = 0;

	previewProvider.registerWithProvider(this);
	metadataProvider.registerWithProvider(this);
	cacheProvider.registerWithProvider(this);
	uploadProvider.registerWithProvider(this);

	if (previewProvider == null || !previewProvider.isActive())
	    previewProviderActive = false;
	else
	    previewProviderActive = true;

	if (metadataProvider == null || !metadataProvider.isActive())
	    metadataProviderActive = false;
	else
	    metadataProviderActive = true;

	if (cacheProvider == null || !cacheProvider.isActive())
	    cacheProviderActive = false;
	else
	    cacheProviderActive = true;
	
	if (uploadProvider == null || !uploadProvider.isActive())
	    uploadProviderActive = false;
	else
	    uploadProviderActive = true;

	activateUpload();
	cellRenderer.activateInfo(metadataProviderActive);
	cellRenderer.activatePreview(previewProviderActive);
	
	wpComponent.add(scrollPane);
	wpComponent.validate();

	buildASPComponent();
	return true;
    }

    protected boolean isSupportedFile (String fileName) {
	return FileUtilities.isSupported(fileName);
    }

    protected void setListFocused(){
	listPane.requestFocusInWindow();
    }

    protected void setNewSelection(final int selection[]){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    if (selection.length > 0)
			makeVisible(selection[0]);
		}
	    });
    }

    protected void showNextItem(){
	int index[] = listPane.getSelectedIndices();
	
	int next, i = 0;
	for (i = 0; i < index.length; i++)
	    if (index[i] == focusedIndex)
		break;
	
	if (i == index.length - 1)
	    next = index[0];
	else
	    next = index[i + 1];

	makeVisible(next);
    }

    protected void showPrevItem(){
	int index[] = listPane.getSelectedIndices();
	
	int next, i = 0;
	for (i = 0; i < index.length; i++)
	    if (index[i] == focusedIndex)
		break;
	
	if (i == 0)
	    next = index[index.length - 1];
	else
	    next = index[i - 1];

	makeVisible(next);

    }

    protected void makeVisible(int index){
	focusedIndex = index;
	/*
	Point p = listPane.indexToLocation(focusedIndex);
	JViewport viewport = scrollPane.getViewport();
	Dimension viewSize = scrollPane.getSize();
	Point currentPosition = viewport.getViewPosition();

	if (p.getY() > currentPosition.getY() + viewSize.height - cellSize.height ||
	    p.getY() < currentPosition.getY()){

	    if (p.getY() < listPane.size().height - viewSize.height/2)
		p.translate(0, -viewSize.height/2);
	    else
		p = new Point ((int)p.getX(), listPane.size().height - viewSize.height);
	    
	    viewport.setViewPosition(p);
	}
	*/
	listPane.ensureIndexIsVisible(focusedIndex);

    }

    protected void buildASPComponent(){
	aspComponent.setLayout(new GridBagLayout());
	
	aspStatusLabel = new RSLabel("");
	
	aspComponent.add(aspStatusLabel, getGbc(0, 0, gfillboth()));
    }

    protected void setStatus(String text){
	aspStatusLabel.setText(text);
	aspComponent.validate();
	aspComponent.repaint();
    }

    public void onMaximize(){

    }

    public void onMinimize(){

    }

    public void onUnload(){
	wpComponent.removeAll();

	previewProvider.unregisterWithProvider(getName());
	metadataProvider.unregisterWithProvider(getName());
	cacheProvider.unregisterWithProvider(getName());
	uploadProvider.unregisterWithProvider(getName());

	duplicates.clear();
    }

    protected synchronized void uploadSelection(){
	File[] files = fileChooser.getSelectedFiles();
	ArrayList<File> list = new ArrayList<File>();
	getUploadableFiles(files, list);

	setStatus(list.size() + " file(s) remaining.");

	for (File file: list){
	    try{
		uploadProvider.uploadFile(file.getCanonicalPath(), name);
	    } catch (Exception e){
		logger.error("UploadSelection: Encountered exception: ", e);
	    }
	}
    }

    protected void getUploadableFiles(File[] files, ArrayList<File> list){
	for (File file: files){
	    if (file.isFile()){
		if (filter.accept(file))
		    list.add(file);
	    } else if (file.isDirectory()) {
		File[] nested = file.listFiles();
		getUploadableFiles(nested, list);
	    }
	}
    }

    protected synchronized void cancelUpload(){
	uploadProvider.cancelUpload();
    }

    protected void selectIndices(HashSet<Integer> list){
	final int selection[] = new int[list.size()];
	int size = listPane.getModel().getSize();
	
	int i = 0;
	for (int index: list){
	    selection[i] = size - index - 1;
	    i++;
	}
	
	listPane.setSelectedIndices(selection);
    }

    protected void startPreview(RadioProgramMetadata program){
	String programID = program.getItemID();
	
	logger.debug("StartPreview: "+programID);

	if (previewProviderActive && previewProvider.isPlayable()){
	    if (previewProvider.play(programID, this) < 0)
		errorInference.displayPlaylistError("Unable to send preview-start command", new String[] {PlaylistController.UNABLE_TO_SEND_COMMAND});
	}
    }

    protected void stopPreview(RadioProgramMetadata program){
	String programID = program.getItemID();
	
	logger.debug("StopPreview: "+programID);
	
	if (previewProviderActive && previewProvider.isStoppable(programID, this)){
	    if (previewProvider.stop(programID, this) < 0)
		errorInference.displayPlaylistError("Unable to send preview-stop command", new String[]{PlaylistController.UNABLE_TO_SEND_COMMAND});
	}
    }
    
    protected void launchMetadataWidget(RadioProgramMetadata program){
	if (metadataProviderActive) {
	    String programID = program.getItemID();
	    MetadataWidget metadataWidget = MetadataWidget.getMetadataWidget(rsApp, new String[]{programID}, false);
	    if (!metadataWidget.isLaunched()) {
		controlPanel.initWidget(metadataWidget, false);
		controlPanel.cpJFrame().launchWidget(metadataWidget);
	    } else {
		controlPanel.cpJFrame().setWidgetMaximized(metadataWidget);
	    }
	}
    }
    
    protected void activateUpload(){
	if (cacheProviderActive && uploadProviderActive)
	    fileChooser.setEnabled(true);
	else{
	    fileChooser.setEnabled(false);
	}
    }


    /* Provider Callbacks */

    public synchronized void activateMediaLibUpload(boolean state, String[] error){
	uploadProviderActive = state;
	activateUpload();
    }

    String uploadingFileName = null;

    public synchronized void uploadSuccess(String fileName, String libFileName, int numRemaining, boolean isDuplicate){
	RadioProgramMetadata query = new RadioProgramMetadata(libFileName);
	RadioProgramMetadata program =  metadataProvider.getSingle(query);

	if (program == null){
	    logger.error("UploadSuccess: Could not locate file in database.");
	    return;
	}

	logger.info("GRINS_USAGE:FILE_UPLOADED:"+fileName+":"+libFileName+":"+numRemaining+":"+isDuplicate);

	if (numRemaining > 0)
	    setStatus(numRemaining+" file(s) remaining.");
	else
	    setStatus("");

	for (Object obj: listModel.toArray()){
	    if (((RadioProgramMetadata) obj).getItemID().equals(program.getItemID()))
		return;
	}

	cacheProvider.addCacheObjectListener(program, listModel);
	listModel.add(0, program);
	if (isDuplicate)
	    duplicates.add(program);
	progressBar.setString("Uploaded file "+fileName);
	progressBar.setValue(0);
	uploadingFileName = null;

	quickSearch.addString(program.getTitle(), count++);
    }

    public void updateProgress(String fileName, long uploadedLength, long totalLength){
	int uploadedPercentage = (int)(1.0*uploadedLength/totalLength * 100.0);
	progressBar.setString("Uploading file "+fileName+" "+uploadedPercentage+"%");
	progressBar.setValue(uploadedPercentage);
	progressBar.repaint();
    }
    
    public synchronized void uploadError(String fileName, String error, int numRemaining){
	errorInference.displayUploadError("Upload Error: "+fileName, new String[] {error});
	progressBar.setString("Could not upload file "+fileName);
	if (numRemaining > 0)
	    setStatus(numRemaining+" file(s) remaining.");
	else
	    setStatus("");

	uploadingFileName = null;
    }

    //Call back from provider. Not used here
    public void uploadGiveup(String fileName) {
    }

    public synchronized void unknownError(){
	errorInference.displayUploadError("Upload Error", new String[] {MediaLibUploadProvider.ERROR_UNKNOWN});
    }

    public synchronized void playActive(boolean success, String programID, String playType, long telecastTime, String[] error){
	if (success){
	    cellRenderer.playActive(programID);
	    listPane.repaint();
	}
	else{
	    logger.error("PlayActive: Provider unable to play.");
	    errorInference.displayServiceError("Unable to preview", error);
	}
    }

    public synchronized void pauseActive(boolean success, String programID, String playType, String[] error){

    }

    public synchronized void stopActive(boolean success, String programID, String playType, String[] error){
	if (success){
	    cellRenderer.stopActive(programID);
	    listPane.repaint();
	}
	else{
	    logger.error("StopActive: Provider unable to stop.");
	    errorInference.displayServiceError("Unable to stop", error);
	}
    }

    public synchronized void playDone(boolean success, String programID, String playType){
	if (success){
	    cellRenderer.stopActive(programID);
	    listPane.repaint();
	}
	else
	    errorInference.displayServiceError("Error in playing", new String[] {});
    }

    protected String getErrorMessage(String error){
	if (error.equals(RSPipeline.ERROR_NO_DATA))
	    return "File does not contain any data.";
	else if (error.equals(RSPipeline.ERROR_DEVICE_BUSY))
	    return "Another application is using the soundcard currently.";
	else if (error.equals(RSPipeline.ERROR_DEVICE_CANT_OPEN))
	    return "Unable to use the soundcard in current configuration.";
	else if (error.equals(RSPipeline.ERROR_UNKNOWN_FORMAT))
	    return "Unknown file format.";

	return error;
    }

    public void audioGstError(String programID, String error, String playType){
	errorInference.displayGstError("Audio Error", error);
    }

    public void playoutPreviewError(String errorCode, String[] errorParams) {
	logger.error("playoutPreviewError: " + errorCode);
    }

    public void uploadProviderError(String errorCode, String[] errorParams) {
	
    }

    public synchronized void activateAudio(boolean activate, String playType, String[] error){
	if (playType.equals(AudioService.PREVIEW)){
	    previewProviderActive = activate;
	    cellRenderer.activatePreview(activate);
	    listPane.repaint();
	}
    }

    public synchronized void enableAudioUI(boolean enable, String playType, String[] error){
	if (playType.equals(AudioService.PREVIEW)){
	    cellRenderer.enablePreviewUI(enable);
	    listPane.repaint();
	}
    }
    
    /*
    protected void displayError(String error){
	logger.error(error);
    }
    */

    public synchronized void activateMetadataService(boolean activate, String[] error){
	metadataProviderActive = activate;
	cellRenderer.activateInfo(activate);
	listPane.repaint();
    }

    public synchronized void activateCache(boolean activate, String[] error){
	cacheProviderActive = activate;
	activateUpload();
    }
    
    public void cacheObjectsDeleted(String objectType, String[] objectID){}
    public void cacheObjectsUpdated(String objectType, String[] objectID){}
    public void cacheObjectsInserted(String objectType, String[] objectID){}


    class UploadMediaModel extends DefaultListModel implements CacheObjectListener {
	public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKey){
	    for (int j = 0; j < metadata.length; j++) {
		RadioProgramMetadata program = (RadioProgramMetadata) metadata[j];
		String newItemID = newKey[j].substring(0, newKey[j].lastIndexOf("."));
		RadioProgramMetadata dummyProgram, newProgram=null;
		Metadata mdata;
		if (!newItemID.equals("")) {
		    dummyProgram = RadioProgramMetadata.getDummyObject(newItemID);
		    mdata = cacheProvider.getSingle(dummyProgram);
		    if (mdata == null)
			return;
		    newProgram = (RadioProgramMetadata)mdata;
		}
		
		for (int i = 0; i < getSize(); i++){
		    RadioProgramMetadata item = (RadioProgramMetadata) getElementAt(i);
		    if (program.getItemID().equals(item.getItemID())) {
			if (!newItemID.equals("")) {
			    setElementAt(newProgram, i);
			    break;
			} else {
			    removeElementAt(i);
			    break;
			}
		    }
		}
	    }
	}

	public void cacheObjectValueChanged(Metadata metadata){
	    RadioProgramMetadata program = (RadioProgramMetadata) metadata;
	    
	    for (int i = 0; i < getSize(); i++){
		RadioProgramMetadata item = (RadioProgramMetadata) getElementAt(i);
		if (program.getItemID().equals(item.getItemID())){
		    setElementAt(program, i);
		}
	    }
	    
	}

    }

    class UploadCellRenderer extends JPanel {
	Dimension cellSize;
	JLabel titleLabel;
	JLabel typeLabel;	
	Dimension typeLabelSize;

	Color bgColor;
	//final int titleHorizFrac = 89;
	//final int playoutTimeHorizFrac = 28;
	final int buttonHorizFrac = 22;

	RSButton selectedButton = null;
	RadioProgramMetadata playingItem = null, infoItem = null;
	RSButton infoButton, previewButton, stopButton;

	boolean isMousePressed = false;
	boolean isPlayActive = false;
	boolean isPreviewEnabled = true;

	int playingIndex = -1;
	int infoIndex = -1;

	Color selectedForeground = new Color(10, 24, 225);

	public UploadCellRenderer(Dimension cellSize, final UploadMediaWidget widget){
	    super();
	    this.cellSize = cellSize;
	    setPreferredSize(cellSize);
	    setMinimumSize(cellSize);
	    setSize(cellSize);
	    setMaximumSize(cellSize);

	    setLayout(new GridBagLayout());
	    
	    titleLabel = new RSLabel("");
	    titleLabel.setForeground(stationConfig.getColor(StationConfiguration.LISTITEM_TITLE_COLOR));

	    Dimension titleSize = new Dimension(cellSize.width*(100 - buttonHorizFrac)/100, cellSize.height);
	    titleLabel.setSize(titleSize);
	    titleLabel.setMinimumSize(titleSize);
	    add(titleLabel, getGbc(0, 0, gridheight(2), weightx(1.0), gfillboth()));

	    
	    Dimension buttonSize = new Dimension(cellSize.width*(buttonHorizFrac)/(4*100), 
						 (int)(cellSize.height * 0.60));

	    typeLabelSize = buttonSize;
	    typeLabel = new RSLabel();
	    typeLabel.setMinimumSize(typeLabelSize);
	    typeLabel.setPreferredSize(typeLabelSize);
	    add(typeLabel, getGbc(1, 1));

	    Dimension previewButtonSize = buttonSize;

	    previewButton = new RSButton("");
	    previewButton.setSize(previewButtonSize);
	    previewButton.setMinimumSize(previewButtonSize);
	    previewButton.setPreferredSize(previewButtonSize);
	    previewButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (playingItem != null)
			    widget.startPreview(playingItem);
		    }
		});
	    previewButton.setToolTipText("Preview");
	    add(previewButton, getGbc(2, 1));

	    //Dimension stopButtonSize = new Dimension(infoButtonSize.width - previewButtonSize.width, previewButtonSize.height);
	    Dimension stopButtonSize = buttonSize;

	    stopButton = new RSButton ("");
	    stopButton.setSize(stopButtonSize);
	    stopButton.setMinimumSize(stopButtonSize);
	    stopButton.setPreferredSize(stopButtonSize);
	    stopButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (playingItem != null)
			    widget.stopPreview(playingItem);
		    }
		});
	    stopButton.setToolTipText("Stop");
	    add(stopButton, getGbc(3, 1));

	    infoButton = new RSButton("");
	    infoButton.setSize(previewButtonSize);
	    infoButton.setMinimumSize(previewButtonSize);
	    infoButton.setPreferredSize(previewButtonSize);
	    infoButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (infoItem != null)
			    widget.launchMetadataWidget(infoItem);
		    }
		});
	    infoButton.setToolTipText("Info");
	    add(infoButton, getGbc(4, 1));



	    bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_COLOR);
	    setBackground(bgColor);
	    
	    Color buttonBgColor = stationConfig.getColor(StationConfiguration.SEARCHCLEAR_BUTTON_COLOR);
	    infoButton.setBackground(buttonBgColor);
	    previewButton.setBackground(buttonBgColor);
	    stopButton.setBackground(buttonBgColor);

	    infoButton.setIcon(getIcon(StationConfiguration.INFO_ICON, (int)(buttonSize.getWidth()), 
						     (int)(buttonSize.getHeight())));
	    previewButton.setIcon(getIcon(StationConfiguration.PREVIEW_ICON, (int)(previewButtonSize.getWidth()), 
							(int)(previewButtonSize.getHeight())));
	    stopButton.setIcon(getIcon(StationConfiguration.PREVIEW_STOP_ICON, (int)(stopButtonSize.getWidth()), 
						     (int)(stopButtonSize.getHeight())));
   
	    setBorder(new GradientBorder(stationConfig.getColor(StationConfiguration.LIST_ITEM_BORDER_COLOR)));
	}

	public void activatePreview(boolean activate){
	    enablePreviewUI(activate);
	    if (!activate)
		stopActive(null);
	}

	public void enablePreviewUI(boolean enable){
	    isPreviewEnabled = enable;
	    previewButton.setEnabled(enable);
	    stopButton.setEnabled(enable);
	}

	public void activateInfo(boolean activate){
	    infoButton.setEnabled(activate);
	}

	public void setCell(JList list, Object value, boolean isSelected, int index){
	    RadioProgramMetadata program = (RadioProgramMetadata) value;
	    
	    titleLabel.setText(" "+program.getTitle());
	    titleLabel.setToolTipText(program.getTitle());

	    if (isSelected) {
		setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_LISTITEM_SELECT_COLOR));
	    } else if (duplicates.contains(program)){
		setBackground(stationConfig.getColor(StationConfiguration.UPLOADLIST_DUPLICATE_COLOR));
	    } else {
		setBackground(bgColor);
	    }
	    
	    typeLabel.setIcon(GUIUtilities.getIcon(program, typeLabelSize.width, typeLabelSize.height));
	    
	    previewButton.setPressed(false);
	    stopButton.setPressed(false);
	    infoButton.setPressed(false);

	    if (program.getPreviewStoreAttempts() != FileStoreManager.UPLOAD_DONE){
		previewButton.setEnabled(false);
		stopButton.setEnabled(false);
	    } else if (isPreviewEnabled) {
		previewButton.setEnabled(true);
		stopButton.setEnabled(true);
	    }

	    if (isMousePressed){
		if (selectedButton == infoButton && index == infoIndex)
		    infoButton.setPressed(true);
		if (selectedButton == stopButton && index == playingIndex)
		    stopButton.setPressed(true);
		if (selectedButton == previewButton && index == playingIndex)
		    previewButton.setPressed(true);
	    }
	    
	    if (isPlayActive && index == playingIndex)
		previewButton.setPressed(true);
	    
	}

	protected RSButton getButtonAt(Point p){
	    setLocation(0, 0);
	    setSize(cellSize);

	    int y = (int)p.getY()%(int)getPreferredSize().getHeight();
	    int x = (int)p.getX();
		    
	    Component button = getComponentAt(x, y);
	    if (button instanceof RSButton)
		return ((RSButton)button);
	    else
		return null;
	}

	public void mouseClicked(MouseEvent e, RadioProgramMetadata program){
	    if (selectedButton != null)
		selectedButton.doClick();
	    else if (e.getClickCount() == 2){
		infoItem = program;
		infoButton.doClick();
	    }
	}

	public void mousePressed(MouseEvent e, RadioProgramMetadata program){
	    RSButton tmp = getButtonAt(e.getPoint());
	    if ((tmp == null) ||
	       (isPlayActive && tmp == previewButton) ||
	       (!isPlayActive && tmp == stopButton) ||
	       (isPlayActive && tmp == stopButton && selectedIndex != playingIndex)){
		selectedButton = null;
		return;
	    }

	    isMousePressed = true;

	    if (tmp == previewButton){
		playingIndex = selectedIndex;
		playingItem  = program;
	    } else if (tmp == infoButton) {
		infoIndex = selectedIndex;
		infoItem  = program;
	    }

	    selectedButton = tmp;

	}

	public void mouseReleased(MouseEvent e, RadioProgramMetadata program){
	    isMousePressed = false;
	}

	public void playActive(String programID){
	    isPlayActive = true;
	}

	public void stopActive(String programID){
	    isPlayActive = false;
	}

    }
    
}
