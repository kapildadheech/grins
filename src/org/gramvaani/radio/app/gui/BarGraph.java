package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.*;

import java.awt.*;
import java.awt.geom.*;

public class BarGraph {
    static int YTICS = 10;

    static int TOPPAD = 20;
    
    static int YTICK_MIN_PIXELS = 30;

    String title;
    long xMin, xMax;
    String[] xLabels;
    long[] yDataItems;
    long binInterval;
    long maxDataItem;
    String[] infoStrings;

    int canvasWidth, canvasHeight;
    int yTics;
    
    int titleVertFrac = 0;
    int infoVertFrac = 0;
    int ticsVertFrac = 5;
    int labelVertFrac = 20;
    int yPadVertFrac = 16;
    int graphVertFrac = 100 - titleVertFrac - infoVertFrac - yPadVertFrac - ticsVertFrac - labelVertFrac;

    int xPadHorizFrac = 2;
    int ticsHorizFrac = 2;
    int labelHorizFrac = 5;
    int graphHorizFrac = 100 - 2 * xPadHorizFrac - ticsHorizFrac - labelHorizFrac;

    StationConfiguration stationConfig;
    Dimension size;
    Color bgColor, axisColor, dataItemColor, graphColor;
    LinearGradientPaint gradient;

    public BarGraph(String title, long xMin, long xMax, String[] xLabels, long[] yDataItems, 
		    long binInterval, long maxDataItem, String[] infoStrings, Dimension displayDim, int yTics,
		    StationConfiguration stationConfig) {
	this.title = title;
	this.xMin = xMin;
	this.xMax = xMax;
	this.xLabels = xLabels;
	this.yDataItems = yDataItems;
	this.binInterval = binInterval;
	this.maxDataItem = maxDataItem;
	this.infoStrings = infoStrings;

	this.stationConfig = stationConfig;
	
	this.canvasWidth = (int)(displayDim.getWidth());
	this.canvasHeight = (int)(displayDim.getHeight()) - TOPPAD;
	
	size = displayDim;

	if(!title.equals("")) {
	    titleVertFrac = 10;
	    yPadVertFrac -= 10;
	}

	if (!(infoStrings.length == 0)) {
	    infoVertFrac = 10;
	    labelVertFrac -= 10;
	}

	if (yTics == -1) {
	    this.yTics = YTICS;
	} else {
	    this.yTics = yTics;
	}

	bgColor = Color.white;
	axisColor = Color.gray;
	dataItemColor = Color.gray;
	graphColor = stationConfig.getColor(StationConfiguration.GRAPH_COLOR);
	int yPadVert = (int)(canvasHeight * yPadVertFrac / 100.0);
	int graphVert = (int)(canvasHeight * graphVertFrac / 100.0);
	gradient = new LinearGradientPaint(0f, 0.0f, (float)yPadVert, (float)(yPadVert + graphVert), 
					   new float[] {0.0f, 1.0f}, 
					   new Color[] {graphColor.brighter().brighter(), graphColor.darker().darker()});
    }
    
    public void drawGraph(Graphics2D g, FontMetrics fm) {
	int titleVert = (int)(canvasHeight * titleVertFrac / 100.0);
	int infoVert = (int)(canvasHeight * infoVertFrac / 100.0);
	int ticsVert = (int)(canvasHeight * ticsVertFrac / 100.0);
	int labelVert = (int)(canvasHeight * labelVertFrac / 100.0);
	int yPadVert = (int)(canvasHeight * yPadVertFrac / 100.0);
	int graphVert = (int)(canvasHeight * graphVertFrac / 100.0);

	int xPadHoriz = (int)(canvasWidth * xPadHorizFrac / 100.0);
	int ticsHoriz = (int)(canvasWidth * ticsHorizFrac / 100.0);
	int labelHoriz = (int)(canvasWidth * labelHorizFrac / 100.0);
	int graphHoriz = (int)(canvasWidth * graphHorizFrac / 100.0);

	int graphTopX = xPadHoriz + ticsHoriz + labelHoriz;
	int graphTopY = (titleVert > 0 ? yPadVert : TOPPAD) + titleVert;

	double xScale = (xMax - xMin) / graphHoriz;
	//double yScale = (double)maxDataItem / graphVert;

	if (xScale == 0)
	    return;

	//Graph background
	g.setColor(bgColor);
	g.fillRect(0, 0, size.width, size.height);

	
	//Title
	g.setColor(Color.black);
	Font serifFont = new Font("Serif", Font.BOLD, 14);
	Font currentFont = g.getFont();
	g.setFont(serifFont);
	if (!title.equals(""))
	    g.drawString(title, (canvasWidth - fm.stringWidth(title)) / 2, yPadVert);
	g.setFont(currentFont);

	//Summary Info at bottom
	if (infoStrings.length != 0) {
	    int j = 0;
	    for (String infoString: infoStrings) {
		g.drawString(infoString, graphTopX, graphTopY + graphVert + labelVert + (int)(j++ * fm.getHeight() * 1.3) + 2 * yPadVert);
	    }
	}


	//Axes
	g.setColor(axisColor);
	g.drawLine(graphTopX, graphTopY, graphTopX, graphTopY + graphVert);
	g.drawLine(graphTopX, graphTopY + graphVert, graphTopX + graphHoriz, graphTopY + graphVert);


	//Xlabels
	for (int i = 0; i < xLabels.length; i++) {
	    if (xLabels[i].length() <= 6) {
		g.setColor(Color.black);
		g.drawString(xLabels[i], (int)(graphTopX + i * binInterval  / xScale - fm.stringWidth(xLabels[i]) / 2), 
			     graphTopY + graphVert + ticsVert + yPadVert);
	    } else {
		String[] xLabelArr = xLabels[i].split(" ");
		String xLabel;
		if (xLabelArr.length >= 2)
		    xLabel = xLabelArr[0] + " " + xLabelArr[1];
		else
		    xLabel = xLabelArr[0];
		g.setColor(Color.black);
		g.drawString(xLabel, 
			     (int)(graphTopX + i * binInterval  / xScale - fm.stringWidth(xLabel) / 2), 
			     graphTopY + graphVert + ticsVert + yPadVert);

		if (xLabelArr.length >= 2) {
		    StringBuilder str = new StringBuilder();
		    for (int j = 2; j < xLabelArr.length; j++) {
			str.append(xLabelArr[j]);
			str.append(" ");
		    }
		    g.setColor(Color.black);

		    if (title.equals("")) {
			g.drawString(str.toString(), 
				     (int)(graphTopX + i * binInterval  / xScale - fm.stringWidth(xLabel) / 2), 
				     graphTopY + graphVert + ticsVert + yPadVert + (int)(fm.getHeight() * 1.025));
		    } else {
			g.drawString(str.toString(), 
				     (int)(graphTopX + i * binInterval  / xScale - fm.stringWidth(xLabel) / 2), 
				     graphTopY + graphVert + ticsVert + yPadVert + (int)(fm.getHeight() * 1.15));
		    }
		}
	    }
	    g.setColor(Color.blue);
	    g.drawLine((int)(graphTopX + (i + 1) * binInterval / xScale), graphTopY + graphVert /*- ticsVert / 2*/,
		       (int)(graphTopX + (i + 1) * binInterval / xScale), graphTopY + graphVert + ticsVert / 2);
	}

	/*
	if (yTics == 0)
	    return;

	
	if ((double)graphVert / fm.getHeight() < 1.2 * yTics)
	yTics = yTics / 2;
	

	long increment = maxDataItem / yTics;
	if (increment == 0)
	    increment = 1;
	yTics = (int)(maxDataItem / increment);
	*/
	
	final int[] STEPS = new int[] {5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000};

	int maxSteps = graphVert / YTICK_MIN_PIXELS;
	
	if (maxSteps == 0)
	    maxSteps = 1;

	int index = 0;
	while (maxDataItem/STEPS[index] > maxSteps){
	    index++;
	}

	if (index == STEPS.length)
	    index = STEPS.length - 1;

	long increment = STEPS[index];
	yTics = ((int)Math.ceil(maxDataItem/increment)) + 1;

	if (yTics == 0)
	    return;

	double yScale = (double)yTics *increment/ graphVert;

	//YAxis
	for (int i = 0; i < yTics + 1; i++) {
	    String dataInt = (new Long(increment * i)).toString();
	    g.setColor(Color.black);
	    g.drawString(dataInt, (int)(graphTopX - ticsHoriz - labelHoriz * 0.35), 
			 (int)(graphTopY + graphVert - i * increment / yScale));
	    
	    g.setColor(Color.blue);
	    g.drawLine(graphTopX - ticsHoriz / 2, (int)(graphTopY + graphVert - i * increment / yScale),
		       graphTopX, (int)(graphTopY + graphVert - i * increment / yScale));

	    if ((increment == 1 || (yTics - i) % 2 == 1) && !title.equals("")) {
		g.setColor(Color.lightGray);
		g.drawLine(graphTopX, (int)(graphTopY + graphVert - i * increment / yScale),
			   graphTopX + graphHoriz, (int)(graphTopY + graphVert - i * increment / yScale));
	    }
	}

	/*
	//Max data
	g.setColor(Color.black);
	if (maxDataItem >= 0)
	    g.drawString((new Long(maxDataItem)).toString(), (int)(graphTopX - ticsHoriz - labelHoriz * 0.35), 
			 (int)(graphTopY + graphVert - maxDataItem / yScale));
	
	g.setColor(Color.blue);
	g.drawLine(graphTopX - ticsHoriz / 2, (int)(graphTopY + graphVert - maxDataItem / yScale),
		   graphTopX + ticsHoriz / 2, (int)(graphTopY + graphVert - maxDataItem / yScale));
	*/

	g.setColor(Color.black);

	//YLabels

	String yLabel = "";

	if (this instanceof PointGraph) {
	    //if (title.equals("")) {
		/*
		g.drawString("h", (int)(graphTopX - ticsHoriz - labelHoriz * 0.95), 
			     (int)(graphTopY + graphVert - maxDataItem * 0.5 / yScale));
		g.drawString("r", (int)(graphTopX - ticsHoriz - labelHoriz * 0.95), 
			     (int)(graphTopY + graphVert - maxDataItem * 0.25 / yScale));
		g.drawString("s", (int)(graphTopX - ticsHoriz - labelHoriz * 0.95), 
		(int)(graphTopY + graphVert - maxDataItem * 0.00 / yScale));
		*/
		yLabel = "Hours";
		//}
	} else {
	    //if (title.equals("")) {
		/*
		g.drawString("f", (int)(graphTopX - ticsHoriz - labelHoriz * 0.95), 
			     (int)(graphTopY + graphVert - maxDataItem * 0.5 / yScale));
		g.drawString("r", (int)(graphTopX - ticsHoriz - labelHoriz * 0.95), 
			     (int)(graphTopY + graphVert - maxDataItem * 0.25 / yScale));
		g.drawString("e", (int)(graphTopX - ticsHoriz - labelHoriz * 0.95), 
			     (int)(graphTopY + graphVert - maxDataItem * 0.00 / yScale));
		g.drawString("q", (int)(graphTopX - ticsHoriz - labelHoriz * 0.95), 
			     (int)(graphTopY + graphVert + maxDataItem * 0.25 / yScale));
		*/
		yLabel = "Count";
		//}
	}
	
	double angle = Math.PI/2;

	g.rotate(-angle);
	Point srcP = new Point((int)(graphTopX - ticsHoriz - labelHoriz * 0.5), 
			     (int)(graphTopY + graphVert - maxDataItem * 0.5 / yScale));
	g.drawString(yLabel, (float)-srcP.getY(), (float)srcP.getX());
	g.rotate(angle);
	
	plotData(g, fm, graphTopX, graphTopY, graphVert, yPadVert, xScale, yScale);
    }

    protected void plotData(Graphics2D g, FontMetrics fm, int graphTopX, int graphTopY, int graphVert, int yPadVert, double xScale, double yScale) {

	for (int i = 0; i < yDataItems.length; i++) {
	    g.setPaint(gradient);
	    g.fillRect((int)(graphTopX + (i + 0.3) * binInterval / xScale), 
		       (int)(graphTopY + graphVert - yDataItems[i] / yScale),
		       (int)(0.4 * binInterval / xScale), 
		       (int)(yDataItems[i] / yScale));

	    if (yDataItems[i] == 0)
		continue;

	    String dataInt = (new Long(yDataItems[i])).toString();

	    g.setColor(dataItemColor);
	    if (!title.equals("")) {
		g.drawString(dataInt, (int)(graphTopX + (i + 0.5) * binInterval / xScale - fm.stringWidth(dataInt) / 2),
			     (int)(graphTopY + graphVert - yDataItems[i] / yScale - fm.getHeight()));
	    }
	}
    }
}
