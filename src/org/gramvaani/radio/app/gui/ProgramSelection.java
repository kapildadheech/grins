package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.RadioProgramMetadata;

import java.util.ArrayList;

import java.awt.datatransfer.DataFlavor;

public class ProgramSelection extends SelectionList {
    
    ProgramSelection(ArrayList<RadioProgramMetadata> list){
	super(list);
    }

    public static DataFlavor getFlavor(){
 	return SelectionList.getFlavor(ProgramSelection.class);
    }
}