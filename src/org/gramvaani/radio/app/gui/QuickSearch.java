package org.gramvaani.radio.app.gui;

import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashSet;
import javax.swing.Timer;

public class QuickSearch implements ActionListener{
    Timer timer;
    StringBuilder buffer;
    ArrayList<String> wordList;
    ArrayList<Integer> indexList;
    QuickSearchListener listener;
    String query = "";

    final static int keyWaitTime = 300;
    
    public QuickSearch(QuickSearchListener listener){
	timer = new Timer(keyWaitTime, this);
	buffer = new StringBuilder();
	wordList = new ArrayList<String>();
	indexList = new ArrayList<Integer>();
	this.listener = listener;
    }
    
    public void keyTyped(char c){
	buffer.append(c);
	timer.restart();
    }
    
    public void actionPerformed(ActionEvent e){
	timer.stop();
	search(buffer.toString());
	buffer.delete(0, buffer.length());
    }
    
    public void addString(String data, int index){
	for(String str: data.split(" ")){
	    wordList.add(str.toLowerCase());
	    indexList.add(index);
	}
	
    }
    
    public void insertString(String data, int index){
	for (int i = 0; i < indexList.size(); i++){
	    if(indexList.get(i) >= index)
		indexList.set(i, indexList.get(i) + 1);
	}
	addString(data, index);
    }

    public void removeRange(int begin, int end){
	for (int i = 0; i < indexList.size(); i++){
	    if(indexList.get(i) <= end && indexList.get(i) >= begin){
		indexList.remove(i);
		wordList.remove(i);
		i--;
	    }
	}
    }

    public void rerun(){
	search(this.query);
    }

    public void clear(){
	timer.stop();
	wordList.clear();
	indexList.clear();
    }
    
    protected void search(String query){
	if(query.length() == 0)
	    return;
	this.query = query.toLowerCase();
	HashSet<Integer> results = new HashSet<Integer>();
	for(int i = 0; i < wordList.size(); i++){
	    if (wordList.get(i).startsWith(this.query)){
		results.add(indexList.get(i));
	    }
	}
	listener.matchedIndices(results);
    }

    public interface QuickSearchListener{
	public void matchedIndices(HashSet<Integer> indices);
    }

}
