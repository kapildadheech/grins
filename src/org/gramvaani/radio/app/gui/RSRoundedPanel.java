package org.gramvaani.radio.app.gui;

import java.awt.*;
import javax.swing.*;

public class RSRoundedPanel extends JPanel {
    protected void paintComponent(Graphics g){
	if (!isOpaque())
	    return;
	Graphics2D g2 = (Graphics2D) g;
	Color bgColor = getBackground();

	Point p = getLocation();
	Dimension size = getSize();
	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	g2.setColor(bgColor);
	int arcSize = 7;
	g2.fillRoundRect(0, 0, size.width, size.height, arcSize, arcSize);
    }
}