package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.messages.SyncFileStoreMessage;
import org.gramvaani.radio.rscontroller.services.UIService;
import org.gramvaani.radio.app.providers.MediaLibUploadProvider;
import org.gramvaani.radio.app.providers.MediaLibUploadListenerCallback;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.RefreshIndexMessage;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import javax.swing.*;
import javax.swing.event.*;

import java.io.*;

import static org.gramvaani.utilities.IconUtilities.getIcon;
import static org.gramvaani.radio.app.gui.GBCUtilities.*;
import static org.gramvaani.utilities.FileUtilities.copyFile;


public class AdminWidget extends RSWidgetBase implements ChangeListener {
    
    protected RSApp rsApp;
    protected StationConfiguration stationConfig;
    protected LogUtilities logger;
    
    protected RSButton manualSyncButton;
    protected StationNetwork stationNetwork;
    
    protected MediaLib medialib;
    protected ErrorInference errorInference;
    protected UIService uiService;

    Color bgColor, titleColor;
    
    int aspLabelHorFrac = 75;
    int aspValueHorFrac = 25;

    int tabTitleVertFrac = 8;      //Height of the title of the tab
    JTabbedPane tabbedPane;

    static final String BACKUP = "Backup and Restore";
    static final String MANAGEFILES = "Manage Files";
    static String[] adminTabs = new String[]{MANAGEFILES, BACKUP};

    ManageFilesPanel manageFilesPanel;
    BackupPanel backupPanel;
    
    public AdminWidget(RSApp rsApp) {
	super("Administration");
	this.rsApp = rsApp;
	this.stationConfig = rsApp.getStationConfiguration();
	this.uiService = rsApp.getUIService();
	stationNetwork = stationConfig.getStationNetwork();
	IconUtilities.setStationConfig(stationConfig);
	logger = new LogUtilities("AdminWidget");

	wpComponent = new JPanel();
	wpComponent.setLayout(new GridBagLayout());

	aspComponent = new RSPanel();
	bmpIconID = StationConfiguration.ADMIN_ICON;
	bmpToolTip = "Administration Tools";

	medialib = MediaLib.getMediaLib(stationConfig, null);

	manageFilesPanel = new ManageFilesPanel();
	backupPanel = new BackupPanel();
    }

    public boolean onLaunch() {
	bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	titleColor = stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR);

	wpComponent.setBackground(bgColor);
	wpComponent.setLayout(new BoxLayout(wpComponent, BoxLayout.PAGE_AXIS));
	RSTitleBar titleBar = new RSTitleBar(name, wpSize.width);
	titleBar.setBackground(titleColor);
	wpComponent.add(titleBar);

	buildWorkSpace();
	return true;
    }

    public void onMaximize() {
	manageFilesPanel.maximize();
    }

    public void onMinimize() {
	manageFilesPanel.minimize();
    }


    public void onUnload() {
	tabbedPane = null;
	wpComponent.removeAll();
	aspComponent.removeAll();

	manageFilesPanel.unload();
	backupPanel.unload();
    }

    protected void buildWorkSpace() {
	UIManager.put("TabbedPane.selected", stationConfig.getColor(StationConfiguration.LIBRARY_DISPLAYFILTERPANE_COLOR));
	UIManager.put("TabbedPane.selectHighlight", stationConfig.getColor(StationConfiguration.LIBRARY_TOPPANE_COLOR));
	tabbedPane = new JTabbedPane();
	wpComponent.add(tabbedPane, getGbc(0, 0));
	aspComponent.setLayout(new GridBagLayout());

	for(String tabString : adminTabs) {
	    JPanel tabPanel = new JPanel();
	    tabPanel.setSize(new Dimension((int)(wpSize.getWidth() - 6), (int)(wpSize.getHeight() * (100 - tabTitleVertFrac)/100 )));
	    tabbedPane.addTab(tabString, tabPanel);

	    populateTabPanel(tabString, tabPanel);
	    addAspInfo(tabString);
	}
    }

    protected void addAspInfo(String title) {
	if(title.equals(MANAGEFILES)) {
	    manageFilesPanel.addAspInfo();
	}
    }

    protected void populateTabPanel(String title, JPanel tabPanel) {
	if(title.equals(MANAGEFILES)) {
	    manageFilesPanel.populatePanel(tabPanel);
	} else if(title.equals(BACKUP)) {
	    backupPanel.populatePanel(tabPanel);
	}
    }
    
    protected JPanel getTabByTitle(String title) {
	int maxTabs = tabbedPane.getTabCount();
	for(int i = 0; i < maxTabs; i++) {
	    if(title.equals(tabbedPane.getTitleAt(i))) {
		return (JPanel)tabbedPane.getTabComponentAt(i);
	    }
	}

	return null;
    }

    public void stateChanged(ChangeEvent e) {
	if(e.getSource() instanceof JTabbedPane) {
	    logger.debug("StateChanged: Selected tab = " + (((JTabbedPane)(e.getSource())).getSelectedIndex()));
	}
    }

    class BackupPanel implements ActionListener, ProgressListener {
	JPanel formPanel;
	JPanel navPanel;
	JPanel progressPanel;
	JProgressBar progressBar;
	
	static final int formPanelVertFrac = 75;
	static final int progressBarVertFrac = 7;
	static final int progressBarHorizFrac = 80;
	static final int buttonHorizFrac = 12;
	static final int buttonVertFrac = 24;
	static final int navPanelButtonBorder = 10;
	
	int formPanelHeight;
	int progressBarHeight;
	int navPanelHeight;
	
	JButton backButton, forwardButton;
	JRadioButton continueButton, recreateButton, resetButton, restoreButton;
	JSpinner sizeField;
	JComboBox sizeList, volumeList;
	JTextField locationField;
	JTextArea missingVolList;
	
	BackupController backupController;
	ControlPanelJFrame cpJFrame;
	Dimension buttonSize, tabSize;
	
	Icon nextIcon, prevIcon, backupIcon, recreateIcon, restoreIcon;
	
	int blockSize;

	static final String AC_MAIN_NEXT = "AC_MAIN_NEXT";
	static final String AC_CONT_NEXT = "AC_CONT_NEXT";
	static final String AC_TO_MAIN   = "AC_TO_MAIN";
	static final String AC_BACKUP    = "AC_BACKUP";
	static final String AC_RESTORE   = "AC_RESTORE";
	static final String AC_RECREATE  = "AC_RECREATE";
	
	static final int MIN_BLOCK_SIZE = 50;
	static final int MAX_BLOCK_SIZE = 8400;

	public BackupPanel() {
	    backupController = BackupController.getInstance(rsApp, stationConfig, this);
	    cpJFrame = rsApp.getControlPanel().cpJFrame();
	}

	protected void populatePanel(JPanel tabPanel) {
	    tabSize = tabPanel.getSize();
	    tabPanel.setLayout(new BoxLayout(tabPanel, BoxLayout.PAGE_AXIS));
	    
	    formPanelHeight = (tabSize.height) * formPanelVertFrac/100;
	    progressBarHeight = (tabSize.height) * progressBarVertFrac/100;
	    navPanelHeight = tabSize.height - formPanelHeight - progressBarHeight;
	    
	    formPanel = new JPanel();
	    formPanel.setPreferredSize(new Dimension(tabSize.width, formPanelHeight));
	    formPanel.setMinimumSize(new Dimension(tabSize.width, formPanelHeight));
	    tabPanel.add(formPanel);
	    formPanel.setBackground(bgColor);
	    
	    progressPanel = new JPanel();
	    progressPanel.setPreferredSize(new Dimension(tabSize.width, progressBarHeight));
	    progressPanel.setMinimumSize(new Dimension(tabSize.width, progressBarHeight));
	    tabPanel.add(progressPanel);
	    progressPanel.setBackground(bgColor);
	    
	    progressBar = new JProgressBar();
	    progressBar.setPreferredSize(new Dimension(tabSize.width * progressBarHorizFrac/100, progressBarHeight));
	    progressBar.setMaximumSize(new Dimension(tabSize.width * progressBarHorizFrac/100, progressBarHeight));
	    progressBar.setBackground(bgColor);
	    progressBar.setForeground(bgColor.darker().darker());
	    progressBar.setBorder(null);
	    
	    navPanel = new JPanel();
	    navPanel.setPreferredSize(new Dimension(tabSize.width, navPanelHeight));
	    tabPanel.add(navPanel);
	    navPanel.setBackground(bgColor);
	    
	    navPanel.setLayout(new GridBagLayout());
	    JPanel borderPanel = new JPanel();
	    borderPanel.setMinimumSize(new Dimension(tabSize.width * navPanelButtonBorder / 100, navPanelHeight));
	    
	    navPanel.add(borderPanel, getGbc(0, 0, gfillboth(), weighty(1.0), ganchor(GridBagConstraints.BASELINE)));
	    borderPanel.setBackground(bgColor);
	    
	    backButton = new RSButton("");
	    buttonSize = new Dimension(tabSize.width * buttonHorizFrac / 100, navPanelHeight * buttonVertFrac / 100);
	    backButton.setPreferredSize(buttonSize);
	    backButton.setMinimumSize(buttonSize);
	    navPanel.add(backButton, getGbc(1, 0, ganchor(GridBagConstraints.SOUTH)));
	    
	    borderPanel = new JPanel();
	    navPanel.add(borderPanel, getGbc(2, 0, weightx(0.5), gfillboth(), ganchor(GridBagConstraints.SOUTH)));
	    borderPanel.setBackground(bgColor);
	    
	    forwardButton = new RSButton("");
	    forwardButton.setPreferredSize(buttonSize);
	    forwardButton.setMinimumSize(buttonSize);
	    navPanel.add(forwardButton, getGbc(3, 0, ganchor(GridBagConstraints.SOUTH)));
	    
	    borderPanel = new JPanel();
	    borderPanel.setMinimumSize(new Dimension(tabSize.width * navPanelButtonBorder / 100, navPanelHeight));
	    navPanel.add(borderPanel, getGbc(4, 0, gfillboth(), ganchor(GridBagConstraints.SOUTH)));
	    borderPanel.setBackground(bgColor);
	    
	    backButton.addActionListener(this);
	    forwardButton.addActionListener(this);
	    backButton.setBackground(bgColor);
	    forwardButton.setBackground(bgColor);
	    
	    nextIcon = IconUtilities.getIcon(StationConfiguration.NEXT_ICON, buttonSize.height, buttonSize.height);
	    prevIcon = IconUtilities.getIcon(StationConfiguration.PREV_ICON, buttonSize.height, buttonSize.height);
	    backupIcon = IconUtilities.getIcon(StationConfiguration.BACKUP_ICON, buttonSize.height, buttonSize.height);
	    recreateIcon = IconUtilities.getIcon(StationConfiguration.REDO_ICON, buttonSize.height, buttonSize.height);
	    restoreIcon = IconUtilities.getIcon(StationConfiguration.RESTART_ICON, buttonSize.height, buttonSize.height);

	    if (isStartingBackup()){
		showContinue();
	    } else {
		showMain();
	    }
	}

	boolean isStartingBackup(){
	    return false;
	}


	public synchronized void actionPerformed(ActionEvent e){
	    String command = e.getActionCommand();
	    if (command.equals(AC_MAIN_NEXT)){
		if (continueButton.isSelected())
		    showContinue();
		else if (recreateButton.isSelected())
		    showRecreate();
		else if (resetButton.isSelected())
		    showReset();
		else if (restoreButton.isSelected())
		    showRestore();
		
	    } else if (command.equals(AC_TO_MAIN)) {
		showMain();
	    } else if (command.equals(AC_BACKUP)) {
		blockSize = -1;
		
		if (sizeField.isEnabled())
		    blockSize = (Integer)sizeField.getValue();
		else if (sizeList.getSelectedItem().equals("1 CD (700MB)"))
		    blockSize = 700;
		else if (sizeList.getSelectedItem().equals("1 DVD (4600MB)"))
		    blockSize = 4600;
		else if (sizeList.getSelectedItem().equals(backupController.getLastBlockSize()+"MB"))
		    blockSize = backupController.getLastBlockSize();
		if (blockSize >= MIN_BLOCK_SIZE) {
		    Thread t = new Thread(){
			    public void run(){
				forwardButton.setEnabled(false);
				backupController.backup(backupController.getNextVolume() - 1, blockSize, locationField.getText());
				showContinue();
			    }
			};
		    t.start();
		} else {
		    logger.error("ActionPerformed: Invalid blockSize");
		}
	    } else if (command.equals(AC_RESTORE)){
		backupController.restore();
	    } else if (command.equals(AC_RECREATE)){
		Thread t = new Thread(){
			public void run(){
			    forwardButton.setEnabled(false);
			    backupController.recreate(volumeList.getSelectedIndex(), locationField.getText());
			    forwardButton.setEnabled(true);
			}
		    };
		t.start();
	    }
	    
	}

	public void setMaxProgress(int max){
	    progressPanel.add(progressBar);
	    progressBar.setMaximum(max);
	    progressBar.setValue(0);
	    progressPanel.repaint();
	}

	public void setValue(int value){
	    progressBar.setValue(value);
	    progressBar.repaint();
	}
	
	public void finishProgress(){
	    progressPanel.remove(progressBar);
	    progressPanel.repaint();
	}
	
	void showMain(){
	    formPanel.removeAll();
	    backButton.setText("Prev");
	    backButton.setEnabled(false);
	    
	    forwardButton.setText("Next");
	    forwardButton.setIcon(nextIcon);
	    forwardButton.setEnabled(true);
	    forwardButton.setActionCommand(AC_MAIN_NEXT);
	    
	    ButtonGroup buttonGroup = new ButtonGroup();
	    continueButton = new JRadioButton("Continue Backup");
	    recreateButton = new JRadioButton("Recreate Backup Volume");
	    resetButton = new JRadioButton("Reset Backup Process");
	    restoreButton = new JRadioButton("Restore from Backup");
	    
	    int optionVertFrac = 10;
	    Dimension optionSize = new Dimension(tabSize.width / 3, formPanelHeight * optionVertFrac/100);
	    
	    JRadioButton buttons[] = {continueButton, recreateButton, resetButton, restoreButton};
	    
	    for (JRadioButton button: buttons){
		buttonGroup.add(button);
		button.setMinimumSize(optionSize);
		button.setPreferredSize(optionSize);
		button.setBackground(bgColor);
	    }
	    
	    formPanel.setLayout(new GridBagLayout());
	    formPanel.add(continueButton, getGbc(0, 0, gfillboth()));
	    formPanel.add(recreateButton, getGbc(0, 1, gfillboth()));
	    formPanel.add(resetButton, getGbc(0, 2, gfillboth()));
	    formPanel.add(restoreButton, getGbc(0, 3, gfillboth()));
	    
	    continueButton.setSelected(true);
	    
	    formPanel.repaint();
	}

	void showContinue(){
	    formPanel.removeAll();
	    backButton.setIcon(prevIcon);
	    forwardButton.setIcon(backupIcon);
	    
	    if (!backupController.hasMore()){
		formPanel.add(new JLabel("Nothing left to backup."));
		formPanel.repaint();
		backButton.setText("Back");
		backButton.setEnabled(true);
		backButton.setActionCommand(AC_TO_MAIN);
		forwardButton.setText("");
		forwardButton.setEnabled(false);
		return;
	    }
	    
	    if (!backupController.requiresFullBlockSize()){
		// Show that it doesn't use up whole block.
	    }

	    formPanel.setLayout(new GridBagLayout());
	    
	    Insets insets = new Insets(5, 0, 5, 0);
	    int xpad = 20;
	    
	    int nextVolume = backupController.getNextVolume();
	    Date lastDate = new Date(backupController.getLastDate(nextVolume - 2));
	    String date = DateFormat.getDateInstance(DateFormat.SHORT).format(lastDate);
	    JLabel titleLabel = new JLabel("Create Backup Volume-"+nextVolume+" from "+date);
	    titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    formPanel.add(titleLabel, getGbc(0, 0, gridwidth(3), insets, gfillboth()));
	    
	    formPanel.add(new JLabel(" "), getGbc(0, 1, gridwidth(3), insets, gfillboth()));
	    
	    JLabel sizeLabel = new JLabel("Select Volume Size:   ");
	    sizeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	    formPanel.add(sizeLabel, getGbc(0, 2, gfillboth()));
	    
	    String options[];
	    if (backupController.getLastBlockSize() == -1)
		options = new String[] {"1 CD (700MB)", "1 DVD (4600MB)","Custom (MB)"};
	    else
		options = new String[] {"1 CD (700MB)", "1 DVD (4600MB)", backupController.getLastBlockSize()+"MB", "Custom (MB)"};
	    
	    sizeList = new JComboBox(options);
	    if (options.length == 4)
		sizeList.setSelectedIndex(2);
	    formPanel.add(sizeList, getGbc(1, 2, padx(xpad), gfillboth()));
	    
	    
	    sizeField = new JSpinner(new SpinnerNumberModel(100, MIN_BLOCK_SIZE, MAX_BLOCK_SIZE, 10));
	    sizeField.setEnabled(false);
	    sizeField.setBackground(bgColor);
	    
	    Insets spinInsets = new Insets(0, 0, 10, 0);
	    formPanel.add(sizeField, getGbc(1, 3, spinInsets, padx(xpad), gfillboth()));
	    
	    sizeList.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			if (sizeList.getSelectedItem().equals("Custom (MB)")) {
			    sizeField.setEnabled(true);
			    sizeField.setBackground(Color.white);
			    sizeField.setForeground(Color.black);
			} else {
			    sizeField.setForeground(bgColor);
			    sizeField.setEnabled(false);
			    sizeField.setBackground(bgColor);
			}
		    }
		    
		});
	    
	    JLabel locationLabel = new JLabel("Select Location:   ");
	    locationLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	    formPanel.add(locationLabel, getGbc(0, 4, gfillboth()));
	    
	    locationField = new JTextField();
	    formPanel.add(locationField, getGbc(1, 4, padx(xpad), gfillboth()));
	    locationField.setEditable(false);
	    
	    RSButton fileChooserButton = new RSButton("Browse");
	    fileChooserButton.setBackground(bgColor);
	    formPanel.add(fileChooserButton, getGbc(2, 4, gfillboth()));
	    
	    fileChooserButton.addActionListener(new ActionListener(){
		    JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());

		    public void actionPerformed(ActionEvent e){
			FileUtilities.setNewFolderPermissions(fileChooser);
			Thread t = new Thread(){
				public void run(){
				    fileChooser.setBackground(bgColor);
				    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				    int retVal = fileChooser.showDialog(cpJFrame, "Select backup folder");
				    
				    if(retVal != JFileChooser.APPROVE_OPTION)
					return;
				    
				    try{
					locationField.setText(fileChooser.getSelectedFile().getCanonicalPath());
					forwardButton.setEnabled(true);
				    } catch (Exception e){
					logger.error("Browse: Encountered exception:", e);
					return;
				    }
				}
			    };
			t.start();
		    }
		});
	    
	    forwardButton.setEnabled(false);
	    forwardButton.setText("Backup");
	    forwardButton.setActionCommand(AC_BACKUP);
	    backButton.setText("Back");
	    backButton.setEnabled(true);
	    backButton.setActionCommand(AC_TO_MAIN);
	    formPanel.repaint();
	}
	
	void showRecreate(){
	    formPanel.removeAll();
	    
	    formPanel.setLayout(new GridBagLayout());
	    JLabel selectLabel = new JLabel("Select Volume:   ");
	    selectLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	    formPanel.add(selectLabel, getGbc(0, 0, gfillboth()));
	    
	    final String options[] = backupController.getVolumeInfo();
	    volumeList = new JComboBox(options);
	    Insets insets = new Insets(0, 0, 10, 0);
	    formPanel.add(volumeList, getGbc(1, 0, insets, gfillboth()));
	    
	    JLabel locationLabel = new JLabel("Select Location:   ");
	    locationLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	    formPanel.add(locationLabel, getGbc(0, 1, gfillboth()));

	    locationField = new JTextField();
	    formPanel.add(locationField, getGbc(1, 1, gfillboth()));
	    locationField.setEditable(false);
	    
	    RSButton fileChooserButton = new RSButton("Browse");
	    formPanel.add(fileChooserButton, getGbc(1, 2, gfillboth(), padx(40)));
	    
	    fileChooserButton.addActionListener(new ActionListener(){
		    JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());

		    public void actionPerformed(ActionEvent e){
			FileUtilities.setNewFolderPermissions(fileChooser);
			Thread t = new Thread(){
				public void run(){
				    fileChooser.setBackground(bgColor);
				    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				    int retVal = fileChooser.showDialog(cpJFrame, "Select backup folder");
				    
				    if(retVal != JFileChooser.APPROVE_OPTION)
					return;
				    
				    try{
					locationField.setText(fileChooser.getSelectedFile().getCanonicalPath());
					forwardButton.setEnabled(options.length > 0);
				    } catch (Exception e){
					logger.error("Browse: Encountered exception:", e);
					return;
				    }
				}
			    };
			t.start();
		    }
		});
	    
	    backButton.setText("Back");
	    backButton.setEnabled(true);
	    backButton.setIcon(prevIcon);
	    backButton.setActionCommand(AC_TO_MAIN);
	    forwardButton.setText("Recreate Volume");
	    forwardButton.setEnabled(false);
	    forwardButton.setIcon(recreateIcon);
	    forwardButton.setActionCommand(AC_RECREATE);
	    formPanel.repaint();
	}
	
	void showReset(){
	    String message = "Are you sure you want to restart the backup process?";
	    int retVal = JOptionPane.showConfirmDialog(cpJFrame, message, "Restart Backup Process", JOptionPane.YES_NO_OPTION);
	    if (retVal != JOptionPane.YES_OPTION){
		return;
	    }
	    
	    backupController.reset();
	    
	    continueButton.setSelected(true);
	}
	
	void showRestore(){
	    formPanel.removeAll();
	    
	    formPanel.setLayout(new GridBagLayout());
	    
	    JLabel selectLabel = new JLabel("Select Location:   ");
	    selectLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	    formPanel.add(selectLabel, getGbc(0, 0, gfillboth()));
	    
	    RSButton fileChooserButton = new RSButton("Browse");
	    formPanel.add(fileChooserButton, getGbc(1, 0, gfillboth()));
	    
	    JLabel missingLabel = new JLabel("Missing Volumes:   ");
	    missingLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	    Insets insets = new Insets(10, 0, 0, 0);
	    formPanel.add(missingLabel, getGbc(0, 1, insets));
	    
	    missingVolList = new JTextArea();
	    formPanel.add(missingVolList, getGbc(1, 2));
	    
	    missingVolList.setBackground(bgColor);
	    missingVolList.setEditable(false);
	    missingVolList.setLineWrap(true);
	    missingVolList.setWrapStyleWord(true);
	    
	    fileChooserButton.addActionListener(new ActionListener(){
		    JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());

		    public void actionPerformed(ActionEvent e){
			FileUtilities.setNewFolderPermissions(fileChooser);
			Thread t = new Thread(){
				public void run(){
				    fileChooser.setBackground(bgColor);
				    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				    int retVal = fileChooser.showDialog(cpJFrame, "Select backup folder");
				    
				    if (retVal != JFileChooser.APPROVE_OPTION)
					return;
				    
				    if (!backupController.importFromDir(fileChooser.getSelectedFile())){
					logger.error("Restore: Error in importing");
				    }
				    setRestoreButton();
				}
			    };
			t.start();
		    }
		});
	    
	    backButton.setText("Back");
	    backButton.setIcon(prevIcon);
	    backButton.setEnabled(true);
	    backButton.setActionCommand(AC_TO_MAIN);
	    forwardButton.setActionCommand(AC_RESTORE);
	    setRestoreButton();
	    forwardButton.setIcon(restoreIcon);
	    formPanel.repaint();
	}
	
	void setRestoreButton(){
	    int volume = backupController.canRestore();
	    if (volume < 1){
		forwardButton.setEnabled(false);
		forwardButton.setText("Restore");
	    } else {
		forwardButton.setEnabled(true);
		forwardButton.setText("Restore to Volume: "+volume);
	    }
	    
	    StringBuilder str = new StringBuilder();
	    Integer vols[] = backupController.getMissingVolumes();
	    for (Integer i: vols)
		str.append(i+", ");
	    missingVolList.setText(str.toString());
	}
	
	protected void unload() {
	    backupController.cleanup();
	}
    }


    class ManageFilesPanel {
	int labelVertFrac = 6;
	int labelHorFrac = 70;
	int valueHorFrac = 30;
	int buttonHorFrac = 20;
	int buttonVertFrac = 4;

	RSLabel playoutValueLabel, previewValueLabel, aspPlayout, aspPreview, totalLabel;
	RSButton syncButton;
	Thread fileInfoThread;
	Object fileInfoObject = new Object();
	boolean terminateFileInfo = false;
	Color itemBgColor;

	int FILEINFO_REFRESH_INTERVAL = 5000;

	public ManageFilesPanel() {
	    playoutValueLabel = new RSLabel("----");
	    previewValueLabel = new RSLabel("----");
	    aspPlayout = new RSLabel("----");
	    aspPreview = new RSLabel("----");
	    totalLabel = new RSLabel("----");
	    itemBgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR);
	}

	protected void unload() {
	    terminateFileInfo();
	}

	protected void minimize() {
	    setInfoRefreshInterval(30000);
	}

	protected void maximize() {
	    setInfoRefreshInterval(5000);
	}

	protected void setInfoRefreshInterval(int interval) {
	    synchronized(fileInfoObject) {
		FILEINFO_REFRESH_INTERVAL = interval;
		fileInfoObject.notifyAll();
	    }
	}


	protected void populatePanel(JPanel tabPanel) {
	    Dimension tabSize = tabPanel.getSize();

	    tabPanel.setBorder(null);//BorderFactory.createLineBorder(bgColor.darker()));
	    tabPanel.setBackground(bgColor);
	    tabPanel.setLayout(new GridBagLayout());
	    
	    JPanel totalPanel = getTotalFilesPanel(tabSize);
	    JPanel availPanel = getFileAvailabilityPanel(tabSize);
	    JPanel emptyPanel = new JPanel();
	    emptyPanel.setBackground(bgColor);

	    tabPanel.add(totalPanel, getGbc(0,0));
	    tabPanel.add(emptyPanel, getGbc(0,1));
	    tabPanel.add(availPanel, getGbc(0,2));

	    emptyPanel = new JPanel();
	    emptyPanel.setBackground(bgColor);
	    //emptyPanel.setPreferredSize(new Dimension((int)tabSize.width, (int)(tabSize.height - totalPanel.getSize().height - availPanel.getSize().height)));
	    tabPanel.add(emptyPanel, getGbc(0, 3, weighty(1.0)));
	    
	    fileInfoThread = new Thread("OutOfSyncFileReader") {
		    public void run() {
			long startTime, currTime;
			while(true) {
			    getFilesInfo();
			    synchronized(fileInfoObject) {
				startTime = System.currentTimeMillis();
				currTime = startTime;
				
				while(currTime - startTime < FILEINFO_REFRESH_INTERVAL) {
				    try {
					fileInfoObject.wait(FILEINFO_REFRESH_INTERVAL);
				    } catch(Exception e) { }
				    currTime = System.currentTimeMillis();
				    if(terminateFileInfo) {
					terminateFileInfo = false;
					return;
				    }
				}
			    }
			}
		    }
		};
	    
	    fileInfoThread.start();
	}

	protected JPanel getTotalFilesPanel(Dimension tabSize) {
	    JPanel totalPanel = new JPanel();
	    
	    totalPanel.setLayout(new BorderLayout());
	    totalPanel.setBackground(bgColor);
	    totalPanel.setSize(new Dimension((int)tabSize.getWidth(), (int)(tabSize.getHeight() * 2 * labelVertFrac / 100)));

	    RSLabel titleLabel = new RSLabel("Total Files");
	    titleLabel.setBackground(titleColor);
	    titleLabel.setOpaque(true);
	    titleLabel.setPreferredSize(new Dimension((int)tabSize.getWidth(), RSTitleBar.HEIGHT));
	    totalPanel.add(titleLabel, BorderLayout.PAGE_START);
	    
	    RSLabel infoLabel = new RSLabel("Total number of files in the GRINS:");
	    //infoLabel.setBackground(itemBgColor);
	    //infoLabel.setOpaque(true);
	    infoLabel.setHorizontalAlignment(SwingConstants.LEFT);
	    infoLabel.setPreferredSize(new Dimension((int)(tabSize.getWidth()*labelHorFrac/100), (int)(tabSize.getHeight() * labelVertFrac / 100.0)));
	    totalPanel.add(infoLabel, BorderLayout.LINE_START);

	    totalLabel.setPreferredSize(new Dimension((int)(tabSize.getWidth()*valueHorFrac/100), (int)(tabSize.getHeight() * labelVertFrac / 100.0)));
	    totalLabel.setBackground(itemBgColor);
	    totalLabel.setOpaque(true);
	    totalPanel.add(totalLabel, BorderLayout.CENTER);

	    return totalPanel;
	}

	protected JPanel getFileAvailabilityPanel(Dimension tabSize) {
	    JPanel availPanel = new JPanel();

	    availPanel.setLayout(new BorderLayout());
	    availPanel.setBackground(bgColor);
	    int panelHeight = (int)(RSTitleBar.HEIGHT + (tabSize.getHeight() * labelVertFrac * 3/ 100.0));
		
	    availPanel.setSize(new Dimension((int)tabSize.getWidth(), panelHeight));

	    RSLabel titleLabel = new RSLabel("File Availability");
	    titleLabel.setBackground(titleColor);
	    titleLabel.setOpaque(true);
	    titleLabel.setPreferredSize(new Dimension((int)tabSize.getWidth(), RSTitleBar.HEIGHT));
	    availPanel.add(titleLabel, BorderLayout.PAGE_START);


	    JPanel panel = new JPanel();
	    panel.setBackground(bgColor);
	    panel.setLayout(new GridBagLayout());
	    availPanel.add(panel, BorderLayout.CENTER);

	    RSLabel infoLabel = new RSLabel("Number of files unavailable for Playout:");
	    infoLabel.setHorizontalAlignment(SwingConstants.LEFT);
	    infoLabel.setPreferredSize(new Dimension((int)(tabSize.getWidth()*labelHorFrac/100), (int)(tabSize.getHeight() * labelVertFrac / 100.0)));	
	    panel.add(infoLabel, getGbc(0, 0, gfillboth()));
	    
	    playoutValueLabel.setBackground(itemBgColor);
	    playoutValueLabel.setOpaque(true);
	    playoutValueLabel.setPreferredSize(new Dimension((int)(tabSize.getWidth()*valueHorFrac/100), (int)(tabSize.getHeight() * labelVertFrac / 100.0)));	
	    panel.add(playoutValueLabel, getGbc(1, 0, gfillboth()));
	    
	    infoLabel = new RSLabel("Number of files unavailable for Preview:");
	    infoLabel.setHorizontalAlignment(SwingConstants.LEFT);
	    infoLabel.setPreferredSize(new Dimension((int)(tabSize.getWidth()*labelHorFrac/100), (int)(tabSize.getHeight() * labelVertFrac / 100.0)));	
	    panel.add(infoLabel, getGbc(0, 1, gfillboth()));

	    previewValueLabel.setBackground(itemBgColor);
	    previewValueLabel.setOpaque(true);
	    previewValueLabel.setPreferredSize(new Dimension((int)(tabSize.getWidth()*valueHorFrac/100), (int)(tabSize.getHeight() * labelVertFrac / 100.0)));	
	    panel.add(previewValueLabel, getGbc(1, 1, gfillboth()));

	    syncButton = new RSButton("Make Files Available");
	    syncButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			for(String producerService: stationNetwork.getProducerServices()) {
			    
			    SyncFileStoreMessage msg = new SyncFileStoreMessage(RSController.UI_SERVICE, 
										producerService, 
										SyncFileStoreMessage.SYNC_FILE_STORE);
			    uiService.sendMessage(msg);
			}
		    }
		}
		);
	    syncButton.setPreferredSize(new Dimension((int)(tabSize.getWidth()*buttonHorFrac/100), (int)(tabSize.getHeight() * buttonVertFrac / 100.0)));	
	    syncButton.setBackground(bgColor);
	    panel.add(syncButton, getGbc(0, 2, gridwidth(2)));
	    
	    return availPanel;
	}

	protected void addAspInfo() {
	    int labelWidth = (int)(aspComponent.getWidth() * aspLabelHorFrac / 100);
	    int labelHeight = (int)(aspComponent.getHeight() / 8);
	    Dimension labelDim = new Dimension(labelWidth, labelHeight);

	    int valueWidth = (int)(aspComponent.getWidth() * aspValueHorFrac / 100);
	    int valueHeight = labelHeight;
	    Dimension valueDim = new Dimension(valueWidth, valueHeight);
	    
	    RSLabel label = new RSLabel("Playout unavailable:");
	    label.setPreferredSize(labelDim);
	    label.setHorizontalAlignment(SwingConstants.CENTER);
	    aspComponent.add(label, getGbc(0,0));

	    aspPlayout.setPreferredSize(valueDim);
	    aspComponent.add(aspPlayout, getGbc(1,0));

	    label = new RSLabel("Preview unavailable:");
	    label.setPreferredSize(labelDim);
	    label.setHorizontalAlignment(SwingConstants.CENTER);
	    aspComponent.add(label, getGbc(0,1));

	    aspPreview.setPreferredSize(valueDim);
	    aspComponent.add(aspPreview, getGbc(1,1));
	}

	protected void terminateFileInfo() {
	    synchronized(fileInfoObject) {
		terminateFileInfo = true;
		fileInfoObject.notifyAll();
	    }
	}

	protected void getFilesInfo() {
	    if(medialib == null) {
		medialib = MediaLib.getMediaLib(stationConfig, null);
		
		if(medialib == null) {
		    playoutValueLabel.setText("Database Not Connected");
		    previewValueLabel.setText("Database Not Connected");
		    totalLabel.setText("Database Not Connected");
		    aspPlayout.setText("Error");
		    aspPreview.setText("Error");
		}
	    }
	    
	    Long playoutUnavail = getPlayoutUnavail();
	    Long previewUnavail = getPreviewUnavail();
	    Long total = getTotalFiles();
	    
	    playoutValueLabel.setText(" " + playoutUnavail.toString());
	    aspPlayout.setText(" " + playoutUnavail.toString());

	    previewValueLabel.setText(" " + previewUnavail.toString());
	    aspPreview.setText(" " + previewUnavail.toString());

	    totalLabel.setText(" " + total);
	}
	
	protected Long getPlayoutUnavail() {
	    RadioProgramMetadata rpm = RadioProgramMetadata.getDummyObject("");
	    String playoutAttempts = rpm.getPlayoutStoreAttemptsFieldName();

	    return getUnavail(playoutAttempts);
	}

	protected Long getUnavail(String storeAttempts) {
	    RadioProgramMetadata rpm = RadioProgramMetadata.getDummyObject("");
	    String tableName = Metadata.getClassName(rpm.getClass());
	    String op = MediaLib.COUNT_OP;
	    String fieldName = RadioProgramMetadata.getItemIDFieldName();
	    
	    String whereClause = storeAttempts + " != " + FileStoreManager.UPLOAD_DONE +
		" and " + storeAttempts + " != " + FileStoreManager.FILE_DELETED;

	    Object retVal = medialib.mathOp(tableName, whereClause, fieldName, op);
	    if(retVal instanceof Long)
		return (Long)retVal;
	    else 
		return new Long(-1);
	}

	protected long getPreviewUnavail() {
	    RadioProgramMetadata rpm = RadioProgramMetadata.getDummyObject("");
	    String previewAttempts = rpm.getPreviewStoreAttemptsFieldName();

	    return getUnavail(previewAttempts);
	}

	protected Long getTotalFiles() {
	    RadioProgramMetadata rpm = RadioProgramMetadata.getDummyObject("");
	    String tableName = Metadata.getClassName(rpm.getClass());
	    String op = MediaLib.COUNT_OP;
	    String fieldName = RadioProgramMetadata.getItemIDFieldName();
	    String whereClause = "";

	    Object retVal = medialib.mathOp(tableName, whereClause, fieldName, op);
	    if(retVal instanceof Long)
		return (Long)retVal;
	    else 
		return new Long(-1);
	}

	protected String buildOutOfSyncWhereClause() {
	    String[] producers = stationNetwork.getProducerStores();
	    String[] consumers = stationNetwork.getConsumerStores();
	    
	    String whereClause = "(";
	    
	    for(int i = 0; i < producers.length; i++) {
		whereClause += FileStoreUploader.getFileStoreDBFieldName(producers[i]) + "=" + FileStoreManager.UPLOAD_DONE;
		if((i+1) != producers.length)
		    whereClause += " or ";
	    }
	    
	    whereClause += ") and (";
	    for(int i = 0; i < consumers.length; i++) {
		String fieldName = FileStoreUploader.getFileStoreDBFieldName(consumers[i]);
		whereClause += "(" + fieldName + "!=" + FileStoreManager.UPLOAD_DONE + 
		    " and " + fieldName + "!=" + FileStoreManager.FILE_DELETED + ")";

		if((i+1) != consumers.length)
		    whereClause += " or ";
	    }
	    whereClause += ")";
	    
	    logger.debug("BuildOutOfSyncWhereClause: WhereClause= " + whereClause);
	    return whereClause;
	}
    }

}

interface ProgressListener {
    void setMaxProgress(int max);
    void setValue(int value);
    void finishProgress();
}
