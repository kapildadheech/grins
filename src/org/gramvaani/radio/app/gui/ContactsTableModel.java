package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.utilities.LogUtilities;

import javax.swing.table.DefaultTableModel;

import java.util.Hashtable;

public class ContactsTableModel extends DefaultTableModel implements CacheObjectListener {
    static final int NUM_PER_QUERY = 50;
    
    int totalEntities = 0;
    
    Hashtable<Integer, Entity> entities = new Hashtable<Integer, Entity>();
    Hashtable<Integer, Integer> entityRow = new Hashtable<Integer, Integer>();
    
    String query = "";
    MetadataProvider metadataProvider;
    CacheProvider cacheProvider;
    
    LogUtilities logger = new LogUtilities("ContactsTableModel");
    
    ContactsTableModel(MetadataProvider metadataProvider, CacheProvider cacheProvider){
	super();
	this.metadataProvider = metadataProvider;
	this.cacheProvider = cacheProvider;
    }
    
    public int getColumnCount(){
	return 4;
    }
    
    public int getRowCount(){
	return totalEntities;
    }
    
    final String columnNames[] = new String[] {"Name", "Location", "Phone", "Email"};
    
    public String getColumnName(int column){
	return columnNames[column];
    }
    
    public Object getValueAt(int row, int column){
	fetchBatch(row);
	switch(column) {
	case 0:
	    return entities.get(row).getName();
	case 1:
	    return entities.get(row).getLocation();
	case 2:
	    return entities.get(row).getPhone();
	case 3:
	    return entities.get(row).getEmail();
	default:
	    return "";
	}
    }
    
    public Entity getEntityAt(int row){
	return entities.get(row);
    }
    
    public boolean isCellEditable(int row, int col){
	return false;
    }
    
    String getWhereClause(){
	if (query == null || query.equals(""))
	    return "";
	
	return Entity.getNameFieldName() + " like '%" +query+ "%' " + " OR " + 
	    Entity.getLocationFieldName() +" like '%" +query+ "%' " + " OR " +
	    Entity.getPhoneFieldName() +   " like '%" +query+ "%' " + " OR " +
	    Entity.getEmailFieldName() +   " like '%" +query+ "%'";
    }
    
    public void reloadTable(){
	clearData();
	
	Long numResults = (Long) metadataProvider.mathOp(Metadata.getClassName(Entity.class), getWhereClause(), "1", MetadataProvider.COUNT_OP);
	totalEntities = numResults.intValue();
	
	fireTableDataChanged();
    }
    
    public void search(String query){
	this.query = query;
	
	reloadTable();
    }
    
    public void clearData(){
	for (Entity entity: entities.values()){
	    cacheProvider.removeCacheObjectListener(entity, this);
	}
	entities.clear();
	entityRow.clear();
    }
    
    Entity[] getAllEntities() {
	return metadataProvider.get(new Entity(), null, new String[] {Entity.getNameFieldName()}, 
				    new String[] {MetadataProvider.ORDER_ASC}, null, -1);
    }

    void fetchBatch(int row){
	
	int baseRow = (row - row % NUM_PER_QUERY);
	if (entities.get(baseRow) != null)
	    return;
	
	Entity results[] = metadataProvider.get(new Entity(), getWhereClause(), new String[] {Entity.getNameFieldName()}, new String[] {MetadataProvider.ORDER_ASC}, null, baseRow, NUM_PER_QUERY);
	int i = baseRow;
	
	for (Entity dbEntity: results){
	    Entity entity = cacheProvider.getSingle(dbEntity);
	    entities.put(i, entity);
	    entityRow.put(entity.getEntityID(), i++);
	    cacheProvider.addCacheObjectListener(entity, this);
	}
    }
 
    /* Cache Provider Callbacks */

    public void cacheObjectValueChanged(Metadata metadata){
	if (metadata instanceof Entity){
	    Entity entity = (Entity) metadata;
	    
	    Integer row = entityRow.get(entity.getEntityID());
	    
	    if (row == null){
		logger.error("CacheObjectValueChanged: Null object.");
		return;
	    }
	    
	    entities.put(row, entity);
	    fireTableRowsUpdated(row, row);
	}
    }
    
    public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKey){
	
    }
    
}