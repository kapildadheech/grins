package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.RSController;
import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import static org.gramvaani.utilities.IconUtilities.getIcon;
import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class DiagnosticsWidget extends RSWidgetBase {
    public static int OK = 0;
    public static int NOTOK = -1;
    public static int UNTESTED = 1;
    public static int CRITICAL = -2;

    public static String DIAGNOSTICS = "DIAGNOSTICS";
    public static String NETWORK = "NETWORK";

    public static final String MANUAL            = "MANUAL";
    public static final String DETAILS           = "DETAILS";
    
    public static final String PLAYOUT_REACHABLE = "PLAYOUT_REACHABLE";
    public static final String PREVIEW_REACHABLE = "PREVIEW_REACHABLE";
    public static final String PLAYOUT_OK        = "PLAYOUT_OK";
    public static final String PREVIEW_OK        = "PREVIEW_OK";
    public static final String MONITOR_REACHABLE = "MONITOR_REACHABLE";
    public static final String MONITOR_IN        = "MONITOR_IN";
    public static final String MONITOR_OUT       = "MONITOR_OUT";

    public static final String ARCHIVER_REACHABLE= "ARCHIVER_REACHABLE";
    public static final String ARCHIVER_OK       = "ARCHIVER_OK";

    public static final String LIBRARY_REACHABLE = "LIBRARY_REACHABLE";
    public static final String UPLOAD_REACHABLE  = "UPLOAD_REACHABLE";
    public static final String LIBRARY_OK        = "LIBRARY_OK";
    public static final String UPLOAD_OK         = "UPLOAD_OK";
    public static final String DISKSPACE_OK      = "DISKSPACE_OK";

    public static final String MIC_REACHABLE     = "MIC_REACHABLE";
    public static final String MIC_IN		 = "MIC_IN";
    public static final String MIC_OK            = "MIC_OK";
    
    public static final String ONLINETELEPHONY_REACHABLE = "ONLINETELEPHONY_REACHABLE";
    public static final String ONLINETELEPHONY_OK = "ONLINETELEPHONY_OK";
    
    public static final String OFFLINETELEPHONY_REACHABLE = "OFFLINETELEPHONY_REACHABLE";
    public static final String OFFLINETELEPHONY_OK = "OFFLINETELEPHONY_OK";

    protected RSApp rsApp;
    protected DiagnosticsController diagnosticsController;
    protected LogUtilities logger;
    protected StationConfiguration stationConfig;

    protected Hashtable<String, StatusCheckItem> statusItems;
    protected RSButton diagnosticsButton, networkButton;

    boolean networkDisplay = false;
    JPanel systemDiagPanel, systemDiagEntriesPanel;
    DrawablePanel networkDiagPanel;
    GridBagConstraints systemDiagConstraints, networkDiagConstraints;
    Hashtable<String, GridCoord> networkMachineLabels;
    JProgressBar progressBar;
    Color itemBgColor, itemBgColorAlt;

    protected StationNetwork stationNetwork;

    public DiagnosticsWidget(RSApp rsApp) {
	super("Diagnostics");

	this.rsApp = rsApp;

	this.stationConfig = rsApp.getStationConfiguration();
	IconUtilities.setStationConfig(stationConfig);

	wpComponent = new JPanel();
	wpComponent.setLayout(new GridBagLayout());

	aspComponent = new RSPanel();

	bmpIconID = StationConfiguration.DIAGNOSTICS_ICON;
	bmpToolTip = "System diagnostics";

	diagnosticsController = new DiagnosticsController(this, rsApp.getControlPanel(), rsApp);

	logger = new LogUtilities("DiagnosticsWidget");
	statusItems = new Hashtable<String, StatusCheckItem>();

	stationNetwork = stationConfig.getStationNetwork();
	networkMachineLabels = new Hashtable<String, GridCoord>();
    }

    public boolean onLaunch() {
	buildWorkspace();
	buildNetworkWorkspace();
	reinitAspComponent();

	diagnosticsController.init();

	return true;
    }

    static final int titlePanelVF = 4;
    static final int statusItemVF = 4;
    static final int recordingStatusVF 	= (int)statusItemVF * 3;  // archiver reachable, archiver ok
    static final int micStatusVF 	= (int)statusItemVF * 4;        // mic reachable, mic in, mic ok
    static int playoutStatusVF 		= (int)statusItemVF * 5;    // playout/prev reachable, playout/prev ok, mon in/out
    static int libraryStatusVF 		= (int)statusItemVF * 6;    // library/upload reachable, library/upload ok, diskspace ok
    static final int telephonyStatusVF 	= (int)statusItemVF * 3;

    static final int checkTypeHF = 15;
    static final int manualButtonHF = 10;
    static final int statusHF = 10;
    static final double detailsButtonHF = 10.5;
    static final int messageHF = (int)(100 - checkTypeHF - manualButtonHF - statusHF - detailsButtonHF);
    
    boolean micEnabled = false;

    protected void buildNetworkWorkspace() {
	playoutStatusVF = (int)statusItemVF * 5;    // playout/preview reachable, playout/preview ok, monitor in/out
	libraryStatusVF = (int)statusItemVF * 6;    // library/upload reachable, library/upload ok, diskspace ok

	Color bgColorAlt = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR);
	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	//Color borderColor = stationConfig.getColor(StationConfiguration.STD_SELECTED_BORDER_COLOR);
	Color borderColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR).darker();

	networkDiagPanel = new DrawablePanel();
	networkDiagPanel.setLayout(new GridBagLayout());
        networkDiagPanel.setBackground(bgColor); // XXX
	networkDiagPanel.setPreferredSize(new Dimension((int)(wpSize.getWidth()), (int)(wpSize.getHeight() * (100 - statusItemVF) / 100.0)));

	RSLabel titleLabel = new RSLabel("Network status");
	titleLabel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR));
	titleLabel.setOpaque(true);
	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	titleLabel.setPreferredSize(new Dimension((int)(wpSize.getWidth()), (int)(wpSize.getHeight() * titlePanelVF / 100.0)));	
	networkDiagPanel.add(titleLabel, getGbc(0, 0, gridwidth(3)));
	
	Dimension machineBigDim = new Dimension((int)(wpSize.getWidth() / 3), (int)(wpSize.getHeight() * (100 - statusItemVF - titlePanelVF) / 100.0 / 3));
	Dimension machineDim = new Dimension((int)(machineBigDim.getWidth() * 0.75), (int)(machineBigDim.getHeight() * 0.75));
	
	ArrayList<String> machines = stationNetwork.getMachines();
	int machineCount = 0;
	int gridPos = 0;

	for (int i = 0; i < stationNetwork.getNumMachines(); i++) {
	    String machineID = machines.get(i);
	    int xPos, yPos;
	    if (machineID.equals(StationConfiguration.RSCONTROLLER_MACHINE)) {
		xPos = 1; yPos = 1;
		JPanel machineBigPanel = new JPanel();
		machineBigPanel.setPreferredSize(machineBigDim);
		networkDiagPanel.add(machineBigPanel, getGbc(xPos, yPos+1));
		machineBigPanel.setLayout(new GridBagLayout());
		machineBigPanel.setOpaque(false);

		JPanel machinePanel = new JPanel();
		machinePanel.setPreferredSize(machineDim);
		machinePanel.setBorder(BorderFactory.createLineBorder(borderColor));
		machineBigPanel.add(machinePanel, getGbc(0, 0));
		machinePanel.setBackground(bgColorAlt);
		
		populateMachinePanel(machinePanel, machineID, new Dimension((int)(machineDim.getWidth() - 2), (int)(machineDim.getHeight())));
		
	    } else {
		if (machineCount < 3) {
		    xPos = machineCount; yPos = 0;
		} else if (machineCount == 3 || machineCount == 4) {
		    xPos = (machineCount - 3); yPos = 1;
		} else {
		    xPos = (machineCount - 5); yPos = 2;
		}

		JPanel machineBigPanel = new JPanel();
		machineBigPanel.setPreferredSize(machineBigDim);
		networkDiagPanel.add(machineBigPanel, getGbc(xPos, yPos+1));
		machineBigPanel.setLayout(new GridBagLayout());
		machineBigPanel.setOpaque(false);

		JPanel machinePanel = new JPanel();

		machinePanel.setPreferredSize(machineDim);
		machinePanel.setBorder(BorderFactory.createLineBorder(borderColor));
		machineBigPanel.add(machinePanel, getGbc(0, 0));
		machinePanel.setBackground(bgColorAlt);
		
		populateMachinePanel(machinePanel, machineID, new Dimension((int)(machineDim.getWidth() - 2), (int)(machineDim.getHeight())));

		machineCount++;
	    }

	    networkMachineLabels.put(machineID, new GridCoord(xPos, yPos));
	    gridPos = Math.max(gridPos, yPos + 1);

	}
	
	RSLabel blankLabel = new RSLabel("");
	blankLabel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR));
	blankLabel.setOpaque(true);

	networkDiagPanel.add(blankLabel, getGbc(0, ++gridPos, gridwidth(3), weightx(100.0), weighty(100.0)));

	networkDiagConstraints = getGbc(0, 0);

	for (int i = 0; i < stationNetwork.getNumMachines(); i++) {
	    String machineID = machines.get(i);
	    if (!machineID.equals(StationConfiguration.RSCONTROLLER_MACHINE)) {
		showNetworkLink(machineID, UNTESTED);
	    }
	}
    }

    protected void populateMachinePanel(JPanel machinePanel, String machineID, Dimension dim) {
	Dimension statusItemDim = new Dimension((int)(dim.getWidth()), (int)(dim.getHeight() / 8));
	
	machinePanel.setLayout(new GridBagLayout());

	RSLabel titleLabel = new RSLabel(machineID);
	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	titleLabel.setBackground(stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR));
	titleLabel.setPreferredSize(statusItemDim);
	titleLabel.setMinimumSize(statusItemDim);
	titleLabel.setOpaque(true);
	machinePanel.add(titleLabel, getGbc(0, 0, gridwidth(2)));

	Color bgColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);
	Color bgColorAlt = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR);

	ArrayList<String> serviceRoles = stationNetwork.getServiceRolesOnMachine(machineID);
	int numServiceRoles = serviceRoles.size();
	for (int i = 0; i < serviceRoles.size(); i++) {
	    String serviceRole = serviceRoles.get(i);
	    
	    if (serviceRole.equals(StationNetwork.UI)) {
		RSLabel itemLabel = new RSLabel("UI Ok");
		itemLabel.setPreferredSize(new Dimension((int)(statusItemDim.getWidth() * 0.8), (int)(statusItemDim.getHeight())));
		if (i % 2 == 0)
		    itemLabel.setBackground(bgColor);
		else
		    itemLabel.setBackground(bgColorAlt);
		itemLabel.setOpaque(true);
		machinePanel.add(itemLabel, getGbc(0, i+1));

		RSLabel statusLabel = new RSLabel("");
		Dimension statusDim = new Dimension((int)(statusItemDim.getWidth() * 0.2), (int)(statusItemDim.getHeight()));
		statusLabel.setPreferredSize(statusDim);
		if (i % 2 == 0)
		    statusLabel.setBackground(bgColor);
		else
		    statusLabel.setBackground(bgColorAlt);
		statusLabel.setOpaque(true);
		machinePanel.add(statusLabel, getGbc(1, i+1));

		statusLabel.setIcon(getIcon(StationConfiguration.OK_ICON, (int)(statusDim.getWidth()), (int)(statusDim.getHeight())));

	    } else {
		StatusCheckItem statusCheckItem = statusItems.get(serviceRole);
		if (statusCheckItem == null){
		    logger.error("PopulateMachinePanel: Null statusCheckItem: ");
		    continue;
		}

		RSLabel itemLabel = new RSLabel(statusCheckItem.getItemName());
		itemLabel.setPreferredSize(new Dimension((int)(statusItemDim.getWidth() * 0.8), (int)(statusItemDim.getHeight())));
		if (i % 2 == 0)
		    itemLabel.setBackground(bgColor);
		else
		    itemLabel.setBackground(bgColorAlt);
		itemLabel.setOpaque(true);
		machinePanel.add(itemLabel, getGbc(0, i+1));

		RSLabel statusLabel = new RSLabel("");
		Dimension statusDim = new Dimension((int)(statusItemDim.getWidth() * 0.2), (int)(statusItemDim.getHeight()));
		statusLabel.setPreferredSize(statusDim);
		if (i % 2 == 0)
		    statusLabel.setBackground(bgColor);
		else
		    statusLabel.setBackground(bgColorAlt);
		statusLabel.setOpaque(true);
		machinePanel.add(statusLabel, getGbc(1, i + 1));

		statusCheckItem.setNetworkStatusLabel(statusLabel, statusDim);
		statusCheckItem.setNetworkStatus(UNTESTED);
	    }
	}

	RSLabel blankLabel = new RSLabel("");
	machinePanel.add(blankLabel, getGbc(0, numServiceRoles + 1, gridwidth(2), weighty(100.0)));
    }

    public void showNetworkLink(String machineID, int status) {
	logger.debug("showNetworkLink: " + machineID + ":" + status);

	if (!machineID.equals(StationConfiguration.RSCONTROLLER_MACHINE)) {
	    GridCoord machineCoord = networkMachineLabels.get(machineID);
	    GridCoord rscCoord = networkMachineLabels.get(StationConfiguration.RSCONTROLLER_MACHINE);
	    int startX = (int)(wpSize.getWidth() / 3.0 * machineCoord.getX() + wpSize.getWidth() / 3.0 * 0.5);
	    int endX = (int)(wpSize.getWidth() / 3.0 * rscCoord.getX() + wpSize.getWidth() / 3.0 * 0.5);
	    
	    int startY = (int)(wpSize.getHeight() * (100 - titlePanelVF) / 100.0 / 3) * machineCoord.getY() +
		(int)(wpSize.getHeight() * (100 - titlePanelVF) / 100.0 / 3 / 2);
	    int endY = (int)(wpSize.getHeight() * (100 - titlePanelVF) / 100.0 / 3) * rscCoord.getY() +
		(int)(wpSize.getHeight() * (100 - titlePanelVF) / 100.0 / 3 / 2);
	    
	    networkDiagPanel.addLine(machineID, startX, startY, endX, endY, status);
	}
    }

    public void resetAllStatus() {
	if (statusItems.size() == 0)
	    return;

	for (String itemID: statusItems.keySet()){
	    setStatus(itemID, UNTESTED, DiagnosticsController.NO_ERROR, "");
	}
    }

    protected void buildWorkspace() {
	Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	itemBgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR);
	itemBgColorAlt = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);

	Color borderColor = bgColor.darker();
	wpComponent.setBackground(bgColor);
	
	Dimension statusDim = new Dimension((int)(wpSize.getWidth()) - 2, (int)(wpSize.getHeight() * statusItemVF / 100.0));

	systemDiagPanel = new JPanel();
	systemDiagPanel.setLayout(new BorderLayout());
        systemDiagPanel.setBackground(bgColor);
	systemDiagPanel.setPreferredSize(new Dimension((int)(wpSize.getWidth()), (int)(wpSize.getHeight() * (100 - statusItemVF) / 100.0)));
	wpComponent.add(systemDiagPanel, getGbc(0, 0));
	systemDiagConstraints = getGbc(0, 0);


	RSLabel titleLabel = new RSLabel("System diagnostics");
	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	titleLabel.setPreferredSize(new Dimension((int)(wpSize.getWidth()), (int)(wpSize.getHeight() * titlePanelVF / 100.0)));	
	systemDiagPanel.add(titleLabel, BorderLayout.NORTH);
	
	Dimension titleDim = new Dimension((int)(wpSize.getWidth()) - 2, 
					   (int)(wpSize.getHeight() * statusItemVF / 100.0) - 2);

	systemDiagEntriesPanel = new JPanel();
	systemDiagEntriesPanel.setLayout(new GridBagLayout());
	systemDiagEntriesPanel.setBackground(bgColor);
	JScrollPane scrollPane = new JScrollPane(systemDiagEntriesPanel);
	scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	systemDiagPanel.add(scrollPane, BorderLayout.CENTER);

	/* Playout */

	if (stationConfig.isMonitorServiceActive())
	    playoutStatusVF += 3 * statusItemVF;

	JPanel playoutPanel = new JPanel();
	prepareDiagnosticsPanel(playoutPanel, "Playout check", playoutStatusVF);

	StatusCheckItem statusCheckItem;

	statusCheckItem = new StatusCheckItem(PLAYOUT_REACHABLE, false, "Playout reachable", statusDim);
	addStatusItem(playoutPanel, statusCheckItem, 1);

	statusCheckItem = new StatusCheckItem(PREVIEW_REACHABLE, false, "Preview reachable", statusDim);	
	addStatusItem(playoutPanel, statusCheckItem, 2);

	statusCheckItem = new StatusCheckItem(PLAYOUT_OK, true, "Playout OK", statusDim);
	addStatusItem(playoutPanel, statusCheckItem, 3);

	statusCheckItem = new StatusCheckItem(PREVIEW_OK, true, "Preview OK", statusDim);
	addStatusItem(playoutPanel, statusCheckItem, 4);
	

	if (stationConfig.isMonitorServiceActive()){
	    statusCheckItem = new StatusCheckItem(MONITOR_REACHABLE, false, "Monitor reachable", statusDim);
	    addStatusItem(playoutPanel, statusCheckItem, 5);
	    
	    statusCheckItem = new StatusCheckItem(MONITOR_IN, false, "Monitor in", statusDim);
	    addStatusItem(playoutPanel, statusCheckItem, 6);
	    
	    statusCheckItem = new StatusCheckItem(MONITOR_OUT,true, "Monitor out", statusDim);
	    addStatusItem(playoutPanel, statusCheckItem, 7);
	}

	addToDiagnosticsPanel(playoutPanel);

	/* Recording */

	JPanel recordingPanel = new JPanel();
	prepareDiagnosticsPanel(recordingPanel, "Recording check", recordingStatusVF);
	
	statusCheckItem = new StatusCheckItem(ARCHIVER_REACHABLE, false, "Archiver reachable", statusDim);
	addStatusItem(recordingPanel, statusCheckItem, 1);

	statusCheckItem = new StatusCheckItem(ARCHIVER_OK, false, "Archiver OK", statusDim);
	addStatusItem(recordingPanel, statusCheckItem, 2);
	
	addToDiagnosticsPanel(recordingPanel);
	
	/* Library */
	
	JPanel libraryPanel = new JPanel();
	libraryStatusVF = (int)statusItemVF * (5 + diagnosticsController.getNumUniqueStores());
	prepareDiagnosticsPanel(libraryPanel, "Library check", libraryStatusVF);
	
	statusCheckItem = new StatusCheckItem(LIBRARY_REACHABLE, false, "Library reachable", statusDim);
	addStatusItem(libraryPanel, statusCheckItem, 1);
	
	statusCheckItem = new StatusCheckItem(UPLOAD_REACHABLE, false, "Upload reachable", statusDim);	
	addStatusItem(libraryPanel, statusCheckItem, 2);

	statusCheckItem = new StatusCheckItem(LIBRARY_OK, false, "Library OK", statusDim);
	addStatusItem(libraryPanel, statusCheckItem, 3);

	statusCheckItem = new StatusCheckItem(UPLOAD_OK, false, "Upload OK", statusDim);
	addStatusItem(libraryPanel, statusCheckItem, 4);
	

	StationNetwork network = stationConfig.getStationNetwork();
	String[] uniqueStores = network.getUniqueStores();
	String[] stores = network.getStores();

	for (int i = 0; i < uniqueStores.length; i++){
	    String uniqueStore = uniqueStores[i];
	    ArrayList<String> services = new ArrayList<String>();
	    for (String store: stores)
		if ((network.getStoreMachine(store)+network.getStoreDir(store)).equals(uniqueStore))
		    services.add(store);

	    String store = services.get(0);
	    String machine = network.getStoreMachine(store);
	    statusCheckItem = new StatusCheckItem(DISKSPACE_OK + machine, false, "Diskspace available", statusDim);
	    addStatusItem(libraryPanel, statusCheckItem, 5+i);
	}
	addToDiagnosticsPanel(libraryPanel);

	/* Mic */

	if (micEnabled = stationConfig.isMicServiceActive()) {
	    JPanel micPanel = new JPanel();
	    prepareDiagnosticsPanel(micPanel, "Mic check", micStatusVF);

	    statusCheckItem = new StatusCheckItem(MIC_REACHABLE, false, "Mic service reachable", statusDim);
	    addStatusItem(micPanel, statusCheckItem, 1);

	    statusCheckItem = new StatusCheckItem(MIC_IN, false, "Mic service input OK", statusDim);
	    addStatusItem(micPanel, statusCheckItem, 2);

	    statusCheckItem = new StatusCheckItem(MIC_OK, false, "Mic OK", statusDim);
	    addStatusItem(micPanel, statusCheckItem, 3);

	    addToDiagnosticsPanel(micPanel);
	}
	
	/* Telephony */
	
	if (stationConfig.isServiceActive(RSController.ONLINE_TELEPHONY_SERVICE)){
	    JPanel telephonyPanel = new JPanel();
	    prepareDiagnosticsPanel(telephonyPanel, "Telephony check", telephonyStatusVF);

	    statusCheckItem = new StatusCheckItem(ONLINETELEPHONY_REACHABLE, false, "Telephony reachable", statusDim);
	    addStatusItem(telephonyPanel, statusCheckItem, 1);
	    
	    
	    statusCheckItem = new StatusCheckItem(ONLINETELEPHONY_OK, false, "Telephony OK", statusDim);
	    addStatusItem(telephonyPanel, statusCheckItem, 2);
	    

	    addToDiagnosticsPanel(telephonyPanel);
	}
	


	RSLabel blankLabel = new RSLabel("");
	systemDiagEntriesPanel.add(blankLabel, getGbc(0, ++panelCount, gridwidth(5), weighty(100.0)));

	
	JPanel bottomPanel = new JPanel();
	bottomPanel.setBackground(itemBgColorAlt);
	bottomPanel.setLayout(new GridBagLayout());
	bottomPanel.setPreferredSize(new Dimension((int)(wpSize.getWidth()), (int)(wpSize.getHeight() * statusItemVF / 100.0)));


	Dimension buttonDim = new Dimension((int)(statusDim.getWidth() * detailsButtonHF * 2/ 100.0),
					    (int)(statusDim.getHeight()));
	/*
	blankLabel = new RSLabel("");
	blankLabel.setPreferredSize(buttonDim);
	bottomPanel.add(blankLabel, getGbc(0,0));
	*/
	diagnosticsButton = getBottomPanelButton(DIAGNOSTICS, StationConfiguration.DIAGNOSTICS_ICON, "System diagnostics", buttonDim);
	bottomPanel.add(diagnosticsButton, getGbc(0, 0));
	
	networkButton = getBottomPanelButton(NETWORK, StationConfiguration.NETWORK_ICON, "Network diagnostics", buttonDim);
	
	bottomPanel.add(networkButton, getGbc(2, 0));
	
	/*
	blankLabel = new RSLabel("");
	blankLabel.setPreferredSize(buttonDim);
	bottomPanel.add(blankLabel, getGbc(4,0));
	*/
	progressBar = new JProgressBar();
	progressBar.setBackground(itemBgColor);
	progressBar.setForeground(bgColor);
	bottomPanel.add(progressBar, getGbc(1, 0, weightx(100.0), gfillboth()));

	wpComponent.add(bottomPanel, getGbc(0, 1));
    }

    public void startProgressDisplay(final String status){
	SwingUtilities.invokeLater(new Runnable(){
		public void run(){
		    progressBar.setString(status + "...");
		    progressBar.setStringPainted(true);
		    progressBar.setIndeterminate(true);
		}
	    });
    }

    public void stopProgressDisplay(){
	progressBar.setIndeterminate(false);
	progressBar.setStringPainted(false);
    }

    public void disableInput(){
	diagnosticsButton.setEnabled(false);
	networkButton.setEnabled(false);
    }

    public void enableInput(){
	diagnosticsButton.setEnabled(true);
	networkButton.setEnabled(true);
    }

    RSButton getBottomPanelButton(String actionCommand, String iconName, String toolTipText, Dimension buttonDim){
	RSButton button = new RSButton("");
	button.setPreferredSize(buttonDim);
	button.setIcon(getIcon(iconName, (int)(buttonDim.getWidth()), (int)(buttonDim.getHeight())));
	button.setActionCommand(actionCommand);
	button.addActionListener(diagnosticsController);
	button.setToolTipText(toolTipText);
	button.setBackground(itemBgColor);

	return button;
    }
    
    int panelCount = 0;
    void addToDiagnosticsPanel(JPanel panel){
	systemDiagEntriesPanel.add(panel, getGbc(0, ++panelCount, weightx(1.0), gfillboth()));
    }

    void prepareDiagnosticsPanel(JPanel panel, String title, int panelVF) {
	Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	Color borderColor = bgColor.darker();

	panel.setBorder(BorderFactory.createLineBorder(borderColor));
	panel.setLayout(new GridBagLayout());

	Dimension titleDim = new Dimension((int)(wpSize.getWidth())/2, 
					   (int)(wpSize.getHeight() * statusItemVF / 100.0) - 2);
	
	RSLabel titleLabel = new RSLabel(title);
	titleLabel.setBackground(bgColor); 
	titleLabel.setOpaque(true);
	titleLabel.setPreferredSize(titleDim);

	panel.add(titleLabel, getGbc(0, 0, gridwidth(5), weightx(1.0), gfillboth()));

    }

    public void reinitAspComponent() {
	aspComponent.removeAll();

	aspComponent.setLayout(new GridBagLayout());

	Dimension rowDim = new Dimension((int)(aspComponent.getWidth()) - 2, (int)(aspComponent.getHeight() / 8));
	RSLabel titleLabel = new RSLabel("System Status");

	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	aspComponent.add(titleLabel, getGbc(0, 0, gridwidth(2), gfillboth()));

	addDiagnosticsLabel(PLAYOUT_OK, "Playout", UNTESTED, 1, rowDim);
	addDiagnosticsLabel(PREVIEW_OK, "Preview", UNTESTED, 2, rowDim);	
	addDiagnosticsLabel(ARCHIVER_OK, "Recording", UNTESTED, 3, rowDim);
	addDiagnosticsLabel(LIBRARY_OK, "Search", UNTESTED, 4, rowDim);
	addDiagnosticsLabel(UPLOAD_OK, "Upload", UNTESTED, 5, rowDim);
	int position = 5;
	if (micEnabled) {
	    addDiagnosticsLabel(MIC_OK, "Mic Ok", UNTESTED, 6, rowDim);
	    position++;
	}

	position++;
	RSLabel blankLabel = new RSLabel("");
	aspComponent.add(blankLabel, getGbc(0, position, gridwidth(2), weighty(100.0)));
    }

    protected void addDiagnosticsLabel(String checkItemID, String checkItemLabel, int status, int position, Dimension rowDim) {
	StatusCheckItem statusItem = statusItems.get(checkItemID);

	RSLabel label = new RSLabel(" "+checkItemLabel);
	label.setHorizontalAlignment(SwingConstants.LEFT);
	label.setPreferredSize(new Dimension((int)(rowDim.getWidth() * 0.7), (int)(rowDim.getHeight())));
	aspComponent.add(label, getGbc(0, position));
	
	RSLabel iconLabel = new RSLabel("");
	iconLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	Dimension iconDim;
	iconLabel.setPreferredSize(iconDim = new Dimension((int)(rowDim.getWidth() * 0.3), (int)(rowDim.getHeight())));
	aspComponent.add(iconLabel, getGbc(1, position));

	statusItem.setAspComponentIcon(iconLabel, iconDim);
	statusItem.setAspComponentStatus(status);
    }
    
    public void onMaximize(){

    }

    public void onMinimize() {

    }

    public void onUnload() {
	wpComponent.removeAll();

	diagnosticsController.deinit();
    }

    public RSButton getDiagnosticsButton() {
	return diagnosticsButton;
    }

    public RSButton getNetworkButton() {
	return networkButton;
    }

    public boolean isNetworkDisplay() {
	return networkDisplay;
    }

    public void displaySystemDiagnostics() {
	if (networkDisplay)
	    wpComponent.remove(networkDiagPanel);
	networkDisplay = false;
	wpComponent.add(systemDiagPanel, systemDiagConstraints);
	wpComponent.validate();
	wpComponent.repaint();
    }

    public void displayNetworkDiagnostics() {
	if (!networkDisplay)
	    wpComponent.remove(systemDiagPanel);
	networkDisplay = true;
	wpComponent.add(networkDiagPanel, networkDiagConstraints);
	wpComponent.validate();
	wpComponent.repaint();
    }

    public DrawablePanel getNetworkDiagPanel() {
	return networkDiagPanel;
    }

    public void setDetailsEnabled(String checkItemID, boolean enabled) {
	statusItems.get(checkItemID).getDetailsButton().setEnabled(enabled);
    }

    public void setAllDetailsEnabled(boolean enabled) {
	Enumeration<String> e = statusItems.keys();
	while (e.hasMoreElements()) {
	    setDetailsEnabled(e.nextElement(), enabled);
	}
    }

    public void setStatus(String checkItemID, int status, String errorCode, String message) {
	StatusCheckItem checkItem = statusItems.get(checkItemID);
	checkItem.setStatusLabel(status);
	checkItem.setErrorLabel(message);
	if (errorCode.equals(DiagnosticsController.DEPENDENT_ERROR) || errorCode.equals(DiagnosticsController.NO_ERROR))
	    checkItem.getDetailsButton().setEnabled(false);
	else {
	    checkItem.getDetailsButton().setEnabled(true);
	    // XXX should get longer description from inference engine
	    checkItem.setDetailsString(errorCode);
	}
    }

    public void addDetailsString(String checkItemID, String additionalErrorMessage) {
	statusItems.get(checkItemID).addDetailsString(additionalErrorMessage);
    }

    public String getDetailsString(String checkItemID) {
	return statusItems.get(checkItemID).getDetailsString();
    }

    private void addStatusItem(JPanel panel, StatusCheckItem checkItem, int pos) {
	panel.add(checkItem.getPanel(), getGbc(0, pos, weightx(1.0), gfillboth()));
	Color color;
	if (pos %2 == 1)
	    color = itemBgColor;
	else
	    color = itemBgColorAlt;

	checkItem.getPanel().setBackground(color);
    }

    public void refreshDisplay(){
	wpComponent.validate();
	wpComponent.repaint();
    }
    
    public boolean requiresRestart(){
	for (String key: statusItems.keySet())
	    if (key.endsWith("REACHABLE"))
		if (statusItems.get(key).getStatus() == NOTOK){
		    logger.error(statusItems.get(key).getItemName()+" is NOTOK. Suggesting restart.");
		    return true;
		}
	return false;
    }

    public class StatusCheckItem {
	String checkItemID;
	boolean requiresManualStart;
	String itemName;
	RSButton manualButton, detailsButton;
	RSLabel statusLabel, errorLabel;	
	int status;
	String detailsString = "";

	JPanel itemPanel;
	Dimension itemDim;

	public StatusCheckItem(String checkItemID, boolean requiresManualStart, String itemName, Dimension itemDim) {
	    this.checkItemID = checkItemID;
	    this.requiresManualStart = requiresManualStart;
	    this.itemName = itemName;
	    this.itemDim = itemDim;

	    statusItems.put(checkItemID, this);
	    init();
	}

	protected void init() {
	    itemPanel = new JPanel();
	    itemPanel.setLayout(new GridBagLayout());

	    Color buttonBgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	    
	    if (!requiresManualStart) {
		RSLabel titleLabel = new RSLabel(itemName);	
		titleLabel.setPreferredSize(new Dimension((int)(itemDim.getWidth() * (checkTypeHF + manualButtonHF) / 100.0), 
							  (int)(itemDim.getHeight())));
		itemPanel.add(titleLabel, getGbc(0, 0, gridwidth(2)));
	    } else {
		RSLabel titleLabel = new RSLabel(itemName);
		titleLabel.setPreferredSize(new Dimension((int)(itemDim.getWidth() * (checkTypeHF + manualButtonHF) / 100.0),
							  (int)(itemDim.getHeight())));
		itemPanel.add(titleLabel, getGbc(0, 0, gridwidth(2)));
	    }
	
	    statusLabel = new RSLabel("");

	    statusLabel.setPreferredSize(new Dimension((int)(itemDim.getWidth() * statusHF / 100.0),
	    				       (int)(itemDim.getHeight())));
	    statusLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    itemPanel.add(statusLabel, getGbc(2, 0));

	    errorLabel = new RSLabel("");
	    //errorLabel.setPreferredSize(new Dimension((int)(itemDim.getWidth() * messageHF / 100.0),
	    //				      (int)(itemDim.getHeight())));
	    itemPanel.add(errorLabel, getGbc(3, 0, weightx(1.0), gfillboth()));

	    detailsButton = new RSButton("Details");
	    detailsButton.setPreferredSize(new Dimension((int)(itemDim.getWidth() * detailsButtonHF / 100.0),
							 (int)(itemDim.getHeight())));
	    detailsButton.setBackground(buttonBgColor);
	    detailsButton.setActionCommand(DETAILS + ":" + checkItemID);
	    detailsButton.addActionListener(diagnosticsController);

	    itemPanel.add(detailsButton, getGbc(4, 0));

	    status = UNTESTED;
	    setStatusLabel(status);
	}
	
	public String getItemName() {
	    return itemName;
	}

	public JPanel getPanel() {
	    return itemPanel;
	}

	public RSButton getManualButton() {
	    return manualButton;
	}

	public RSLabel getErrorLabel() {
	    return errorLabel;
	}

	public void setErrorLabel(String errorString) {
	    errorLabel.setText(errorString);
	    errorLabel.repaint();
	}

	public RSLabel getStatusLabel() {
	    return statusLabel;
	}

	public RSButton getDetailsButton() {
	    return detailsButton;
	}

	public String getDetailsString() {
	    return detailsString;
	}

	public int getStatus(){
	    return status;
	}
	
	public void setDetailsString(String detailsString) {
	    this.detailsString = detailsString;
	}

	public void addDetailsString(String additionalDetailsString) {
	    this.detailsString = this.detailsString + ":" + additionalDetailsString;
	}

	public void setStatusLabel(int status) {
	    this.status = status;
	    setStatusLabel(status, statusLabel, (int)(itemDim.getWidth() * statusHF / 100.0), (int)(itemDim.getHeight()));
	    setAspComponentStatus(status);
	    setNetworkStatus(status);
	}

	protected void setStatusLabel(int status, RSLabel label, int width, int height) {

	    if (status == OK) {
		label.setIcon(getIcon(StationConfiguration.OK_ICON, width, height)); 
		label.setToolTipText("OK");
	    } else if (status == NOTOK) {
		label.setIcon(getIcon(StationConfiguration.NOTOK_ICON, width, height));
		label.setToolTipText("Not OK!");
	    } else if (status == UNTESTED) {
		label.setIcon(getIcon(StationConfiguration.UNKNOWN_ICON, width, height));
		label.setToolTipText("Untested");
	    } else {
		label.setIcon(getIcon(StationConfiguration.CRITICAL_ICON, width, height));
		label.setToolTipText("Critical: Unable to determine error");
	    }

	    label.repaint();
	}

	Dimension aspIconDim;
	RSLabel aspIconLabel;
	public void setAspComponentIcon(RSLabel iconLabel, Dimension iconDim) {
	    this.aspIconDim = iconDim;
	    this.aspIconLabel = iconLabel;
	}
	
	public void setAspComponentStatus(int status) {
	    if (aspIconDim != null && aspIconLabel != null) {
		setStatusLabel(status, aspIconLabel, (int)(aspIconDim.getWidth()), (int)(aspIconDim.getHeight()));
	    }
	}

	Dimension networkStatusDim;
	RSLabel networkStatusLabel;
	public void setNetworkStatusLabel(RSLabel networkStatusLabel, Dimension networkStatusDim) {
	    this.networkStatusDim = networkStatusDim;
	    this.networkStatusLabel = networkStatusLabel;
	}

	public void setNetworkStatus(int status) {
	    if (networkStatusDim != null && networkStatusLabel != null) {
		setStatusLabel(status, networkStatusLabel, (int)(networkStatusDim.getWidth()), (int)(networkStatusDim.getHeight()));
	    }
	}
    }

    public class GridCoord {
	int x, y;

	public GridCoord(int x, int y) {
	    this.x = x;
	    this.y = y;
	}

	public int getX() {
	    return x;
	}

	public int getY() {
	    return y;
	}
    }

    public class DrawablePanel extends JPanel {
	Hashtable<String, GridCoord> lineStartHash, lineEndHash;
	Hashtable<String, Color> lineColorHash;

	int thickness = 1;

	public DrawablePanel() {
	    super();
	    lineStartHash = new Hashtable<String, GridCoord>();
	    lineEndHash = new Hashtable<String, GridCoord>();
	    lineColorHash = new Hashtable<String, Color>();
	}

	protected synchronized void paintComponent(Graphics g) {
	    super.paintComponent(g);

	    Enumeration<String> e = lineStartHash.keys();
	    while (e.hasMoreElements()) {
		String machineID = e.nextElement();
		GridCoord startCoord = lineStartHash.get(machineID);
		GridCoord endCoord = lineEndHash.get(machineID);
		Color lineColor = lineColorHash.get(machineID);
		logger.debug("paintComponent: Drawing " + startCoord.getX() + ":" + startCoord.getY() + ":" + endCoord.getX() + ":" + 
			     endCoord.getY());
		for (int thicknessX = -thickness; thicknessX <= thickness; thicknessX++) {
		    for (int thicknessY = thickness; thicknessY >= -thickness; thicknessY--) {
			drawLine(g, startCoord.getX() + thicknessX, startCoord.getY() + thicknessY, 
				 endCoord.getX() + thicknessX, endCoord.getY() + thicknessY, lineColor);
		    }
		}
	    }
	}

	protected void drawLine(Graphics g, int startX, int startY, int endX, int endY, Color lineColor) {
	    if (g != null) {
		logger.debug("drawLine: Graphics was not null. Drawing");
		Color c = g.getColor();
		g.setColor(lineColor);
		g.drawLine(startX, startY, endX, endY);
		g.setColor(c);
	    }
	}
	
	public synchronized void addLine(String machineID, int startX, int startY, int endX, int endY, int status) {
	    GridCoord startCoord = new GridCoord(startX, startY);
	    GridCoord endCoord = new GridCoord(endX, endY);
	    lineStartHash.put(machineID, startCoord);
	    lineEndHash.put(machineID, endCoord);
	    if (status == OK)
		lineColorHash.put(machineID, Color.green);
	    else
		lineColorHash.put(machineID, Color.red);
	}
    }
    
}