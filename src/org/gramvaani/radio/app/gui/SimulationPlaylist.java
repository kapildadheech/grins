package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.services.ArchiverService;

import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

public class SimulationPlaylist implements ListModel, CacheObjectListener {
    ArrayList<SimulationWidget.SimulationItemWidget> elements;
    ArrayList<RadioProgramMetadata> programs;
    ArrayList<ListDataListener> listeners;
    CacheProvider cacheProvider;
    SimulationController simulationController;

    public SimulationPlaylist(SimulationController simulationController) {
	elements = new ArrayList<SimulationWidget.SimulationItemWidget>();
	programs = new ArrayList<RadioProgramMetadata>();
	listeners = new ArrayList<ListDataListener>();
	this.simulationController = simulationController;
    }

    public void setCacheProvider(CacheProvider cacheProvider) {
	this.cacheProvider = cacheProvider;
    }

    public void addPrograms(RadioProgramMetadata[] programsMetadata, int index) {
	int i = index;
	for (RadioProgramMetadata program: programsMetadata) {
	    SimulationWidget.SimulationItemWidget itemWidget = simulationController.getSimulationWidget().getNewSimulationItemWidget(program);
	    programs.add(i, program);
	    elements.add(i, itemWidget);
	    cacheProvider.addCacheObjectListener(program, this);
	    i++;
	}

	for (ListDataListener listener: listeners) {
	    listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, index, index + programsMetadata.length));
	}
    }

    public void addItems(ArrayList<SimulationWidget.SimulationItemWidget> itemList, int index){
	int i = index;
	for (SimulationWidget.SimulationItemWidget itemWidget: itemList){
	    programs.add(i, itemWidget.getMetadata());
	    elements.add(i, itemWidget);
	    //cacheProvider.addCacheObjectListener(itemWidget.getMetadata(), this);
	    i++;
	}

	for (ListDataListener listener: listeners) {
	    listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, index, index + itemList.size()));
	}

    }

    public void removeProgramsAt(int[] selectionIndices, boolean removeCacheListener) {
	if (selectionIndices.length == 0)
	    return;

	RadioProgramMetadata metadata;
	for (int i = 0; i < selectionIndices.length; i++) {
	    elements.remove(selectionIndices[i] - i);
	    metadata = programs.remove(selectionIndices[i] - i);
	    if (removeCacheListener)
		cacheProvider.removeCacheObjectListener(metadata, this);
	}
	for (ListDataListener listener: listeners){
	    listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, selectionIndices[0], 
						       selectionIndices[selectionIndices.length - 1]));
	}
    }

    public RadioProgramMetadata[] getPrograms() {
	return programs.toArray(new RadioProgramMetadata[]{});
    }

    public void loadPlaylist(Playlist playlist) {
	int len = elements.size();
	String[] programIDs = playlist.getPrograms();
	PlaylistItem[] playlistItems = playlist.getPlaylistItems();
	
	int i = 0;
	for (String programID: programIDs) {
	    RadioProgramMetadata programMetadata =
		(RadioProgramMetadata)(simulationController.getCacheProvider().getSingle(RadioProgramMetadata.getDummyObject(programID)));
	    programs.add(programMetadata);

	    SimulationWidget.SimulationItemWidget itemWidget = 
		simulationController.getSimulationWidget().getNewSimulationItemWidget(programMetadata);
	    elements.add(itemWidget);

	    if (programID.startsWith(SimulationWidget.GOLIVE_ITEMID) || playlistItems[i].getLiveItem()) {
		itemWidget.isLiveItem(playlistItems[i].getLiveItem());
		itemWidget.playBackgroundMusic(playlistItems[i].getPlayBackground());
		itemWidget.setGoliveDuration(playlistItems[i].getLiveDuration());
	    } else {
		cacheProvider.addCacheObjectListener(programMetadata, this);
	    }

	    itemWidget.setFadeoutDuration(playlistItems[i].getFadeout());

	    i++;
	}

	for (ListDataListener listener: listeners) {
	    listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, len, elements.size() - 1));
	}
    }

    public void clearPlaylist() {
	for (ListDataListener listener: listeners) {
	    listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, 0, elements.size() - 1));
	}
	clearCacheObjectListeners();
	programs.clear();
	elements.clear();
    }

    public void clearCacheObjectListeners() {
	for (RadioProgramMetadata metadata: programs)
	    cacheProvider.removeCacheObjectListener(metadata, this);
    }

    public void removeItem(String programID) {
	int index = 0;
	for (RadioProgramMetadata program: programs) {
	    if (program.getItemID().equals(programID)) {
		removeProgramsAt(new int[]{index}, true);
		break;
	    }
	    index++;
	}
    }

    public void initFadeout(int fadeoutMilli) {
	for (SimulationWidget.SimulationItemWidget itemWidget: elements)
	    if (!itemWidget.getMetadata().getType().equals(ArchiverService.BCAST_MIC))
		itemWidget.setFadeoutDuration(fadeoutMilli);
    }

    public ArrayList<SimulationWidget.SimulationItemWidget> getElements() {
	return elements;
    }

    public void addListDataListener(ListDataListener listener){
	listeners.add(listener);
    }
    
    public Object getElementAt(int index){
	if (index < elements.size())
	    return elements.get(index);
	else
	    return null;
    }

    public String getProgramAt(int index) {
	if (index < elements.size())
	    return elements.get(index).getMetadata().getItemID();
	else
	    return null;
    }

    public int getSize(){
	return elements.size();
    }
    
    public void removeListDataListener(ListDataListener listener){
	listeners.remove(listener);
    }

    //XXX: It seems possible that there are multiple entries in the list
    //with the same metadata. Would the call back happen for each entry in the list?
    //If not then there is a bug here.
    public void cacheObjectValueChanged(Metadata metadata) {
	boolean programFound = false;
	int i;
	for (i = 0; i < programs.size(); i++) {
	    RadioProgramMetadata programMetadata = programs.get(i);
	    if (programMetadata.getItemID().equals(((RadioProgramMetadata)metadata).getItemID())) {
		SimulationWidget.SimulationItemWidget itemWidget = elements.get(i);
		itemWidget.setMetadata((RadioProgramMetadata)metadata);
		programs.set(i, (RadioProgramMetadata)metadata);
		programFound = true;
		break;
	    }
	}

	if (programFound) {
	    for (ListDataListener listener: listeners) {
		listener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, i, i));
	    }
	}
    }

    public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKeys) {
	for (int j = 0; j < metadata.length; j++) {
	    boolean programFound = false;
	    String newItemID = newKeys[j].substring(0, newKeys[j].lastIndexOf("."));
	    RadioProgramMetadata dummyProgram, newProgram=null;
	    Metadata mdata;
	    if (!newItemID.equals("")) {
		dummyProgram = RadioProgramMetadata.getDummyObject(newItemID);
		mdata = cacheProvider.getSingle(dummyProgram);
		if (mdata == null)
		    return;
		newProgram = (RadioProgramMetadata)mdata;
	    }
	    ArrayList<Integer> indicesToDelete = new ArrayList<Integer>();
	    int i;
	    for (i = 0; i < programs.size(); i++) {
		RadioProgramMetadata programMetadata = programs.get(i);
		if (programMetadata.getItemID().equals(((RadioProgramMetadata)metadata[j]).getItemID())) {
		    if (!newItemID.equals("")) {
			SimulationWidget.SimulationItemWidget itemWidget = elements.get(i);
			itemWidget.setMetadata(newProgram);
			programs.set(i, newProgram);
			indicesToDelete.add(i);
			programFound = true;
		    } else {
			indicesToDelete.add(i);
		    }
		}
	    }
	
	    int[] delIndices = new int[indicesToDelete.size()];
	    i = 0;
	    for (Integer index : indicesToDelete) {
		delIndices[i++] = (int)index;
	    }

	    if (!newItemID.equals("")) {
		if (programFound) {
		    for (int index : delIndices) {
			for (ListDataListener listener: listeners) {
			    listener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, index, index));
			}
		    }
		}
	    } else {
		simulationController.removeProgramsAt(delIndices, true);
	    }
	}//For loop over metadata
    }


}