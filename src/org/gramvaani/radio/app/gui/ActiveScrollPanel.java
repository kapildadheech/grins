package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;

import javax.swing.*;

import java.util.Hashtable;
import java.util.HashSet;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

class ActiveScrollPanel extends JScrollPane {
    Hashtable<RSWidgetBase, ASPItem> widgetItems;
    Hashtable<ASPItem, RSWidgetBase> widgets;

    static final int activeElementHorizFrac = 20;
    final int border = (int)verticalScrollBar.getMaximumSize().getWidth() + 2;

    JPanel view;
    
    protected ControlPanelJFrame cpJFrame;
    protected StationConfiguration stationConfig;

    MouseAdapter adapter;
    Dimension size;
    
    JViewport viewport;
    Dimension paneSize;

    public ActiveScrollPanel(final ControlPanelJFrame cpJFrame){
	super();
	this.cpJFrame = cpJFrame;
	this.stationConfig = cpJFrame.getControlPanel().getRSApp().getStationConfiguration();
	widgetItems = new Hashtable<RSWidgetBase, ASPItem>();
	widgets = new Hashtable<ASPItem, RSWidgetBase>();
	adapter = new MouseAdapter(){
		public void mouseClicked(MouseEvent e){
		    RSWidgetBase widget = widgets.get(e.getSource());
		    if(widget.isLaunched())
			cpJFrame.setWidgetMaximized(widget);
		}
	    };

	view = new JPanel();
	view.setLayout(new BoxLayout(view, BoxLayout.PAGE_AXIS));
	setViewportView(view);
	setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	getVerticalScrollBar().setUnitIncrement(10);
	viewport = getViewport();

	view.setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));

	validate();
    }
    
    public void calculateComponentSize(int w, int h) {
	setPreferredSize(new Dimension(w, h));
	size = new Dimension(w - border, w);
    }

    public Dimension getComponentSize(){
	return size;
    }

    public void addWidget(RSWidgetBase widget){
	addWidget(widget, false);
    }

    public void setDefaultWidget(RSWidgetBase widget){
	addWidget(widget, true);
    }

    public void addWidget(RSWidgetBase widget, boolean isDefault){
	JComponent widgetComponent = widget.getActiveScrollComponent();
	ASPItem aspItem = new ASPItem(widget, size, !isDefault);
	widgetItems.put(widget, aspItem);
	widgets.put(aspItem, widget);
	aspItem.setSize(size);
	aspItem.setPreferredSize(size);
	aspItem.setMaximumSize(size);
	aspItem.setMinimumSize(size);
	aspItem.addMouseListener(adapter);
	if(isDefault)
	    view.add(aspItem, 0);
	else
	    view.add(aspItem);
	aspItem.setTransferHandler(new ASPTransferHandler(this, widget));

	paneSize = getSize();
	validate();
    }

    public void removeWidget(RSWidgetBase widget){
	ASPItem aspItem = widgetItems.remove(widget);
	if (aspItem != null) {
	    widgets.remove(aspItem);
	    view.remove(aspItem);
	}
	validate();
	repaint();
    }

    public void widgetLaunched(RSWidgetBase widget){
	widgetItems.get(widget).launched();
    }

    public void maximizeWidget(RSWidgetBase widget){
	ASPItem aspItem = widgetItems.get(widget);
	aspItem.setMaximized();

	Point p = aspItem.getLocation();
	if(p.y > viewport.getViewPosition().y+paneSize.height - size.height)
	    viewport.setViewPosition(new Point(0, p.y + size.height/2 - paneSize.height/2));
	if(p.y < viewport.getViewPosition().y)
	    viewport.setViewPosition(new Point(0, p.y + size.height/2 - paneSize.height/2));
	
	repaint();
    }

    public void minimizeWidget(RSWidgetBase widget){
	ASPItem aspItem;
	if((aspItem = widgetItems.get(widget)) != null){
	    aspItem.setMinimized();
	    repaint();
	}
    }

    protected void handleDrop(RSWidgetBase widget){
	cpJFrame.setWidgetMaximized(widget);
    }

    protected void closeWidget(RSWidgetBase widget){
	cpJFrame.unloadWidget(widget);
    }

    public int getWidgetCount(){
	return widgets.size();
    }

    class ASPItem extends JPanel{
	final RSWidgetBase widget;
	JComponent widgetComponent;
	static final int cbHorizFrac = 12;
	static final int titleVertFrac = 10;

	Color borderColor;
	Color selectedBorderColor;
	Color backgroundColor, selectedBackgroundColor;

	public ASPItem (final RSWidgetBase widget, Dimension size, boolean isCloseable){
	    Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);

	    this.widget = widget;
	    JLabel emptyLabel = new JLabel("Not Loaded.");
	    emptyLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    widgetComponent = (JComponent) emptyLabel;
	    setLayout(new BorderLayout());
	    JLabel label = new JLabel(widget.getName());
	    JPanel titlePanel = new JPanel();

	    titlePanel.setBackground(bgColor);

	    int titleHeight = titleVertFrac * size.height/100;
	    int titleWidth = size.width;
	    int cbWidth = titleWidth * cbHorizFrac/100;
	    titlePanel.setLayout(new GridBagLayout());
	    titlePanel.setSize(titleWidth, titleHeight);
	    
	    label.setSize(titleWidth - cbWidth, titleHeight);
	    label.setHorizontalAlignment(SwingConstants.CENTER);

	    titlePanel.add(label, getGbc(0, 0, gfillboth(), weightx(1.0)));

	    JButton closeButton = new JButton("");
	    closeButton.setEnabled(isCloseable);
	    closeButton.setRequestFocusEnabled(false);
	    closeButton.setPreferredSize(new Dimension(cbWidth, titleHeight));
	    closeButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e){
			closeWidget(widget);
		    }
		});

	    titlePanel.add(closeButton, getGbc(1, 0));

	    closeButton.setBackground(bgColor);
	    closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON, (int)cbWidth, (int)titleHeight));

	    borderColor = stationConfig.getColor(StationConfiguration.STD_BORDER_COLOR);
	    selectedBorderColor = stationConfig.getColor(StationConfiguration.STD_SELECTED_BORDER_COLOR);
	    backgroundColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_BGCOLOR);
	    selectedBackgroundColor = stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR);

	    widget.getActiveScrollComponent().setBackground(selectedBackgroundColor);
	    //	    titlePanel.setBorder(BorderFactory.createLineBorder(selectedBorderColor));

	    add(titlePanel, BorderLayout.PAGE_START);
	    add(widgetComponent, BorderLayout.CENTER);
	}

	public void launched(){
	    remove(widgetComponent);
	    widgetComponent = widget.getActiveScrollComponent();
	    add(widgetComponent, BorderLayout.CENTER);
	    validate();
	}

	public void setMaximized(){
	    setBorder(BorderFactory.createLineBorder(selectedBorderColor));
	    widget.getActiveScrollComponent().setBackground(selectedBackgroundColor);
	}

	public void setMinimized(){
	    setBorder(BorderFactory.createLineBorder(borderColor));
	    widget.getActiveScrollComponent().setBackground(backgroundColor);
	}

    }

    class ASPTransferHandler extends TransferHandler {
	final RSWidgetBase widget;
	//final DataFlavor aspFlavors[], wpFlavors[];
	final HashSet<DataFlavor> supportedFlavors, aspFlavors;
	final ActiveScrollPanel asp;

	public ASPTransferHandler(ActiveScrollPanel asp, RSWidgetBase widget){
	    super();
	    this.widget = widget;
	    this.asp = asp;

	    DataFlavor aspFlavorsArr[] = widget.getASPDataFlavors();
	    DataFlavor wpFlavors[]  = widget.getWPDataFlavors();

	    aspFlavors = new HashSet<DataFlavor>();
	    supportedFlavors = new HashSet<DataFlavor>();

	    for(DataFlavor flavor: aspFlavorsArr){
		aspFlavors.add(flavor);
		supportedFlavors.add(flavor);
	    }

	    for(DataFlavor flavor: wpFlavors)
		supportedFlavors.add(flavor);
	}
	
	public boolean canImport(TransferHandler.TransferSupport support){
	    if(!support.isDrop())
		return false;
	    
	    for(DataFlavor flavor: support.getDataFlavors()){
		if(supportedFlavors.contains(flavor)){
		    asp.handleDrop(widget);
		}
		if(aspFlavors.contains(flavor))
		    return true;
	    }

	    return false;
	}
    }
    
}

