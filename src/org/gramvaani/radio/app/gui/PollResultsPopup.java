package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.medialib.Poll;
import org.gramvaani.radio.medialib.PollOption;
import org.gramvaani.radio.medialib.Vote;
import org.gramvaani.utilities.*;

import org.jfree.data.*;
import org.jfree.chart.*;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;

import java.awt.image.BufferedImage;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.Document;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class PollResultsPopup extends JFrame {
    Poll poll;
    PollOption[] options;
    Vote[] votes;
    Hashtable<String, Integer> results;

    PollResultsListener listener;
    LogUtilities logger = new LogUtilities("PollResultsPopup");
    RSPanel resultsPanel;
    RSLabel titleLabel, chartLabel;
    RSApp rsApp;
    StationConfiguration stationConfig;
    Color bgColor;
    Dimension size;

    int titleVertFrac = 8;
    int buttonPanelVertFrac = 8;
    int labelHorizFrac = 30;
    int labelVertFrac = 8;
    
    int chartHeight;

    
    public PollResultsPopup(PollResultsListener listener, int width, int height, Color bgColor, Color titleColor, StationConfiguration stationConfig, RSApp rsApp){
	this.listener = listener;
	this.rsApp    = rsApp;
	this.stationConfig = stationConfig;
	this.bgColor = bgColor;

	resultsPanel = new RSPanel();

	size = new Dimension(width, height);
	resultsPanel.setBorder(BorderFactory.createLineBorder(bgColor.darker()));
	resultsPanel.setPreferredSize(size);
	resultsPanel.setLayout(new BorderLayout());

	int titleHeight = titleVertFrac * size.height/100;
	int buttonPanelHeight = buttonPanelVertFrac * size.height/100;
	chartHeight = size.height - titleHeight - buttonPanelHeight;

	Dimension titleSize = new Dimension(size.width, titleHeight);
	titleLabel = new RSLabel("");
	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	titleLabel.setPreferredSize(titleSize);
	titleLabel.setOpaque(true);
	titleLabel.setBackground(titleColor);
	Font f = titleLabel.getFont();
	titleLabel.setFont(f.deriveFont(f.getStyle() ^ Font.BOLD));
	resultsPanel.add(titleLabel, BorderLayout.NORTH);
	
	Dimension buttonPanelSize = new Dimension(size.width, buttonPanelHeight);
	RSPanel buttonPanel = new RSPanel();
	buttonPanel.setBackground(bgColor);
	buttonPanel.setPreferredSize(buttonPanelSize);
	resultsPanel.add(buttonPanel, BorderLayout.SOUTH);

	int buttonHorizFrac = 25;
	int buttonWidth = buttonHorizFrac * size.width/100;
	int buttonGap = 4;
	Dimension buttonSize = new Dimension(buttonWidth, buttonPanelSize.height - buttonGap * 2);
	int iconSize = (buttonSize.height * 8)/10;
	buttonPanel.setLayout(new GridBagLayout());

	RSButton exportButton = new RSButton("Export Results");
	exportButton.setPreferredSize(buttonSize);
	exportButton.setBackground(bgColor);
	exportButton.setIcon(IconUtilities.getIcon(StationConfiguration.MAKE_REPORT_ICON, iconSize));
	exportButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    exportResults();
		}
	    });
	buttonPanel.add(exportButton);
	
	RSLabel emptyLabel = new RSLabel(" ");
	buttonPanel.add(emptyLabel);

	RSButton removeButton = new RSButton("Remove Votes");
	removeButton.setPreferredSize(buttonSize);
	removeButton.setBackground(bgColor);
	removeButton.setIcon(IconUtilities.getIcon(StationConfiguration.REMOVE_ICON, iconSize));
	removeButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    removeResults();
		}
	    });
	buttonPanel.add(removeButton);
	
	emptyLabel = new RSLabel(" ");
	buttonPanel.add(emptyLabel);

	RSButton cancelButton = new RSButton("Cancel");
	cancelButton.setPreferredSize(buttonSize);
	cancelButton.setBackground(bgColor);
	cancelButton.setIcon(IconUtilities.getIcon(StationConfiguration.CANCEL_ICON, iconSize));
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    closeResults();
		}
	    });
	buttonPanel.add(cancelButton);
	
	KeyListener escapeListener = new KeyAdapter(){
		public void keyPressed(KeyEvent e){
		    if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			closeResults();
		    }
		}
	    };

	
	for (JComponent c: new JComponent[]{resultsPanel, buttonPanel}){
	    c.addKeyListener(escapeListener);
	}

	addWindowFocusListener(new WindowAdapter(){
		public void windowLostFocus(WindowEvent e){
		    closeResults();
		}
	    });
	add(resultsPanel);

	setUndecorated(true);
	pack();
    }

    void exportResults() {
	if(results.keySet().size() == 0)
	    return;

	String separator = "\t";
	ArrayList<String> resultList = new ArrayList<String>();
	
	JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	fileChooser.updateUI();
	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int retVal = fileChooser.showDialog(rsApp.getControlPanel().cpJFrame(), "Export Results");

	if (retVal != JFileChooser.APPROVE_OPTION)
	    return;

	String destFolder = null;
	try{
	    destFolder = fileChooser.getSelectedFile().getCanonicalPath() + System.getProperty("file.separator");
	} catch (Exception e){
	    logger.error("ExportResults: Unable to find canonical path of selected folder.");
	    return;
	}

	String destFile = destFolder + poll.getPollName() + ".xls";
	resultList.add("Option" + separator + "Votes");
	for(String key :results.keySet())
	    resultList.add(key + separator + results.get(key));
	resultList.add("" + separator + "");
	resultList.add("Total" + separator + votes.length);
	
	if(FileUtilities.writeToXLS(destFile, resultList, separator, true)) {
	    FileUtilities.setPermissions(destFile, false);
	    JOptionPane.showMessageDialog(null, "Results exported to file: " + destFile, "information", JOptionPane.INFORMATION_MESSAGE);
	} else {
	    JOptionPane.showMessageDialog(null, "Results export failed!!", "information", JOptionPane.INFORMATION_MESSAGE);
	}

    }

    
    void removeResults() {
	if(votes.length != 0) {
	    String confirm = "Are you sure you want to delete all " + votes.length + " votes of this poll?";
	    String title = "Remove votes";
	    int retVal = JOptionPane.showConfirmDialog(rsApp.getControlPanel().cpJFrame(), confirm, title, JOptionPane.YES_NO_OPTION);
	    if (retVal != JOptionPane.YES_OPTION)
		return;

	    listener.removeVotes(poll.getID());
	}	
    }

    void closeResults(){
	setVisible(false);
    }

    void showPopup(Poll poll, PollOption[] options, Vote[] votes) {
	this.poll = poll;
	this.options = options;
	this.votes = votes;

	titleLabel.setText(poll.getPollName() + " - " + votes.length + " vote(s)");

	if(votes.length != 0) {
	    if(poll.getPollType() == Poll.FIXED_OPTIONS)
		results = prepareResults(options, votes);
	    else
		results = prepareResults(votes);
	    
	    makePieChart(results);
	}

	setLocationRelativeTo(null);
	setVisible(true);
	resultsPanel.requestFocusInWindow();
    }

    void makePieChart(Hashtable <String, Integer> results) {
	DefaultPieDataset pieDataset = new DefaultPieDataset();

	for(String key :results.keySet())
	    pieDataset.setValue(key, results.get(key));

	PiePlot piePlot = new PiePlot(pieDataset);
	StandardPieSectionLabelGenerator labels = new StandardPieSectionLabelGenerator("{0} = {2}");
	piePlot.setLabelGenerator(labels);

	JFreeChart chart = new JFreeChart(piePlot);
	chart.removeLegend();

	Dimension chartSize = new Dimension(size.width, chartHeight);
	BufferedImage chartImage = chart.createBufferedImage(chartSize.width, chartSize.height);

	if(chartLabel != null)
	    resultsPanel.remove(chartLabel);

	chartLabel = new RSLabel("");
	chartLabel.setBackground(bgColor);
	chartLabel.setIcon(new ImageIcon(chartImage));
	resultsPanel.add(chartLabel, BorderLayout.CENTER);
    }

    PollOption getOption(PollOption[] options, int optionID) {
	for(PollOption option :options)
	    if(option.getID() == optionID)
		return option;

	return null;
    }

    Hashtable<String, Integer>  prepareResults(Vote[] votes) {
	Hashtable<String, Integer> results = new Hashtable<String, Integer>();

	for(Vote vote :votes) {
	    String optionText = vote.getPollOptionText();
	    Integer voteCount = results.get(optionText);
	    if(voteCount == null) {
		results.put(optionText, new Integer(1));
	    } else {
		results.put(optionText, voteCount + 1);
	    }	    
	}

	return results;
    }

    Hashtable<String, Integer> prepareResults(PollOption[] options, Vote[] votes) {
	Hashtable<String, Integer> results = new Hashtable<String, Integer>();
	
	for(Vote vote :votes) {
	    PollOption option = getOption(options, vote.getPollOptionID());
	    Integer voteCount = results.get(option.getPollOptionName());
	    if(voteCount == null) {
		results.put(option.getPollOptionName(), new Integer(1));
	    } else {
		results.put(option.getPollOptionName(), voteCount + 1);
	    }
	}

	return results;
    }

    public interface PollResultsListener {
	public void removeVotes(int pollID);
    }

}