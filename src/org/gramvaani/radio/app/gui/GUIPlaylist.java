package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.rscontroller.services.ArchiverService;
import org.gramvaani.utilities.LogUtilities;

import javax.swing.SwingUtilities;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListDataEvent;

import java.util.*;

public class GUIPlaylist implements ListModel, CacheObjectListener {
    ArrayList<String> ordering;
    ArrayList<PlaylistWidget.PlayItemWidget> listElements;
    ArrayList<ListDataListener> listeners;
    RadioProgramMetadata firstProgramMetadata;
    PlaylistWidget.PlayItemWidget firstProgramItemWidget;

    CacheProvider cacheProvider;
    PlaylistController playlistController;

    LogUtilities logger;
    int playlistID;
    String playlistName;
    //long playlistStartTime;

    int previewIndex;
    int newPreviewIndex = -1;

    public GUIPlaylist(PlaylistController playlistController) {
	ordering = new ArrayList<String>();
	listElements = new ArrayList<PlaylistWidget.PlayItemWidget>();
	listeners = new ArrayList<ListDataListener> ();

	logger = new LogUtilities("GUIPlaylist");

	previewIndex = -1;
	this.playlistController = playlistController;
    }

    public void dump() {
	logger.info("** guiplaylist dump: " + firstProgramMetadata + ":" + 
		   (firstProgramItemWidget != null ? firstProgramItemWidget.getMetadata().getItemID() : "null"));
	for (PlaylistWidget.PlayItemWidget playItemWidget: listElements)
	    logger.info("** guiplaylist dump: " + playItemWidget.getMetadata());

	dumpOrdering();
    }

    public void setPreviewIndex(int previewIndex) {
	this.previewIndex = previewIndex;
    }

    public void unsetPreviewIndex() {
	previewIndex = -1;
    }

    public synchronized int getPreviewIndex() {
	if (newPreviewIndex > -1){
	    previewIndex = newPreviewIndex;
	    newPreviewIndex = -1;
	}
	    
	//System.err.println("GET PREVIEW IND: "+previewIndex);
	return previewIndex;
    }

    protected void stopPreview(int newIndex) {
	//System.err.println("STOP PREVIEW: "+newIndex);
	newPreviewIndex = newIndex;
	if (previewIndex > -1) {
	    playlistController.stopPreview(previewIndex);
	    previewIndex = newIndex;
	}
    }

    protected void stopPreview(){
	stopPreview(-1);
    }
    

    // called only for index = 0
    protected void activatePreview(int index) {
	playlistController.activatePreview(index);
    }

    public void initFadeout(int fadeoutMilli) {
	if (firstProgramMetadata != null && !firstProgramMetadata.getType().equals(ArchiverService.BCAST_MIC))
	    if (firstProgramItemWidget != null)
		firstProgramItemWidget.setFadeoutDuration(fadeoutMilli);

	for (PlaylistWidget.PlayItemWidget itemWidget: listElements)
	    if (!itemWidget.getMetadata().getType().equals(ArchiverService.BCAST_MIC))
		itemWidget.setFadeoutDuration(fadeoutMilli);
    }

    public void setCacheProvider(CacheProvider cacheProvider) {
	this.cacheProvider = cacheProvider;
    }

    public void init(Playlist playlist, int index) {
	String[] programs = playlist.getPrograms();

	if (ordering.size() == index)
	    index--;

	if (previewIndex != -1 && previewIndex > index)
	    previewIndex += programs.length;

	for (String program: programs){
	    ordering.add(++index, program);
	}
    }

    public void initDetails(Playlist playlist){
	playlistID = playlist.getPlaylistID();
	playlistName = playlist.getName();
	//playlistStartTime = playlist.getStartTime();
    }

    public void dumpOrdering(){
	logger.info("DumpOrdering: "+ordering.size()+" listElements: "+listElements.size());
	for (String s: ordering)
	    logger.info("ordering "+s);
    }

    public int getPlaylistID() {
	return playlistID;
    }
    
    public String getPlaylistName() {
	return playlistName;
    }

    /*
    public long getStartTime() {
	return playlistStartTime;
    }

    public void setStartTime(long playlistStartTime) {
	this.playlistStartTime = playlistStartTime;
    }
    */

    public void setFirstProgram(RadioProgramMetadata program) {
	firstProgramMetadata = program;
	playlistController.updateFirstProgram(program);
    }

    public RadioProgramMetadata getFirstProgram() {
	return firstProgramMetadata;
    }

    public void setFirstProgramItemWidget(PlaylistWidget.PlayItemWidget widget) {
	this.firstProgramItemWidget = widget;
	cacheProvider.addCacheObjectListener(widget.getMetadata(), this);
    }

    public PlaylistWidget.PlayItemWidget getPlayItemWidget(String programID) {
	if (firstProgramItemWidget != null && firstProgramItemWidget.getMetadata().getItemID().equals(programID))
	    return firstProgramItemWidget;

	for (PlaylistWidget.PlayItemWidget playItemWidget: listElements)
	    if (playItemWidget.getMetadata().getItemID().equals(programID))
		return playItemWidget;
	
	return null;
    }

    public String firstProgram() {
	if (ordering.size() > 0)
	    return ordering.get(0);
	else
	    return null;
    }

    public void removeFirstProgram() {
	if (ordering.size() > 0) {
	    ordering.remove(0);
	    firstProgramMetadata = null;
	    firstProgramItemWidget = null;

	    if (previewIndex != -1) {
		if (previewIndex == 0) {
		    stopPreview();
		} else {
		    previewIndex--;
		    if (previewIndex == 0)
			activatePreview(0);
		}
	    }
	}

    }

    // XXX bad call
    public int getItemIndex(String programID) {
	for (int i = 0; i < ordering.size(); i++){
	    if (ordering.get(i).equals(programID))
		return i;
	}
	return -1;
    }

    // XXX bad call
    public RadioProgramMetadata getProgram(String programID){
	int index = getItemIndex(programID);
	
	if (index == 0 && firstProgramMetadata != null)
	    return firstProgramMetadata;
	else if (index == 0 && firstProgramMetadata == null)
	    return listElements.get(index).getMetadata();
	else
	    return listElements.get(index - 1).getMetadata();
    }

    public void moveToBeginning(int index) {
	if (index != -1) {
	    String programID = ordering.remove(index);
	    ordering.add(0, programID);

	    if (previewIndex != -1 && index == previewIndex) {
		previewIndex = 0;
		activatePreview(0);
	    }
	}
    }

    public synchronized int size() {
	return ordering.size();
    }

    public void clear() {
	stopPreview();

	clearCacheObjectListeners();

	int size = listElements.size();
	ordering.clear();
	listElements.clear();
	firstProgramMetadata = null;
	firstProgramItemWidget = null;

	for (ListDataListener listener: listeners){
	    listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, 0, size));
	}
    }

    PlaylistWidget.PlayItemWidget returnItemWidget = null;
    boolean returnItemPicked = false;
    Object returnItemLock = new Object();

    public PlaylistWidget.PlayItemWidget removeItemAt(final int i){
	final int index = i + (firstProgramMetadata == null ? 1 : 0);

	returnItemPicked = false;

	if (index > 0) {
	    try{
		Runnable remover = new Runnable(){
			
			public void run(){
			    returnItemWidget = listElements.remove(index - 1);
			    cacheProvider.removeCacheObjectListener(returnItemWidget.getMetadata(), GUIPlaylist.this);
			    
			    for (ListDataListener listener: listeners){
				listener.intervalRemoved(new ListDataEvent(GUIPlaylist.this, 
									   ListDataEvent.INTERVAL_REMOVED, 
									   index - 1, index - 1));
			    }

			    synchronized(returnItemLock){
				returnItemPicked = true;
				returnItemLock.notifyAll();
			    }

			}
		    };

		if (SwingUtilities.isEventDispatchThread())
		    remover.run();
		else
		    SwingUtilities.invokeLater(remover);
		
		
		synchronized(returnItemLock){
		    while (returnItemPicked == false){
			try{
			    returnItemLock.wait();
			} catch (InterruptedException ie){}
		    }
		}
		

	    } catch( Exception e ){
		logger.error("RemoveItemAt: Could not remove:", e);
	    }
	    
	    return returnItemWidget;

	} else {
	    // should not come here
	    logger.error("RemoveItemAt: Removing item at 0");
	}

	return null;
    }

    public ArrayList<String> getPrograms() {
	return ordering;
    }

    public Iterable<PlaylistWidget.PlayItemWidget> getListElements() {
	return listElements;
    }

    public PlaylistWidget.PlayItemWidget getItemAt(int index) {
	if (index == 0)
	    return null;
	else
	    return listElements.get(index - 1);
    }

    public PlaylistWidget.PlayItemWidget getItemAtNotNull(int index) {
	if (index == 0)
	    return firstProgramItemWidget;
	else
	    return listElements.get(index - 1);
    }

    // index is the list model index here. no need to subtract 1
    public synchronized void addItem(PlaylistWidget.PlayItemWidget widget, int index, boolean addToOrdering) {

	if (index < 0)
	    listElements.add(index + 1, widget);
	else
	    listElements.add(index, widget);

	cacheProvider.addCacheObjectListener(widget.getMetadata(), this);
	if (addToOrdering) {
	    ordering.add(index + 1, widget.getMetadata().getItemID());
	    if (previewIndex != -1 && previewIndex >= index + 1){
		previewIndex++;
	    }
	}

	for (ListDataListener listener: listeners){
	    if (index < 0)
		index++;

	    listener.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, index, index));
	}
    }

    // index is the list model index here. 
    public synchronized void removeProgramsAt(int indices[], boolean removeCacheListener, int newPreviewIndex) {
	for (int i = 0; i <indices.length; i++) {	    
	    /*
	    for (ListDataListener listener: listeners){
		listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, indices[i] - i, indices[i] - i));
	    }
	    */
	    Metadata metadata = listElements.remove(indices[i] - i).getMetadata();
	    
	    if (removeCacheListener)
		cacheProvider.removeCacheObjectListener(metadata, this);
	    ordering.remove(indices[i] - i + 1);
	    if (previewIndex != -1 && previewIndex == indices[i] - i + 1) {
		stopPreview(newPreviewIndex);
	    } else if (previewIndex != -1 && previewIndex > indices[i] - i + 1) {
		//if (newPreviewIndex == -1){
		    previewIndex --;
		    if (previewIndex == 0)
			activatePreview(0);
		    //}
	    }

	    for (ListDataListener listener: listeners){
		listener.intervalRemoved(new ListDataEvent(this, ListDataEvent.INTERVAL_REMOVED, indices[i] - i, indices[i] - i));
	    }
	}
    }

    public synchronized void addListDataListener(ListDataListener listener){
	listeners.add(listener);
    }


    // index is the list model index here
    public synchronized Object getElementAt(int index){
	//StackTraceElement ste = (new Exception()).getStackTrace()[1];
	//String pos = ste.getClassName()+":"+ste.getMethodName()+":"+ste.getLineNumber();

	//System.err.println("size: "+listElements.size()+" index: "+index+" pos: "+pos);
	return listElements.get(index);
    }

    // size is the list model size
    public synchronized int getSize(){
	return listElements.size();
    }
    
    public synchronized void removeListDataListener(ListDataListener listener){
	listeners.remove(listener);
    }

    public void cacheObjectValueChanged(Metadata metadata) {
	RadioProgramMetadata programMetadata = (RadioProgramMetadata)metadata;
	int i = 0;
	//System.out.println("--- guiplaylist: value changed " + metadata);
	for (PlaylistWidget.PlayItemWidget playItemWidget: listElements) {
	    if (playItemWidget.getMetadata().getItemID().equals(programMetadata.getItemID())) {
		playItemWidget.setMetadata(programMetadata);
		//System.out.println("---- guiplaylist: found metadata " + programMetadata);
		
		for (ListDataListener listener: listeners){
		    listener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, i, i));
		}

	    }
	    i++;
	}

	if (firstProgramItemWidget != null && firstProgramItemWidget.getMetadata().getItemID().equals(programMetadata.getItemID())) {
	    setFirstProgram(programMetadata);
	    firstProgramItemWidget.setMetadata(programMetadata);
	    for (ListDataListener listener: listeners){
		listener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, -1, -1));
	    }
	}
    }

    public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKeys) {
	for (int j = 0; j < metadata.length; j++) {
	    RadioProgramMetadata programMetadata = (RadioProgramMetadata)metadata[j];
	    String newItemID = newKeys[j].substring(0, newKeys[j].lastIndexOf("."));
	    RadioProgramMetadata dummyProgram, newProgram=null;
	    Metadata mdata;
	    if (!newItemID.equals("")) {
		dummyProgram = RadioProgramMetadata.getDummyObject(newItemID);
		mdata = cacheProvider.getSingle(dummyProgram);
		if (mdata == null){
		    logger.warn("CacheObjectKeyChanged: Metadata not present in cache: "+newItemID);
		    return;
		}
		newProgram = (RadioProgramMetadata)mdata;
	    }
	    
	    int i = 0;
	    ArrayList<Integer> indicesToRemove = new ArrayList<Integer>();
	    for (PlaylistWidget.PlayItemWidget playItemWidget: listElements) {
		if (playItemWidget.getMetadata().getItemID().equals(programMetadata.getItemID())) {
		    if (!newItemID.equals("")) {
			playItemWidget.setMetadata(newProgram);
			ordering.remove(i+1);
			ordering.add(i+1, newProgram.getItemID());
			
			for (ListDataListener listener: listeners){
			    listener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, i, i));
			}
		    } else {
			indicesToRemove.add(i);
		    }
		}
		i++;
	    }

	    int[] delIndices = new int[indicesToRemove.size()];
	    i = 0;
	    for (Integer index : indicesToRemove)
		delIndices[i++] = (int)index;
	    
	    playlistController.removeProgramsAt(delIndices, true, getPreviewIndex());
	    
	    if (firstProgramItemWidget != null && firstProgramItemWidget.getMetadata().getItemID().equals(programMetadata.getItemID())) {
		//System.err.println("------- guiplaylist: found first program ");
		if (!newItemID.equals("")) {
		    setFirstProgram(newProgram);
		    firstProgramItemWidget.setMetadata(newProgram);
		    ordering.remove(0);
		    ordering.add(0, newProgram.getItemID());
		    
		    for (ListDataListener listener: listeners){
			listener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, -1, -1));
		    }
		} else {
		    playlistController.removeFirstProgram();
		}
	    }
	}//for loop over metadata
    }

    public void clearCacheObjectListeners() {
	for (PlaylistWidget.PlayItemWidget playItemWidget: listElements)
	    cacheProvider.removeCacheObjectListener(playItemWidget.getMetadata(), this);
    }

}
