package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.awt.*;
import java.awt.event.*;
import java.lang.Math;

import javax.swing.*;
import javax.swing.event.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class LengthFilterWidget extends ActiveFilterWidget implements ChangeListener {
    MetadataProvider metadataProvider;

    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;

    JSlider lengthSlider;
    StationConfiguration stationConfig;
    JSpinner lengthSpinner;

    public LengthFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
			      LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	metadataProvider = libraryController.getMetadataProvider();
	this.stationConfig = libraryController.getStationConfiguration();
	init();
    }

    protected void init() {
	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();
	int blankLabelHeight = 10;
	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, filterVertSize * 2 + blankLabelHeight + borderHeight));	
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());
	titleLabel.setVerticalAlignment(SwingConstants.CENTER);
	add(titleLabel, getGbc(0, 0, weightx(100.0), gfillboth()));
	
	RSLabel blankLabel = new RSLabel(" ");
	blankLabel.setPreferredSize(new Dimension(filterHorizSize, blankLabelHeight));
	blankLabel.setMinimumSize(new Dimension(filterHorizSize, blankLabelHeight));
	add(blankLabel, getGbc(0, 1, gridwidth(2)));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));
	add(closeButton, getGbc(1, 0, gfillboth()));

	long maxLength = metadataProvider.getMaxLength();

	// 2 hours is the max, unless maxLength is greater
	maxLength = (maxLength > -1 ? maxLength: (long)2 * 60 * 60 * 1000);
	// in minutes
	maxLength = (long)(Math.ceil(((double)maxLength) / (1000 * 60)));

	lengthSlider = new JSlider(JSlider.HORIZONTAL, 0, (int)maxLength, (int)(maxLength / 3));
	lengthSlider.setMajorTickSpacing((int)(maxLength / 4));
	lengthSlider.setMinorTickSpacing((int)(maxLength / 20));
	lengthSlider.setSnapToTicks(true);
	lengthSlider.setPaintTicks(true);
	lengthSlider.setPaintLabels(true);
	lengthSlider.addChangeListener(this);
	lengthSlider.setPreferredSize(new Dimension((int)(filterHorizSize * (tlHorizFrac + cbHorizFrac) / 100.0), filterVertSize * 2));

	Color bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTERPANE_COLOR);
	setBackground(bgColor);
	titleLabel.setBackground(bgColor);
	closeButton.setBackground(bgColor);
	bgColor = stationConfig.getColor(StationConfiguration.LIBRARY_ACTIVEFILTER_BUTTON_COLOR);
	lengthSlider.setBackground(bgColor);
	lengthSlider.setForeground(Color.darkGray);
	blankLabel.setBackground(bgColor);
	blankLabel.setOpaque(true);

	closeButton.setIcon(IconUtilities.getIcon(StationConfiguration.CLOSE_ICON,
						  filterVertSize, filterVertSize));

	JPanel panel = new JPanel();
	panel.setLayout(new GridBagLayout());
	add(panel, getGbc(0, 2, weightx(1.0), gridwidth(2), gfillboth()));

	RSLabel lengthLabel = new RSLabel("Minutes:");
	lengthLabel.setPreferredSize(new Dimension(filterHorizSize /2, filterVertSize));
	lengthLabel.setBackground(bgColor);
	lengthLabel.setOpaque(true);
	panel.add(lengthLabel, getGbc(0, 0));
	
	lengthSpinner = new JSpinner(new SpinnerNumberModel(maxLength, 1, maxLength, 1));
	lengthSpinner.setPreferredSize(new Dimension(filterHorizSize/2, filterVertSize));
	lengthSpinner.setBackground(bgColor);
	lengthSpinner.addChangeListener(this);
	panel.add(lengthSpinner, getGbc(1, 0));

    }

    public void stateChanged(ChangeEvent e) {
	int value = ((Double)lengthSpinner.getValue()).intValue();
	long maxLength = (long)value * 60 * 1000;
	((LengthFilter)filter).activateAnchor(LengthFilter.LENGTH_ANCHOR, null);
	((LengthFilter)filter).setSelectedLength(maxLength);
	SearchResult result = libraryController.activateFilter(sessionID, filter);
	libraryController.updateResults(result, sessionID);
    }

    public void setEnabled(boolean activate) {
	super.setEnabled(activate);
	lengthSpinner.setEnabled(activate);
    }

    public void update(){
	int value = ((Double)lengthSpinner.getValue()).intValue();
	removeAll();
	init();
	lengthSpinner.setValue(new Double(value * 1.0));
    }
}