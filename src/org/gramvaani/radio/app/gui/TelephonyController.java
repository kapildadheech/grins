package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.rscontroller.services.UIService;
import org.gramvaani.radio.rscontroller.services.ArchiverService;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.utilities.FileUtilities;
import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.utilities.StringUtilities;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.telephonylib.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.messages.TelephonyEventMessage;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;
import java.io.*;


class TelephonyController implements TelephonyListenerCallback, MetadataListenerCallback, MicListenerCallback {
    static TelephonyController telephonyController;

    TelephonyProvider telephonyProvider;
    MetadataProvider metadataProvider;
    MicProvider micProvider;
    PlayoutProvider playoutProvider;
    ArchiverProvider archiverProvider;

    TelephonyWidget widget;
    RSApp rsApp;
    StationConfiguration stationConfig;
    ErrorInference errorInference;
    ControlPanel controlPanel;

    int lastBlockSize = -1;
    LogUtilities logger;
    String configFileName;
    String importConf;
    String libDirName;
    String dbName, login, password;
    String confProgramID = null;

    Hashtable<String,CallInfo> callInfos;
    boolean isOnline = false;
    boolean isTelephonyProviderActive = false;
    boolean isMetadataProviderActive = false;
    boolean isPlayoutProviderActive = false;
    boolean isArchiverProviderActive = false;

    boolean isTelephonyUIEnabled = false;
    boolean isConferenceEnabled = false;
    boolean micProviderActive = false;
    boolean micUIEnabled = false;
    boolean micServiceActive = false;    
    CallInfo outgoingCall = null, prevCall = null;

    protected TelephonyController(RSApp rsApp, TelephonyWidget widget, StationConfiguration stationConfig){
	this.rsApp = rsApp;
	this.stationConfig = stationConfig;
	this.widget = widget;
	logger = new LogUtilities("TelephonyController");
	
	dbName 	= stationConfig.getStringParam(StationConfiguration.LIB_DB_NAME);
	login  	= stationConfig.getStringParam(StationConfiguration.LIB_DB_LOGIN);
	password= stationConfig.getStringParam(StationConfiguration.LIB_DB_PASSWORD);
	libDirName = stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR);

	micServiceActive = stationConfig.isMicServiceActive();
	callInfos = new Hashtable<String, CallInfo>();
    }

    public void init() {
	telephonyProvider = rsApp.getProvider(TelephonyProvider.class);
	metadataProvider = rsApp.getProvider(MetadataProvider.class);
	micProvider   = (MicProvider) rsApp.getProvider(RSApp.MIC_PROVIDER);
	playoutProvider = rsApp.getProvider(PlayoutProvider.class);
	archiverProvider = rsApp.getProvider(ArchiverProvider.class);

	controlPanel = rsApp.getControlPanel();

	registerWithProviders();

	if (telephonyProvider == null || !telephonyProvider.isActive()) {
	    isTelephonyProviderActive = false;
	    isTelephonyUIEnabled = false;
	} else {
	    isTelephonyProviderActive = true;
	    isTelephonyUIEnabled = true;
	}

	if (metadataProvider == null || !metadataProvider.isActive())
	    isMetadataProviderActive = false;
	else
	    isMetadataProviderActive = true;

	if (micServiceActive) {
	    if (micProvider == null || !micProvider.isActive()) {
		micProviderActive = false;
	    } else {
		micProviderActive = true;
		micUIEnabled = true;
	    }
	} else {
	    micProviderActive = false;
	}
	
	if (metadataProvider != null)
	    metadataProvider.setCacheProvider(rsApp.getProvider(CacheProvider.class));

	updateGUIButtons();
	fillMetadata();
	getAvailableDialOutPorts();
    }

    public void getAvailableDialOutPorts() {
	for (String line : stationConfig.getAllTelephonyLineIDs()) {
	    widget.addDialOutPort(line);
	}
	telephonyProvider.checkStatus();
    }

    public void unregisterWithProviders(){
	if (telephonyProvider != null)
	    telephonyProvider.unregisterWithProvider(getName());
	if (metadataProvider != null)
	    metadataProvider.unregisterWithProvider(getName());
	if (micProvider != null)
	    micProvider.unregisterWithProvider(getName());
    }

    public void registerWithProviders(){
	telephonyProvider.registerWithProvider(this);
	metadataProvider.registerWithProvider(this);
	micProvider.registerWithProvider(this);
    }

    public void activateMetadataService(boolean activate, String[] error) {
	
	isMetadataProviderActive = activate;
	/*
	if (activate) {
	    for (CallInfo info:callInfos.values()) {
		if (info.getPotentialCallers() == null) {
		    Entity[] potentialCallers = metadataProvider.getPotentialCallers(info.getCallerID());
		    info.setPotentialCallers(potentialCallers);
		}
	    }
	} else 
	    errorInference.displayServiceError("Unable to activate metadata service", error);
	*/
	updateGUIButtons();
	fillMetadata();
	
    }

    void fillMetadata(){
	if (isMetadataProviderActive){
	    ArrayList<Category> list = new ArrayList<Category>();
	    for (Category category: metadataProvider.getCategories()){
		metadataProvider.populateChildren(category);
		list.add(category);
	    }
	    //widget.fillCategories(list.toArray(new Category[]{}));
	}
    }

    public MetadataProvider getMetadataProvider(){
	return metadataProvider;
    }

    public synchronized void disconnect(){
	if (isTelephonyProviderActive){
	    //goOffline();
	    telephonyProvider.disconnect();
	} else {
	    logger.error("Disconnect: isTelephonyProviderActive: false.");
	}

    }

    public synchronized void dial(String lineID, String number, String name, String location) {

	RSCallerID callerID = new RSCallerID(lineID, number);

	outgoingCall = new CallInfo(null, callerID, name, location);

	if (isTelephonyProviderActive && telephonyProvider.dial(callerID)) {

	} else {
	    logger.error("DialNumber: Dial failed. providerActive: "+isTelephonyProviderActive);
	    //XXX report error to error inference
	}
    }

    public synchronized void acceptCall(CallInfo callInfo) {
	String callGUID = callInfo.getGUID();
	
	logger.info("GRINS_USAGE:ACCEPT_CALL:"+callGUID);

	if (callGUID == null || callGUID.equals("")){
	    logger.error("AcceptCall: empty GUID");
	    return;
	}

	if (isTelephonyProviderActive && telephonyProvider.acceptCall(callGUID)) {
	    //prevCall = callInfo; 
	} else {
	    logger.error("AcceptCall: Accept failed. providerActive: "+isTelephonyProviderActive);
	    //XXX report error to error inference
	}

    }

    public synchronized void previewCall(CallInfo callInfo) {
	String callGUID = callInfo.getGUID();
	
	if (callGUID == null || callGUID.equals("")){
	    logger.error("PreviewCall: empty GUID.");
	    return;
	}

	if (isTelephonyProviderActive && telephonyProvider.previewCall(callGUID)){
	    logger.info("PreviewCall: Previewing call: "+callGUID+" : "+ callInfo.getCallerID());
	    widget.setCurrentCall(callInfo);
	    //prevCall = callInfo; 
	} else {
	    logger.error("PreviewCall: Preview failed. providerActive: "+isTelephonyProviderActive);
	    //XXX error to error inference
	}

    }

    public synchronized void holdCall(CallInfo callInfo){
	String callGUID = callInfo.getGUID();
	if (callGUID == null || callGUID.equals("")){
	    logger.error("HoldCall: empty GUID.");
	    return;
	}

	if (isTelephonyProviderActive && telephonyProvider.holdCall(callGUID)){
	    logger.info("HoldCall: Held call: "+callGUID+":"+callInfo.getCallerID());	    
	} else {
	    logger.error("HoldCall: Hold failed. providerActive: "+isTelephonyProviderActive);
	}
    }

    public synchronized void hangupCall(CallInfo callInfo) {
	if (callInfo == null)
	    return;
	String callGUID = callInfo.getGUID();

	logger.info("GRINS_USAGE:HANGUP_CALL:"+callGUID);

	if (callGUID == null || callGUID.equals("")){
	    logger.error("HangupCall: empty GUID");
	    return;
	}

	if (isTelephonyProviderActive && telephonyProvider.hangupCall(callGUID)) {
	    logger.info("HangupCall: Hungup: "+callGUID+" : "+callInfo.getCallerID());
	} else {
	    logger.error("HangupCall: Hangup failed. providerActive: "+isTelephonyProviderActive);
	    //XXX report error to error inference
	}
    }

    public synchronized void muteCall(CallInfo callInfo){
	String callGUID = callInfo.getGUID();

	logger.info("GRINS_USAGE:MUTE_CALL:"+callGUID);

	if (callGUID == null || callGUID.equals("")){
	    logger.error("MuteCall: empty GUID");
	    return;
	}

	if (isTelephonyProviderActive && telephonyProvider.muteCall(callGUID)){
	    logger.info("MuteCall: Muted: "+callGUID+":"+callInfo.getCallerID());
	} else {
	    logger.error("MuteCall: Mute failed. providerActive: "+isTelephonyProviderActive);
	}
    }

    public synchronized void unmuteCall(CallInfo callInfo){
	String callGUID = callInfo.getGUID();
	
	logger.info("GRINS_USAGE:MUTE_CALL:"+callGUID);

	if (callGUID == null || callGUID.equals("")){
	    logger.error("UnmuteCall: empty GUID");
	    return;
	}

	if (isTelephonyProviderActive && telephonyProvider.unmuteCall(callGUID)){
	    logger.info("UnmuteCall: Unmuted: "+callGUID+":"+callInfo.getCallerID());
	    widget.setCurrentCall(callInfo);
	} else {
	    logger.error("UnmuteCall: Unmute failed. providerActive: "+isTelephonyProviderActive);
	}
    }

    public synchronized void enableConferenceMode(boolean enable){
	
	logger.info("GRINS_USAGE:ENABLE_CONFERENCE:"+enable);

	if (isTelephonyProviderActive && telephonyProvider.enableConferenceMode(enable)) {

	}
    }

    public boolean isConferenceEnabled() {
	return isConferenceEnabled;
    }
    
    public synchronized void goOnline() {
	if (!isOnline) {
	    if (isTelephonyProviderActive && telephonyProvider.goOnline()) {
		logger.info("GoOnline: Going Online");
	    } else {
		//XXX report error to errorinference
		logger.error("GoOnline: Could not go online. providerActive: "+isTelephonyProviderActive);
	    }
	} else {
	    logger.error("GoOnline: already online.");
	}
    }

    public synchronized void goOffline() {
	if (isOnline) {
	    if (isTelephonyProviderActive && telephonyProvider.goOffline()) {
		logger.info("GoOffline: Let's go offline!!");
	    } else {
		//XXX report error to errorinference
		logger.error("GoOffline: Could not go offline. providerActive: "+isTelephonyProviderActive);
	    }
	} else {
	    logger.error("GoOffline: already offline.");
	}
    }

    public synchronized void goOnAir(boolean isConference) {
	boolean telephonyStatus = false;
	boolean goLiveStatus = true;

	logger.info("GRINS_USAGE:PUT_CALL_ONAIR:"+isConference);

	if (isTelephonyProviderActive && telephonyProvider.goOnAir(isConference)) {
	    telephonyStatus = true;
	    logger.info("GoOnAir: telephony on air.");
	    rsApp.getPlaylistWidget().pause();
	} else {
	    logger.error("GoOnAir: Could not go on-air. providerActive: "+isTelephonyProviderActive);
	}

	if (telephonyStatus) {
	    if(archiverProvider.isArchivable() && (!micServiceActive || micProvider.isStartable()))
		goLiveStatus = rsApp.getPlaylistWidget().getController().startGoLive();

	    if (goLiveStatus) {
		logger.info("GoOnAir: Mic/Archiver enabled.");
	    } else {
		logger.error("GoOnAir: Could not enable mic/archiver.");
		telephonyProvider.goOffAir(isConference);
	    } 
	}
    }

    public synchronized void goOffAir(boolean isConference) {

	logger.info("GRINS_USAGE:PUT_CALL_OFFAIR:"+isConference);

	if (isTelephonyProviderActive && telephonyProvider.goOffAir(isConference))
	    logger.info("GoOffAir: telephony offair");
	else
	    logger.error("GoOffAir: Could not put telephony off-air. providerActive: "+isTelephonyProviderActive);

	/*
	if (micServiceActive && micProviderActive) {
	    if (micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE) == 0) 
		logger.info("GoOffAir: mic offair.");
	    else
		logger.error("GoOffAir: Could not put mic off-air.");
	}
	*/

    }

    /*Call backs from provider*/

    public synchronized void telephonyOnline(boolean success, String[] error) {
	if (success) {
	    isOnline = true;
	    updateGUIButtons();
	} else {
	    //XXX report error to errorinference
	}
    }

    public synchronized void telephonyOffline(boolean success, String[] error) {
	if (success) {
	    isOnline = false;
	    updateGUIButtons();
	} else {
	    //XXX report error to errorinference
	}
    }

    public synchronized void callAccepted(boolean success, String callGUID, String programID, String[] error) {
	if (!success) {
	    //XXX report error to error inference
	} else {
	    CallInfo info = callInfos.get(callGUID);
	    if (callGUID != null && info != null) {
		info.setActive(true);
		info.setProgramID(programID);
		if (isMetadataProviderActive) {
		    Entity[] potentialCallers = RSCallerID.getPotentialContacts(info.getCallerID(), metadataProvider);
		    info.setPotentialCallers(potentialCallers);
		    if (potentialCallers.length == 1)
			info.setCaller(0);
		}
		if (isConferenceEnabled()) {
		    info.addConference(confProgramID);
		}
		widget.setCurrentCall(info);
		widget.callAccepted(info);
	    }
	}
    }
    
    public synchronized void callHungup(boolean success, String callGUID, String[] error) {
	if (success) {
	    endCall(callGUID);
	    updateGUIButtons();
	} else {
	    //XXX report error to errorinference
	}
    }
    
    public synchronized void callerHungup(String callGUID, String reason) {
	endCall(callGUID);
	updateGUIButtons();
    }

    void endCall(String guid) {
	
	widget.endCall(callInfos.remove(guid));
    }

    public synchronized void newCall(String callGUID, RSCallerID callerID) {
	CallInfo callInfo = new CallInfo(callGUID, callerID);

	callInfos.put(callGUID, callInfo);
	widget.addIncomingCall(callInfo);
	updateGUIButtons();
    }

    public void pbxStatusUpdate(boolean status, String[] error) {
    }

    public synchronized void lineIDUpdate(String lineID, String status) {
	if (status.equals(TelephonyProvider.SUCCESS))
	    widget.addDialOutPort(lineID);
	else
	    widget.removeDialOutPort(lineID);
    }

    public synchronized void updateOutgoingCall(String callGUID, String newState, String... options) {

	if (newState.equals(TelephonyLib.DIALING)){

	    outgoingCall.setGUID (callGUID);
	    callInfos.put (callGUID, outgoingCall);
	    widget.addOutgoingCall(outgoingCall);

	} else if (newState.equals(TelephonyLib.RINGING)) {

	    widget.outgoingCallRinging(outgoingCall);

	} else if (newState.equals(TelephonyLib.SUCCESS)) {
	    outgoingCall.setProgramID(options[0]);
	    outgoingCall.setActive(true);
	    if (isMetadataProviderActive) {
		Entity[] potentialCallers = RSCallerID.getPotentialContacts(outgoingCall.getCallerID(), metadataProvider);
		outgoingCall.setPotentialCallers(potentialCallers);
		if (potentialCallers.length == 1)
		    outgoingCall.setCaller(0);
	    }
	    if (isConferenceEnabled()) {
		outgoingCall.addConference(confProgramID);
	    }
	    widget.outgoingCallConnected(outgoingCall);

	} else if (newState.equals(TelephonyLib.NOANSWER)) {

	    widget.outgoingCallNoAnswer(outgoingCall);

	} else if (newState.equals(TelephonyLib.BUSY)) {

	    widget.outgoingCallBusy(outgoingCall);

	} else if (newState.equals(TelephonyLib.FAILURE)) {
	    //XXX: check
	    endCall(callGUID);
	    outgoingCall = null;
	}

    }
    /*
    public synchronized void telephonyStatusUpdate(boolean status, String[] error, Hashtable<String, String> options) {
	String[] dahdiUpChannels = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.DAHDI_OK));
	for (String num : dahdiUpChannels) {
	    lineIDUpdate(TelephonyLib.DAHDI + num, telephonyProvider.SUCCESS);
	}

	String[] dahdiDownChannels = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.DAHDI_ERROR));
	for (String num : dahdiDownChannels) {
	    lineIDUpdate(TelephonyLib.DAHDI + num, telephonyProvider.FAILURE);
	}

	String[] registeredATAs = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.ATA_REGISTERED));
	for (String ataID : registeredATAs) {
	    lineIDUpdate(ataID, telephonyProvider.SUCCESS);
	}

	String[] unregisteredATAs = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.ATA_UNREGISTERED));
	for (String ataID : unregisteredATAs) {
	    lineIDUpdate(ataID, telephonyProvider.FAILURE);
	}
    }
    */

    public synchronized void callOnAir(boolean success, String programID, long telecastTime, String[] error){
	if (success) {
	    if (programID != null) {
		controlPanel.cpJFrame().notifyPlayout(programID, telecastTime);
	    }
	} else {
	    /*
	    if (micServiceActive && micProviderActive) {
		micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE);
	    }
	    */
	}
    }

    public synchronized void callOffAir(boolean success, String[] error){
	if (!success)
	    return;

	widget.goOffAir();
    }

    public synchronized void callDialed(boolean success, String extension, String[] error) {
 
    }

    public synchronized void confEnabled(boolean success, String programID, String[] error) {
	if (success) {

	    logger.info("GRINS_USAGE:CONFERENCE_ENABLED:"+programID);

	    isConferenceEnabled = true;
	    confProgramID = programID;
	    for (CallInfo info: callInfos.values()) {
		if (info.isActive()) {
		    info.addConference(programID);
		    Entity caller = info.getSavedCaller();
		    if (caller != null) {
			addProgramCaller(confProgramID, caller.getEntityID(), Creator.CALLER);
		    }
		}
	    }
	}
    }

    public synchronized void confDisabled(boolean success, String[] error) {
	if (success) {
	    confProgramID = null;
	    isConferenceEnabled = false;
	}
    }

    public synchronized void activateTelephony(boolean activate, String[] error) {
	isTelephonyProviderActive = activate;
	isTelephonyUIEnabled = activate;

	if (!activate) {
	    isOnline = false;
	    //XXX send error to error inference
	    for (CallInfo info : callInfos.values()) {
		widget.endCall(info);
	    }
	    callInfos.clear();
	}
	widget.reInitButtons();
	updateGUIButtons();
    }

    public synchronized void enableTelephonyUI(boolean enable, String[] error) {
	isTelephonyUIEnabled = enable;

	if (!enable) {
	    //XXX send error to error inference
	}

	updateGUIButtons();
    }

    /*call backs from telephony provider end here*/

    /*call backs from micprovider*/
    public synchronized void activateMic(boolean activate, String[] error) {
	if (!micServiceActive)
	    return;

	micProviderActive = activate;
	micUIEnabled = activate;

	if (!activate) {
	    //XXX: should go off air
	}

	updateGUIButtons();
    }

    public synchronized void enableMicUI(boolean enable, String[] error) {
	if (!micServiceActive)
	    return;

	micUIEnabled = enable;

	if (!enable) {
	    //XXX send error to error inference
	}

	updateGUIButtons();
    }

    public synchronized void micGstError(String error) {
	if (micServiceActive) {
	    telephonyProvider.goOffAir(isConferenceEnabled());
	    //XXX: send error to error inference
	}
    }

    public synchronized void micEnabled(boolean success, String[] error) {
	if (!micServiceActive)
	    return;

	if (!success) {
	    telephonyProvider.goOffAir(isConferenceEnabled());	    
	    //XXX: send error to error inference	    
	}
    }

    public synchronized void micDisabled(boolean success, String[] error) {

    }

    public void micLevelTested(boolean success, int level) {

    }
    /*call backs from micprovider end here*/


    protected void updateGUIButtons() {
	//XXX: Ask widget to recheck the callinfos to see if potential callers have become available
	//this happens when metadataprovider was not active earlier and had become active now.
    }

    public String getName() {
	return "TelephonyController";
    }

    public void exportFiles(ArrayList<CallInfo> callInfos){
	if (callInfos.size() == 0)
	    return;

	final JFileChooser fileChooser = new JFileChooser(stationConfig.getUserDesktopPath());
	FileUtilities.setNewFolderPermissions(fileChooser);
	fileChooser.setBackground(stationConfig.getColor(StationConfiguration.PLAYLIST_TOPPANE_COLOR));
	fileChooser.updateUI();

	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	int retVal = fileChooser.showDialog(ControlPanelJFrame.getInstance(), "Export");

	if (retVal != JFileChooser.APPROVE_OPTION)
	    return;

	String destFolder = null;
	try{
	    destFolder = fileChooser.getSelectedFile().getCanonicalPath()+System.getProperty("file.separator");
	} catch (Exception e){
	    logger.error("ExportFiles: Unable to find canonical path of selected folder.");
	    return;
	}
	
	final String destFolderFinal = destFolder;
	final ArrayList<RadioProgramMetadata> programs = new ArrayList<RadioProgramMetadata>();

	for (CallInfo callInfo: callInfos){
	    RadioProgramMetadata query = new RadioProgramMetadata(callInfo.getProgramID());
	    programs.add(metadataProvider.getSingle(query));
	}

	Thread t = new Thread("Telephony Export"){
		public void run(){
		    FileStoreDownloader fsDownloader = new FileStoreDownloader(stationConfig);
		    fsDownloader.exportFiles(programs.toArray(new RadioProgramMetadata[0]), destFolderFinal);
		}
	    };
	t.start();
    }


    
    protected void updateCallerInfo(Entity e, String lineID, String callerID, String name, String location) {
	if (!isMetadataProviderActive)
	    return;

	RSCallerID oldID = new RSCallerID(lineID, e.getPhone());
	RSCallerID newID = new RSCallerID(lineID, callerID);

	if (!e.getName().equals(name) ||
	   !oldID.equals(newID) || 
	   !e.getLocation().equals(location)) {

	    metadataProvider.editEntity(e.getEntityID(), name, e.getUrl(), RSCallerID.getIDType(newID.getLineID()), 
					 newID.toDBString(), 
					 e.getEmail(), location, e.getEntityType());
	}
    }
    

    protected void addCallerToConferences(ArrayList<String> conferences, int entityID) {
	if (conferences == null)
	    return;

	for (String confProgramID : conferences) {
	    addProgramCaller(confProgramID, entityID, Creator.CALLER);
	}
    }

    protected void editCallerInConferences(ArrayList<String> conferences, int oldEntityID, int newEntityID) {
	if (conferences == null)
	    return;

	for (String confProgramID : conferences) {
	    editProgramCaller(confProgramID, oldEntityID, newEntityID, Creator.CALLER);
	}
    }

    protected void addProgramCaller(String programID, Integer entityID, String affiliation) {
	if (!isMetadataProviderActive || programID == null || entityID == null)
	    return;

	metadataProvider.addProgramCreator(programID, entityID, affiliation);
    }

    protected void editProgramCaller(String programID, Integer oldEntityID, Integer newEntityID, String affiliation) {
	if (!isMetadataProviderActive || programID == null || oldEntityID == null || newEntityID == null)
	    return;

	metadataProvider.editProgramCreator(programID, oldEntityID, newEntityID, affiliation);
    }

    public Entity[] searchForEntity(String entityName){
	if (entityName.length() < 3)
	    return null;
	Entity[] entities = metadataProvider.selectAllFields(new Entity(), Entity.getNameFieldName()+" like '%"+entityName+"%'", -1);
	return entities;
    }

    public void saveCallInfo(CallInfo callInfo){
	if (callInfo == null){
	    logger.error("SaveCallInfo: Null callinfo.");
	    return;
	}
	//Tags, Categories etc.
	String programID = callInfo.getProgramID();
	
	if (programID == null || programID.equals("")){
	    logger.error("SaveCallInfo: Null programID.");
	    return;
	}

	if (isMetadataProviderActive){
	    ItemCategory categoryQuery = new ItemCategory(programID);
	    metadataProvider.remove(categoryQuery);
	    
	    for (Category cat: callInfo.getCategories()){
		ItemCategory newItemCategory = new ItemCategory(programID, cat.getNodeID());
		metadataProvider.insert(newItemCategory);
	    }

	    for (Category cat: callInfo.getRemovedCategories()){
		metadataProvider.remove(new ItemCategory(programID, cat.getNodeID()));
	    }

	    RadioProgramMetadata programQuery = new RadioProgramMetadata(programID);
	    RadioProgramMetadata newProgram = new RadioProgramMetadata("");
	    newProgram.setDescription(callInfo.getDescription());
	    metadataProvider.update(programQuery, newProgram);
	}

	metadataProvider.updateProgramIndex(programID);
	metadataProvider.flushProgramIndex();
    }

    public void launchMetadataWidget(String programID){
	if (programID == null || programID.equals("")){
	    logger.warn("LaunchMetadataWidget: Empty programID");
	    return;
	}

	if (isMetadataProviderActive) {
	    MetadataWidget metadataWidget = MetadataWidget.getMetadataWidget(rsApp, new String[]{programID}, false);
	    if (!metadataWidget.isLaunched()) {
		controlPanel.initWidget(metadataWidget, false);
		controlPanel.cpJFrame().launchWidget(metadataWidget);
	    } else {
		controlPanel.cpJFrame().setWidgetMaximized(metadataWidget);
	    }
	}
    }
    

    public class CallInfo {
	String GUID = null;
	String name = null, location = null, description = null;
	Entity[] potentialCallers = null;
	Entity caller = null;
	int callerIndex = -1;
	int savedCallerIndex = -1;
	boolean isActive = false;
	String programID = null;
	RSCallerID rsCallerID = null;
	ArrayList<String> conferences = new ArrayList<String>();
	HashSet<Category> categories = new HashSet<Category>();
	HashSet<Category> removedCategories = new HashSet<Category>();
	
	public CallInfo(String GUID, RSCallerID callerID) {
	    this.GUID = GUID;
	    this.rsCallerID = callerID;
	}
	
	public CallInfo(String GUID, RSCallerID callerID, String name, String location){
	    this.GUID = GUID;
	    this.rsCallerID = callerID;
	    this.name = name;
	    this.location = location;
	}

	public synchronized void setPotentialCallers(Entity[] potentialCallers) {
	    this.potentialCallers = potentialCallers;
	}

	public void addConference(String conferenceProgramID) {
	    if (conferenceProgramID != null)
		conferences.add(conferenceProgramID);
	}

	public void setActive(boolean active) {
	    isActive = active;
	}

	public boolean isActive() {
	    return isActive;
	}

	public String[] getConferences() {
	    return conferences.toArray(new String[]{});
	}

	public synchronized Entity[] getPotentialCallers() {
	    return potentialCallers;
	}

	public synchronized String[] getPotentialCallerNames() {
	    if (potentialCallers == null) {
		return new String[] {};
	    }

	    String[] callerNames = new String[potentialCallers.length];

	    for (int i = 0; i < callerNames.length; i++) 
		callerNames[i] = potentialCallers[i].getName();

	    return callerNames;
	}

	public synchronized Entity getCaller() {
	    if (callerIndex != -1 && potentialCallers != null && potentialCallers.length >= callerIndex) {
		return potentialCallers[callerIndex];
	    } else {
		return null;
	    }
	}

	public synchronized int getCallerIndex() {
	    return callerIndex;
	}

	public synchronized void setCaller(int index) {
	    if (index == callerIndex || index < 0 || index > potentialCallers.length)
		return;

	    callerIndex = index;
	}

	public synchronized void saveCaller() {
	    Entity newCaller = potentialCallers[callerIndex];
	    Entity oldCaller = getSavedCaller();
	    if (oldCaller == null) {
		addProgramCaller(programID, newCaller.getEntityID(), Creator.CALLER);
		addCallerToConferences(conferences, newCaller.getEntityID());
	    } else {
		editProgramCaller(programID, oldCaller.getEntityID(), newCaller.getEntityID(), Creator.CALLER);
		editCallerInConferences(conferences, oldCaller.getEntityID(), newCaller.getEntityID());
	    }

	    savedCallerIndex = callerIndex;
	}
	
	public synchronized Entity getSavedCaller() {
	    if (savedCallerIndex == -1) 
		return null;
	    else 
		return potentialCallers[savedCallerIndex];
	}

	public synchronized void revertCallInfo() {
	    callerIndex = savedCallerIndex;
	}

	public synchronized void addCaller(String lineID, String callerID, String name, String location) {
	    int i;
	    if (name.equals(""))
		return;

	    Entity oldCaller = getSavedCaller();

	    Entity newCaller = new Entity();
	    newCaller.setName(name);
	    newCaller.setLocation(location);
	    newCaller.setPhone(callerID);

	    rsCallerID = new RSCallerID(lineID, callerID);
	    if (isMetadataProviderActive) {
		Integer id;
		try {
		    logger.info("GRINS_USAGE: ADD_CALLER");
		    id = new Integer(metadataProvider.addEntity(name, "", RSCallerID.getIDType(rsCallerID.getLineID()), rsCallerID.toDBString(), "", location, ""));
		} catch(NumberFormatException e) {
		    logger.error("AddCaller: Got exception.", e);
		    return;
		}
		
		newCaller.setEntityID(id);
		Entity[] tmpList = new Entity[potentialCallers.length + 1];
		for (i = 0; i < potentialCallers.length; i++) {
		    tmpList[i] = potentialCallers[i];
		}
		tmpList[i] = newCaller;
		callerIndex = i;
		potentialCallers = tmpList;
	    }

	    if (oldCaller != null) {
		editProgramCaller(programID, oldCaller.getEntityID(), newCaller.getEntityID(), Creator.CALLER);
		editCallerInConferences(conferences, oldCaller.getEntityID(), newCaller.getEntityID());
	    } else {
		addProgramCaller(programID, newCaller.getEntityID(), Creator.CALLER);
		addCallerToConferences(conferences, newCaller.getEntityID());
	    }
	    
	    savedCallerIndex = callerIndex;
	}

	public String getProgramID() {
	    return programID;
	}

	public void setProgramID(String id) {
	    programID = id;
	}

	public String getGUID(){
	    return GUID;
	}

	public void setGUID(String GUID){
	    this.GUID = GUID;
	}

	public RSCallerID getCallerID(){
	    return rsCallerID;
	}
	
	public String getName(){
	    return name;
	}

	public String getLocation(){
	    return location;
	}
	
	public String getDescription(){
	    return description;
	}

	public void setDescription(String text){
	    description = text;
	}

	public boolean addCategory(Category c){
	    Category tmp = c;

	    for (Category category: removedCategories){
		if (category.getNodeID() == c.getNodeID()){
		    tmp = category;
		    break;
		}
	    }
	    
	    removedCategories.remove(tmp);
	    return categories.add(c);
	}

	public void removeCategory(Category c){
	    Category tmp = c;

	    for (Category category: categories){
		if (category.getNodeID() == c.getNodeID()){
		    tmp = category;
		    break;
		}
	    }

	    categories.remove(tmp);
	    removedCategories.add(tmp);
	}

	public void setCategories(Iterable<Category> iter){
	    categories.clear();
	    for (Category category: iter){
		categories.add(category);
	    }
	}

	public void setItemCategories(ItemCategory items[]){
	    categories.clear();
	    for (ItemCategory item: items){
		Category c = metadataProvider.getSingle(new Category(item.getCategoryNodeID()));
		categories.add(c);
	    }
	}

	public Iterable<Category> getCategories(){
	    return categories;
	}

	public Iterable<Category> getRemovedCategories(){
	    return removedCategories;
	}

	public String getTagsString(){
	    ArrayList<Category> list = new ArrayList<Category> (categories);
	    Collections.sort(list);
	    return StringUtilities.joinFromIterable(categories, ", ");
	}
    }
}