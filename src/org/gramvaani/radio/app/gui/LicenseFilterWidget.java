package org.gramvaani.radio.app.gui;

import org.gramvaani.radio.medialib.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import static org.gramvaani.radio.app.gui.GBCUtilities.*;

public class LicenseFilterWidget extends ActiveFilterWidget implements ActionListener {
    //MediaLib medialib;
    
    static final int tlHorizFrac = 80;
    static final int cbHorizFrac = 90 - tlHorizFrac;

    static final String nullItem = "Select license";

    JComboBox licenseList;
    boolean ignoreUpdate = false;

    public LicenseFilterWidget(SearchFilter filter, ActiveFiltersModel activeFiltersModel, 
			       LibraryController libraryController, LibraryWidget libraryWidget, String sessionID) {
	super(filter, activeFiltersModel, libraryController, libraryWidget, sessionID);
	//medialib = MediaLib.getMediaLib(libraryController.getStationConfiguration(), libraryController.getTimeKeeper());
	init();
    }

    protected void init() {
	int filterVertSize = libraryWidget.getFilterVertSize();
	int filterHorizSize = libraryWidget.getFilterHorizSize();
							  
	Dimension fsize;
	setPreferredSize(fsize = new Dimension(filterHorizSize, filterVertSize * 2));
	setMinimumSize(fsize);

	setLayout(new GridBagLayout());
	add(titleLabel, getGbc(0, 0, weightx(100.0), gfillboth()));

	closeButton.setPreferredSize(new Dimension(filterVertSize, filterVertSize));
	add(closeButton, getGbc(1, 0, gfillboth()));

	String[] licenses = ((LicenseFilter)filter).getLicenses(libraryController.getCacheProvider());
	String[] licensesOption = new String[licenses.length + 1];
	licensesOption[0] = nullItem;
	int i = 1;
	for(String license: licenses)
	    licensesOption[i++] = license;
	licenseList = new JComboBox(licensesOption);
        licenseList.addActionListener(this);

	licenseList.setPreferredSize(new Dimension(filterHorizSize, filterVertSize));
	add(licenseList, getGbc(0, 1, gridwidth(2), weightx(100.0), gfillboth()));
    }

    public void actionPerformed(ActionEvent e) {
	if (ignoreUpdate){
	    ignoreUpdate = false;
	    return;
	}

	String licenseSelection = (String)(licenseList.getSelectedItem());
	if(!licenseSelection.equals(nullItem)) {
	    ((LicenseFilter)filter).activateAnchor(LicenseFilter.LICENSE_ANCHOR, licenseSelection);
	    SearchResult result = libraryController.activateFilter(sessionID, filter);
	    libraryController.updateResults(result, sessionID);
	} else {
	    ((LicenseFilter)filter).activateAnchor(LicenseFilter.LICENSE_ANCHOR, "");
	    SearchResult result = libraryController.activateFilter(sessionID, filter);
	    libraryController.updateResults(result, sessionID);
	}
    }

    public void setEnabled(boolean activate) {
	super.setEnabled(activate);
	licenseList.setEnabled(activate);
    }

    public void update(){
	Object license = licenseList.getSelectedItem();
	removeAll();
	init();

	ignoreUpdate = true;
	licenseList.setSelectedItem(license);
    }
    
}