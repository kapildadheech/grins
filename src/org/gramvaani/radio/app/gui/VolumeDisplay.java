package org.gramvaani.radio.app.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.GeneralPath;

import javax.swing.*;

public class VolumeDisplay extends JPanel {
    final static int bottomInset = 5;
    final static int leftInset = 2;
    final static int topInset = 5;
    final static int rightInset = 2;
    
    Dimension size;
    GeneralPath sliderPath, filledPath;
    LinearGradientPaint gradient;
    Color bgColor, lineColor, labelColor, adjustingLabelColor;
    int minGain, maxGain, width, height, tmpGain;
    int currentGain;
    String currentPercentString, adjustingPercentString;

    Font labelFont;
    int labelHeight, labelWidth;

    static final double labelHorizFrac = 2.75d;
    boolean valueAdjusting = false;
    VolumeChangeListener listener;

    public VolumeDisplay (VolumeChangeListener listener, Dimension size, Color bgColor, final int minGain, final int maxGain){
	this.size = size;
	this.bgColor = bgColor;
	this.minGain = minGain;
	this.maxGain = maxGain;
	this.listener = listener;

	lineColor = new Color (128, 128, 128);
	labelColor = new Color(196, 196, 196);
	adjustingLabelColor = new Color(80, 80, 80);

	setMinimumSize (size);
	setPreferredSize (size);
	
	height = size.height;
	width  = size.width;
	
	sliderPath = new GeneralPath ();
	
	sliderPath.moveTo (leftInset, height - bottomInset);
	sliderPath.lineTo (width - rightInset, topInset);
	sliderPath.lineTo (width - rightInset, height - bottomInset);
	sliderPath.lineTo (leftInset, height - bottomInset);

	gradient = new LinearGradientPaint (leftInset, 0.0f, width - rightInset, 0.0f, 
					    new float[]{0.0f, 0.5f, 1.0f}, 
					    new Color[]{Color.green, Color.orange, Color.red});

	makeFilledPath();
	calculateFontSize();
	setGain(0);

	addMouseListener(new MouseAdapter(){
		public void mouseEntered(MouseEvent e){
		    valueAdjusting = true;
		}

		public void mouseExited(MouseEvent e){
		    valueAdjusting = false;
		    repaint();
		}

		public void mousePressed(MouseEvent e){
		    setValue();
		}
	    });

	addMouseMotionListener(new MouseMotionAdapter(){
		public void mouseMoved(MouseEvent e){
		    int x = (int) e.getPoint().getX();
		    if (x < leftInset)
			x = leftInset;
		    if (x >= width - rightInset)
			x = width - rightInset;
		    x -= leftInset;
		    
		    tmpGain = (int)((1.0f*x/(width - leftInset - rightInset)) * (maxGain - minGain)) + minGain;
		    adjustingPercentString = Integer.toString(200 * (tmpGain - minGain)/(maxGain - minGain)) + "%";
		    repaint();
		}
	    });
    }

    void setValue(){
	listener.volumeChanged(tmpGain);
    }

    void calculateFontSize(){
	labelWidth = (int) ((width - (leftInset+rightInset))/labelHorizFrac);
	
	labelFont = GUIUtilities.getDefaultFont();
	FontMetrics fontMetrics = getFontMetrics(labelFont);
	
	int size = labelFont.getSize();

	while (fontMetrics.stringWidth("888%") > labelWidth){
	    labelFont = labelFont.deriveFont(1.0f * size--);
	    fontMetrics = getFontMetrics(labelFont);
	}

	labelHeight = fontMetrics.getHeight();
	labelWidth  = fontMetrics.stringWidth("888%");
    }

    public void setGain(int gain){
	currentGain = gain;
	tmpGain = gain;
	currentPercentString = Integer.toString(200 * (gain - minGain)/(maxGain - minGain)) + "%";
	makeFilledPath();
	repaint();
    }

    public void changeGain(int gain){
	setGain(currentGain + gain);
    }

    protected void makeFilledPath(){
	filledPath = new GeneralPath();

	int maxX = (int) (1.0 * (currentGain - minGain)/(maxGain - minGain) * (width - rightInset - leftInset)) + leftInset;
	int maxY = (int) (-1.0 * (currentGain - minGain)/(maxGain - minGain) * (height - bottomInset - topInset)) + height - bottomInset;
	    
	maxX = Math.min (maxX, width - rightInset);
	maxY = Math.max (maxY, topInset);
 
	filledPath.moveTo (leftInset, height - bottomInset);
	filledPath.lineTo (maxX, maxY);
	filledPath.lineTo (maxX, height - bottomInset);
	filledPath.lineTo (leftInset, height - bottomInset);

    }
	
    public void paintComponent (Graphics g){
	Graphics2D g2 = (Graphics2D) g;
	g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	g2.setColor(bgColor);
	g2.fillRect (0, 0, size.width, size.height);
	
	g2.setFont(labelFont);
	if (!valueAdjusting){
	    g2.setPaint(labelColor);
	    g2.drawString(currentPercentString, leftInset, labelHeight);
	} else {
	    g2.setPaint(adjustingLabelColor);
	    g2.drawString(adjustingPercentString, leftInset, labelHeight);
	}
	g2.setPaint(gradient);
	g2.fill(filledPath);
	g2.setColor(lineColor);
	g2.draw(sliderPath);
    }

    interface VolumeChangeListener {
	public void volumeChanged(int gain);
    }
}
