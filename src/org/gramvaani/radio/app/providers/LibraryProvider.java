package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.app.gui.StatsDisplay;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities;

import java.util.*;

public class LibraryProvider extends RSProviderBase implements MediaLibListener{

    protected final static int CONFIGURING = -1;
    protected final static int LISTENING = 0;

    protected int state = CONFIGURING;
    protected int prevState = state;

    protected UIService uiService;
    protected StationConfiguration stationConfiguration;
    protected TimeKeeper timeKeeper;
    protected MediaLib medialib = null;
    protected boolean isSearchActivated = false;
    protected boolean isLibServiceActive = false;
    protected boolean isMediaLibActive = false;
    protected boolean rscActive = true;
    protected Hashtable<String, LibraryListenerCallback> listenerCallbacks;

    
    public LibraryProvider(UIService uiService) {
	super (uiService, RSApp.LIBRARY_PROVIDER);
	this.uiService = uiService;
	this.stationConfiguration = uiService.getStationConfiguration();
	this.timeKeeper = uiService.getTimeKeeper();
	this.listenerCallbacks = new Hashtable<String, LibraryListenerCallback>();

    }
    
    public synchronized int activate() {
	logger.info("Activate: Provider activated.");

	MediaLib.registerListener("LibraryProvider", this);
	MediaLib.setIPCServer(uiService.getIPCServer());
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null) {
	    isMediaLibActive = true;
	} else {
	    logger.error("LibraryProvider: Unable to get access medialib.");
	}

	setState(CONFIGURING);
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String[] {RSController.INDEX_SERVICE, 
										  RSController.LIB_SERVICE});
	int retval = sendCommand(message);
	if (retval == -1)
	    rscActive = false;

	return retval;

    }
    
    public boolean isActive(){
	if (state() != LISTENING)
	    return false;
	else
	    return isMediaLibActive;
    }

    public StationConfiguration getStationConfig() {
	return stationConfiguration;
    }
    
    public synchronized void activateMediaLib(boolean activate){
	isMediaLibActive = activate;
	if (activate){/*
	    medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
	    if (state() == LISTENING){
		if (listenerCallback != null)
		    listenerCallback.activateSearch(true);
		    }*/
	} else {
	    if (state() == LISTENING)
		for (LibraryListenerCallback listenerCallback: listenerCallbacks.values())
		    listenerCallback.activateSearch(false, new String[]{RSController.ERROR_DB_FAILED});
	}
    }
    
    public void deletePrograms(String[] programs) {
	DeleteProgramsMessage message = new DeleteProgramsMessage(RSController.UI_SERVICE, 
								  RSController.LIB_SERVICE,
								  programs);
	
	sendCommand(message);
    }

    public synchronized void receiveMessage(IPCMessage message) {
	if (message == null){
	    logger.error("ReceiveMessage: Null message received.");
	    return;
	}

	logger.debug("ReceiveMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());

	if (message instanceof ServiceAckMessage){
	    ServiceAckMessage ackMessage = (ServiceAckMessage) message;
	    for (String service: ackMessage.getActiveServices()){
		    if (service.equals(RSController.INDEX_SERVICE))
		    	isSearchActivated = true;
		    if (service.equals(RSController.LIB_SERVICE))
			isLibServiceActive = true;
	    }

	    for (String service: ackMessage.getInactiveServices()){
		if ( (service.equals(RSController.LIB_SERVICE)) ||
		    (service.equals(RSController.INDEX_SERVICE)) ){
		    setState(CONFIGURING);
		    for (LibraryListenerCallback listener: listenerCallbacks.values()) {
			listener.activateSearch(false, new String[]{RSController.ERROR_SERVICE_DOWN,
								   service});
		    }
		}
	    }
	    if (!isSearchActivated)
		logger.error("ReceiveMessage: IndexService not activated.");
	    
	} else if (message instanceof ServiceNotifyMessage){
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage) message;
	    for (String service: notifyMessage.getActivatedServices()){
		if (service.equals(RSController.INDEX_SERVICE)){
		    isSearchActivated = true;
		} else if (service.equals(RSController.LIB_SERVICE))
		    isLibServiceActive = true;
		else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing.
		    }
		}
	    }
	    boolean inactivate = false;
	    String disconnectedService = "";
	    for (String service: notifyMessage.getInactivatedServices()) {
		if ( (service.equals(RSController.LIB_SERVICE)) ||
		    (service.equals(RSController.INDEX_SERVICE)) ){
		    inactivate = true;
		    disconnectedService = service;
		    break;
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController disconnected.");
		    inactivate = true;
		    disconnectedService = service;
		    break;
		}
	    }
	    if (isMediaLibActive && inactivate){
		setState(CONFIGURING);
		for (LibraryListenerCallback listenerCallback: listenerCallbacks.values()) {
		    // VVV send error to inference engine
		    listenerCallback.activateSearch(false, new String[]{RSController.ERROR_SERVICE_DISCONNECTED,
									disconnectedService});
		}
		return;
	    }

	} else if (message instanceof MachineStatusResponseMessage){
	    MachineStatusResponseMessage responseMessage = (MachineStatusResponseMessage) message;
	    handleMachineStatusResponse(responseMessage.getSource(), responseMessage.getCommand(), responseMessage.getResults());
	}

	if (isSearchActivated && isLibServiceActive){
	    searchServiceActivated();
	}
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: From: "+message.getSource()+" Type: " + message.getParam("serviceerror"));
	

	String serviceName = null;
	String errorCode = null;

	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}

	//ErrorCode ignored here.
	boolean deactivate = false;
	if (serviceName != null){
	    if (serviceName.equals(RSController.INDEX_SERVICE)){
		deactivate = true;
		isSearchActivated = false;
	    } else if (serviceName.equals(RSController.LIB_SERVICE)){
		deactivate = true;
		isLibServiceActive = false;
	    } else if (serviceName.equals(RSController.RESOURCE_MANAGER)){
		deactivate = true;
		isSearchActivated = false;
		isLibServiceActive = false;
		rscActive = false;
	    }
	}

	if (deactivate) {
	    setState(CONFIGURING);
	    if (isMediaLibActive){
		for (LibraryListenerCallback listenerCallback: listenerCallbacks.values()) {
		    // VVV probably not required
		    listenerCallback.activateSearch(false, new String[]{RSController.ERROR_SERVICE_DOWN,
									serviceName});
		}
	    }
	} else {
	    logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
	    for (LibraryListenerCallback listenerCallback: listenerCallbacks.values()) {
		// VVV send error to inference engine
	    }
	}
    }

    protected void searchServiceActivated () {
	if (state() == CONFIGURING){
	    setState (LISTENING);
	    if (medialib == null)
		medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	    if (medialib != null){
		isMediaLibActive = true;
		logger.info("SearchServiceActivated: Search Service is now activated");
		for (LibraryListenerCallback listenerCallback: listenerCallbacks.values())
		    listenerCallback.activateSearch(true, new String[]{});
	    }
	}
    }

    protected void handleMachineStatusResponse(String service, String command, String[] results){
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    String machineIP = stationConfiguration.getMachineIP(stationConfiguration.getServiceMachineID(service));
	    String dirName = results[0];
	    long usedSpace = Long.parseLong(results[1]);
	    long availableSpace = Long.parseLong(results[2]);
	    
	    for (LibraryListenerCallback listener: listenerCallbacks.values())
		listener.diskSpaceChecked(machineIP, dirName, usedSpace, availableSpace);
	}
    }

    protected int state() {
	return state;
    }

    protected void setState(int state) {
	prevState = this.state;
	this.state = state;
	logger.info("Previous state: "+prevState+" Current state: "+state);

	switch(state) {
	case CONFIGURING:
	    // disable UI
	    break;
	case LISTENING:
	    // enable UI
	    break;
	}
    }

    public boolean isSearchable() {
	return (state() == LISTENING);
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listenerCallbacks.put(listener.getName(), (LibraryListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	listenerCallbacks.remove(listenerName);
    }


    public Hashtable<String, ArrayList<String>> getApplicableAnchors(String anchorText) {
	return sendAnchorSearch(anchorText);
    }

    public SearchDetails getMatchingPrograms(String queryString, String whereClause, String joinTableStr, int startingOffset, int maxResults) {
        SearchDetails searchDetails = sendProgramSearch(queryString, whereClause, joinTableStr, startingOffset, maxResults);
	return searchDetails;
    }

    public GraphStats getBarGraphStats(String queryString, String whereClause, String joinTableStr, long minTelecastTime, long maxTelecastTime) {
	GraphStats barGraphStats = sendGraphStats(queryString, whereClause, joinTableStr, minTelecastTime, maxTelecastTime, StatsDisplay.BAR_GRAPH);
	return barGraphStats;
    }

    public GraphStats getPointGraphStats(String queryString, String whereClause, String joinTableStr, long minTelecastTime, long maxTelecastTime) {
	GraphStats pointGraphStats = sendGraphStats(queryString, whereClause, joinTableStr, minTelecastTime, maxTelecastTime, StatsDisplay.POINT_GRAPH);
	return pointGraphStats;
    }

    private Hashtable<String, ArrayList<String>> sendAnchorSearch(String anchorText) {
	try {
	    AnchorSearchMessage anchorMessage = new AnchorSearchMessage(IPCSyncMessage.SYNC_MESSAGE, 
									RSController.INDEX_SERVICE,
									anchorText);
	    AnchorSearchResponseMessage anchorResponseMessage = (AnchorSearchResponseMessage)(sendSyncCommand(anchorMessage));
	    
	    if (anchorResponseMessage == null) {
		return null;
	    } else {
		return anchorResponseMessage.getResults();
	    }
	} catch(IPCSyncMessageTimeoutException e) {
	    return null;
	}
    }

    private SearchDetails sendProgramSearch(String queryString, String whereClause, String joinTableStr, int startingOffset, int maxResults) {
	try {
	    ProgramSearchMessage programMessage = new ProgramSearchMessage(IPCSyncMessage.SYNC_MESSAGE, 
									   RSController.INDEX_SERVICE,
									   queryString, whereClause, joinTableStr, startingOffset, maxResults);
	    ProgramSearchResponseMessage programResponseMessage = (ProgramSearchResponseMessage)(sendSyncCommand(programMessage));

	    if (programResponseMessage == null) {
		return null;
	    } else {
		SearchDetails searchDetails = new SearchDetails(programResponseMessage.getResults(), programResponseMessage.getNumSearchResults());
		return searchDetails;
	    }
	} catch(IPCSyncMessageTimeoutException e) {
	    return null;
	}
    }

    private GraphStats sendGraphStats(String queryString, String whereClause, String joinTableStr, long minTelecastTime, long maxTelecastTime,
				      int graphType) {

	
	try {
	    GraphStatsMessage statsMessage = new GraphStatsMessage(IPCSyncMessage.SYNC_MESSAGE, 
								   RSController.INDEX_SERVICE,
								   queryString, whereClause, joinTableStr, minTelecastTime, maxTelecastTime,
								   graphType);
	    GraphStatsResponseMessage statsResponseMessage = (GraphStatsResponseMessage)(sendSyncCommand(statsMessage));

	    if (statsResponseMessage == null) {
		return new GraphStats();
	    } else {
		GraphStats graphStats = new GraphStats();
		graphStats.setMinTelecastTime(statsResponseMessage.getMinTelecastTime());
		graphStats.setMaxTelecastTime(statsResponseMessage.getMaxTelecastTime());
		graphStats.setXLabels(statsResponseMessage.getXLabels());
		graphStats.setDataItems(statsResponseMessage.getDataItems());
		graphStats.setApproxBinInterval(statsResponseMessage.getApproxBinInterval());
		graphStats.setMaxDataItem(statsResponseMessage.getMaxDataItem());

		return graphStats;
	    }
	} catch(IPCSyncMessageTimeoutException e) {
	    return new GraphStats();
	}
    }

    public class SearchDetails {
	String[] programs;
	int numSearchResults;

	public SearchDetails(String[] programs, int numSearchResults) {
	    this.programs = programs;
	    this.numSearchResults = numSearchResults;
	}

	public String[] getPrograms() {
	    return programs;
	}

	public int numSearchResults() {
	    return numSearchResults;
	}
    }

}
