package org.gramvaani.radio.app.providers;

public interface MediaLibUploadListenerCallback extends ListenerCallback{
    public void activateMediaLibUpload(boolean activate, String[] error);
    public void uploadError(String fileName, String error, int numRemaining);
    public void uploadSuccess(String fileName, String libFileName, int numRemaining, boolean isDuplicate);
    public void updateProgress(String fileName, long uploadedLength, long totalLength);
    public void uploadGiveup(String fileName);
}