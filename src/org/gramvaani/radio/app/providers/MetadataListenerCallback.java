package org.gramvaani.radio.app.providers;

public interface MetadataListenerCallback extends ListenerCallback {

    public void activateMetadataService(boolean activate, String[] error);

}