package org.gramvaani.radio.app.providers;

import java.util.Hashtable;
import org.gramvaani.radio.telephonylib.RSCallerID;

public interface TelephonyListenerCallback extends ListenerCallback {

    public void activateTelephony(boolean activate, String[] error);
    public void enableTelephonyUI(boolean enable, String[] error);

    public void telephonyOffline(boolean success, String[] error);
    public void telephonyOnline(boolean success, String[] error);
    public void callAccepted(boolean success, String callGUID, String programID, String[] error);
    public void callHungup(boolean success, String callGUID, String[] error);
    public void callerHungup(String callGUID, String reason);
    public void newCall(String callGUID, RSCallerID callerID);
    public void callOnAir(boolean success, String programID, long telecastTime, String[] error);
    public void callOffAir(boolean success, String[] error);
    public void callDialed(boolean success, String extension, String[] error);
    public void updateOutgoingCall(String callGUID, String newState, String... options);
    public void pbxStatusUpdate(boolean status, String[] error);
    public void confEnabled(boolean success, String programID, String[] error);
    public void confDisabled(boolean success, String[] error);
    public void lineIDUpdate(String devicePortNames, String status);
}