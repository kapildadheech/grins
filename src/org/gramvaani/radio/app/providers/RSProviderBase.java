package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.providers.*;

import java.util.Hashtable;

public abstract class RSProviderBase implements RSProvider {
    protected UIService uiService;
    protected String name;
    protected LogUtilities logger;

    public RSProviderBase (UIService uiService, String name){
	this.uiService = uiService;
	this.name = name;
	logger = new LogUtilities(name);
	uiService.registerProvider(this);
    }

    public int sendCommand (IPCMessage message){
	int returnValue = uiService.sendCommand(message);
	if (returnValue != 0)
	    logger.error("SendCommand: Unable to send message. To: "+message.getDest()+" Type: "+message.getMessageClass());
	return returnValue;
    }

    public IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	IPCSyncResponseMessage responseMessage = uiService.sendSyncCommand(message);
	if(responseMessage == null)
	    logger.error("SendSyncCommand: Unable to send message. To: " + message.getDest() + " Type: " + message.getMessageClass());
	return responseMessage;
    }

    public abstract void receiveMessage(IPCMessage message);

    public abstract void receiveErrorMessage (IPCMessage message);


    public String getName (){
	return this.name;
    }
    
    public abstract int activate();
    
    public abstract void registerWithProvider(ListenerCallback listener);
    
    public abstract void unregisterWithProvider(String listenerName);

    public abstract boolean isActive();

}
