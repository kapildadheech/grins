package org.gramvaani.radio.app.providers;

public interface StreamingListener extends ListenerCallback {

    public void activateStreaming(boolean activate, String[] error);
    public void streamingGstError(String error);
    public void streamingStarted(boolean success, String[] error);
    public void streamingStopped(boolean success, String[] error);
}