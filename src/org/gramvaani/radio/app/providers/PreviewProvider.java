package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.*;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;


public class PreviewProvider extends PlayoutPreviewProvider {
    protected final static int CONFIGURING = -2;
    protected final static int RESOURCE_INTEREST = -1;
    protected final static int LISTENING = 0;
    protected final static int PLAY_COMMAND_SENT = 1;
    protected final static int PLAY_ACTIVE = 2;
    protected final static int PAUSE_COMMAND_SENT = 3;
    protected final static int PAUSE_ACTIVE = 4;
    protected final static int STOP_COMMAND_SENT = 5;

    protected int state = CONFIGURING;
    protected int prevState = state;
    protected boolean prematurePlay = false;

    protected String sessionID = "";
    protected String fileID;
    protected PlayoutPreviewListenerCallback activeListener;

    protected String serviceInstanceName;
    protected String audioServiceID = "";

    protected Hashtable<String, PlayoutPreviewListenerCallback> listeners;
    protected Hashtable<String, String> programs;
    protected String playType;
    protected boolean rscActive = true;
    protected boolean audioDisconnected = false;

    protected final static int POOL_THREAD_COUNT = 1;
    protected ExecutorService listenersActivator;

    public PreviewProvider(UIService uiService) {
	super (uiService, RSApp.PREVIEW_PROVIDER);

	serviceInstanceName = uiService.getProviderServiceInstance(RSApp.PREVIEW_PROVIDER, AudioService.PREVIEW);
	listeners = new Hashtable<String, PlayoutPreviewListenerCallback>();
	programs = new Hashtable<String, String>();
	playType = AudioService.PREVIEW;

	listenersActivator = Executors.newFixedThreadPool(POOL_THREAD_COUNT);
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    listenersActivator.shutdown();
		}
	    });

	logger.debug("Initialized with serviceInstanceName="+serviceInstanceName);
    }

    public synchronized int activate() {
	logger.info("Activate: Provider activated.");
	setState(CONFIGURING);
	audioDisconnected = false;
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String[] {serviceInstanceName});

	int retval = sendCommand(message);
	if (retval == -1)
	    rscActive = false;

	return retval;
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listeners.put(listener.getName(), (PlayoutPreviewListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	PlayoutPreviewListenerCallback listener = listeners.get(listenerName);

	for (String fileID: programs.keySet())
	    if (programs.get(fileID).equals(listenerName))
		stop(fileID, listener);

	listeners.remove(listenerName);
    }

    public synchronized boolean isActive() {
	if (state() == CONFIGURING)
	    return false;
	else
	    return true;
    }

    public synchronized void receiveMessage(IPCMessage message) {
	logger.debug("ReceiveMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	boolean isAudioActivated = false;
	if (message instanceof AudioAckMessage){
	    AudioAckMessage ackMessage = (AudioAckMessage) message;
	    if (state() == PLAY_COMMAND_SENT && ackMessage.getCommand().equals(AudioService.PLAY_MSG))
		sessionID = ackMessage.getSessionID();
	    
	    handleAudioAck(ackMessage.getAck(), ackMessage.getItemID(), ackMessage.getCommand(), ackMessage.getParams(), ackMessage.getError());

	} else if (message instanceof ServiceAckMessage){
	    
	    ServiceAckMessage ackMessage = (ServiceAckMessage) message;
	    String[] activeServices = ackMessage.getActiveServices();
	    String[] activeIDs = ackMessage.getActiveServiceIDs();
	    
	    for (int i = 0; i < activeServices.length; i++){
		if (activeServices[i].equals(serviceInstanceName)) {
		    audioServiceConnected(activeIDs[i]);
		    isAudioActivated = true;
		}
	    }

	    for (String service: ackMessage.getInactiveServices()) {
		if (service.equals(serviceInstanceName)) {
		    isAudioActivated = false;
		    setState (CONFIGURING);
		    activateAudio(false, playType, new String[]{RSController.ERROR_SERVICE_DOWN, service});
		}
	    }


	} else if (message instanceof ServiceNotifyMessage){

	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage) message;
	    for (String service: notifyMessage.getActivatedServices()) {
		if (service.equals(serviceInstanceName)){
		    if (!audioDisconnected)
			isAudioActivated = true;
		    else {
			audioDisconnected = false;
			//for (PlayoutPreviewListenerCallback callback: listeners.values()) 
			//    callback.enableAudioUI(true, playType, new String[] {});
			enableAudioUI(true, playType, new String[] {});
			return;
		    }
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing. Wait for lib service to be activated.
		    }
		}
	    }
	    
	    
	    for (String service: notifyMessage.getInactivatedServices()){
		if (service.equals(serviceInstanceName)){
		    audioDisconnected = true;
		    /*
		    for (PlayoutPreviewListenerCallback callback: listeners.values()){
			//VVV send message to inference engine
			callback.enableAudioUI(false, playType, new String[] {RSController.ERROR_SERVICE_DISCONNECTED, serviceInstanceName});
		    }
		    */
		    enableAudioUI(false, playType, new String[] {RSController.ERROR_SERVICE_DISCONNECTED, serviceInstanceName});
		    return;
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController disconnected.");
		    audioDisconnected = true;
		    /*
		    for (PlayoutPreviewListenerCallback callback: listeners.values()){
			//VVV send error to inference engine
			callback.enableAudioUI(false, playType, new String[] {RSController.ERROR_SERVICE_DISCONNECTED, RSController.RESOURCE_MANAGER});
		    }
		    */
		    enableAudioUI(false, playType, new String[] {RSController.ERROR_SERVICE_DISCONNECTED, RSController.RESOURCE_MANAGER});
		    return;
		}
	    }

	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();
	    
	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(serviceInstanceName))
		    audioServiceConnected(activatedIDs[i]);
	    }

	} else if (message instanceof AudioErrorMessage){
	    AudioErrorMessage errorMessage = (AudioErrorMessage) message;
	    handleAudioError(errorMessage.getItemID(), errorMessage.getError());
	} else if (message instanceof AudioSessionsInfoMessage) {
	    AudioSessionsInfoMessage infoMessage = (AudioSessionsInfoMessage)message;
	    handleAudioSessionsInfoMessage(infoMessage);
	}

	if (isAudioActivated)
	    audioServiceActivated();
    }

    @SuppressWarnings("unchecked")
    //State management dictates that there would be just one 
    protected void handleAudioSessionsInfoMessage(AudioSessionsInfoMessage infoMessage) {
	String[] remoteProgramIDs = infoMessage.getProgramIDs();
	Hashtable<String, String> localPrograms = (Hashtable<String,String>)programs.clone();

	for (String localProgram : localPrograms.keySet()) {
	    boolean found = false;
	    for (String remoteProgram : remoteProgramIDs) {
		if (remoteProgram.equals(localProgram)) {
		    found = true;
		    break;
		}
	    }
	    if (!found && state == PLAY_ACTIVE) {
		setState(LISTENING);
		PlayoutPreviewListenerCallback callback = listeners.get(programs.remove(localProgram));
		logger.warn("HandleAudioSessionsInfoMessage: programID:" + localProgram + " not found with playout service. Calling play done.");		
		if (callback != null)
		    callback.playDone(true, localProgram, AudioService.PREVIEW);
	    }
	}
    }

    protected void audioServiceConnected(String newID){
	logger.debug("AudioServiceConnected with ID: "+newID);
	
	if (!audioServiceID.equals("") && !audioServiceID.equals(newID)){
	    activateAudio(false, playType, new String[]{RSController.ERROR_SERVICE_DOWN, serviceInstanceName});
	    activateAudio(true, playType, new String[]{});
	}
	
	audioServiceID = newID;
    }


    /* Sending callbacks to listeners routed through an executor to avoid deadlocks */

    protected void audioGstError(final PlayoutPreviewListenerCallback callback, final String programID, final String error, final String playType) {
	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null)
			callback.audioGstError(programID, error, playType);
		}
	    };

	listenersActivator.execute(r);
    }


    protected void playoutPreviewError(final PlayoutPreviewListenerCallback callback, final String errorCode, final String[] error) {
	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null) 
			callback.playoutPreviewError(errorCode, error);
		}
	    };

	listenersActivator.execute(r);
    }
    

    protected void playActive(final PlayoutPreviewListenerCallback callback, final boolean success, final String programID, final String playType, final long telecastTime, final String[] error) {

	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null) 
			callback.playActive(success, programID, playType, telecastTime, error);
		}
	    };

	listenersActivator.execute(r);
    }

    protected void stopActive(final PlayoutPreviewListenerCallback callback, final boolean success, final String programID, final String playType, final String[] error) {

	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null)
			callback.stopActive(success, programID, playType, error);
		}
	    };

	listenersActivator.execute(r);
    }

    protected void activateAudio(final boolean activate, final String playType, final String[] error) {
	Runnable r = new Runnable() {
		public void run() {
		    for (PlayoutPreviewListenerCallback callback: listeners.values()) {
			if (callback != null)
			    callback.activateAudio(activate, playType, error);
		    }
		    
		}
	    };

	listenersActivator.execute(r);
    }
    
    protected void enableAudioUI(final boolean enable, final String playType, final String[] error) {
	Runnable r = new Runnable() {
		public void run() {
		    for (PlayoutPreviewListenerCallback callback: listeners.values()) {
			if (callback != null)			
			    callback.enableAudioUI(enable, playType, error);
		    }
		    
		}
	    };

	listenersActivator.execute(r);
    }

    /* End of call backs through executors */


    protected void handleAudioError(String programID, String error){
	logger.error("HandleAudioError: For program: "+programID+" error: "+error);
	String fileID = programs.remove(programID);
	if (fileID != null) {
	    PlayoutPreviewListenerCallback callback = listeners.get(fileID);
	    /*
	      if (callback != null)
	      callback.audioGstError(programID, error, playType);
	    */
	    audioGstError(callback, programID, error, playType);
	}
    }

    protected void handleAudioAck (String ack, String programID, String command, Hashtable<String,String> params, String[] error){
	logger.info("HandleAudioAck: " + ack + ":" + state);
	boolean ackProcessed = false;

	switch (state()){

	case PLAY_COMMAND_SENT:
	    if (prematurePlay && command.equals(AudioService.STOP_MSG)) {
		PlayoutPreviewListenerCallback callback = listeners.get(programs.remove(programID));
		if (ack.equals(AudioService.SUCCESS)) {
		    //if (callback != null)
		    //callback.stopActive(true, programID, playType, error);
		    stopActive(callback, true, programID, playType, error);
		} else {
		    //VVV send error to inference engine
		    //if (callback != null)
		    //callback.stopActive(false, programID, playType, error);
		    stopActive(callback, false, programID, playType, error);
		}
		prematurePlay = false;

	    } else if (command.equals(AudioService.PLAY_MSG)) {
		if (ack.equals(AudioService.SUCCESS)){
		    logger.info("HandleAudioAck: Playing.");
		    setState(PLAY_ACTIVE);
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		    //if (callback != null)
		    //callback.playActive(true, programID, playType, Long.parseLong(params.get(AudioAckMessage.TELECAST_TIME)), error);
		    playActive(callback, true, programID, playType, Long.parseLong(params.get(AudioAckMessage.TELECAST_TIME)), error);
		} else {
		    logger.error("HandleAudioAck: Unable to play.");
		    displayError("Unable to play.");
		    setState(LISTENING);
		    String fileID = programs.get(programID);
		    if (fileID != null) {
			PlayoutPreviewListenerCallback callback = listeners.get(fileID);
			//if (callback != null)
			//callback.playActive(false, programID, playType, -1L, error);
			playActive(callback, false, programID, playType, -1L, error);
		    }
		}
	    }
	    break;

	case PAUSE_COMMAND_SENT:
	    if (command.equals(AudioService.PAUSE_MSG)) {
		if (ack.equals(AudioService.SUCCESS)){
		    logger.info("HandleAudioAck: Paused.");
		    setState(PAUSE_ACTIVE);
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		    if (callback != null)
			callback.pauseActive(true, programID, playType, error);
		} else {
		    logger.error("HandleAudioAck: Unable to pause.");
		    setState(PLAY_ACTIVE);
		    displayError("Unable to pause.");
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		    if (callback != null)
			callback.pauseActive(false, programID, playType, error);
		}
	    }
	    break;

	case STOP_COMMAND_SENT:
	    if (command.equals(AudioService.STOP_MSG)) {
		ackProcessed = true;
		if (ack.equals(AudioService.SUCCESS)){
		    logger.info("HandleAudioAck: Stopped.");
		    setState(LISTENING);
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.remove(programID));
		    //if (callback != null)
		    //callback.stopActive(true, programID, playType, error);
		    stopActive(callback, true, programID, playType, error);
		} else {
		    logger.error("HandleAudioAck: Unable to stop.");
		    displayError("Unable to stop.");
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.remove(programID));
		    //if (callback != null)
		    //callback.stopActive(false, programID, playType, error);
		    stopActive(callback, false, programID, playType, error);
		}
	    }
	    break;
	case PLAY_ACTIVE:
	    if (params.get("command").equals(AudioService.PLAY_DONE_MSG)){
		if (ack.equals(AudioService.SUCCESS)){
		    logger.info("HandleAudioAck: Play done.");
		    setState(LISTENING);
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.remove(programID));
		    if (callback != null)
			callback.playDone(true, programID, playType);
		}
		else{
		    logger.error("HandleAudioAck: Unknown error received with message "+AudioService.PLAY_DONE_MSG);
		    displayError("Unknown Error.");
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.remove(programID));
		    /*
		    if (callback != null) {
			// VVV
			callback.playoutPreviewError(null, new String[]{programID, playType});
		    }
		    */
		    playoutPreviewError(callback, null, new String[]{programID, playType});
		}
	    }
	    
	    break;
	}

	//For handling preemption by rscontroller
	if (command.equals(AudioService.STOP_MSG) && 
	    ack.equals(AudioService.SUCCESS) &&
	    !ackProcessed) {
	    logger.info("HandleAudioAck: Stopped.");
	    setState(LISTENING);
	    PlayoutPreviewListenerCallback callback = listeners.get(programs.remove(programID));
	    stopActive(callback, true, programID, playType, error);
	}
    }

    protected void audioServiceActivated (){
	if (state() == CONFIGURING){
	    logger.info("AudioServiceActivated: Audio Service is now activated");
	    setState (LISTENING);
	    /*
	    for (PlayoutPreviewListenerCallback callback: listeners.values()) {
		callback.activateAudio(true, playType, new String[] {});
	    }
	    */
	    activateAudio(true, playType, new String[] {});
	}
    }

    protected void displayError (String error){
	logger.error(error);
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: From: "+message.getSource()+" Type: " + message.getParam("serviceerror"));
	
	String serviceName = null;
	String errorCode = null;

	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	    logger.error("ReceiveErrorMessage: ServiceName=" + serviceName + " errorCode=" + errorCode);
	}
	
	//ErrorCode ignored here.
	if (serviceName != null && serviceName.equals(serviceInstanceName)) {
	    setState(CONFIGURING);
	    programs.clear();
	    audioDisconnected = false;
	    /*
	    for (PlayoutPreviewListenerCallback callback: listeners.values())
	    callback.activateAudio(false, playType, new String[] {RSController.ERROR_SERVICE_DOWN, serviceInstanceName});
	    */
	    activateAudio(false, playType, new String[] {RSController.ERROR_SERVICE_DOWN, serviceInstanceName});
	    //VVV send error to UI
	} else if (serviceName != null && serviceName.equals(RSController.RESOURCE_MANAGER)){
	    setState(CONFIGURING);
	    audioDisconnected = false;
	    rscActive = false;
	    programs.clear();
	    /*
	    for (PlayoutPreviewListenerCallback callback: listeners.values())
		callback.activateAudio(false, playType, new String[] {RSController.ERROR_SERVICE_DOWN, RSController.RESOURCE_MANAGER});
	    */
	    activateAudio(false, playType, new String[] {RSController.ERROR_SERVICE_DOWN, RSController.RESOURCE_MANAGER});
	    //VVV send error to UI
	}else {
	    logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
	    for (PlayoutPreviewListenerCallback callback: listeners.values()) {
		// VVV send error to UI
		//callback.playoutPreviewError(null, new String[]{null, playType});
		playoutPreviewError(callback, null, new String[]{null, playType});
	    }
	}

    }

    public synchronized int play(String fileID, PlayoutPreviewListenerCallback listener) {
	return play(fileID, -1, listener);
    }

    public synchronized int play(String fileID, int position, PlayoutPreviewListenerCallback listener) {

	if (state() != LISTENING && state() != PAUSE_ACTIVE && state() != STOP_COMMAND_SENT) {
	    logger.warn("Play: Unable to start since Provider not in LISTENING or PAUSE_ACTIVE states");
	    return -1;
	}

	logger.info("GRINS_USAGE:PREVIEW_PLAY:"+fileID);

	if (state() != PAUSE_ACTIVE)
	    sessionID = "";

	//      XXX not compatible with the test suite
	//	if (listeners.get(listener.getName()) == null) {
	//	    logger.warn("Play: Listener not registered with provider");
	//	    return -1;
	//	}

	this.fileID = fileID;
	this.activeListener = listener;
	if (listener != null){
	    programs.put(fileID, listener.getName());
	}else{
	    programs.put(fileID, "");
	}

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(AudioService.OPTION_POSITION, Integer.toString(position));

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.PLAY_MSG, 
							      fileID, playType, options);

	if (sendCommand(message) == 0){
	    logger.info("Play: Play command sent.");
	    if (state() == STOP_COMMAND_SENT)
		prematurePlay = true;
	    setState(PLAY_COMMAND_SENT);
	} else {
	    logger.error("Play: Unable to play since play message could not be sent.");
	    setState(LISTENING);
	    return -1;
	}

	return 0;
    }
    
    public synchronized int pause(String fileID, PlayoutPreviewListenerCallback listener) {
	if (!fileID.equals(this.fileID))
	    return -1;
	if (!programs.get(fileID).equals(listener.getName()))
	    return -1;
	if (state() != PLAY_ACTIVE){
	    logger.warn("Pause: Unable to pause since not in PLAY_ACTIVE state.");
	    return -1;
	}

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.PAUSE_MSG, 
							      fileID, playType, null);

	if (sendCommand(message) == 0){
	    logger.info("Pause: Pause command sent.");
	    setState(PAUSE_COMMAND_SENT);
	} else {
	    logger.error("Pause: Unable to pause since pause message could not be sent.");
	    setState(LISTENING);
	    return -1;
	}

	return 0;
    }

    public synchronized int stop(String fileID, PlayoutPreviewListenerCallback listener) {
	if (!fileID.equals(this.fileID)) {
	    logger.error("Stop: File Id provided:" + fileID + " is not the same as what we are playing:" + this.fileID);
	    return -1;
	}

	if (programs.get(fileID) == null) {
	    logger.error("Stop: Could not find entry for " + fileID + " in programs.");
	    return -1;
	}

	if (!programs.get(fileID).equals(listener.getName()))
	    return -1;

	if (state() != PLAY_ACTIVE && state() != PAUSE_ACTIVE){
	    logger.warn("Stop: Unable to stop since not in PLAY_ACTIVE state.");
	    return -1;
	}

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.STOP_MSG, 
							      fileID, playType, null);

	if (sendCommand(message) == 0){
	    logger.info("Stop: Stop command sent.");
	    setState(STOP_COMMAND_SENT);
	} else {
	    logger.error("Stop: Stop command could not be sent.");
	    setState(LISTENING);
	    return -1;
	}

	return 0;
    }

    public synchronized int forceStop(PlayoutPreviewListenerCallback listener) {
	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      "", AudioService.FORCE_STOP_MSG, 
							      "", playType, null);

	if (sendCommand(message) == 0){
	    logger.info("ForceStop: Stop command sent.");

	    if (activeListener != null)
		if (listener != activeListener)
		    activeListener.stopActive(true, fileID, playType, new String[]{});

	    prematurePlay = false;
	    setState(LISTENING);

	} else {
	    logger.error("ForceStop: Stop command could not be sent.");
	    setState(LISTENING);
	    return -1;
	}

	return 0;

    }
    
    public synchronized int volumeUp(String fileID, PlayoutPreviewListenerCallback listener) {
	if (!fileID.equals(this.fileID))
	    return -1;
	if (!programs.get(fileID).equals(listener.getName()))
	    return -1;

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(),
							      sessionID, AudioService.VOL_UP_MSG, 
							      "", playType, null);
	if (sendCommand(message) == 0){
	    logger.debug("VolumeUp: Volume up command sent.");
	} else {
	    logger.error("VolumeUp: Volume up command could not be sent.");
	    return -1;
	}

	return 0;
    }

    public synchronized int volumeDn(String fileID, PlayoutPreviewListenerCallback listener) {
	if (!fileID.equals(this.fileID))
	    return -1;
	if (!programs.get(fileID).equals(listener.getName()))
	    return -1;

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(),
							      sessionID, AudioService.VOL_DOWN_MSG, "", 
							      playType, null);
	if (sendCommand(message) == 0){
	    logger.debug("VolumeDn: Volume down command sent.");
	} else {
	    logger.error("VolumeDn: Volume down command could not be sent.");
	    return -1;
	}

	return 0;
    }

    public synchronized int seekToPosition (String fileID, int position, PlayoutPreviewListenerCallback listener){
	if (!fileID.equals(this.fileID))
	    return -1;
	if (!programs.get(fileID).equals(listener.getName()))
	    return -1;
	
	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(AudioService.OPTION_POSITION, Integer.toString(position));

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.SEEK_MSG, "", 
							      playType, options);

	if (sendCommand(message) == 0){
	    logger.debug("SeekToPosition: Seek command sent.");
	    return 0;
	} else {
	    logger.error("SeekToPosition: Seek command could not be sent.");
	    return -1;
	}
    }

    public boolean isPlaying() {
	return (state() != LISTENING);
    }

    public boolean isPlayable() {
	return (state() == LISTENING || state() == PAUSE_ACTIVE);	    
    }

    public boolean isPausable() {
	return (state() == PLAY_ACTIVE);
    }

    public boolean isStoppable(String programID, PlayoutPreviewListenerCallback listener) {
	return (state() == PLAY_ACTIVE || state() == PAUSE_ACTIVE) && programID.equals(fileID) && programs.get(fileID).equals(listener.getName());
    }

    public boolean isVolumeAdjustable() {
	return (state() == PLAY_ACTIVE || state() == PAUSE_ACTIVE);
    }

    protected int state() {
	return state;
    }

    protected void setState(int state) {
	prevState = this.state;
	this.state = state;
	logger.info("Previous state: "+prevState+" Current state: "+state);
	
	switch(state) {
	case CONFIGURING:
	    // disable UI
	    break;
	case LISTENING:
	    // enable UI
	    sessionID = "";
	    break;
	case PLAY_ACTIVE:
	case PAUSE_ACTIVE:
	    // make changes in UI
	    break;
	}
    }

}

