package org.gramvaani.radio.app.providers;

import org.gramvaani.simpleipc.IPCMessage;
import org.gramvaani.radio.app.providers.*;

import java.util.Hashtable;

public interface RSProvider {
    public int activate();
    public String getName();
    public void receiveMessage(IPCMessage message);
    public void receiveErrorMessage(IPCMessage message);
    
    public void registerWithProvider(ListenerCallback listener);
    public void unregisterWithProvider(String listenerName);
    public boolean isActive();

}
