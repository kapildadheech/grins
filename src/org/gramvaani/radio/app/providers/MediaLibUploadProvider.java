package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.UpdateProgramMessage;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.FlushIndexMessage;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.gui.*;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.*;
import java.io.*;
import java.net.*;


public class MediaLibUploadProvider extends RSProviderBase implements Runnable, DiagnosticUtilitiesServletCallback, FileStoreUploadListener {
    public static final String PENDING = "PENDING";
    public static final String DONE    = "DONE";
    public static final String ERROR   = "ERROR";

    public static final String FAILURE_MSG = "FAILURE";
    public static final String SUCCESS_MSG = "SUCCESS";

    public static final String ERROR_FILE_PATH    = FileStoreUploader.ERROR_FILE_PATH;
    public static final String ERROR_NO_SPACE     = FileStoreUploader.ERROR_NO_SPACE;
    public static final String ERROR_NOT_WRITABLE = FileStoreUploader.ERROR_NOT_WRITABLE;
    public static final String ERROR_UNKNOWN	  = FileStoreUploader.ERROR_UNKNOWN;
    public static final String ERROR_NOT_READY    = FileStoreUploader.ERROR_NOT_READY;
    public static final String ERROR_MAX_EXCEEDED = FileStoreUploader.ERROR_MAX_EXCEEDED;
    public static final String ERROR_DB_FAILED    = FileStoreUploader.ERROR_DB_FAILED;
    public static final String ERROR_NOT_SUPPORTED= FileStoreUploader.ERROR_NOT_SUPPORTED;
    public static final String ERROR_FILE_EXISTS  = FileStoreUploader.ERROR_FILE_EXISTS;
    public static final String ERROR_FILE_COPY_FAILED = FileStoreUploader.ERROR_FILE_COPY_FAILED;

    public static final String UPLOAD_FILE_PARAM = "uploadfile";

    protected static final int CONFIGURING = -1;
    protected static final int LISTENING = 0;
    protected static final int UPLOAD_ACTIVE = 1;

    protected static final int MAX_UPLOAD_ATTEMPTS = 3;
    
    static final int MB_CONVERTER = 1024*1024;

    protected int state = CONFIGURING;
    protected int prevState = state;
    protected boolean isIndexServiceActive = false;
    protected DiagnosticUtilities diagUtil;
    protected boolean recoveryAttempted = false;
    protected boolean rscActive = true;
    protected int uploadAttempts = 0;
    protected final Object waitObject = new Object();
    protected StationConfiguration stationConfig;
    protected StationNetwork stationNetwork;
    
    private String listenerName;
    private Thread uploaderThread;
    private Hashtable<String, MediaLibUploadListenerCallback> listeners;
    private Hashtable<String, String> keyListenerMap;
    private Hashtable<String, Integer> fileUploadAttempts;
    private LinkedBlockingDeque<String> uploadQueue;
    private Hashtable<String, Boolean> fileDBInsertMap;
    private Hashtable<String, Boolean> fileUpdateIndexMap;
    private Hashtable<String, String> fileLibFileNameMap;
   
    private FileStoreUploader fileStoreUploader;
    
    public MediaLibUploadProvider(UIService uiService) {
	super (uiService, RSApp.MEDIALIB_UPLOAD_PROVIDER); 
	listeners = new Hashtable<String, MediaLibUploadListenerCallback>();
	keyListenerMap = new Hashtable<String, String>();
	fileUploadAttempts = new Hashtable<String, Integer>();
	uploadQueue = new LinkedBlockingDeque<String>();
	fileDBInsertMap = new Hashtable<String, Boolean>();
	fileUpdateIndexMap = new Hashtable<String, Boolean>();
	fileLibFileNameMap = new Hashtable<String, String>();

	stationConfig = uiService.getStationConfiguration();
	stationNetwork = stationConfig.getStationNetwork();
	MediaLib.setIPCServer(uiService.getIPCServer());
	fileStoreUploader = new FileStoreUploader(this, stationConfig, RSApp.MEDIALIB_UPLOAD_PROVIDER, uiService.getTimeKeeper());
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities();

	uploaderThread = new Thread(this, "MediaLibUploadProvider:UploaderThread");
	logger.debug("MediaLibUploadProvider: Initialized.");
    }
    
    public synchronized int activate() {
	logger.info("Activate: Activating widget.");

	fileStoreUploader.activate();
	if (!uploaderThread.isAlive())
	    uploaderThread.start();


	setState(CONFIGURING);
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String[] {RSController.LIB_SERVICE,
										  RSController.INDEX_SERVICE});
	int retval = sendCommand(message);
	if (retval == -1) {
	    rscActive = false;
	} else {
	    fileStoreUploader.connectionUp(true);
	}
	return retval;
    }

    public void registerWithProvider(ListenerCallback listener){
	this.listeners.put(listener.getName(), (MediaLibUploadListenerCallback)listener);
    }

    public void unregisterWithProvider(String name){
	listeners.remove(name);
    }

    public synchronized void receiveMessage(IPCMessage message){
	logger.debug("ReceiveMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	boolean isUploadActivated = false;
	if (message instanceof ServiceNotifyMessage){
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage) message;
	    for (String service: notifyMessage.getActivatedServices()){
		if (service.equals(RSController.LIB_SERVICE)){
		    isUploadActivated = true;
		    uploadActivated();
		}
		else if (service.equals(RSController.INDEX_SERVICE))
		    isIndexServiceActive = true;
		else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController active now.");
		    if (fileStoreUploader != null) 
			fileStoreUploader.connectionUp(true);
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing. Wait for lib service to be activated.
			logger.warn("Waiting for LibService to be activated.");
		    }
		}
	    }
	    for (String service: notifyMessage.getInactivatedServices()) {
		if (service.equals(RSController.LIB_SERVICE)) {
		    setState(CONFIGURING);
		    for (MediaLibUploadListenerCallback listener: listeners.values()) {
			listener.activateMediaLibUpload(false, new String[]{RSController.ERROR_SERVICE_DISCONNECTED,
									    RSController.LIB_SERVICE});
			// VVV send error message to inference engine
		    }
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController disconnected.");
		    setState(CONFIGURING);
		    for (MediaLibUploadListenerCallback listener: listeners.values()) {
			listener.activateMediaLibUpload(false, new String[]{RSController.ERROR_SERVICE_DISCONNECTED,
									    RSController.RESOURCE_MANAGER});
		    }
		    //VVV send error message to inference engine
		}
	    }
	} else if (message instanceof ServiceAckMessage){
	    ServiceAckMessage ackMessage = (ServiceAckMessage) message;
	    for (String service: ackMessage.getActiveServices()) {
		if (service.equals(RSController.LIB_SERVICE)){
		    isUploadActivated = true;
		    uploadActivated();
		}
		if (service.equals(RSController.INDEX_SERVICE))
		    isIndexServiceActive = true;
	    }

	    for (String service: ackMessage.getInactiveServices()) {
		if (service.equals(RSController.LIB_SERVICE)){
		    isUploadActivated = false;
		    setState (LISTENING);
		    for (MediaLibUploadListenerCallback listener: listeners.values())
			listener.activateMediaLibUpload(true, new String[]{RSController.ERROR_SERVICE_DOWN,
									   service});
		}
		if (service.equals(RSController.INDEX_SERVICE))
		    isIndexServiceActive = false;
	    }
	} else if (message instanceof UploadDoneAckMessage) {
	    fileStoreUploader.receiveMessage((UploadDoneAckMessage)message);
	}

    }

    protected void addToQueue(String fileName){
	synchronized(uploadQueue){
	    if (!uploadQueue.contains(fileName)) {
		fileUploadAttempts.put(fileName, 0);
		uploadQueue.addLast(fileName);
	    }
	}
    }

    public synchronized void cancelUpload(){
	uploadQueue.clear();
	fileUploadAttempts.clear();
	keyListenerMap.clear();
	fileDBInsertMap.clear();
	fileUpdateIndexMap.clear();
	fileLibFileNameMap.clear();
    }

    public synchronized void uploadFile(String fileName, String listenerName) {
	uploadFile(fileName, listenerName, null, null, null);
    }

    public synchronized void uploadFile(String fileName, String listenerName, String libFileName, Boolean doDBInsert, Boolean updateIndex) {
	if (state() == LISTENING) {
	    File file = new File(fileName);
	    try {
		fileName = file.getCanonicalPath();

		if (file.length() > stationConfig.getIntParam(StationConfiguration.UPLOAD_MAX_FILESIZE) * MB_CONVERTER){
		    uploadError(fileName, ERROR_MAX_EXCEEDED, listenerName, 0, StationNetwork.UPLOAD);
		    return;
		}

		if (doDBInsert != null)
		    fileDBInsertMap.put(fileName, doDBInsert);

		if (libFileName != null) 
		    fileLibFileNameMap.put(fileName, libFileName);

		if (updateIndex != null) 
		    fileUpdateIndexMap.put(fileName, updateIndex);

		keyListenerMap.put(fileName, listenerName);
		addToQueue(fileName);

	    } catch(Exception e) {
		logger.error("UploadFile: Unable to upload file: " + fileName, e);
		uploadError(fileName, ERROR_FILE_PATH, listenerName, 0, "");
		return;
	    }
	    logger.debug("UploadFile: Uploading file: "+fileName);
	} else {
	    logger.error("UploadFile: Unable to upload file "+fileName+", since not in LISTENING state.");
	    uploadError(fileName, ERROR_NOT_READY, listenerName, 0, StationNetwork.UPLOAD);
	}
    }

    
    public synchronized void uploadDir(String dirName, String listenerName) {
	if (state() == LISTENING) {
	    try {
		File dirFile = new File(dirName);
		File[] dirFiles = dirFile.listFiles();
		for (File file: dirFiles) {
		    if (file.isFile() && isAudioFile(file)) {
			addToQueue(file.getCanonicalPath());
			keyListenerMap.put(file.getCanonicalPath(), listenerName);
		    }
		}
	    } catch(Exception e) {
		logger.error("Unable to read files to upload: " + dirName, e);
		uploadError(dirName, ERROR_UNKNOWN, listenerName, 0, "");
	    }

	    logger.debug("UploadDir: Uploading directory: "+dirName);
	} else {
	    logger.error("UploadDir: Unable to upload directory "+dirName+", since not in LISTENING state.");
	    uploadError(dirName, ERROR_NOT_READY, listenerName, 0, StationNetwork.UPLOAD);
	}
    }
    
    public void attemptUpload(){
	synchronized(waitObject){
	    recoveryAttempted = true;
	    waitObject.notifyAll();
	}
    }

    protected void recoverFromError(String fileName, String errorCode, String listenerName, int uploadAttempts){
	if (errorCode.equals(RSController.ERROR_SERVLET_CONNECTION_FAILED)){
	    logger.info("RecoverFromError: Connection error detected.");
	    recoveryAttempted = false;

	    synchronized(uploadQueue){
		    if (uploadQueue.contains(fileName)){
			uploadQueue.removeLastOccurrence(fileName);
			fileUploadAttempts.remove(fileName);
		    }
		    fileUploadAttempts.put(fileName, uploadAttempts);
		    keyListenerMap.put(fileName, listenerName);
		    uploadQueue.addFirst(fileName);
	    }
	    synchronized(waitObject){
		diagUtil.servletError(StationNetwork.UPLOAD, this);
		while (!recoveryAttempted){
		    try{
			waitObject.wait();
		    }catch(InterruptedException e){}
		}
	    }
	}
    }

    protected String getLibFileName(String fileName, String fileStore) {
	if (fileStore.equals(StationNetwork.UPLOAD)) {
	    String libFileName = fileLibFileNameMap.get(fileName);
	    if (libFileName != null) {
		return libFileName;
	    } else {
		return "";
	    }
	} else {
	    return fileName;
	}
    }

    protected void sendFileToUpload(String fileName, String listenerName, int uploadAttempts) {
	String checksum = FileUtilities.getSha1Hex(fileName);

	if (checksum != null){
	    RadioProgramMetadata queryProgram = RadioProgramMetadata.getDummyObject("");
	    queryProgram.setChecksum(checksum);
	    MediaLib medialib = MediaLib.getMediaLib(stationConfig, uiService.getTimeKeeper());
	    RadioProgramMetadata result = medialib.getSingle(queryProgram);

	    if (result != null){
		//Duplicate file detected.
		MediaLibUploadListenerCallback listener;
		if (listenerName != null && (listener = listeners.get(listenerName)) != null)
		    listener.uploadSuccess(fileName, result.getItemID(), uploadQueue.size(), true);
		return;
	    }
	}

	String returnCode;
	String libFileName = getLibFileName(fileName, StationNetwork.UPLOAD);

	if (stationNetwork.isLocalStore(StationNetwork.UPLOAD)) {
	    logger.info("SendFileToUpload: File store is local. Copying directly.");
	    copyFileLocally(fileName, libFileName, StationNetwork.UPLOAD, listenerName, uploadAttempts);
	} else {
	    logger.info("SendFileToUpload: File store is remote. Uploading through servlet.");
	    sendFileToServlet(fileName, StationNetwork.UPLOAD, listenerName, uploadAttempts, libFileName);
	}
    }
    
    protected void sendFileToPlayout(String srcFileName, String libFileName) {
	String returnCode;

	if (stationNetwork.isSameFileStore(StationNetwork.UPLOAD, StationNetwork.AUDIO_PLAYOUT)) {
	    logger.info("SendFileToPlayout: Upload and playout are the same file stores.");
	} else {
	    if (stationNetwork.isLocalStore(StationNetwork.AUDIO_PLAYOUT)) {
		logger.info("SendFileToPlayout: Calling copy file locally.");
		copyFileLocally(srcFileName, libFileName, StationNetwork.AUDIO_PLAYOUT, "", 0);
	    } else {
		logger.info("SendFileToPlayout: Playout store is remote.");
		//Do nothing. Lib service will push it to Playout.
	    }
	}
    }

    //Note: Order of calls to sendFileToPlayout and sendFileToPreview is important
    protected void sendFileToPreview(String srcFileName, String libFileName) {
	String returnCode;
	if (stationNetwork.isSameFileStore(StationNetwork.UPLOAD, StationNetwork.AUDIO_PREVIEW)) {
	    logger.info("SendFileToPreview: Upload and preview are the same file stores.");	
	} else if (stationNetwork.isSameFileStore(StationNetwork.AUDIO_PLAYOUT, StationNetwork.AUDIO_PREVIEW)) {
	    logger.info("SendFileToPreview: Playout and preview are the same file stores.");
	} else {
	    if (stationNetwork.isLocalStore(StationNetwork.AUDIO_PREVIEW)) {
		logger.info("SendFileToPreview: Copy file locally.");
		copyFileLocally(srcFileName, libFileName, StationNetwork.AUDIO_PREVIEW, "", 0);
	    } else {
		//Do nothing. Lib service will push it to Preview
		logger.debug("SendFileToPreview: LibService will push file to preview");
	    }
	}
    }
    
    protected void copyFileLocally(String srcFileName, String libFileName, String fileStore, String listenerName, int uploadAttempts) {
	String storeDir = stationNetwork.getStoreDir(fileStore);
	String returnCode;

	if (shouldDoDBInsert(srcFileName, fileStore)) {
	    returnCode = fileStoreUploader.copyFile(srcFileName, libFileName, true, fileStore);
	} else {
	    logger.info("copying file locally.:" + srcFileName + " " + libFileName);
	    returnCode = fileStoreUploader.copyFile(srcFileName, libFileName, false, fileStore);
	}
	
	if (!FileUtilities.isSupported(returnCode)) {
	    uploadError(srcFileName, returnCode, listenerName, uploadAttempts, fileStore);
	} else {
	    logger.info("Upload successful: " + srcFileName);
	    uploadSuccess(srcFileName, returnCode, uploadQueue.size(), listenerName, fileStore);
	}

    }

    protected void sendFileToServlet(String fileName, String fileStore, String listenerName, int uploadAttempts) {
	sendFileToServlet(fileName, fileStore, listenerName, uploadAttempts, "");
    }

    protected void sendFileToServlet(String fileName, String fileStore, String listenerName, int uploadAttempts, String libFileName) {
	String returnCode;

	if (shouldDoDBInsert(fileName, fileStore)) {
	    returnCode = fileStoreUploader.uploadFile(fileName, fileStore, true, libFileName);
	} else {
	    returnCode = fileStoreUploader.uploadFile(fileName, fileStore, false, libFileName);
	}

	if (!FileUtilities.isSupported(returnCode)) {
	    uploadError(fileName, returnCode, listenerName, uploadAttempts, fileStore);
	} else {
	    logger.info("Upload successful: " + fileName);
	    uploadSuccess(fileName, returnCode, uploadQueue.size(), listenerName, fileStore);
	}
    }

    public void run() {
	byte[] inBytes = new byte[1024];
	String url;

	while (true) {
	    String key = null;
	    while (key == null){
		try{
		    key = uploadQueue.takeFirst();
		    listenerName = keyListenerMap.remove(key);
		    uploadAttempts = fileUploadAttempts.remove(key);

		    uploadAttempts++;
		    break;

		} catch (Exception e){
		    logger.error("Run: Interrupted exception.", e);
		}
	    }

	    sendFileToUpload(key, listenerName, uploadAttempts);

	}
    }
    
    protected void uploadActivated (){
	if (state() == CONFIGURING){
	    logger.info("UploadActivated: Library upload is now activated");
	    setState (LISTENING);
	    for (MediaLibUploadListenerCallback listener: listeners.values())
		listener.activateMediaLibUpload(true, new String[]{});
	}
    }

    protected void displayError (String error){
	logger.error(error);
	//VVV send error to inference engine
    }

    protected void uploadError(String fileName, String error, String listenerName, int uploadAttempts, String fileStore) {
	MediaLibUploadListenerCallback listener = null;
	logger.error("UploadError: file: " + fileName + " error: " + error + " upload attempts: " + uploadAttempts + "fileStore: " + fileStore);

	//Process the error only if it has happened at the upload store
	if (!fileStore.equals(StationNetwork.UPLOAD)) {
	    fileDBInsertMap.remove(fileName);
	    fileLibFileNameMap.remove(fileName);
	    fileUpdateIndexMap.remove(fileName);
	    return;
	}

	if (listenerName != null && (listener = listeners.get(listenerName)) != null)
	    listener.uploadError(fileName, error, uploadQueue.size());

	if (error.equals(RSController.ERROR_SERVLET_CONNECTION_FAILED)) {
	
	    if (uploadAttempts == MAX_UPLOAD_ATTEMPTS) {
		logger.error("Upload failed "+uploadAttempts+" times. Giving up uploading of file: " + fileName);
		fileDBInsertMap.remove(fileName);
		fileUpdateIndexMap.remove(fileName);
		fileLibFileNameMap.remove(fileName);
		if (listenerName != null && (listener = listeners.get(listenerName)) != null)
		    listener.uploadGiveup(fileName);
		uploadAttempts = 0;
	    } else {
		recoverFromError(fileName, error, listenerName, uploadAttempts);
	    }
	
	} else if (error.equals(ERROR_DB_FAILED)){
	    StatusCheckRequestMessage msg = new StatusCheckRequestMessage(RSController.ANON_SERVICE, 
									  RSController.LIB_SERVICE, 
									  "", "1");
	    if (sendCommand(msg) == -1)
	    	logger.error("UploadError: Could not send status check message to library service.");
	
	} else {
	    fileDBInsertMap.remove(fileName);
	    fileUpdateIndexMap.remove(fileName);
	    fileLibFileNameMap.remove(fileName);
	}
		
    }

    protected void uploadSuccess(String fileName, final String libFileName, int numRemaining, String listenerName, String fileStore){
	logger.info("UploadSuccess: fileName =" + fileName + ", libFileName =" + libFileName + ", fileStore=" + fileStore);
	File file = new File(fileName);
	String srcFileName = file.getName();

	if (fileStore.equals(StationNetwork.UPLOAD)) {
	        
	    //Note ordering of the calls is important here
	    if (!srcFileName.equals(DiagnosticsController.DIAGNOSTICS_PLAYOUT_FILE)) {
		sendFileToPlayout(fileName, libFileName);
		sendFileToPreview(fileName, libFileName);
	    
		if (shouldUpdateIndex(fileName)) {
		    Thread t = new Thread(){
			    public void run(){
				
				UpdateProgramMessage updateMessage = new UpdateProgramMessage("", RSController.INDEX_SERVICE, new String[] {libFileName});
				FlushIndexMessage flushIndexMessage = new FlushIndexMessage("", RSController.INDEX_SERVICE);
				if (isIndexServiceActive){
				    try{
					sendSyncCommand(updateMessage);
					sendSyncCommand(flushIndexMessage);
				    } catch (Exception e) {
					logger.error("UploadSuccess: Unable to send update message.", e);
				    }
				}
				
			    }
			};
		    
		    t.start();
		}
	    }
	    
	    fileDBInsertMap.remove(fileName);
	    fileLibFileNameMap.remove(fileName);
	    fileUpdateIndexMap.remove(fileName);

	    MediaLibUploadListenerCallback listener;
	    if (listenerName != null && (listener = listeners.get(listenerName)) != null)
		listener.uploadSuccess(fileName, libFileName, numRemaining, false);
	}
    }

    public void updateProgress(String fileName, long uploadedLength, long totalLength){
	MediaLibUploadListenerCallback listener;
	if (listenerName != null && (listener = listeners.get(listenerName)) != null)
	    listener.updateProgress(fileName, uploadedLength, totalLength);
    }

    public boolean isActive(){
	if (state() == CONFIGURING)
	    return false;
	else
	    return true;
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: From: "+message.getSource()+" Type: " + message.getParam("serviceerror"));
	
	String serviceName = null;
	String errorCode = null;
	
	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}
	
	//ErrorCode ignored here.

	if (serviceName != null){
	    if (serviceName.equals(RSController.LIB_SERVICE)){
		setState(CONFIGURING);
		for (MediaLibUploadListenerCallback listener: listeners.values()) {
		    listener.activateMediaLibUpload(false, new String[]{RSController.ERROR_SERVICE_DOWN,
									RSController.LIB_SERVICE});
		}
	    } else if (serviceName.equals(RSController.INDEX_SERVICE)){
		isIndexServiceActive = false;
	    } else if (serviceName.equals(RSController.RESOURCE_MANAGER)){
		isIndexServiceActive = false;
		rscActive = false;
		setState(CONFIGURING);
		for (MediaLibUploadListenerCallback listener: listeners.values())
		    listener.activateMediaLibUpload(false, new String[]{RSController.ERROR_SERVICE_DOWN,
									RSController.RESOURCE_MANAGER});
	    } else {
		logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
		// VVV send error to inference engine
	    }
	}
    }

    protected int state() {
	return state;
    }

    protected void setState(int state) {
	if (this.state != state){
	    prevState = this.state;
	    this.state = state;
	    logger.info("Previous state: "+prevState+" Current state: "+state);
	    
	    switch(state) {
	    case CONFIGURING:
		// disable UI
		break;
	    case LISTENING:
		// enable UI
		break;
	    }
	}
    }

    protected boolean isAudioFile(File file) {
	String name = file.getName();
	return FileUtilities.isSupported(name);
    }

    protected boolean shouldUpdateIndex(String fileName) {
	Boolean updateIndex = fileUpdateIndexMap.get(fileName);
	
	logger.info("ShouldUpdateIndex: updateIndex=" + updateIndex + " fileName=" + fileName);

	if (updateIndex != null) {
	    return updateIndex;
	} else {
	    return true;
	}
    }

    protected boolean shouldDoDBInsert(String fileName, String fileStore) {
	Boolean doDBInsert = fileDBInsertMap.get(fileName);

	if (doDBInsert != null) {
	    return doDBInsert;
	} else if (fileStore.equals(StationNetwork.UPLOAD)) {
	    return true;
	} else {
	    return false;
	}
    }

    public int sendUploadDoneMessage(UploadDoneMessage message) {
	message.setWidgetName(RSApp.MEDIALIB_UPLOAD_PROVIDER);
	int retVal = sendCommand(message);
	if (retVal == -1) {
	    fileStoreUploader.connectionUp(false);
	}

	return retVal;
    }

}
