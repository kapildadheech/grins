package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.app.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;


public class CacheProvider extends RSProviderBase implements MediaLibListener, MediaLibActivationListener {
    protected final static int CONFIGURING = -1;
    protected final static int LISTENING = 0;
    protected final static int REGISTERING = 1;
    protected final static int REGISTERED = 2;

    protected final static int POOL_THREAD_COUNT = 1;

    public static final String ORDER_ASC  = MediaLib.ORDER_ASC;
    public static final String ORDER_DESC = MediaLib.ORDER_DESC;

    protected int MAX_CACHE_OBJECTS;
    
    protected final Hashtable<String, CacheListenerCallback> listeners;
    protected MediaLib medialib;
    boolean isMediaLibActive = false;

    LinkedHashMap<String, MetadataCacheObject> objectHash;
    Hashtable<String, UniqueListCacheObject> listObjectHash;
    Hashtable<String, String> dependencyHash;
    Hashtable<String, String> invDependencyHash;
    Hashtable<String, ArrayList<CacheObjectListener>> objectListeners;
    Metadata[] allCategories = null;
    ExecutorService listenersActivator;

    Hashtable<String, String> anchorNames;

    protected int state;
    protected boolean cacheEnabled = true;
    protected boolean rscActive = true;

    public CacheProvider(UIService uiService) {
	super (uiService, RSApp.CACHE_PROVIDER); 
	listeners = new Hashtable<String, CacheListenerCallback>();

	/*
	MediaLib.registerListener(RSApp.CACHE_PROVIDER, this);
	MediaLib.setIPCServer(uiService.getIPCServer());
	medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
	if (medialib != null) {
	    isMediaLibActive = true;
	    medialib.setCacheCallback(this);
	}
	*/

	MAX_CACHE_OBJECTS = uiService.getStationConfiguration().getIntParam(StationConfiguration.MAX_CACHE_OBJECTS);
	objectHash = new LinkedHashMap<String, MetadataCacheObject>(MAX_CACHE_OBJECTS, 0.75F, true) {

	    protected boolean removeEldestEntry(Map.Entry eldest) {
		if (size() > MAX_CACHE_OBJECTS - 2) {
		    String key = genKey(((MetadataCacheObject)(eldest.getValue())).getMetadataCacheObject());
		    clearDependencies(key);
		    return true;
		}
		return false;
	    }
	};

	
	listObjectHash = new Hashtable<String, UniqueListCacheObject>();
	dependencyHash = new Hashtable<String, String>();
	invDependencyHash = new Hashtable<String, String>();
	objectListeners = new Hashtable<String, ArrayList<CacheObjectListener>>();
	listenersActivator = Executors.newFixedThreadPool(POOL_THREAD_COUNT);

	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    listenersActivator.shutdown();
		}
	    });

	anchorNames = new Hashtable<String, String>();
	anchorNames.put("RadioProgramMetadata." + RadioProgramMetadata.getLanguageFieldName(), 
			SearchFilter.LANGUAGE_FILTER + "." + LanguageFilter.LANGUAGE_ANCHOR);
	anchorNames.put("RadioProgramMetadata." + RadioProgramMetadata.getTypeFieldName(), 
			SearchFilter.TYPE_FILTER + "." + TypeFilter.TYPE_ANCHOR);
	anchorNames.put("Entity." + Entity.getLocationFieldName(), SearchFilter.CREATOR_FILTER + "." + CreatorFilter.LOCATION_ANCHOR);
	anchorNames.put("Entity." + Entity.getNameFieldName(), SearchFilter.CREATOR_FILTER + "." + CreatorFilter.NAME_ANCHOR);
	anchorNames.put("Creator." + Creator.getAffiliationFieldName(), SearchFilter.CREATOR_FILTER + "." + CreatorFilter.AFFILIATION_ANCHOR);
	anchorNames.put("Entity." + Entity.getEntityTypeFieldName(), SearchFilter.CREATOR_FILTER + "." + CreatorFilter.ROLE_ANCHOR);
    }

    public int activate() {
	logger.info("Activate: Cache provider activated");
	
	MediaLib.registerListener(RSApp.CACHE_PROVIDER, this);
	MediaLib.setIPCServer(uiService.getIPCServer());
	medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
	if (medialib != null) {
	    isMediaLibActive = true;
	    medialib.setActivationListener(this);
	}

	setState(CONFIGURING);
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String [] {RSController.LIB_SERVICE});
	int retval = sendCommand(message);
	if (retval == -1)
	    rscActive = false;

	return retval;
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listeners.put(listener.getName(), (CacheListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	listeners.remove(listenerName);
    }

    public synchronized boolean isActive() {
	
	//System.out.println("CACHEPROVIDER: state: "+state()+" ml "+isMediaLibActive);

	if (state() != REGISTERED)
	    return false;
	else
	    return isMediaLibActive;
    }

    public synchronized void activateMediaLib(boolean activate){
	if (activate){/*
	    medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
	    if (state() == REGISTERED){
		for (CacheListenerCallback listener: listeners.values())
		    listener.activateCache(true);
		    }*/
	} else {
	    if (isActive()){
		//for (CacheListenerCallback listener: listeners.values())
		//    listener.activateCache(false, new String[]{RSController.ERROR_DB_FAILED});
		callbackListeners(false, new String[]{RSController.ERROR_DB_FAILED});
	    }
	}
	isMediaLibActive = activate;
    }

    public synchronized void receiveMessage(IPCMessage message){
	logger.debug("Received Message from: "+message.getSource()+" type: "+message.getMessageClass());
	String disconnectedService = "";
	if (message instanceof ServiceNotifyMessage) {

	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    boolean inactivate = false;
	    for (String service: notifyMessage.getInactivatedServices()) {
		if (service.equals(RSController.LIB_SERVICE)) {
		    logger.debug("ReceiveMessage: Got lib service inactive.");
		    inactivate = true;
		    disconnectedService = RSController.LIB_SERVICE;
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController disconnected.");
		    inactivate = true;
		    disconnectedService = RSController.RESOURCE_MANAGER;
		}

		if (inactivate &&  isActive()){
		    /*
		    for (CacheListenerCallback listener: listeners.values()){
			listener.activateCache(false, new String[] {RSController.ERROR_SERVICE_DISCONNECTED,
								    disconnectedService});
			// VVV send error inference engine
		    }
		    */
		    callbackListeners(false, new String[] {RSController.ERROR_SERVICE_DISCONNECTED,
							   disconnectedService});
		    setState(CONFIGURING);
		}
	    }

	    for (String service: notifyMessage.getActivatedServices()){
		if (service.equals(RSController.LIB_SERVICE)) {
		   	logger.debug("ReceiveMessage: Lib service now ready.");
			setState(LISTENING);
			sendRegisterMessage();
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController active now.");
		    MediaLib.setIPCServer(uiService.getIPCServer());
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing. Wait for lib service to be activated.
		    }

		    refreshCache();
		}
	    }
	    
	} else if (message instanceof ServiceAckMessage){

	    ServiceAckMessage serviceAckMessage = (ServiceAckMessage) message;
	    for (String service: serviceAckMessage.getActiveServices()){
		if (service.equals(RSController.LIB_SERVICE)) {
		    setState(LISTENING);
		    sendRegisterMessage();
		}
	    }

	    for (String service: serviceAckMessage.getInactiveServices()){
		if (service.equals(RSController.LIB_SERVICE)) {
		    setState(CONFIGURING);
		    /*
		    for (CacheListenerCallback listener: listeners.values()) {
			listener.activateCache(false, new String[]{RSController.ERROR_SERVICE_DOWN,
								   RSController.LIB_SERVICE});
		    }
		    */
		    callbackListeners(false, new String[]{RSController.ERROR_SERVICE_DOWN,
							  RSController.LIB_SERVICE});
		}
	    }

	} else if (message instanceof RegisterCacheProviderAckMessage) {

	    if (state() == REGISTERING) {
		if (((RegisterCacheProviderAckMessage)message).getStatus().equals(LibService.SUCCESS_MSG)) {
		    setState(REGISTERED);
		    if (medialib == null)
			medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
		    if (medialib != null){
			isMediaLibActive = true;
			medialib.setActivationListener(this);
			callbackListeners(true, new String[]{});
		    }
		} else {
		    setState(LISTENING);
		}
	    }

	} else if (message instanceof InvalidateCacheMessage) {
	    
	    InvalidateCacheMessage cacheMessage = (InvalidateCacheMessage)message;
	    handleInvalidateCacheMessage(cacheMessage.getObjectType(), cacheMessage.getActionType(), cacheMessage.getObjectIDs());

	} else if (message instanceof InvalidateCacheKeyMessage) {

	    InvalidateCacheKeyMessage cacheMessage = (InvalidateCacheKeyMessage)message;
	    handleInvalidateCacheKeyMessage(cacheMessage.getObjectType(), cacheMessage.getActionType(), cacheMessage.getOldObjectIDs(),
					    cacheMessage.getNewObjectIDs());

	}
    }
    
    protected void callbackListeners(boolean activate, String[] error) {
	CallbackListenersTask callbackTask = new CallbackListenersTask(activate, error);
	listenersActivator.execute(callbackTask);
    }

    protected void sendRegisterMessage() {
	RegisterCacheProviderMessage registerMessage = new RegisterCacheProviderMessage("", RSController.LIB_SERVICE, getName());
	if (sendCommand(registerMessage) > -1)
	    setState(REGISTERING);
	else
	    logger.error("sendRegisterMessage: Unable to send cache register to lib service");
    }
    
    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: from: "+message.getSource()+" Error: " + message.getParam("serviceerror"));
	
	String serviceName = null;
	String errorCode = null;
	
	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}

	//ErrorCode ignored here.
	if (serviceName != null && (serviceName.equals(RSController.LIB_SERVICE))){ 
	    if (isActive()){
		/*
		for (CacheListenerCallback listener: listeners.values()){
		    listener.activateCache(false, new String[]{RSController.ERROR_SERVICE_DOWN,
							       RSController.LIB_SERVICE});
		    // VVV send error to inference engine
		}
		*/
		callbackListeners(false, new String[]{RSController.ERROR_SERVICE_DOWN,
						      RSController.LIB_SERVICE});
	    }
	    setState(CONFIGURING);
	} else if (serviceName != null && serviceName.equals(RSController.RESOURCE_MANAGER)){
	    if (isActive()){
		/*
		for (CacheListenerCallback listener: listeners.values()){
		    listener.activateCache(false, new String[]{RSController.ERROR_SERVICE_DOWN,
							       RSController.RESOURCE_MANAGER});
		    //VVV send error to inference engine
		}
		*/
		callbackListeners(false, new String[]{RSController.ERROR_SERVICE_DOWN,
						      RSController.RESOURCE_MANAGER});
	    }
	    setState(CONFIGURING);
	    rscActive = false;
	} else {
	    logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
		// VVV send error to inference engine
	}

    }


    protected int state() {
	return state;
    }

    protected void setState(int state) {
	int previousState = this.state;
	this.state = state;
	logger.info("Previous state: "+previousState+" Current State: "+state);

	switch(state) {
	case CONFIGURING:
	    // disable UI
	    break;
	case LISTENING:
	    // enable UI
	    break;
	}
    }

    @SuppressWarnings("unchecked")
    public synchronized void mediaLibUp(MediaLib medialib) {
	LinkedHashMap<String, MetadataCacheObject> tmpObjectHash = (LinkedHashMap<String, MetadataCacheObject>)objectHash.clone();
	//Iterator<String> iter = (Iterator<String>)(tmpObjectHash.keySet().iterator());
	//while (iter.hasNext()) {
	//    String key = iter.next();
	for (String key: tmpObjectHash.keySet()){
	    MetadataCacheObject obj = objectHash.get(key);
	    if (obj != null && obj.isDirty()) {
		if (obj.getMetadataCacheObject() != null && obj.getMetadataCacheObject() instanceof RadioProgramMetadata) {
		    RadioProgramMetadata dummyProgram = new RadioProgramMetadata(((RadioProgramMetadata)obj.getMetadataCacheObject()).getItemID());
		    getSingle(dummyProgram);
		}
	    }
	}
    }

    @SuppressWarnings("unchecked")
    protected synchronized void refreshCache() {
	logger.debug("refreshCache: Entering");
	if (objectHash.size() == 0)
	    return;

	medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
	if (medialib == null) {
	    logger.error("RefreshCache: Cache cannot be refreshed. Medialib is null.");
	    return;
	}
	medialib.setActivationListener(this);

	LinkedHashMap tmpObjectHash = (LinkedHashMap)objectHash.clone();
	Iterator<MetadataCacheObject> iter = (Iterator<MetadataCacheObject>)(tmpObjectHash.values().iterator());

	while (iter.hasNext()) {
	    MetadataCacheObject object = iter.next();

	    Metadata metadata = object.getMetadataCacheObject();
	    String objectType = medialib.getTableName(metadata);

	    //if (objectType.equals("RadioProgramMetadata")) {
	    if (metadata instanceof RadioProgramMetadata) {
		String key = object.getKey();
		String[] objectIDs = key.split(objectType);
		String objectID = objectIDs[0].substring(0, objectIDs[0].length() - 1);
		logger.debug("refreshCache: Internal invocation of handleInvalidateCacheMessage: " + objectID);
		handleInvalidateCacheMessage(objectType, LibService.CACHE_UPDATE, new String[]{objectID});
	    }
	}
    }

    public synchronized void handleInvalidateCacheMessage(String objectType, String actionType, String[] objectIDs) {
	logger.debug("HandleInvalidateCacheMessage: invalidate cache -- " + objectType + ":" + actionType + ":" + (objectIDs.length > 0 ? objectIDs[0] : ""));

	for (CacheListenerCallback listener: listeners.values()){
	    if (actionType.equals(LibService.CACHE_INSERT)){
		listener.cacheObjectsInserted(objectType, objectIDs);
	    } else if (actionType.equals(LibService.CACHE_UPDATE)){
		listener.cacheObjectsUpdated(objectType, objectIDs);
	    } else if (actionType.equals(LibService.CACHE_DELETE)){
		listener.cacheObjectsDeleted(objectType, objectIDs);
	    }
	}


	for (String objectID: objectIDs) {
	    if (actionType.equals(LibService.CACHE_DELETE) || actionType.equals(LibService.CACHE_UPDATE) 
	       || actionType.equals(LibService.CACHE_INSERT)) {

		String key =  objectID + "." + objectType;
		if (hasMetadataCacheObject(key)) {
		    objectHash.remove(key);
		    logger.debug("********* removed metadata key " + key);
		}

		if (dependencyHash.get(key) != null) {
		    String dependentKey = dependencyHash.get(key);
		    //System.out.println("********* found dependent key " + dependentKey);
		    MetadataCacheObject dependentObj = objectHash.get(dependentKey);
		    if (dependentObj != null) {
			setAssocFieldsInitialized(dependentObj.getMetadataCacheObject(), false);
			clearDependencies(dependentKey);
		    }
		}
	    }
	    
	    if (actionType.equals(LibService.CACHE_INSERT)) {
		String key =  objectType + "." + objectID;
		if (listObjectHash.get(key) != null) {
		    listObjectHash.remove(key);
		    //System.out.println("********* removed list key " + key);
		}
	    }

	    if (actionType.equals(LibService.CACHE_UPDATE) || actionType.equals(LibService.CACHE_INSERT)) {

		if (isUpdatableType(objectType)) {
		    //System.out.println("Doing getsingle.");
		    Metadata query = getQueryMetadata(objectType, objectID);
		    if (query == null){
			logger.error("HandleInvalidateCacheMessage: Unsupported type:"+objectType + " Id: "+objectID);
			return;
		    }
		    logger.debug("handleInvalidateCacheMessage: medialib = " + medialib);
		    if (medialib != null && medialib.isConnected())
			getSingle(query);
		    else
			markDirty(query);
		}
	    }
	}

	// libservice: RadioProgramMetadata, insert, new archive
	// libservice: PlayoutHistory, insert, new stat
	// indexlib: AnchorId, insert, filter
	// medialib: tablename, insert/update/remove, unique()
    }

    static final String UPDATABLE_OBJECT_TYPES[] = new String[] {Metadata.getClassName(RadioProgramMetadata.class), 
								 Metadata.getClassName(Entity.class),
								 Metadata.getClassName(Category.class)
    };

    boolean isUpdatableType(String type){
	for (String var: UPDATABLE_OBJECT_TYPES){
	    if (type.equals(var))
		return true;
	}
	return false;
    }   
    
    Metadata getQueryMetadata(String objectType, String id){
	if (objectType.equals(Metadata.getClassName(RadioProgramMetadata.class))){
	    return new RadioProgramMetadata(id);
	} else if (objectType.equals(Metadata.getClassName(Entity.class))){
	    return new Entity(Integer.parseInt(id));
	} else if (objectType.equals(Metadata.getClassName(Category.class))){
	    return new Category(id);
	} else {
	    logger.error("GetQueryMetadata: Insupported objectType: "+objectType);
	    return null;
	}
    }

    public synchronized void handleInvalidateCacheKeyMessage(String objectType, String actionType, String[] oldObjectIDs, String[] newObjectIDs) {
	logger.info("HandleInvalidateCacheKeyMessage: " + objectType + ":" + actionType + ":" + oldObjectIDs.length);

	for (CacheListenerCallback listener: listeners.values()){
	    if (actionType.equals(LibService.CACHE_INSERT)){
		listener.cacheObjectsInserted(objectType, oldObjectIDs);
	    } else if (actionType.equals(LibService.CACHE_UPDATE)){
		listener.cacheObjectsUpdated(objectType, oldObjectIDs);
	    } else if (actionType.equals(LibService.CACHE_DELETE)){
		listener.cacheObjectsDeleted(objectType, oldObjectIDs);
	    }
	}

	Hashtable<CacheObjectListener, ArrayList<KeyChangeInfo>>  listenerMap = new Hashtable<CacheObjectListener, ArrayList<KeyChangeInfo>>();
	int i = 0;
	for (String objectID: oldObjectIDs) {
	    if (actionType.equals(LibService.CACHE_UPDATE) || actionType.equals(LibService.CACHE_DELETE)) {
		String key =  objectID + "." + objectType;
		String tmpKey;

		if (!newObjectIDs[i].equals(LibService.CACHE_DELETE))
		    tmpKey = newObjectIDs[i] + "." + objectType;
		else
		    tmpKey = "." + objectType;

		final String newKey = tmpKey;
		if (hasMetadataCacheObject(key)) {
		    final MetadataCacheObject cacheObject = objectHash.get(key);
		    objectHash.remove(key);

		    final ArrayList<CacheObjectListener> objectListener = objectListeners.get(key);
		    if (objectListener != null) {
			for (CacheObjectListener listener : objectListener) {
			    ArrayList<KeyChangeInfo> listenerObjects = listenerMap.get(listener);
			    if (listenerObjects == null) {
				listenerObjects = new ArrayList<KeyChangeInfo>();
				listenerMap.put(listener, listenerObjects);
			    }
			    KeyChangeInfo info = new KeyChangeInfo(cacheObject, newKey);
			    listenerObjects.add(info);
			}
		    }

		    objectListeners.remove(key);
		    if (!newObjectIDs[i].equals(LibService.CACHE_DELETE) && objectListener != null)
			objectListeners.put(newKey, objectListener);
		    
		    if (dependencyHash.get(key) != null) {
			String dependentKey = dependencyHash.get(key);
			dependencyHash.remove(key);
			if (!newObjectIDs[i].equals(LibService.CACHE_DELETE))
			    dependencyHash.put(newKey, dependentKey);

			if (invDependencyHash.get(dependentKey) != null) {
			    if (!newObjectIDs[i].equals(LibService.CACHE_DELETE))
				invDependencyHash.put(dependentKey, newKey);
			}
		    }

		    if (!newObjectIDs[i].equals(LibService.CACHE_DELETE))
			key = newKey;

		}

		//XXX: This might be incorrect and may require testing
		if (dependencyHash.get(key) != null) {
		    String dependentKey = dependencyHash.get(key);
		    //System.err.println("********* found dependent key " + dependentKey);
		    MetadataCacheObject dependentObj = objectHash.get(dependentKey);
		    if (dependentObj != null) {
			setAssocFieldsInitialized(dependentObj.getMetadataCacheObject(), false);
			clearDependencies(dependentKey);
		    }
		}
	    }

	    i++;
	}	

	for (CacheObjectListener listener : listenerMap.keySet()) {
	    final ArrayList<Metadata> objectList = new ArrayList<Metadata>();
	    final ArrayList<String> keyList = new ArrayList<String>();
	    ArrayList<KeyChangeInfo> infoList = listenerMap.get(listener);

	    for (KeyChangeInfo info : infoList) {
		objectList.add(info.getCacheObject().getMetadataCacheObject());
		keyList.add(info.getNewKey());
	    }

	    final CacheObjectListener objectListener = listener;
	    Runnable runnable = new Runnable(){
		    public void run(){
			objectListener.cacheObjectKeyChanged(objectList.toArray(new Metadata[]{}), keyList.toArray(new String[]{}));
		    }
		};
	    listenersActivator.execute(runnable);
	}
    }

    public synchronized Object[] uniqueFromDB(Metadata metadata, String fieldName) {
	return medialib.unique(metadata, fieldName);
    }

    public synchronized Object[] uniqueFromAnchors(Metadata metadata, String fieldName) {
	String key = genKey(metadata, fieldName);
	//System.out.println("******* uniqueFromAnchors cache lookup " + key);
	if (listObjectHash.get(key) != null) {
	    //System.out.println("******** cache hit " + key);
	    return listObjectHash.get(key).getListCacheObject();
	}
	
	Object[] retval =  medialib.unique(metadata, fieldName);
	if (retval != null) {
	    UniqueListCacheObject listCacheObject = new UniqueListCacheObject(retval, key);
	    listObjectHash.put(key, listCacheObject);
	}
	return retval;
    }

    @SuppressWarnings("unchecked") 
    public synchronized <T extends Metadata> T[] getAll(T metadata, String[] sortField, String[] order, String[] between, int limit){
	String key = genKey(metadata);
	//System.out.println("******* getAll cache lookup " + key);
	if (!key.startsWith(".") && hasMetadataCacheObject(key) && subFieldsInitialized(key)) {
	    //System.out.println("************ cache hit " + key);
	    return (T[])(new Metadata[]{getMetadataCacheObject(key)});
	}

	T[] retval = medialib.getAll(metadata, sortField, order, between, limit);
	if (retval != null) {
	    for (T retvalObj: retval) {
		ArrayList<Metadata> subFields = medialib.getSubFields(retvalObj);
		for (Metadata subField: subFields) {
		    key = genKey(subField);
		    if (!hasMetadataCacheObject(key))
			setMetadataCacheObject(subField, key);
		}
		key = genKey(retvalObj);
		if (!hasMetadataCacheObject(key))
		    setMetadataCacheObject(retvalObj, key);
	    }
	}
	return retval;
    }

    public synchronized Metadata[] get(Metadata metadata) {
	String key = genKey(metadata);

	if (!key.startsWith(".") && hasMetadataCacheObject(key)) {

	    return new Metadata[]{getMetadataCacheObject(key)};

	} else if (metadata instanceof Category && key.startsWith(".")) {
	    if (allCategories == null) {
		allCategories = medialib.get(metadata);
		updateCategoryListFromCache();
	    }

	    return allCategories;
	}

	Metadata[] retval = medialib.get(metadata);
	if (retval != null) {
	    for (Metadata retvalObj: retval) {
		key = genKey(retvalObj);
		if (!hasMetadataCacheObject(key)) {
		    setMetadataCacheObject(retvalObj, key);
		}
	    }
	}
	return retval;
    }
    
    public synchronized Metadata[] getAll(Metadata metadata) {
	String key = genKey(metadata);
	//System.out.println("******* getAll cache lookup " + key);
	if (!key.startsWith(".") && hasMetadataCacheObject(key) && subFieldsInitialized(key)) {
	    //System.out.println("************ cache hit " + key);
	    return new Metadata[]{getMetadataCacheObject(key)};
	}

	Metadata[] retval = medialib.getAll(metadata);
	if (retval != null) {
	    for (Metadata retvalObj: retval) {
		ArrayList<Metadata> subFields = medialib.getSubFields(retvalObj);
		for (Metadata subField: subFields) {
		    key = genKey(subField);
		    if (!hasMetadataCacheObject(key))
			setMetadataCacheObject(subField, key);
		}
		key = genKey(retvalObj);
		if (!hasMetadataCacheObject(key))
		    setMetadataCacheObject(retvalObj, key);
	    }
	}
	return retval;
    }

    protected void markDirty(Metadata metadata) {
	String key = genKey(metadata);
	if (!key.startsWith(".")) {
	    if (hasMetadataCacheObject(key)) {
		objectHash.get(key).markDirty();
	    }
	}
    }

    @SuppressWarnings("unchecked")
    public synchronized <T extends Metadata> T getSingle(T metadata) {
	Metadata retval = null;
	String key = genKey(metadata);

	if (!key.startsWith(".")) {
	    if (hasMetadataCacheObject(key)) {
		
		retval = getMetadataCacheObject(key);

	    } else {

		retval = medialib.getSingle(metadata);
		if (retval == null)
		    logger.error("GetSingle: Unable to lookup data from database for: "+metadata.dump());
		else
		    setMetadataCacheObject(retval, key);
	    }
	} else {
	    retval = medialib.getSingle(metadata);
	}
	return (T) retval;
    }

    protected void updateCategoryListFromCache() {
	if (allCategories != null) {
	    int i = 0;
	    for (Metadata category: allCategories) {
		String key = genKey(category);
		if (!key.startsWith("."))
		    if (hasMetadataCacheObject(key))
			allCategories[i] = getMetadataCacheObject(key);
		i++;
	    }
	}
    }

    protected void updateCategoryListFromCache(Category category) {
	if (allCategories != null) {
	    int i = 0;
	    for (Metadata metadata: allCategories) {
		if (((Category)metadata).getNodeID() == category.getNodeID()) {
		    allCategories[i] = category;
		    break;
		}
		i++;
	    }
	}
    }

    public void addCacheObjectListener(Metadata cacheObject, CacheObjectListener listener) {
	if (cacheObject == null){
	    logger.error("AddCacheObjectListener: Attempting to add listener to null cache object.");
	    return;
	}

	String key = genKey(cacheObject);

	ArrayList<CacheObjectListener> objectListener = objectListeners.get(key);
	if (objectListener == null)
	    objectListeners.put(key, objectListener = new ArrayList<CacheObjectListener>());
	if (!objectListener.contains(listener)) {
	    objectListener.add(listener);
	}
    }

    public void removeCacheObjectListener(Metadata cacheObject, CacheObjectListener listener) {
	String key = genKey(cacheObject);
	ArrayList<CacheObjectListener> objectListener = objectListeners.get(key);
	if (objectListener != null) {
	    objectListener.remove(listener);
	    //System.out.println("---- cacheProvider: removed listener: " + cacheObject);
	}
    }

    public void setMetadataCacheObject(Metadata cacheObject) {
	String key = genKey(cacheObject);
	//System.out.println("---- setMetadataCacheObject: " + cacheObject + ":" + key);
	setMetadataCacheObject(cacheObject, key);
    }

    protected void setMetadataCacheObject(final Metadata cacheObject, String key) {
	//logger.debug("SetMetadataCacheObject: "+cacheObject);

    	MetadataCacheObject obj = objectHash.get(key);

    	if (obj == null) {
    	    obj = new MetadataCacheObject(cacheObject, key);
    	    objectHash.put(key, obj);
	    //System.out.println("******* size of cache table = " + objectHash.size());
    	} else {
    	    obj.setMetadataCacheObject(cacheObject);
	    obj.markClean();
    	}

	if (cacheObject instanceof Category)
	    updateCategoryListFromCache((Category)cacheObject);

	final ArrayList<CacheObjectListener> objectListener = objectListeners.get(key);
	
	Runnable runnable = new Runnable() {
		@SuppressWarnings("unchecked")
		public void run(){
		    if (objectListener != null) {
			ArrayList<CacheObjectListener> listeners = (ArrayList<CacheObjectListener>) objectListener.clone();
			for (CacheObjectListener listener: listeners) {
			    listener.cacheObjectValueChanged(cacheObject);
			}
		    }
		}
	    };
	
	listenersActivator.execute(runnable);
    }

    protected boolean hasMetadataCacheObject(String key) {
	return (objectHash.get(key) != null);
    }

    protected Metadata getMetadataCacheObject(String key) {
	return objectHash.get(key).getMetadataCacheObject();
    }

    public synchronized boolean subFieldsInitialized(Metadata metadata) {
	String key = genKey(metadata);
	return subFieldsInitialized(key);
    }

    protected boolean subFieldsInitialized(String key) {
	MetadataCacheObject cacheObject = objectHash.get(key);
	if (cacheObject != null)
	    return cacheObject.subFieldsInitialized();
	else
	    return false;
    }

    public synchronized boolean assocFieldsInitialized(Metadata metadata) {
	String key = genKey(metadata);
	return assocFieldsInitialized(key);
    }

    protected boolean assocFieldsInitialized(String key) {
	MetadataCacheObject cacheObject = objectHash.get(key);
	if (cacheObject != null)
	    return cacheObject.assocFieldsInitialized();
	else
	    return false;
    }

    public void setAssocFieldsInitialized(Metadata metadata, boolean init) {
	String key = genKey(metadata);
	MetadataCacheObject cacheObject = objectHash.get(key);
	if (cacheObject != null)
	    cacheObject.setAssocFieldsInit(init);
    }

    public void setAssocDependencies(Metadata metadata, Metadata[] dependencies) {
	String key = genKey(metadata);
	StringBuilder depKeyBld = new StringBuilder();
	for (Metadata dependency: dependencies) {
	    String depKey = genKey(dependency);
	    depKeyBld.append(","); depKeyBld.append(depKey); 
	    dependencyHash.put(depKey, key);
	}
	if (depKeyBld.length() > 0)
	    invDependencyHash.put(key, depKeyBld.substring(1));
    }

    protected void clearDependencies(String dependentKey) {
	String oldDependencies = invDependencyHash.get(dependentKey);
	if (oldDependencies != null && !oldDependencies.equals("")) {
	    String[] oldDeps = StringUtilities.getArrayFromCSV(oldDependencies);
	    for (String oldDep: oldDeps)
		dependencyHash.remove(oldDep);
	    invDependencyHash.remove(dependentKey);
	}
    }

    protected String genKey(Metadata metadata) {
	return metadata.unique() + "." + MediaLib.getTableName(metadata);
    }

    protected String genKey(Metadata metadata, String fieldName) {
	return anchorNames.get(MediaLib.getTableName(metadata) + "." + fieldName);
    }

    public class UniqueListCacheObject {
	Object[] uniqueList;
	String key;

	public UniqueListCacheObject(Object[] uniqueList, String key) {
	    this.key = key;
	    this.uniqueList = uniqueList;
	}

	public Object[] getListCacheObject() {
	    return uniqueList;
	}

	public void setListCacheObject(Object[] uniqueList) {
	    this.uniqueList = uniqueList;
	}
    }

    public class MetadataCacheObject {
	Metadata cacheObject;
	String key;
	boolean subFieldsInit = false;
	boolean assocFieldsInit = false;
	boolean isDirty = false;

	public MetadataCacheObject(Metadata cacheObject, String key) {
	    this.key = key;
	    setMetadataCacheObject(cacheObject);
	}

	public Metadata getMetadataCacheObject() {
	    return cacheObject;
	}

	public void setMetadataCacheObject(Metadata cacheObject) {
	    this.cacheObject = cacheObject;
	    initSubFields(cacheObject);
	    initAssocFields(cacheObject);
	}

	public String getKey() {
	    return key;
	}

	public void setKey(String key) {
	    this.key = key;
	}

	public boolean subFieldsInitialized() {
	    return subFieldsInit;
	}

	public void setSubFieldsInit(boolean subFieldsInit) {
	    this.subFieldsInit = subFieldsInit;
	}

	public boolean assocFieldsInitialized() {
	    return assocFieldsInit;
	}

	public void setAssocFieldsInit(boolean assocFieldsInit) {
	    this.assocFieldsInit = assocFieldsInit;
	}

	public void initSubFields(Metadata cacheObject) {
	    ArrayList<Metadata> subFields = medialib.getSubFields(cacheObject);
	    if (subFields.size() > 0) {
		subFieldsInit = true;
	    } else {
		subFieldsInit = false;
	    }
	}

	public void initAssocFields(Metadata cacheObject) {
	    ArrayList<Metadata[]> assocFields = medialib.getAssocFields(cacheObject);
	    if (assocFields.size() > 0) {
		assocFieldsInit = true;
	    } else {
		assocFieldsInit = false;
	    }
	}

	public void markDirty() {
	    isDirty = true;
	}

	public void markClean() {
	    isDirty = false;
	}

	public boolean isDirty() {
	    return isDirty;
	}

    }

    class CallbackListenersTask implements Runnable {
	boolean activate;
	String[] error;

	public CallbackListenersTask(boolean activate, String[] error) {
	    this.activate = activate;
	    this.error = error;
	    if ((!activate) && (error == null))
		this.error = new String[]{};
	}

	public void run() {
	    for (CacheListenerCallback listener: listeners.values()) {
		listener.activateCache(activate, error);
	    }
	}
    }

    class KeyChangeInfo {
	MetadataCacheObject cacheObject;
	String newKey;

	public KeyChangeInfo(MetadataCacheObject object, String key) {
	    cacheObject = object;
	    newKey = key;
	}

	public MetadataCacheObject getCacheObject() {
	    return cacheObject;
	}

	public String getNewKey() {
	    return newKey;
	}
    }


}
