package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.libmsgs.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.*;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class ArchiverProvider extends RSProviderBase {
    public static String PORT_ROLE = ArchiverService.BCAST_MIC;
    
    protected final static int CONFIGURING = -1;
    protected final static int LISTENING = 0;
    protected final static int ARCHIVE_COMMAND_SENT = 1;
    protected final static int ARCHIVING = 2;
    protected final static int STOP_COMMAND_SENT = 3;

    protected final static String SUCCESS = "SUCCESS";
    protected final static String FAILURE = "FAILURE";

    protected int state = CONFIGURING;
    protected String fileID;
    protected String bcastServiceInstanceName;
    protected String archiverServiceID = "";

    protected Hashtable<String, ArchiverListenerCallback> listeners = null;
    protected String currentArchiverCallback = null;
    protected boolean prematureArchiving = false;
    protected boolean archiverDisconnected = true;
    protected boolean rscDisconnected = false;
    protected boolean rscActive = true;

    protected ExecutorService listenerExecutor;
    protected final static int POOL_THREAD_COUNT = 1;

    final static int MIN_LEVEL_THRESHOLD = -90;
    
    public ArchiverProvider(UIService uiService) {
	super (uiService, RSApp.ARCHIVER_PROVIDER); 
	bcastServiceInstanceName = uiService.getProviderServiceInstance(RSApp.ARCHIVER_PROVIDER, ArchiverService.BCAST_MIC);
	listeners = new Hashtable<String, ArchiverListenerCallback>();;

	listenerExecutor = Executors.newFixedThreadPool(POOL_THREAD_COUNT);
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    listenerExecutor.shutdown();
		}
	    });
	
    }
    
    
    public synchronized int activate() {
	logger.info("Activate: Provider Activated");
	setState(CONFIGURING);
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String [] {bcastServiceInstanceName});
	int retval = sendCommand(message);
	if (retval == -1)
	    rscActive = false;

	return retval;
	
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listeners.put(listener.getName(), (ArchiverListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	listeners.remove(listenerName);
    }

    public synchronized boolean isActive() {
	if (state() == CONFIGURING)
	    return false;
	else
	    return true;
    }

    public synchronized void receiveMessage(IPCMessage message){
	logger.debug("Received Message from: "+message.getSource()+" type: "+message.getMessageClass());
	if (message instanceof ArchiverAckMessage) {
	    ArchiverAckMessage ackMessage = (ArchiverAckMessage)message;
	    handleArchiverAckMessage(ackMessage.getAck(), ackMessage.getCommand(), ackMessage.getError());

	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    boolean inactivate = false;
	    for (String service: notifyMessage.getInactivatedServices()) {
		if (service.equals(bcastServiceInstanceName)) {
		    archiverDisconnected = true;
		    inactivate = true;
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController disconnected.");
		    rscDisconnected = true;
		    inactivate = true;
		}

		if (inactivate){
		    String[] error;
		    if (archiverDisconnected)
			error = new String[]{RSController.ERROR_SERVICE_DISCONNECTED, bcastServiceInstanceName};
		    else //rscDisconnected
			error = new String[]{RSController.ERROR_SERVICE_DISCONNECTED, RSController.RESOURCE_MANAGER};
		    /*
		    for (ArchiverListenerCallback listener: listeners.values()) {
			listener.enableArchiverUI(false, error);
			// VVV call the inference engine with the error
		    }
		    */
		    enableArchiverUI(false, error);
		}
	    }
	    
	    for (String service: notifyMessage.getActivatedServices()){
		if (service.equals(bcastServiceInstanceName)) {
		    if (!archiverDisconnected){
			setState(LISTENING);
			//for (ArchiverListenerCallback listener: listeners.values())
			//    listener.activateArchiver(true, new String[]{});
			activateArchiver(true, new String[]{});
		    } else {
			archiverDisconnected = false;
			/*
			for (ArchiverListenerCallback listener: listeners.values())
			    listener.enableArchiverUI(true, new String[]{});
			*/
			enableArchiverUI(true, new String[]{});
		    }
		} else if (service.equals(RSController.RESOURCE_MANAGER)){
		     logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			rscDisconnected = false;
		    }
		}
	    }

	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();
	    
	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(bcastServiceInstanceName))
		    archiverServiceConnected(activatedIDs[i]);
	    }
	    
	} else if (message instanceof ServiceAckMessage){
	    ServiceAckMessage serviceAckMessage = (ServiceAckMessage) message;
	    for (String service: serviceAckMessage.getActiveServices()){
		if (service.equals(bcastServiceInstanceName)) {
		    archiverDisconnected = false;
		    rscDisconnected = false; //probably not needed but does not hurt to do this.
		    setState(LISTENING);
		    /*
		    for (ArchiverListenerCallback listener: listeners.values())
			listener.activateArchiver(true, new String[]{});
		    */
		    activateArchiver(true, new String[]{});
		}
	    }
	    
	    for (String service: serviceAckMessage.getInactiveServices()){
		if (service.equals(bcastServiceInstanceName)) {
		    archiverDisconnected = false;
		    rscDisconnected = false; //probably not needed but does not hurt to do this.
		    setState(CONFIGURING);
		    /*
		    for (ArchiverListenerCallback listener: listeners.values())
			listener.activateArchiver(false, new String[]{RSController.ERROR_SERVICE_DOWN,
								      bcastServiceInstanceName});
		    */
		    activateArchiver(false, new String[]{RSController.ERROR_SERVICE_DOWN,
							 bcastServiceInstanceName});
		}
	    }
	} else if (message instanceof GstreamerErrorMessage){
	    GstreamerErrorMessage errorMessage = (GstreamerErrorMessage)message;
	    handleGstreamerErrorMessage(errorMessage.getError());
	} else if (message instanceof ArchivingDoneResponseMessage) {

	    ArchivingDoneResponseMessage archivingMessage = (ArchivingDoneResponseMessage)message;
	    handleArchivingDoneResponseMessage(archivingMessage.getArchivedFilename(), archivingMessage.getArchivedLibFilename(),
					       archivingMessage.getArchivedFileStatus(), archivingMessage.getStartTime(), 
					       archivingMessage.getError());

	} else if (message instanceof ArchiverLevelsMessage){
	    ArchiverLevelsMessage levelsMessage = (ArchiverLevelsMessage) message;
	    handleArchiverLevelsMessage(levelsMessage.getAck(), levelsMessage.getLevels());
	}
    }

    protected void archiverServiceConnected(String newID){
	logger.debug("ArchiverServiceConnected with ID: "+newID);
	
	if (!archiverServiceID.equals("") && !archiverServiceID.equals(newID)){
	    /*
	    for (ArchiverListenerCallback listener: listeners.values())
		listener.activateArchiver(false, new String[]{RSController.ERROR_SERVICE_DOWN, bcastServiceInstanceName});
	    
	    for (ArchiverListenerCallback listener: listeners.values())
		listener.activateArchiver(true,  new String[]{});
	    */
	    activateArchiver(false, new String[]{RSController.ERROR_SERVICE_DOWN, bcastServiceInstanceName});
	    activateArchiver(true,  new String[]{});
	}
	
	archiverServiceID = newID;
    }

    /*Using executor to handle callbacks. This will avoid deadlocks */

    protected void activateArchiver(final boolean activate, final String[] error){
	listenerExecutor.execute( new Runnable(){
		public void run(){
		    for (ArchiverListenerCallback listener: listeners.values())
			listener.activateArchiver(activate, error);

		}
	    });
    }

    protected void enableArchiverUI(final boolean enable, final String[] error){
	listenerExecutor.execute( new Runnable(){
		public void run(){
		    for (ArchiverListenerCallback listener: listeners.values())
			listener.enableArchiverUI(enable, error);
		}
	    });
    }

    protected void archiveStarted(final ArchiverListenerCallback listener, final boolean activate, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    if (listener != null)
			listener.archiveStarted(activate, error);
		}
	    });
    }

    protected void archiveStopped(final ArchiverListenerCallback listener, final boolean activate, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    if (listener != null)
			listener.archiveStopped(activate, error);
		}
	    });
    }

    protected void archiverGstError(final ArchiverListenerCallback listener, final String error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    if (listener != null)
			listener.archiverGstError(error);
		}
	    });
    }

    /* End of call backs through executors */

    protected void handleGstreamerErrorMessage(String error){
	logger.error("HandleGstreamerErrorMessage: Received message: "+error+" from archiver service.");
	ArchiverListenerCallback listener;
	if (currentArchiverCallback != null && (listener = listeners.get(currentArchiverCallback)) != null) {
	    archiverGstError(listener, error);
	    //VVV inform of this error to inference engine also
	}
	setState(LISTENING);
    }

    protected void handleArchiverAckMessage (String ack, String command, String[] error){
	logger.info("HandleArchiverAck: " + ack + ":" + state + ":" + command);
	switch (state()){

	case ARCHIVE_COMMAND_SENT:
	    if (prematureArchiving && command.equals(ArchiverService.STOP_MSG)) {
		if (ack.equals(ArchiverService.SUCCESS)) {
		    ArchiverListenerCallback listener;
		    if (currentArchiverCallback != null && (listener = listeners.get(currentArchiverCallback)) != null)
			archiveStopped(listener, true, error);
		} else {
		    ArchiverListenerCallback listener;
		    if (currentArchiverCallback != null && (listener = listeners.get(currentArchiverCallback)) != null)
			archiveStopped(listener, false, error);
		}
		prematureArchiving = false;

	    } else {
		if (ack.equals(ArchiverService.SUCCESS)){
		    logger.info("HandleArchiverAckMessage: Started archiving.");
		    setState(ARCHIVING);
		    ArchiverListenerCallback listener;
		    if (currentArchiverCallback != null && (listener = listeners.get(currentArchiverCallback)) != null)
			archiveStarted(listener, true, new String[]{});
		} else {
		    logger.error("HandleArchiverAckMessage: Unable to archive.");
		    displayError("Unable to archive.");
		    setState(LISTENING);
		    ArchiverListenerCallback listener;
		    if (currentArchiverCallback != null && (listener = listeners.get(currentArchiverCallback)) != null)
			archiveStarted(listener, false, error);
		}
	    }
	    break;

	case STOP_COMMAND_SENT:
	    if (ack.equals(ArchiverService.SUCCESS)){
		logger.info("HandleArchiverAckMessage: Stopped archiving.");
		setState(LISTENING);
		ArchiverListenerCallback listener;
		if (currentArchiverCallback != null && (listener = listeners.get(currentArchiverCallback)) != null)
		    archiveStopped(listener, true, error);
	    } else {
		logger.error("HandleArchiverAckMessage: Unable to stop archiving.");
		displayError("Unable to stop archving.");
		ArchiverListenerCallback listener;
		if (currentArchiverCallback != null && (listener = listeners.get(currentArchiverCallback)) != null)
		    archiveStopped(listener, false, error); 
		//Not changing the line above as right now Archiver service never sends nack
		//hence there will never be an error message for it.
	    }
	}
    }

    protected void handleArchivingDoneResponseMessage(String archivedFilename, final String archivedLibFilename,final String archivedFileStatus, final long startTime,final String error) {	
	final ArchiverListenerCallback listener;
	if (currentArchiverCallback == null || listeners.get(currentArchiverCallback) == null)
	    return;
	listener = listeners.get(currentArchiverCallback);

	listenerExecutor.execute(new Runnable(){
		public void run(){
		    if (archivedFileStatus.equals(ArchiverService.SUCCESS)) {
			listener.archivingDone(archivedLibFilename, true, startTime, "");
		    } else {
			listener.archivingDone(archivedLibFilename, false, startTime, error);
		    }
		}
	    });
    }

    protected void handleArchiverLevelsMessage(String ack, String[] levelStrings){
	if (currentArchiverCallback == null || listeners.get(currentArchiverCallback) == null)
	    return;
	final ArchiverListenerCallback listener = listeners.get(currentArchiverCallback);
	final int level = getAverageLevels(levelStrings);

	final boolean success;
	if (level < MIN_LEVEL_THRESHOLD)
	    success = false;
	else
	    success = true;
	
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    listener.archiverLevelTested(success, level);
		}
	    });
    }

    protected int getAverageLevels(String[] levelStrings){
	if (levelStrings.length < 1){
	    logger.error("GetAverageLevels: No level values present.");
	    return MIN_LEVEL_THRESHOLD - 1;
	}

	int sum = 0;
	for (String level: levelStrings)
	    sum += Integer.parseInt(level);

	sum /= levelStrings.length;

	return sum;
    }

    protected void displayError (String error){
	logger.error(error);
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: from: "+message.getSource()+" Error: " + message.getParam("serviceerror"));
	
	
	String serviceName = null;
	String errorCode = null;

	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}

	//ErrorCode ignored here.
	if (serviceName != null && serviceName.equals(bcastServiceInstanceName)) { 
	    setState(CONFIGURING);
	    /*
	    for (ArchiverListenerCallback listener: listeners.values()) {
		listener.activateArchiver(false, new String[] {RSController.ERROR_SERVICE_DOWN, bcastServiceInstanceName});
		// VVV send message to inference engine
	    }
	    */
	    activateArchiver(false, new String[] {RSController.ERROR_SERVICE_DOWN, bcastServiceInstanceName});
	    archiverDisconnected = false;
	} else if (serviceName.equals(RSController.RESOURCE_MANAGER)){
	    setState(CONFIGURING);
	    /*
	    for (ArchiverListenerCallback listener: listeners.values()) {
		listener.activateArchiver(false, new String[] {RSController.ERROR_SERVICE_DOWN,RSController.RESOURCE_MANAGER});
		//VVV send message to inference engine
	    }
	    */
	    activateArchiver(false, new String[] {RSController.ERROR_SERVICE_DOWN,RSController.RESOURCE_MANAGER});
	    archiverDisconnected = false;
	    rscActive = false;
	}else {
	    logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
	    for (ArchiverListenerCallback listener: listeners.values()) {
		// VVV send message to inference engine
	    }
	}

    }

    public synchronized int startArchive(int archivingType, String archiverListenerName, boolean addToDatabase) {
	if (state() != LISTENING && state() != STOP_COMMAND_SENT){
	    logger.warn("StartArchive: Cannot start since not in LISTENING state");
	    return -1;
	}

	ArchiverCommandMessage message = new ArchiverCommandMessage("", bcastServiceInstanceName, getName(), 
								    ArchiverService.START_MSG, PORT_ROLE,
								    LibService.UNKNOWN, ArchiverService.ARCHIVING_TYPE[archivingType],
								    addToDatabase);

	if (sendCommand(message) == 0) {
	    if (state() == STOP_COMMAND_SENT)
		prematureArchiving = true;
	    setState(ARCHIVE_COMMAND_SENT);
	    currentArchiverCallback = archiverListenerName;
	} else {
	    setState(LISTENING);
	    logger.error("startArchive: Unable to send startArchive message");
	    return -1;
	}

	return 0;
    }

        public synchronized int startArchiverDiagnostics(int archivingType, String archiverListenerName, boolean addToDatabase) {
	if (state() != LISTENING && state() != STOP_COMMAND_SENT){
	    logger.warn("StartArchive: Cannot start since not in LISTENING state");
	    return -1;
	}

	ArchiverCommandMessage message = new ArchiverCommandMessage("", bcastServiceInstanceName, getName(), 
								    ArchiverService.START_DIAGNOSTICS_MSG, PORT_ROLE,
								    LibService.UNKNOWN, ArchiverService.ARCHIVING_TYPE[archivingType],
								    addToDatabase);

	if (sendCommand(message) == 0) {
	    if (state() == STOP_COMMAND_SENT)
		prematureArchiving = true;
	    setState(ARCHIVE_COMMAND_SENT);
	    currentArchiverCallback = archiverListenerName;
	} else {
	    setState(LISTENING);
	    logger.error("startArchive: Unable to send startArchive message");
	    return -1;
	}

	return 0;
    }

    
    public synchronized int stopArchive(int archivingType, boolean addToDatabase) {
	if (state() != ARCHIVING){
	    logger.warn("StopArchive: Cannot stop since not in ARCHIVING state");
	    return -1;
	}

	ArchiverCommandMessage message = new ArchiverCommandMessage("", bcastServiceInstanceName, getName(), 
								    ArchiverService.STOP_MSG, PORT_ROLE,
								    uiService.getLanguage(), ArchiverService.ARCHIVING_TYPE[archivingType],
								    addToDatabase);

	if (sendCommand(message) == 0){
	    setState(STOP_COMMAND_SENT);
	} else {
	    setState(LISTENING);
	    logger.error("stopArchive: Unable to send stopArchive message");
	    return -1;
	}

	return 0;
    }

    public synchronized int forceStop(boolean addToDatabase) {
	
	ArchiverCommandMessage message = new ArchiverCommandMessage("", bcastServiceInstanceName, getName(), 
								    ArchiverService.FORCE_STOP_MSG, PORT_ROLE,
								    uiService.getLanguage(), "", addToDatabase);
	if (sendCommand(message) == 0){
	    setState(STOP_COMMAND_SENT);

	    if (currentArchiverCallback != null && listeners.get(currentArchiverCallback) != null) {
		archiveStopped(listeners.get(currentArchiverCallback), true, new String[]{});
		// XXX should also call archivingDone callback, but do not have libFilename right now. Hence data will be recovered after cleanup
	    }

	} else {
	    setState(LISTENING);
	    logger.error("stopArchive: Unable to send stopArchive message");
	    return -1;
	}

	return 0;
    }    

    public boolean isArchivable() {
	return (state() == LISTENING || state() == STOP_COMMAND_SENT);
    }

    public boolean isStoppable(String archiverListenerName) {
	return (state() == ARCHIVING && archiverListenerName.equals(currentArchiverCallback));
    }

    protected int state() {
	return state;
    }

    protected void setState(int state) {
	int previousState = this.state;
	this.state = state;
	logger.info("Previous state: "+previousState+" Current State: "+state);

	switch(state) {
	case CONFIGURING:
	    // disable UI
	    break;
	case LISTENING:
	    // enable UI
	    break;
	case ARCHIVING:
	    // make changes in UI
	    break;
	}
    }
}
