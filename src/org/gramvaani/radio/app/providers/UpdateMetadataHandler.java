package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.*;
import org.gramvaani.radio.rscontroller.services.LibService;
import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.medialib.*;

import java.util.*;

import static org.gramvaani.utilities.StringUtilities.sanitize;

public class UpdateMetadataHandler implements MediaLibListener{

    // make required metadata objects and issues calls to medialib
    // this should also be called from ArchiverService and UploadService, or the same actions should be taken from their own routines

    protected StationConfiguration stationConfiguration;
    protected TimeKeeper timeKeeper;
    protected LogUtilities logger;
    protected IndexLib indexlib = null;
    protected MediaLib medialib = null;
    protected boolean isMediaLibActive = false;
    protected MetadataProvider provider = null;
    protected CacheProvider cacheProvider;

    protected boolean updateHandler;

    public UpdateMetadataHandler(StationConfiguration stationConfiguration, TimeKeeper timeKeeper, MetadataProvider provider) {
	this.stationConfiguration = stationConfiguration;
	this.timeKeeper = timeKeeper;
	this.logger = new LogUtilities("UpdateMetadata");
	this.provider = provider;
	updateHandler = false;
	
	/*
	MediaLib.registerListener("UpdateMetadata", this);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null) {
	    isMediaLibActive = true;
	    if (provider == null) {
		indexlib = IndexLib.getIndexLib(stationConfiguration, logger, medialib);
		if (indexlib != null) {
		    // XXX indexlib.setIPCServer(get IPC server from uiService)
		    updateHandler = true;
		}
	    } else {
		updateHandler = true;
	    }
	}
	*/
    }

    public void initialize() {
	logger.debug("Initialize: Initializing medialib and indexlib.");
	MediaLib.registerListener("UpdateMetadata", this);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null) {
	    isMediaLibActive = true;
	    if (provider == null) {
		indexlib = IndexLib.getIndexLib(stationConfiguration, logger, medialib);
		if (indexlib != null) {
		    // XXX indexlib.setIPCServer(get IPC server from uiService)
		    updateHandler = true;
		}
	    } else {
		updateHandler = true;
	    }
	}
	logger.debug("Initialize: UpdateHandler set to " + updateHandler);
    }

    public void activate(){
	updateHandler = false;
	String error = "";
	if (medialib == null)
	    medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null) {
	    isMediaLibActive = true;
	    if (provider == null) {
		indexlib = IndexLib.getIndexLib(stationConfiguration, logger, medialib);
		if (indexlib != null) {
		    // XXX indexlib.setIPCServer(get IPC server from uiService)
		    updateHandler = true;
		}
	    } else {
		updateHandler = true;
	    }
	} else {
	    error = RSController.ERROR_DB_FAILED;
	}
	
	if (provider != null)
	    provider.activateUpdateHandler(isActive(), error);
    }

    public boolean isActive() {
	return updateHandler && isMediaLibActive;
    }

    public synchronized void activateMediaLib(boolean activate){
	isMediaLibActive = activate;
	if (activate){
	    //medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	}
	provider.activateUpdateHandler(isActive(), RSController.ERROR_DB_FAILED);
    }

    public void setCacheProvider(CacheProvider cacheProvider) {
	this.cacheProvider = cacheProvider;
    }

    public RadioProgramMetadata[] getProgramMetadata(String[] programIDs) {
	if (cacheProvider == null) {
	    logger.error("GetProgramMetadata: CacheProvider is null");
	    return new RadioProgramMetadata[]{};
	}

	ArrayList<RadioProgramMetadata> programsArr = new ArrayList<RadioProgramMetadata>();
	RadioProgramMetadata queryObject = RadioProgramMetadata.getDummyObject("");
	for (String programID: programIDs) {
	    queryObject.setItemID(programID);

	    RadioProgramMetadata programMetadata = cacheProvider.getSingle(queryObject);
	    if (programMetadata != null) {
		programsArr.add(programMetadata);
		if (!cacheProvider.assocFieldsInitialized(programMetadata)) {
		    programMetadata.populate__history(medialib);
		    programMetadata.populate__creators(medialib);
		    // XXX	    programMetadata.populate__relatedContent(medialib);
		    programMetadata.populate__categories(medialib);
		    programMetadata.populate__tags(medialib);

		    cacheProvider.setAssocDependencies(programMetadata, programMetadata.getHistory());
		    cacheProvider.setAssocDependencies(programMetadata, programMetadata.getCreators());
		    cacheProvider.setAssocDependencies(programMetadata, programMetadata.getCategories());
		    cacheProvider.setAssocDependencies(programMetadata, programMetadata.getTags());

		    cacheProvider.setAssocFieldsInitialized(programMetadata, true);
		}
	    }
	}

	return programsArr.toArray(new RadioProgramMetadata[]{});
    }

    public String[] getLanguages() {
	return LanguageFilter.getLanguages(provider);
    }

    public String[] getAffiliations() {
	return CreatorFilter.getAffiliations(provider);
    }

    public String[] getRoles() {
	return CreatorFilter.getRoles(provider);
    }

    public String[] getNames() {
	return CreatorFilter.getNames(provider);
    }

    public String[] getLocations() {
	return CreatorFilter.getLocations(provider);
    }

    public Category[] getCategories() {
	if (cacheProvider == null) {
	    logger.error("GetCategories: CacheProvider is null");
	    return new Category[]{};
	}

	return MetaSearchFilter.getAllCategories(medialib, cacheProvider);
    }

    public Playlist[] getPlaylists() {
	Playlist metadata = new Playlist(-1);
	Metadata[] playlists = null;
	
	if (medialib == null) 
	    medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);

	if (medialib != null) 
	    playlists = medialib.get(metadata);

	if (playlists != null) {
	    Playlist[] arr = new Playlist[playlists.length];
	    int i = 0;
	    for (Metadata playlist: playlists) {
		arr[i++] = (Playlist)playlist;
	    }
	    return arr;
	}
	return new Playlist[]{};
    }

    public Entity[] getPotentialContacts(String numberType, String callerID, boolean doSuffixMatch) {
	if (callerID == null || callerID.equals(""))
	    return new Entity[]{};
	
	numberType = sanitize(numberType);
	callerID = sanitize(callerID);

	Entity dummyCaller = new Entity();
	if (doSuffixMatch) {
	    String whereClause = dummyCaller.getPhoneFieldName() + " like '%" + callerID + "' and " +
		dummyCaller.getPhoneTypeFieldName() + " = '" + numberType + "'";
	    return medialib.selectAllFields(dummyCaller, whereClause, -1);
	} else {
	    dummyCaller.setPhone(callerID);
	    dummyCaller.setPhoneType(numberType);
	    return medialib.get(dummyCaller);
	}
    }

    public int addPlaylist(Playlist playlist) {
	if (medialib.insert(playlist) != -1) {
	    playlist = (Playlist)(medialib.getSingle(playlist));
	    return playlist.getPlaylistID();
	}
	return -1;
    }

    public int addPlaylistItem(PlaylistItem playlistItem) {
	return medialib.insert(playlistItem);
    }

    public int updatePlaylist(Playlist oldPlaylist, Playlist newPlaylist) {
	return medialib.update(oldPlaylist, newPlaylist);
    }

    public int removePlaylistItems(PlaylistItem playlistItem) {
	return medialib.remove(playlistItem);
    }

    public PlaylistItem[] getPlaylistItems(Playlist playlist) {
	if (cacheProvider == null) {
	    logger.error("GetPlaylistItems: CacheProvider is null");
	    return new PlaylistItem[]{};
	}

	return playlist.populate__playlistItems(medialib, cacheProvider);
    }

    public void reloadPlaylistItems(Playlist playlist) {
	if (cacheProvider == null) {
	    logger.error("ReloadPlaylistItems: CacheProvider is null");
	    return;
	}

	playlist.clear__playlistItems();
	playlist.populate__playlistItems(medialib, cacheProvider);
    }

    public void populateChildren(Category categoryNode) {
	if (cacheProvider == null) {
	    logger.error("PopulateChildren: CacheProvider is null");
	    return;
	}
	MetaSearchFilter.populateChildren(categoryNode, medialib, cacheProvider);
    }

    public int addCategoryAnchorIndex(String anchorID, String filterID, String indexStr) {
	if (provider != null)
	    return sendAddAnchorTerm(anchorID, filterID, indexStr);
	else
	    return indexlib.addAnchorTerm(anchorID, filterID, indexStr);
    }
    
    public int addToLanguageAnchorIndex(String language) {
	if (provider != null)
	    return sendAddAnchorTerm(LanguageFilter.LANGUAGE_ANCHOR, SearchFilter.LANGUAGE_FILTER, language);
	else
	    return indexlib.addAnchorTerm(LanguageFilter.LANGUAGE_ANCHOR, SearchFilter.LANGUAGE_FILTER, language);
    }

    public int addToAffiliationAnchorIndex(String affiliation) {
	if (provider != null)
	    return sendAddAnchorTerm(CreatorFilter.AFFILIATION_ANCHOR, SearchFilter.CREATOR_FILTER, affiliation);
	else
	    return indexlib.addAnchorTerm(CreatorFilter.AFFILIATION_ANCHOR, SearchFilter.CREATOR_FILTER, affiliation);
    }

    public int addToRoleAnchorIndex(String role) {
	if (provider != null)
	    return sendAddAnchorTerm(CreatorFilter.ROLE_ANCHOR, SearchFilter.CREATOR_FILTER, role);
	else
	    return indexlib.addAnchorTerm(CreatorFilter.ROLE_ANCHOR, SearchFilter.CREATOR_FILTER, role);
    }

    public int addToLocationAnchorIndex(String location) {
	if (provider != null)
	    return sendAddAnchorTerm(CreatorFilter.LOCATION_ANCHOR, SearchFilter.CREATOR_FILTER, location);
	else
	    return indexlib.addAnchorTerm(CreatorFilter.LOCATION_ANCHOR, SearchFilter.CREATOR_FILTER, location);
    }

    public int addToNameAnchorIndex(String name) {
	if (provider != null)
	    return sendAddAnchorTerm(CreatorFilter.NAME_ANCHOR, SearchFilter.CREATOR_FILTER, name);
	else
	    return indexlib.addAnchorTerm(CreatorFilter.NAME_ANCHOR, SearchFilter.CREATOR_FILTER, name);
    }


    /*
    public int addToTagAnchorIndex(String tagsCSV) {
	if (provider != null)
	    return sendAddAnchorTerm(TextFilter.TAGS_ANCHOR, SearchFilter.TEXT_FILTER, tagsCSV);
	else
	    return indexlib.addAnchorTerm(TextFilter.TAGS_ANCHOR, SearchFilter.TEXT_FILTER, tagsCSV);
    }
    */

    
    /*
    public int addTagToProgramMetadata(String programID, String tagsCSV, int startTime, int endTime) {
	Tags tags = new Tags(programID, startTime, endTime, tagsCSV);
	cacheProvider.setAssocDependencies(new RadioProgramMetadata(programID), new Tags[]{tags});
	return medialib.insert(tags);
    }
    
    public int editTagsInProgramMetadata(String programID, String tagsCSV, int startTime, int endTime) {
	Tags tags = new Tags(programID, startTime, endTime, "");
	tags = cacheProvider.getSingle(tags);
	logger.debug("editTagsInProgramMetadata: " + tags + ":" + programID);
	if (tags != null) {
	    //	    String tagsStr = tags.getTagsCSV();
	    //	    if (!tagsStr.equals(""))
	    //		tagsStr = tagsStr + ",";
	    //	    tagsStr = tagsStr + tagsCSV;
	    logger.debug("editTagsInProgramMetadata: " + programID + ":" + tagsCSV + ":" + tags.getTagsCSV());
	    if (tagsCSV.equals("")) {
		return medialib.remove(tags);
	    } else {
		if (!tags.getTagsCSV().equals(tagsCSV)) {
		    Tags newTags = new Tags(programID, startTime, endTime, tagsCSV);
		    return medialib.update(tags, newTags);
		} else
		    return 0;
	    }
	} else {
	    if (tagsCSV.equals(""))
		return 0;
	    else
		return addTagToProgramMetadata(programID, tagsCSV, startTime, endTime);
	}
    }
    */

    //public int addNewTags(
    //XXX: This can be optimized.
    void addNewTags(Iterable<String> tagStrings){
	Tags[] allTags = getAllTags();
	
	HashSet<String> tagSet = new HashSet<String>();
	for (Tags tag: allTags)
	    tagSet.add(tag.getTagsCSV());

	StringBuilder str = new StringBuilder();

	for (String tagString: tagStrings)
	    if (!tagSet.contains(tagString)){
		str.append(",");
		str.append(tagString);
	    }
	if (str.length() == 0)
	    return;
	str.deleteCharAt(0);
	//addToTagAnchorIndex(str.toString());
    }
    
    void removeTagsFromProgram(String programID, Iterable<String> tags){
	for (String tagString: tags){
	    Tags tag = new Tags(programID);
	    tag.setTagsCSV(tagString);
	    medialib.remove(tag);
	}
    }

    void addTagsToProgram(String programID, Iterable<String> tags){
	if (cacheProvider == null) {
	    logger.error("AddTagsToProgram: CacheProvider is null");
	    return;
	}

	for (String tagString: tags){
	    Tags tag = new Tags(programID, 0, -1, tagString);
	    cacheProvider.setAssocDependencies(new RadioProgramMetadata(programID), new Tags[]{tag});
	    medialib.insert(tag);
	}
    }

    @SuppressWarnings("unchecked")
    public int setProgramTags(String programID, String tagsCSV){
	if (cacheProvider == null) {
	    logger.error("SetProgramTags: CacheProvider is null");
	    return -1;
	}

	String tagsArray[] = tagsCSV.split(",");
	
	HashSet<String> tagStrings = new HashSet<String>();

	for (String str: tagsArray){
	    if (str.trim().length() > 0)
		tagStrings.add(sanitize(str.trim()).toLowerCase());
	}

	addNewTags(tagStrings);
	
	Metadata tags[] = cacheProvider.get(new Tags(programID));
	HashSet<String> currentTags = new HashSet<String>();
	for (Metadata tag: tags)
	    currentTags.add(((Tags)tag).getTagsCSV());
	
	HashSet<String> common = (HashSet<String>) tagStrings.clone();
	common.retainAll(currentTags);

	currentTags.removeAll(common);
	tagStrings.removeAll(common);

	removeTagsFromProgram(programID, currentTags);
	addTagsToProgram(programID, tagStrings);

	return 0;
    }
    

    public Tags[] getAllTags() {
	if (cacheProvider == null) {
	    logger.error("GetAllTags: CacheProvider is null");
	    return new Tags[]{};
	}

	Tags tags = new Tags();
	Metadata[] tagsMetadata = cacheProvider.get(tags);
	Tags[] retTags = new Tags[tagsMetadata.length];
	int i = 0;
	for (Metadata tagMetadata: tagsMetadata)
	    retTags[i++] = (Tags)tagMetadata;
	return retTags;
    }

    public int addCategoryToProgramMetadata(String programID, int categoryNodeID) {
	if (cacheProvider == null) {
	    logger.error("AddCategoryToProgramMetadata: CacheProvider is null");
	    return -1;
	}
	ItemCategory itemCategory = new ItemCategory(programID, categoryNodeID);
	cacheProvider.setAssocDependencies(new RadioProgramMetadata(programID), new ItemCategory[]{itemCategory});
	return medialib.insert(itemCategory);
    }

    public int removeCategoryFromProgramMetadata(String programID, int categoryNodeID) {
	ItemCategory itemCategory = new ItemCategory(programID, categoryNodeID);
	return medialib.remove(itemCategory);
    }

    public int editCategoryInProgramMetadata(String programID, int oldCategoryNodeID, int newCategoryNodeID) {
	ItemCategory oldCategory = new ItemCategory(programID, oldCategoryNodeID);
	ItemCategory newCategory = new ItemCategory(programID, newCategoryNodeID);
	return medialib.update(oldCategory, newCategory);
    }

    public int editCreatorInProgramMetadata(String programID, int entityID, String affiliation) {
	if (cacheProvider == null) {
	    logger.error("EditCreatorInProgramMetadata: CacheProvider is null");
	    return -1;
	}

	Creator creator = new Creator(programID, entityID, "");
	creator = cacheProvider.getSingle(creator);
	if (creator != null) {
	    Creator newCreator = new Creator(programID, entityID, affiliation);
	    return medialib.update(creator, newCreator);
	} else
	    return -1;
    }

    public int editCreatorInProgramMetadata(String programID, int oldEntityID, int entityID, String affiliation) {
	if (cacheProvider == null) {
	    logger.error("EditCreatorInProgramMetadata: CacheProvider is null");
	    return -1;
	}

	Creator creator = new Creator(programID, oldEntityID, "");
	creator = cacheProvider.getSingle(creator);
	if (creator != null) {
	    Creator newCreator = new Creator(programID, entityID, affiliation);
	    return medialib.update(creator, newCreator);
	} else
	    return -1;
    }
    
    public int addCreatorInProgramMetadata(String programID, int entityID, String affiliation) {
	if (cacheProvider == null) {
	    logger.error("AddCreatorInProgramMetadata: CacheProvider is null");
	    return -1;
	}

	if (entityID < 0)
	    return -1;
	Creator creator = new Creator(programID, entityID, affiliation);
	cacheProvider.setAssocDependencies(new RadioProgramMetadata(programID), new Creator[]{creator});
	return medialib.insert(creator);
    }

    public int removeCreatorInProgramMetadata(String programID, int entityID) {
	Creator creator = new Creator(programID);
	creator.setEntityID(entityID);
	return medialib.remove(creator);
    }

    public int addPollMetadata(String name, String keyword, int type, int state) {
	Poll poll = new Poll(name, keyword, type, state);
	medialib.insert(poll, false);
	poll = (Poll)(medialib.getSingle(poll));

	if(poll != null)
	    return poll.getID();
	else
	    return -1;
    }
    
    public int editPollMetadata(int pollID, String name, String keyword, int type, int state) {
	Poll poll = Poll.getDummyObject(pollID);
	Poll newPoll = new Poll(name, keyword, type, state);
	medialib.update(poll, newPoll, false);

	newPoll = (Poll)(medialib.getSingle(poll));
	if(newPoll != null)
	    return newPoll.getID();
	else
	    return -1;
    }

    public int addPollOptionMetadata(int pollID, String optionName, String optionCode) {
	PollOption option = new PollOption(pollID, optionName, optionCode);
	medialib.insert(option, false);
	option = (PollOption)(medialib.getSingle(option));
	
	if(option != null)
	    return option.getID();
	else
	    return -1;
    }

    public int removePollOptionMetadata(int pollOptionID) {
	PollOption option = PollOption.getDummyObject(pollOptionID);
	option = (PollOption)(medialib.getSingle(option));

	Vote dummyVote = new Vote(option.getPollID(), pollOptionID);
	Vote[] votes = (Vote[])medialib.get(dummyVote);
	for(Vote vote :votes)
	    medialib.remove(vote, false);
	return medialib.remove(option, false);
    }

    public int addEntityMetadata(String name, String url, String phoneType, String phone, String email, String location, String type) {
	if (name == null || name.equals(""))
	    return -1;

	Entity entity = new Entity(sanitize(name), sanitize(url), sanitize(phoneType), sanitize(phone), 
				   sanitize(email), sanitize(location), sanitize(type));
	medialib.insert(entity);
	entity = (Entity)(medialib.getSingle(entity));

	if (entity != null) {
	    return entity.getEntityID();
	} else
	    return -1;
    }

    public String editEntityMetadata(int entityID, String name, String url, String phoneType, String phone, String email, String location, String type) {
	if (cacheProvider == null) {
	    logger.error("EditEntityMetadata: CacheProvider is null");
	    return null;
	}

	Entity entity = Entity.getDummyObject(new Integer(entityID));
	Entity newEntity = new Entity(sanitize(name), sanitize(url), sanitize(phoneType), sanitize(phone), 
				      sanitize(email), sanitize(location), sanitize(type));
	newEntity.setEntityID(entityID);
	medialib.update(entity, newEntity);
	newEntity = cacheProvider.getSingle(newEntity);

	if (newEntity != null) {
	    return new Integer(newEntity.getEntityID()).toString();
	} else
	    return null;	
    }

    public int editProgramMetadata(String programID, String title, String description, String language, String license) {
	RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(programID);
	RadioProgramMetadata newMetadata = RadioProgramMetadata.getDummyObject("");
	newMetadata.setTitle(title);
	newMetadata.setDescription(description);
	newMetadata.setLanguage(language);
	newMetadata.setLicenseType(license);
	newMetadata.truncateFields();
	return medialib.update(metadata, newMetadata);
    }

    public int updateProgramIndex(String programID) {
	//RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(programID);
	//metadata = (RadioProgramMetadata)(cacheProvider.getSingle(metadata));
	if (provider != null)
	    return sendUpdateMessage(programID);
	else
	    return indexlib.updateProgram(programID);
    }

    //    public int flushAnchorIndex() {
    //	if (provider == null)
    //	    return indexlib.flushAnchorIndex();
    //	else
    //	    return -1;
    //    }

    public int flushProgramIndex() {
	if (provider == null)
	    return indexlib.flushProgramIndex();
	else
	    return sendFlushIndexMessage();
    }

    //    public void dumpAnchorIndex() {
    //	indexlib.dumpAnchorIndex();
    //    }

    //    public void dumpProgramIndex() {
    //	indexlib.dumpProgramIndex();
    //    }

    private int sendAddAnchorTerm(String anchorID, String filterID, String indexStr) {
	logger.debug("SendAddAnchorTerm");
	try {
	    AddAnchorTermMessage anchorMessage = new AddAnchorTermMessage(IPCSyncMessage.SYNC_MESSAGE, 
									  RSController.INDEX_SERVICE,
									  anchorID, filterID, indexStr);
	    AddAnchorTermResponseMessage anchorResponseMessage = (AddAnchorTermResponseMessage)(provider.sendSyncCommand(anchorMessage));
	    if (anchorResponseMessage == null) {
		// unable to send
		return -1;
	    } else {
		// success
		return anchorResponseMessage.getReturnValue();
	    }
	} catch(IPCSyncMessageTimeoutException e) {
	    // timeout
	    return -1;
	}
    }

    private int sendUpdateMessage(String programID) {
	logger.debug("SendUpdateMessage");
	try {
	    UpdateProgramMessage programMessage = new UpdateProgramMessage(IPCSyncMessage.SYNC_MESSAGE, 
									   RSController.INDEX_SERVICE,
									   new String[]{programID});
	    UpdateProgramResponseMessage programResponseMessage = (UpdateProgramResponseMessage)(provider.sendSyncCommand(programMessage));

	    if (programResponseMessage == null) {
		return -1;
	    } else {
		return programResponseMessage.getReturnValue()[0];
	    }
	} catch(IPCSyncMessageTimeoutException e) {
	    return -1;
	}
    }

    private int sendFlushIndexMessage() {
	logger.debug("SendFlushIndexMessage");
	try {
	    FlushIndexMessage flushMessage = new FlushIndexMessage(IPCSyncMessage.SYNC_MESSAGE, RSController.INDEX_SERVICE);
	    FlushIndexResponseMessage flushResponseMessage = (FlushIndexResponseMessage)(provider.sendSyncCommand(flushMessage));

	    if (flushResponseMessage == null)
		return -1;
	    else
		return flushResponseMessage.getReturnValue();
	} catch(IPCSyncMessageTimeoutException e) {
	    return -1;
	}
    }

        /* Exposing Medialib Functions */
    //XXX Add error trapping and forwarding.

    public int update(Metadata queryMetadata, Metadata updateMetadata){
	return medialib.update(queryMetadata, updateMetadata);
    }

    public int insert(Metadata metadata){
	return medialib.insert(metadata);
    }

    public int remove(Metadata metadata){
	return medialib.remove(metadata);
    }

    public int remove(Metadata metadata, boolean invalidateCache) {
	return medialib.remove(metadata, invalidateCache);
    }

    public <T extends Metadata> T[] get(T metadata, String whereClause, String[] sortField, String[] order, String[] between, int... limits){
	return medialib.get(metadata, whereClause, sortField, order, between, limits);
    }

    public <T extends Metadata> T[] get(T metadata){
	return medialib.get(metadata);
    }

    public <T extends Metadata> T getSingle(T metadata){
	return medialib.getSingle(metadata);
    }

    public <T extends Metadata> T[] selectAllFields(T metadata, String whereClause, int... limits) {
	return medialib.selectAllFields(metadata, whereClause, limits);
    }

    public Object mathOp(String tableName, String whereClause, String fieldName, String operation) {
	return medialib.mathOp(tableName, whereClause, fieldName, operation);
    }
    
    public Object[] select(String joinTableNames, String whereClause, String fieldName, int limit) {
	return medialib.select(joinTableNames, whereClause, fieldName, limit);
    }

    public Object[] unique(Metadata metadata, String fieldName) {
	return medialib.unique(metadata, fieldName);
    }

    /* PlayoutHistoryFilter Functions */
    
    public long getMinTelecastTime(){
	return PlayoutHistoryFilter.getMinTelecastTime(medialib);
    }

    public long getMaxTelecastTime(){
	return PlayoutHistoryFilter.getMaxTelecastTime(medialib);
    }

    /* CreationTimeFilter Functions */

    public long getMinCreationTime(){
	return CreationTimeFilter.getMinCreationTime(medialib);
    }

    public long getMaxCreationTime(){
	return CreationTimeFilter.getMaxCreationTime(medialib);
    }

    /* LengthFilter Functions */

    public long getMaxLength(){
	return LengthFilter.getMaxLength(medialib);
    }

}
