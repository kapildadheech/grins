package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;

public abstract class PlayoutPreviewProvider extends RSProviderBase {

    public PlayoutPreviewProvider(UIService uiService, String providerName) {
	super(uiService, providerName);
    }

    
    public abstract int play(String fileID, PlayoutPreviewListenerCallback listener);
    //public abstract int play(String fileID, long fadeoutStartTime, PlayoutPreviewListenerCallback listener);
    public abstract int pause(String fileID, PlayoutPreviewListenerCallback listener);
    public abstract int stop(String fileID, PlayoutPreviewListenerCallback listener);
    public abstract int forceStop(PlayoutPreviewListenerCallback listener);
    public abstract int volumeUp(String fileID, PlayoutPreviewListenerCallback listener);
    public abstract int volumeDn(String fileID, PlayoutPreviewListenerCallback listener);

    public abstract boolean isPlaying();
    public abstract boolean isPlayable();
    public abstract boolean isPausable();
    public abstract boolean isVolumeAdjustable();

}