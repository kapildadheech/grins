package org.gramvaani.radio.app.providers;

public interface MicListenerCallback extends ListenerCallback {

    public void activateMic(boolean activate, String[] error);
    public void enableMicUI(boolean enable, String[] error);
    public void micGstError(String error);
    public void micEnabled(boolean success, String[] error);
    public void micDisabled(boolean success, String[] error);

    public void micLevelTested(boolean success, int level);
}