package org.gramvaani.radio.app.providers;

public interface CacheListenerCallback extends ListenerCallback {

    public void activateCache(boolean activate, String[] errorCodeAndParams);
    public void cacheObjectsDeleted(String objectType, String[] objectID);
    public void cacheObjectsUpdated(String objectType, String[] objectID);
    public void cacheObjectsInserted(String objectType, String[] objectID);

}