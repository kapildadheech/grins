package org.gramvaani.radio.app.providers;

public interface PlayoutPreviewListenerCallback extends ListenerCallback {

    public void playActive(boolean success, String programID, String playType, long telecastTime, String[] errorCodeAndParams);
    public void pauseActive(boolean success, String programID, String playType, String[] errorCodeAndParams);
    public void stopActive(boolean success, String programID, String playType, String[] errorCodeAndParams);
    public void playDone(boolean success, String programID, String playType);
    public void audioGstError(String programID, String error, String playType);
    public void playoutPreviewError(String errorCode, String[] errorParams);
    public void activateAudio(boolean activate, String playType, String[] errorCodeAndParams);
    public void enableAudioUI(boolean enable, String playType, String[] errorCodeAndParams);

}