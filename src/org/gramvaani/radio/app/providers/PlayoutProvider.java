package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.*;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class PlayoutProvider extends PlayoutPreviewProvider {
    protected final static int CONFIGURING = -2;
    protected final static int RESOURCE_INTEREST = -1;
    protected final static int LISTENING = 0;
    protected final static int PLAY_COMMAND_SENT = 1;
    protected final static int PLAY_ACTIVE = 2;
    protected final static int PAUSE_COMMAND_SENT = 3;
    protected final static int PAUSE_ACTIVE = 4;
    protected final static int STOP_COMMAND_SENT = 5;

    protected final static int POOL_THREAD_COUNT = 1;

    protected int state = CONFIGURING;
    protected int prevState = state;
    protected boolean prematurePlay = false;
    protected double oldVolume = 0.0, newVolume = 0.0;
    

    //protected String sessionID = "";
    protected String fileID;

    protected String serviceInstanceName;
    protected String audioServiceID = "";

    protected Hashtable<String, PlayoutPreviewListenerCallback> listeners;
    protected Hashtable<String, SessionInfo> sessionInfos = new Hashtable<String, SessionInfo>();

    protected Hashtable<String, String> programs;
    protected Hashtable<String, String> guids;
    protected ExecutorService listenersActivator;
    protected String playType;

    protected boolean audioDisconnected = false;
    protected boolean rscActive = true;
    
    protected StationConfiguration stationConfig;

    public PlayoutProvider(UIService uiService) {
	super (uiService, RSApp.PLAYOUT_PROVIDER);

	serviceInstanceName = uiService.getProviderServiceInstance(RSApp.PLAYOUT_PROVIDER, AudioService.PLAYOUT);
	listeners = new Hashtable<String, PlayoutPreviewListenerCallback>();
	//sessionInfos = new Hashtable<String, SessionInfo>();
	programs = new Hashtable<String, String>();
	guids = new Hashtable<String, String>();

	playType = AudioService.PLAYOUT;
	stationConfig = uiService.getStationConfiguration();

	listenersActivator = Executors.newFixedThreadPool(POOL_THREAD_COUNT);
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    listenersActivator.shutdown();
		}
	    });

	logger.debug("Initialized with serviceInstanceName="+serviceInstanceName);
    }

    public synchronized int activate() {
	logger.info("Activate: Provider activated.");
	setState(CONFIGURING);
	audioDisconnected = false;
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String[] {serviceInstanceName});

	int retval = sendCommand(message);
	if (retval == -1)
	    rscActive = false;

	return retval;
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listeners.put(listener.getName(), (PlayoutPreviewListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	listeners.remove(listenerName);
    }

    public synchronized boolean isActive() {
	return !(state() == CONFIGURING);
    }

    public synchronized void receiveMessage(IPCMessage message) {
	logger.debug("ReceiveMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	boolean isAudioActivated = false;
	if (message instanceof AudioAckMessage){
	    AudioAckMessage ackMessage = (AudioAckMessage) message;
	    String sessionID;
	    if (state() == PLAY_COMMAND_SENT && ackMessage.getCommand().equals(AudioService.PLAY_MSG))
		sessionID = ackMessage.getSessionID();
	    
	    handleAudioAck(ackMessage.getAck(), ackMessage.getItemID(), ackMessage.getCommand(), 
			   ackMessage.getSessionID(), ackMessage.getParams(), ackMessage.getError(), ackMessage.getOptions());

	} else if (message instanceof ServiceAckMessage){
	    
	    ServiceAckMessage ackMessage = (ServiceAckMessage) message;
	    for (String service: ackMessage.getActiveServices())
		if (service.equals(serviceInstanceName))
		    isAudioActivated = true;

	    for (String service: ackMessage.getInactiveServices()) {
		if (service.equals(serviceInstanceName)) {
		    isAudioActivated = false;
		    setState (CONFIGURING);
		    activateAudio(false, playType, new String[]{RSController.ERROR_SERVICE_DOWN,
									     service});

		}
	    }

	    String[] activatedServices = ackMessage.getActiveServices();
	    String[] activatedIDs = ackMessage.getActiveServiceIDs();

	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(serviceInstanceName))
		    audioServiceConnected(activatedIDs[i]);
	    }


	} else if (message instanceof ServiceNotifyMessage){
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage) message;
	    for (String service: notifyMessage.getActivatedServices()) {
		if (service.equals(serviceInstanceName)){
		    if (!audioDisconnected){
			isAudioActivated = true;
		    } else {
			audioDisconnected = false;
			enableAudioUI(true, playType, new String[]{});
			return;
		    }
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing. Wait for playout service to be activated
		    }
		}
	    }
	    for (String service: notifyMessage.getInactivatedServices()){
		if (service.equals(serviceInstanceName)){
		    audioDisconnected = true;
		    enableAudioUI(false, playType, new String[] {RSController.ERROR_SERVICE_DISCONNECTED, serviceInstanceName});
		    return;
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    audioDisconnected = true;
		    logger.debug("ReceiveMessage: Got RSController disconnected.");
		    enableAudioUI(false, playType, new String[] {RSController.ERROR_SERVICE_DISCONNECTED, RSController.RESOURCE_MANAGER});
		    return;
		}
	    }

	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();

	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(serviceInstanceName))
		    audioServiceConnected(activatedIDs[i]);
	    }

	} else if (message instanceof AudioErrorMessage) {
	    AudioErrorMessage errorMessage = (AudioErrorMessage) message;
	    handleAudioError(errorMessage.getItemID(), errorMessage.getSessionID(), errorMessage.getError(), errorMessage.getOptions());
	} else if (message instanceof AudioSessionsInfoMessage) {
	    AudioSessionsInfoMessage infoMessage = (AudioSessionsInfoMessage)message;
	    handleAudioSessionsInfoMessage(infoMessage);
	}

	if (isAudioActivated)
	    audioServiceActivated();
    }

    protected void handleAudioSessionsInfoMessage(AudioSessionsInfoMessage infoMessage) {
	String[] remoteSessionIDs = infoMessage.getSessionIDs();

	for (String localID : sessionInfos.keySet()) {
	    boolean foundID = false;
	    for (String remoteID : remoteSessionIDs) {
		if (remoteID.equals(localID)) {
		    
		    foundID = true;
		    break;
		}
	    }
	    
	    if (!foundID) {
		SessionInfo session = sessionInfos.remove(localID);
		PlayoutPreviewListenerCallback listener = session.getListener();
		String programID = session.getProgramID();
		String guid = guids.get(programID);
		logger.warn("HandleAudioSessionsInfoMessage: SessionID:" + localID + " not found with playout service. Calling play done for programID:" + programID);
		guids.remove(programID);
		if (guid != null) 
		    programs.remove(programID + "." + guid);

		playDone(listener, true, programID, AudioService.PLAYOUT);
	    }
	}
    }

    protected void audioServiceConnected(String newID){
	logger.debug("AudioServiceConnected with ID: "+newID);

	if (!audioServiceID.equals("") && !audioServiceID.equals(newID)){
	    activateAudio(false, playType, new String[]{RSController.ERROR_SERVICE_DOWN, serviceInstanceName});
	    activateAudio(true, playType, new String[]{});
	}

	audioServiceID = newID;
    }

    
    /* Sending callbacks to listeners routed through an executor to avoid deadlocks */

    protected void audioGstError(final PlayoutPreviewListenerCallback callback, final String programID, final String error, final String playType) {
	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null)
			callback.audioGstError(programID, error, playType);
		}
	    };

	listenersActivator.execute(r);
    }

    protected void playoutPreviewError(final PlayoutPreviewListenerCallback callback, final String errorCode, final String[] error) {
	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null) 
			callback.playoutPreviewError(errorCode, error);
		}
	    };

	listenersActivator.execute(r);
    }
    
    protected void playActive(final PlayoutPreviewListenerCallback callback, final boolean success, final String programID, final String playType, final long telecastTime, final String[] error) {

	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null) 
			callback.playActive(success, programID, playType, telecastTime, error);
		}
	    };

	listenersActivator.execute(r);
    }

    protected void pauseActive(final PlayoutPreviewListenerCallback callback, final boolean success, final String programID, final String playType, final String[] error) {

	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null) 
			callback.pauseActive(success, programID, playType, error);
		}
	    };

	listenersActivator.execute(r);
    }

    protected void stopActive(final PlayoutPreviewListenerCallback callback, final boolean success, final String programID, final String playType, final String[] error) {

	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null)
			callback.stopActive(success, programID, playType, error);
		}
	    };

	listenersActivator.execute(r);
    }

    protected void playDone(final PlayoutPreviewListenerCallback callback, final boolean success, final String programID, final String playType){
	Runnable r = new Runnable() {
		public void run() {
		    if (callback != null)
			callback.playDone(success, programID, playType);
		}
	    };

	listenersActivator.execute(r);
    }

    protected void activateAudio(final boolean activate, final String playType, final String[] error) {
	Runnable r = new Runnable() {
		public void run() {
		    for (PlayoutPreviewListenerCallback callback: listeners.values()) {
			if (callback != null)
			    callback.activateAudio(activate, playType, error);
		    }
		    
		}
	    };

	listenersActivator.execute(r);
    }

    protected void enableAudioUI(final boolean enable, final String playType, final String[] error) {
	Runnable r = new Runnable() {
		public void run() {
		    for (PlayoutPreviewListenerCallback callback: listeners.values()) {
			if (callback != null)			
			    callback.enableAudioUI(enable, playType, error);
		    }
		    
		}
	    };

	listenersActivator.execute(r);
    }

    /* End of call backs through executors */

    protected void handleAudioError(String programID, String sessionID, String error, String[] options){
	logger.error("HandleAudioError: For program: "+programID+" error: "+error);
	SessionInfo info = sessionInfos.remove(sessionID);
	PlayoutPreviewListenerCallback listener = null;

	//if (info != null)
	String guid = "";
	if (options.length > 0)
	    guid = options[0];
	String listenerName = programs.get(programID + "." + guid);
	if (listenerName != null)
	    listener = listeners.get(listenerName);

	if (listener != null){
	    logger.debug("HandleAudioError: Sending gstError to listener.");
	    //VVV send message to inference engine
	    audioGstError(listener, programID, error, playType);
	} else {
	    logger.error("HandleAudioError: Unable to locate listener for session: "+sessionID);
	}

	//logger.error("ERROR REMOVING PROGRAMID: "+programID);
	programs.remove(programID + "." + guid);
	if (guids.get(programID).equals(guid)) {
	    logger.warn("AUDIO ERROR MESSAGE: remove from guids: " + programID + ":" + guids.get(programID));
	    guids.remove(programID);
	}
    }

    /*
    protected void handleAudioAck (String ack, String programID, String command, 
				   String sessionID, Hashtable<String,String> params){

	logger.info("HandleAudioAck: " + ack + ":" + state);
	switch (state()){

	case PLAY_COMMAND_SENT:
	    if (prematurePlay && command.equals(AudioService.STOP_MSG)) {

		//PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		PlayoutPreviewListenerCallback callback = sessionListeners.get(sessionID);
		
		if (ack.equals(AudioService.SUCCESS)) {
		    if (callback != null)
			callback.stopActive(true, programID, playType);
		} else {
		    if (callback != null)
			callback.stopActive(false, programID, playType);
		}
		prematurePlay = false;
		
		programs.remove(programID);
		sessionListeners.remove(sessionID);

	    } else {
		if (ack.equals(AudioService.SUCCESS)){
		    logger.info("HandleAudioAck: Playing.");
		    setState(PLAY_ACTIVE);
		    //PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		    //PlayoutPreviewListenerCallback callback = listeners.get(programs.remove(programID));
		    sessionListeners.put(sessionID, callback);
		    if (callback != null)
			callback.playActive(true, programID, playType, Long.parseLong(params.get(AudioAckMessage.TELECAST_TIME)));
		} else {
		    logger.error("HandleAudioAck: Unable to play.");
		    displayError("Unable to play.");
		    setState(LISTENING);

		    PlayoutPreviewListenerCallback callback = null;
		    if ((callback = sessionListeners.get(sessionID)) == null)
			callback = listeners.get(programs.get(programID));

		    programs.remove(programID);
		    sessionListeners.remove(sessionID);

		    if (callback != null)
			callback.playActive(false, programID, playType, -1L);
		}
	    }
	    break;

	case PAUSE_COMMAND_SENT:
	    if (ack.equals(AudioService.SUCCESS)){
		logger.info("HandleAudioAck: Paused.");
		setState(PAUSE_ACTIVE);
		PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		if (callback != null)
		    callback.pauseActive(true, programID, playType);
	    } else {
		logger.error("HandleAudioAck: Unable to pause.");
		setState(PLAY_ACTIVE);
		displayError("Unable to pause.");
		PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		if (callback != null)
		    callback.pauseActive(false, programID, playType);
	    }
	    break;

	case STOP_COMMAND_SENT:
	    if (ack.equals(AudioService.SUCCESS)){
		logger.info("HandleAudioAck: Stopped.");
		setState(LISTENING);
		PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		if (callback != null)
		    callback.stopActive(true, programID, playType);
	    } else {
		logger.error("HandleAudioAck: Unable to stop.");
		displayError("Unable to stop.");
		PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		if (callback != null)
		    callback.stopActive(false, programID, playType);
	    }
	    break;
	case PLAY_ACTIVE:
	    if (params.get("command").equals(AudioService.PLAY_DONE_MSG)){
		if (ack.equals(AudioService.SUCCESS)){
		    logger.info("HandleAudioAck: Play done.");
		    setState(LISTENING);
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		    if (callback != null)
			callback.playDone(true, programID, playType);
		}
		else{
		    logger.error("HandleAudioAck: Unknown error received with message "+AudioService.PLAY_DONE_MSG);
		    displayError("Unknown Error.");
		    PlayoutPreviewListenerCallback callback = listeners.get(programs.get(programID));
		    if (callback != null)
			callback.unknownError(programID, playType);
		}
	    }
	    else {
		logger.error("HandleAudioAck: Uknown Ack received in PLAY ACTIVE state.");
	    }
	    break;
	}
    }

    */

    protected void handleAudioAck(String ack, String programID, String command, 
				  String sessionID, Hashtable<String, String> params, String[] error, String[] options){
	if (state() != LISTENING){
	    logger.error("HandleAudioAck: Ack received when not in LISTENING state.");
	    return;
	}

	PlayoutPreviewListenerCallback listener = null;

	String guid = "";
	if (options.length == 3)
	    guid = options[2];
	else if (options.length == 1)
	    guid = options[0];

	if (command == null) {
	    logger.error("HandleAudioAck: Null command");
	    return;
	}

	if (command.equals(AudioService.STOP_MSG)){
	    listener = sessionInfos.get(sessionID).getListener();

	    boolean success = ack.equals(AudioService.SUCCESS);
	    if (listener != null)
		stopActive(listener, success, programID, playType, error);
	    
	    if (success)
		logger.info("HandleAudioAck: Stopping: "+programID+":"+sessionID);
	    else {
		logger.error("HandleAudioAck: Unable to stop: "+programID+":"+sessionID);
		// VVV send error message to inference engine
	    }

	    //logger.error("Audio ack REMOVING PROGRAMID: "+programID);
	    programs.remove(programID + "." + guid);
	    if (guids.get(programID).equals(guid)) {
		logger.warn("HandleAudioAck:STOP message: remove from guids: " + programID + ":" + guids.get(programID));
		guids.remove(programID);
	    }
	    sessionInfos.remove(sessionID);

	} else if (command.equals(AudioService.PLAY_MSG)){

	    //Could be new play or resume.
	    
	    boolean success = ack.equals(AudioService.SUCCESS);
	    listener = null;

	    if (success){
		if (sessionInfos.get(sessionID) != null)
		    listener = sessionInfos.get(sessionID).getListener();

		if (listener == null){
		    //new file being played.
		    //logger.error("audio ack 2 REMOVING PROGRAMID: "+programID);
		    listener = listeners.get(programs.remove(programID + "." + guid));
		    sessionInfos.put(sessionID, new SessionInfo(sessionID, programID, listener));
		}
	    } else {
		if (listener == null && programs.get(programID + "." + guid) != null){
		    //logger.error("audio ack 3 REMOVING PROGRAMID: "+programID);
		    listener = listeners.get(programs.remove(programID + "." + guid));
		}
	    }

	    if (success)
		logger.info("HandleAudioAck: Playing: "+programID+":"+sessionID);
	    else {
		logger.error("HandleAudioAck: Unable to play: "+programID+":"+sessionID);
		//VVV send the error to inference engine
	    }
	    
	    if (listener != null)
		playActive(listener, success, programID, playType, Long.parseLong(params.get(AudioAckMessage.TELECAST_TIME)), error);
	    else {
		logger.error("HandleAudioAck: Unable to locate listener for program: "+programID+" session: "+sessionID);
	    }

	} else if (command.equals(AudioService.PAUSE_MSG)){
	    
	    listener = sessionInfos.get(sessionID).getListener();
	    boolean success = ack.equals(AudioService.SUCCESS);

	    if (success)
		logger.info("HandleAudioAck: Paused: "+programID+":"+sessionID);
	    else {
		logger.error("HandleAudioAck: Unable to Pause: "+programID+":"+sessionID);
		//VVV send message to inference engine
	    }

	    if (listener != null)
		pauseActive(listener, success, programID, playType, error);

	} else if (command.equals(AudioService.PLAY_DONE_MSG)){
	    //logger.error("audio ack play done REMOVING PROGRAMID: "+programID);
	    programs.remove(programID + "." + guid);
	    if (guids.get(programID) != null && guids.get(programID).equals(guid)) {
		//logger.warn("HandleAudioAck: PLAY DONE message: remove from guids: " + programID + ":" + guids.get(programID));
		guids.remove(programID);
	    }

	    SessionInfo session = sessionInfos.remove(sessionID);
	    if (session == null) {
		logger.warn("HandleAudioAck: No session found for programID:" + programID + " and sessionID:" + sessionID + ". The session may have been removed in response to audio session info message.");
		return;
	    } 
	    
	    listener = session.getListener();

	    if (ack.equals(AudioService.SUCCESS)){
		logger.info("HandleAudioAck: Play done: "+programID+":"+sessionID);
		playDone(listener, true, programID, playType);
	    } else {
		logger.error("HandleAudioAck: Unknown error received with msg: "+AudioService.PLAY_DONE_MSG);
		if (listener != null) {
		    playoutPreviewError(listener, null, new String[]{programID, playType});
		}
	    }
		
	}

    }

    protected void audioServiceActivated (){
	if (state() == CONFIGURING) {
	    logger.info("AudioServiceActivated: Audio Service is now activated");
	    setState (LISTENING);
	    activateAudio(true, playType, new String[]{});
	}
    }

    protected void displayError (String error){
	logger.error(error);
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: From: "+message.getSource()+" Type: " + message.getParam("serviceerror"));
	
	String serviceName = null;
	String errorCode = null;

	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}

	//ErrorCode ignored here.
	if (serviceName != null && serviceName.equals(serviceInstanceName)) {
	    setState(CONFIGURING);
	    audioDisconnected = false;
	    sessionInfos.clear();
	    programs.clear();
	    // VVV send message inference engine
	    activateAudio(false, playType, new String[] {RSController.ERROR_SERVICE_DOWN, serviceInstanceName});

	} else if (serviceName.equals(RSController.RESOURCE_MANAGER)){
	    setState(CONFIGURING);
	    audioDisconnected = false;
	    rscActive = false;
	    sessionInfos.clear();
	    programs.clear();
	    // VVV send message inference engine
	    activateAudio(false, playType, new String[] {RSController.ERROR_SERVICE_DOWN, RSController.RESOURCE_MANAGER});

	} else {
	    logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
	    for (PlayoutPreviewListenerCallback callback: listeners.values()) {
		// VVV send message inference engine
		playoutPreviewError(callback, null, new String[]{null, playType});
	    }
	}

    }

    String getSessionID(String programID, PlayoutPreviewListenerCallback listener){
	for (SessionInfo info: sessionInfos.values()){
	    if (info.getProgramID().equals(programID) && info.getListener() == listener)
		return info.getSessionID();
	}
	return "";
    }

    public synchronized int replay(String fileID, PlayoutPreviewListenerCallback listener) {
	return play(fileID, guids.get(fileID), -1, -1, false, listener, false, true);
    }

    public synchronized int replay(String fileID, long fadeoutStartTime, boolean goLive, PlayoutPreviewListenerCallback listener) {
	return play(fileID, guids.get(fileID), -1, fadeoutStartTime, goLive, listener, false, true);
    }

    public synchronized int play(String fileID, PlayoutPreviewListenerCallback listener){
	return play(fileID, "", -1, -1, false, listener, true, true);
    }

    public synchronized int play(String fileID, PlayoutPreviewListenerCallback listener, boolean updateHistory){
	return play(fileID, "", -1, -1, false, listener, true, updateHistory);
    }

    public synchronized int play(String fileID, long position, long fadeoutStartTime, boolean goLive, PlayoutPreviewListenerCallback listener) {
	return play(fileID, "", position, fadeoutStartTime, goLive, listener, true, true);
    }

    protected synchronized int play(String fileID, String guid, long position, long fadeoutStartTime, boolean goLive, PlayoutPreviewListenerCallback listener, boolean freshPlay, boolean updateHistory) {
	
	if (state() != LISTENING && state() != PAUSE_ACTIVE && state() != STOP_COMMAND_SENT) {
	    logger.warn("Play: Unable to start since Provider not in LISTENING or PAUSE_ACTIVE states");
	    return -1;
	}
	
	logger.info("GRINS_USAGE:PLAYOUT_PLAY:"+fileID);

	String sessionID = getSessionID(fileID, listener);
	if (freshPlay) {
	    if (sessionID != null && !sessionID.equals("")) {
		logger.warn("play: Have not received a stop Ack as yet. Sending command down anyways: " + sessionID);
		sessionID = "";
	    }
	}
	
	SessionInfo info = sessionInfos.get(sessionID);
	
	/*
	if (info != null)
	    synchronized(info){
		if (info.isStopping()){
		    sessionID = "";
		}
	    }
	*/

	this.fileID = fileID;
	if (guid.equals("")) {
	    guid = GUIDUtils.getGUID();
	    logger.debug("PLAY MESSAGE: put into guids: " + fileID + ":" + guid);
	    guids.put(fileID, guid);
	}
	if (freshPlay) {
	    if (listener != null) {
		programs.put(fileID + "." + guid, listener.getName());
	    } else {
		programs.put(fileID + "." + guid, "");
	    }
	}
	
	String gain = "0.0";
	/*if (goLive){
	    gain = Double.toString(stationConfig.getDoubleParam(StationConfiguration.GOLIVE_BG_GAIN));
	    }*/

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(AudioService.OPTION_FADEOUT_START, Long.toString(fadeoutStartTime));
	options.put(AudioService.OPTION_GAIN, gain);
	options.put(AudioService.OPTION_GUID, guid);
	options.put(AudioService.OPTION_GOLIVE, Boolean.toString(goLive));
	options.put(AudioService.OPTION_UPDATE_HISTORY, Boolean.toString(updateHistory));
	options.put(AudioService.OPTION_POSITION, Long.toString(position));

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.PLAY_MSG, 
							      fileID, playType, options);

	if (sendCommand(message) == 0){
	    logger.info("Play: Play command sent: fileID: "+fileID+" session: "+sessionID);
	} else {
	    logger.error("Play: Unable to play since play message could not be sent.");
	    setState(LISTENING);
	    return -1;
	}

	return 0;
    }
    
    public synchronized int setGoLiveActive(String fileID, boolean active, PlayoutPreviewListenerCallback listener) {

	String sessionID = getSessionID(fileID, listener);
	if (sessionID == null) {
	    logger.warn("SetGoLiveActive: Attempting to set gain on a non existing session.");
	    return -1;
	}

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(AudioService.OPTION_GOLIVE, Boolean.toString(active));

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.SET_GOLIVE_MSG, 
							      fileID, playType, options);

	if (sendCommand(message) == 0){
	    logger.info("SetGoLiveActive: Set go live command sent: fileID: " + fileID + " session: " + sessionID + " active: " + active);
	} else {
	    logger.error("SetGoLiveActive: Set go live message could not be sent.");
	    return -1;
	}

	return 0;
    }

    public synchronized int pause(String fileID, PlayoutPreviewListenerCallback listener) {
	
	String sessionID = getSessionID(fileID, listener);

	if (sessionID.equals("")){
	    logger.warn("Pause: Not able to locate existing sessions for: "+fileID+":"+listener);
	    return -1;
	}
	
	logger.info("GRINS_USAGE:PLAYOUT_PAUSE:"+fileID);

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.PAUSE_MSG, 
							      fileID, playType, null);

	if (sendCommand(message) == 0){
	    logger.info("Pause: Pause command sent.");
	} else {
	    logger.error("Pause: Unable to pause since pause message could not be sent.");
	    setState(LISTENING);
	    return -1;
	}

	return 0;
    }

    public synchronized int stop(String fileID, PlayoutPreviewListenerCallback listener) {
	String sessionID = getSessionID(fileID, listener);

	if (sessionID == null || sessionID.equals("")){
	    logger.warn("Stop: Unable to stop since not able to locate session: "+fileID+":"+listener);
	    return -1;
	}
	
	logger.info("GRINS_USAGE:PLAYOUT_STOP:"+fileID);

	SessionInfo info = sessionInfos.get(sessionID);
	info.setStopping();

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.STOP_MSG, 
							      fileID, playType, null);

	if (sendCommand(message) == 0){
	    logger.info("Stop: Stop command sent.");
	} else {
	    logger.error("Stop: Stop command could not be sent.");
	    setState(LISTENING);
	    return -1;
	}

	return 0;
    }

    public synchronized int forceStop(PlayoutPreviewListenerCallback listener) {
	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      "", AudioService.FORCE_STOP_MSG, "", 
							      playType, null);

	if (sendCommand(message) == 0){
	    logger.info("ForceStop: Stop command sent.");

	    //Leaving this stopActive as it is and not using ExecutorService
	    //This helps ensure that all methods calling ExecutorSevice.execute
	    //are called from the UI service thread only and hence their thread
	    //safety is not a concern. 
	    for (SessionInfo sessionInfo: sessionInfos.values())
		if (sessionInfo.getListener() != null && sessionInfo.getListener() != listener)
		    sessionInfo.getListener().stopActive(true, sessionInfo.getProgramID(), AudioService.PLAYOUT, new String[]{});

	    programs.clear();
	    guids.clear();
	    sessionInfos.clear();
	    setState(LISTENING);

	} else {
	    logger.error("ForceStop: Stop command could not be sent.");
	    setState(LISTENING);
	    return -1;
	}

	return 0;

    }

    public synchronized int forceStop(){
	return forceStop(null);
    }
    
    
    public synchronized int volumeUp(String fileID, PlayoutPreviewListenerCallback listener) {
	String sessionID = getSessionID(fileID, listener);
	if (sessionID == null) {
	    logger.warn("SetGain: Attempting to set gain on a non existing session.");
	    return -1;
	}

	String deltaGain = Double.toString(AudioService.DELTA_GAIN);

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(AudioService.OPTION_GAIN, deltaGain);

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(),
							      sessionID, AudioService.VOL_UP_MSG, "", 
							      playType, options);
	if (sendCommand(message) == 0){
	    logger.debug("VolumeUp: Volume up command sent.");
	} else {
	    logger.error("VolumeUp: Volume up command could not be sent.");
	    return -1;
	}

	return 0;
    }

    public synchronized int volumeDn(String fileID, PlayoutPreviewListenerCallback listener) {
	String sessionID = getSessionID(fileID, listener);
	if (sessionID == null) {
	    logger.warn("SetGain: Attempting to set gain on a non existing session.");
	    return -1;
	}
	
	String deltaGain = Double.toString(-AudioService.DELTA_GAIN);

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(AudioService.OPTION_GAIN, deltaGain);

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(),
							      sessionID, AudioService.VOL_DOWN_MSG, "", 
							      playType, options);
	if (sendCommand(message) == 0){
	    logger.debug("VolumeDn: Volume down command sent.");
	} else {
	    logger.error("VolumeDn: Volume down command could not be sent.");
	    return -1;
	}


	return 0;
    }
    
    
    public synchronized int volumeChanged(String fileID, PlayoutPreviewListenerCallback listener, int gain){
	String sessionID = getSessionID(fileID, listener);
	if (sessionID == null) {
	    logger.warn("volumeChanged: Attempting to set gain on a non existing session.");
	    return -1;
	}
	
	String newGain = Double.toString(1.0 * gain);

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(AudioService.OPTION_GAIN, newGain);

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(),
							      sessionID, AudioService.VOL_CHANGED_MSG, "", 
							      playType, options);
	if (sendCommand(message) == 0){
	    logger.debug("VolumeChanged: Volume changed command sent.");
	} else {
	    logger.error("VolumeChanged: Volume changed command could not be sent.");
	    return -1;
	}
	return 0;
    }
    

    /*
    public synchronized int seekToPosition (String fileID, int position, PlayoutPreviewListenerCallback listener){
	String sessionID = getSessionID(fileID, listener);
	if (sessionID == null){
	    logger.warn("SeekToPosition: Attempting to seek in a non existing session.");
	    return -1;
	}

	AudioCommandMessage message = new AudioCommandMessage("", serviceInstanceName, getName(), 
							      sessionID, AudioService.SEEK_MSG, "", 
							      playType, new String[]{Integer.toString(position)});
	
	if (sendCommand(message) == 0){
	    logger.debug("SeekToPosition: Seek command sent.");
	} else {
	    logger.error("SeekToPosition: Seek command could not be sent.");
	    return -1;
	}
	return 0;
    }
    */

    public boolean isPlaying() {
	return sessionInfos.size() > 0;
    }

    public boolean isPlaying(String fileID, PlayoutPreviewListenerCallback listener){
	return getSessionID(fileID, listener).length() > 0;
    }

    public boolean isPlayable() {
	return (state() == LISTENING || state() == PAUSE_ACTIVE);	    
    }

    public boolean isPausable() {
	//return (state() == PLAY_ACTIVE);
	return (state() == LISTENING) && isSessionActive();
    }

    public synchronized boolean isStoppable() {
	//return (state() == PLAY_ACTIVE || state() == PAUSE_ACTIVE);
	return (state() == LISTENING) && isSessionActive();
    }

    public synchronized boolean isCreatingSession(){

	boolean stoppingSessions = false;
	
	for (SessionInfo info: sessionInfos.values())
	    if (info.isStopping()){
		stoppingSessions = true;
		break;
	    }
	
	//System.err.println("CREATING: "+(programs.size() > 0 || stoppingSessions));
	//System.err.println("CREATING: "+(programs.size()));
	return (programs.size() > 0) || stoppingSessions;
    }

    public boolean isVolumeAdjustable() {
	//return (state() == PLAY_ACTIVE || state() == PAUSE_ACTIVE);
	return (state() == LISTENING);
    }

    protected int state() {
	return state;
    }

    protected boolean isSessionActive(){
	return (sessionInfos.size() > 0);
    }

    protected void setState(int state) {
	prevState = this.state;
	this.state = state;
	logger.info("Previous state: "+prevState+" Current state: "+state);
	
	switch(state) {
	case CONFIGURING:
	    // disable UI
	    break;
	case LISTENING:
	    // enable UI
	    //sessionID = "";
	    break;
	case PLAY_ACTIVE:
	case PAUSE_ACTIVE:
	    // make changes in UI
	    break;
	}
    }

    class SessionInfo {
	String sessionID = null;
	String programID = null;
	PlayoutPreviewListenerCallback listener = null;
	boolean isStopping = false;

	SessionInfo(String sessionID, String programID, PlayoutPreviewListenerCallback listener){
	    this.sessionID = sessionID;
	    this.programID = programID;
	    this.listener  = listener;
	}

	public String getSessionID(){
	    return sessionID;
	}
	
	public void setSessionID(String sessionID){
	    this.sessionID = sessionID;
	}
	
	public String getProgramID(){
	    return programID;
	}

	public void setProgramID(String programID){
	    this.programID = programID;
	}

	public PlayoutPreviewListenerCallback getListener(){
	    return listener;
	}
	
	public void setListener(PlayoutPreviewListenerCallback listener){
	    this.listener = listener;
	}
	
	public void setStopping(){
	    isStopping = true;
	}

	public boolean isStopping(){
	    return isStopping;
	}

    }
}
