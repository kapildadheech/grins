package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.app.gui.ErrorInference;

import java.util.*;

public class MonitorProvider extends RSProviderBase {
    public static String SRC_PORT_ROLE = MonitorService.BCAST_MIC;
    public static String SINK_PORT_ROLE = MonitorService.PLAYOUT;

    protected final static int CONFIGURING = -1;
    protected final static int LISTENING = 0;
    protected final static int MONITOR_COMMAND_SENT = 1;
    protected final static int MONITORING = 2;
    protected final static int STOP_COMMAND_SENT = 3;

    protected final static String SUCCESS = "SUCCESS";
    protected final static String FAILURE = "FAILURE";

    protected int state = CONFIGURING;

    protected String monitorServiceInstanceName;
    protected boolean rscActive = true;
    protected boolean monitorServiceDisconnected = false;
    protected Hashtable<String, MonitorListenerCallback> listeners = null;
    
    protected StationConfiguration stationConfig;
    protected ErrorInference errorInference;

    protected final static int MAX_FREQUENCY_DISPERSION = 2;

    public MonitorProvider(UIService uiService) {
	super (uiService, RSApp.MONITOR_PROVIDER); 
	monitorServiceInstanceName = uiService.getProviderServiceInstance(RSApp.MONITOR_PROVIDER, MonitorService.MONITOR);
	listeners = new Hashtable<String, MonitorListenerCallback>();
	stationConfig = uiService.getStationConfiguration();
	errorInference = ErrorInference.getErrorInference(stationConfig);
    }

    
    public synchronized int activate() {
	// display UI. keep it disabled
	logger.info("Activate: Provider Activated");
	setState(CONFIGURING);
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), new String [] {monitorServiceInstanceName});
	
	int retval = sendCommand(message);
	if (retval < 0) {
	    logger.error("Activate: Send command failed.");
	    rscActive = false;
	}

	return retval;
    }

    public boolean isActive(){
	return (state() != CONFIGURING);
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listeners.put(listener.getName(), (MonitorListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	listeners.remove(listenerName);
    }


    public synchronized void receiveMessage(IPCMessage message){
	logger.debug("Received Message from: "+message.getSource()+" type: "+message.getMessageClass());
	if (message instanceof MonitorAckMessage) {

	    MonitorAckMessage ackMessage = (MonitorAckMessage)message;
	    handleMonitorAckMessage(ackMessage.getAck(), ackMessage.getCommand(), ackMessage.getError());

	} else if (message instanceof ServiceNotifyMessage) {

	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    boolean inactivate = false;
	    String[] error = null;
	    for (String service: notifyMessage.getInactivatedServices()) {
		if (service.equals(monitorServiceInstanceName)) {
		    logger.debug("ReceiveMessage: Got "+monitorServiceInstanceName+" disconnected.");
		    error = new String[]{RSController.ERROR_SERVICE_DISCONNECTED, monitorServiceInstanceName};
		    inactivate = true;
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController disconnected.");
		    error = new String[]{RSController.ERROR_SERVICE_DISCONNECTED, RSController.RESOURCE_MANAGER};
		    inactivate = true;
		}
	    }
	    if (inactivate && isActive()) {
		//VVV send error to inference engine
		monitorServiceDisconnected = true;
		for (MonitorListenerCallback listener: listeners.values()) {
			listener.activateMonitor(false, error);
		}
		return;
	    }
		
	    for (String service: notifyMessage.getActivatedServices()){
		if (service.equals(monitorServiceInstanceName)) {
		    if (monitorServiceDisconnected) {
			monitorServiceDisconnected = false;
		    } else {
			if (state() == CONFIGURING) {
			    setState(LISTENING);
			    startMonitor();
			}
		    }
		    for (MonitorListenerCallback listener: listeners.values())
			listener.activateMonitor(true, new String[]{});

		} else if (service.equals(RSController.RESOURCE_MANAGER)) {

		    logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing. Wait for lib service to be activated.
		    }

		}
	    }
	} else if (message instanceof ServiceAckMessage){
	    ServiceAckMessage serviceAckMessage = (ServiceAckMessage) message;
	    for (String service: serviceAckMessage.getActiveServices()){
		if (service.equals(monitorServiceInstanceName)) {
		    logger.info("Got monitor service active.");
		    if (monitorServiceDisconnected) {
			monitorServiceDisconnected = false;
		    } else {
			if (state() == CONFIGURING) {
			    setState(LISTENING);
			    startMonitor();		
			}	
		    }
		    for (MonitorListenerCallback listener: listeners.values())
			listener.activateMonitor(true, new String[]{});
		}
	    }
	    
	    for (String service: serviceAckMessage.getInactiveServices()){
		if (service.equals(monitorServiceInstanceName)) {
		    logger.info("Got monitor service inactive");
		    setState(CONFIGURING);
		    for (MonitorListenerCallback listener: listeners.values())
			listener.activateMonitor(false, new String[]{RSController.ERROR_SERVICE_DOWN, 
								     monitorServiceInstanceName});
		}
	    }
	} else if (message instanceof GstreamerErrorMessage){
	    GstreamerErrorMessage errorMessage = (GstreamerErrorMessage)message;
	    handleGstreamerErrorMessage(errorMessage.getError());
	} else if (message instanceof MonitorFrequencyMessage){
	    MonitorFrequencyMessage freqMessage = (MonitorFrequencyMessage) message;
	    handleFrequencyMessage(freqMessage.getAck(), freqMessage.getFrequencies());
	}
    }

    protected double average(String[] values){
	double sum = 0;
	for (String value: values)
	    sum += Double.parseDouble(value);
	return (sum /= values.length);
    }

    protected void handleFrequencyMessage(String ack, String[] frequencies){
	logger.debug("HandleFrequencyMessage: Received ack: "+ack+" :"+StringUtilities.concat(frequencies));
	
	boolean success = false;
	int testFrequency = stationConfig.getIntParam(StationConfiguration.MONITOR_TEST_FREQUENCY);

	if (ack.equals(MonitorService.SUCCESS)){
	    if (Math.abs(average(frequencies) - testFrequency) < MAX_FREQUENCY_DISPERSION)
		success = true;
	} else {
	    success = false;
	}

	for (MonitorListenerCallback listener: listeners.values()){
	    listener.monitorFrequencyTested(success);
	}
    }

    protected void handleGstreamerErrorMessage(String error){
	logger.error("HandleGstreamerErrorMessage: Received message: "+error+" from monitor service.");
	for (MonitorListenerCallback listener: listeners.values()) {
	    listener.monitorGstError(error);
	    //VVV inform of this error to inference engine also
	}
	errorInference.displayGstError("Unable to Monitor", error);
	setState(LISTENING);
    }

    protected void handleMonitorAckMessage (String ack, String command, String[] error){
	logger.info("HandleMonitorAck: " + ack + ":" + state);
	switch (state()){

	case MONITOR_COMMAND_SENT:
	    if (ack.equals(MonitorService.SUCCESS)){

		logger.info("HandleMonitorAckMessage: Started monitoring.");
		setState(MONITORING);
		for (MonitorListenerCallback listener: listeners.values())
		    listener.monitorStarted(true, error);

	    } else if (ack.equals(MonitorService.FAILURE)){

		logger.error("HandleMonitorAckMessage: Unable to monitor.");
		displayError("Unable to monitor.");
		setState(LISTENING);
		for (MonitorListenerCallback listener: listeners.values())
		    listener.monitorStarted(false, error);

	    } else if (ack.equals(MonitorService.WAITING)) {

		logger.info("HandleMonitorAckMessage: Persistent resource request registered. But resource is currently not available.");
		setState(MONITORING);
		for (MonitorListenerCallback listener: listeners.values())
		    listener.monitorStarted(true, error);

	    }
	    
	    break;

	case STOP_COMMAND_SENT:
	    if (ack.equals(MonitorService.SUCCESS)){
		logger.info("HandleMonitorAckMessage: Stopped monitoring.");
		setState(LISTENING);
		for (MonitorListenerCallback listener: listeners.values())
		    listener.monitorStopped(true, error);
	    } else {
		logger.error("HandleMonitorAckMessage: Unable to stop monitoring.");
		displayError("Unable to stop monitoring.");
		for (MonitorListenerCallback listener: listeners.values())
		    listener.monitorStopped(false, error);
	    }
	    
	    break;
	}
    }

    protected void displayError (String error){
	logger.error(error);
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: from: "+message.getSource()+" Error: " + message.getParam("serviceerror"));
	
	/*if (  message instanceof ServiceErrorRelayMessage && 
	   ((ServiceErrorRelayMessage)message).getErrorCode() == ServiceErrorRelayMessage.NOT_FOUND) {
	    setState(CONFIGURING);
	}
	else {
	    setState(LISTENING);
	    }*/

	String serviceName = null;
	String errorCode = null;
	String[] error = null;
	boolean inactivate = false;

	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}

	if (serviceName != null && serviceName.equals(monitorServiceInstanceName)) { 
	     logger.debug("ReceiveErrorMessage: Got "+monitorServiceInstanceName+" down.");
	     error = new String[]{RSController.ERROR_SERVICE_DOWN, monitorServiceInstanceName};
	     inactivate = true;
	} else if (serviceName != null && serviceName.equals(RSController.RESOURCE_MANAGER)) {
	    logger.debug("ReceiveMessage: Got RSController down.");
	    error = new String[]{RSController.ERROR_SERVICE_DOWN, RSController.RESOURCE_MANAGER};
	    rscActive = false;
	    inactivate = true;
	}
	
	if (inactivate && isActive()) {
	    //VVV send error to inference engine
	    setState(CONFIGURING);
	    monitorServiceDisconnected = false;
	    for (MonitorListenerCallback listener: listeners.values()) {
		listener.activateMonitor(false, error);
	    }
	    return;
	}
    }

    public synchronized int startMonitor() {
	if (state() != LISTENING){
	    logger.warn("StartMonitor: Cannot start since not in LISTENING state.");
	    return -1;
	}
	MonitorCommandMessage message = new MonitorCommandMessage("", monitorServiceInstanceName, getName(), 
								  MonitorService.START_MSG, SRC_PORT_ROLE,
								  SINK_PORT_ROLE, uiService.getLanguage());
	int retVal = sendCommand(message);
	if (retVal == 0) {
	    setState(MONITOR_COMMAND_SENT);
	} else {
	    setState(LISTENING);
	}

	return retVal;
    }

    public synchronized int startMonitorDiagnostics(){
	if (state() != LISTENING){
	    logger.warn("StartMonitorDiagnostics: Cannot start since not in LISTENING state.");
	    return -1;
	}
	MonitorCommandMessage message = new MonitorCommandMessage("", monitorServiceInstanceName, getName(), 
								  MonitorService.START_DIAGNOSTICS_MSG, SRC_PORT_ROLE,
								  SINK_PORT_ROLE, uiService.getLanguage());
	int retVal = sendCommand(message);
	if (retVal == 0) {
	    setState(MONITOR_COMMAND_SENT);
	} else {
	    setState(LISTENING);
	}
	
	return retVal;
	
    }

    public synchronized boolean isMonitoring() {
	return (state() == MONITORING);
    }

    public synchronized int stopMonitor() {
	if (state() != MONITORING){
	    logger.warn("StopMonitor: Cannot stop since not in MONITORING state");
	    return -1;
	}

	MonitorCommandMessage message = new MonitorCommandMessage("", monitorServiceInstanceName, getName(), 
								    MonitorService.STOP_MSG, SRC_PORT_ROLE,
								    SINK_PORT_ROLE, uiService.getLanguage());

	int retVal = sendCommand(message);
	if (retVal == 0){
	    setState(STOP_COMMAND_SENT);
	} else {
	    setState(LISTENING);
	}

	return retVal;
    }


    protected int state() {
	return state;
    }

    protected void setState(int state) {
	int previousState = this.state;
	this.state = state;
	logger.info("Previous state: "+previousState+" Current State: "+state);
    }
}
