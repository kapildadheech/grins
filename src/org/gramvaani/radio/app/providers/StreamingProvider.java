package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.gui.ErrorInference;

import org.gramvaani.simpleipc.IPCMessage;

import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class StreamingProvider extends RSProviderBase {
    
    
    String streamingServiceInstanceName;
    StationConfiguration stationConfig;
    ErrorInference errorInference;

    Hashtable<String, StreamingListener> listeners = new Hashtable<String, StreamingListener>();
    ExecutorService listenerExecutor;
    final static int POOL_THREAD_COUNT = 1;
    
    static final String DEFAULT_ROLE = "streaming";

    public StreamingProvider(UIService uiService){
	super(uiService, RSApp.STREAMING_PROVIDER);
	streamingServiceInstanceName = uiService.getProviderServiceInstance(RSApp.STREAMING_PROVIDER, DEFAULT_ROLE);
	stationConfig = uiService.getStationConfiguration();
	errorInference = ErrorInference.getErrorInference(stationConfig);

	listenerExecutor = Executors.newFixedThreadPool(POOL_THREAD_COUNT);
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    listenerExecutor.shutdown();
		}
	    });

    }

    public boolean isActive(){
	return (rsmActive && streamingActive);
    }

    public synchronized int activate() {
	logger.info("Activate: Provider Activated");
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), new String [] {streamingServiceInstanceName});
	
	int retval = sendCommand(message);
	if (retval < 0) {
	    logger.error("Activate: Send command failed.");
	}

	return retval;
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listeners.put(listener.getName(), (StreamingListener)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	listeners.remove(listenerName);
    }
    
    public void receiveErrorMessage(IPCMessage message){

    }

    public void receiveMessage(IPCMessage message){
	MessageHandlerUtility.handleMessage(this, message);
    }

    @MessageHandler
    void handleStreamingAckMessage(StreamingAckMessage msg){
	if (msg.getCommand().equals(StreamingCommandMessage.CMD_START))
	    streamingStarted(msg.getAck(), msg.getError());
	else if (msg.getCommand().equals(StreamingCommandMessage.CMD_STOP))
	    streamingStopped(msg.getAck(), msg.getError());
    }

    @MessageHandler
    void handleGstreamerErrorMessage(GstreamerErrorMessage msg){
	streamingGstError(msg.getError());
    }

    @MessageHandler
    void handleServiceNotifyMessage(ServiceNotifyMessage msg){
	handleServicesInactivated(msg.getInactivatedServices());
	handleServicesActivated(msg.getActivatedServices());
    }

    @MessageHandler
    void handleServiceAckMessage(ServiceAckMessage msg){
	handleServicesInactivated(msg.getInactiveServices());
	handleServicesActivated(msg.getActiveServices());
    }

    boolean rsmActive = false, streamingActive = false;
    void handleServicesInactivated(String[] inactivatedServices){
	for (String service: inactivatedServices){
	    if (service.equals(streamingServiceInstanceName))
		streamingActive = false;
	    else if (service.equals(RSController.RESOURCE_MANAGER))
		rsmActive = false;

	} 
	activateStreaming(rsmActive && streamingActive, new String[] {RSController.ERROR_SERVICE_DISCONNECTED, RSController.STREAMING_SERVICE});
    }

    void handleServicesActivated(String[] activatedServices){
	for (String service: activatedServices){
	    if (service.equals(streamingServiceInstanceName))
		streamingActive = true;
	    else if (service.equals(RSController.RESOURCE_MANAGER))
		rsmActive = true;

	}

	activateStreaming(rsmActive && streamingActive, new String[] {});
    }

    void activateStreaming(final boolean enabled, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (StreamingListener listener: listeners.values()){
			listener.activateStreaming(enabled, error);
		    }
		}
	    });
    }

    void streamingStarted(final boolean ack, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (StreamingListener listener: listeners.values()){
			listener.streamingStarted(ack, error);
		    }
		}
	    });
    }

    void streamingStopped(final boolean ack, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (StreamingListener listener: listeners.values()){
			listener.streamingStopped(ack, error);
		    }
		}
	    });
    }

    void streamingGstError(final String error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (StreamingListener listener: listeners.values())
			listener.streamingGstError(error);
		}
	    });
    }


    /* API to Controllers */

    public void startStreaming(Hashtable<String, String> options){
	sendStreamingCommand(StreamingCommandMessage.CMD_START, options);
    }

    public void updateStream(Hashtable<String, String> options){
	sendStreamingCommand(StreamingCommandMessage.CMD_UPDATE, options);
    }
    
    public void stopStreaming(){
	sendStreamingCommand(StreamingCommandMessage.CMD_STOP);
    }

    boolean sendStreamingCommand(String command){
	return sendStreamingCommand(command, null);
    }

    boolean sendStreamingCommand(String command, Hashtable<String, String> options){
	StreamingCommandMessage message = new StreamingCommandMessage("", streamingServiceInstanceName, 
								      getName(), command, options);
	
	int retVal = sendCommand(message);
	if (retVal == 0) {
	    logger.info("SendStreamingCommand: Command sent:" + command);
	    return true;
	} else {
	    logger.error("SendStreamingCommand: Failed to send command:" + command);
	    return false;
	}

    }

    /* End of API */
}
