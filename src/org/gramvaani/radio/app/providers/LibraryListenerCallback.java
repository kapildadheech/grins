package org.gramvaani.radio.app.providers;

public interface LibraryListenerCallback extends ListenerCallback {

    public void activateSearch(boolean success, String[] error);
    public void diskSpaceChecked(String machineIP, String dirName, long usedSpace, long availableSpace);
}