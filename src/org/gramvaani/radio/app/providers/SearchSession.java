package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.gui.StatsDisplay;

import java.util.*;

public class SearchSession {
    protected MediaLib medialib;
    protected LibConnectionPool connectionPool;
    protected LogUtilities logger;
    protected String sessionID;

    protected StationConfiguration stationConfiguration;
    protected TimeKeeper timeKeeper;
    protected LibraryProvider libraryProvider;

    protected boolean active = false;
    protected MetaSearchFilter metaSearchFilter;

    public SearchSession(StationConfiguration stationConfiguration, TimeKeeper timeKeeper, LibConnectionPool connectionPool,
			 LibraryProvider libraryProvider, CacheProvider cacheProvider, String sessionID) {
	this.timeKeeper = timeKeeper;
	this.stationConfiguration = stationConfiguration;
	this.sessionID = sessionID;
	this.libraryProvider = libraryProvider;
	this.connectionPool = connectionPool;
	this.medialib = connectionPool.getConnectionFromPool();
	logger = new LogUtilities("SearchSession -- " + sessionID);
	if(medialib != null) {
	    active = true;
	    metaSearchFilter = new MetaSearchFilter(medialib, libraryProvider, cacheProvider, logger);
	}
    }

    public boolean isActive() {
	return active;
    }

    public void close() {
	connectionPool.returnConnectionToPool(medialib);
	medialib = null;
    }

    public SearchFilter getFilter(String filterName) {
	return metaSearchFilter.getFilter(filterName);
    }

    public SearchFilter getCategoryFilter(String filterName) {
	return metaSearchFilter.getCategoryFilter(filterName);
    }

    public SearchFilter[] getAllFilters() {
	return metaSearchFilter.getAllFilters();
    }

    public SearchFilterThin[] getAllCategoryFilters() {
	return metaSearchFilter.getAllCategoryFilters();
    }

    public SearchFilter[] getDefaultDisplayFilters() {
	return metaSearchFilter.getDefaultDisplayFilters();
    }

    public void displayFilter(SearchFilter displayFilter) {
	metaSearchFilter.displayFilter(displayFilter);
    }

    public void removeProgramFromSearchResult(String itemID) {
	metaSearchFilter.removeProgramFromSearchResult(itemID);
    }

    public SearchResult activateFilter(SearchFilter activeFilter) {
	metaSearchFilter.activateFilter(activeFilter);
	metaSearchFilter.rerun();
	SearchResult searchResult = new SearchResult(metaSearchFilter.getDisplayFilters(),
						     metaSearchFilter.getActiveFilters(),
						     metaSearchFilter.getPrograms(),
						     metaSearchFilter.numSearchResults());
	return searchResult;
    }

    public SearchResult deactivateFilter(SearchFilter inactiveFilter) {
	metaSearchFilter.deactivateFilter(inactiveFilter);
	metaSearchFilter.rerun();
	SearchResult searchResult = new SearchResult(metaSearchFilter.getDisplayFilters(),
						     metaSearchFilter.getActiveFilters(),
						     metaSearchFilter.getPrograms(),
						     metaSearchFilter.numSearchResults());
	return searchResult;
    }
    
    public GraphStats getBarGraphStats(long minTelecastTime, long maxTelecastTime) {
	return metaSearchFilter.getGraphStats(minTelecastTime, maxTelecastTime, StatsDisplay.BAR_GRAPH);
    }

    public GraphStats getPointGraphStats(long minTelecastTime, long maxTelecastTime) {
	return metaSearchFilter.getGraphStats(minTelecastTime, maxTelecastTime, StatsDisplay.POINT_GRAPH);
    }

    public SearchFilter[] getActiveFilters() {
	return metaSearchFilter.getActiveFilters();
    }

    public SearchResult textSearch(int goBackN) {
	metaSearchFilter.inferRelevantFilters(goBackN);
	metaSearchFilter.rerun();
	SearchResult searchResult = new SearchResult(metaSearchFilter.getDisplayFilters(),
						     metaSearchFilter.getActiveFilters(),
						     metaSearchFilter.getPrograms(),
						     metaSearchFilter.numSearchResults());
	return searchResult;	
    }

    public SearchResult textSearch(String text) {
	metaSearchFilter.inferRelevantFilters(text);
	// XXX not sure if this should be allowed only if no filters are already active
	//	    if(metaSearchFilter.numActiveFilters() == 0)
	//		metaSearchFilter.inferActiveFilters();
	metaSearchFilter.rerun();
	SearchResult searchResult = new SearchResult(metaSearchFilter.getDisplayFilters(),
						     metaSearchFilter.getActiveFilters(),
						     metaSearchFilter.getPrograms(),
						     metaSearchFilter.numSearchResults());
	return searchResult;
    }
    
    public SearchResult getAllResults() {
	SearchResult searchResult = new SearchResult(metaSearchFilter.getDisplayFilters(),
						     metaSearchFilter.getActiveFilters(),
						     metaSearchFilter.getAllPrograms(),
						     metaSearchFilter.numSearchResults());
	return searchResult;
    }

    public SearchResult getMoreResults() {
	if(metaSearchFilter.hasMoreResults()) {
	    metaSearchFilter.getMoreResults();
	}

	SearchResult searchResult = new SearchResult(metaSearchFilter.getDisplayFilters(),
						     metaSearchFilter.getActiveFilters(),
						     metaSearchFilter.getPrograms(),
						     metaSearchFilter.numSearchResults());
	return searchResult;
    }

    public boolean hasMoreResults() {
	return metaSearchFilter.hasMoreResults();
    }

    public int getSearchOffset() {
	return metaSearchFilter.getSearchOffset();
    }

}