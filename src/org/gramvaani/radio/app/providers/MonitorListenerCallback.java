package org.gramvaani.radio.app.providers;

public interface MonitorListenerCallback extends ListenerCallback {

    public void activateMonitor(boolean activate, String[] error);
    public void monitorGstError(String error);
    public void monitorStarted(boolean success, String[] error);
    public void monitorStopped(boolean success, String[] error);
    public void monitorFrequencyTested(boolean success);
}