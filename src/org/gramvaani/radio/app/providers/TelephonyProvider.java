package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.telephonylib.*;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class TelephonyProvider extends RSProviderBase {
        
    public final static String SUCCESS = "SUCCESS";
    public final static String FAILURE = "FAILURE";

    //provider states
    protected final static int CONFIGURING          = -1;
    protected final static int LISTENING            = 0;

    //online-offline states
    protected final static int ONLINE_COMMAND_SENT  = 1;
    protected final static int ONLINE               = 2;
    protected final static int OFFLINE_COMMAND_SENT = 3;
    protected final static int OFFLINE              = 4;

    //call states
    protected final static int DIALING              = 5;
    protected final static int RINGING              = 6;
    protected final static int ACCEPT_COMMAND_SENT  = 7;
    protected final static int HANGUP_COMMAND_SENT  = 8;
    protected final static int DIAL_COMMAND_SENT    = 9;
    protected final static int CONNECTED            = 10;

    //on air off air states
    protected final static int ONAIR_COMMAND_SENT   = 11;
    protected final static int ONAIR                = 12;
    protected final static int OFFAIR_COMMAND_SENT  = 13;
    protected final static int OFFAIR               = 14;

    protected int state = CONFIGURING;
    protected int onlineOfflineState = OFFLINE;

    protected String onlineTelephonyServiceInstanceName;
    protected boolean rscActive = true;
    protected boolean onlineTelephonyDisconnected = true;
    protected String onlineTelephonyServiceID = "";
    protected Hashtable<String, TelephonyListenerCallback> listeners = null;
    protected Hashtable<String, TelephoneCall> callList = null;

    protected ExecutorService listenerExecutor;
    protected final static int POOL_THREAD_COUNT = 1;
    long lastMessageTime = 0;
    final static int MIN_MESSAGE_INTERVAL = 1000;

    public TelephonyProvider(UIService uiService) {
	super (uiService, RSApp.TELEPHONY_PROVIDER); 
	onlineTelephonyServiceInstanceName = uiService.getProviderServiceInstance(RSApp.TELEPHONY_PROVIDER, OnlineTelephonyService.ONLINE_TELEPHONY);
	listeners = new Hashtable<String, TelephonyListenerCallback>();
	callList = new Hashtable<String, TelephoneCall>();

	listenerExecutor = Executors.newFixedThreadPool(POOL_THREAD_COUNT);
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    listenerExecutor.shutdown();
		}
	    });

    }

    
    public synchronized int activate() {
	// display UI. keep it disabled
	logger.info("Activate: Provider Activated");
	setState(CONFIGURING);
	
	//three lines below may not be needed, but does not hurt to have them
	setOnlineOfflineState(OFFLINE);
	callList.clear();

	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String[] {onlineTelephonyServiceInstanceName});

	int retVal = sendCommand(message);
	if (retVal != 0)
	    rscActive = false;

	return retVal;
    }

    public boolean isActive(){
	return (state() != CONFIGURING);
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	if (listener == null) {
	    logger.error("RegisterWithProvider: null listener.");
	    return;
	}
	listeners.put(listener.getName(), (TelephonyListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	if (listenerName == null) {
	    logger.error("UnregisterWithProvider: null listenerName.");
	    return;
	}
	listeners.remove(listenerName);
    }

    public synchronized void receiveMessage(IPCMessage message){
	logger.debug("Received Message from: "+message.getSource()+" type: "+message.getMessageClass());

	if (message instanceof TelephonyAckMessage) {
	    TelephonyAckMessage ackMessage = (TelephonyAckMessage)message;
	    handleTelephonyAckMessage(ackMessage.getAck(), ackMessage.getCommand(), ackMessage.getOptions(), ackMessage.getError());

	} else if (message instanceof TelephonyEventMessage) {
	    TelephonyEventMessage eventMessage = (TelephonyEventMessage)message;
	    handleTelephonyEventMessage(message.getSource(), eventMessage.getEvent(), eventMessage.getOptions());
	    
	} else if (message instanceof TelephonyInfoMessage) {
	    TelephonyInfoMessage infoMessage = (TelephonyInfoMessage)message;
	    handleTelephonyInfoMessage(infoMessage.getCalls(), 
				       infoMessage.isConferenceOn(),
				       infoMessage.getOnairCall(), infoMessage.getOffairCall());

	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    boolean inactivate = false;
	    String disconnectedService = "";
	    for (String service: notifyMessage.getInactivatedServices()) {
		if (service.equals(onlineTelephonyServiceInstanceName)) {
		    disconnectedService = service;
		    onlineTelephonyDisconnected = true;
		    inactivate = true;
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    disconnectedService = service;
		    inactivate = true;
		}
	    }

	    if (inactivate) {
		logger.debug("ReceiveMessage: Found "+disconnectedService+" disconnected");

		String[] error;
		error = new String[]{RSController.ERROR_SERVICE_DISCONNECTED, disconnectedService};
		
		enableTelephonyUI(false, error);
	    }
	    
	    for (String service: notifyMessage.getActivatedServices()){
		if (service.equals(onlineTelephonyServiceInstanceName)) {
		    if (!onlineTelephonyDisconnected) {
			setState(LISTENING);
			activateTelephony(true, new String[]{});
		    } else {
			onlineTelephonyDisconnected = false;
			enableTelephonyUI(true, new String[]{});
		    }
		} else if (service.equals(RSController.RESOURCE_MANAGER)){
		     logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing
		    }
		}
	    }

	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();

	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(onlineTelephonyServiceInstanceName))
		    onlineTelephonyServiceConnected(activatedIDs[i]);
	    }

	} else if (message instanceof ServiceAckMessage){
	    ServiceAckMessage serviceAckMessage = (ServiceAckMessage) message;
	    String[] activeServices = serviceAckMessage.getActiveServices();
	    String[] activeIDs = serviceAckMessage.getActiveServiceIDs();
	    for (int i = 0; i < activeServices.length; i++) {
		String service = activeServices[i];
		if (service.equals(onlineTelephonyServiceInstanceName)) {
		    onlineTelephonyDisconnected = false;
		    setState(LISTENING);
		    onlineTelephonyServiceConnected(activeIDs[i]);
		    activateTelephony(true, new String[]{});
		}
	    }

	    for (String service: serviceAckMessage.getInactiveServices()){
		if (service.equals(onlineTelephonyServiceInstanceName)) {
		    onlineTelephonyDisconnected = false;
		    callList.clear();
		    setOnlineOfflineState(OFFLINE);
		    setState(CONFIGURING);

		    activateTelephony(false, new String[]{RSController.ERROR_SERVICE_DOWN,
						    service});
		}
	    }
	} else if (message instanceof StatusCheckResponseMessage) {
	    StatusCheckResponseMessage responseMessage = (StatusCheckResponseMessage) message;
	    handleStatusCheckResponseMessage(responseMessage.getResponse(), responseMessage.getOptions());
	}

    }

    /* Calls coming in from the UI*/

    public synchronized boolean disconnect() {
	return sendTelephonyCommand(TelephonyCommandMessage.DISCONNECT);
    }

    public synchronized boolean enableConferenceMode(boolean enable){
	if (enable)
	    return sendTelephonyCommand(TelephonyCommandMessage.ENABLE_CONF);
	else
	    return sendTelephonyCommand(TelephonyCommandMessage.DISABLE_CONF);
    }
    
    public synchronized boolean goOnAir(boolean isConference) {

	if (!canGoOnAir()) {
	    logger.error("GoOnAir: Attempting to go on air in wrong state.");
	    return false;
	}

	Hashtable<String, String> options = new Hashtable<String,String>();
	if (isConference) 
	    options.put(TelephonyCommandMessage.IS_CONFERENCE, TelephonyCommandMessage.TRUE);
	else
	    options.put(TelephonyCommandMessage.IS_CONFERENCE, TelephonyCommandMessage.FALSE);

	
	if (sendTelephonyCommand(TelephonyCommandMessage.GO_ONAIR, options)) {
	    return true;
	} else {
	    return false;
	}
    }

    public synchronized boolean goOffAir(boolean isConference) {
	Hashtable<String, String> options = new Hashtable<String,String>();
	if (isConference) 
	    options.put(TelephonyCommandMessage.IS_CONFERENCE, TelephonyCommandMessage.TRUE);
	else
	    options.put(TelephonyCommandMessage.IS_CONFERENCE, TelephonyCommandMessage.FALSE);
	
	if (sendTelephonyCommand(TelephonyCommandMessage.GO_OFFAIR, options)) {
	    return true;
	} else {
	    return false;
	}
    }

    public synchronized boolean goOnline() {
	if (state != LISTENING){
	    logger.error("GoOnline: Attempting to go online in wrong state. state:" + state + " onlineOfflineState:" + onlineOfflineState);
	    return false;
	}

	if (sendTelephonyCommand(TelephonyCommandMessage.GO_ONLINE)) {
	    setOnlineOfflineState(ONLINE_COMMAND_SENT);
	    return true;
	} else {
	    return false;
	}
    }

    public synchronized boolean goOffline() {
	if (onlineOfflineState == OFFLINE || state == CONFIGURING) {
	    logger.error("GoOffline: Attempting to go online in wrong state. state:" + state + " onlineOfflineState:" + onlineOfflineState);
	    return false;
	}

	if (sendTelephonyCommand(TelephonyCommandMessage.GO_OFFLINE)) {
	    setOnlineOfflineState(OFFLINE_COMMAND_SENT);
	    return true;
	} else {
	    return false;
	}
    }
    
    public void getDevicePortStatus() {
	
    }

    public synchronized boolean dial(RSCallerID extension) {
	if (state == CONFIGURING) {
	    logger.error("Dial: Attempting to make a call in wrong state. state:" + state);
	    return false;
	}

	if (extension == null || extension.toString().equals("")){
	    logger.error("Dial: Extension: "+extension);
	    return false;
	}

	String guid = GUIDUtils.getGUID();

	Hashtable<String,String> options = new Hashtable<String,String>();
	options.put(TelephonyCommandMessage.CALLER_ID, extension.toString());
	options.put(TelephonyCommandMessage.TEMP_CALL_GUID, guid);

	if (sendTelephonyCommand(TelephonyCommandMessage.DIAL, options)) {
	    TelephoneCall phoneCall = new TelephoneCall(guid, extension, false);
	    callList.put(guid, phoneCall);
	    return true;
	} else {
	    return false;
	}
    }

    public synchronized boolean acceptCall(String callGUID) {
	if (onlineOfflineState != ONLINE) {
	    logger.error("AcceptCall: Trying to accept call " + callGUID + " in wrong onlineOfflineState: " + onlineOfflineState);
	    return false;
	}
	
	if (callGUID == null || callGUID.equals("")) {
	    logger.error("AcceptCall: null callguid.");
	    return false;
	}

	TelephoneCall phoneCall = callList.get(callGUID);

	if (phoneCall == null) {
	    logger.error("AcceptCall: callGUID:" + callGUID + " not found in callList.");
	    return false;
	}

	logger.info("AcceptCall: Accept call: "+callGUID);

	Hashtable<String,String> options = new Hashtable<String,String>();
	options.put(TelephonyCommandMessage.CALL_GUID, callGUID);

	return sendTelephonyCommand(TelephonyCommandMessage.ACCEPT_CALL, options);
    }

    public synchronized boolean previewCall(String callGUID) {
	//XXX: state checking

	if (state == CONFIGURING) {
	    logger.error("PreviewCall: Trying to previewCall "+callGUID+" in state: "+state);
	    return false;
	}

	if (callGUID == null || callGUID.equals("")){
	    logger.error("PreviewCall: null callGUID.");
	    return false;
	}
	
	//TelephoneCall phoneCall = callList.get(callGUID);
	//XXX: review the use of TelephoneCall to maintain state here.

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(TelephonyCommandMessage.CALL_GUID, callGUID);
	
	if (sendTelephonyCommand(TelephonyCommandMessage.PREVIEW_CALL, options)) {
	    return true;
	} else {
	    logger.error("PreviewCall: Unable to send previewCall command.");
	    return false;
	}
    }

    public synchronized boolean holdCall(String callGUID) {
	if (state == CONFIGURING) {
	    logger.error("HoldCall: Trying to hold call "+callGUID +" in wrong state. state: "+state);
	    return false;
	}

	if (callGUID == null || callGUID.equals("")){
	    logger.error("HoldCall: null callGUID.");
	    return false;
	}

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(TelephonyCommandMessage.CALL_GUID, callGUID);

	if (sendTelephonyCommand(TelephonyCommandMessage.HOLD_CALL, options)) {
	    return true;
	} else {
	    logger.error("HoldCall: Unable to send holdCall command.");
	    return false;
	}
    }

    public synchronized boolean hangupCall(String callGUID) {
	if (state == CONFIGURING) {
	   logger.error("HangupCall: Trying to hangup call " + callGUID + " in wrong state. state: " + state + " onlineOfflineState:" + onlineOfflineState);
	   return false; 
	}

	if (callGUID == null || callGUID.equals("")) {
	    logger.error("HangupCall: null call guid.");
	    return false;
	}

	//System.err.println("calllist: "+callList);

	TelephoneCall phoneCall = callList.get(callGUID);

	if (phoneCall == null) {
	    logger.error("HangupCall: callGUID:" + callGUID + " not found in callList.");
	    return false;
	}

	Hashtable<String,String> options = new Hashtable<String,String>();
	options.put(TelephonyCommandMessage.CALL_GUID, callGUID);

	return sendTelephonyCommand(TelephonyCommandMessage.HANGUP_CALL, options);
    }

    public synchronized boolean muteCall(String callGUID){
	if (state == CONFIGURING){
	    logger.error("MuteCall: Trying to hangup call "+callGUID +" in wrong state. state: "+state+" onlineOfflineState: "+onlineOfflineState);
	    return false;
	}

	if (callGUID == null || callGUID.equals("")){
	    logger.error("MuteCall: null callGUID");
	    return false;
	}

	Hashtable<String,String> options = new Hashtable<String,String>();
	options.put(TelephonyCommandMessage.CALL_GUID, callGUID);

	if (sendTelephonyCommand(TelephonyCommandMessage.MUTE_CALL, options)) {
	    return true;
	} else {
	    return false;
	}

    }
    

    public synchronized boolean unmuteCall(String callGUID){
	if (state == CONFIGURING){
	    logger.error("UnmuteCall: Trying to hangup call "+callGUID +" in wrong state. state: "+state+" onlineOfflineState: "+onlineOfflineState);
	    return false;
	}

	if (callGUID == null || callGUID.equals("")){
	    logger.error("UnmuteCall: null callGUID");
	    return false;
	}

	Hashtable<String,String> options = new Hashtable<String,String>();
	options.put(TelephonyCommandMessage.CALL_GUID, callGUID);

	if (sendTelephonyCommand(TelephonyCommandMessage.UNMUTE_CALL, options)) {
	    return true;
	} else {
	    return false;
	}

    }

    public synchronized boolean checkStatus(){
	StatusCheckRequestMessage checkMessage = new StatusCheckRequestMessage("", onlineTelephonyServiceInstanceName, getName(), "");
	int retVal = sendCommand(checkMessage);

	if (retVal == 0){
	    logger.info("CheckStatus: command sent.");
	    return true;
	} else {
	    logger.error("CheckStatus: Unable to send command.");
	    return false;
	}
    }
    
    /* Calls from UI end here*/

    protected boolean sendTelephonyCommand(String command) {
	return sendTelephonyCommand(command, null);
    }

    protected boolean sendTelephonyCommand(String command, Hashtable<String,String> options) {

	if (command == null || command.equals("")) {
	    logger.error("SendTelephonyCommand: null command.");
	    return false;
	}
	
	long currentTime = System.currentTimeMillis();

	if ((currentTime - lastMessageTime) < MIN_MESSAGE_INTERVAL){
	    try {
		long sleepTime = MIN_MESSAGE_INTERVAL - (currentTime - lastMessageTime);
		Thread.sleep(sleepTime);
	    } catch (Exception e){}
	}
	
	TelephonyCommandMessage message = new TelephonyCommandMessage("", onlineTelephonyServiceInstanceName,
								      getName(), command, options);

	int retVal = sendCommand(message);
	if (retVal == 0) {
	    logger.info("SendTelephonyCommand: Command sent:" + command);
	    lastMessageTime = currentTime;
	    return true;
	} else {
	    logger.error("SendTelephonyCommand: Failed to send command:" + command);
	    return false;
	}
    }

    protected void onlineTelephonyServiceConnected(String newID) {
	logger.info("TelephonyServiceConnected with ID: "+newID + " oldID=" + onlineTelephonyServiceID);
		
	if (!onlineTelephonyServiceID.equals("") && !onlineTelephonyServiceID.equals(newID)) {
	    sendTelephonyCommand(TelephonyCommandMessage.DESTROY_ASTERISK_CHANNELS);
	    activateTelephony(false, new String[]{RSController.ERROR_SERVICE_DOWN, onlineTelephonyServiceInstanceName});
	    activateTelephony(true,  new String[]{});
	}
	
	onlineTelephonyServiceID = newID;

    }

    /* Callbacks to listeners */
    protected void activateTelephony(final boolean activate, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.activateTelephony(activate, error);
		}
	    });
    }

    protected void enableTelephonyUI(final boolean enable, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.enableTelephonyUI(enable, error);
		}
	    });
    }

    
    protected void telephonyOnline(final boolean success, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.telephonyOnline(success, error);
		}
	    });
    }

    protected void telephonyOffline(final boolean success, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run(){
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.telephonyOffline(success, error);
		}
	    });
    }

    protected void newCall(final String callGUID, final RSCallerID callerID) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.newCall(callGUID, callerID);
		}
	    });
    }
    
    protected void callerHungup(final String callGUID, final String reason) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.callerHungup(callGUID, reason);
		}
	    });
    }

    protected void callHungup(final boolean success, final String callGUID, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.callHungup(success, callGUID, error);
		}
	    });
    }

    protected void callAccepted(final boolean success, final String callGUID, final String programID, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.callAccepted(success, callGUID, programID, error);
		}
	    });
    }

    protected void callOnAir(final boolean success, final String programID, final long telecastTime, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.callOnAir(success, programID, telecastTime, error);
		}
	    });
    }

    protected void callOffAir(final boolean success, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.callOffAir(success, error);
		}
	    });
    }

    protected void callDialed(final boolean success, final String extension, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.callDialed(success, extension, error);
		}
	    });
    }

    protected void confEnabled(final boolean success, final String monitorFile, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.confEnabled(success, monitorFile, error);
		}
	    });
    }

    protected void confDisabled(final boolean success, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.confDisabled(success, error);
		}
	    });
    }
    
    protected void updateOutgoingCall(final String callGUID, final String newState, final String... options) {
	listenerExecutor.execute(new Runnable(){
		public void run() {
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.updateOutgoingCall(callGUID, newState, options);
		}
	    });
    }

    protected void telephonyStatusUpdate(final boolean status, final String[] error, final Hashtable<String, String> options){
	String[] lineIDs = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.PHONE_LINE_OK));
	for (String id : lineIDs) {
	    lineIDUpdate(id, SUCCESS);
	}

	lineIDs = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.PHONE_LINE_ERROR));
	for (String id : lineIDs) {
	    lineIDUpdate(id, FAILURE);
	}

	//pbxStatus should be sent after all lineidupdates
	//diagnosticscontrollet uses this as a triger to notify the thread waiting for status response
	pbxStatusUpdate(status, error);

	/*
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.telephonyStatusUpdate(status, error, options);
		}
	    });
	*/
    }

    void pbxStatusUpdate(final boolean status, final String[] error) {
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.pbxStatusUpdate(status, error);
		}
	    });
    }

    protected void lineIDUpdate(final String devicePortName, final String status) {
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (TelephonyListenerCallback listener: listeners.values())
			listener.lineIDUpdate(devicePortName, status);
		}
	    });
    }

    /* Callbacks to listeners end here*/

    protected void handleStatusCheckResponseMessage(String response, Hashtable<String, String> options){
	if (response.equals(StatusCheckResponseMessage.TRUE))
	    telephonyStatusUpdate(true, new String[]{}, options);
	else
	    telephonyStatusUpdate(false, new String[] {response}, options);
    }

    protected void handleTelephonyInfoMessage(Hashtable<String,String> callsAtService, 
					      boolean isConferenceOn,
					      String onairCall, String offairCall) {
	HashSet<Hashtable<String,String>> newCalls = new HashSet<Hashtable<String,String>>();
	ArrayList<String> callsToRemove = new ArrayList<String>();

	for (String guid: callList.keySet()) {
	    if (callsAtService.get(guid) == null) {
		callsToRemove.add(guid);
		callerHungup(guid, "");
	    }
	}
	
	for (String guid : callsToRemove)
	    callList.remove(guid);

	for (String guid: callsAtService.keySet()) {
	    if (callList.get(guid) == null) {
		Hashtable<String,String> options = new Hashtable<String,String>();
		options.put(TelephonyEventMessage.CALL_GUID, guid);
		options.put(TelephonyEventMessage.CALLER_ID, callsAtService.get(guid));
		newCalls.add(options);
	    }
	}
	
	for (Hashtable<String,String> options : newCalls) {
	    handleTelephonyEventMessage("", TelephonyEventMessage.NEW_CALL, options);
	}
    }

    protected void handleTelephonyAckMessage(String ack, String command, Hashtable<String,String> options, String[] error) {
	logger.info("HandleTelephonyAckMessage: command:" + command + " ack:" + ack);
	String callGUID = options.get(TelephonyCommandMessage.CALL_GUID);

	if (command.equals(TelephonyCommandMessage.ACCEPT_CALL)) {
	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		TelephoneCall phoneCall = callList.get(callGUID);
		String monitorFile = options.get(TelephonyCommandMessage.MONITOR_FILE_NAME);
		if (phoneCall == null) {
		    logger.error("HandleTelephonyAckMessage: Could not find callGUID:" + callGUID + " in callList.");
		}
		callAccepted(true, callGUID, monitorFile, error);
	    } else {
		logger.error("TelephonyAckMessage: Accept call failed for : " + callGUID + ". Error: " + error[0]);
		TelephoneCall phoneCall = callList.get(callGUID);
		callAccepted(false, callGUID, null, error);
	    }
	} else if (command.equals(TelephonyCommandMessage.HANGUP_CALL)) {

	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		callList.remove(callGUID);
		callHungup(true, callGUID, error);
	    } else {
		logger.error("TelephonyAckMessage: Hangup call failed for : " + callGUID + ". Error: " + error[0]);
		callHungup(false, callGUID, error);
	    }

	} else if (command.equals(TelephonyCommandMessage.GO_ONLINE)) {

	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		setOnlineOfflineState(ONLINE);
		telephonyOnline(true, error);
	    } else {
		setOnlineOfflineState(OFFLINE);
		telephonyOnline(false, error);
	    }

	} else if (command.equals(TelephonyCommandMessage.GO_OFFLINE)) {

	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		setOnlineOfflineState(OFFLINE);
		//clearIncomingCalls();
		telephonyOffline(true, error);
	    } else {
		setOnlineOfflineState(ONLINE);
		telephonyOffline(false, error);
	    }

	} else if (command.equals(TelephonyCommandMessage.GO_ONAIR)) {

	    boolean isConference;
	    String programID = options.get(TelephonyAckMessage.PROGRAM_ID);
	    long telecastTime = 0;

	    try {
		telecastTime = Long.parseLong(options.get(TelephonyAckMessage.TELECAST_TIME));
	    } catch (NumberFormatException nfe) {
		logger.error("NumberFormatException: " + nfe.getMessage() + ":no telecast time");
		telecastTime = 0;
	    }

	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		callOnAir(true, programID, telecastTime, error);
	    } else {
		callOnAir(false, programID, telecastTime, error);
	    }

	} else if (command.equals(TelephonyCommandMessage.GO_OFFAIR)) {

	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		callOffAir(true, error);
	    } else {
		callOffAir(false, error);
	    }

	} else if (command.equals(TelephonyCommandMessage.DIAL)) {
	    String callerID = options.get(TelephonyCommandMessage.CALLER_ID);

	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		callDialed(true, callerID, error);
	    } else {
		callDialed(false, callerID, error);
	    }
	} else if (command.equals(TelephonyCommandMessage.ENABLE_CONF)) {
	    String confMonitorFile = options.get(TelephonyCommandMessage.MONITOR_FILE_NAME);
	    
	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		confEnabled(true, confMonitorFile, error);
	    } else {
		confEnabled(false, confMonitorFile, error);
	    }
	} else if (command.equals(TelephonyCommandMessage.DISABLE_CONF)) {
	    if (ack.equals(TelephonyAckMessage.TRUE)) {
		confDisabled(true, error);
	    } else {
		confDisabled(false, error);
	    }
	}
    }

    protected void handleTelephonyEventMessage (String source, String event, Hashtable<String,String> options) {
	String callGUID = options.get(TelephonyEventMessage.CALL_GUID);
	String monitorFile = null;
	logger.info("HandleTelephonyEventMessage: event:" + event + " callGUID:" + callGUID);

	if (event.equals(TelephonyEventMessage.CALLER_HUNG_UP)) {
	    if (callList.get(callGUID) != null) {
		callList.remove(callGUID);
		callerHungup(callGUID, options.get(TelephonyEventMessage.HANGUP_CAUSE));
	    }

	} else if (event.equals(TelephonyEventMessage.NEW_CALL)) {
	    if (state == CONFIGURING || onlineOfflineState == OFFLINE) {
		logger.error("HandleTelephonyEventMessage: New call received in wrong state. state:" + state + " onlineOfflineState:" + onlineOfflineState);
		return;
	    }

	    RSCallerID callerID = new RSCallerID(options.get(TelephonyEventMessage.CALLER_ID));

	    logger.info("HandleTelephonyEventMessage: New call: " + callGUID + ", " + callerID);
	    TelephoneCall phoneCall = new TelephoneCall(callGUID, callerID, true);
	    callList.put(callGUID, phoneCall);
	    
	    newCall(callGUID, callerID);

	} else if (event.equals(TelephonyEventMessage.OUTGOING_CALL_UPDATE)) {
	    if (state == CONFIGURING) {
		logger.error("HandleTelephonyEventMessage: Outgoing call update received in wrong state. state:" + state + " onlineOfflineState:" + onlineOfflineState);
		return;
	    }

	    String callState = options.get(TelephonyEventMessage.CALL_STATE);
	    logger.info("HandleTelephonyEventMessage: Outgoing call update. callGUID:" + callGUID + " state:" + callState);
	    
	    if (callState.equals(TelephonyLib.DIALING)) {
		String tempCallGUID = options.get(TelephonyEventMessage.TEMP_CALL_GUID);
		TelephoneCall phoneCall = callList.remove(tempCallGUID);
		if (phoneCall == null) {
		    logger.error("HandleTelephonyEventMessage: Got dialing state for an unknown call. callguid:" + callGUID);
		    return;
		}

		phoneCall.setCallGUID(callGUID);
		callList.put(callGUID, phoneCall);
		
		//XXX update controller about this
	    } else if (callState.equals(TelephonyLib.RINGING)) {
		TelephoneCall phoneCall = callList.get(callGUID);
		if (phoneCall == null) {
		    logger.error("HandleTelephonyEventMessage: Got ringing state for an unknown call. callGUID:" + callGUID);
		    return;
		}

		//XXX update controller about this
	    } else if (callState.equals(TelephonyLib.SUCCESS)) {
		TelephoneCall phoneCall = callList.get(callGUID);
		monitorFile = options.get(TelephonyEventMessage.MONITOR_FILE_NAME);
		if (phoneCall == null) {
		    logger.error("HandleTelephonyEventMessage: Got success state for an unknown call. callGUID:" + callGUID);
		    return;
		}

		//XXX update controller about this
	    } else if (callState.equals(TelephonyLib.BUSY)) {
		TelephoneCall phoneCall = callList.remove(callGUID);
		if (phoneCall == null) {
		    logger.warn("HandleTelephonyEventMessage: Got busy state for an unknown call. callGUID:" + callGUID);
		    return;
		}

		logger.error("HandleTelephonyEventMessage: Remote end is busy:" + phoneCall.getCallerID());

		//XXX update controller about this
	    } else if (callState.equals(TelephonyLib.NOANSWER)) {
		TelephoneCall phoneCall = callList.remove(callGUID);
		if (phoneCall == null) {
		    logger.warn("HandleTelephonyEventMessage: Got no-answer state for an unknown call. callGUID:" + callGUID);
		    return;
		}

		logger.error("HandleTelephonyEventMessage: Remote end did not answer:" + phoneCall.getCallerID());

		//XXX update controller about this
	    } else if (callState.equals(TelephonyLib.FAILURE)) {

		for (String guid : callList.keySet()) {
		    TelephoneCall phoneCall = callList.get(guid);
		    
		    if (callGUID.equals(phoneCall.getCallerID())) {
			logger.error("HandleTelephonyEventMessage: Failed to place a call:" + phoneCall.getCallerID());
			callGUID = guid;
			break;
		    }
		}
		callList.remove(callGUID);
	    }

	    if (callState.equals(TelephonyLib.SUCCESS)) 
		updateOutgoingCall(callGUID, callState, monitorFile);
	    else
		updateOutgoingCall(callGUID, callState);
	} else if (event.equals(TelephonyEventMessage.PHONE_LINE_OK)) {
	    String[] lineIDs = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.PHONE_LINE_ID));
	    for (String id : lineIDs) {
		lineIDUpdate(id, SUCCESS);
	    }
	} else if (event.equals(TelephonyEventMessage.PHONE_LINE_ERROR)) {
	    String[] lineIDs = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.PHONE_LINE_ID));
	    for (String id : lineIDs) {
		lineIDUpdate(id, FAILURE);
	    }
	} 
	/*
	else if (event.equals(TelephonyEventMessage.DAHDI_ERROR)) {
	    //System.err.println("chann err: "+options);
	    String[] channelNos = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.CHANNEL_NO));
	    for (String num : channelNos) {
		lineIDUpdate(TelephonyLib.DAHDI + num, FAILURE);
	    }
	} else if (event.equals(TelephonyEventMessage.DAHDI_OK)) {
	    //System.err.println("chann ok: "+options);
	    String[] channelNos = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.CHANNEL_NO));
	    for (String num : channelNos) {
		lineIDUpdate(TelephonyLib.DAHDI + num, SUCCESS);
	    }
	} else if (event.equals(TelephonyEventMessage.ATA_REGISTERED)) {
	    String[] ataIDs = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.ATA_ID));
	    for (String id : ataIDs) {
		lineIDUpdate(id, SUCCESS);
	    }
	} else if (event.equals(TelephonyEventMessage.ATA_UNREGISTERED)) {
	    String[] ataIDs = StringUtilities.getArrayFromCSV(options.get(TelephonyEventMessage.ATA_ID));
	    for (String id : ataIDs) {
		lineIDUpdate(id, FAILURE);
	    }
	    //System.err.println("ata unreg: "+options);
	}
	*/
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: from: "+message.getSource()+" Error: " + message.getParam("serviceerror"));
	boolean serviceDown = false;
	String service = "";
	String serviceName = ""; 
	String errorCode = "";
	if (message instanceof ServiceErrorRelayMessage)
	{
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}

	//ErrorCode ignored here.
	if (serviceName != null && (serviceName.equals(onlineTelephonyServiceInstanceName))){
	    serviceDown = true;
	    service = serviceName;
	    onlineTelephonyDisconnected = false;
	} else if (serviceName != null &&  serviceName.equals(RSController.RESOURCE_MANAGER)){
	    serviceDown = true;
	    service = serviceName;
	    rscActive = false;
	    onlineTelephonyDisconnected = false;
	} else {
	    logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
	    setState(CONFIGURING);
	    setOnlineOfflineState(OFFLINE);
	    callList.clear();
	}

	if (serviceDown){
	    setState(CONFIGURING);
	    setOnlineOfflineState(OFFLINE);
	    callList.clear();
	    String[] error = new String[]{RSController.ERROR_SERVICE_DOWN, service};

	    activateTelephony(false, error);
	}
    }

    
    protected int state() {
	return state;
    }

    /*
    protected synchronized void clearIncomingCalls() {
	ArrayList<TelephoneCall> oldCallList = new ArrayList<TelephoneCall> (callList.values());
	for (TelephoneCall phoneCall : oldCallList)
	    if (phoneCall.isIncomingCall())
		callList.remove(phoneCall.getCallGUID());
    }
    */

    protected synchronized boolean canGoOnAir() {
	logger.info("CanGoOnAir: total active calls=" + callList.size());

	for (String phoneCallKey : callList.keySet()) {
	    TelephoneCall phoneCall = callList.get(phoneCallKey);
	    if (phoneCall != null)
		return true;
	}

	return false;
    }

    protected synchronized void setState(int state) {
	int previousState = this.state;
	this.state = state;
	logger.info("SetState: Previous state: "+previousState+" Current State: "+state);
    }

    protected synchronized void setOnlineOfflineState(int newState) {
	int oldState = onlineOfflineState;
	onlineOfflineState = newState;
	logger.info("SetOnlineOfflineState: Previous state: " + oldState + " Current State: " + onlineOfflineState);
    }

    class TelephoneCall {
	String callGUID;
	RSCallerID callerID;

	boolean isIncomingCall;

	public TelephoneCall(String callGUID, RSCallerID callerID, boolean isIncomingCall) {
	    this.callGUID = callGUID;
	    this.callerID = callerID;
	    this.isIncomingCall = isIncomingCall;
	}

	public synchronized void setCallGUID(String callGUID) {
	    this.callGUID = callGUID;
	}

	public synchronized String getCallGUID() {
	    return callGUID;
	}

	public synchronized RSCallerID getCallerID() {
	    return callerID;
	}

	public synchronized boolean isIncomingCall() {
	    return isIncomingCall;
	}
	
    }
}
