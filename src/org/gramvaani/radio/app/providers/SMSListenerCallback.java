package org.gramvaani.radio.app.providers;

import java.util.Hashtable;
import org.gramvaani.radio.telephonylib.RSCallerID;

public interface SMSListenerCallback extends ListenerCallback {

    public void activateSMS(boolean activate, String[] error);
    public void enableSMSUI(boolean enable, String[] error);
    public void SMSStatusUpdate(boolean status, String[] availableLibs, String[] unavailableLibs);
}