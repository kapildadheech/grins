package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.medialib.*;

public interface CacheObjectListener {

    public void cacheObjectValueChanged(Metadata metadata);
    public void cacheObjectKeyChanged(Metadata[] metadata, String[] newKey);
    
}