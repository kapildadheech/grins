package org.gramvaani.radio.app.providers;

public interface ListenerCallback {
    
    public String getName();

}