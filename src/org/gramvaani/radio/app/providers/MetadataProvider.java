package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.app.gui.StatsDisplay;

import java.util.*;

public class MetadataProvider extends RSProviderBase {
    public static String NO_QUERY = "NO_QUERY";
    
    public static final String ORDER_ASC  = MediaLib.ORDER_ASC;
    public static final String ORDER_DESC = MediaLib.ORDER_DESC;

    public static final String MIN_OP   = MediaLib.MIN_OP;
    public static final String MAX_OP   = MediaLib.MAX_OP;
    public static final String COUNT_OP = MediaLib.COUNT_OP;

    protected final static int CONFIGURING = -1;
    protected final static int LISTENING = 0;

    protected int state = CONFIGURING;
    protected int prevState = state;
    protected boolean isIndexActivated = false;
    protected boolean isLibraryActivated = false;
    protected boolean rscActive = true;
    
    protected UIService uiService;
    protected UpdateMetadataHandler updateHandler;
    protected CacheProvider cacheProvider;
    protected MediaLib medialib;

    protected Hashtable<String, MetadataListenerCallback> listeners;

    public MetadataProvider(UIService uiService) {
	super (uiService, RSApp.METADATA_PROVIDER); 
	this.uiService = uiService;
	listeners = new Hashtable<String, MetadataListenerCallback>();
	updateHandler = new UpdateMetadataHandler(uiService.getStationConfiguration(), uiService.getTimeKeeper(), this);

	/*
	MediaLib.setIPCServer(uiService.getIPCServer());
	medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
	if (medialib == null) {
	    logger.error("MetadataProvider: Unable to get access medialib.");
	}
	*/
    }

    public synchronized int activate() {
	// display UI. keep it disabled
	logger.info("Activate: Provider activated.");
	updateHandler.initialize();
	MediaLib.setIPCServer(uiService.getIPCServer());
	medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
	if (medialib == null) {
	    logger.error("MetadataProvider: Unable to get access medialib.");
	}
	
	setState(CONFIGURING);
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String[] {RSController.INDEX_SERVICE, 
										  RSController.LIB_SERVICE});
	int retval = sendCommand(message);
	if (retval == -1)
	    rscActive = false;

	return retval;
    }

    public void setCacheProvider(CacheProvider cacheProvider) {
	if (cacheProvider == null) {
	    logger.error("SetCacheProvider: null cacheprovider.");
	    return;
	}
	this.cacheProvider = cacheProvider;
	updateHandler.setCacheProvider(cacheProvider);
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listeners.put(listener.getName(), (MetadataListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	listeners.remove(listenerName);
    }

    public synchronized boolean isActive() {
	if (state() == CONFIGURING)
	    return false;
	else
	    return isIndexActivated && isLibraryActivated && updateHandler.isActive();
    }

    public StationConfiguration getStationConfig() {
	return uiService.getStationConfiguration();
    }

    public synchronized void receiveMessage(IPCMessage message){
	logger.debug("ReceiveMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof ServiceAckMessage){
	    ServiceAckMessage ackMessage = (ServiceAckMessage) message;
	    for (String service: ackMessage.getActiveServices()) {
		    if (service.equals(RSController.INDEX_SERVICE))
		    	isIndexActivated = true;
		    else if (service.equals(RSController.LIB_SERVICE)) {
			isLibraryActivated = true;
			if (medialib == null)
			    medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
			if (medialib != null) {
			    medialib.setLibraryActivated(true);
			    updateHandler.activate();
			} else {
			    logger.error("ReceiveMessage: Unable to get access medialib.");
			}
			//XXX Clarify semantics of library activated.
		    }
	    }

	    for (String service: ackMessage.getInactiveServices()) {
		if (service.equals(RSController.INDEX_SERVICE)) {
		    	isIndexActivated = false;
			setState (CONFIGURING);
			for (MetadataListenerCallback callback: listeners.values()) {
			    callback.activateMetadataService(false, new String[]{RSController.ERROR_SERVICE_DOWN,
										 service});
			}
		}
		else if (service.equals(RSController.LIB_SERVICE)) {
			isLibraryActivated = false;
			setState (CONFIGURING);
			for (MetadataListenerCallback callback: listeners.values()) {
			    callback.activateMetadataService(false, new String[]{RSController.ERROR_SERVICE_DOWN,
										 service});
			}
		}
		    
			
	    }

	} else if (message instanceof ServiceNotifyMessage){
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage) message;
	    for (String service: notifyMessage.getActivatedServices()) {
		if (service.equals(RSController.INDEX_SERVICE))
		    isIndexActivated = true;
		else if (service.equals(RSController.LIB_SERVICE)) {
		    isLibraryActivated = true;
		    if (medialib == null)
			medialib = MediaLib.getMediaLib(uiService.getStationConfiguration(), uiService.getTimeKeeper());
		    if (medialib != null) {
			medialib.setLibraryActivated(true);
			updateHandler.activate();
		    } else {
			logger.error("ReceiveMessage: Unable to get access medialib.");
		    }
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing. Wait for lib service to be activated.
		    }
		}
	    }
	    for (String service: notifyMessage.getInactivatedServices()) {
		if (service.equals(RSController.LIB_SERVICE)) {
		    setState(CONFIGURING);
		    isLibraryActivated = false;
		    //VVV send the error to inference engine
		    for (MetadataListenerCallback callback: listeners.values()) {
			callback.activateMetadataService(false, new String[]{RSController.ERROR_SERVICE_DISCONNECTED,
									     RSController.LIB_SERVICE});
		    }
		} else if (service.equals(RSController.INDEX_SERVICE)) {
		    setState(CONFIGURING);
		    isIndexActivated = false;
		    //VVV send the error to inference engine
		    for (MetadataListenerCallback callback: listeners.values()) {
			callback.activateMetadataService(false, new String[]{RSController.ERROR_SERVICE_DISCONNECTED,
									     RSController.INDEX_SERVICE});
		    }
		}else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    logger.debug("ReceiveMessage: Got RSController disconnected.");
		    //VVV send the error to inference engine
		    for (MetadataListenerCallback callback: listeners.values()) {
			callback.activateMetadataService(false, new String[]{RSController.ERROR_SERVICE_DISCONNECTED,
									     RSController.RESOURCE_MANAGER});
		    }
		    return;
		} 
	    }
	}

	if (isIndexActivated && isLibraryActivated && updateHandler.isActive())
	    updateServiceActivated();
    }

    public synchronized void activateUpdateHandler(boolean activate, String error){
	if (activate){
	    if (isIndexActivated && isLibraryActivated){
		for (MetadataListenerCallback callback: listeners.values())
		    callback.activateMetadataService(true, new String[]{});
	    }
	} else {
	    for (MetadataListenerCallback callback: listeners.values())
		callback.activateMetadataService(false, new String[]{error});
	}
	    
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: From: "+message.getSource()+" Type: " + message.getParam("serviceerror"));
	boolean isDeactivated = false;

	String serviceName = null;
	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	}

	if (serviceName != null){
	    if (serviceName.equals(RSController.LIB_SERVICE)) {
		isLibraryActivated = false;
		isDeactivated = true;
		if (medialib != null)
		    medialib.setLibraryActivated(false);
	    } else if (serviceName.equals(RSController.INDEX_SERVICE)) {
		isIndexActivated = false;
		isDeactivated = true;
	    } else if (serviceName.equals(RSController.RESOURCE_MANAGER)){
		isIndexActivated = false;
		isLibraryActivated = false;
		isDeactivated = true;
		rscActive = false;
		if (medialib != null)
		    medialib.setLibraryActivated(false);
	    }
	}

	if (isDeactivated) {
	    //VVV send message to inference engine
	    for (MetadataListenerCallback callback: listeners.values()) {
		callback.activateMetadataService(false, new String[]{RSController.ERROR_SERVICE_DOWN,
								     serviceName});
	    }
	}
	setState(CONFIGURING);
    }

    protected void updateServiceActivated (){
	if (state() == CONFIGURING){
	    logger.info("UpdateServiceActivated: Update Service is now activated");
	    setState (LISTENING);

	    for (MetadataListenerCallback callback: listeners.values()) {
		callback.activateMetadataService(true, new String[]{});
	    }
	}
    }

    protected int state() {
	return state;
    }

    protected void setState(int state) {
	prevState = this.state;
	this.state = state;
	logger.info("Previous state: "+prevState+" Current state: "+state);

	switch(state) {
	case CONFIGURING:
	    // disable UI
	    break;
	case LISTENING:
	    // enable UI
	    break;
	}
    }

    public UpdateMetadataHandler getUpdateHandler() {
	return updateHandler;
    }

    public MediaLib getMediaLib() {
	return medialib;
    }

    // methods correspond to actions initiated in the UI
    // an ordering in invocations will have to be preserved. for example, to add a new creator to a program, first the addCreator method should be called
    //      followed by addProgramCreator

    public RadioProgramMetadata[] getProgramMetadata(String[] programIDs) {
	if (state() == LISTENING)
	    return updateHandler.getProgramMetadata(programIDs);
	else
	    return null;
    }

    public String[] getLanguages() {
	/*
	System.out.println("*********************************");
	for (String language: updateHandler.getLanguages())
	    System.out.print(language  + " ");
	System.out.println();
	System.out.println("---------------------------------");
	*/
	return updateHandler.getLanguages();
    }

    public Category[] getCategories() {
	return updateHandler.getCategories();
    }

    public void populateChildren(Category categoryNode) {
	updateHandler.populateChildren(categoryNode);
    }

    public String[] getAffiliations() {
	return updateHandler.getAffiliations();
    }

    public String[] getRoles() {
	return updateHandler.getRoles();
    }

    public String[] getNames() {
	return updateHandler.getNames();
    }

    public String[] getLocations() {
	return updateHandler.getLocations();
    }

    public Playlist[] getPlaylists() {
	return updateHandler.getPlaylists();
    }

    public Entity[] getPotentialContacts(String numberType, String callerID, boolean doSuffixMatch) {
	if (state() == LISTENING)
	    return updateHandler.getPotentialContacts(numberType, callerID, doSuffixMatch);
	else
	    return new Entity[]{};
    }

    public int addPlaylist(Playlist playlist) {
	return updateHandler.addPlaylist(playlist);
    }

    public int addPlaylistItem(PlaylistItem playlistItem) {
	return updateHandler.addPlaylistItem(playlistItem);
    }

    public int updatePlaylist(Playlist oldPlaylist, Playlist newPlaylist) {
	return updateHandler.updatePlaylist(oldPlaylist, newPlaylist);
    }

    public int removePlaylistItems(PlaylistItem playlistItem) {
	return updateHandler.removePlaylistItems(playlistItem);
    }

    public PlaylistItem[] getPlaylistItems(Playlist playlist) {
	return updateHandler.getPlaylistItems(playlist);
    }

    public void reloadPlaylistItems(Playlist playlist) {
	updateHandler.reloadPlaylistItems(playlist);
    }
    
    public int addLanguage(String language) {
	if (state() == LISTENING)
	    return updateHandler.addToLanguageAnchorIndex(language);
	else
	    return -1;
    }

    public int addCreatorAffiliation(String affiliation) {
	if (state() == LISTENING)
	    return updateHandler.addToAffiliationAnchorIndex(affiliation);
	else
	    return -1;
    }

    public int addCreatorRole(String role) {
	if (state() == LISTENING)
	    return updateHandler.addToRoleAnchorIndex(role);
	else
	    return -1;
    }

    public int addCreatorLocation(String location) {
	if (state() == LISTENING)
	    return updateHandler.addToLocationAnchorIndex(location);
	else
	    return -1;
    }

    public int addCreatorName(String name) {
	if (state() == LISTENING)
	    return updateHandler.addToNameAnchorIndex(name);
	else
	    return -1;
    }
    
    /*
    public int addTags(String tagsCSV) {
	if (state() == LISTENING) {
	    Tags[] tags = updateHandler.getAllTags();
	    Hashtable<String, String> tagsHash = new Hashtable<String, String>();
	    for (Tags currTags: tags) {
		String[] tagsArr = StringUtilities.getArrayFromCSV(currTags.getTagsCSV());
		for (String tag: tagsArr) {
		    tag = StringUtilities.sanitize(tag);
		    tagsHash.put(tag, tag);
		}
	    }
	    
	    StringBuilder str = new StringBuilder();
	    String[] newTags = StringUtilities.getArrayFromCSV(tagsCSV);
	    for (String newTag: newTags) {
		if (tagsHash.get(newTag) == null) {
		    str.append(newTag);
		    str.append(",");
		}
	    }
	    if (str.length() > 0)
		return updateHandler.addToTagAnchorIndex(str.substring(0, str.length() - 1));
	    else
		return 0;
	} else
	    return -1;
    }
    */
    /*
    public int addTags(String tagsCSV){
	if (state() == LISTENING) {
	    
	} else {
	    return -1;
	}
    }
    */
    public int updateProgramIndex(String programID) {
	return updateHandler.updateProgramIndex(programID);
    }

    public int flushProgramIndex() {
	return updateHandler.flushProgramIndex();
    }

    /*
    public int addProgramTags(String programID, String tagsCSV) {
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.addTagToProgramMetadata(programID, tagsCSV, 0, -1) == 0)
	    return 0;
	else
	    return -2;
    }

    public int editProgramTags(String programID, String tagsCSV) {
	logger.debug("editProgramTags: called with " + programID + ":" + tagsCSV + ":" + state());
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.editTagsInProgramMetadata(programID, tagsCSV, 0, -1) == 0)
	    return 0;
	else
	    return -2;
    }
    */

    
    public int setProgramTags(String programID, String tagsCSV) {
	if (state() != LISTENING)
	    return -1;
	if (updateHandler.setProgramTags(programID, tagsCSV) == 0)
	    return 0;
	else
	    return -2;
    }

    public int addProgramCategory(String programID, int categoryNodeID) {
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.addCategoryToProgramMetadata(programID, categoryNodeID) == 0)
	    return 0;
	else
	    return -2;
    }

    public int removeProgramCategory(String programID, int categoryNodeID) {
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.removeCategoryFromProgramMetadata(programID, categoryNodeID) == 0)
	    return 0;
	else
	    return -2;
    }

    public int editProgramCategory(String programID, int oldCategoryNodeID, int newCategoryNodeID) {
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.editCategoryInProgramMetadata(programID, oldCategoryNodeID, newCategoryNodeID) == 0)
	    return 0;
	else
	    return -2;
    }

    public int editProgramCreator(String programID, int entityID, String affiliation) {
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.editCreatorInProgramMetadata(programID, entityID, affiliation) == 0)
	    return 0;
	else
	    return -2;
    }

    public int editProgramCreator(String programID, int oldEntityID, int entityID, String affiliation) {
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.editCreatorInProgramMetadata(programID, oldEntityID, entityID, affiliation) == 0)
	    return 0;
	else
	    return -2;
    }

    public int removeProgramCreator(String programID, int entityID) {
	if (state() != LISTENING)
	    return -1;
	
	if (updateHandler.removeCreatorInProgramMetadata(programID, entityID) == 0)
	    return 0;
	else
	    return -2;
    }

    public int addProgramCreator(String programID, int entityID, String affiliation) {
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.addCreatorInProgramMetadata(programID, entityID, affiliation) == 0)
	    return 0;
	else
	    return -2;
    }

    public String[] getAllPollKeywords() {
	Object[] keywordObjs = unique(Poll.getDummyObject(-1), Poll.getKeywordFieldName());

	ArrayList<String> keywordList = new ArrayList<String>();
	for (Object keyword: keywordObjs)
	    keywordList.add((String)keyword);

	return keywordList.toArray(new String[keywordList.size()]);
    }

    public PollOption[] getPollOptionsOfPoll(int pollID) {
	PollOption option = PollOption.getDummyObject(-1);
	option.setPollID(pollID);
	return (PollOption[])get(option);
    }

    public Vote[] getVotesOfPoll(int pollID) {
	Vote vote = Vote.getDummyObject(-1);
	vote.setPollID(pollID);
	return (Vote[])get(vote);
    }

    public int addPoll(String name, String keyword, int type, int state) {
	if (state() == LISTENING)
	    return updateHandler.addPollMetadata(name, keyword, type, state);
	else 
	    return -1;
    }

    public int editPoll(int pollID, String name, String keyword, int type, int state) {
	if (state() == LISTENING)
	    return updateHandler.editPollMetadata(pollID, name, keyword, type, state);
	else 
	    return -1;
    }

    public int addPollOption(int pollID, String name, String code) {
	return updateHandler.addPollOptionMetadata(pollID, name, code);
    }

    public int removePollOption(int optionID) {
	return updateHandler.removePollOptionMetadata(optionID);
    }

    public int addEntity(String name, String url, String phoneType, String phone, String email, String location, String type) {
	if (state() == LISTENING) {
	    int entityID = updateHandler.addEntityMetadata(name, url, phoneType, phone, email, location, type);
	    return entityID;
	} else
	    return -1;
    }

    public String editEntity(int entityID, String name, String url, String phoneType, String phone, String email, String location, String type) {

	if (state() == LISTENING) {
	    return updateHandler.editEntityMetadata(entityID, name, url, phoneType, phone, email, location, type);
	} else {
	    return null;
	}
    }

    public int editProgram(String programID, String title, String description, String language, String license) {
	if (state() != LISTENING)
	    return -1;

	if (updateHandler.editProgramMetadata(programID, title, description, language, license) == 0)
	    return 0;
	else
	    return -2;
    }


    /* Exposing Medialib Functions */
    //XXX Add error trapping and forwarding.

    public int update(Metadata queryMetadata, Metadata updateMetadata){
	return updateHandler.update(queryMetadata, updateMetadata);
    }

    public int insert(Metadata metadata){
	return updateHandler.insert(metadata);
    }

    public int remove(Metadata metadata){
	return updateHandler.remove(metadata);
    }

    public int remove(Metadata metadata, boolean invalidateCache) {
	return updateHandler.remove(metadata, invalidateCache);
    }

    public <T extends Metadata> T[] get(T metadata, String whereClause, String[] sortField, String[] order, String[] between, int... limits){
	return updateHandler.get(metadata, whereClause, sortField, order, between, limits);
    }

    public <T extends Metadata> T[] get(T metadata){
	return updateHandler.get(metadata);
    }

    public <T extends Metadata> T getSingle(T metadata){
	return updateHandler.getSingle(metadata);
    }

    public <T extends Metadata> T[] selectAllFields(T metadata, String whereClause, int... limits) {
	return updateHandler.selectAllFields(metadata, whereClause, limits);
    }

    public Object mathOp(String tableName, String whereClause, String fieldName, String operation) {
	return updateHandler.mathOp(tableName, whereClause, fieldName, operation);
    }
    
    public Object[] select(String joinTableNames, String whereClause, String fieldName, int limit) {
	return updateHandler.select(joinTableNames, whereClause, fieldName, limit);
    }

    public Object[] unique(Metadata metadata, String fieldName) {
	return updateHandler.unique(metadata, fieldName);
    }

    /* PlayoutHistoryFilter Functions */
    
    public long getMinTelecastTime(){
	return updateHandler.getMinTelecastTime();
    }

    public long getMaxTelecastTime(){
	return updateHandler.getMaxTelecastTime();
    }

    /* CreationTimeFilter Functions */

    public long getMinCreationTime(){
	return updateHandler.getMinCreationTime();
    }

    public long getMaxCreationTime(){
	return updateHandler.getMaxCreationTime();
    }

    /* LengthFilter Functions */

    public long getMaxLength(){
	return updateHandler.getMaxLength();
    }


    public GraphStats getBarGraphStats(String whereClause, long minTelecastTime, long maxTelecastTime) {
	GraphStats barGraphStats = sendGraphStats(whereClause, minTelecastTime, maxTelecastTime, StatsDisplay.BAR_GRAPH);
	return barGraphStats;
    }

    public GraphStats getPointGraphStats(String whereClause, long minTelecastTime, long maxTelecastTime) {
	GraphStats pointGraphStats = sendGraphStats(whereClause, minTelecastTime, maxTelecastTime, StatsDisplay.POINT_GRAPH);
	return pointGraphStats;
    }

    private GraphStats sendGraphStats(String whereClause, long minTelecastTime, long maxTelecastTime,
				      int graphType) {
	try {
	    GraphStatsMessage statsMessage = new GraphStatsMessage(IPCSyncMessage.SYNC_MESSAGE, 
								   RSController.INDEX_SERVICE,
								   NO_QUERY, whereClause, "", 
								   minTelecastTime, maxTelecastTime,
								   graphType);
	    GraphStatsResponseMessage statsResponseMessage = (GraphStatsResponseMessage)(sendSyncCommand(statsMessage));
	    
	    if (statsResponseMessage == null) {
		return new GraphStats();
	    } else {
		GraphStats graphStats = new GraphStats();
		graphStats.setMinTelecastTime(statsResponseMessage.getMinTelecastTime());
		graphStats.setMaxTelecastTime(statsResponseMessage.getMaxTelecastTime());
		graphStats.setXLabels(statsResponseMessage.getXLabels());
		graphStats.setDataItems(statsResponseMessage.getDataItems());
		graphStats.setApproxBinInterval(statsResponseMessage.getApproxBinInterval());
		graphStats.setMaxDataItem(statsResponseMessage.getMaxDataItem());

		return graphStats;
	    }
	} catch(IPCSyncMessageTimeoutException e) {
	    return new GraphStats();
	}
    }
    
}