package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class MicProvider extends RSProviderBase {
    public static String SRC_PORT_ROLE = MicService.BCAST_MIC;
    public static String PLAYOUT_PORT_ROLE = MicService.PLAYOUT;
    public static String PREVIEW_PORT_ROLE = MicService.PREVIEW;
    
    protected final static int CONFIGURING = -1;
    protected final static int DISABLED = 0; //Equivalent to Listening state in most other providers
    protected final static int ENABLE_COMMAND_SENT = 1;
    protected final static int ENABLED = 2;
    protected final static int MUTE_COMMAND_SENT = 3;
    protected final static int MUTED = 4;
    protected final static int UNMUTE_COMMAND_SENT = 5;
    protected final static int VOLUME_CHANGE_SENT = 6;
    protected final static int DISABLE_COMMAND_SENT = 7;

    protected final static String SUCCESS = "SUCCESS";
    protected final static String FAILURE = "FAILURE";

    protected int state = CONFIGURING;
    protected String micServiceInstanceName;
    protected String micServiceID = "";
    protected String currentMicCallback = null;
    protected boolean rscActive = true;
    protected boolean micDisconnected = true;
    protected boolean prematureEnable = false;
    protected Hashtable<String, MicListenerCallback> listeners = null;

    protected ExecutorService listenerExecutor;
    protected final static int POOL_THREAD_COUNT = 1;

    static final int MIN_LEVEL_THRESHOLD = -90;
    
    public MicProvider(UIService uiService) {
	super (uiService, RSApp.MIC_PROVIDER); 
	micServiceInstanceName = uiService.getProviderServiceInstance(RSApp.MIC_PROVIDER, MicService.BCAST_MIC);
	listeners = new Hashtable<String, MicListenerCallback>();

	listenerExecutor = Executors.newFixedThreadPool(POOL_THREAD_COUNT);
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    listenerExecutor.shutdown();
		}
	    });

    }

    
    public synchronized int activate() {
	// display UI. keep it disabled
	logger.info("Activate: Provider Activated");
	setState(CONFIGURING);
	
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String[] {micServiceInstanceName});

	int retVal = sendCommand(message);
	if (retVal != 0)
	    rscActive = false;

	return retVal;
    }

    public boolean isActive(){
	return (state() != CONFIGURING);
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	listeners.put(listener.getName(), (MicListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	listeners.remove(listenerName);
    }

    public synchronized void receiveMessage(IPCMessage message){
	logger.debug("Received Message from: "+message.getSource()+" type: "+message.getMessageClass());
	if (message instanceof MicAckMessage) {
	    MicAckMessage ackMessage = (MicAckMessage)message;
	    handleMicAckMessage(ackMessage.getAck(), ackMessage.getCommand(), ackMessage.getError());

	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    boolean inactivate = false;
	    String disconnectedService = "";
	    for (String service: notifyMessage.getInactivatedServices()) {
		if (service.equals(micServiceInstanceName)) {

		    disconnectedService = service;
		    micDisconnected = true;
		    inactivate = true;

		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    disconnectedService = service;
		    inactivate = true;
		}
	    }

	    if (inactivate) {
		logger.debug("ReceiveMessage: Found "+disconnectedService+" disconnected");

		String[] error;
		error = new String[]{RSController.ERROR_SERVICE_DISCONNECTED, disconnectedService};
		
		enableMicUI(false, error);
	    }
	    
	    for (String service: notifyMessage.getActivatedServices()){
		if (service.equals(micServiceInstanceName)) {
		    if (!micDisconnected){
			setState(DISABLED);
			activateMic(true, new String[]{});
		    } else {
			micDisconnected = false;
			enableMicUI(true, new String[]{});
		    }
		} else if (service.equals(RSController.RESOURCE_MANAGER)){
		     logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			rscActive = true;
			logger.debug("ReceiveMessage: Calling activate.");
			activate();
		    } else {
			//Do nothing
		    }
		}
	    }

	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();

	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(micServiceInstanceName))
		    micServiceConnected(activatedIDs[i]);
	    }

	} else if (message instanceof ServiceAckMessage){

	    ServiceAckMessage serviceAckMessage = (ServiceAckMessage) message;
	    for (String service: serviceAckMessage.getActiveServices()){
		if (service.equals(micServiceInstanceName)) {
		    micDisconnected = false;
		    setState(DISABLED);
		    activateMic(true, new String[]{});
		}
	    }

	    for (String service: serviceAckMessage.getInactiveServices()){
		if (service.equals(micServiceInstanceName)) {
		    micDisconnected = false;
		    setState(CONFIGURING);
		    activateMic(false, new String[]{RSController.ERROR_SERVICE_DOWN,
						    service});
		}
	    }
	} else if (message instanceof GstreamerErrorMessage){
	    GstreamerErrorMessage errorMessage = (GstreamerErrorMessage)message;
	    handleGstreamerErrorMessage(errorMessage.getError());
	} else if (message instanceof MicLevelsMessage){
	    MicLevelsMessage levelsMessage = (MicLevelsMessage) message;
	    handleMicLevelsMessage(levelsMessage.getAck(), levelsMessage.getLevels());
	}


    }

    protected void micServiceConnected(String newID){
	logger.debug("MicServiceConnected with ID: "+newID);
		
	if (!micServiceID.equals("") && !micServiceID.equals(newID)){
	    activateMic(false, new String[]{RSController.ERROR_SERVICE_DOWN, micServiceInstanceName});
	    activateMic(true,  new String[]{});
	}
	
	micServiceID = newID;

    }

    /* Sending callbacks to listeners routed through an executor to avoid deadlocks */

    protected void activateMic(final boolean activate, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (MicListenerCallback listener: listeners.values())
			listener.activateMic(activate, error);
		}
	    });
    }

    protected void enableMicUI(final boolean enable, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (MicListenerCallback listener: listeners.values())
			listener.enableMicUI(enable, error);
		}
	    });
    }

    protected void micEnabled(final MicListenerCallback listener, final boolean success, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    //if (listener != null)
		    //listener.micEnabled(success, error);
		    for (MicListenerCallback listener: listeners.values())
			listener.micEnabled(success, error);
		}
	    });
    }

    protected void micDisabled(final MicListenerCallback listener, final boolean success, final String[] error){
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    //if (listener != null)
		    //listener.micDisabled(success, error);
		    for (MicListenerCallback listener: listeners.values())
		    	listener.micDisabled(success, error);
		}
	    });
    }

    /* End of call backs through executors */

    protected void handleMicLevelsMessage(String ack, String[] levelStrings){
	if (currentMicCallback == null || listeners.get(currentMicCallback) == null)
	    return;

	final MicListenerCallback listener = listeners.get(currentMicCallback);
	final int level = getAverageLevels(levelStrings);

	final boolean success;
	if (level < MIN_LEVEL_THRESHOLD)
	    success = false;
	else
	    success = true;
	listenerExecutor.execute(new Runnable(){
		public void run(){
		    listener.micLevelTested(success, level);
		}
	    });

    }

    protected int getAverageLevels(String[] levelStrings){
	if (levelStrings.length < 1){
	    logger.error("GetAverageLevels: No level values present.");
	    return MIN_LEVEL_THRESHOLD - 1;
	}

	int sum = 0;
	for (String level: levelStrings)
	    sum += Integer.parseInt(level);

	sum /= levelStrings.length;

	return sum;
    }


    protected void handleGstreamerErrorMessage(final String error){
	logger.error("HandleGstreamerErrorMessage: Received message: "+error+" from archiver service.");
	final MicListenerCallback listener;
	if (currentMicCallback != null && (listener = listeners.get(currentMicCallback)) != null) {
	    listenerExecutor.execute(new Runnable(){
		    public void run(){
			listener.micGstError(error);
			//VVV inform of this error to inference engine also
		    }
		});
	}
	setState(DISABLED);
    }

    protected void handleMicAckMessage (String ack, String command, String[] error){
	logger.info("HandleMicAck: " + ack + ":" + state());
	MicListenerCallback listener;
	switch (state()){
	case ENABLE_COMMAND_SENT:
	    if (prematureEnable && command.equals(MicService.DISABLE_MSG)){
		if (ack.equals(MicService.SUCCESS)) {
		    //XXX: Race condition here when disable command was sent from playlist controller
		    //XXX: and enable sent from simulation widget. Unlikely but possible
		    if (currentMicCallback != null && (listener = listeners.get(currentMicCallback)) != null)
			micDisabled(listener, true, error);
		} else {

		    if (currentMicCallback != null && (listener = listeners.get(currentMicCallback)) != null)
			micDisabled(listener, false, error);
		}
		prematureEnable = false;

	    } else {
		    if (ack.equals(MicService.SUCCESS)){
			logger.info("HandleMicAckMessage: Enabled.");
			setState(ENABLED);

			if (currentMicCallback != null && (listener = listeners.get(currentMicCallback)) != null)
			    micEnabled(listener, true, error);
		    } else {
			logger.error("HandleMicAckMessage: Unable to enable.");
			setState(DISABLED);

			if (currentMicCallback != null && (listener = listeners.get(currentMicCallback)) != null)
			    micEnabled(listener, false, error);
		    }
	    }
	    break;

	case DISABLE_COMMAND_SENT:
	    if (command.equals(MicService.DISABLE_MSG)){
		if (ack.equals(MicService.SUCCESS)){
		    logger.info("HandleMicAckMessage: Disabled.");
		    setState(DISABLED);
		    if (currentMicCallback != null && (listener = listeners.get(currentMicCallback)) != null)
			micDisabled(listener, true, error);
		}else{
		    logger.error("HandleMicAckMessage: Unable to disable.");
		    if (currentMicCallback != null && (listener = listeners.get(currentMicCallback)) != null)
			micDisabled(listener, false, error);
		}
	    }

	    break;

	default:
	    logger.error("HandleMicAckMessage: Ack received for command:" +command+ " in unexpected state:"+state());
	    break;
	}
    }

    protected void displayError (String error){
	logger.error(error);
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: from: "+message.getSource()+" Error: " + message.getParam("serviceerror"));
	boolean serviceDown = false;
	String service = "";
	String serviceName = ""; 
	String errorCode = "";
	if (message instanceof ServiceErrorRelayMessage) {
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}

	//ErrorCode ignored here.
	if (serviceName != null && (serviceName.equals(micServiceInstanceName))){
	    serviceDown = true;
	    service = serviceName;
	    micDisconnected = false;
	} else if (serviceName != null &&  serviceName.equals(RSController.RESOURCE_MANAGER)){
	    serviceDown = true;
	    service = serviceName;
	    rscActive = false;
	    micDisconnected = false;
	} else {
	    logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
	    setState(CONFIGURING);
	}

	if (serviceDown){
	    setState(CONFIGURING);
	    String[] error = new String[]{RSController.ERROR_SERVICE_DOWN, service};
	    /*
	    for (MicListenerCallback listener: listeners.values())
		listener.activateMic(false, error);
	    */
	    activateMic(false, error);
	}
    }

    public synchronized boolean isStartable() {
	return (state() == DISABLED || state() == DISABLE_COMMAND_SENT);
    }

    public synchronized boolean isStarted() {
	return (state() == ENABLE_COMMAND_SENT || state() == ENABLED);
    }

    public synchronized int enable(String micListenerName, String portRole) {
	if (isStarted())
	    return 0;

	if (!isStartable()){
	    logger.warn("Enable: Cannot Enable since not in DISABLED state");
	    return -1;
	}
	
	MicCommandMessage message = new MicCommandMessage("", micServiceInstanceName, getName(), 
							  MicService.ENABLE_MSG, SRC_PORT_ROLE,
							  portRole, uiService.getLanguage());
	int retVal = sendCommand(message);
	if (retVal == 0) {
	    if (state() == DISABLE_COMMAND_SENT)
		prematureEnable = true;
	    currentMicCallback = micListenerName;
	    setState(ENABLE_COMMAND_SENT);
	} else {
	    setState(DISABLED);
	    logger.error("Enable: Unable to send enable message.");
	}

	return retVal;
    }

    public synchronized int enableDiagnostics(String micListenerName) {
	if (state() != DISABLED && state() != DISABLE_COMMAND_SENT){
	    logger.warn("EnableDiagnostics: Cannot Enable since not in DISABLED state");
	    return -1;
	}
	
	MicCommandMessage message = new MicCommandMessage("", micServiceInstanceName, getName(), 
							  MicService.ENABLE_DIAG_MSG, SRC_PORT_ROLE,
							  PLAYOUT_PORT_ROLE, uiService.getLanguage());
	int retVal = sendCommand(message);
	if (retVal == 0) {
	    if (state() == DISABLE_COMMAND_SENT)
		prematureEnable = true;
	    currentMicCallback = micListenerName;
	    setState(ENABLE_COMMAND_SENT);
	} else {
	    setState(DISABLED);
	    logger.error("Enable: Unable to send enable message.");
	}

	return retVal;
    }
    
    public synchronized int disable(String portRole) {
	if (state() != ENABLED && state() != ENABLE_COMMAND_SENT){
	    logger.warn("Disable: Cannot disable since provider not in ENABLED state. State=" + state());
	    return -1;
	}

	MicCommandMessage message = new MicCommandMessage("", micServiceInstanceName, getName(), 
							  MicService.DISABLE_MSG, SRC_PORT_ROLE,
							  portRole, uiService.getLanguage());
	
	int retVal = sendCommand(message);
	if (retVal == 0){
	    setState(DISABLE_COMMAND_SENT);
	} else {
	    logger.error("Disable: Unable to send disable message.");
	    setState(ENABLED);
	}
	
	return retVal;
    }

    protected int state() {
	return state;
    }

    protected void setState(int state) {
	int previousState = this.state;
	this.state = state;
	logger.info("Previous state: "+previousState+" Current State: "+state);
    }
}
