package org.gramvaani.radio.app.providers;

public interface ArchiverListenerCallback extends ListenerCallback {
    
    public void activateArchiver(boolean activate, String[] errorCodeAndParams);
    public void enableArchiverUI(boolean enable, String[] errorCodeAndParams); //used for setting accessibility/clickability of UI
    public void archiveStarted(boolean activate, String[] errorCodeAndParams);
    public void archiveStopped(boolean activate, String[] errorCodeAndParams);
    public void archivingDone(String filename, boolean success, long startTime, String error);
    public void archiverGstError(String error);

    public void archiverLevelTested(boolean success, int level);
}