package org.gramvaani.radio.app.providers;

import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.gui.StatsDisplay;

import java.util.*;

public class MetaSearchFilter {
    private static final int MAX_ACTIVE_CATEGORY_FILTERS = 5;
    public static final int BUFFERED_SEARCH_RESULTS = 20;
    public static final int ALL_SEARCH_RESULTS = -1;

    protected MediaLib medialib;
    protected LibraryProvider libraryProvider;
    protected CacheProvider cacheProvider;
    protected LogUtilities logger;

    protected CreatorFilter creatorFilter;
    protected LanguageFilter languageFilter;
    protected LengthFilter lengthFilter;
    protected LicenseFilter licenseFilter;
    protected TypeFilter typeFilter;
    protected TextFilter textFilter;
    protected PlayoutHistoryFilter playoutFilter;
    protected NewItemFilter newItemFilter;
    protected TagsFilter tagsFilter;
    protected CreationTimeFilter creationTimeFilter;

    protected ArrayList<String> searchTexts;

    private int numActiveCategoryFilters;
    private Hashtable<String, SearchFilter> filters;
    private Hashtable<String, SearchFilter> activeFilters;
    private Hashtable<String, SearchFilter> displayFilters;
    private RadioProgramMetadata[] initialSearchSet;
    private int numSearchResults;
    private int numNullResults;

    protected boolean hasMoreResults = false;
    protected int searchOffset = 0;
    protected int lastIncrement = 0;

    public MetaSearchFilter(MediaLib medialib, LibraryProvider libraryProvider, CacheProvider cacheProvider, LogUtilities logger) {
	this.medialib = medialib;
	this.libraryProvider = libraryProvider;
	this.cacheProvider = cacheProvider;
	this.filters = new Hashtable<String, SearchFilter>();
	this.activeFilters = new Hashtable<String, SearchFilter>();
	this.displayFilters = new Hashtable<String, SearchFilter>();
	this.logger = logger;

	searchTexts = new ArrayList<String>();
	init();
    }

    public void init() {
	creatorFilter = new CreatorFilter();
	languageFilter = new LanguageFilter();
	lengthFilter = new LengthFilter();
	licenseFilter = new LicenseFilter();
	typeFilter = new TypeFilter();
	textFilter = new TextFilter();
	playoutFilter = new PlayoutHistoryFilter();
	creationTimeFilter = new CreationTimeFilter();
	newItemFilter = new NewItemFilter();
	tagsFilter = new TagsFilter();

	filters.put(creatorFilter.getFilterName(), creatorFilter);
	filters.put(languageFilter.getFilterName(), languageFilter);
	filters.put(lengthFilter.getFilterName(), lengthFilter);
	filters.put(licenseFilter.getFilterName(), licenseFilter);
	filters.put(typeFilter.getFilterName(), typeFilter);
	filters.put(textFilter.getFilterName(), textFilter);
	filters.put(playoutFilter.getFilterName(), playoutFilter);
	filters.put(creationTimeFilter.getFilterName(), creationTimeFilter);
	filters.put(newItemFilter.getFilterName(), newItemFilter);
	filters.put(tagsFilter.getFilterName(), tagsFilter);

	// always active
	addActiveFilter(textFilter);

	numActiveCategoryFilters = 0;
	
	initialSearchSet = null;
    }

    public SearchFilter[] getDefaultDisplayFilters() {
	ArrayList<SearchFilter> defaultFilters = new ArrayList<SearchFilter>();
	defaultFilters.add(filters.get(SearchFilter.LANGUAGE_FILTER));
	defaultFilters.add(filters.get(SearchFilter.CREATOR_FILTER));

	return defaultFilters.toArray(new SearchFilter[]{});
    }

    public SearchFilter[] getAllFilters() {
	ArrayList<SearchFilter> defaultFilters = new ArrayList<SearchFilter>();
	defaultFilters.add(filters.get(SearchFilter.LANGUAGE_FILTER));
	defaultFilters.add(filters.get(SearchFilter.CREATOR_FILTER));
	defaultFilters.add(filters.get(SearchFilter.LENGTH_FILTER));
	defaultFilters.add(filters.get(SearchFilter.TYPE_FILTER));
	//	defaultFilters.add(filters.get(SearchFilter.LICENSE_FILTER));
	defaultFilters.add(filters.get(SearchFilter.PLAYOUTHISTORY_FILTER));
	defaultFilters.add(filters.get(SearchFilter.CREATION_TIME_FILTER));
	defaultFilters.add(filters.get(SearchFilter.NEWITEM_FILTER));
	defaultFilters.add(filters.get(SearchFilter.TAG_FILTER));

	return defaultFilters.toArray(new SearchFilter[]{});	
    }

    public SearchFilterThin[] getAllCategoryFilters() {
	Category[] allCategories = getAllCategories(medialib, cacheProvider);
	SearchFilterThin[] defaultFilters = new SearchFilterThin[allCategories.length];
	int i = 0;

	for (Category category: allCategories) {
	    defaultFilters[i++] = new SearchFilterThin(SearchFilter.CATEGORY_FILTER + "_" + category.getNodeName(),
						       category.getLabel());
	}

	return defaultFilters;
    }

    public static Category[] getAllCategories(MediaLib medialib, CacheProvider cacheProvider) {
	ArrayList<Category> categories = new ArrayList<Category>();
	Category category = new Category();
	// XXX this is inefficient. need to change medialib to allow query construction for null or -1 initialized fields
	Metadata[] allCategories = cacheProvider.get(category);
	for (Metadata categoryMetadata: allCategories) {
	    if (((Category)categoryMetadata).getParentID() == -1) {
		categories.add((Category)categoryMetadata);
	    }
	}
	
	return categories.toArray(new Category[]{});
    }

    public SearchFilter getFilter(String filterName) {
	return filters.get(filterName);
    }

    public SearchFilter getCategoryFilter(String filterName) {
	if (filters.get(filterName) != null)
	    return filters.get(filterName);

	String categoryName = filterName.split(SearchFilter.CATEGORY_FILTER + "_")[1];
	Category categoryNode = cacheProvider.getSingle(Category.getDummyObjectByNonPrimaryKey(categoryName));
	Category[] allChildren = null;

	if (!cacheProvider.assocFieldsInitialized(categoryNode)) {
	    allChildren = populateChildren(categoryNode, medialib, cacheProvider);

	    // XXX categories do not change right now. not setting assocDependencies
	    cacheProvider.setAssocFieldsInitialized(categoryNode, true);
	} else {
	    allChildren = populateChildren(categoryNode);
	}
	CategoryFilter categoryFilter = new CategoryFilter();
	categoryFilter.setParentNode(categoryNode);
	categoryFilter.setSelectedNode(categoryNode);
	categoryFilter.setAllChildren(allChildren);

	filters.put(filterName, categoryFilter);

	return categoryFilter;
    }

    public static Category[] populateChildren(Category categoryNode, MediaLib medialib, CacheProvider cacheProvider) {
	if (cacheProvider.assocFieldsInitialized(categoryNode)) {
	    return categoryNode.getAllChildren();
	} else {
	    Category[] allChildren = categoryNode.getAllChildren(medialib);
	    cacheProvider.setAssocFieldsInitialized(categoryNode, true);
	    return allChildren;
	}
    }

    public static Category[] populateChildren(Category categoryNode) {
	return categoryNode.getAllChildren();
    }

    public void displayFilter(SearchFilter displayFilter) {
	displayFilters.put(displayFilter.getFilterName(), displayFilter);
    }

    public void activateFilter(SearchFilter filter) {
	addActiveFilter(filter);
    }

    public void deactivateFilter(SearchFilter filter) {
	removeActiveFilter(filter.getFilterName());
	// XXX check if this removal from displayFilters leads to any problems	if (filter.getFilterName().equals(SearchFilter.NEWITEM_FILTER))
	displayFilters.remove(filter.getFilterName());
    }

    public LibraryProvider.SearchDetails allResults() {
	String queryString = formQueryString();
	logger.info("allresults: initial search querystring = " + queryString);

	String whereClause = formWhereClause();
	logger.info("allresults: initial search where clause = " + whereClause);
	
	String joinTableStr = formJoinTableString();
	logger.info("allresults: form join table string = " + joinTableStr);

	if (queryString.equals("") && whereClause.equals(""))
	    return null;

	LibraryProvider.SearchDetails searchDetails = libraryProvider.getMatchingPrograms(queryString, whereClause, joinTableStr, 0, 9999);

	return searchDetails;
    }

    public GraphStats getGraphStats(long minTelecastTime, long maxTelecastTime, int graphType) {
	String queryString = formQueryString();
	logger.info("getbargraphstats: initial search querystring = " + queryString);
	
	String whereClause = formWhereClause();
	logger.info("getbargraphstats: initial search where clause = " + whereClause);
	
	String joinTableStr = formJoinTableString();
	logger.info("getbargraphstats: form join table string = " + joinTableStr);

	if (queryString.equals("") && whereClause.equals(""))
	    return new GraphStats();
	
	if (graphType == StatsDisplay.BAR_GRAPH)
	    return libraryProvider.getBarGraphStats(queryString, whereClause, joinTableStr, minTelecastTime, maxTelecastTime);
	else
	    return libraryProvider.getPointGraphStats(queryString, whereClause, joinTableStr, minTelecastTime, maxTelecastTime);	    
    }

    public void rerun() {
	logger.info("rerun: starting a rerun");

	//  if (initialSearchSet == null) {

	String queryString = formQueryString();
	logger.info("rerun: initial search querystring = " + queryString);

	String whereClause = formWhereClause();
	logger.info("rerun: initial search where clause = " + whereClause);
	
	String joinTableStr = formJoinTableString();
	logger.info("rerun: form join table string = " + joinTableStr);

	if (queryString.equals("") && whereClause.equals("")){
	    initialSearchSet = null;
	    numSearchResults = numNullResults = 0;
	    return;
	}

	LibraryProvider.SearchDetails searchDetails = libraryProvider.getMatchingPrograms(queryString, whereClause, 
											  joinTableStr, 0, BUFFERED_SEARCH_RESULTS);

	if (searchDetails == null){
	    logger.error("Rerun: Null searchDetails. Unable to retrieve matching programs.");
	    return;
	}

	String[] programIDs = searchDetails.getPrograms();
	numSearchResults = searchDetails.numSearchResults();
	numNullResults = 0;

	if (programIDs != null) {
	    logger.info("rerun: search returned " + programIDs.length + " programs out of "+numSearchResults);
	    
	    searchOffset = 0;
	    lastIncrement = 0;

	    if (programIDs.length < numSearchResults)
		hasMoreResults = true;
	    else
		hasMoreResults = false;
	
	    lastIncrement += programIDs.length;

	    initialSearchSet = new RadioProgramMetadata[programIDs.length];
	    getProgramDetails(programIDs, initialSearchSet, 0);

	} else {
	    logger.error("rerun: returned null");
	}

	logger.info("rerun: leaving");
    }

    public synchronized void removeProgramFromSearchResult(String itemID) {
	if (itemID == null){
	    logger.warn("RemoveProgramFromSearchResult: Trying to remove null item.");
	    return;
	}
	//Return if we have not searched anything
	if (initialSearchSet == null)
	    return;

	ArrayList<RadioProgramMetadata> newSearchSet = new ArrayList<RadioProgramMetadata>();

	for (RadioProgramMetadata program : initialSearchSet) {
	    if (program != null && program.getItemID() != null && !program.getItemID().equals(itemID)) {
		newSearchSet.add(program);
	    } 
	}

	initialSearchSet = newSearchSet.toArray(new RadioProgramMetadata[]{});
	numSearchResults--;
	lastIncrement--;
    }

    public boolean hasMoreResults() {
	return hasMoreResults;
    }

    public int getSearchOffset() {
	return searchOffset;
    }

    public void getMoreResults() {
	logger.info("getMoreResults: getting more results");
	searchOffset += lastIncrement;

	String queryString = formQueryString();
	logger.info("getMoreResults: query string = " + queryString);

	String whereClause = formWhereClause();
	logger.info("rerun: initial search where clause = " + whereClause);
	
	String joinTableStr = formJoinTableString();
	logger.info("rerun: form join table string = " + joinTableStr);

	if (queryString.equals("") && whereClause.equals(""))
	    return;

	LibraryProvider.SearchDetails searchDetails = libraryProvider.getMatchingPrograms(queryString, whereClause, 
											  joinTableStr, searchOffset, BUFFERED_SEARCH_RESULTS);

	//XXX: null ptr once. follow up.
	if (searchDetails == null) {
	    logger.error("GetMoreResults: Null search details returned.");
	    return;
	}

	String[] programIDs = searchDetails.getPrograms();
	numSearchResults = searchDetails.numSearchResults();

	if (programIDs != null) {
	    logger.info("getMoreResults: found " + programIDs.length + " programs : " + initialSearchSet.length + ":" + numSearchResults);

	    //	    if (programIDs.length >= BUFFERED_SEARCH_RESULTS)
	    //		hasMoreResults = true;
	    //	    else
	    //		hasMoreResults = false;

	    if (searchOffset + programIDs.length < numSearchResults)
		hasMoreResults = true;
	    else
		hasMoreResults = false;

	    lastIncrement = programIDs.length;
	    
	    RadioProgramMetadata[] currentSearchSet = new RadioProgramMetadata[initialSearchSet.length];
	    int i = 0;
	    for (RadioProgramMetadata metadata: initialSearchSet)
		currentSearchSet[i++] = metadata;
	    
	    initialSearchSet = new RadioProgramMetadata[currentSearchSet.length + programIDs.length];
	    getProgramDetails(programIDs, initialSearchSet, currentSearchSet.length);
	    
	    i = 0;
	    for (RadioProgramMetadata metadata: currentSearchSet)
		initialSearchSet[i++] = metadata;
	} else {
	    logger.error("getMoreResults: returned null");
	}

	logger.info("getMoreResults: Leaving");
    }
    
    private void getProgramDetails(String[] programIDs, RadioProgramMetadata[] metadata, int offset) {
	
	RadioProgramMetadata queryObject = RadioProgramMetadata.getDummyObject("");
	int i = 0;
	for (String programID: programIDs) {
	    queryObject.setItemID(programID);
	    
	    RadioProgramMetadata programMetadata = cacheProvider.getSingle(queryObject);
	    if (programMetadata == null){
		numNullResults++;
		logger.error("GetProgramDetails: Unable to retrieve program info for: "+programID);
		continue;
	    }
	    logger.debug("GetProgramDetails: retrieved program " + programMetadata.getItemID());
	    metadata[offset + i] = programMetadata;
	    if (!cacheProvider.assocFieldsInitialized(programMetadata)) {

		programMetadata.populate__history(medialib);
		programMetadata.populate__creators(medialib);
		// XXX	    programMetadata.populate__relatedContent(medialib);
		programMetadata.populate__categories(medialib);
		programMetadata.populate__tags(medialib);

		cacheProvider.setAssocDependencies(programMetadata, programMetadata.getHistory());
		cacheProvider.setAssocDependencies(programMetadata, programMetadata.getCreators());
		cacheProvider.setAssocDependencies(programMetadata, programMetadata.getCategories());
		cacheProvider.setAssocDependencies(programMetadata, programMetadata.getTags());

		cacheProvider.setAssocFieldsInitialized(programMetadata, true);
	    }
	    
	    i++;
	}
    }

    private String formJoinTableString() {
	StringBuilder str = new StringBuilder();
	HashSet<String> tablesDone = new HashSet<String>();

	for (String filterName: activeFilters.keySet()){
	    SearchFilter filter = activeFilters.get(filterName);
	    logger.debug("joinTableString: looking at filter " + filterName);

	    for (String joinTable: filter.getFilterJoinTables()) {
		if (!tablesDone.contains(joinTable)) {
		    str.append("," + joinTable);
		    tablesDone.add(joinTable);
		}
	    }
	}

	if (str.length() > 0)
	    return str.substring(1);
	else
	    return "";
    }

    private String formWhereClause() {
	StringBuilder str = new StringBuilder();

	for (String filterName: activeFilters.keySet()){
	    SearchFilter filter = activeFilters.get(filterName);
	    logger.debug("formWhereString: looking at filter " + filterName);

	    String query = filter.getFilterWhereClause();
	    logger.debug("formWhereClause: adding clause: " + query);
	    if (!query.equals("")) {
		str.append("("); str.append(query); str.append(") AND ");
	    }
	}

	if (str.length() > 0)
	    return str.substring(0, str.length() - 4);
	else
	    return "";
    }

    private String formQueryString() {
	StringBuilder str = new StringBuilder();
	Enumeration<String> e = activeFilters.keys();
	while (e.hasMoreElements()) {
	    String filterName = e.nextElement();
	    SearchFilter filter = activeFilters.get(filterName);
	    logger.debug("formQueryString: looking at filter " + filterName);
	    boolean considerFilter = true;
	    if (filterName.equals(SearchFilter.TEXT_FILTER)) {
		TextFilter textFilter = (TextFilter)filter;
		String selectedText = textFilter.getSelectedText("");
		if (selectedText.equals("")) {
		    considerFilter = false;
		} else {
		    Enumeration<String> e2 = activeFilters.keys();
		    while (e2.hasMoreElements()) {
			String competingFilterName = e2.nextElement();
			if (competingFilterName.equals(SearchFilter.LANGUAGE_FILTER) &&
			   activeFilters.get(SearchFilter.LANGUAGE_FILTER).getSelectedText(LanguageFilter.LANGUAGE_ANCHOR).equals(selectedText))
			    considerFilter = false;
			else if (competingFilterName.equals(SearchFilter.TYPE_FILTER)) 
				 // && activeFilters.get(SearchFilter.TYPE_FILTER).getSelectedText(TypeFilter.TYPE_ANCHOR).equals(selectedText))
			    considerFilter = false;
			else if (competingFilterName.equals(SearchFilter.CREATOR_FILTER) &&
				activeFilters.get(SearchFilter.CREATOR_FILTER).getSelectedText(CreatorFilter.AFFILIATION_ANCHOR).equals(selectedText))
			    considerFilter = false;
			else if (competingFilterName.equals(SearchFilter.CREATOR_FILTER) &&
				activeFilters.get(SearchFilter.CREATOR_FILTER).getSelectedText(CreatorFilter.ROLE_ANCHOR).equals(selectedText))
			    considerFilter = false;
			else if (competingFilterName.equals(SearchFilter.CREATOR_FILTER) &&
				activeFilters.get(SearchFilter.CREATOR_FILTER).getSelectedText(CreatorFilter.NAME_ANCHOR).equals(selectedText))
			    considerFilter = false;
			else if (competingFilterName.equals(SearchFilter.CREATOR_FILTER) &&
				activeFilters.get(SearchFilter.CREATOR_FILTER).getSelectedText(CreatorFilter.LOCATION_ANCHOR).equals(selectedText))
			    considerFilter = false;
//			else if (competingFilterName.equals(SearchFilter.LICENSE_FILTER) &&
//				activeFilters.get(SearchFilter.LICENSE_FILTER).getSelectedText(LicenseFilter.LICENSE_ANCHOR).equals(selectedText))
//			    considerFilter = false;
			else if (competingFilterName.startsWith(SearchFilter.CATEGORY_FILTER) &&
				activeFilters.get(competingFilterName).getSelectedText(CategoryFilter.CATEGORY_ANCHOR).
				toLowerCase().indexOf(selectedText.toLowerCase()) != -1)
			    considerFilter = false;
		    }
		}
	    } 

	    if (considerFilter) {
		String query = filter.getFilterQueryClause();
		logger.debug("formQueryString: adding query: " + query);
		if (!query.equals("")) {
		    str.append("("); str.append(query); str.append(") AND ");
		}
	    }
	}
	
	if (str.length() > 0)
	    return str.substring(0, str.length() - 4);
	else
	    return "";
    }

    public SearchFilter[] getDisplayFilters() {
	return displayFilters.values().toArray(new SearchFilter[]{});
    }

    public SearchFilter[] getActiveFilters() {
	return activeFilters.values().toArray(new SearchFilter[]{});
    }

    public RadioProgramMetadata[] getPrograms() {
	return initialSearchSet;
    }

    public RadioProgramMetadata[] getAllPrograms() {
	String queryString = formQueryString();
	logger.info("GetAllResults: query string = " + queryString);

	String whereClause = formWhereClause();
	logger.info("rerun: initial search where clause = " + whereClause);
	
	String joinTableStr = formJoinTableString();
	logger.info("rerun: form join table string = " + joinTableStr);

	if (queryString.equals("") && whereClause.equals(""))
	    return (new RadioProgramMetadata[0]);

	LibraryProvider.SearchDetails searchDetails = libraryProvider.getMatchingPrograms(queryString, whereClause, 
											  joinTableStr, 0, 
											  numSearchResults);

	if (searchDetails == null) {
	    logger.error("GetAllResults: Null search details returned.");
	    return null;
	}
	
	String[] programIDs = searchDetails.getPrograms();
	if (programIDs != null) {
	    RadioProgramMetadata[] metadatas = new RadioProgramMetadata[programIDs.length];
	    getProgramDetails(programIDs, metadatas, 0);
	    return metadatas;
	} else {
	    logger.error("GetAllResults: returned null");
	    return (new RadioProgramMetadata[0]);
	}
    }

    public int numSearchResults() {
	return numSearchResults - numNullResults;
    }

    public void inferRelevantFilters(int goBackN) {
	if (goBackN > searchTexts.size())
	    goBackN = searchTexts.size();
	
	int size = searchTexts.size();
	for (int i = 0; i < goBackN; i++) {
	    searchTexts.remove(size - i);
	}
	
	if (size - goBackN - 1 >= 0)
	    inferRelevantFilters(searchTexts.get(size - goBackN - 1));
    }

    public void inferRelevantFilters(String selectedText) {
	logger.info("inferRelevantFilters: finding filters for " + selectedText);
	if (!selectedText.equals(""))
	    searchTexts.add(selectedText);

	Hashtable<String, ArrayList<String>> filterMatches = libraryProvider.getApplicableAnchors(selectedText);
	if (filterMatches != null) {
	    Enumeration<String> e = filterMatches.keys();
	    while (e.hasMoreElements()) {
		String filterName = e.nextElement();
		SearchFilter filter = null;
		
		boolean filterDisplay = false;
		logger.info("inferRelevantFilters: examining " + filterName + ":" + filters.get(filterName));
		if (filterName.startsWith(SearchFilter.CATEGORY_FILTER) && filters.get(filterName) == null) {
		    String parentNodeName = filterName.split(SearchFilter.CATEGORY_FILTER + "_")[1];
		    String childNodeName = filterMatches.get(filterName).get(0).split(CategoryFilter.CATEGORY_ANCHOR + "_")[1];
		    logger.info("inferRelevantFilters: found category filter. parent = " + parentNodeName + ": child = " + childNodeName);
		    Category parentNode = cacheProvider.getSingle(Category.getDummyObjectByNonPrimaryKey(parentNodeName));
		    if (parentNode != null) {
			if (!cacheProvider.assocFieldsInitialized(parentNode)) {
			    parentNode.getAllChildren(medialib);
			    cacheProvider.setAssocFieldsInitialized(parentNode, true);
			}
		    }
		    Category childNode = cacheProvider.getSingle(Category.getDummyObjectByNonPrimaryKey(childNodeName));
		    Category[] allChildren = null;
		    if (childNode != null) {
			if (!cacheProvider.assocFieldsInitialized(childNode)) {
			    allChildren = childNode.getAllChildren(medialib);
			    // XXX no dependencies initialized for categories
			    cacheProvider.setAssocFieldsInitialized(childNode, true);
			} else {
			    allChildren = childNode.getAllChildren();
			}

		    }
		    CategoryFilter categoryFilter = new CategoryFilter();
		    categoryFilter.setParentNode(parentNode);
		    categoryFilter.setSelectedNode(childNode);
		    categoryFilter.setAllChildren(allChildren);
		    
		    filter = categoryFilter;
		    filterDisplay = true;
		} else if (filters.get(filterName) != null) {
		    filter = filters.get(filterName);
		    filterDisplay = true;
		    logger.info("inferRelevantFilter: found filter " + filter);
		}

		if (filterDisplay) {
		    displayFilters.put(filterName, filter);
		    String[] applicableAnchors = filterMatches.get(filterName).toArray(new String[]{});
		    for (String anchor: applicableAnchors) {
			filter.activateAnchor(anchor, selectedText);
		    }
		}
	    }
	} else {
	    logger.error("inferRelevantFilters: returned null");
	}
	
	textFilter.activateAnchor(TextFilter.TITLE_ANCHOR, selectedText);
	textFilter.activateAnchor(TextFilter.DESC_ANCHOR, selectedText);
	//textFilter.activateAnchor(TextFilter.TAGS_ANCHOR, selectedText);
	textFilter.activateAnchor(TextFilter.CREATOR_ANCHOR, selectedText);
	textFilter.activateAnchor(TextFilter.ENTITY_ANCHOR, selectedText);
	logger.info("inferRelevantFilters: Leaving");
    }

    public void inferActiveFilters() {
	logger.debug("inferActiveFilters: inferring active filters");
	Enumeration<String> e = displayFilters.keys();
	while (e.hasMoreElements()) {
	    String filterName = e.nextElement();
	    SearchFilter filter = displayFilters.get(filterName);
	    if (filterName.equals(SearchFilter.LANGUAGE_FILTER) || filterName.equals(SearchFilter.CREATOR_FILTER))
		addActiveFilter(filter);
	    if (filterName.startsWith(SearchFilter.CATEGORY_FILTER) && numActiveCategoryFilters < MAX_ACTIVE_CATEGORY_FILTERS)
		addActiveFilter(filter);
	}
	logger.debug("inferActiveFilters: leaving");
    }

    public int numActiveFilters() {
	return activeFilters.size();
    }

    private void addActiveFilter(SearchFilter filter) {
	logger.debug("addActiveFilter: activating filter " + filter.getFilterName());
	if (activeFilters.get(filter.getFilterName()) == null) {
	    activeFilters.put(filter.getFilterName(), filter);
	    if (filter.getFilterName().startsWith(SearchFilter.CATEGORY_FILTER))
		numActiveCategoryFilters ++;
	}
    }

    private void removeActiveFilter(String filterName) {
	logger.debug("removeActiveFilter: deactivating filter " + filterName);
	activeFilters.remove(filterName);
	if (filterName.startsWith(SearchFilter.CATEGORY_FILTER))
	    numActiveCategoryFilters--;
    }


}