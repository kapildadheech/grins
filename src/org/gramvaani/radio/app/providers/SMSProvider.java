package org.gramvaani.radio.app.providers;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class SMSProvider extends RSProviderBase {
        
    public final static String SUCCESS = "SUCCESS";
    public final static String FAILURE = "FAILURE";

    //provider states
    protected final static int CONFIGURING          = -1;
    protected final static int LISTENING            = 0;

    protected int state = CONFIGURING;

    protected String SMSServiceInstanceName;
    protected boolean rscActive = true;
    protected boolean SMSDisconnected = true;

    protected String SMSServiceID = "";
    protected Hashtable<String, SMSListenerCallback> listeners = null;

    protected ExecutorService listenerExecutor;
    protected final static int POOL_THREAD_COUNT = 1;
    long lastCommandTime = 0;
    final static int MIN_COMMAND_INTERVAL = 1000;

    public SMSProvider(UIService uiService) {
	super (uiService, RSApp.SMS_PROVIDER); 
	SMSServiceInstanceName = uiService.getProviderServiceInstance(RSApp.SMS_PROVIDER, org.gramvaani.radio.rscontroller.services.SMSService.SMS_STR);
	listeners = new Hashtable<String, SMSListenerCallback>();
	listenerExecutor = Executors.newFixedThreadPool(POOL_THREAD_COUNT);
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    listenerExecutor.shutdown();
		}
	    });
    }
    
    public synchronized int activate() {
	logger.info("Activate: Provider Activated");
	setState(CONFIGURING);
	ServiceInterestMessage message = new ServiceInterestMessage("", RSController.RESOURCE_MANAGER, getName(), 
								    new String[] {SMSServiceInstanceName});
	int retVal = sendCommand(message);
	if (retVal != 0)
	    rscActive = false;
	return retVal;
    }

    public boolean isActive(){
	return (state() != CONFIGURING);
    }

    public synchronized void registerWithProvider(ListenerCallback listener) {
	if (listener == null) {
	    logger.error("RegisterWithProvider: null listener.");
	    return;
	}
	listeners.put(listener.getName(), (SMSListenerCallback)listener);
    }

    public synchronized void unregisterWithProvider(String listenerName) {
	if (listenerName == null) {
	    logger.error("UnregisterWithProvider: null listenerName.");
	    return;
	}
	listeners.remove(listenerName);
    }

    public synchronized void receiveMessage(IPCMessage message){
	logger.debug("Received Message from: "+message.getSource()+" type: "+message.getMessageClass());

	if (message instanceof SMSAckMessage) {
	    SMSAckMessage ackMessage = (SMSAckMessage)message;
	    handleSMSAckMessage(ackMessage.getAck(), ackMessage.getCommand(), ackMessage.getOptions(), ackMessage.getError());

	} else if (message instanceof SMSEventMessage) {
	    SMSEventMessage eventMessage = (SMSEventMessage)message;
	    handleSMSEventMessage(message.getSource(), eventMessage.getEvent(), eventMessage.getOptions());
	    
	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    boolean disableUI = false;
	    String disconnectedService = "";
	    for (String service: notifyMessage.getInactivatedServices()) {
		if (service.equals(SMSServiceInstanceName)) {
		    disconnectedService = service;
		    SMSDisconnected = true;
		    disableUI = true;
		} else if (service.equals(RSController.RESOURCE_MANAGER)) {
		    disconnectedService = service;
		    disableUI = true;
		}
	    }

	    if (disableUI) {
		logger.debug("ReceiveMessage: Found "+disconnectedService+" disconnected");
		String[] error;
		error = new String[]{RSController.ERROR_SERVICE_DISCONNECTED, disconnectedService};
		enableSMSUI(false, error);
	    }
	    
	    for (String service: notifyMessage.getActivatedServices()){
		if (service.equals(SMSServiceInstanceName)) {
		    logger.debug("ReceiveMessage: Got " + SMSServiceInstanceName + " active now.");
		    if (!SMSDisconnected) {
			setState(LISTENING);
			activateSMS(true, new String[]{});
		    } else {
			SMSDisconnected = false;
			enableSMSUI(true, new String[]{});
		    }
		} else if (service.equals(RSController.RESOURCE_MANAGER)){
		    logger.debug("ReceiveMessage: Got RSController active now.");
		    if (!rscActive){
			logger.debug("ReceiveMessage: Calling activate.");
			rscActive = true;
			activate();
		    } else {
			//Do nothing
		    }
		}
	    }

	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();
	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(SMSServiceInstanceName))
		    SMSServiceConnected(activatedIDs[i]);
	    }

	} else if (message instanceof ServiceAckMessage){
	    ServiceAckMessage serviceAckMessage = (ServiceAckMessage) message;
	    String[] activeServices = serviceAckMessage.getActiveServices();
	    String[] activeIDs = serviceAckMessage.getActiveServiceIDs();
	    for (int i = 0; i < activeServices.length; i++) {
		String service = activeServices[i];
		if (service.equals(SMSServiceInstanceName)) {
		    SMSDisconnected = false;
		    setState(LISTENING);
		    SMSServiceConnected(activeIDs[i]);
		    activateSMS(true, new String[]{});
		}
	    }

	    for (String service: serviceAckMessage.getInactiveServices()){
		if (service.equals(SMSServiceInstanceName)) {
		    SMSDisconnected = false;
		    setState(CONFIGURING);
		    activateSMS(false, new String[]{RSController.ERROR_SERVICE_DOWN, service});
		}
	    }
	} else if (message instanceof StatusCheckResponseMessage) {
	    StatusCheckResponseMessage responseMessage = (StatusCheckResponseMessage) message;
	    handleStatusCheckResponseMessage(responseMessage.getResponse(), responseMessage.getOptions());
	}

    }

    /* Calls coming in from the UI*/
    public synchronized boolean sendSMS(String smsLine, String message, String[] phoneNos) {

	for(String phoneNo :phoneNos)
	    logger.info("GRINS_USAGE:SEND_SMS:" + phoneNo + ":" + message);

	String phoneNosCSV = StringUtilities.getCSVFromArray(phoneNos);
	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(SMSCommandMessage.SMSLINE, smsLine);
	options.put(SMSCommandMessage.MESSAGE, message);
	options.put(SMSCommandMessage.PHONE_NOS, phoneNosCSV);
	return sendSMSCommand(SMSCommandMessage.SEND, options);
    }

    public synchronized boolean sendPending() {
	return sendSMSCommand(SMSCommandMessage.SEND_PENDING);
    }

    public synchronized boolean cancelSend(String[] smsIDs) {
	String smsIDsCSV = StringUtilities.getCSVFromArray(smsIDs);
	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(SMSCommandMessage.SMS_IDS, smsIDsCSV);
	return sendSMSCommand(SMSCommandMessage.CANCEL_SEND, options);
    }

    public synchronized boolean checkStatus(){
	StatusCheckRequestMessage checkMessage = new StatusCheckRequestMessage("", SMSServiceInstanceName, getName(), "");
	int retVal = sendCommand(checkMessage);

	if (retVal == 0){
	    logger.info("CheckStatus: command sent.");
	    return true;
	} else {
	    logger.error("CheckStatus: Unable to send command.");
	    return false;
	}
    }
    
    /* Calls from UI end here*/

    protected boolean sendSMSCommand(String command) {
	return sendSMSCommand(command, null);
    }

    protected boolean sendSMSCommand(String command, Hashtable<String,String> options) {
	if (command == null || command.equals("")) {
	    logger.error("SendSMSCommand: null command.");
	    return false;
	}
	
	long currentTime = System.currentTimeMillis();
	if ((currentTime - lastCommandTime) < MIN_COMMAND_INTERVAL){
	    try {
		long sleepTime = MIN_COMMAND_INTERVAL - (currentTime - lastCommandTime);
		Thread.sleep(sleepTime);
	    } catch (Exception e){}
	}
	
	SMSCommandMessage message = new SMSCommandMessage("", SMSServiceInstanceName, getName(), command, options);
	int retVal = sendCommand(message);
	if (retVal == 0) {
	    logger.info("SendSMSCommand: Command sent:" + command);
	    lastCommandTime = currentTime;
	    return true;
	} else {
	    logger.error("SendSMSCommand: Failed to send command:" + command);
	    return false;
	}
    }

    protected void SMSServiceConnected(String newID) {
	logger.info("SMSServiceConnected with ID: "+newID + " oldID=" + SMSServiceID);
	if (!SMSServiceID.equals("") && !SMSServiceID.equals(newID)) {
	    activateSMS(false, new String[]{RSController.ERROR_SERVICE_DOWN, SMSServiceInstanceName});
	    activateSMS(true,  new String[]{});
	}
	SMSServiceID = newID;
    }

    /* Callbacks to listeners */
    protected void activateSMS(final boolean activate, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (SMSListenerCallback listener: listeners.values())
			listener.activateSMS(activate, error);
		}
	    });
    }

    protected void enableSMSUI(final boolean enable, final String[] error) {
	listenerExecutor.execute(new Runnable() {
		public void run() {
		    for (SMSListenerCallback listener: listeners.values())
			listener.enableSMSUI(enable, error);
		}
	    });
    }

    protected void SMSStatusUpdate(final boolean status, Hashtable<String, String> options){
	final String[] availableLibs = StringUtilities.getArrayFromCSV(options.get(SMSEventMessage.SMSLIB_OK));
	final String[] unavailableLibs = StringUtilities.getArrayFromCSV(options.get(SMSEventMessage.SMSLIB_ERROR));

	listenerExecutor.execute(new Runnable(){
		public void run(){
		    for (SMSListenerCallback listener: listeners.values())
			listener.SMSStatusUpdate(status, availableLibs, unavailableLibs);
		}
	    });
    }
    /* Callbacks to listeners end here*/

    protected void handleStatusCheckResponseMessage(String response, Hashtable<String, String> options){
	if (response.equals(StatusCheckResponseMessage.TRUE))
	    SMSStatusUpdate(true, options);
	else
	    SMSStatusUpdate(false, options);
    }

    protected void handleSMSAckMessage(String ack, String command, Hashtable<String,String> options, String[] error) {
	logger.info("HandleTelephonyAckMessage: command:" + command + " ack:" + ack);
    }

    protected void handleSMSEventMessage(String source, String event, Hashtable<String,String> options) {
	logger.info("HandleSMSEventMessage: event:" + event);
    }

    public synchronized void receiveErrorMessage(IPCMessage message){
	logger.error("ReceiveErrorMessage: from: "+message.getSource()+" Error: " + message.getParam("serviceerror"));
	boolean serviceDown = false;
	String service = "";
	String serviceName = ""; 
	String errorCode = "";
	if (message instanceof ServiceErrorRelayMessage)
	{
	    serviceName = ((ServiceErrorRelayMessage)message).getServiceName();
	    errorCode = ((ServiceErrorRelayMessage)message).getErrorCode();
	}

	//ErrorCode ignored here.
	if (serviceName != null && (serviceName.equals(SMSServiceInstanceName))){
	    serviceDown = true;
	    service = serviceName;
	    SMSDisconnected = false;
	} else if (serviceName != null &&  serviceName.equals(RSController.RESOURCE_MANAGER)){
	    serviceDown = true;
	    service = serviceName;
	    rscActive = false;
	    SMSDisconnected = false;
	} else {
	    logger.error("ReceiveErrorMessage: Unexpected message "+message.getMessageString());
	}

	if (serviceDown){
	    setState(CONFIGURING);
	    String[] error = new String[]{RSController.ERROR_SERVICE_DOWN, service};
	    activateSMS(false, error);
	}
    }

    
    protected int state() {
	return state;
    }

    protected synchronized void setState(int state) {
	int previousState = this.state;
	this.state = state;
	logger.info("SetState: Previous state: "+previousState+" Current State: "+state);
    }

}
