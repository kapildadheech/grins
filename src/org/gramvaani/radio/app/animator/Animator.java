package org.gramvaani.radio.app.animator;

import java.util.*;
import java.util.concurrent.*;
import org.gramvaani.utilities.*;

public class Animator implements Runnable {
    Thread animatorThread;
    long prevTime = 0;
    long currTime = 0;
    long sleepTime = 0;
    long elapsedTime;
    int numWorkers = 1;

    Hashtable<Animated, ArrayList<AnimationContext>> contextHash;
    final LinkedBlockingQueue<AnimationContext> dispatchQueue;

    static final Object NULLKEY = (Object)"__NULLKEY__";
    final LogUtilities logger = null;

    public Animator(int numWorkers){
	prevTime = System.currentTimeMillis();
	contextHash = new Hashtable<Animated, ArrayList<AnimationContext>> ();
	dispatchQueue = new LinkedBlockingQueue<AnimationContext>();
	for (int i = 0; i < numWorkers; i++)
	    new Thread(){
		public void run(){
		    while(true){
			AnimationContext context = null;
			try{
			    context = dispatchQueue.take();
			}catch(InterruptedException e){}
			context.animated.animate(context.getStartTime(), currTime - context.getLastTime(), context.getKey());
			context.setLastTime(currTime);
		    }
		}
	    }.start();

	animatorThread = new Thread(this, "AnimatorThread");
	animatorThread.start();
    }
    
    public void run(){
	while (true){
	    synchronized(this){
		try{
		    wait(sleepTime);
		}catch (InterruptedException e){}
	    }

	    currTime = System.currentTimeMillis();
	    elapsedTime = currTime - prevTime;
	    sleepTime = Long.MAX_VALUE;

	    for (ArrayList<AnimationContext> contextList: contextHash.values()){
		for (AnimationContext context: contextList) {
		    long remainingTime = context.getRemainingTime();
		    if (remainingTime <= elapsedTime){
			dispatch(context);
			remainingTime = context.getRefreshTime();
			}else {
			remainingTime -= elapsedTime;
		    }
		    context.setRemainingTime(remainingTime);
		    if (remainingTime < sleepTime)
			sleepTime = remainingTime;
		    
		}
	    }
	    if (sleepTime == Long.MAX_VALUE)
		sleepTime = 0;
	    prevTime = currTime;
	}
    }

    void dispatch(AnimationContext context) {
	dispatchQueue.add(context);
    }

    public synchronized void start(Animated animated, int refreshTime, Object key){
	long startTime = System.currentTimeMillis();
	AnimationContext context = new AnimationContext(animated, refreshTime, key, startTime);
	ArrayList<AnimationContext> list = contextHash.get(animated);

	if (list == null){
	    list = new ArrayList<AnimationContext>();
	    contextHash.put(animated, list);
	}
	list.add(context);

	dispatch(context);
	context.setOffset(startTime - prevTime);
	if ((refreshTime < sleepTime)||(sleepTime == 0))
	    sleepTime = refreshTime;
	notifyAll();
    }

    public synchronized void stop(Animated animated, Object key){
	ArrayList<AnimationContext> list = contextHash.get(animated);
	if (key == null)
	    key = NULLKEY;
	AnimationContext toRemove = null;
	for (AnimationContext context: list){
	    if (context.getKey().equals(key))
		toRemove = context;
	}
	list.remove(toRemove);
    }

    class AnimationContext {
	Animated animated;
	long remainingTime;
	Object key;
	long startTime;
	long refreshTime;
	long lastTime;
	
	public AnimationContext(Animated animated, long refreshTime, Object key, long startTime){
	    this.animated = animated;
	    this.remainingTime = refreshTime;
	    this.refreshTime = refreshTime;
	    if(key != null)
		this.key = key;
	    else
		this.key = NULLKEY;
	    this.startTime = startTime;
	    this.lastTime = startTime;
	}

	public Animated getAnimated(){
	    return animated;
	}
	
	public long getRemainingTime(){
	    return remainingTime;
	}

	public Object getKey(){
	    return key;
	}

	public long getRefreshTime(){
	    return refreshTime;
	}
	
	public long getStartTime(){
	    return startTime;
	}

	public long getLastTime(){
	    return lastTime;
	}

	public void setRemainingTime(long remainingTime){
	    this.remainingTime = remainingTime;
	}

	public void setOffset(long offsetTime){
	    remainingTime += offsetTime;
	}
	
	public void setLastTime (long lastTime){
	    this.lastTime = lastTime;
	}
    }
    
}
