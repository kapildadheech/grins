package org.gramvaani.radio.app.animator;

public interface Animated {

    Animator animator = null;
    public void animate(long startTime, long elapsedTime, Object key);
}