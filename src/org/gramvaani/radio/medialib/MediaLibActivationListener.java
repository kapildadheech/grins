package org.gramvaani.radio.medialib;

import org.gramvaani.radio.medialib.MediaLib;

public interface MediaLibActivationListener {

    public void mediaLibUp(MediaLib medialib);

}