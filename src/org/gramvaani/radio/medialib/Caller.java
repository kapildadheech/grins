package org.gramvaani.radio.medialib;

import java.util.*;

public class Caller extends Metadata {
    protected String _callerID;
    protected String _callerName;

    public String dump() {
	return "Caller: " + _callerID + ":" + _callerName;
    }

    public Caller(String callerID, String callerName) {
	_callerID = callerID;
	if(callerName == null)
	    _callerName = "";
	else
	    _callerName = callerName;
    }

    public Caller(String callerID) {
	_callerID = callerID;
	_callerName = "";
    }

    public void setCallerID(String callerID) {
	_callerID = callerID;
    }

    public String getCallerID() {
	return _callerID;
    }

    public String getCallerName() {
	return _callerName;
    }

    public void setCallerName(String name) {
	_callerName = name;
    }

    public static Caller getDummyObject(String callerID) {
	Caller caller = new Caller(callerID);
	return caller;
    }

    public String unique() {
	return _callerID.toString();
    }

    public String uniqueNull() {
	return "-1";
    }

    public Caller cloneObject() {
	return getDummyObject(_callerID);
    }

    public void truncateFields(){

    }
}
