package org.gramvaani.radio.medialib;

public class LengthFilter extends SearchFilter {
    public long selectedLength;
    static final int LENGTH_MARGIN_PERCENT = 15;
    static final int MINUTE = 60 * 1000;

    public static final String LENGTH_ANCHOR = "LENGTH_ANCHOR"; boolean lengthActive = false;
    public static final String lengthKey = Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getLengthFieldName();

    public LengthFilter() {
	this.selectedLength = -1;
    }

    public String getFilterName() {
	return SearchFilter.LENGTH_FILTER;
    }

    public String getFilterLabel() {
	return "Length";
    }

    public String getFilterQueryClause() {
	return "";
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if(selectedLength != -1 && lengthActive) {
	    long minLength = (long) selectedLength * (100 - LENGTH_MARGIN_PERCENT)/100;
	    long maxLength = (long) selectedLength * (100 + LENGTH_MARGIN_PERCENT)/100;

	    if (selectedLength - minLength < MINUTE)
		minLength = selectedLength - MINUTE;

	    if (maxLength - selectedLength < MINUTE)
		maxLength = selectedLength + MINUTE;
	    
	    str.append(lengthKey); str.append(" = -1 or "); 
	    str.append(lengthKey); //str.append(" < "); str.append(selectedLength);
	    str.append(" between ");
	    str.append(minLength);
	    str.append(" and ");
	    str.append(maxLength);
	}
	
	return str.toString();
    }

    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(RadioProgramMetadata.class)};
    }

    public String getSelectedText(String anchorID) {
	return "";
    }

    public void setSelectedLength(long length) {
	this.selectedLength = length;
    }

    public long getSelectedLength() {
	return selectedLength;
    }

    public void activateAnchor(String anchor, String anchorText) {
	// this will never get called on length filter
	if(anchor.equals(LENGTH_ANCHOR)) {
	    lengthActive = true;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }

    public static long getMaxLength(MediaLib medialib) {
	Object maxLength = medialib.mathOp(new RadioProgramMetadata(""), RadioProgramMetadata.getLengthFieldName(), MediaLib.MAX_OP);
	if(maxLength instanceof Long) {
	    return ((Long)maxLength).longValue();
	} else {
	    return -1;
	}
    }

}
