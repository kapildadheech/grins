package org.gramvaani.radio.medialib;

public class PlaylistItem extends Metadata {
    protected Integer _playlistID;
    protected String _itemID;
    protected Integer _order;
    protected Integer _liveItem;
    protected Integer _playBackground;
    protected Long _liveDuration;
    protected Long _fadeout;

    public PlaylistItem(int playlistID) {
	_playlistID = new Integer(playlistID);
	_itemID = "";
	_order = new Integer(-1);
	_liveItem = new Integer(-1);
	_playBackground = new Integer(-1);
	_liveDuration = new Long(-1);
	_fadeout = new Long(-1);
    }

    public PlaylistItem(int playlistID, String itemID, int order) {
	_playlistID = new Integer(playlistID);
	_itemID = itemID;
	_order = new Integer(order);
	_liveItem = new Integer(-1);
	_playBackground = new Integer(-1);
	_liveDuration = new Long(-1);
	_fadeout = new Long(-1);
    }

    public PlaylistItem(String itemID){
	_playlistID = -1;
	_itemID = itemID;
	_order = -1;
	_liveItem = -1;
	_playBackground = -1;
	_liveDuration = -1L;
	_fadeout = -1L;
	
    }

    public String dump() {
	return "PlaylistItem: " + _playlistID + ":" + _itemID + ":" + _order + ":" + _liveItem + ":" + _playBackground + 
	    ":" + _liveDuration + ":" + _fadeout;
    }


    public int getPlaylistID() {
	return _playlistID.intValue();
    }

    public void setPlaylistID(int playlistID) {
	_playlistID = new Integer(playlistID);
    }

    public String getItemID() {
	return _itemID;
    }

    public void setItemID(String itemID) {
	_itemID = itemID;
    }

    public int getOrder() {
	return _order.intValue();
    }

    public void setOrder(int order) {
	_order = new Integer(order);
    }

    public boolean getLiveItem() {
	if(_liveItem.intValue() <= 0)
	    return false;
	else
	    return true;
    }

    public void setLiveItem(boolean liveItem) {
	if(liveItem)
	    _liveItem = new Integer(1);
	else
	    _liveItem = new Integer(0);
    }

    public boolean getPlayBackground() {
	if(_playBackground.intValue() <= 0)
	    return false;
	else
	    return true;
    }

    public void setPlayBackground(boolean playBackground) {
	if(playBackground)
	    _playBackground = new Integer(1);
	else
	    _playBackground = new Integer(0);
    }

    public long getLiveDuration() {
	return _liveDuration.longValue();
    }

    public void setLiveDuration(long liveDuration) {
	_liveDuration = new Long(liveDuration);
    }

    public long getFadeout() {
	return _fadeout.longValue();
    }

    public void setFadeout(long fadeout) {
	_fadeout = new Long(fadeout);
    }

    public String unique() {
	return "_playlistID, _itemID";
    }

    public static PlaylistItem getDummyObject(String itemID){
	return new PlaylistItem(itemID);
    }

    public static PlaylistItem getDummyObject(int playlistID) {
	return new PlaylistItem(playlistID);
    }

    public PlaylistItem cloneObject() {
	return getDummyObject(-1);
    }

    public void truncateFields(){

    }

}