package org.gramvaani.radio.medialib;

import java.util.*;

public class Poll extends Metadata {
    //Poll status
    public static final int NOT_RUNNING = 0;
    public static final int RUNNING     = 1;

    public static final String STATUS_STR[] = {"Not Running", "Running"};

    //Poll type
    public static final int FIXED_OPTIONS = 0;
    public static final int OPEN_OPTIONS  = 1;

    Integer _pollID;
    String _pollName;
    String _keyword;
    Integer _pollType;
    Integer _state;
    Long _startTime;
    Long _endTime;

    public String dump() {
	return "Poll: " + _pollID + ":" + _pollName + ":" + _keyword + ":" + _pollType + ":" + _state;
    }

    public Poll(String pollName, String keyword, int type, int state) {
	_pollID = new Integer(-1);
	_pollName = pollName;
	_keyword = keyword;
	_pollType = type;
	_state = state;
	_startTime = -1L;
	_endTime = -1L;
    }

    public Poll(int pollID) {
	this();
	_pollID = pollID;
    }

    public Poll() {
	_pollID = -1;
	_pollName = "";
	_keyword = "";
	_pollType = -1;
	_state = -1;
	_startTime = -1L;
	_endTime = -1L;
    }

    public int getID() {
	return _pollID;
    }

    public String getPollName() {
	return _pollName;
    }

    public void setPollName(String pollName) {
	_pollName = pollName;
    }

    public static String getPollNameFieldName() {
	return "_pollName";
    }

    public String getKeyword() {
	return _keyword;
    }

    public void setKeyword(String keyword) {
	_keyword = keyword;
    }

    public static String getKeywordFieldName() {
	return "_keyword";
    }

    public int getPollType() {
	return _pollType;
    }

    public void setPollType(int type) {
	_pollType = type;
    }
    
    public static String getPollTypeFieldName() {
	return "_pollType";
    }

    public int getState() {
	return _state;
    }

    public void setState(int state) {
	_state = state;
    }

    public static String getStateFieldName() {
	return "_state";
    }

    public Long getStartTime() {
	return _startTime;
    }

    public void setStartTime(Long time) {
	_startTime = time;
    }

    public static String getStartTimeFieldName() {
	return "_startTime";
    }

    public Long getEndTime() {
	return _endTime;
    }

    public void setEndTime(Long time) {
	_endTime = time;
    }

    public static String getEndTimeFieldName() {
	return "_endTime";
    }

    public static Poll getDummyObject(Integer pollID) {
	Poll poll = new Poll(pollID);
	return poll;
    }

    public String primaryKey() {
	return "_pollID";
    }

    public String unique() {
	return _pollID.toString();
    }

    public String uniqueNull() {
	return "-1";
    }

    public Poll cloneObject() {
	return getDummyObject(new Integer(-1));
    }

    public void truncateFields(){

    }
}
