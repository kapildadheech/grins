package org.gramvaani.radio.medialib;

import java.util.ArrayList;
import java.util.HashSet;

public class TagsFilter extends SearchFilter {

    public static String TAGS_ANCHOR = "TAGS_ANCHOR";
    
    final static String programItem = Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getItemIDFieldName();
    //final static String categoryItem = Metadata.getClassName(ItemCategory.class) +"." + ItemCategory.getItemIDFieldName();

    //final static String categoryNode = Metadata.getClassName(ItemCategory.class) +"." + ItemCategory.getCategoryNodeIDFieldName();
    final static String categoryItem = ItemCategory.getItemIDFieldName();
    final static String categoryNode = ItemCategory.getCategoryNodeIDFieldName();
    final static String itemCategoryAlias = "ic";

    Integer[][] categoryIDs = null;

    public String getFilterName(){
	return SearchFilter.TAG_FILTER;
    }

    public String getFilterLabel(){
	return "Tags";
    }

    public String getFilterQueryClause(){
	return "";
    }

    public void setCategories(Integer[][] categoryIDs){
	this.categoryIDs = categoryIDs;
    }

    public String getFilterWhereClause(){

	if (categoryIDs == null || categoryIDs.length == 0 || categoryIDs[0].length == 0)
	    return "";
	

	StringBuilder str = new StringBuilder();
	int index = 0;
	for (Integer[] set: categoryIDs){
	    str.append(programItem);
	    str.append(" = ");
	    str.append(itemCategoryAlias);
	    str.append(index++);
	    str.append(".");
	    str.append(categoryItem);
	    str.append(" and ");
	}
	
	for (int j = 0; j < categoryIDs.length; j++){
	    str.append(" ( ");
	    str.append(itemCategoryAlias);
	    str.append(j);
	    str.append(".");
	    str.append(categoryNode);
	    str.append(" in ( ");
	    for (int i = 0; i < categoryIDs[j].length; i++){
		str.append(categoryIDs[j][i]);
		if (i < categoryIDs[j].length - 1)
		    str.append(",");
		else
		    str.append(" )");
	    }
	    str.append(" ) ");
	    if (j < categoryIDs.length - 1)
		str.append(" and ");
	}

	return str.toString();
    }

    public String[] getFilterJoinTables(){
	/*
	return new String[]{Metadata.getClassName(RadioProgramMetadata.class), Metadata.getClassName(ItemCategory.class)};
	*/
	if (categoryIDs == null)
	    return new String[]{Metadata.getClassName(RadioProgramMetadata.class), Metadata.getClassName(ItemCategory.class)};

	ArrayList<String> list = new ArrayList<String>();
	list.add(Metadata.getClassName(RadioProgramMetadata.class));
	int i = 0;

	for (Integer[] set: categoryIDs){
	    list.add(Metadata.getClassName(ItemCategory.class) + " " + itemCategoryAlias + (i++));
	}

	return list.toArray(new String[list.size()]);
    }

    public String getSelectedText(String text){
	return "";
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return null;
    }
}