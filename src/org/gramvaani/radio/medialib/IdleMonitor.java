package org.gramvaani.radio.medialib;

import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;

import java.io.*;

public class IdleMonitor implements Runnable {
    protected StationConfiguration stationConfiguration;
    protected IdleMonitorCallback callback;
    protected CircularBuffer circularBuf;
    protected final LogUtilities logger;

    protected String monitorCommand;
    
    private int tokenNum = -1;
    boolean exit = false;

    public IdleMonitor(StationConfiguration stationConfiguration, IdleMonitorCallback callback, String logPrefix) {
	this.stationConfiguration = stationConfiguration;
	this.callback = callback;

	if (stationConfiguration.getOperatingSystem().equals(StationConfiguration.WINDOWS))
	    monitorCommand = "top";
	else
	    monitorCommand = "mpstat";

	String loggerName = logPrefix + ":IdleMonitor";
	this.circularBuf = new CircularBuffer(STAT_AMOUNT, loggerName);
	logger = new LogUtilities(loggerName);

	Thread t = new Thread(this, "IdleMonitorThread");
	t.start();
	logger.debug("IdleMonitor: Initialized with " + monitorCommand);
    }

    protected int parseLine(String readLine, String command) {
	if (command.equals("mpstat")) {

	    String[] tokens;
	    int i;
	    if (readLine.equals("") || readLine.startsWith("Linux")) {
		return -1;
	    }
	    if (readLine.indexOf("CPU") != -1) {
		tokens = readLine.split(" ");
		i = 0;
		for(String token: tokens) {
		    if(token.indexOf("idl") != -1) {
			tokenNum = i;
			break;
		    }
		    i++;
		}
		return -1;
	    }
	    int idlePercent = 0;
	    if (tokenNum > -1) {
		tokens = readLine.split(" ");
		idlePercent = (int)(Float.parseFloat(tokens[tokenNum]));
	    }
	    logger.debug("IdleMonitor: Idle percent: " + idlePercent);
	    return idlePercent;
	    
	    /*
	} else if(command.equals("vmstat")) {

	    String[] tokens;
	    int i;
	    if(readLine.equals("") || readLine.startsWith("procs")) {
		return -1;
	    }
	    if(readLine.indexOf("id") != -1) {
		tokens = readLine.split(" ");
		i = 0;
		for(String token: tokens) {
		    if(token.indexOf("id") != -1) {
			tokenNum = i;
			break;
		    }
		    i++;
		}
		return -1;
	    }
	    int idlePercent = 0;
	    if(tokenNum > -1) {
		tokens = readLine.split(" ");
		idlePercent = (int)(Float.parseFloat(tokens[tokenNum]));
	    }
	    logger.debug("IdleMonitor: Idle percent: " + idlePercent);
	    return idlePercent;
	    
	    */
	} else if (command.equals("top")) {
	    if (!readLine.startsWith("Cpu(s)"))
		return -1;
	    return (int) Float.parseFloat(readLine.split("\\s+")[7].split("%")[0]);
	} else {
	    return -1;
	}
    }

    public void run() {
	while(true) {
	    try {
		logger.debug("IdleMonitor: monitor command = " + monitorCommand);
		ProcessBuilder pb = null;
		if (monitorCommand.equals("top")){
		    pb = new ProcessBuilder(monitorCommand, "-b -d"+stationConfiguration.getStringParam(StationConfiguration.IDLE_MONITOR_POLL_INTERVAL));
		} else {
		    pb = new ProcessBuilder(monitorCommand, stationConfiguration.getStringParam(StationConfiguration.IDLE_MONITOR_POLL_INTERVAL));
		}
		Process p = pb.start();
		try {
		    BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    String readLine;
		    while ((readLine = in.readLine()) != null) {
			readLine = readLine.replaceAll("\\s+", " ");
			int idlePercent = parseLine(readLine, monitorCommand);

			if (idlePercent == -1)
			    continue;
			
			addStat(idlePercent);
			if (isIdle()) {
			    logger.debug("IdleMonitor: Detected Idle.");
			    callback.detectedIdle();
			} else {
			    logger.debug("IdleMonitor: Not idle");
			}
			callback.keepListeningForPreempts();
		    }
		    if (!exit)
			logger.error("IdleMonitor: monitor exited: " + readLine);
		} catch(Exception e) {
		    logger.error("IdleMonitor: Unable to read from process", e);
		}
	    } catch(Exception e) {
		logger.error("IdleMonitor: Failed to create Idle process", e);
		try {
		    Thread.sleep(60000);
		} catch(Exception e2) { }
	    }

	    try {
		Thread.sleep(60000);
	    } catch(Exception e3) { }
	}
    }
    
    protected static int STAT_AMOUNT = 10;                    // minutes: maintain stats for the last 10 minutes
    protected static int MEAN_THRESH = 90;                    // percent: idle state threshold
    protected static int SPIKE_SENSITIVITY = 2;               // minutes: ignore if spike occured recently

    protected void addStat(int idlePercent) {
	circularBuf.put(idlePercent, System.currentTimeMillis());
    }

    protected boolean isIdle() {
	if (circularBuf.mean() > MEAN_THRESH && !circularBuf.lastExceed(MEAN_THRESH, SPIKE_SENSITIVITY)) {
	    return true;
	} else {
	    return false;
	}
	    
    }

}
