package org.gramvaani.radio.medialib;

import org.gramvaani.utilities.LogUtilities;
import java.util.*;
import java.lang.reflect.*;

public abstract class Metadata implements Cloneable{
    
    protected static Hashtable<Class<? extends Metadata>, String[]> columnNames;

    static {
	columnNames = new Hashtable<Class<? extends Metadata>, String[]>();
    }

    public String primaryKey() {
	return "";
    }

    public String unique() {
	return "";
    }

    public String uniqueNull() {
	return "";
    }

    public abstract Metadata cloneObject();

    public abstract String dump();

    public String toString(){
	return dump();
    }

    protected String[] allowedNulls = new String[]{};
    public String[] getAllowedNullFields() {
	return allowedNulls;
    }
    public void setAllowedNullFields(String[] str) {
	this.allowedNulls = str;
    }

    public static String getClassName(Class classinst) {
	return classinst.getName().split(classinst.getPackage().getName())[1].substring(1);
    }

    
    public static Hashtable<String, Object> getColumnNamesAndValues(Metadata metadata) throws Exception {
	Hashtable<String, Object> colNamesAndValues = new Hashtable<String, Object>();
	Field[] fields = metadata.getClass().getDeclaredFields();
	for (Field field: fields) {
	    if (!(field.getType().getSuperclass() == Metadata.class) && field.getName().startsWith("_") && !(field.getName().startsWith("__"))) {
		colNamesAndValues.put(field.getName(), field.get(metadata));
	    }
	}

	return colNamesAndValues;
    }

    public static Hashtable<String, Metadata[]> getSupplementaryColumnAndValues(Metadata metadata) throws Exception {
	Hashtable<String, Metadata[]> supplementaryData = new Hashtable<String, Metadata[]>();
	Field[] fields = metadata.getClass().getDeclaredFields();
	for (Field field: fields) {
	    if (field.getName().startsWith("__") && field.get(metadata) != null) {
		supplementaryData.put(field.getName(), (Metadata[])(field.get(metadata)));
	    }
	}

	return supplementaryData;
    }

    public static String[] getColumnNames(Metadata metadata) throws Exception {
	String[] retVals = null;
	if ((retVals = columnNames.get(metadata.getClass())) == null){
	    ArrayList<String> arrList = new ArrayList<String>();
	    Field[] fields = metadata.getClass().getDeclaredFields();
	    for (Field field: fields) {
		if (!(field.getType().getSuperclass() == Metadata.class) && field.getName().startsWith("_") && !(field.getName().startsWith("__"))) {
		    arrList.add(field.getName());
		}
	    }
	    
	    retVals = arrList.toArray(new String[]{});
	    columnNames.put(metadata.getClass(), retVals);
	}

	return retVals;
    }

    public static Field[] getNestedFields(Metadata metadata) throws Exception {
	ArrayList<Field> arrList = new ArrayList<Field>();
	Field[] fields = metadata.getClass().getDeclaredFields();
	for (Field field: fields) {
	    if ((field.getType().getSuperclass() ==  Metadata.class) && field.getName().startsWith("_") && !(field.getName().startsWith("__"))) {
		arrList.add(field);
	    }
	}

	return arrList.toArray(new Field[]{});
    }

    public static Field[] getAssocFields(Metadata metadata) throws Exception {
	ArrayList<Field> arrList = new ArrayList<Field>();
	Field[] fields = metadata.getClass().getDeclaredFields();
	for (Field field: fields) {
	    if ((field.getType().getSuperclass() ==  Metadata.class) && field.getName().startsWith("__")) {
		arrList.add(field);
	    }
	}

	return arrList.toArray(new Field[]{});
    }

    public static String getWhereClause(Metadata metadata) throws Exception {
	Hashtable<String, Object> colNamesAndValues = getColumnNamesAndValues(metadata);
	StringBuilder whereClause = new StringBuilder();
	Enumeration<String> e = colNamesAndValues.keys();
	while (e.hasMoreElements()) {
	    String column = e.nextElement();
	    Object value = colNamesAndValues.get(column);
	    if (Metadata.nullAllowed(metadata, column) || value != null && notNull(value)) {
		whereClause.append(column); whereClause.append("='"); whereClause.append(value.toString()); whereClause.append("'");
		whereClause.append(" and ");
	    }
	}
	String whereClauseStr = "";
	if (whereClause.length() > 0) {
	    whereClauseStr = whereClause.substring(0, whereClause.length() - 5);
	}
	
	return whereClauseStr;
    }

    public static void addMetadataToHashtable(Metadata metadata, String prefix, Hashtable<String, String> params) throws Exception {
	Field[] fields = metadata.getClass().getDeclaredFields();
	for (Field field: fields) {
	    if (!(field.getType().getSuperclass() == Metadata.class) && field.getName().startsWith("_") && !(field.getName().startsWith("__"))) {
		params.put(prefix + field.getName(), field.get(metadata).toString());
	    }
	}
    }
    
    public static void getMetadataFromHashtable(Metadata metadata, String prefix, Hashtable<String, String> params) throws Exception {
	Field[] fields = metadata.getClass().getDeclaredFields();
	for (Field field: fields) {
	    if (!(field.getType().getSuperclass() == Metadata.class) && field.getName().startsWith("_") && !(field.getName().startsWith("__"))) {
		initField(field, metadata, params.get(prefix + field.getName()));
	    }
	}
    }

    public static boolean nullAllowed(Metadata metadata, String fieldName) {
	String[] allowedNulls = metadata.getAllowedNullFields();
	for (String str: allowedNulls)
	    if (str.equals(fieldName))
		return true;
	return false;
    }

    public static boolean notNull(Object obj) {
	if (obj instanceof String) {
	    if (((String)obj).equals(""))
		return false;
	    else
		return true;
	} else if (obj instanceof Integer) {
	    if (((Integer)obj).intValue() == -1)
		return false;
	    else
		return true;
	} else if (obj instanceof Long) {
	    if (((Long)obj).longValue() == -1)
		return false;
	    else
		return true;
	}
	
	return false;
    }

    public static void initField(Field field, Metadata metadata, String val) throws Exception {	
	if (field.getType() == String.class) {
	    field.set(metadata, val);
	} else if (field.getType() == Integer.class) {
	    field.set(metadata, new Integer(val));
	} else if (field.getType() == Long.class) {
	    field.set(metadata, new Long(val));
	} else {
	    field.set(metadata, val);
	}
    }

    public abstract void truncateFields();

    @SuppressWarnings("unchecked") 
    public <T extends Metadata> T clone(){
	T obj = null;
	try{
	    obj = (T) super.clone();
	} catch (Exception e){
	    LogUtilities.getDefaultLogger().error("Clone: Unable to clone object.", e);
	}
	return obj;
    }
}
