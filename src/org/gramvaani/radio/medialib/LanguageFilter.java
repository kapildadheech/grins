package org.gramvaani.radio.medialib;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.app.providers.CacheProvider;

import java.util.*;

public class LanguageFilter extends SearchFilter {
    public String selectedLanguage;

    public static final String languageKey = Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getLanguageFieldName();

    public static final String LANGUAGE_ANCHOR = "LANGUAGE_ANCHOR"; 
    boolean languageActive = false;

    public LanguageFilter() {
	this.selectedLanguage = "";
    }

    public String getFilterName() {
	return SearchFilter.LANGUAGE_FILTER;
    }

    public String getFilterLabel() {
	return "Language";
    }

    public String getFilterQueryClause() {
	StringBuilder str = new StringBuilder();
	if(!selectedLanguage.equals("") && languageActive) {
	    str.append(LANGUAGE_ANCHOR); str.append(":\""); str.append(selectedLanguage); str.append("\"");
	}
	
	return str.toString();
    }

    public String getFilterWhereClause() {
	return "";
    }

    public String[] getFilterJoinTables() {
	return new String[]{};
    }

    public String getSelectedText(String anchorID) {
	if(anchorID.equals(LANGUAGE_ANCHOR))
	    return selectedLanguage;
	else
	    return "";
    }

    public void setSelectedLanguage(String language) {
	this.selectedLanguage = language;
    }

    public void activateAnchor(String anchor, String anchorText) {
	if(anchor.equals(LANGUAGE_ANCHOR)) {
	    languageActive = true;
	    selectedLanguage = anchorText;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }

    /*
    public static String[] getLanguages(CacheProvider cacheProvider) {
	Object[] languages = cacheProvider.uniqueFromAnchors(RadioProgramMetadata.getDummyObject(""), RadioProgramMetadata.getLanguageFieldName());
	return getLanguages(languages);
    }
    */

    public static String[] getLanguages(MetadataProvider metadataProvider) {
	Object[] languages = metadataProvider.unique(RadioProgramMetadata.getDummyObject(""), RadioProgramMetadata.getLanguageFieldName());
	return getLanguages(languages);
    }

    public static String[] getLanguages(MediaLib medialib) {
	Object[] languages = medialib.unique(RadioProgramMetadata.getDummyObject(""), RadioProgramMetadata.getLanguageFieldName());
	return getLanguages(languages);
    }

    private static String[] getLanguages(Object[] languages) {
	ArrayList<String> arr = new ArrayList<String>();
	for(Object language: languages)
	    arr.add((String)language);
	return arr.toArray(new String[]{});
    }

}


/*
    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(RadioProgramMetadata.class)};
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if(!selectedLanguage.equals("") && languageActive) {
	    str.append(languageKey); str.append(" = '"); str.append(selectedLanguage); str.append("'");
	}
	
	return str.toString();
    }


*/
