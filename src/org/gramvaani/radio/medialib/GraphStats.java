package org.gramvaani.radio.medialib;

public class GraphStats {
    long minTelecastTime, maxTelecastTime;
    String[] xLabels;
    long[] dataItems;
    long approxBinInterval;
    long maxDataItem;

    public GraphStats() {
	minTelecastTime = -1;
	maxTelecastTime = -1;
	xLabels = new String[]{};
	dataItems = new long[]{};
	approxBinInterval = -1;
	maxDataItem = -1;
    }
    
    public long getMinTelecastTime() {
	return minTelecastTime;
    }

    public void setMinTelecastTime(long minTelecastTime) {
	this.minTelecastTime = minTelecastTime;
    }

    public long getMaxTelecastTime() {
	return maxTelecastTime;
    }

    public void setMaxTelecastTime(long maxTelecastTime) {
	this.maxTelecastTime = maxTelecastTime;
    }

    public String[] getXLabels() {
	return xLabels;
    }

    public void setXLabels(String[] xLabels) {
	this.xLabels = xLabels;
    }

    public long[] getDataItems() {
	return dataItems;
    }

    public void setDataItems(long[] dataItems) {
	this.dataItems = dataItems;
    }

    public long getApproxBinInterval() {
	return approxBinInterval;
    }

    public void setApproxBinInterval(long approxBinInterval) {
	this.approxBinInterval = approxBinInterval;
    }

    public long getMaxDataItem() {
	return maxDataItem;
    }

    public void setMaxDataItem(long maxDataItem) {
	this.maxDataItem = maxDataItem;
    }

    public long getSumDataItems() {
	long sum = 0;

	for (int i = 0; i < dataItems.length; i++) {
	    sum += dataItems[i];
	}

	return sum;
    }
}