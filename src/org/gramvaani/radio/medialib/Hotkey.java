package org.gramvaani.radio.medialib;

import java.util.*;

public class Hotkey extends Metadata {

    protected Integer _keyID;
    protected Integer _color;
    protected String _itemID;

    protected RadioProgramMetadata _program; protected static String foreign_program = "_itemID", local_program="_itemID";


    public Hotkey(int color){
	_keyID = new Integer(-1);
	_color = new Integer(color);
	_itemID = "";
    }

    public Hotkey(){
	_keyID = new Integer(-1);
	_color = new Integer(-1);
	_itemID = "";
    }

    public Hotkey(String itemID){
	_keyID = new Integer(-1);
	_color = new Integer(-1);
	_itemID = itemID;
    }

    public String dump(){
	return "Hotkey: "+_keyID+":"+_itemID+":"+_color;
    }

    public void setProgram(RadioProgramMetadata program){
	_program = program;
    }

    public RadioProgramMetadata getProgram(){
	return _program;
    }

    public void setKeyID(int keyID){
	_keyID = new Integer(keyID);
    }

    public int getKeyID(){
	return _keyID.intValue();
    }

    public void setColor(int color){
	_color = new Integer(color);
    }

    public int getColor(){
	return _color.intValue();
    }

    public void setItemID(String itemID){
	_itemID = itemID;
    }

    public String getItemID(){
	return _itemID;
    }

    public Hotkey cloneObject(){
	return new Hotkey();
    }

    public static String getKeyIDField(){
	return "_keyID";
    }

    public String unique() {
	return _keyID.toString();
    }

    public void truncateFields(){

    }
}