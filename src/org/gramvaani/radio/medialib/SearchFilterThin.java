package org.gramvaani.radio.medialib;

public class SearchFilterThin {
    String filterName;
    String filterLabel;

    public SearchFilterThin(String filterName, String filterLabel) {
	this.filterName = filterName;
	this.filterLabel = filterLabel;
    }

    public String getFilterName() {
	return filterName;
    }

    public String getFilterLabel() {
	return filterLabel;
    }
    
}