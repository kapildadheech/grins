package org.gramvaani.radio.medialib;

import java.util.*;

public class Creator extends Metadata {
    //Any affiliations added to this list should be added to the method
    //getAffiliations
    
    public static final String SCRIPT 	= "scriptwriter";
    public static final String FUNDS 	= "fundingagency";
    public static final String PRODUCER = "producer";
    public static final String ARTIST 	= "artist";
    public static final String CALLER 	= "caller";

    static final HashSet<String> affiliations = new HashSet<String>();
    static {
	affiliations.add(SCRIPT);
	affiliations.add(FUNDS);
	affiliations.add(PRODUCER);
	affiliations.add(ARTIST);
	affiliations.add(CALLER);
    }

    protected String _itemID;
    protected Integer _entityID;
    protected String _affiliation;

    public String dump() {
	return "Creator: " + _itemID + ":" + _entityID + ":" + _affiliation;
    }

    protected Entity _entity;             protected static String foreign_entity = "_entityID", local_entity = "_entityID";

    public Creator(String itemID, int entityID, String affiliation) {
	this._itemID = itemID;
	this._entityID = new Integer(entityID);
	this._affiliation = affiliation;
    }

    public Creator(String itemID) {
	_itemID = itemID;
	_entityID = new Integer(-1);
	_affiliation = "";
    }

    public Creator() {
	_itemID = "";
	_entityID = new Integer(-1);
	_affiliation = "";
    }

    public void setItemID(String itemID) {
	this._itemID = itemID;
    }

    public String getItemID() {
	return _itemID;
    }

    public int getEntityID() {
	return _entityID.intValue();
    }

    public void setEntityID(int entityID) {
	_entityID = new Integer(entityID);
    }

    public String getAffiliation() {
	return _affiliation;
    }

    public void setAffiliation(String affiliation) {
	this._affiliation = affiliation;
    }

    public static String getAffiliationFieldName() {
	return "_affiliation";
    }

    public Entity getEntity() {
	return _entity;
    }

    public void setEntity(Entity entity) {
	this._entity = entity;
	this._entityID = new Integer(entity.getEntityID());
    }

    public static Creator getDummyObject(String itemID) {
	Creator creator = new Creator();
	creator.setItemID(itemID);
	return creator;
    }

    public String unique() {
	return _itemID + ":" + _entityID.toString();
    }

    public String uniqueNull() {
	return ":-1";
    }

    public Creator cloneObject() {
	return getDummyObject("");
    }

    public static HashSet<String> getAffiliations() {
	return affiliations;
    }

    public void truncateFields(){

    }	
}
