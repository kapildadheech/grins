package org.gramvaani.radio.medialib;

import org.gramvaani.utilities.LogUtilities;

public class CircularBuffer {
    long maxMillisToStore;
    
    int[] stats;
    long[] updateTimes;
    int beginIndex, currIndex;

    int currsum, currcount;
    int lastmin, lastminindex;
    
    LogUtilities logger;

    public CircularBuffer(int maxMinutesToStore, String logPrefix) {
	this.maxMillisToStore = maxMinutesToStore * 60 * 1000;
	stats = new int[maxMinutesToStore * 2];         // assuming that stats will not be pushed in at a rate more than 1 per minute
	updateTimes = new long[maxMinutesToStore * 2];
	beginIndex = -1;
	currIndex = -1;

	currsum = 0;
	currcount = 0;
	lastmin = 100; lastminindex = -1;

	logger = new LogUtilities(logPrefix+ ":CircularBuffer");
    }

    public void put(int stat, long currtime) {
	if(beginIndex == -1) {
	    beginIndex = currIndex = 0;
	}

	boolean reprocessmin = false;
	stats[currIndex] = stat;
	updateTimes[currIndex] = currtime;
	currsum += stat;
	currcount ++;

	while(updateTimes[currIndex] - updateTimes[beginIndex] > maxMillisToStore) {
	    if(lastminindex == beginIndex)
		reprocessmin = true;
	    currsum -= stats[beginIndex];
	    currcount --;
	    beginIndex++;
	    beginIndex %= stats.length;
	}
	
	if(!reprocessmin && stat < lastmin) {
	    lastmin = stat;
	    lastminindex = currIndex;
	}

	currIndex ++;
	currIndex %= stats.length;

	if(reprocessmin) {
	    lastmin = 100;
	    int i = beginIndex;
	    while(i != currIndex) {
		if(stats[i] < lastmin) {
		    lastmin = stats[i];
		    lastminindex = i;
		}
		i++;
		i %= stats.length;
	    }
	}

	logger.debug("Put: beginIndex = " + beginIndex + ":" + "currIndex = " + currIndex + ":" + "lastminindex = " + lastminindex);
	int i = beginIndex;
	while(i != currIndex) {
		logger.debug("Put: i:" + stats[i] + ":" + updateTimes[i]);
		i++;
		i %= stats.length;
	}
    }

    public int mean() {
	if(currcount > 0 && (updateTimes[(currIndex + stats.length - 1) % stats.length] - updateTimes[beginIndex] > maxMillisToStore * 0.75)) {
	    int meanVal = currsum / currcount;
	    logger.debug("Mean: meanVal = " + meanVal);
	    return meanVal;
	} else {
	    logger.debug("Mean: currTime: " + (updateTimes[(currIndex + stats.length - 1) % stats.length] / 1000 / 60) + ": beginTime: " + (updateTimes[beginIndex] / 1000 / 60));
	    return -1;
	}
    }

    public boolean lastExceed(int cutoff, int cutoffTimeInMin) {
	if(lastmin < cutoff && (updateTimes[lastminindex] - updateTimes[beginIndex] < cutoffTimeInMin * 60 * 1000)) {
	    logger.debug("LastExceed: returning true: " + lastmin);
	    return true;
	} else {
	    logger.debug("LastExceed: returning false: " + lastmin);
	    return false;
	}
    }

}
