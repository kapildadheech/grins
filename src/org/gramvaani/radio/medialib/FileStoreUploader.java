package org.gramvaani.radio.medialib;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.UpdateProgramMessage;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.FlushIndexMessage;
import org.gramvaani.radio.rscontroller.pipeline.RSPipeline;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.app.gui.DiagnosticsController;
import org.gramvaani.radio.rscontroller.servlets.MediaLibUploadServlet;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.*;
import java.io.*;
import java.net.*;
import java.nio.channels.FileChannel;


public class FileStoreUploader implements Runnable {
    
    public static final String ERROR_FILE_PATH    = "ERROR_FILE_PATH";
    public static final String ERROR_NO_SPACE     = "ERROR_NO_SPACE";
    public static final String ERROR_NOT_WRITABLE = "ERROR_NOT_WRITABLE";
    public static final String ERROR_UNKNOWN	  = "ERROR_UNKNOWN";
    public static final String ERROR_NOT_READY    = "ERROR_NOT_READY";
    public static final String ERROR_MAX_EXCEEDED = "ERROR_MAX_EXCEEDED";
    public static final String ERROR_DB_FAILED    = "ERROR_DB_FAILED";
    public static final String ERROR_NOT_SUPPORTED= "ERROR_NOT_SUPPORTED";
    public static final String ERROR_FILE_EXISTS  = "ERROR_FILE_EXISTS";
    public static final String ERROR_FILE_COPY_FAILED = "ERROR_FILE_COPY_FAILED";

    static final String ERROR_NO_SPACE_STRING     = "No space left on device";
    static final String PERMISSION_DENIED_STRING  = "Permission denied";

    public static final String LOCAL = "LOCAL";
    public static final String SERVLET = "SERVLET";
    public static final String FAILURE_MSG = "FAILURE";
    public static final String SUCCESS_MSG = "SUCCESS";

    public static final String UPLOAD_FILE_PARAM = "uploadfile";

    static final long MESSAGE_ACK_WAIT_TIME = 2000; //2 seconds
    static final int MAX_RETRIES = 3; //trying to send the same message three times. Then giving up.

    LogUtilities logger;
    FileStoreUploadListener uploadListener;
    StationConfiguration stationConfig;
    StationNetwork stationNetwork;
    MediaLib medialib;
    TimeKeeper timeKeeper;
    String scriptsDir;
    LinkedBlockingQueue<UploadDoneMessage> libMessageQueue;
    UploadDoneMessage currentUploadDoneMessage;
    Thread libCommunicationThread;
    String uploaderName;
    Boolean isConnectionUp = true;
    boolean ackReceived = false;

    public FileStoreUploader(FileStoreUploadListener uploadListener, StationConfiguration stationConfig, String logPrefix, TimeKeeper timeKeeper) {
	logger = new LogUtilities(logPrefix+":FileStoreUploader");
	uploaderName = logPrefix + ":FileStoreUploader";
	this.uploadListener = uploadListener;
	this.stationConfig = stationConfig;
	stationNetwork = stationConfig.getStationNetwork();
	this.timeKeeper = timeKeeper;
	//medialib = MediaLib.getMediaLib(stationConfig, timeKeeper);
	libMessageQueue = new LinkedBlockingQueue<UploadDoneMessage>();
	libCommunicationThread = new Thread(this);
	//libCommunicationThread.start();
	try{
	    String configPath = stationConfig.getStringParam(StationConfiguration.CONFIGPATH);
	    String configDir = new File(configPath).getParentFile().getAbsolutePath();
	    scriptsDir = configDir + File.separator + "scripts";
	} catch (Exception e){
	    logger.error("Init: Unable to find config directory.");
	}
    }
    
    public synchronized void activate() {
	logger.debug("Activate:");
	medialib = MediaLib.getMediaLib(stationConfig, timeKeeper);
	if (!libCommunicationThread.isAlive())
	    libCommunicationThread.start();
    }

    public synchronized String uploadFile(String fileName, String destFileStore, boolean doDBInsert, String libFileName) {
	File file = new File(fileName);
	try {
	    fileName = file.getCanonicalPath();
	} catch(Exception e) {
	    logger.error("UploadFile: Unable to upload file: " + fileName, e);
	    return ERROR_FILE_PATH;
	}
	
	return uploadFileToServlet(fileName, destFileStore, doDBInsert, libFileName);
	
    }

    public synchronized String copyFile(String srcFileName, String destFileName, boolean doDBInsert, String destFileStore){

	String storeDir = stationNetwork.getStoreDir(destFileStore);
	if (medialib == null) {
	    medialib = MediaLib.getMediaLib(stationConfig, timeKeeper);
	    if (medialib == null) {
		logger.error("CopyFile: Unable to connect to database.");
		return ERROR_DB_FAILED;
	    }
	}
	
	if (destFileName.equals("")) {
	    destFileName = medialib.newBulkItemFilename() + "." + getExtension(srcFileName);
	}

	try {
	    destFileName = new File(storeDir + File.separator + destFileName).getCanonicalPath();
	} catch(Exception e) {
	    logger.error("CopyFile: Exception occurred while trying to get canonical path of destination file.", e);
	    return ERROR_FILE_PATH;
	}

	//In case the OS is windows ignore case for comparison
	if (srcFileName.equals(destFileName)) {
	    logger.info("CopyFile: Source and destination are the same files: " + srcFileName);
	} else {
	    logger.info("CopyFile: " + srcFileName + " != " + destFileName);
	    FileChannel srcChannel, dstChannel;
	    try {
		srcChannel = new FileInputStream(srcFileName).getChannel();
	    } catch(FileNotFoundException fre) {
		logger.error("CopyFile: Could not open source file " + srcFileName + " for reading.", fre);
		return ERROR_FILE_PATH;
	    } catch(Exception e) {
		logger.error("CopyFile: Unknown error occured.", e);
		return ERROR_UNKNOWN;
	    }

	    try {
		dstChannel = new FileOutputStream(destFileName).getChannel();
	    } catch(FileNotFoundException fwe) {
		logger.error("CopyFile: Could not open destination file for writing.", fwe);
		if (fwe.getMessage().contains(PERMISSION_DENIED_STRING))
		    return ERROR_NOT_WRITABLE;
		else
		    return ERROR_UNKNOWN;
	    } catch(Exception e) {
		logger.error("CopyFile: Unknown error occured.", e);
		return ERROR_UNKNOWN;
	    }
	    
	    boolean isTestFile = false;
	    
	    try {
		String testFileName = new File(stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR) + "/" + DiagnosticsController.DIAGNOSTICS_PLAYOUT_FILE).getCanonicalPath();

		if (srcFileName.equals(testFileName))
		    isTestFile = true;
	    } catch(Exception e) {
		logger.error("CopyFile: Could not obtain canonical path for test file.");
	    }

	    
	    try{
		dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
		srcChannel.close();
		dstChannel.close();
		//FileUtilities.copyFileOrThrowException(srcFileName, destFileName);
	    } catch (IOException e) {
		logger.error("CopyFile: Error occured while copying file.", e);
		if (e.getMessage().startsWith(ERROR_NO_SPACE_STRING))
		    return ERROR_NO_SPACE;
		else 
		    return ERROR_UNKNOWN;
	    } catch (Exception e) {
		logger.error("CopyFile: Unknown error occured while copying file.", e);
		return ERROR_UNKNOWN;
	    } finally {
		try{
		    if (isTestFile)
			(new File(destFileName)).delete();
		} catch (Exception e){
		    logger.error("CopyFile: Could not remove test upload file.", e);
		}
	    }
	}//if src and destination are not the same files

	boolean retVal;
	File srcFile = new File(srcFileName);
	File destFile = new File(destFileName);
	if (doDBInsert) {
	    retVal = insertProgram(srcFile.getName(), destFile.getName(), destFileStore);
	} else {
	    retVal = updateProgram(destFile.getName(), destFileStore);
	}
	String oldProgramID;
	if ((oldProgramID = medialib.findImportedFile(srcFile.getName())) != null){
	    medialib.updateImportedFile(destFile.getName(), oldProgramID);
	}

	if (retVal)
	    return destFile.getName();
	else
	    return ERROR_DB_FAILED;
    }

    protected boolean updateProgram(String libFileName, String fileStoreName) {
	 RadioProgramMetadata whereMetadata = new RadioProgramMetadata(libFileName);
	 RadioProgramMetadata updateMetadata = whereMetadata.clone();
	 setFileStoreField(fileStoreName, updateMetadata, FileStoreManager.UPLOAD_DONE);
	 
	 if (medialib.update(whereMetadata, updateMetadata, true) >= 0) {
	     if (fileStoreName.equals(StationNetwork.UPLOAD))
		 sendUploadDoneMessage(libFileName);

	     return true;
	 } else {
	     return false;
	 }
    }


    protected boolean insertProgram(String originalName, String libFileName, String fileStoreName) {
	String storeDir = stationNetwork.getStoreDir(fileStoreName);
	String completeFileName = storeDir + File.separator + libFileName;

	String checksum = FileUtilities.getSha1Hex(completeFileName);
	if (checksum == null)
	    checksum = "";

	RadioProgramMetadata uploadedMetadata = new RadioProgramMetadata(libFileName, 0, "", 
									 LibService.UNKNOWN, 
									 getTimekeeperTime(), 
									 LibService.DONE_UPLOAD, 
									 LibService.MEDIA_NOT_INDEXED,
									 checksum,
									 LibService.UNKNOWN, 
									 LibService.UNKNOWN);

	

	uploadedMetadata.setTitle(StringUtilities.sanitize(removeExtension(originalName)));
	
	try{
	    Hashtable<String, String> fileMetadata = RSPipeline.getMetadata(completeFileName);
	    medialib.updateFileMetadata(uploadedMetadata, fileMetadata);
	} catch (Exception e){
	    logger.error("InsertProgram: Unable to get file metadata", e);
	}
	
	clearAllFileStoreFields(uploadedMetadata);
	setFileStoreField(fileStoreName, uploadedMetadata, FileStoreManager.UPLOAD_DONE);
	
	if (fileStoreName.equals(StationNetwork.UPLOAD))
	    uploadedMetadata.setType("upload");
	
	uploadedMetadata.truncateFields();
	if (medialib.insert(uploadedMetadata) >= 0) {
	    //Inform lib service only if we are putting the file in upload filestore
	    if (fileStoreName.equals(StationNetwork.UPLOAD) && 
	       !originalName.equals(DiagnosticsController.DIAGNOSTICS_PLAYOUT_FILE))
		sendUploadDoneMessage(libFileName);
	} else {
	    return false;
	}
	
	// Dummy file uploaded here. It can be removed from the system.
	if (originalName.equals(DiagnosticsController.DIAGNOSTICS_PLAYOUT_FILE)){
	    logger.info("InsertProgram: Diagnostics playout file uploaded. Removing from system.");
	    if (medialib.remove(uploadedMetadata) < 0)
		return false; //This is probably not quite right. Same thing done in servlet.
	    try{
		File file = new File(libFileName);
		file.delete();
	    } catch (Exception e) {
		logger.error("DoPost: Unable to remove Diagnostics playout file:", e);
	    }
	}

	return true;
    }


    protected synchronized String uploadFileToServlet(String fileName, String storeName, boolean doDBInsert, String libFileName) {
	byte[] inBytes = new byte[1024];
	URL url;
	URLConnection urlConn;
	String uploadURL = stationNetwork.getServletURL(storeName);
	uploadURL += "?" + MediaLibUploadServlet.PARAM_FILESTORE_NAME 	+ "=" + storeName;
	uploadURL += "&" + MediaLibUploadServlet.PARAM_DB_INSERT  	+ "=" + doDBInsert;
	uploadURL += "&" + MediaLibUploadServlet.PARAM_UPLOADER_NAME  	+ "=" + uploadListener.getName();
	if (!libFileName.equals(""))
	    uploadURL += "&" + MediaLibUploadServlet.PARAM_LIB_FILENAME + "=" + libFileName;

	logger.debug("UploadFileToServlet: URL: "+uploadURL);
	
	try {
	    url = new URL(uploadURL);
	    urlConn = url.openConnection();
	    if (urlConn instanceof HttpURLConnection) {
		HttpURLConnection httpConn = (HttpURLConnection)urlConn;
		httpConn.setChunkedStreamingMode(0);
		httpConn.setRequestMethod("POST");
	    }
	    String boundary = "--------------------" + Long.toString(System.currentTimeMillis(), 16);
	    urlConn.setDoOutput(true);
	    urlConn.setDoInput(true);
	    urlConn.setUseCaches(false);
	    urlConn.setDefaultUseCaches(false);
	    urlConn.setRequestProperty("Accept", "*/*");
	    urlConn.setRequestProperty("Connection", "Keep-Alive");
	    urlConn.setRequestProperty("Cache-Control", "no-cache");
	    urlConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
	    DataOutputStream out = new DataOutputStream(urlConn.getOutputStream());
	    out.writeBytes("--");
	    out.writeBytes(boundary);
	    out.writeBytes("\r\n");
	    out.writeBytes("Content-Disposition: form-data; name=\"" + UPLOAD_FILE_PARAM  + "\"; filename=\"" + fileName + "\"; fileStore=\"Upload\"");
	    out.writeBytes("\r\n");
	    out.writeBytes("Content-Type: application/octet-stream; charset=ISO-8859-1");
	    out.writeBytes("\r\n");
	    out.writeBytes("\r\n");
	    
	    long fileLength = (new File(fileName)).length();
	    DataInputStream in = new DataInputStream(new FileInputStream(fileName));
	    int numBytesRead = 0;
	    long sumRead = 0;
	    while ((numBytesRead = in.read(inBytes, 0, inBytes.length)) != -1) {
		out.write(inBytes, 0, numBytesRead);
		sumRead += numBytesRead;
		updateProgress(fileName, sumRead, fileLength);
	    }
	    in.close();
	    out.writeBytes("\r\n");
	    out.writeBytes("--");
	    out.writeBytes(boundary);
	    out.writeBytes("--");
	    out.flush();
	    out.close();
	} catch(Exception ex) {
		logger.error("Unable to upload file: " + fileName, ex);
		if (ex.toString().startsWith("java.net.ConnectException: Connection refused"))
		    return RSController.ERROR_SERVLET_CONNECTION_FAILED;
		else
		    return ERROR_UNKNOWN;
	}

	String error = ERROR_UNKNOWN;
	try {
	    BufferedReader response = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
	    String line = "";
	    
	    //XXX throws Stream closed IOException sometimes.
	    while ((line = response.readLine()) != null) {
		logger.debug("Multipart read: " + line);
		if (line.equals(ERROR_NO_SPACE))
		    error = ERROR_NO_SPACE;
		else if (line.equals(ERROR_NOT_WRITABLE))
		    error = ERROR_NOT_WRITABLE;
		else if (line.equals(ERROR_DB_FAILED))
		    error = ERROR_DB_FAILED;
		else if (line.equals(ERROR_MAX_EXCEEDED))
		    error = ERROR_MAX_EXCEEDED;
		
		if (line.startsWith("libfilename="))
		    libFileName = line.split("libfilename=")[1];
		if (line.indexOf(FAILURE_MSG) != -1) {
		    logger.error("Got upload error from server on file: " + fileName+" url: "+uploadURL);
		    response.close();
		    return error;
		}
	    }
	    response.close();
	} catch(Exception ex) {
	    logger.error("Unable to read upload response from server on file: " + fileName, ex);
	    return error;
	}
	
	logger.info("Upload successful: URL: " +uploadURL + " filename " + fileName);
	return libFileName;
    }


    public void run() {
	int retries;
	UploadDoneMessage message;

	while (true) {
	    message = null;
	    while (message == null) {
		try {
		    message = libMessageQueue.take();
		} catch(InterruptedException e) {
		    logger.warn("Run: Interrupted while taking message from the queue.");
		}
	    }
	    currentUploadDoneMessage = message;
	    retries = 0;
	   
	    while (retries < MAX_RETRIES) {
		if (uploadListener.sendUploadDoneMessage(message) == -1) {
		    logger.error("Run: Unable to send upload done message.");
		    libMessageQueue.add(message);
		    connectionError();
		} else {
		    try {
			Thread.sleep(MESSAGE_ACK_WAIT_TIME);
		    } catch(InterruptedException e) {
			if (ackReceived) {
			    logger.debug("Run: Ack message received for file: " + message.getFileName());
			    ackReceived = false;
			    break;
			} else {
			    //XXX: Should we be checking for unexpected interruption and sleeping again?
			    logger.warn("Run: Thread sleep interrupted.");
			}
		    }
		    logger.warn("Run: Ack message not received for file: " + message.getFileName());
		    retries++;
		}
		
	    } //end while

	    if (retries == MAX_RETRIES) {
		logger.error("Run: Failed to get ack message for file: " + message.getFileName() + ". Giving up.");
		retries = 0;
	    }
	}
    }

    protected void connectionError() {
	long interval = stationConfig.getIntParam(StationConfiguration.NETWORK_DOWN_RECONNECTION_INTERVAL) * 1000;
	while (!isConnectionUp) {
	    try {
		Thread.sleep(interval);
	    } catch(InterruptedException e){}
	}
    }

    public void sendUploadDoneMessage(String fileName) {
	UploadDoneMessage uploadDoneMessage = new UploadDoneMessage(getName(),
								    RSController.LIB_SERVICE,
								    fileName,
								    uploadListener.getName());
	libMessageQueue.add(uploadDoneMessage);
    }

    public String getName() {
	return uploaderName;
    }

    protected void updateProgress(String fileName, long uploadedLength, long totalLength){
	if (uploadListener != null)
	    uploadListener.updateProgress(fileName, uploadedLength, totalLength);
    }

    protected String getExtension(String filename) {
	return filename.substring(filename.lastIndexOf(".") + 1);
    }

    public long getTimekeeperTime() {
	if (timeKeeper != null) {
	    return timeKeeper.getRealTime();
	} else {
	    return System.currentTimeMillis();
	}
    }

    public String removeExtension(String filename){
	if (filename.matches(".*[.].{3}$"))
	    return filename.substring(0, filename.length()-4);
	else
	    return filename;
    }

    public void connectionUp(boolean set) {
	synchronized(isConnectionUp) {
	    isConnectionUp = set;
	}
    }

    public void receiveMessage(IPCMessage message) {
	if (message instanceof UploadDoneAckMessage) {
	    UploadDoneAckMessage ackMessage = (UploadDoneAckMessage)message;
	    if (ackMessage.getFileName().equals(currentUploadDoneMessage.getFileName())) {
		ackReceived = true;
		libCommunicationThread.interrupt();
	    } else {
		logger.warn("ReceiveMessage: Spurious Ack received.");
	    }
	}
    }

    public static void clearAllFileStoreFields(RadioProgramMetadata metadata) {
	for (String store: StationNetwork.getStores())
	    setFileStoreField(store, metadata, FileStoreManager.ZERO_ATTEMPTS);
    }

    public static void setFileStoreField(String fileStoreName, RadioProgramMetadata metadata, int count) {
	//String[] stores =((StationNetwork)stationConfig.getStationNetwork()).getSameStores(fileStoreName);
	String[] stores = StationNetwork.getSameStores(fileStoreName);
	for (String storeName : stores) {
	    if (storeName.equals(StationNetwork.AUDIO_PLAYOUT))
		metadata.setPlayoutStoreAttempts(count);
	    else if (storeName.equals(StationNetwork.AUDIO_PREVIEW))
		metadata.setPreviewStoreAttempts(count);
	    else if (storeName.equals(StationNetwork.UPLOAD))
		metadata.setUploadStoreAttempts(count);
	    else if (storeName.equals(StationNetwork.ARCHIVER))
		metadata.setArchiveStoreAttempts(count);
	    else if (storeName.equals(StationNetwork.ONLINETELEPHONY))
		metadata.setTelephonyStoreAttempts(count);
	}
    }

    public static String getFileStoreDBFieldName(String storeName) {
	if (storeName.equals(StationNetwork.AUDIO_PLAYOUT))
	    return RadioProgramMetadata.getPlayoutStoreAttemptsFieldName();
	else if (storeName.equals(StationNetwork.AUDIO_PREVIEW))
	    return RadioProgramMetadata.getPreviewStoreAttemptsFieldName();
	else if (storeName.equals(StationNetwork.UPLOAD))
	    return RadioProgramMetadata.getUploadStoreAttemptsFieldName();
	else if (storeName.equals(StationNetwork.ARCHIVER))
	    return RadioProgramMetadata.getArchiveStoreAttemptsFieldName();
	else if (storeName.equals(StationNetwork.ONLINETELEPHONY))
	    return RadioProgramMetadata.getTelephonyStoreAttemptsFieldName();
	else
	    return "";
    }
}


