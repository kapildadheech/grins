package org.gramvaani.radio.medialib;

import java.util.*;

public class RadioProgramMetadata extends Metadata {

    protected String _itemID;
    protected Integer _level;
    protected String _parentID;
    protected String _type;
    protected Long _creationTime;
    protected Long _length;
    protected Integer _state;
    protected Integer _indexed;
    protected String _checksum;
    protected String _language;
    protected String _description;
    protected int descriptionLength = 1024;
    protected String _title;
    protected int titleLength = 128;
    protected String _licenseType;
    protected Integer _playoutStoreAttempts;
    protected Integer _previewStoreAttempts;
    protected Integer _uploadStoreAttempts;
    protected Integer _archiveStoreAttempts;
    protected Integer _telephonyStoreAttempts;

    protected RadioProgramMetadata _parent;           
    protected static String foreign_parent = "_itemID", local_parent = "_parentID";

    public static final String IMPORTED_TELEPHONY_TYPE 	= "telephonyimport";
    public static final String TELEPHONY_TYPE 		= "telephony";
    public static final String UPLOAD_TYPE 		= "upload";
    public static final String MIC_TYPE 		= "mic";

    static final String HF_IMPORTED_TELEPHONY_TYPE 	= "Edited Telephone Conversation";
    static final String HF_TELEPHONY_TYPE 		= "Telephone Conversation";
    static final String HF_UPLOAD_TYPE 		        = "Prerecorded Program";
    static final String HF_MIC_TYPE                     = "Live Speech";
    

    public String dump() {
	return "RadioProgramMetadata: " + _itemID + ":" + _level + ":" + _parentID + ":" + _type + ":" + _creationTime + ":" + _length + ":" + _state + ":" + _language + ":" + _title + ":" + _checksum;
    }

    // XXX    protected Transcript __transcript;
    protected PlayoutHistory[] __history;
    protected Creator[] __creators;
    // XXX    protected Applicability __applicability;
    // XXX    protected RelatedContent[] __relatedContent;
    protected ItemCategory[] __categories;
    protected Tags[] __tags;
    protected RadioProgramMetadata[] __children;

    public RadioProgramMetadata(String filename, int level, String parent, String type, 
				long creationTime, int state, int indexed, String checksum, 
				String language, String stationName) {
	_itemID = filename;
	_level = new Integer(level);
	_parentID = parent;
	_type = type;
	_creationTime = new Long(creationTime);
	_length = new Long(-1);
	_state = new Integer(state);
	_indexed = new Integer(indexed);
	_checksum = checksum;
	_language = language;
	_description = "";
	_title = "";
	_licenseType = "";
	_parent = null;
	_playoutStoreAttempts = new Integer(-1);
	_previewStoreAttempts = new Integer(-1);
	_uploadStoreAttempts = new Integer(-1);
	_archiveStoreAttempts = new Integer(-1);
	_telephonyStoreAttempts = new Integer(-1);

	__history = null;
	__creators = null;
	//	__relatedContent = null;
	__categories = null;
	__tags = null;
	__children = null;
    }

    public RadioProgramMetadata(String filename) {
	_itemID = filename;
	_level = new Integer(-1);
	_parentID = "";
	_type = "";
	_creationTime = new Long(-1);
	_length = new Long(-1);
	_state = new Integer(-1);
	_indexed = new Integer(-1);
	_checksum = "";
	_language = "";
	_description = "";
	_title = "";
	_licenseType = "";
	_parent = null;
	_playoutStoreAttempts = new Integer(-1);
	_previewStoreAttempts = new Integer(-1);
	_uploadStoreAttempts = new Integer(-1);
	_archiveStoreAttempts = new Integer(-1);
	_telephonyStoreAttempts = new Integer(-1);

	__history = null;
	__creators = null;
	// 	__relatedContent = null;
	__categories = null;
	__tags = null;
	__children = null;
    }

    public void setState(int state) {
	_state = new Integer(state);
    }

    public void setFilename(String filename) {
	_itemID = filename;
    }

    public void setParentID(String parentID) {
	_parentID = parentID;
    }

    public String getFilename() {
	return _itemID;
    }

    public String getItemID() {
	return _itemID;
    }

    public void setItemID(String itemID) {
	_itemID = itemID;
    }

    public void setPlayoutStoreAttempts(int count) {
	_playoutStoreAttempts = new Integer(count);
    }

    public void setPreviewStoreAttempts(int count) {
	_previewStoreAttempts = new Integer(count);
    }

    public void setUploadStoreAttempts(int count) {
	_uploadStoreAttempts = new Integer(count);
    }

    public void setArchiveStoreAttempts(int count) {
	_archiveStoreAttempts = new Integer(count);
    }

    public void setTelephonyStoreAttempts(int count) {
	_telephonyStoreAttempts = new Integer(count);
    }

    public int getPlayoutStoreAttempts() {
	return _playoutStoreAttempts;
    }

    public int getPreviewStoreAttempts() {
	return _previewStoreAttempts;
    }

    public int getUploadStoreAttempts() {
	return _uploadStoreAttempts;
    }

    public int getArchiveStoreAttempts() {
	return _archiveStoreAttempts;
    }

    public int getTelephonyStoreAttempts() {
	return _telephonyStoreAttempts;
    }

    public static String getUploadStoreAttemptsFieldName() {
	return "_uploadStoreAttempts";
    }

    public static String getPlayoutStoreAttemptsFieldName() {
	return "_playoutStoreAttempts";
    }

    public static String getPreviewStoreAttemptsFieldName() {
	return "_previewStoreAttempts";
    }

    public static String getArchiveStoreAttemptsFieldName() {
	return "_archiveStoreAttempts";
    }

    public static String getTelephonyStoreAttemptsFieldName() {
	return "_telephonyStoreAttempts";
    }

    public static String getItemIDFieldName() {
	return "_itemID";
    }

    public int getLevel() {
	return _level.intValue();
    }

    public String getParentID() {
	return _parentID;
    }

    public String getType() {
	return _type;
    }

    public void setType(String type) {
	_type = type;
    }

    public String getHumanFriendlyTypeString() {
	if(_type.equals(MIC_TYPE)) {
	    return HF_MIC_TYPE;
	} else if(_type.equals(TELEPHONY_TYPE)) {
	    return HF_TELEPHONY_TYPE;
	} else if(_type.equals(IMPORTED_TELEPHONY_TYPE)) {
	    return HF_IMPORTED_TELEPHONY_TYPE;
	} else if(_type.equals(UPLOAD_TYPE)) {
	    return HF_UPLOAD_TYPE;
	} else {
	    return "Unknown";
	}
    }

    public static String getTypeFieldName() {
	return "_type";
    }

    public long getCreationTime() {
	return _creationTime.longValue();
    }

    public static String getCreationTimeFieldName() {
	return "_creationTime";
    }

    public long getLength() {
	return _length.longValue();
    }

    public static String getLengthFieldName() {
	return "_length";
    }

    public void setLength(long length) {
	_length = length;
    }

    public int getState() {
	return _state.intValue();
    }

    public int getIndexed() {
	return _indexed.intValue();
    }

    public void setIndexed(int indexed){
	_indexed = indexed;
    }

    public String getChecksum(){
	return _checksum;
    }

    public void setChecksum(String checksum){
	_checksum = checksum;
    }

    public static String getStateFieldName() {
	return "_state";
    }

    public String getLanguage() {
	return _language;
    }

    public void setLanguage(String language) {
	_language = language;
    }

    public static String getLanguageFieldName() {
	return "_language";
    }

    public String getDescription() {
	return _description;
    }

    public void setDescription(String desc) {
	_description = desc;
    }

    public static String getDescriptionFieldName() {
	return "_description";
    }

    public String getTitle() {
	return _title;
    }

    public void setTitle(String title) {
	_title = title;
    }

    public static String getTitleFieldName() {
	return "_title";
    }

    public String getLicenseType() {
	return _licenseType;
    }

    public static String getLicenseFieldName() {
	return "_licenseType";
    }

    public void setLicenseType(String license) {
	_licenseType = license;
    }

    public RadioProgramMetadata getParent() {
	return _parent;
    }
    
    public String primaryKey() {
	return "_itemID";
    }

    public String unique() {
	return _itemID;
    }

    public String uniqueNull() {
	return "";
    }
    
    public static RadioProgramMetadata getDummyObject(String filename) {
	return new RadioProgramMetadata(filename);
    }

    public PlayoutHistory[] populate__history(MediaLib medialib) {
	PlayoutHistory playout = PlayoutHistory.getDummyObject(_itemID);
	Metadata[] history = medialib.get(playout);
	__history = new PlayoutHistory[history.length];
	int i = 0;
	for (Metadata metadata: history)
	    __history[i++] = (PlayoutHistory)metadata;
	return __history;
    }

    public PlayoutHistory[] getHistory() {
	return __history;
    }

    public void setHistory(PlayoutHistory[] history){
	__history = history;
    }

    public Creator[] populate__creators(MediaLib medialib) {
	Creator creator = Creator.getDummyObject(_itemID);
	Metadata[] creators = medialib.getAll(creator);
	__creators = new Creator[creators.length];
	int i = 0;
	for (Metadata metadata: creators)
	    __creators[i++] = (Creator)metadata;
	return __creators;
    }

    public Creator[] getCreators() {
	return __creators;
    }

    public void setCreators(Creator[] creators) {
	__creators = creators;
    }

    public String[] getCreatorNames(MediaLib medialib) {
	ArrayList<String> names = new ArrayList<String>();
	
	for(Creator creator: getCreators()) {
	    Entity entity = new Entity(creator.getEntityID());
	    Entity[] entityArr = medialib.getAll(entity);
	    String name = entityArr[0].getName();
	    names.add(name);
	}

	return names.toArray(new String[names.size()]);
    }

    //    public RelatedContent[] populate__relatedContent(MediaLib medialib) {
    //	RelatedContent related = RelatedContent.getDummyObject(_itemID);
    //	Metadata[] relatedContent = medialib.get(related);
    //	__relatedContent = new RelatedContent[relatedContent.length];
    //	int i = 0;
    //	for (Metadata metadata: relatedContent)
    //	    __relatedContent[i++] = (RelatedContent)metadata;
    //	return __relatedContent;
    //    }

    //    public RelatedContent[] getRelatedContent() {
    //	return __relatedContent;
    //    }

    public ItemCategory[] populate__categories(MediaLib medialib) {
	ItemCategory category = ItemCategory.getDummyObject(_itemID);
	Metadata[] categories = medialib.getAll(category);
	__categories = new ItemCategory[categories.length];
	int i = 0;
	for (Metadata metadata: categories)
	    __categories[i++] = (ItemCategory)metadata;
	return __categories;
    }

    public ItemCategory[] getCategories() {
	return __categories;
    }
    
    public String[] getCategoryNames(MediaLib medialib) {
	ArrayList<String> names = new ArrayList<String>();

	for(ItemCategory itemCat: getCategories()) {
	    Category cat = new Category(itemCat.getCategoryNodeID());
	    Category[] catArr = medialib.getAll(cat);
	    String name = catArr[0].getNodeName();
	    names.add(name);
	}
	
	return names.toArray(new String[names.size()]);
    }

    public void setCategories(ItemCategory[] itemCategories) {
	__categories = itemCategories;
    }

    public Tags[] populate__tags(MediaLib medialib) {
	Tags tag = Tags.getDummyObject(_itemID);
	Metadata[] tags = medialib.get(tag);
	__tags = new Tags[tags.length];
	int i =0;
	for (Metadata metadata: tags)
	    __tags[i++] = (Tags)metadata;
	return __tags;
    }

    public Tags[] getTags() {
	return __tags;
    }

    public void setTags(Tags[] tags) {
	__tags = tags;
    }
    
    public RadioProgramMetadata[] populate__children(MediaLib medialib) {
	RadioProgramMetadata program = RadioProgramMetadata.getDummyObject("");
	program.setParentID(_itemID);
	Metadata[] children = medialib.get(program);
	__children = new RadioProgramMetadata[children.length];
	int i = 0;
	for (Metadata metadata: children)
	    __children[i++] = (RadioProgramMetadata)metadata;
	return __children;
    }

    public RadioProgramMetadata[] getChildren() {
	return __children;
    }

    public RadioProgramMetadata cloneObject() {
	return getDummyObject("");
    }
    
    public boolean isFileStoreField(String field) {
	return (field.equals(getPlayoutStoreAttemptsFieldName()) ||
		field.equals(getPreviewStoreAttemptsFieldName()) ||
		field.equals(getUploadStoreAttemptsFieldName()) ||
		field.equals(getArchiveStoreAttemptsFieldName()) ||
		field.equals(getTelephonyStoreAttemptsFieldName()));
    }

    public void truncateFields(){
	if (_description.length() >= descriptionLength){
	    _description = _description.substring(0, descriptionLength);
	}

	if (_title.length() >= titleLength){
	    _title = _title.substring(0, titleLength);
	}
    }

}

