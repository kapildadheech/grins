package org.gramvaani.radio.medialib;

import org.gramvaani.radio.rscontroller.services.LibService;
import org.gramvaani.radio.rscontroller.servlets.MediaLibUploadServlet;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.timekeeper.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class FileStoreDownloader {

    StationConfiguration stationConfig;
    StationNetwork stationNetwork;
    LogUtilities logger;
    MediaLib medialib;
    TimeKeeper timeKeeper;

    public FileStoreDownloader(StationConfiguration stationConfig) {
	this.stationConfig = stationConfig;
	stationNetwork = stationConfig.getStationNetwork();
	this.timeKeeper = timeKeeper;
	logger = new LogUtilities("FileStoreDownloader");
    }

    public boolean downloadFile(String fileName, String fileStore, String destDir){
	String downloadURL = stationNetwork.getServletURL(fileStore);
	downloadURL += "?"+MediaLibUploadServlet.PARAM_DOWNLOAD_FILENAME+"="+fileName;
	downloadURL += "&"+MediaLibUploadServlet.PARAM_FILESTORE_NAME+"="+fileStore;
	logger.info("DownloadFile: Downloading file from url: "+downloadURL);
	try{
	    HttpURLConnection connection = (HttpURLConnection) (new URL(downloadURL)).openConnection();
	    connection.setChunkedStreamingMode(0);
	    connection.setRequestMethod("POST");
	    connection.setDoOutput(true);
	    InputStream in = connection.getInputStream();
	    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(destDir + File.separator + fileName));
	    byte buf[] = new byte[4 * 1024];
	    int count;
	    while ((count = in.read(buf)) != -1){
		out.write(buf, 0, count);
	    }
	    out.close();

	    File outFile = new File(destDir + File.separator + fileName);
	    outFile.setReadable(true, false);
	    outFile.setWritable(true, false);
	} catch (Exception e){
	    logger.error("DownloadFile: Could not download file: "+fileName+" from: "+fileStore, e);
	    return false;
	}
	return true;
    }

    public boolean getFile(String fileName, String destDirName) {
	String srcDirName = stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR);

	File srcFile = new File(srcDirName + File.separator + fileName);

	if (srcFile.exists()) {
	    if (FileUtilities.copyFile(srcDirName + File.separator + fileName, 
				       destDirName + File.separator + fileName) < 0) {

		logger.error("GetFile: Could not copy file: "+ srcDirName + File.separator + fileName 
			     + " to " + destDirName);
		return false;

	    } else {

		logger.debug("GetFile: Copied file locally. srcFile=" + srcDirName + File.separator + 
			     fileName + " destFile=" + destDirName + File.separator + fileName);
		return true;
	    }
	} else {
	    String fileStore = getBestStoreForDownload(fileName);
	    return downloadFile(fileName, fileStore, destDirName);
	}
    }

    protected String getBestStoreForDownload(String fileName) {
	medialib = MediaLib.getMediaLib(stationConfig, null);
	if (medialib == null) {
	    logger.error("GetBestStoreForDownload: Cannot query database for program metadata. medialib is null.");
	    return null;
	}

	RadioProgramMetadata query = RadioProgramMetadata.getDummyObject(fileName);
	RadioProgramMetadata[] results = medialib.get(query);
	    
	if (results.length == 0) {
	    logger.error("GetBestStoreForDownload: Could not find program metadata for " + fileName + " in the database.");
	    return null;
	}

	RadioProgramMetadata program = results[0];

	if (program.getUploadStoreAttempts() == FileStoreManager.UPLOAD_DONE) {
	    return StationNetwork.UPLOAD;
	} else if (program.getArchiveStoreAttempts() == FileStoreManager.UPLOAD_DONE) {
	    return StationNetwork.ARCHIVER;
	} else {
	    logger.error("GetBestStoreForDownload: Could not find the file any producer store.");
	    return null;
	}
    }

    public void exportFiles(RadioProgramMetadata[] programs, String destFolder){
	String srcFolder = stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR) + File.separator;
	
	for (RadioProgramMetadata program: programs){
	    if (program == null){
		logger.warn("exportFiles: Null program");
		continue;
	    }
	    String destFileName = null;
	    String baseName = null, extension;
	    try{
		if (program.getType().equals(RadioProgramMetadata.TELEPHONY_TYPE)){
		    baseName = program.getItemID();
		    extension = "";
		} else {
		    baseName = program.getTitle();
		    extension = program.getItemID().substring(program.getItemID().lastIndexOf("."));
		}
		
		destFileName = baseName + extension;
		int i = 0;
			    
		try{
		    while ((new File(destFolder + destFileName)).exists()){
			destFileName = baseName + "-"+(i++)+extension;
		    }
		} catch (Exception e){
		    logger.error("ExportFiles: Error in checking existing files.", e);
		}
			    
		String srcFilename = srcFolder + program.getItemID();
		String destFilename = destFolder + destFileName;
			    
		if (getFile(program.getItemID(), destFolder)){
		    logger.debug("ExportFiles: Copied file "+srcFilename+" to "+destFilename);
		    //If baseName is itemID, it is the same file.
		    if (!baseName.equals(program.getItemID()))
			FileUtilities.changeFilename(destFolder + program.getItemID(), destFilename);
		} else {
		    logger.error("ExportFiles: Could not copy file "+srcFilename+" to " +destFilename);
		}
			    
	    } catch (Exception e){
		logger.error("ExportFiles: Unable to export file: "+srcFolder+baseName+" to "+destFolder+destFileName, e);
	    }
	}
    }
}