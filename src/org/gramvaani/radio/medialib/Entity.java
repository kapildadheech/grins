package org.gramvaani.radio.medialib;

import java.util.*;

public class Entity extends Metadata {
    public static String VOLUNTEER = "volunteer";
    public static String STAFF = "staff member";
    public static String PERSON = "independent artist";
    public static String RADIO_STATION = "radio station";
    public static String NGO = "ngo";
    public static String COMPANY = "company";

    protected Integer _entityID;
    protected String _name;
    protected String _url;
    protected String _phoneType;
    protected String _phone;
    protected String _email;
    protected String _location;
    protected String _entityType;

    public String dump() {
	return "Entity: " + _entityID + ":" + _name + ":" + _url + ":" + _phone + ":" + _email + ":" + _location + ":" + _entityType;
    }

    public Entity(String name, String url, String phoneType, String phone, String email, String location, String entityType) {
	this._entityID = new Integer(-1);
	this._name = name;
	this._url = url;
	this._phoneType = phoneType;
	this._phone = phone;
	this._email = email;
	this._location = location;
	this._entityType = entityType;
    }

    public Entity() {
	_entityID = new Integer(-1);
	_name = "";
	_url = "";
	_phoneType = "";
	_phone = "";
	_email = "";
	_location = "";
	_entityType = "";
    }

    public Entity(int entityID) {
	this();
	_entityID = entityID;
    }

    public void setEntityID(int entityID) {
	this._entityID = new Integer(entityID);
    }

    public void setEntityID(Integer entityID) {
	this._entityID = entityID;
    }

    public int getEntityID() {
	return _entityID.intValue();
    }

    public String getName() {
	return _name;
    }

    public void setName(String name) {
	this._name = name;
    }

    public static String getNameFieldName() {
	return "_name";
    }

    public String getUrl() {
	return _url;
    }

    public String getPhoneType() {
	return _phoneType;
    }

    public String getPhone() {
	return _phone;
    }

    public void setPhoneType(String phoneType) {
	_phoneType = phoneType;
    }

    public void setPhone(String phone) {
	_phone = phone;
    }

    public String getEmail() {
	return _email;
    }

    public String getLocation() {
	return _location;
    }

    public void setLocation(String location) {
	this._location = location;
    }

    public static String getLocationFieldName() {
	return "_location";
    }

    public static String getPhoneFieldName() {
	return "_phone";
    }

    public static String getPhoneTypeFieldName() {
	return "_phoneType";
    }

    public static String getEmailFieldName() {
	return "_email";
    }

    public String getEntityType() {
	return _entityType;
    }

    public void setEntityType(String entityType) {
	this._entityType = entityType;
    }

    public static String getEntityTypeFieldName() {
	return "_entityType"; 
    }

    public static Entity getDummyObject(Integer entityID) {
	Entity entity = new Entity();
	entity.setEntityID(entityID);
	return entity;
    }

    public String primaryKey() {
	return "_entityID";
    }

    public String unique() {
	return _entityID.toString();
    }

    public String uniqueNull() {
	return "-1";
    }

    public Entity cloneObject() {
	return getDummyObject(new Integer(-1));
    }

    public boolean equals(Entity entity){
	if (!_entityID.equals(entity.getEntityID()))
	    return false;

	if (!_name.equals(entity.getName()))
	    return false;
	
	if (! _url.equals(entity.getUrl()))
	    return false;

	if (! _phoneType.equals(entity.getPhoneType()))
	    return false;

	if (! _phone.equals(entity.getPhone()))
	    return false;

	if (! _email.equals(entity.getEmail()))
	    return false;

	if (! _location.equals(entity.getLocation()))
	    return false;

	if (! _entityType.equals(entity.getEntityType()))
	    return false;

	return true;
    }

    public void truncateFields(){

    }

}
