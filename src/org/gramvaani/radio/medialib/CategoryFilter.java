package org.gramvaani.radio.medialib;

public class CategoryFilter extends SearchFilter {
    Category parentNode;
    Category selectedNode;
    Category[] allChildren;

    public static final String CATEGORY_ANCHOR = "CATEGORY_ANCHOR"; boolean categoryActive = false;
 
    public CategoryFilter() {
	selectedNode = null;
	this.parentNode = null;
	allChildren = null;
    }

    public String getFilterName() {
	return SearchFilter.CATEGORY_FILTER + "_" + parentNode.getNodeName();
    }

    public String getFilterLabel() {
	return parentNode.getLabel();
    }

    public String getFilterQueryClause() {
	StringBuilder str = new StringBuilder();
	if(selectedNode != null && categoryActive) {
	    str.append(CATEGORY_ANCHOR); str.append(":\""); str.append(selectedNode.getNodeName()); str.append("\" OR ");
	    for(Category childNode: allChildren) {
		str.append(CATEGORY_ANCHOR); str.append(":\""); str.append(childNode.getNodeName()); str.append("\" OR ");
	    }
	}
	
	if(str.length() > 0)
	    return str.substring(0, str.length() - 3);
	else
	    return "";
    }

    public String getFilterWhereClause() {
	return "";
    }

    public String[] getFilterJoinTables() {
	return new String[]{};
    }

    public String getSelectedText(String anchorID) {
	return selectedNode.getLabel();
    }

    public void setSelectedNode(Category node) {
	this.selectedNode = node;
    }

    public Category getSelectedNode() {
	return selectedNode;
    }

    public Category getParentNode() {
	return parentNode;
    }

    public Category[] getAllChildren() {
	return allChildren;
    }

    public void setAllChildren(Category[] allChildren) {
	this.allChildren = allChildren;
    }

    public void setParentNode(Category node) {
	this.parentNode = node;
    }

    public void activateAnchor(String anchor, String anchorText) {
	if(anchor.startsWith(CATEGORY_ANCHOR) && anchor.endsWith(selectedNode.getNodeName())) {
	    categoryActive = true;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }


}





/*
    public static final String categoryKey = Metadata.getClassName(ItemCategory.class) + "." + ItemCategory.getCategoryNodeIDFieldName();

    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(ItemCategory.class)};
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if(selectedNode != null) {
	    str.append(categoryKey); str.append(" = "); str.append(selectedNode.getNodeID()); str.append(" or ");
	    for(Category childNode: allChildren) {
		str.append(categoryKey); str.append(" = "); str.append(childNode.getNodeID()); str.append(" or ");
	    }
	}
	
	if(str.length() > 0)
	    return str.substring(0, str.length() - 3);
	else
	    return "";
    }

*/

