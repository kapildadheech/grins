package org.gramvaani.radio.medialib;

import java.util.*;

public class Vote extends Metadata {

    Integer _voteID;
    String _smsLine;
    String _phoneNo;
    Long _dateTime;
    String _text;
    Integer _pollID;
    Integer _pollOptionID;
    String _pollOptionText;

    public String dump() {
	return "Vote: " + _voteID + ":" + _pollID + ":" + _pollOptionID + ":" + _pollOptionText;
    }

    public Vote(String smsLine, String phoneNo, Long dateTime, String text, Integer pollID, Integer pollOptionID, String pollOptionText) {
	_voteID = -1;
	_smsLine  = smsLine;
	_phoneNo = phoneNo;
	_dateTime = dateTime;
	_text = text;
	_pollID = pollID;
	_pollOptionID = pollOptionID;
	_pollOptionText = pollOptionText;
    }

    public Vote(int voteID) {
	this();
	_voteID = voteID;
    }

    public Vote(int pollID, int pollOptionID) {
	this();
	_pollID = pollID;
	_pollOptionID = pollOptionID;
    }

    public Vote() {
	_voteID = -1;
	_smsLine = "";
	_phoneNo = "";
	_dateTime = -1L;
	_text = "";
	_pollID = -1;
	_pollOptionID = -1;
	_pollOptionText = "";
    }

    public int getID() {
	return _voteID;
    }

    public String getSMSLine() {
	return _smsLine;
    }

    public void setSMSLine(String smsLine) {
	_smsLine = smsLine;
    }

    public static String getSMSLineFieldName() {
	return "_smsLine";
    }

    public void setPhoneNo(String phoneNo) {
	this._phoneNo = phoneNo;
    }

    public String getPhoneNo() {
	return _phoneNo;
    }

    public static String getPhoneNoFieldName() {
	return "_phoneNo";
    }

     public Long getDateTime() {
	return _dateTime;
    }

    public void setDateTime(Long dateTime) {
	_dateTime = dateTime;
    }

    public static String getDateTimeFieldName() {
	return "_dateTime";
    }

    public String getText() {
	return _text;
    }

    public void setText(String text) {
	_text = text;
    }

    public String getTextFieldName() {
	return "_text";
    }

    public int getPollID() {
	return _pollID;
    }

    public void setPollID(Integer pollID) {
	_pollID = pollID;
    }
    
    public String getPollIDFieldName() {
	return "_pollID";
    }

    public int getPollOptionID() {
	return _pollOptionID;
    }

    public void setPollOptionID(Integer pollOptionID) {
	_pollOptionID = pollOptionID;
    }

    public static String getPollOptionIDFieldName() {
	return "_pollOptionID";
    }

    public String getPollOptionText() {
	return _pollOptionText;
    }

    public void setPollOptionText(String text) {
	_pollOptionText = text;
    }

    public static String getPollOptionTextFieldName() {
	return "_pollOptionText";
    }

    public static Vote getDummyObject(Integer voteID) {
	Vote vote = new Vote(voteID);
	return vote;
    }

    public String primaryKey() {
	return "_voteID";
    }

    public String unique() {
	return _voteID.toString();
    }

    public String uniqueNull() {
	return "-1";
    }

    public Vote cloneObject() {
	return getDummyObject(new Integer(-1));
    }

    public void truncateFields(){

    }
}
