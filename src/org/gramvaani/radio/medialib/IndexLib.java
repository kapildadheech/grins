package org.gramvaani.radio.medialib;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.app.gui.StatsDisplay;
import org.gramvaani.radio.app.providers.MetadataProvider;

import org.apache.lucene.index.*;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.standard.*;
import org.apache.lucene.store.*;
import org.apache.lucene.document.*;
import org.apache.lucene.search.*;
import org.apache.lucene.queryParser.*;

import java.util.*;
import java.io.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class IndexLib {
    public final static String GENERIC_ANCHOR_TERMS 	= "GENERIC_ANCHOR_TERMS";
    public final static String ANCHOR_ID 		= "ANCHOR_ID";
    public final static String FILTER_ID 		= "FILTER_ID";
    public final static String INDEX_ID 		= "INDEX_ID";
    public final static String ANCHOR_INDEX 		= "ANCHOR_INDEX";
    public final static String PROGRAM_INDEX 		= "PROGRAM_INDEX";

    public final static String PROGRAM_ITEM_ID 		= "PROGRAM_ITEM_ID";

    static final int DUMP_ANCHOR_MAX_HITS   = 50;
    static final int DUMP_PROGRAM_MAX_HITS  = 1000;
    static final int SEARCH_ANCHOR_MAX_HITS = 50; 

    static final String ERROR_NO_SPACE_STRING = "No space left on device";

    public static final int ERROR_NO_SPACE_CODE = -10;
    public static final int ERROR_UNKNOWN_CODE  = -11;

    public static final String ERROR_NO_SPACE = "ERROR_NO_SPACE";
    public static final String ERROR_UNKNOWN  = "ERROR_UNKNOWN";

    public static final int POOL_THREAD_COUNT = 1;

    static IndexLib indexlib = null;
    public static IndexLib getIndexLib(StationConfiguration stationConfiguration, LogUtilities logger, MediaLib medialib) {
	if (indexlib == null) {
	    	try{
		    indexlib = new IndexLib(stationConfiguration, logger, medialib);
		} catch (Exception e){
		    logger.error("GetIndexLib: Could not instantiate IndexLib, ", e);
		    return null;
		}
	}

	if (indexlib.isActive())
	    return indexlib;
	else
	    return null;
    }

    protected StationConfiguration stationConfiguration;
    protected final LogUtilities logger;
    protected MediaLib medialib;
    protected IndexWriter programIndexWriter, anchorIndexWriter;
    protected IndexSearcher programIndexSearcher, anchorIndexSearcher;
    protected IndexReader programIndexReader, anchorIndexReader;
    protected Analyzer programIndexAnalyzer, anchorIndexAnalyzer, programIndexKeywordAnalyzer, anchorIndexKeywordAnalyzer;
    protected String programIndexDir, anchorIndexDir;
    protected boolean active;
    protected IPCServer ipcServer;
    protected boolean indexLibInitializing = true;
    protected final Object indexSyncObject = new Object();
    ExecutorService executor;
    
    final String whereSubClauseFilter;

    public IndexLib(StationConfiguration stationConfiguration, final LogUtilities logger, MediaLib medialib) {
	this.stationConfiguration = stationConfiguration;
	this.logger = logger;
	this.medialib = medialib;
	this.programIndexDir = stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR)+"/"+stationConfiguration.getStringParam(StationConfiguration.PROGRAM_INDEX_DIR);
	this.anchorIndexDir = stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR)+"/"+stationConfiguration.getStringParam(StationConfiguration.ANCHOR_INDEX_DIR);
	
	
	this.programIndexAnalyzer = new StopAnalyzer();
	this.anchorIndexAnalyzer = new StopAnalyzer();
	this.programIndexKeywordAnalyzer = new KeywordAnalyzer();
	this.anchorIndexKeywordAnalyzer = new KeywordAnalyzer();
	
	whereSubClauseFilter = " and " + Metadata.getClassName(RadioProgramMetadata.class) + "."
	    + RadioProgramMetadata.getUploadStoreAttemptsFieldName() + "=" + Integer.toString(FileStoreManager.UPLOAD_DONE)
	    + " and " + Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getStateFieldName()
	    + "!=" + Integer.toString(LibService.REPLACING_ORIGINAL) ;
	    
	executor = Executors.newFixedThreadPool(POOL_THREAD_COUNT);

	if ((programIndexWriter = initIndexWriter(programIndexDir, programIndexAnalyzer, false)) == null || 
	   (anchorIndexWriter = initIndexWriter(anchorIndexDir, anchorIndexAnalyzer, false)) == null) {
	    active = false;
	    return;
	}

	if ((programIndexSearcher = initIndexSearcher(programIndexDir)) == null || 
	   (anchorIndexSearcher = initIndexSearcher(anchorIndexDir)) == null) {
	    active = false;
	    return;
	}
	
	programIndexReader = programIndexSearcher.getIndexReader();
	anchorIndexReader = anchorIndexSearcher.getIndexReader();

	active = true;
	indexLibInitializing = false;
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    try {
			logger.info("ShutdownHook: Closing anchorIndexWriter");
			anchorIndexWriter.close();
			anchorIndexReader.close();
		    } catch(Exception e) {
			logger.error("ShutdownHook: unable to close anchorIndexWriter", e);
		    }
		    try {
			logger.info("ShutdownHook: Closing programIndexWriter");
			programIndexWriter.close();
			programIndexReader.close();
		    } catch(Exception e) {
			logger.error("ShutdownHook: Unable to close programIndexWriter", e);
		    }

		    executor.shutdown();
		}
	    });

    }

    public void setIPCServer(IPCServer ipcServer) {
	this.ipcServer = ipcServer;
    }

    public void setMediaLib(MediaLib medailib) {
	this.medialib = medialib;
    }

    protected synchronized IndexWriter initIndexWriter(String path, Analyzer analyzer, boolean forceNew) {
	logger.debug("initIndexWriter: initializing indexWriter " + path); 
	try{
	    File lockFile = new File(path+System.getProperty("file.separator")+"write.lock");
	    if (lockFile.exists())
		if (lockFile.delete()){
		    logger.warn("InitIndexWriter: Deleted existing lock file.");
		} else {
		    logger.error("InitIndexWriter: Unable to remove existing lock file.");
		}
	    
	} catch (Exception e) {
	    logger.error("InitIndexWriter: Unable to remove existing lock file.", e);
	}

	boolean refreshIndex = false;
	for (int i = 0; i < 2; i++){
	    try {
		FSDirectory directory = FSDirectory.getDirectory(new File(path));
		IndexWriter indexWriter;

		if (!new File(path+"/segments.gen").exists())
		    refreshIndex = true;

		if (!forceNew && (new File(path + "/segments.gen")).exists())
		    indexWriter = new IndexWriter(directory, analyzer, false, IndexWriter.MaxFieldLength.UNLIMITED);
		else
		    indexWriter = new IndexWriter(directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED);

		if (refreshIndex){
		    if (path.equals(anchorIndexDir))
			refreshAnchorIndex();
		    else
			refreshProgramIndex();
		}

		return indexWriter;
	    } catch(Exception e) {
		logger.error("initIndexWriter: Unable to initialize index writer", e);
		if (i == 1)
		    return null;
		if (e instanceof FileNotFoundException)
		    forceNew = true;
		refreshIndex = true;
	    }
	}
	return null;
    }

    protected synchronized IndexSearcher initIndexSearcher(String path) {
	logger.debug("initIndexSearcher: initializing indexSearcher " + path);
	for (int i = 0; i < 2; i++){
	    try {

		IndexSearcher indexSearcher = new IndexSearcher(path);
		return indexSearcher;

	    } catch(Exception e) {
		logger.error("initIndexSearcher: Unable to get index searcher", e);

		if (i == 1)
		    return null;

		if (e instanceof CorruptIndexException || 
		    e.getMessage().equals("read past EOF") ||
		    e.getMessage().startsWith("No sub-file with id") ||
		    e instanceof AlreadyClosedException ||
		    e instanceof FileNotFoundException ){

		    if (path.equals(programIndexDir))
			refreshProgramIndex();
		    else
			refreshAnchorIndex();
		}
	    }
	}
	return null;
    }

    public synchronized void refreshIndex(){
	logger.info("RefreshIndex:");
	refreshProgramIndex();
	refreshAnchorIndex();
    }
    
    protected void refreshProgramIndex(){
	logger.info("RefreshProgramIndex: Refreshing program index.");

	programIndexWriter = initIndexWriter(programIndexDir, programIndexAnalyzer, true);

	RadioProgramMetadata[] programs = medialib.getAll(new RadioProgramMetadata(""));
	
	for (RadioProgramMetadata program: programs){
	    if (program.getUploadStoreAttempts() != FileStoreManager.FILE_DELETED)
		updateProgram(program.getItemID());

	}

	flushProgramIndex();
    }

    protected void refreshAnchorIndex(){
	logger.info("RefreshAnchorIndex: Refreshing anchor index.");
	
	anchorIndexWriter = initIndexWriter(anchorIndexDir, anchorIndexAnalyzer, true);

	RadioProgramMetadata queryProgram = new RadioProgramMetadata("");
	Object[] langs = medialib.unique(queryProgram, queryProgram.getLanguageFieldName());
	
	ArrayList<String> languages = new ArrayList<String>();
	for (Object obj: langs)
	    languages.add((String) obj);

	Creator queryCreator = new Creator();
	Object[] affiliationArr = medialib.unique(queryCreator, queryCreator.getAffiliationFieldName());

	ArrayList<String> affiliations = new ArrayList<String>();
	for (Object obj: affiliationArr)
	    affiliations.add((String) obj);

	Entity queryEntity = new Entity();
	Object[] roleArr = medialib.unique(queryEntity, queryEntity.getEntityTypeFieldName());
	Object[] locationArr = medialib.unique(queryEntity, queryEntity.getLocationFieldName());
	Object[] nameArr = medialib.unique(queryEntity, queryEntity.getNameFieldName());
	
	ArrayList<String> roles = new ArrayList<String>();
	ArrayList<String> locations = new ArrayList<String>();
	ArrayList<String> names = new ArrayList<String>();

	for (Object obj: roleArr)
	    roles.add((String) obj);
	for (Object obj: locationArr)    
	    locations.add((String) obj);
	for (Object obj: nameArr)
	    names.add((String) obj);

	Tags queryTag = new Tags();

	Tags[] metadata = medialib.getAll(queryTag);
	HashSet<String> tagsCSV = new HashSet<String>();
	for (Tags data: metadata){
	    tagsCSV.add(data.getTagsCSV());
	}
	
	addNewAnchorTerms(LanguageFilter.LANGUAGE_ANCHOR, SearchFilter.LANGUAGE_FILTER, languages);
	addNewAnchorTerms(CreatorFilter.AFFILIATION_ANCHOR, SearchFilter.CREATOR_FILTER, affiliations);
	addNewAnchorTerms(CreatorFilter.ROLE_ANCHOR, SearchFilter.CREATOR_FILTER, roles);
	addNewAnchorTerms(CreatorFilter.LOCATION_ANCHOR, SearchFilter.CREATOR_FILTER, locations);
	addNewAnchorTerms(CreatorFilter.NAME_ANCHOR, SearchFilter.CREATOR_FILTER, names);
	//addNewAnchorTerms(TextFilter.TAGS_ANCHOR, SearchFilter.TEXT_FILTER, tagsCSV);

	Category[] allCategories = medialib.get(new Category());

	if (allCategories != null) {
	    for (Category category: allCategories) {
		if (category.getParentID() == -1) {
		    Category[] allChildren = category.getAllChildren(medialib);
		    for (Category child: allChildren) {
			Category[] grandChildren = child.getAllChildren(medialib);
			StringBuilder str = new StringBuilder();
			for (Category grandChild: grandChildren) {
			    str.append(","); str.append(grandChild.getLabel());
			}
			str.append(","); str.append(child.getLabel());
			str.append(","); str.append(category.getLabel());
			addAnchorTerm(CategoryFilter.CATEGORY_ANCHOR + "_" + child.getNodeName(),
				      SearchFilter.CATEGORY_FILTER + "_" + category.getNodeName(),
				      str.substring(1));
		    }
		}
	    }
	}
    }

    public synchronized boolean isActive() {
	return active;
    }

    private synchronized int flushIndexWriter(IndexWriter indexWriter) {
	try {
	    indexWriter.commit();
	    indexWriter.close();
	    logger.debug("flushIndexWriter: Index flushed.");
	} catch(Exception e) {
	    logger.error("flushIndexWriter: Unable to write changes in indexwriter to disk", e);
	    int error;

	    if (e.getMessage().startsWith(ERROR_NO_SPACE_STRING))
		error = ERROR_NO_SPACE_CODE;
	    else
		error = ERROR_UNKNOWN_CODE;

	    return error;
	}
	return 0;
    }

    public synchronized int flushAnchorIndex() {
	logger.debug("flushAnchorIndex: flushing indexWriter");
	int retVal;
	if ((retVal = flushIndexWriter(anchorIndexWriter)) < 0 )
	    return retVal;
	logger.debug("flushAnchorIndex: recreating indexWriter");
	if ((anchorIndexWriter = initIndexWriter(anchorIndexDir, anchorIndexAnalyzer, false)) == null)
	    return -1;
	logger.debug("flushAnchorIndex: recreating indexSearcher");
	if ((anchorIndexSearcher = initIndexSearcher(anchorIndexDir)) == null)
	    return -2;
	logger.debug("flushAnchorIndex: recreating indexReader");
	anchorIndexReader = anchorIndexSearcher.getIndexReader();

	return 0;
    }

    public synchronized int flushProgramIndex() {
	return flushProgramIndex(false);
    }

    public synchronized int flushProgramIndex(boolean flushSerially) {
	FlushProgramIndexRunnable flushRunnable = new FlushProgramIndexRunnable();

	if (indexLibInitializing || flushSerially) {
	    flushRunnable.run();
	    return 0;
	} else {
	    executor.execute(flushRunnable);
	    return 0;
	}
    }

    public synchronized void dumpAnchorIndex() {
	logger.debug("dumpAnchorIndex: dumping");
	try {
	    QueryParser parser = new QueryParser(INDEX_ID, anchorIndexKeywordAnalyzer);
	    Query query = parser.parse("INDEX_ID:\"" + ANCHOR_INDEX + "\"");
	    TopDocCollector collector = new TopDocCollector(DUMP_ANCHOR_MAX_HITS);
	    anchorIndexSearcher.search(query, collector);
	    ScoreDoc[] hits = collector.topDocs().scoreDocs;
	    
	    for (int i = 0; i < hits.length; i++){
		int docID = hits[i].doc;
		Document doc = anchorIndexSearcher.doc(docID);

		Field genericTerms = doc.getField(GENERIC_ANCHOR_TERMS);
		Field anchorID = doc.getField(ANCHOR_ID);
		Field filterID = doc.getField(FILTER_ID);
		logger.debug("dumping: " + filterID.stringValue() + ":" + anchorID.stringValue() + ":" + genericTerms.stringValue());
	    }

	} catch(Exception e) {
	    logger.error("dumpAnchorIndex: Unable to search and dump", e);
	}
    }

    public synchronized void dumpProgramIndex() {
	logger.debug("dumpProgramIndex: dumping");
	try {
	    QueryParser parser = new QueryParser(INDEX_ID, programIndexKeywordAnalyzer);
	    Query query = parser.parse("INDEX_ID:\"" + PROGRAM_INDEX + "\"");
	    TopDocCollector collector = new TopDocCollector(DUMP_PROGRAM_MAX_HITS);
	    programIndexSearcher.search(query, collector);
	    ScoreDoc[] hits = collector.topDocs().scoreDocs;

	    for (int i = 0; i < hits.length; i++){
		int docID = hits[i].doc;
		Document doc = programIndexSearcher.doc(docID);

		Field programID = doc.getField(PROGRAM_ITEM_ID);
		Field type = doc.getField(TypeFilter.TYPE_ANCHOR);
		Field language = doc.getField(LanguageFilter.LANGUAGE_ANCHOR);
		Field title = doc.getField(TextFilter.TITLE_ANCHOR);
		Field desc = doc.getField(TextFilter.DESC_ANCHOR);
		Field license = doc.getField(LicenseFilter.LICENSE_ANCHOR);
		Field affiliation = doc.getField(CreatorFilter.AFFILIATION_ANCHOR);
		Field name = doc.getField(CreatorFilter.NAME_ANCHOR);
		Field location = doc.getField(CreatorFilter.LOCATION_ANCHOR);
		Field role = doc.getField(CreatorFilter.ROLE_ANCHOR);
		Field category = doc.getField(CategoryFilter.CATEGORY_ANCHOR);
		//Field tags = doc.getField(TextFilter.TAGS_ANCHOR);
		//		Field length = doc.getField(LengthFilter.LENGTH_ANCHOR);
		logger.info("dumping: " + programID.stringValue() + ":" + type.stringValue() + ":" + language.stringValue() + ":" +
			    title.stringValue() + ":" + desc.stringValue() + ":" + license.stringValue() + ":" + affiliation.stringValue() + ":" +
			    name.stringValue() + ":" + location.stringValue() + ":" + role.stringValue() + ":" + category.stringValue() + ":" 
			    ); //tags.stringValue() + ":");
		
	    }
	    
	} catch(Exception e) {
	    logger.error("dumpProgramIndex: Unable to search and dump", e);
	}
    }

    public synchronized int addAnchorTerm(String anchorID, String filterID, String term) {
	logger.info("addAnchorTerm: adding anchor term " + anchorID + ":" + filterID + ":" + term);
	try {
	    QueryParser parser = new QueryParser(GENERIC_ANCHOR_TERMS, anchorIndexKeywordAnalyzer);
	    Query query = parser.parse(ANCHOR_ID + ":" + anchorID);

	    TopDocCollector collector = new TopDocCollector(1);
	    anchorIndexSearcher.search(query, collector);
	    ScoreDoc[] hits = collector.topDocs().scoreDocs;

	    if (hits.length > 0) {
		try {
		    Document doc = anchorIndexSearcher.doc(hits[0].doc);

		    Field genericTerms = doc.getField(GENERIC_ANCHOR_TERMS);
		    StringBuilder str = new StringBuilder(genericTerms.stringValue());
		    if (str.length() > 0)
			str.append(",");
		    logger.debug("addAnchorTerm: earlier term was " + str.toString());
		    str.append(term);
		    logger.debug("addAnchorTerm: before delete " + anchorIndexReader.numDocs());
		    anchorIndexWriter.deleteDocuments(new Term(ANCHOR_ID, anchorID));
		    logger.debug("addAnchorTerm: after delete " + anchorIndexWriter.numDocs());

		    Document newdoc = new Document();
		    newdoc.add(new Field(INDEX_ID, ANCHOR_INDEX, Field.Store.YES, Field.Index.NOT_ANALYZED));
		    newdoc.add(new Field(ANCHOR_ID, anchorID, Field.Store.YES, Field.Index.NOT_ANALYZED));
		    newdoc.add(new Field(GENERIC_ANCHOR_TERMS, str.toString(), Field.Store.YES, Field.Index.ANALYZED));
		    newdoc.add(new Field(FILTER_ID, filterID, Field.Store.YES, Field.Index.NO));
		    anchorIndexWriter.addDocument(newdoc);
		    flushAnchorIndex();

		    sendInvalidateCacheMessage(filterID, LibService.CACHE_INSERT, new String[]{anchorID});

		} catch(Exception e2) {
		    logger.error("addAnchorTerm: Unable to search and update", e2);
		    return -1;
		}
	    } else {
		try {
		    Document doc = new Document();
		    doc.add(new Field(INDEX_ID, ANCHOR_INDEX, Field.Store.YES, Field.Index.NOT_ANALYZED));
		    doc.add(new Field(ANCHOR_ID, anchorID, Field.Store.YES, Field.Index.NOT_ANALYZED));
		    doc.add(new Field(GENERIC_ANCHOR_TERMS, term, Field.Store.YES, Field.Index.ANALYZED));
		    doc.add(new Field(FILTER_ID, filterID, Field.Store.YES, Field.Index.NO));
		    anchorIndexWriter.addDocument(doc);
		    flushAnchorIndex();

		} catch(Exception e2) {
		    logger.error("addAnchorTerm: Unable to search and update", e2);
		    return -1;
		}
	    }
	} catch(Exception e) {
	    logger.error("addAnchorTerm: Unable to add anchor term", e);
	    return -1;
	}

	return 0;
    }

    public synchronized int addNewAnchorTerms(String anchorID, String filterID, Iterable<String> terms) {
	logger.debug("addNewAnchorTerms: adding anchor term " + anchorID + ":" + filterID );
	for (String term: terms){
	    try {
		Document doc = new Document();
		doc.add(new Field(INDEX_ID, ANCHOR_INDEX, Field.Store.YES, Field.Index.NOT_ANALYZED));
		doc.add(new Field(ANCHOR_ID, anchorID, Field.Store.YES, Field.Index.NOT_ANALYZED));
		doc.add(new Field(GENERIC_ANCHOR_TERMS, term, Field.Store.YES, Field.Index.ANALYZED));
		doc.add(new Field(FILTER_ID, filterID, Field.Store.YES, Field.Index.NO));
		anchorIndexWriter.addDocument(doc);
		logger.debug("addNewAnchorTerms: added term "+term+" to new anchor");
	    } catch(Exception e2) {
		logger.error("addNewAnchorTerms: Unable to search and update", e2);
		return -1;
	    }
	}
	flushAnchorIndex();
	
	return 0;
    }
    
    protected void sendInvalidateCacheMessage(String objectType, String actionType, String[] objectIDs) {
	logger.debug("sendInvalidateCacheMessage: sending message " + objectType + " actionType = " + actionType);
	if (ipcServer != null) {
	    if (objectType.equals(SearchFilter.LANGUAGE_FILTER) || objectType.equals(SearchFilter.CREATOR_FILTER) ||
	       objectType.equals(SearchFilter.LICENSE_FILTER) || objectType.equals(SearchFilter.TYPE_FILTER)) {
		InvalidateCacheMessage invalidateMessage = new InvalidateCacheMessage(RSController.INDEX_SERVICE, 
										      RSController.LIB_SERVICE,
										      new String[]{}, 
										      objectType, actionType, objectIDs);
		int retVal = ipcServer.handleOutgoingMessage(invalidateMessage);
		if (retVal == -1) {
		    logger.error("SendInvalidateCacheMessage: Unable to send message " + objectType + ":" + actionType);
		} else if (retVal == -2) {
		    logger.error("SendInvalidateCacheMessage: Unable to send message " + objectType + ":" + actionType);
		} else {
		    logger.info("SendInvalidateCacheMessage: Message sent " + objectType + ":" + actionType);
		}
	    }
	} else {
	    logger.error("SendInvalidateCacheMessage: Unable to send message because ipcServer not set");
	}
    }

    public int removeProgram(String programID) {
	return removeProgram(programID, false);
    }

    public int removeProgram(final String programID, boolean serialize) {
	Runnable r = new Runnable() {
		public void run() {
		    synchronized(IndexLib.this) {
			synchronized(indexSyncObject) {
			    try {
				programIndexWriter.deleteDocuments(new Term(PROGRAM_ITEM_ID, programID));
			    } catch(Exception e) {
				logger.error("removeProgram: Unable to remove program", e);
				return;
			    }
			    
			    logger.info("removeProgram: removed: "+programID);
			    return;
			}
		    }
		}
	    };
	if (serialize) {
	    r.run();
	} else {
	    executor.execute(r);
	}

	return 0;
    }

    public int updateProgram(String programID) {
	return updateProgram(programID, false);
    }

    public int updateProgram(final String programID, boolean serialize) {
	Runnable r = new Runnable() {
		public void run() {
		    synchronized (IndexLib.this) {
			synchronized (indexSyncObject) {
			    RadioProgramMetadata program = medialib.getSingle(new RadioProgramMetadata(programID));
			    if (program == null)
				return;
			    logger.debug("updateProgram: updating " + program.getItemID());
			    if (removeProgram(programID, true) == -1)
				return;
			    
			    Creator[] creators = program.populate__creators(medialib);
			    ItemCategory[] categories = program.populate__categories(medialib);
			    Tags[] tags = program.populate__tags(medialib);

			    //	PlayoutHistory[] playoutHistory = program.populate__history(medialib);
			    
			    StringBuilder tagStr = new StringBuilder();
			    for (Tags tag: tags) {
				tagStr.append(tag.getTagsCSV());
				tagStr.append(",");
			    }
			    String tagString = "";
			    if (tagStr.length() > 0)
				tagString = tagStr.substring(0, tagStr.length() - 1);
			    else
				tagString = "";
			    
			    StringBuilder creatorStr = new StringBuilder();
			    StringBuilder entityStr = new StringBuilder();
			    for (Creator thisCreator: creators) {
				creatorStr.append(thisCreator.getAffiliation());
				creatorStr.append(".");
				
				Entity thisEntity = thisCreator.getEntity();
				entityStr.append(thisEntity.getName() + " ");
				entityStr.append(thisEntity.getLocation() + " ");
				entityStr.append(thisEntity.getEntityType() + ".");
			    }
			    String creatorString = "";
			    if (creatorStr.length() > 0)
				creatorString = creatorStr.substring(0, creatorStr.length() - 1);
			    else
				creatorString = "";
			    String entityString = "";
			    if (entityStr.length() > 0)
				entityString = entityStr.substring(0, entityStr.length() - 1);
			    else
				entityString = "";
			    
			    if (creators.length == 0){
				Creator dummyCreator = new Creator();
				dummyCreator.setEntity(new Entity());
				creators = new Creator[] {dummyCreator};
			    }
			    
			    if (categories.length == 0){
				ItemCategory dummyCategory = new ItemCategory();
				dummyCategory.setCategory(new Category());
				categories = new ItemCategory[] {dummyCategory};
			    }
			    
			    //XXX: moved loop on category inside. verify
			    for (Creator creator: creators) {
				//for (ItemCategory category: categories) {
				    //		for (PlayoutHistory history: playoutHistory) {
				    try {
					Document doc = new Document();
					doc.add(new Field(INDEX_ID, PROGRAM_INDEX, Field.Store.YES, Field.Index.NOT_ANALYZED));
					doc.add(new Field(PROGRAM_ITEM_ID, program.getItemID(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					doc.add(new Field(TypeFilter.TYPE_ANCHOR, program.getType(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					doc.add(new Field(LanguageFilter.LANGUAGE_ANCHOR, program.getLanguage(), Field.Store.YES, Field.Index.ANALYZED));
					doc.add(new Field(TextFilter.TITLE_ANCHOR, program.getTitle(), Field.Store.YES, Field.Index.ANALYZED));
					doc.add(new Field(TextFilter.DESC_ANCHOR, program.getDescription(), Field.Store.YES, Field.Index.ANALYZED));
					doc.add(new Field(LicenseFilter.LICENSE_ANCHOR, program.getLicenseType(), Field.Store.YES, Field.Index.ANALYZED));
					doc.add(new Field(CreatorFilter.AFFILIATION_ANCHOR, creator.getAffiliation(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					doc.add(new Field(CreatorFilter.NAME_ANCHOR, creator.getEntity().getName(), Field.Store.YES, Field.Index.ANALYZED));
					doc.add(new Field(CreatorFilter.LOCATION_ANCHOR, creator.getEntity().getLocation(), Field.Store.YES, Field.Index.ANALYZED));
					doc.add(new Field(CreatorFilter.ROLE_ANCHOR, creator.getEntity().getEntityType(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					String categoryCSV = "";
					
					for (ItemCategory category: categories){
					    if (category.getCategory() == null)
						continue;
					    if (category.getCategory().getNodeID() != -1) {
						
						categoryCSV += category.getCategory().getAllChildrenNamesCSV(medialib) + "," + category.getCategory().getNodeName();
						if (categoryCSV.startsWith(","))
						    categoryCSV = categoryCSV.substring(1);
					    }
					}
					
					doc.add(new Field(CategoryFilter.CATEGORY_ANCHOR, categoryCSV, Field.Store.YES, Field.Index.ANALYZED));
					//doc.add(new Field(TextFilter.TAGS_ANCHOR, tagString, Field.Store.YES, Field.Index.ANALYZED));
					doc.add(new Field(TextFilter.CREATOR_ANCHOR, creatorString, Field.Store.YES, Field.Index.ANALYZED));
					doc.add(new Field(TextFilter.ENTITY_ANCHOR, entityString, Field.Store.YES, Field.Index.ANALYZED));
					//doc.add(new Field(LengthFilter.LENGTH_ANCHOR, new Long(program.getLength()).toString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					//doc.add(new Field(PlayoutHistoryFilter.PLAYOUTHISTORY_ANCHOR, new Long(history.getTelecastTime()).toString(), Field.Store.YES,
					//Field.Index.NOT_ANALYZED));
					programIndexWriter.addDocument(doc);


				    } catch(Exception e2) {
					logger.error("Unable to add document", e2);
				    }
				    //		}
				    //}
			    }
			    
			    RadioProgramMetadata queryProgram = new RadioProgramMetadata(program.getItemID());
			    RadioProgramMetadata updateProgram = new RadioProgramMetadata(program.getItemID());
			    updateProgram.setIndexed(LibService.MEDIA_INDEXED);
			    
			    if (medialib.update(queryProgram, updateProgram, false) < 0)
				logger.error("UpdateProgram: Unable to set state to MEDIA_INDEXED in database: "+program.getItemID());
			    
			    logger.info("updateProgram: updated: "+program.getItemID());
			    return;	
			}
		    }
		}
	    };
	if (serialize) {
	    r.run();
	} else {
	    executor.execute(r);
	}
	return 0;
    }

    public synchronized Hashtable<String, ArrayList<String>> searchAnchorIndex(String text) {
	logger.info("searchAnchorIndex: searching anchor index for " + text);
	Hashtable<String, ArrayList<String>> filterMatches = new Hashtable<String, ArrayList<String>>();
	if (text.length() == 0){
	    logger.warn("SearchAnchorIndex: Searching for null string.");
	    return filterMatches;
	}

	try {
	    QueryParser parser = new QueryParser(GENERIC_ANCHOR_TERMS, anchorIndexAnalyzer);
	    Query query = parser.parse(GENERIC_ANCHOR_TERMS + ":" + text);

	    TopDocCollector collector = new TopDocCollector(SEARCH_ANCHOR_MAX_HITS);
	    anchorIndexSearcher.search(query, collector);
	    ScoreDoc[] hits = collector.topDocs().scoreDocs;

	    for (int i = 0; i < hits.length; i++){
		int docID = hits[i].doc;
		Document doc = anchorIndexSearcher.doc(docID);
		
		String anchorID = doc.get(ANCHOR_ID);
		String filterID = doc.get(FILTER_ID);
		if (filterMatches.get(filterID) == null)
		    filterMatches.put(filterID, new ArrayList<String>());
		filterMatches.get(filterID).add(anchorID);
		logger.info("searchAnchorIndex: Matched " + anchorID + ":" + filterID + " for " + doc.get(GENERIC_ANCHOR_TERMS));
	    }

	} catch(Exception e) {
	    logger.error("searchAnchorIndex: Unable to search", e);
	}
		
	return filterMatches;
    }

    Hashtable<String, Integer> numSearchResults = new Hashtable<String, Integer>();
    public synchronized int numSearchResults(String queryString, String whereClause, String joinTableStr) {
	String hashKey = queryString + ":" + whereClause + ":" + joinTableStr;
	if (numSearchResults.get(hashKey) != null) {
	    int retval = numSearchResults.get(hashKey).intValue();
	    numSearchResults.remove(hashKey);
	    return retval;
	} else {
	    return 0;
	}
    }

    // XXX this coalesces the same programIDs into one result. should be returning some indication of what fields matched for snippet generation
    public synchronized String[] searchProgramIndex(String queryString, String whereClause, String joinTableStr, 
						    int startingOffset, int maxResults) {
	logger.info("searchProgramIndex: searching program index for " + queryString + ":" + whereClause + ":" + joinTableStr);

	String hashKey = queryString + ":" + whereClause + ":" + joinTableStr;
	Object[] whereProgramIDs = null;

	if (queryString.length() == 0) {

	    if (!whereClause.equals("") && !joinTableStr.equals("")) {

		whereClause +=  whereSubClauseFilter;

		whereProgramIDs = medialib.select(joinTableStr, whereClause, 
						  Metadata.getClassName(RadioProgramMetadata.class) + "." +
						  RadioProgramMetadata.getItemIDFieldName(), -1);
		if (whereProgramIDs.length > 0)
		    numSearchResults.put(hashKey, new Integer(whereProgramIDs.length));

		if (whereProgramIDs.length > startingOffset) {
		    String[] retarr = new String[Math.min(whereProgramIDs.length - startingOffset,
							  maxResults)];
		    for (int i = startingOffset; i < whereProgramIDs.length && i < startingOffset + maxResults; i++)
			retarr[i - startingOffset] = (String)whereProgramIDs[i];

		    return retarr;
		}
	    }

	    return new String[]{};
	}

	if (!whereClause.equals("") && !joinTableStr.equals("")) {
	    whereClause +=  whereSubClauseFilter;
	    whereProgramIDs = medialib.select(joinTableStr, whereClause, 
					      Metadata.getClassName(RadioProgramMetadata.class) + "." +
					      RadioProgramMetadata.getItemIDFieldName(), -1);
	} else {
	    whereProgramIDs = new String[]{};
	}

	HashSet<String> wherePrograms = new HashSet<String>();
	for (Object whereProgramID: whereProgramIDs) {
	    wherePrograms.add((String)whereProgramID);
	    logger.debug("SearchProgramIndex: whereProgramID: " + whereProgramID);
	}
	logger.debug("SearchProgramIndex: Total wherePrograms: "+wherePrograms.size());
	
	HashSet<String> programIDs = new HashSet<String>();
	
	try {
	    QueryParser parser = new QueryParser(TextFilter.TITLE_ANCHOR, programIndexAnalyzer);
	    Query query = parser.parse(queryString);

	    TopDocCollector collector = new TopDocCollector(9999);
	    programIndexSearcher.search(query, collector);
	    ScoreDoc[] hits = collector.topDocs().scoreDocs;
	 
	    if (startingOffset > 0 && hits.length > 0)
		logger.debug("SearchProgramIndex: Skipping initial entries.");
	    
	    boolean ignoreWherePrograms = (whereClause.equals("") || joinTableStr.equals(""));//(wherePrograms.size() == 0);

	    logger.debug("SearchProgramIndex: Total index hits = " + hits.length + " offset: " + startingOffset + " max: " + maxResults);
	    
	    int numResults = 0;
	    HashSet<String> allPrograms = new HashSet<String>();

	    for (int i = 0; i < hits.length; i++){
		int docID = hits[i].doc;
		Document doc = programIndexSearcher.doc(docID);

		String programID = doc.get(PROGRAM_ITEM_ID);
		logger.debug("SearchProgramIndex: Index hit programID: " + programID);
		if (ignoreWherePrograms || wherePrograms.remove(programID) != false) {
		    if (numResults >= startingOffset && programIDs.size() < maxResults)
			programIDs.add(programID);
		    
		    if (allPrograms.add(programID))
			numResults++;
		}
	    }
	    
	    //XXX: hits returning multiple items with same programID!!

	    numSearchResults.put(hashKey, new Integer(allPrograms.size()));

	} catch(Exception e) {
	    logger.error("searchProgramIndex: Unable to search", e);
	}

	ArrayList<String> programArr = new ArrayList<String>();
	programArr.addAll(programIDs);
	Collections.sort(programArr);

	for (String programStr: programArr)
	    logger.debug("SearchProgramIndex: returning " + programStr);
       
	return programArr.toArray(new String[]{});
    }

    public synchronized GraphStats getGraphStats(String queryString, String whereClause, String joinTableStr, 
						 long minTelecastTime, long maxTelecastTime, int graphType) {
	logger.info("getBarGraphStats: getting bar graph stats for " + queryString + ":" + whereClause + ":" + joinTableStr + ":" + minTelecastTime + ":" + maxTelecastTime);
	

	String[] programIDs = new String[]{};
	if (!queryString.equals(MetadataProvider.NO_QUERY))
	    programIDs = searchProgramIndex(queryString, whereClause, joinTableStr, 0, 9999);
	else if (!whereClause.equals("")) {
	    Object[] programObjs = medialib.select(Metadata.getClassName(RadioProgramMetadata.class),
						   whereClause + whereSubClauseFilter, 
						   RadioProgramMetadata.getItemIDFieldName(), -1);
	    if (programObjs != null) {
		programIDs = new String[programObjs.length];
		int i = 0;
		for (Object programObj: programObjs)
		    programIDs[i++] = (String)programObj;
	    }
	}

	int maxNumPerIter = 100;
	int maxIter = programIDs.length / maxNumPerIter  + (programIDs.length % maxNumPerIter == 0 ? 0 : 1);

	long[] iterMinTelecastTime = new long[maxIter];
	long[] iterMaxTelecastTime = new long[maxIter];
	String[] whereClauses = new String[maxIter];


	for (int iter = 0; iter < maxIter; iter++) {

	    StringBuilder str = new StringBuilder();
	    str.append("(");
	    for (int i = iter * maxNumPerIter; i < Math.min((iter + 1) * maxNumPerIter, programIDs.length); i++) {
		String programID = programIDs[i];
		str.append(PlayoutHistory.getItemIDFieldName());
		str.append("='");
		str.append(programID);
		str.append("' or ");
	    }

	    if (str.length() > 1)
		whereClause = str.substring(0, str.length() - 3) + ")";
	    else
		whereClause = "";

	    whereClauses[iter] = whereClause;

	    // get min telecast time
	    String whereMinClause = whereClause + (whereClause.equals("") ? "" : " and ") + 
		PlayoutHistory.getTelecastTimeFieldName() + " >= " + minTelecastTime;

	    Object minTel = medialib.mathOp(Metadata.getClassName(PlayoutHistory.class), whereMinClause, 
					    PlayoutHistory.getTelecastTimeFieldName(), MediaLib.MIN_OP);

	    if (minTel instanceof Long)
		iterMinTelecastTime[iter] = (((Long)minTel).longValue() > 0 ? ((Long)minTel).longValue() : 
					     (minTelecastTime > 0 ? minTelecastTime : Long.MAX_VALUE));
	    else
		iterMinTelecastTime[iter] = (minTelecastTime > 0 ? minTelecastTime : Long.MAX_VALUE);
	
	    // get max telecast time
	    String whereMaxClause;
	    if (maxTelecastTime > 0)
		whereMaxClause = whereClause + (whereClause.equals("") ? "" : " and ") + 
		    PlayoutHistory.getTelecastTimeFieldName() + " <= " + maxTelecastTime;	
	    else
		whereMaxClause = whereClause;
	    
	    Object maxTel = medialib.mathOp(Metadata.getClassName(PlayoutHistory.class), whereMaxClause, 
					    PlayoutHistory.getTelecastTimeFieldName(), MediaLib.MAX_OP);
	    if (maxTel instanceof Long)
		iterMaxTelecastTime[iter] = (((Long)maxTel).longValue() > iterMinTelecastTime[iter] ? ((Long)maxTel).longValue() : 
				   (maxTelecastTime > 0 ? maxTelecastTime : System.currentTimeMillis()));
	    else
		iterMaxTelecastTime[iter] = (maxTelecastTime > 0 ? maxTelecastTime : System.currentTimeMillis());
	    
	    // ??? Adi -- why?	    iterMaxTelecastTime[iter] += (iterMaxTelecastTime[iter] % StatsDisplay.MAX_BINS);
	
	}	

	for (int iter = 0; iter < maxIter; iter++) {
	    if (minTelecastTime < 1 || minTelecastTime > iterMinTelecastTime[iter])
		minTelecastTime = iterMinTelecastTime[iter];
	    if (maxTelecastTime < iterMaxTelecastTime[iter])
		maxTelecastTime = iterMaxTelecastTime[iter];
	}

	if (minTelecastTime == Long.MAX_VALUE)
	    minTelecastTime = -1;

	// make x labels
	long approxBinInterval = (maxTelecastTime - minTelecastTime) / StatsDisplay.MAX_BINS;
	Calendar calendar = new GregorianCalendar();
	String[] xLabels = new String[StatsDisplay.MAX_BINS + 1];
		
	if (approxBinInterval >= (long)10 * 365 * 24 * 60 * 60 * 1000L) {                       // order of decades
	    for (int i = 0; i < StatsDisplay.MAX_BINS + 1; i++) {
		calendar.setTimeInMillis(minTelecastTime + approxBinInterval * i);
		xLabels[i] = (new Integer(calendar.get(Calendar.YEAR))).toString();
	    }

	} else if (approxBinInterval < (long) 3 * 60 * 60 * 1000L) {                                // order of minutes
	    calendar.setTimeInMillis(minTelecastTime);
	    for (int i = 0; i < StatsDisplay.MAX_BINS + 1; i++) {
		calendar.setTimeInMillis(minTelecastTime + approxBinInterval * i);
		if (i % (StatsDisplay.MAX_BINS / 4) == 0) {
		    xLabels[i] = (new Integer(calendar.get(Calendar.MINUTE))).toString() + " min " +
			(new Integer(calendar.get(Calendar.HOUR_OF_DAY))).toString() + " hrs " +  
			(new Integer(calendar.get(Calendar.DAY_OF_MONTH))).toString() + " " + StatsDisplay.months[calendar.get(Calendar.MONTH)] +
			" " + (new Integer(calendar.get(Calendar.YEAR))).toString();
		} else {
		    xLabels[i] = (new Integer(calendar.get(Calendar.MINUTE))).toString() + " min";
		}
	    }

	} else if (approxBinInterval < (long) 3 * 24 * 60 * 60 * 1000L) {                                // order of hours
	    calendar.setTimeInMillis(minTelecastTime);
	    for (int i = 0; i < StatsDisplay.MAX_BINS + 1; i++) {
		calendar.setTimeInMillis(minTelecastTime + approxBinInterval * i);
		if (i % (StatsDisplay.MAX_BINS / 4) == 0) {
		    xLabels[i] = (new Integer(calendar.get(Calendar.HOUR_OF_DAY))).toString() + " hrs " +
			(new Integer(calendar.get(Calendar.DAY_OF_MONTH))).toString() + " " + StatsDisplay.months[calendar.get(Calendar.MONTH)] +
			" " + (new Integer(calendar.get(Calendar.YEAR))).toString();
		} else {
		    xLabels[i] = (new Integer(calendar.get(Calendar.HOUR_OF_DAY))).toString() + " hrs";
		}
	    }

	} else if (approxBinInterval < (long) 3 * 30 * 24 * 60 * 60 * 1000L) {                           // order of days
	    calendar.setTimeInMillis(minTelecastTime);
	    for (int i = 0; i < StatsDisplay.MAX_BINS + 1; i++) {
		calendar.setTimeInMillis(minTelecastTime + approxBinInterval * i);
		if (i % (StatsDisplay.MAX_BINS / 4) == 0) {
		    xLabels[i] = (new Integer(calendar.get(Calendar.DAY_OF_MONTH))).toString() + " " + StatsDisplay.months[calendar.get(Calendar.MONTH)] + " " +
			(new Integer(calendar.get(Calendar.YEAR))).toString();	    
		    
		} else {
		    xLabels[i] = (new Integer(calendar.get(Calendar.DAY_OF_MONTH))).toString() + " " + StatsDisplay.months[calendar.get(Calendar.MONTH)];
		}
	    }

	} else if (approxBinInterval < (long)3 * 365 * 24 * 60 * 60 * 1000L) {                      // order of months
	    calendar.setTimeInMillis(minTelecastTime);
	    for (int i = 0; i < StatsDisplay.MAX_BINS + 1; i++) {
		calendar.setTimeInMillis(minTelecastTime + approxBinInterval * i);
		if (i % (StatsDisplay.MAX_BINS / 4) == 0) {
		    xLabels[i] = StatsDisplay.months[calendar.get(Calendar.MONTH)] + " " + (new Integer(calendar.get(Calendar.YEAR))).toString();
		} else {
		    xLabels[i] = StatsDisplay.months[calendar.get(Calendar.MONTH)];
		}
	    }

	} else {                                                                                  // order of years
	    for (int i = 0; i < StatsDisplay.MAX_BINS + 1; i++) {
		calendar.setTimeInMillis(minTelecastTime + approxBinInterval * i);
		xLabels[i] = (new Integer(calendar.get(Calendar.YEAR))).toString();
	    }
	}
	
	long[] dataItems = new long[]{};
	long maxDataItem = 0;

	if (graphType == StatsDisplay.BAR_GRAPH) {
	    dataItems = new long[StatsDisplay.MAX_BINS];
	    for (int iter = 0; iter < maxIter; iter++) {
		// get data points
		if (!whereClauses[iter].equals("")) {
		    for (int i = 0; i < StatsDisplay.MAX_BINS; i++) {
			long begTime = minTelecastTime + approxBinInterval * i;
			long endTime = minTelecastTime + approxBinInterval * (i + 1);
			String whereRangeClause = "(" + whereClauses[iter] + ") and " + PlayoutHistory.getTelecastTimeFieldName() + " >= " + begTime + " and " +
			    PlayoutHistory.getTelecastTimeFieldName() + " < " + endTime;
			Object countVal = medialib.mathOp(Metadata.getClassName(PlayoutHistory.class), whereRangeClause,
							  PlayoutHistory.getTelecastTimeFieldName(),
							  MediaLib.COUNT_OP);
			if (countVal instanceof Long) {
			    dataItems[i] += ((Long)countVal).longValue();
			    //System.out.println("value " + i + ":" + dataItems[i]);
			    if (dataItems[i] > maxDataItem)
				maxDataItem = dataItems[i];
			} else {
			    //System.out.println("obj: " + countVal + ":" + countVal.getClass());
			}
		    }
		}
		
		//System.out.println("maxDataItem = " + maxDataItem);
	    }
	} else {
	    if (!whereClause.equals("")) {
		String whereRangeClause = "(" + whereClause + ") and " + PlayoutHistory.getTelecastTimeFieldName() + " >= " + minTelecastTime + " and " + 
		    PlayoutHistory.getTelecastTimeFieldName() + " < " + maxTelecastTime;
		Object[] playTimes = medialib.select(Metadata.getClassName(PlayoutHistory.class), whereRangeClause, 
						     PlayoutHistory.getTelecastTimeFieldName(), -1);
		if (playTimes != null) {
		    dataItems = new long[playTimes.length];
		    int i = 0;
		    for (Object playTime: playTimes) {
			dataItems[i] = ((Long)playTime).longValue();
			calendar.setTimeInMillis(dataItems[i]);
			int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
			if (hourOfDay > maxDataItem)
			    maxDataItem = hourOfDay;
			i++;
		    }
		}
	    }
	    
	    logger.debug("maxDataItem = " + maxDataItem);
	}

	GraphStats graphStats = new GraphStats();
	graphStats.setMinTelecastTime(minTelecastTime);
	graphStats.setMaxTelecastTime(maxTelecastTime);
	graphStats.setXLabels(xLabels);
	graphStats.setDataItems(dataItems);
	graphStats.setApproxBinInterval(approxBinInterval);
	graphStats.setMaxDataItem(maxDataItem);

	return graphStats;
    }

    protected class FlushProgramIndexRunnable implements Runnable {
	public void run() {
	    synchronized (indexSyncObject) {
		logger.info("flushProgramIndex: flushing indexWriter");
		int retVal;
		if ((retVal = flushIndexWriter(programIndexWriter)) < 0)
		    return;
		logger.debug("flushProgramIndex: recreating indexWriter");
		if ((programIndexWriter = initIndexWriter(programIndexDir, programIndexAnalyzer, false)) == null)
		    return;
		logger.debug("flushProgramIndex: recreating indexSearcher");
		if ((programIndexSearcher = initIndexSearcher(programIndexDir)) == null)
			    return;
		logger.debug("flushProgramIndex: recreating indexReader");
		programIndexReader = programIndexSearcher.getIndexReader();
		return;
	    }
	}
    }
}

