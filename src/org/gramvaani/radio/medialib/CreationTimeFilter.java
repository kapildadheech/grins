package org.gramvaani.radio.medialib;

public class CreationTimeFilter extends SearchFilter {
    public long creationStartTime, creationEndTime;

    public static final String CREATION_TIME_ANCHOR = "CREATION_TIME_ANCHOR";
    boolean creationTimeActive = false;
    public static final String creationKey = Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getCreationTimeFieldName();

    public CreationTimeFilter() {
	creationStartTime = -1;
	creationEndTime = -1;
    }

    public String getFilterName() {
	return SearchFilter.CREATION_TIME_FILTER;
    }

    public String getFilterLabel() {
	return "Creation Time";
    }

    public String getFilterQueryClause() {
	return "";
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if (creationStartTime != -1 && creationEndTime != -1 && creationTimeActive) {
	    str.append(" ");
	    str.append(creationKey); 
	    str.append(" between ");
	    str.append(creationStartTime);
	    str.append(" and ");
	    str.append(creationEndTime);
	}
	
	return str.toString();
    }

    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(RadioProgramMetadata.class)};
    }

    public String getSelectedText(String anchorID) {
	return "";
    }

    public void setStartTime(long startTime) {
	creationStartTime = startTime;
    }

    public long getStartTime() {
	return creationStartTime;
    }

    public void setEndTime(long endTime) {
	creationEndTime = endTime;
    }

    public long getEndTime() {
	return creationEndTime;
    }

    public void activateAnchor(String anchor, String anchorText) {
	if (anchor.equals(CREATION_TIME_ANCHOR)){
	    creationTimeActive = true;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }
    
    
    public static long getMinCreationTime(MediaLib medialib) {
	Object minCreationTime = medialib.mathOp(new RadioProgramMetadata(""), RadioProgramMetadata.getCreationTimeFieldName(), MediaLib.MIN_OP);

	if (minCreationTime instanceof Long) {
	    return ((Long)minCreationTime).longValue();
	} else {
	    return -1;
	}
    }

    public static long getMaxCreationTime(MediaLib medialib) {
	Object maxCreationTime = medialib.mathOp(new RadioProgramMetadata(""), RadioProgramMetadata.getCreationTimeFieldName(), MediaLib.MAX_OP);

	if (maxCreationTime instanceof Long) {
	    return ((Long)maxCreationTime).longValue();
	} else {
	    return -1;
	}
    }
    

}
