package org.gramvaani.radio.medialib;

public interface IdleMonitorCallback {
    
    public void keepListeningForPreempts();
    public void detectedIdle();
    public void preemptBackground();

}