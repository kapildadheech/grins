package org.gramvaani.radio.medialib;

import java.util.*;
import org.gramvaani.radio.app.providers.CacheProvider;

public class SMS extends Metadata {
    //Message type
    public static final int INCOMING = 0;
    public static final int OUTGOING = 1;

    public static final String RECEIVED_STR   = "Received";
    public static final String PENDING_STR    = "Pending";
    public static final String SENT_STR       = "Sent";
    public static final String CANCELLED_STR  = "Cancelled";
    public static final String FAILED_STR     = "Failed";

    //Message status for outgoing messages
    public static final int PENDING       = 0;
    public static final int SENT          = 1;
    public static final int CANCELLED     = 2;
    public static final int FAILED        = 3;

    //Message status for incoming messages
    public static final int RECEIVED      = 0;

    public static final Hashtable<Integer, String> SEND_STATUS;
    public static final Hashtable<Integer, String> RECV_STATUS;
    
    static {
	SEND_STATUS = new Hashtable<Integer, String>();
	SEND_STATUS.put(PENDING, PENDING_STR);
	SEND_STATUS.put(SENT, SENT_STR);
	SEND_STATUS.put(CANCELLED, CANCELLED_STR);
	SEND_STATUS.put(FAILED, FAILED_STR);
	
	RECV_STATUS = new Hashtable<Integer, String>();
	RECV_STATUS.put(RECEIVED, RECEIVED_STR);
    }

    Integer _smsID;
    String _phoneNo;
    String _message;
    Long _dateTime;
    String _smsLine;
    Integer _type;
    Integer _status;


    Entity __entity;

    public String dump() {
	return "SMS: " + _smsID + ":" + _smsLine + ":" + _dateTime + ":" + _phoneNo + ":" + _message + ":" + _type + ":" + _status;
    }

    public SMS(String smsLine, String phoneNo, String message, int type) {
	_smsID = new Integer(-1);
	_phoneNo = phoneNo;
	_message = message;
	_type = type;
	_dateTime = -1L;
	_status = -1;
	_smsLine = smsLine;
    }

    public SMS(String smsLine, String phoneNo, String message, Long dateTime, int type, int status) {
	_smsID = new Integer(-1);
	_phoneNo = phoneNo;
	_message = message;
	_type = type;
	_dateTime = dateTime;
	_status = status;
	_smsLine = smsLine;
    }

    public SMS(int smsID) {
	this();
	_smsID = smsID;
    }

    public SMS() {
	_smsID = -1;
	_phoneNo = "";
	_message = "";
	_type = -1;
	_status = -1;
	_dateTime = -1L;
	_smsLine = "";
    }

    public int getID() {
	return _smsID;
    }

    public void setPhoneNo(String phoneNo) {
	this._phoneNo = phoneNo;
    }

    public String getPhoneNo() {
	return _phoneNo;
    }

    public static String getPhoneNoFieldName() {
	return "_phoneNo";
    }

    public String getMessage() {
	return _message;
    }

    public void setMessage(String message) {
	this._message = message;
    }

    public static String getMessageFieldName() {
	return "_message";
    }

    public Long getDateTime() {
	return _dateTime;
    }

    public void setDateTime(Long dateTime) {
	_dateTime = dateTime;
    }

    public static String getDateTimeFieldName() {
	return "_dateTime";
    }

    public String getSMSLine() {
	return _smsLine;
    }

    public void setSMSLine(String smsLine) {
	_smsLine = smsLine;
    }

    public static String getSMSLineFieldName() {
	return "_smsLine";
    }

    public int getType() {
	return _type;
    }

    public void setType(int type) {
	_type = type;
    }

    public static String getTypeFieldName() {
	return "_type";
    }

    public int getStatus() {
	return _status;
    }

    public void setStatus(int status) {
	_status = status;
    }

    public static String getStatusFieldName() {
	return "_status";
    }

    public Entity getEntity(CacheProvider cacheProvider) {
	if(__entity == null)
	    populateEntity(cacheProvider);

	return __entity;
    }

    public static SMS getDummyObject(Integer smsID) {
	SMS message = new SMS(smsID);
	return message;
    }

    public String primaryKey() {
	return "_smsID";
    }

    public String unique() {
	return _smsID.toString();
    }

    public String uniqueNull() {
	return "-1";
    }

    public SMS cloneObject() {
	return getDummyObject(new Integer(-1));
    }

    void populateEntity(CacheProvider cacheProvider) {
	//XXXX fetch entity with appropriate phone number
	//See if cacheprovider can be gotten rid off.
    }

    public void truncateFields(){

    }
}
