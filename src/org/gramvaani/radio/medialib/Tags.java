package org.gramvaani.radio.medialib;

import java.util.*;

public class Tags extends Metadata {
    protected String _itemID;
    protected Integer _startOffset;
    protected Integer _endOffset;
    protected String _tagsCSV;

    public String dump() {
	return "Tags: " + _itemID + ":" + _startOffset + ":" + _endOffset + ":" + _tagsCSV;
    }

    public Tags(String itemID, int startOffset, int endOffset, String tagsCSV) {
	this._itemID = itemID;
	this._startOffset = new Integer(startOffset);
	this._endOffset = new Integer(endOffset);
	this._tagsCSV = tagsCSV;
    }

    public Tags(String itemID) {
	_itemID = itemID;
	_startOffset = new Integer(-1);
	_endOffset = new Integer(-1);
	_tagsCSV = "";
    }

    public Tags() {
	_itemID = "";
	_startOffset = new Integer(-1);
	_endOffset = new Integer(-1);
	_tagsCSV = "";
    }

    public void setItemID(String itemID) {
	this._itemID = itemID;
    }

    public String getItemID() {
	return _itemID;
    }

    public int getStartOffset() {
	return _startOffset.intValue();
    }

    public int getEndOffset() {
	return _endOffset.intValue();
    }

    public String getTagsCSV() {
	return _tagsCSV;
    }

    public void setTagsCSV(String tagsCSV) {
	_tagsCSV = tagsCSV;
    }

    public static String getTagsFieldName() {
	return "_tagsCSV";
    }

    public static Tags getDummyObject(String itemID) {
	Tags tags = new Tags();
	tags.setItemID(itemID);
	return tags;
    }

    public String unique() {
	return _itemID + ":" + _startOffset.toString() + ":" +  _endOffset.toString();
    }

    public String uniqueNull() {
	return ":-1:-1";
    }

    public Tags cloneObject() {
	return getDummyObject("");
    }

    public void truncateFields(){

    }
}
