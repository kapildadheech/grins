package org.gramvaani.radio.medialib;

public abstract class SearchFilter {
    public static final String CREATION_TIME_FILTER = "CREATION_TIME_FILTER";
    public static final String CATEGORY_FILTER 	= "CATEGORY_FILTER";
    public static final String CREATOR_FILTER 	= "CREATOR_FILTER";
    public static final String LANGUAGE_FILTER 	= "LANGUAGE_FILTER";
    public static final String LENGTH_FILTER 	= "LENGTH_FILTER";
    public static final String LICENSE_FILTER 	= "LICENSE_FILTER";
    public static final String NEWITEM_FILTER 	= "NEWITEM_FILTER";
    public static final String PLAYOUTHISTORY_FILTER = "PLAYOUTHISTORY_FILTER";
    public static final String TAG_FILTER 	= "TAG_FILTER";
    public static final String TYPE_FILTER 	= "TYPE_FILTER";
    public static final String TEXT_FILTER 	= "TEXT_FILTER";

    boolean filterActive = false;
    public void setActive(boolean active) {
	filterActive = active;
    }
    public boolean getActive() {
	return filterActive;
    }

    public abstract String getFilterName();

    public abstract String getFilterLabel();

    public abstract String getFilterQueryClause();
    public abstract String getFilterWhereClause();
    public abstract String[] getFilterJoinTables();

    public abstract RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs);

    public void activateAnchor(String anchor, String anchorText) {
	// empty method
    }

    public abstract String getSelectedText(String anchorID);
}


