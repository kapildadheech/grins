package org.gramvaani.radio.medialib;

import java.util.*;

public class PollOption extends Metadata {

    Integer _pollOptionID;
    Integer _pollID;
    String _pollOptionName;
    String _pollOptionCode;

    public String dump() {
	return "PollOption: " + _pollOptionID + ":" + _pollID + ":" + _pollOptionName + ":" + _pollOptionCode;
    }

    public PollOption(Integer pollID, String pollOptionName, String code) {
	_pollOptionID = new Integer(-1);
	_pollID = pollID;
	_pollOptionName = pollOptionName;
	_pollOptionCode = code;
    }

    public PollOption(int pollOptionID) {
	this();
	_pollOptionID = pollOptionID;
    }

    public PollOption() {
	_pollOptionID = -1;
	_pollID = -1;
	_pollOptionName = "";
	_pollOptionCode = "";
    }

    public int getID() {
	return _pollOptionID;
    }

    public int getPollID() {
	return _pollID;
    }

    public void setPollID(Integer pollID) {
	_pollID = pollID;
    }
    
    public String getPollIDFieldName() {
	return "_pollID";
    }

    public String getPollOptionName() {
	return _pollOptionName;
    }

    public void setPollOptionName(String pollOptionName) {
	_pollOptionName = pollOptionName;
    }

    public static String getPollOptionNameFieldName() {
	return "_pollOptionName";
    }

    public String getPollOptionCode() {
	return _pollOptionCode;
    }

    public void setPollOptionCode(String code) {
	_pollOptionCode = code;
    }

    public static String getPollOptionCodeFieldName() {
	return "_pollOptionCode";
    }

    public static PollOption getDummyObject(Integer pollOptionID) {
	PollOption pollOption = new PollOption(pollOptionID);
	return pollOption;
    }

    public String primaryKey() {
	return "_pollOptionID";
    }

    public String unique() {
	return _pollOptionID.toString();
    }

    public String uniqueNull() {
	return "-1";
    }

    public PollOption cloneObject() {
	return getDummyObject(new Integer(-1));
    }

    public void truncateFields(){

    }
}
