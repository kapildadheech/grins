package org.gramvaani.radio.medialib;

import org.gramvaani.radio.app.providers.MetadataProvider;
import org.gramvaani.radio.app.providers.CacheProvider;

import java.util.*;

public class CreatorFilter extends SearchFilter {
    String selectedAffiliation;
    String selectedRole;
    String selectedName;
    String selectedLocation;

    public static final String AFFILIATION_ANCHOR = "AFFILIATION_ANCHOR"; boolean affiliationActive = false;
    public static final String ROLE_ANCHOR = "ROLE_ANCHOR"; boolean roleActive = false;
    public static final String NAME_ANCHOR = "NAME_ANCHOR"; boolean nameActive = false;
    public static final String LOCATION_ANCHOR = "LOCATION_ANCHOR"; boolean locationActive = false;

    public CreatorFilter() {
	this.selectedAffiliation = "";
	this.selectedRole = "";
	this.selectedName = "";
	this.selectedLocation = "";
    }

    public String getFilterName() {
	return SearchFilter.CREATOR_FILTER;
    }

    public String getFilterLabel() {
	return "Contacts";
    }

    public String getFilterQueryClause() {
	StringBuilder str = new StringBuilder();
	if (!selectedAffiliation.equals("") && affiliationActive) {
	    str.append(AFFILIATION_ANCHOR); str.append(":\""); str.append(selectedAffiliation); str.append("\" AND ");
	}
	if (!selectedRole.equals("") && roleActive) {
	    str.append(ROLE_ANCHOR); str.append(":\""); str.append(selectedRole); str.append("\" AND ");
	}
	if (!selectedName.equals("") && nameActive) {
	    str.append(NAME_ANCHOR); str.append(":\""); str.append(selectedName); str.append("\" AND ");
	}
	if (!selectedLocation.equals("") && locationActive) {
	    str.append(LOCATION_ANCHOR); str.append(":\""); str.append(selectedLocation); str.append("\" AND ");
	}

	//	if (!selectedText.equals("")) {
	//	    if (nameActive && locationActive)
	//		str.append("(");
	//	    if (nameActive) {
	//		str.append(NAME_ANCHOR); str.append(":\""); str.append(selectedText); str.append("\" OR ");
	//	    }
	//	    if (locationActive) {
	//		str.append(LOCATION_ANCHOR); str.append(":\""); str.append(selectedText); str.append("\" OR ");
	//	    }
	//	    if (str.length() > 0)
	//		str = new StringBuilder(str.substring(0, str.length() - 3));
	//	    if (nameActive && locationActive)
	//		str.append(")");
	//	    if (nameActive || locationActive)
	//		str.append(" AND ");
	//	}
	
	if (str.length() > 0)
	    return str.substring(0, str.length() - 4);
	else
	    return "";
    }

    public String getFilterWhereClause() {
	return "";
    }

    public String[] getFilterJoinTables() {
	return new String[]{};
    }

    public String getSelectedText(String anchorID) {
	if (anchorID.equals(AFFILIATION_ANCHOR))
	    return selectedAffiliation;
	else if (anchorID.equals(ROLE_ANCHOR))
	    return selectedRole;
	else if (anchorID.equals(NAME_ANCHOR))
	    return selectedName;
	else if (anchorID.equals(LOCATION_ANCHOR))
	    return selectedLocation;
	else
	    return "";
    }

    public void setSelectedAffiliation(String affiliation) {
	this.selectedAffiliation = affiliation;
    }

    public void setSelectedRole(String role) {
	this.selectedRole = role;
    }

    public void setSelectedText(String text) {
	//	this.selectedText = text;
    }

    public void activateAnchor(String anchor, String anchorText) {
	if (anchor.equals(AFFILIATION_ANCHOR)) {
	    affiliationActive = true;
	    selectedAffiliation = anchorText;
	} else if (anchor.equals(ROLE_ANCHOR)) {
	    roleActive = true;
	    selectedRole = anchorText;
	} else if (anchor.equals(NAME_ANCHOR)) {
	    nameActive = true;
	    selectedName = anchorText;
	} else if (anchor.equals(LOCATION_ANCHOR)) {
	    locationActive = true;
	    selectedLocation = anchorText;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }
    
    /*
    public static String[] getAffiliations(CacheProvider cacheProvider) {
	Object[] affiliations = cacheProvider.uniqueFromAnchors(Creator.getDummyObject(""), Creator.getAffiliationFieldName());
	return getAffiliations(affiliations);
    }
    */

    public static String[] getAffiliations(MediaLib medialib) {
	Object[] affiliations = medialib.unique(Creator.getDummyObject(""), Creator.getAffiliationFieldName());
	return getAffiliations(affiliations);
    }

    public static String[] getAffiliations(MetadataProvider metadataProvider) {
	Object[] affiliations = metadataProvider.unique(Creator.getDummyObject(""), Creator.getAffiliationFieldName());
	return getAffiliations(affiliations);
    }

    private static String[] getAffiliations(Object[] affiliations) {
	HashSet<String> hash = new HashSet<String>();
	for (Object affiliation: affiliations)
	    hash.add((String)affiliation);

	HashSet<String> staticAffiliations = Creator.getAffiliations();

	hash.addAll(staticAffiliations);

	return hash.toArray(new String[hash.size()]);
    }
    
    /*
    public static String[] getRoles(CacheProvider cacheProvider) {
	Object[] roles = cacheProvider.uniqueFromAnchors(Entity.getDummyObject(new Integer(-1)), Entity.getEntityTypeFieldName());
	return getRoles(roles);
    }
    */

    public static String[] getRoles(MediaLib medialib) {
	Object[] roles = medialib.unique(Entity.getDummyObject(new Integer(-1)), Entity.getEntityTypeFieldName());
	return getRoles(roles);
    }


    public static String[] getRoles(MetadataProvider metadataProvider) {
	Object[] roles = metadataProvider.unique(Entity.getDummyObject(new Integer(-1)), Entity.getEntityTypeFieldName());
	return getRoles(roles);
    }

    private static String[] getRoles(Object[] roles) {
	Hashtable<String, String> hash = new Hashtable<String, String>();
	for (Object role: roles)
	    hash.put((String)role, (String)role);
	hash.put(Entity.VOLUNTEER, Entity.VOLUNTEER);
	hash.put(Entity.STAFF, Entity.STAFF);
	hash.put(Entity.PERSON, Entity.PERSON);
	hash.put(Entity.RADIO_STATION, Entity.RADIO_STATION);
	hash.put(Entity.NGO, Entity.NGO);
	hash.put(Entity.COMPANY, Entity.COMPANY);

	Collection<String> values = hash.values();
	String[] arr = new String[hash.size()];
	int i = 0;
	for (String value: values)
	    arr[i++] = value;
	return arr;
    }
    
    /*
    public static String[] getNames(CacheProvider cacheProvider) {
	Object[] names = cacheProvider.uniqueFromAnchors(Entity.getDummyObject(new Integer(-1)), Entity.getNameFieldName());
	return getNames(names);
    }
    */

    public static String[] getNames(MetadataProvider metadataProvider) {
	Object[] names = metadataProvider.unique(Entity.getDummyObject(new Integer(-1)), Entity.getNameFieldName());
	return getNames(names);
    }

    public static String[] getNames(MediaLib medialib) {
	Object[] names = medialib.unique(Entity.getDummyObject(new Integer(-1)), Entity.getNameFieldName());
	return getNames(names);
    }

    private static String[] getNames(Object[] names) {
	ArrayList<String> arr = new ArrayList<String>();
	for (Object name: names)
	    arr.add((String)name);
	return arr.toArray(new String[]{});
    }

    /*
    public static String[] getLocations(CacheProvider cacheProvider) {
	Object[] locations = cacheProvider.uniqueFromAnchors(Entity.getDummyObject(new Integer(-1)), Entity.getLocationFieldName());
	return getLocations(locations);
    }
    */

    public static String[] getLocations(MediaLib medialib) {
	Object[] locations = medialib.unique(Entity.getDummyObject(new Integer(-1)), Entity.getLocationFieldName());
	return getLocations(locations);
    }

    public static String[] getLocations(MetadataProvider metadataProvider) {
	Object[] locations = metadataProvider.unique(Entity.getDummyObject(new Integer(-1)), Entity.getLocationFieldName());
	return getLocations(locations);
    }

    private static String[] getLocations(Object[] locations) {
	ArrayList<String> arr = new ArrayList<String>();
	for (Object location: locations)
	    arr.add((String)location);
	return arr.toArray(new String[]{});
    }

}

/*
    public static final String affiliationKey = Metadata.getClassName(Creator.class) + "." + Creator.getAffiliationFieldName();
    public static final String roleKey        = Metadata.getClassName(Entity.class) + "." + Entity.getEntityTypeFieldName();
    public static final String textNameKey    = Metadata.getClassName(Entity.class) + "." + Entity.getNameFieldName();
    public static final String textLocationKey = Metadata.getClassName(Entity.class) + "." + Entity.getLocationFieldName();

    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(Creator.class), Metadata.getClassName(Entity.class)};
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if (!selectedAffiliation.equals("") && affiliationActive) {
	    str.append(affiliationKey); str.append(" = '"); str.append(selectedAffiliation); str.append("' and ");
	}
	if (!selectedRole.equals("") && roleActive) {
	    str.append(roleKey); str.append(" = '"); str.append(selectedRole); str.append("' and ");
	}
	if (!selectedText.equals("")) {
	    if (nameActive && locationActive)
		str.append("(");
	    if (nameActive) {
		str.append("instr("); str.append(textNameKey); str.append(" , '"); str.append(selectedText); str.append("') != 0 or ");
	    }
	    if (locationActive) {
		str.append("instr("); str.append(textLocationKey); str.append(", '"); str.append(selectedText); str.append("') != 0");
	    }
	    if (nameActive && locationActive)
		str.append(")");
	    if (nameActive || locationActive)
		str.append(" and ");
	}
	
	if (str.length() > 0)
	    return str.substring(0, str.length() - 3);
	else
	    return "";
    }
*/


