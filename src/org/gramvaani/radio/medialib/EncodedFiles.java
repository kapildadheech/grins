package org.gramvaani.radio.medialib;

import java.util.*;

public class EncodedFiles extends Metadata {
    protected String _oldItemID;
    protected String _newItemID;

    public String dump() {
	return "EncodedFiles: " + _oldItemID + ":" + _newItemID;
    }

    public EncodedFiles(String oldItemID, String newItemID) {
	this._oldItemID = oldItemID;
	this._newItemID = newItemID;
    }

    public EncodedFiles() {
	_oldItemID = "";
	_newItemID = "";
    }

    public String getOldItemID() {
	return _oldItemID;
    }

    public String getNewItemID() {
	return _newItemID;
    }

    public void setNewItemID(String itemID) {
	_newItemID = itemID;
    }

    public void setOldItemID(String itemID) {
	_oldItemID = itemID;
    }

    public static EncodedFiles getDummyObject() {
	EncodedFiles encodedFile = new EncodedFiles();
	return encodedFile;
    }

    public String unique() {
	return "_oldItemID";
    }

    public EncodedFiles cloneObject() {
	return getDummyObject();
    }

    public void truncateFields(){

    }
}
