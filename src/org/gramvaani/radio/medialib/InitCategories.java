package org.gramvaani.radio.medialib;

import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.io.*;
import java.util.*;

import gnu.getopt.Getopt;

public class InitCategories {
    protected StationConfiguration stationConfiguration;
    protected LogUtilities logger;
    protected Hashtable<String, Category> categoryNodes;
    protected Hashtable<String, String> childParent;
    protected Hashtable<String, ArrayList<String>> parentChildren;
    protected MediaLib medialib;
    protected UpdateMetadataHandler updateHandler;

    public InitCategories(StationConfiguration stationConfiguration, UpdateMetadataHandler updateMetadata) {
	this.stationConfiguration = stationConfiguration;
	this.logger = new LogUtilities("InitCategories");
	this.categoryNodes = new Hashtable<String, Category>();
	this.childParent = new Hashtable<String, String>();
	this.parentChildren = new Hashtable<String, ArrayList<String>>();
	this.medialib = MediaLib.getMediaLib(stationConfiguration, null);
	if(this.medialib == null) {
	    logger.fatal("unable to create connection to media lib");
	    System.exit(-1);
	}
	if(updateMetadata == null)
	    this.updateHandler = new UpdateMetadataHandler(stationConfiguration, null, null);
	else
	    this.updateHandler = updateMetadata;

	updateHandler.initialize();
    }

    public void start() {
	logger.info("Start: starting to read categories file");
	String categoriesFile = stationConfiguration.getStringParam(StationConfiguration.CATEGORIES_FILE);
	try {
	    BufferedReader in = new BufferedReader(new FileReader(categoriesFile));
	    String readLine;

	    while((readLine = in.readLine()) != null) {
		if(readLine.equals("") || readLine.trim().startsWith("#"))
		    continue;
		String[] categoryParams = StringUtilities.getArrayFromCSV(readLine);
		String nodeName = categoryParams[0].toLowerCase();
		String nodeLabel = categoryParams[1].toLowerCase();
		String parentName = categoryParams[2].toLowerCase();
		logger.debug("Start: read line:" + nodeName + ":" + nodeLabel + ":" + parentName);
		int parentID = Category.ROOT_PARENT_ID;
		if(parentName.equals("null") || childParent.get(parentName) != null) {
		    childParent.put(nodeName, parentName);
		    logger.debug("Start: added to child parent " + nodeName + ":" + parentName);

		    if(parentChildren.get(nodeName) == null)
			parentChildren.put(nodeName, new ArrayList<String>());

		    if(!parentName.equals("null"))
			parentChildren.get(parentName).add(nodeName);

		    if(!parentName.equals("null") && categoryNodes.get(parentName) == null) {
			logger.debug("Start: putting parent in categoryNodes");
			Category parentCategory = Category.getDummyObjectByNonPrimaryKey(parentName);
			parentCategory = medialib.getSingle(parentCategory);
			if(parentCategory != null) {
			    parentID = parentCategory.getNodeID();
			    categoryNodes.put(parentName, parentCategory);
			    logger.debug("Start: putting parent in categoryNodes: " + parentID + ":" + parentName);
			} else {
			    logger.error("Start: error in categories file: unknown parent " + parentName + " for child " + nodeName);
			    continue;
			}
		    } else if(!parentName.equals("null")) {
			parentID = categoryNodes.get(parentName).getNodeID();
			logger.debug("Start: got parent from categoryNodes " + parentID + ":" + parentName);
		    }

		    logger.debug("Start: inserting node in medialib");
		    Category nodeCategory = new Category(nodeName, nodeLabel, parentID);
		    if(medialib.insert(nodeCategory) > -1) {
			logger.debug("Start: successfully inserted node " + nodeName);
			nodeCategory = medialib.getSingle(nodeCategory);
			if(nodeCategory != null) {
			    categoryNodes.put(nodeName, nodeCategory);
			    logger.debug("Start: successfully retrieved inserted node " + nodeCategory.getNodeID() + ":" + nodeCategory.getNodeName());
			}
		    } else {
			logger.error("Start: Unable to insert node " + nodeName);
		    }
		}		    
	    }
	} catch(Exception e) {
	    logger.error("Start: Unable to read categories file", e);
	    return;
	}

	Enumeration<String> e = categoryNodes.keys();
	while(e.hasMoreElements()) {
	    String nodeName = e.nextElement();
	    Category node = categoryNodes.get(nodeName);
	    StringBuilder str = new StringBuilder(node.getLabel());
	    
	    String[] children = parentChildren.get(nodeName).toArray(new String[]{});
	    ArrayList<String> childrenArr;
	    while(children.length > 0) {
		childrenArr = new ArrayList<String>();
		for(String childName: children) {
		    Category childNode = categoryNodes.get(childName);
		    if(childNode != null) {
			str.append(","); str.append(childNode.getLabel());
			childrenArr.addAll(parentChildren.get(childNode.getNodeName()));
		    }
		}
		children = childrenArr.toArray(new String[]{});
	    }
	    
	    String parentNodeSearch = nodeName;
	    while(!childParent.get(parentNodeSearch).equals("null"))
		parentNodeSearch = childParent.get(parentNodeSearch);

	    logger.info("Start: adding category anchor " + nodeName + ":" + str.toString());
	    if(updateHandler.isActive()) {
		updateHandler.addCategoryAnchorIndex(CategoryFilter.CATEGORY_ANCHOR + "_" + nodeName,
						     SearchFilter.CATEGORY_FILTER + "_" + parentNodeSearch,
						     str.toString());
	    } else {
		logger.error("Start: updateHandler is not active. Index will not be updated.");
	    }
	}

	logger.info("Start: Leaving");
    }

    public static void main(String[] args) {
	Getopt g = new Getopt("InitCategories", args, "c:");
	int c;
	String configFilePath = "./automation.conf";
	while((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    }
	}
	
	StationConfiguration stationConfiguration = new StationConfiguration();
	stationConfiguration.readConfig(configFilePath);

	if(!stationConfiguration.isValidInstance()) {
	    System.err.println("InitCategories: Invalid instance. Cannot continue.");
	    System.exit(-4);
	}

	InitCategories initCategories = new InitCategories(stationConfiguration, null);
	initCategories.start();
	System.exit(0);
    }
    
}