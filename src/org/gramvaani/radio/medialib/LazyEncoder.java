package org.gramvaani.radio.medialib;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.utilities.*;

import java.util.*;
import java.io.File;

import org.gstreamer.*;
import org.gstreamer.elements.*;
import org.gstreamer.message.*;

public class LazyEncoder implements Runnable, EncodingPipelineListener, TaggingPipelineListener {
    protected MediaLib medialib;
    protected StationConfiguration stationConfiguration;
    protected final LogUtilities logger;

    private ArrayList<String> encodePending;
    protected Thread t = null;
    protected EncodingPipeline encodingPipeline = null;
    protected TaggingPipeline taggingPipeline   = null;
    protected Hashtable<String, ArrayList<TagList>> tagLists;

    static final String NOTAGSUFFIX = "_NO_TAG";

    public LazyEncoder(MediaLib medialib, StationConfiguration stationConfiguration, String logPrefix) {
	this.medialib = medialib;
	this.stationConfiguration = stationConfiguration;
	encodePending = new ArrayList<String>();
	String loggerName = logPrefix + ":LazyEncoder";
	logger = new LogUtilities (loggerName);
	int sampleRate = stationConfiguration.getIntParam(StationConfiguration.ENCODE_SAMPLE_RATE);

	boolean isLinux;
	if (stationConfiguration.getOperatingSystem().equals(StationConfiguration.LINUX))
	    isLinux = true;
	else
	    isLinux = false;
	encodingPipeline = new EncodingPipeline(sampleRate, this, loggerName, isLinux);
	taggingPipeline  = new TaggingPipeline(this, isLinux);

	tagLists = new Hashtable<String, ArrayList<TagList>>();

	logger.debug("LazyEncoder: Initialized with sampleRate="+sampleRate);
    }

    public void run() {
	while (true) {
	    if (encodePending.size() == 0 && !encodingPipeline.isStarted()) {
		logger.debug("Run: encodePending size is 0. Fetching pending encoding jobs from Library Service");
		fetchPending(encodePending, LibService.DONE_ARCHIVING_PLAYOUT);
		fetchPending(encodePending, LibService.DONE_ARCHIVING_RECORD);
		fetchPending(encodePending, LibService.DONE_UPLOAD);
		fetchPending(encodePending, LibService.DONE_TELEPHONY);
	    }

	    if (encodePending.size() == 0) {
		logger.debug("Run: No pending encoding jobs at Library Service either. Waiting.");
		synchronized(this) {
		    try {
			wait();
		    } catch(Exception e) { continue; }
		}
		continue;
	    }
	    

	    encodeNext();

	    synchronized(this) { 
		try { 
		    wait(); 
		} catch(Exception ex) { } 
	    }
	}
    }

    protected String[] getIOFileNames(){
	String inFileName;
	if (encodePending.size() > 0){
	    synchronized(encodePending){
		inFileName = encodePending.remove(0);	    
	    }
	} else {
	    return null;
	}
	
	//String outFileName = inFileName.split("[\\.][^\\.]*$")[0] + "." + stationConfiguration.getStringParam(StationConfiguration.LIB_CODEC);
	
	String outFileName = medialib.newBulkItemFilename() + "." + stationConfiguration.getStringParam(StationConfiguration.LIB_CODEC);
	String separator = System.getProperty("file.separator");
	inFileName = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.UPLOAD)
	    + separator + inFileName;
	outFileName = stationConfiguration.getStringParam(StationConfiguration.LIB_ENC_DIR)
	    + separator + outFileName;
	
	return new String[] {inFileName, outFileName};
    }
    
    public synchronized void startEncoding() {
	if (t == null) {
	    t = new Thread(this, "LazyEncoderThread");
	    t.start();
	}
	notifyAll();
	
	if (encodingPipeline.isPaused())
	    if (encodingPipeline.resume())
		logger.info("StartEncoding: Resumed encoding.");
	    else
		logger.error("StartEncoding: Could not resume encoding.");
    }

    public synchronized void pauseEncoding() {
	if (encodingPipeline.pause())
	    logger.info("PauseEncoding: Paused encoding.");
	else
	    logger.warn("PauseEncoding: Could not pause encoding.");
    }

    public static String noTagFileName(String fileName){
	if (!fileName.endsWith(NOTAGSUFFIX))
	    return fileName+NOTAGSUFFIX;
	else
	    return fileName;
    }

    public static String taggedFileName(String fileName){
	if (fileName.endsWith(NOTAGSUFFIX)){
	    return fileName.substring(0, fileName.length() - NOTAGSUFFIX.length());
	} else {
	    //logger.error("TaggedFileName: fileName without NOTAGSUFFIX: "+fileName);
	    return fileName;
	}
    }

    public void setMediaLib(MediaLib medialib){
	this.medialib = medialib;
    }

    public synchronized void encodingDone(EncodingPipeline encodingPipeline, String inFileName, String outFileName){
	
	if (taggingPipeline != null){
	    String noTagFileName = outFileName;
	    taggingPipeline.tag(noTagFileName, inFileName, tagLists.get(inFileName));
	}
	//encodeNext();
    }

    private void encodeNext(){
	if (encodingPipeline.isStarted())
	    return;
	String[] fileNames = getIOFileNames();
	if (fileNames == null)
	    return;
	
	File origFile = new File(fileNames[0]);
	if (origFile.exists()) {
	    if (encodingPipeline.start(fileNames[0], noTagFileName(fileNames[1])))
		logger.info("EncodeNext: Started Encoding "+fileNames[0] +" to "+fileNames[1]);
	    else
		logger.error("EncodeNext: Could not start Encoding "+fileNames[0] +" to "+fileNames[1]);
	} else {
	    logger.warn("EncodeNext: Original file " + fileNames[0] + " not found. The file may have been deleted.");
	    //encodeNext();
	    encodingFailed (encodingPipeline, fileNames[0], fileNames[1]);
	}
    }
    
    protected void updateFileStatus(String inFileName, String outFileName){
	String dirName  = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.UPLOAD)+"/";
	String encDirName = stationConfiguration.getStringParam(StationConfiguration.LIB_ENC_DIR) + "/";
	String fileName = inFileName.split(dirName)[1];
	String newFileName = outFileName.split(encDirName)[1];
	boolean dbUpdateSuccess = true;

	RadioProgramMetadata encodedProgramQuery = RadioProgramMetadata.getDummyObject("");
	encodedProgramQuery.setFilename(fileName);
	encodedProgramQuery.setUploadStoreAttempts(FileStoreManager.UPLOAD_DONE);
	RadioProgramMetadata encodedProgram = medialib.getSingle(encodedProgramQuery);

	//Original program has been removed from the system while we were encoding it
	if (encodedProgram == null) {
	    File oldFile = new File(fileName);
	    if (oldFile.delete()) {
		logger.info("UpdateFileStatus: Deleted file " + fileName + " as it has been removed from the system.");
	    } else {
		logger.warn("UpdateFileStatus: Could not remove file " + fileName + " from disk. The file has been removed from the system.");
	    }

	    File newFile = new File(newFileName);
	    if (newFile.delete()) {
		logger.info("UpdateFileStatus: Deleted encoded file " + newFileName + " as its corresponding original file " + fileName +" has been removed from the system.");
	    } else {
		logger.error("UpdateFileStatus: Could not remove file " + newFileName + " from disk. The corresponding original file " + fileName + " has been removed from the system.");
	    }

	    return;
	}

	RadioProgramMetadata update = RadioProgramMetadata.getDummyObject("");
	update.setState(LibService.DONE_ENCODING);
	
	RadioProgramMetadata query = new RadioProgramMetadata(encodedProgram.getItemID());
	if (medialib.update(query, update) >= 0) {
	    logger.debug("UpdateFileStatus: Finished Encoding "+fileName);
	} else {
	    logger.error("UpdateFileStatus: Database error " + fileName);
	}
	
	EncodedFiles encodedFile = new EncodedFiles(fileName, newFileName);
	if (medialib.insert(encodedFile) >= 0) {
	    logger.debug("UpdateFileStatus: Updated database with encoded filename");
	} else {
	    logger.error("UpdateFileStatus: Database error " + fileName);
	}
	
    }
    
    public synchronized void encodingFailed(EncodingPipeline encodingPipeline, String inFileName, String outFileName){
	logger.error("EncodingFailed: Failed to encode file: "+inFileName+" to "+outFileName);
	String dirName  = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.UPLOAD) + "/";
	String encDirName = stationConfiguration.getStringParam(StationConfiguration.LIB_ENC_DIR) + "/";
	String fileName = inFileName.split(dirName)[1];
	String newFileName = outFileName.split(encDirName)[1];
	
	RadioProgramMetadata encodedProgramQuery = RadioProgramMetadata.getDummyObject("");
	encodedProgramQuery.setFilename(fileName);
	encodedProgramQuery.setUploadStoreAttempts(FileStoreManager.UPLOAD_DONE);

	RadioProgramMetadata encodedProgram = medialib.getSingle(encodedProgramQuery);

	if (encodedProgram != null) {
	    RadioProgramMetadata update = RadioProgramMetadata.getDummyObject("");
	    update.setState(LibService.ENCODING_FAILED);
	    
	    RadioProgramMetadata query = new RadioProgramMetadata(encodedProgram.getItemID());
	    if (medialib.update(query, update) >= 0) {
		logger.debug("EncodingFailed: Updated status of " + fileName);
	    } else {
		logger.error("EncodingFailed: Database error " + fileName);
	    }
	}
	
	encodeNext();
    }

    private void fetchPending(ArrayList<String> encodePending, int state) {
	RadioProgramMetadata radioProgram = RadioProgramMetadata.getDummyObject("");
	radioProgram.setState(state);
	radioProgram.setUploadStoreAttempts(FileStoreManager.UPLOAD_DONE);
	Metadata[] pendingPrograms = medialib.get(radioProgram);
	if (pendingPrograms == null){
	    logger.debug("FetchPending: No pending programs found in state "+ state);
	    return;
	}
	int count = 0;
	synchronized(encodePending){
	    for (Metadata pendingProgram: pendingPrograms){
		count++;
		encodePending.add(((RadioProgramMetadata)pendingProgram).getFilename());
	    }    
	}
	logger.debug("FetchPending: "+count+" programs found pending for encoding in state "+state);
    }

    public synchronized void handleTag (String fileName, TagList tagList){
	logger.debug("HandleTag: Adding to file: "+fileName+" : "+tagList);

	//Currently adding only replaygain tags. 

	if (!tagList.getTagNames().get(0).equals("replaygain-track-peak"))
	    return;
	
	if (tagLists.get(fileName) == null)
	    tagLists.put(fileName, new ArrayList<TagList>());

	tagLists.get(fileName).add(tagList);
    }

    protected String concatList(List<Object> list){
	StringBuilder str = new StringBuilder();
	for (Object o: list)
	    str.append(o.toString()+", ");
	if (str.length() >= 2)
	    str.delete(str.length()-2, str.length());
	return str.toString();
    }

    public void taggingDone(String fileName, String newFileName){
	logger.info("TaggingDone: Tagging done: "+fileName+":"+newFileName);
	try{
	    File noTagFile = new File(noTagFileName(newFileName));
	    if (noTagFile.delete())
		logger.debug("TaggingDone: Removed untagged file.");
	    else
		logger.error("TaggingDone: Unable to remove untagged file.");
	} catch (Exception e){
	    logger.error("TaggingDone: Unable to remove untagged intermediate file.", e);
	}
	
	updateFileStatus(fileName, newFileName);
	
	encodeNext();
    }

    public void taggingFailed(String fileName, String newFileName){
	boolean update = true;
	tagLists.remove(fileName);

	logger.error("TaggingFailed: Tagging failed.");
	try{
	    File noTagFile = new File(noTagFileName(newFileName));
	    File taggedFile = new File(newFileName);
	    if (taggedFile.exists())
		taggedFile.delete();
	    
	    File origFile = new File(fileName);
	    if (!origFile.exists()) {

		if (noTagFile.delete()) {
		    logger.info("TaggingFailed: Removed file: " + noTagFile.getName() + " from disk.");
		} else {
		    logger.error("TaggingFailed: Failed to Remove file: " + noTagFile.getName() + " from disk.");
		}
		update = false;

	    } else {
		
		if (noTagFile.renameTo(taggedFile)){
		    logger.debug("Encoding Done: Removed NOTAGSUFFIX by renaming.");
		} else {
		    logger.error("Encoding Done: Unable to remove NOTAGSUFFIX by renaming.");
		    newFileName = noTagFileName(newFileName);
		}
	    }
	} catch (Exception e){
	    logger.error("Encoding Done: Unable to remove NOTAGSUFFIX by renaming.", e);
	    newFileName = noTagFileName(newFileName);
	}

	if (update)
	    updateFileStatus(fileName, newFileName);
	
	encodeNext();
    }
    

}


class EncodingPipeline implements Runnable, DecodeBin.NEW_DECODED_PAD {
    Thread thread;
    int sampleRate;
    Pipeline pipe;
    Element fileSrc, audioConvert, rgAnalysis, resample, lame, id3v2mux, fileSink;
    Caps caps;
    DecodeBin decodeBin;
    Bus bus;

    boolean isStarted = false, isPaused = false;
    String srcFileName, sinkFileName;

    final LogUtilities logger;
    final EncodingPipelineListener listener;

    public EncodingPipeline (final int sampleRate, final EncodingPipelineListener listener, String logPrefix, boolean isLinux){
	this.sampleRate = sampleRate;
	this.listener 	= listener;

	logger = new LogUtilities(logPrefix+":EncodingPipeline");

	for (int i =0; i < 3; i++){
	    try{
		String args[] = Gst.init("EncodingPipelineInstance", new String[] {});
		pipe = new Pipeline("LazyEncoderPipeline");
		break;
	    } catch (Exception e){
		logger.error("Encoding pipeline: Failed to initialize.", e);
		if (i==2){
		    logger.error("Encoding pipeline: Failed to initialize thrice.", e);
		    //System.exit(-1);
		}
	    }
	}

	try{
	    fileSrc 	= ElementFactory.make("filesrc", "filesrc");
	    decodeBin 	= new DecodeBin ("decodebin");
	    decodeBin.connect(this);
	    
	    audioConvert= ElementFactory.make("audioconvert", "audioconvert");

	    //Disabling rganalysis for windows
	    if (isLinux)
		rgAnalysis = ElementFactory.make("rganalysis", "rganalysis");
	    else
		rgAnalysis = ElementFactory.make("audioconvert", "audioconvert1");
   
	    resample 	= ElementFactory.make("audioresample", "resample");
	    lame 	= ElementFactory.make("lame", "lame");

	    if (isLinux)
		id3v2mux = ElementFactory.make("id3v2mux", "id3v2mux");
	    else
		id3v2mux = ElementFactory.make("id3mux", "id3mux");

	    fileSink 	= ElementFactory.make("filesink", "filesink");
	    
	    pipe.addMany(fileSrc, decodeBin, 
			 audioConvert, rgAnalysis, resample, lame, id3v2mux, fileSink);
	    
	    pipe.linkMany(fileSrc, decodeBin);
	    pipe.linkMany(audioConvert, rgAnalysis, resample);
	    caps = Caps.fromString("audio/x-raw-int, rate="+Integer.toString(sampleRate));
	    pipe.linkPadsFiltered(resample, "src", lame, "sink", caps);
	    pipe.linkMany(lame, id3v2mux, fileSink);
	    
	    bus = pipe.getBus();

	    final EncodingPipeline encodingPipeline = this;
	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int errorCode, String error){
			logger.error(error);
			pipe.setState(State.NULL);
			pipe.getState();
			isStarted = false;
			listener.encodingFailed(encodingPipeline, srcFileName, sinkFileName);
		    }
		});
	    bus.connect(new Bus.TAG(){
		    public void tagsFound (GstObject source, TagList tagList){
			logger.debug("Found tags:"+tagList);
			listener.handleTag(srcFileName, tagList);
		    }
		});
	    bus.connect(new Bus.EOS(){
		    public void endOfStream (GstObject source){
			logger.info("EndOfStream: Finished encoding: "+srcFileName);
			encodingPipeline.stop();
			isStarted = false;
			listener.encodingDone(encodingPipeline, srcFileName, sinkFileName);
		    }
		});
	    
	    pipe.setState(State.NULL);

	} catch (Exception e){
	    logger.error("EncodingPipeline: Unable to initialize.", e);
	    return;
	}

	logger.debug("EncodingPipeline: Initialized");
	thread = new Thread(this, "LazyEncoderGstreamerThread");
	thread.start();
	
    }

    public void newDecodedPad (Element element, Pad pad, boolean last){
	try{
	    pipe.linkMany(decodeBin, audioConvert);
	}catch (Exception e){
	    logger.error("NewDecodedPad: Exception encountered during linking decoded pad.", e);
	}
    }
    
    public void run(){
	logger.info("Run: About to enter Gstreamer main loop.");
	try{
	    Gst.main();
	}catch(Exception e){
	    logger.error("Run: Gstreamer Error. Exited from Gst.main().", e);
	}
    }

    public synchronized boolean isPaused(){
	return isPaused;
    }
    
    public synchronized boolean isStarted(){
	return isStarted;
    }
    
    public synchronized boolean start(String srcFileName, String sinkFileName){
	logger.debug("Start: srcFileName="+srcFileName+" sinkFileName="+sinkFileName);
	this.srcFileName  = srcFileName;
	this.sinkFileName = sinkFileName;
	try{
	    fileSrc.set("location", srcFileName);
	    fileSink.set("location", sinkFileName);
	    
	    if (pipe.setState(State.PLAYING) == StateChangeReturn.FAILURE){
		logger.error("Start: Gstreamer could not set pipeline to playing state.");
		return false;
	    } else {
		isPaused = false;
		isStarted = true;
		logger.info("Start: Gstreamer set state to playing successful.");
		return true;
	    }
	} catch (Exception e){
	    logger.error("Start: Exception encountered in gstreamer.", e);
	    return false;
	}
	
    }

    public synchronized boolean pause(){
	if (!isStarted){
	    logger.warn("Pause: Pipeline not started, hence not pausing.");
	    return false;
	}

	try{
	    if (pipe.setState(State.PAUSED) == StateChangeReturn.FAILURE){
		logger.error("Pause: Gstreamer could not set pipeline to paused state.");
		return false;
	    } else {
		isPaused = true;
		logger.info("Pause: Gstreamer set state to paused successful.");
		return true;
	    }
	} catch (Exception e){
	    logger.error("Pause: Exception encountered in gstreamer.", e);
	    return false;
	}
    }

    public synchronized boolean resume(){
	if (!isStarted){
	    logger.warn("Resume: Pipeline not started, hence not resuming.");
	    return false;
	}
	
	try{
	    if (pipe.setState(State.PLAYING) == StateChangeReturn.FAILURE){
		logger.error("Resume: Gstreamer could not set pipeline to playing state.");
		return false;
	    } else {
		isPaused = false;
		logger.info("Resume: Gstreamer set state to playing successful.");
		return true;
	    }
	}catch(Exception e){
	    logger.error("Resume: Exception occured in gstreamer.", e);
	    return false;
	}
	
    }

    synchronized boolean stop(){
	if (!isStarted){
	    logger.warn("Stop: Pipeline not started, hence not stopping.");
	    return false;
	}

	try{
	    if (pipe.setState(State.NULL) == StateChangeReturn.FAILURE){
		logger.error("Stop: Gstreamer could not set pipeline to NULL state.");
		return false;
	    } else {
		isPaused = false;
		isStarted = false;
		logger.info("Stop: Gstreamer set state to NULL successful.");
		return true;
	    }
	}catch(Exception e){
	    logger.error("Stop: Exception occured in gstreamer.", e);
	    return false;
	}
    }
}

interface EncodingPipelineListener {
    public void handleTag(String fileName, TagList tagList);
    public void encodingDone(EncodingPipeline encodingPipeline, String inFileName, String outFileName);
    public void encodingFailed(EncodingPipeline encodingPipeline, String inFileName, String outFileName);
}

class TaggingPipeline implements Runnable{
    Pipeline pipe;
    Element fileSrc, taginject, apev2mux, fileSink;
    Bus bus;

    final LogUtilities logger;
    final TaggingPipelineListener listener;
    
    Thread t;
    String inFileName, outFileName;

    boolean isTagging = false;
    boolean tagStatus = false;

    public TaggingPipeline(final TaggingPipelineListener listener, boolean isLinux){
	logger = new LogUtilities("LazyEncoder:TaggingPipeline");
	this.listener = listener;

	for (int i =0; i < 3; i++){
	    try{
		String args[] = Gst.init("TaggingPipelineInstance", new String[] {});
		pipe = new Pipeline("TaggingPipeline");
		break;
	    } catch (Exception e){
		logger.error("Tagging pipeline: Failed to initialize.", e);
		if (i==2){
		    logger.error("Tagging pipeline: Failed to initialize thrice.", e);
		    //System.exit(-1);
		    return;
		}
	    }
	}

	try{
	    
	    fileSrc   = ElementFactory.make("filesrc", "taggingFileSrc");
	    if (isLinux){
		taginject = ElementFactory.make("taginject", "taggingTagInject");
		apev2mux  = ElementFactory.make("apev2mux", "taggingApeV2Mux");
	    } else {
		taginject = ElementFactory.make("identity", "taggingIdentity");
		apev2mux  = ElementFactory.make("id3mux", "taggingID3Mux");
	    }

	    fileSink  = ElementFactory.make("filesink", "taggingFileSink");

	    pipe.addMany(fileSrc, taginject, apev2mux, fileSink);
	    pipe.linkMany(fileSrc, taginject, apev2mux, fileSink);

	    bus = pipe.getBus();
	    
	    bus.connect(new Bus.EOS(){
		    public void endOfStream(GstObject source){
			logger.info("Tagging done: "+inFileName);
			pipe.setState(State.NULL);
			listener.taggingDone(inFileName, outFileName);
		    }
		});
	    
	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int errorCode, String error){
			logger.error(error);
			pipe.setState(State.NULL);
			listener.taggingFailed(inFileName, outFileName);
		    }
		});
	    
	    pipe.setState(State.NULL);
	    
	} catch (Exception e){
	    logger.error("Unable to initialize tagging pipeline elements.", e);
	    return;
	}
	
	t = new Thread(this, "LazyEncoderTaggingThread");
	t.start();
    }
    
    public void run(){
	logger.info("Run: About to enter Gstreamer main loop.");
	try{
	    Gst.main();
	    logger.warn("Run: Exiting gstreamer main loop.");
	}catch(Exception e){
	    logger.error("Run: Gstreamer Error. Exited from Gst.main().", e);
	}
    }
    
    public synchronized void tag(String noTagFileName, String inFileName, ArrayList<TagList> tagLists){
	this.inFileName = inFileName;
	this.outFileName = LazyEncoder.taggedFileName(noTagFileName);
	String tag = tagString(tagLists);
	if (tag != null) {
	    try{
		fileSrc.set("location", noTagFileName);
		fileSink.set("location", outFileName);
		taginject.set("tags", tagString(tagLists)); 
		if (pipe.setState(State.PLAYING) == StateChangeReturn.FAILURE){
		    logger.error("Tag: Could not set state to playing.");
		    listener.taggingFailed(inFileName, outFileName);
		} else {
		    logger.info("Tag: Pipeline set to playing.");
		}
	    } catch(Exception e){
		logger.error("Tag: Unable to tag file: "+inFileName, e);
		listener.taggingFailed(inFileName, outFileName);
	    }
	} else {
	    logger.warn("Tag: No tags found.");
	    listener.taggingDone(inFileName, outFileName);
	}
    }

    protected String tagString(ArrayList<TagList> tagLists){
	if (tagLists != null) {
	    StringBuilder str = new StringBuilder();
	    for (TagList tagList: tagLists){
		for (String tagName: tagList.getTagNames()){
		    str.append(tagName);
		    str.append('=');
		    for (Object obj: tagList.getValues(tagName)){
			str.append(obj);
			str.append(',');
		    }
		}
	    }

	    str.setLength(str.length()-1);
	    String tagString = str.toString();

	    return tagString;
	} else {
	    return null;
	}
    }
    
}

interface TaggingPipelineListener {
    public void taggingFailed(String fileName, String newFileName);
    public void taggingDone(String fileName, String newFileName);
}

