package org.gramvaani.radio.medialib;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.UpdateProgramMessage;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.FlushIndexMessage;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.diagnostics.*;

import java.util.*;
import java.io.*;

public class FileStoreManager implements Runnable, FileStoreUploadListener, DiagnosticUtilitiesServletCallback {
    static final long FILE_PUSH_ATTEMPT_INTERVAL = 60000; //1 minute 
    
    public static final int MAX_UPLOAD_ATTEMPTS = 10;
    public static final int ZERO_ATTEMPTS 	= 1;
    public static final int UPLOAD_DONE 	= 0;
    public static final int FILE_DELETED 	= -5;
    public static final int MAX_RETRY_ATTEMPTS 	= 3;

    LogUtilities logger;
    IPCServer ipcServer;
    StationConfiguration stationConfig;
    String[] destinationStores;
    String[] uniqueStores;
    Thread fileUploaderThread;
    FileStoreUploader fileStoreUploader;
    Hashtable<String, FileToUpload> filesToUpload;
    MediaLib medialib;
    DiagnosticUtilities diagUtil;
    TimeKeeper timeKeeper;
    String localStoreName;
    String callerName;
    boolean recoveryAttempted;
    Object waitObject = new Object();
    Object threadInterruptObject = new Object();
    boolean uploadTriggered = false;
    boolean stopUploaderThread = false;
    String logPrefix;

    static final LogUtilities staticLogger;

    static {
	staticLogger = new LogUtilities("FileStoreUploader: Static");
    }

    public FileStoreManager(StationConfiguration stationConfig, String logPrefix, IPCServer ipcServer, String[] destinationStores, TimeKeeper timeKeeper, String localStoreName, String callerName) {
	logger = new LogUtilities(logPrefix + ":FileStoreManager");
	this.ipcServer = ipcServer;
	this.stationConfig = stationConfig;
	this.destinationStores = destinationStores;
	this.localStoreName = localStoreName;
	this.callerName = callerName; //used to return in getName()
	fileStoreUploader = new FileStoreUploader(this, stationConfig, logPrefix + ":FileStoreManager", timeKeeper);
	filesToUpload = new Hashtable<String, FileToUpload>();
	uniqueStores = ((StationNetwork)stationConfig.getStationNetwork()).getUniqueStores(destinationStores);
	medialib = MediaLib.getMediaLib(stationConfig, timeKeeper);
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities();
	this.timeKeeper = timeKeeper;
	this.logPrefix = logPrefix;
	fileUploaderThread = new Thread(this, logPrefix + ":FileStoreManager");
	fileUploaderThread.start();
    }

    //XXX: Make FileToUpload use unique stores instead of all destination stores.
    //No correctness issues. just a cleaner way to do things.
    public void addFileToUpload(String fileName, int[] destinationStoreAttempts) {
	String storeDir = stationConfig.getStationNetwork().getStoreDir(localStoreName);
	FileToUpload file = new FileToUpload(storeDir + File.separator + fileName, destinationStores, destinationStoreAttempts);
	synchronized(filesToUpload) {
	    filesToUpload.put(fileName, file);
	}
    }

    public void addFileToUpload(String fileName) {
	String storeDir = stationConfig.getStationNetwork().getStoreDir(localStoreName);
	FileToUpload file = new FileToUpload(storeDir + File.separator + fileName, destinationStores);
	synchronized(filesToUpload) {
	    filesToUpload.put(fileName, file);
	}
    }

    protected boolean stopUploaderThread() {
	synchronized(threadInterruptObject) {
	    stopUploaderThread = true;
	    threadInterruptObject.notifyAll();
	}

	try {
	    fileUploaderThread.join();
	} catch(InterruptedException e) {
	    logger.error("HandleCleanup: Interrupted while waiting for uploader thread to terminate");
	    return false;
	}

	return true;
    }

    public synchronized boolean syncStoresManually() {
	int uploadAttempts;
	boolean needUpdate;
	RadioProgramMetadata whereMetadata;

	if (stopUploaderThread()) {
	    filesToUpload.clear();
	    fileUploaderThread = new Thread(this, logPrefix + ":FileStoreManager");
	    fileUploaderThread.start();

	    RadioProgramMetadata[] localFilesMetadata = getLocalFilesMetadata(true);
	    ArrayList<RadioProgramMetadata> toCleanup = new ArrayList<RadioProgramMetadata>();

	    if (localFilesMetadata == null)
		return false;
	    
	    for (RadioProgramMetadata fileMetadata : localFilesMetadata) {
		needUpdate = false;
		for (String store : uniqueStores) {
		    uploadAttempts = getDestinationStoreUploadAttempts(fileMetadata, store);
		    if (uploadAttempts != UPLOAD_DONE && uploadAttempts != FILE_DELETED) {
			//Update the file store attempts to ZERO_ATTEMPTS
			fileStoreUploader.setFileStoreField(store, fileMetadata, ZERO_ATTEMPTS);
			needUpdate = true;
		    }
		}
		
		if (needUpdate) {
		    whereMetadata = new RadioProgramMetadata(fileMetadata.getItemID());
		    medialib.update(whereMetadata, fileMetadata);
		}
	    }

	    doCleanup();
	    return true;
	} else
	    return false;
    }

    public synchronized void handleCleanup() {
	if (stopUploaderThread()) {
	    filesToUpload.clear();
	    fileUploaderThread = new Thread(this, logPrefix + ":FileStoreManager");
	    fileUploaderThread.start();
	    doCleanup();
	}	
    }
    
    //Note: Calling doCleanup when there are already some files present in the filesToUpload
    //hash can lead to incorrectness. The best way will be to terminate the uploader thread
    //clear out the hash table, start the uploader thread and finally call docleanup.
    protected void doCleanup() {
	RadioProgramMetadata[] localFiles = getLocalFilesMetadata(false);
	if (localFiles == null)
	    return;

	logger.info("DoCleanup: " + localFiles.length + " files to cleanup.");

	int uploadAttempts;
	for (RadioProgramMetadata fileMetadata : localFiles) {
	    for (String store : destinationStores) {
		uploadAttempts = getDestinationStoreUploadAttempts(fileMetadata, store);
		if (uploadAttempts != UPLOAD_DONE && uploadAttempts != FILE_DELETED && uploadAttempts < MAX_UPLOAD_ATTEMPTS) {
		    addFileToUpload(fileMetadata.getItemID(), getAllDestinationStoreAttempts(fileMetadata));
		    break;
		}
	    }
	}
	
	triggerUpload();
    }

    protected RadioProgramMetadata[] getLocalFilesMetadata(boolean forManualSync) {
	RadioProgramMetadata localStoreMetadata = RadioProgramMetadata.getDummyObject("");
	medialib = MediaLib.getMediaLib(stationConfig, timeKeeper);
	if (medialib == null) {
	    logger.error("GetLocalFilesMetadata: MediaLib is null.");
	    return null;
	}
	
	String whereClause;
	if (forManualSync) {
	    whereClause = getManualSyncWhereClause(localStoreName);
	} else {
	    whereClause = getRegularSyncWhereClause(localStoreName);
	}

	return medialib.selectAllFields(localStoreMetadata, whereClause, -1);
    }

    protected String getManualSyncWhereClause(String localStoreName) {
	String whereClause = getFileStoreDBFieldName(localStoreName) + "=" + FileStoreManager.UPLOAD_DONE;
	whereClause += " and (";
	for (int i = 0; i < destinationStores.length; i++) {
	    String destination = destinationStores[i];
	    String destFieldName = getFileStoreDBFieldName(destination);
	    whereClause += "(" + destFieldName + "!=" + UPLOAD_DONE + 
		" and " + destFieldName + "!=" + FILE_DELETED + ")";
	    if ((i+1) != destinationStores.length)
		whereClause += " or ";
	}
	whereClause += ")";

	logger.debug("GetManualSyncWhereClause: WhereClause= " + whereClause);
	return whereClause;
    }

    protected String getRegularSyncWhereClause(String localStoreName) {
	String whereClause = getFileStoreDBFieldName(localStoreName) + "=" + FileStoreManager.UPLOAD_DONE;
	whereClause += " and (";
	for (int i = 0; i < destinationStores.length; i++) {
	    String destination = destinationStores[i];
	    String destFieldName = getFileStoreDBFieldName(destination);
	    whereClause += "(" + destFieldName + "!=" + UPLOAD_DONE + 
		" and " + destFieldName + "<" + MAX_UPLOAD_ATTEMPTS + 
		" and " + destFieldName + "!=" + FILE_DELETED + ")";
	    if ((i+1) != destinationStores.length)
		whereClause += " or ";
	}
	whereClause += ")";

	logger.debug("GetRegularSyncWhereClause: WhereClause= " + whereClause);
	return whereClause;
    }

    public static String getFileStoreDBFieldName(String storeName) {
	return FileStoreUploader.getFileStoreDBFieldName(storeName);
    }

    public void triggerUpload() {
	synchronized(threadInterruptObject) {
	    uploadTriggered = true;
	    threadInterruptObject.notifyAll();
	}
    }

    @SuppressWarnings("unchecked")
    public void run() {
	String fileName;
	FileToUpload file;
	String returnCode;
	int uploadAttempts;
	Hashtable<String, FileToUpload> tmpToUpload;
	ArrayList<String> itemsToRemove = new ArrayList<String>();

	while (true) {
	    itemsToRemove.clear();
	    synchronized(filesToUpload) {
		tmpToUpload = (Hashtable<String, FileToUpload>)filesToUpload.clone();
	    }
	    
	    Enumeration<String> e = tmpToUpload.keys();

	    while (e.hasMoreElements()) {
		if (ipcServer == null) {
		    logger.debug("Run: IPCServer is null going back to sleep.");
		    break;
		}
		fileName = e.nextElement();
		file = tmpToUpload.get(fileName);
		
		boolean invalidateCache = false;
		for (String destination: uniqueStores){
		    if (attemptUpload(destination, file)) {
			if (!invalidateCache && shouldInvalidateCache(destination))
			    invalidateCache = true;
		    }
		}
		
		if (invalidateCache){
		    InvalidateCacheMessage invalidateMessage = new InvalidateCacheMessage(getName(), RSController.LIB_SERVICE, new String[]{}, Metadata.getClassName(RadioProgramMetadata.class), LibService.CACHE_UPDATE, new String[]{fileName});
		    
		    if (sendMessage(invalidateMessage) < 0)
			logger.error("Run: Unable to send invalidate message.");
		    else
			logger.debug("Run: Sent Invalidate Cache.");
		}
			
		if (shouldRemoveFile(file)) {
		    itemsToRemove.add(fileName);
		}
	    }

	    synchronized(filesToUpload) {
		for (String item : itemsToRemove) {
		    filesToUpload.remove(item);
		}
	    }
	    
	    synchronized(threadInterruptObject) {
		if (stopUploaderThread) {
		    stopUploaderThread = false;
		    break;
		}

		if (uploadTriggered) {
		    uploadTriggered = false;
		    continue;
		}

		try {
		    threadInterruptObject.wait(FILE_PUSH_ATTEMPT_INTERVAL);
		} catch(InterruptedException ex) {}
		
		uploadTriggered = false;

		if (stopUploaderThread) {
		    stopUploaderThread = false;
		    break;
		}
	    }

	}
    }

    //Same method also present in upload servlet
    //XXX: move common code to a common entity like say FileStoreUtilities
    protected boolean shouldInvalidateCache(String fileStoreName) {
	if (!fileStoreName.equals(StationNetwork.UPLOAD))
	    return true;
	
	String[] stores =((StationNetwork)stationConfig.getStationNetwork()).getSameStores(fileStoreName);
	for (String store : stores) {
	    if (!store.equals(fileStoreName))
		return true;
	}

	return false;
    }

    //Used internally. Different from attemptUpload() which is a callback from diagutil
    protected boolean attemptUpload(String destination, FileToUpload file) {
	String returnCode;
	String fileName = file.getFileName();
	int uploadAttempts = file.getUploadAttempts(destination);
	File destFile = new File(fileName);
	String destFileName = destFile.getName();
	if (uploadAttempts != UPLOAD_DONE && uploadAttempts != MAX_UPLOAD_ATTEMPTS && uploadAttempts != FILE_DELETED) {
	    if (!isFileStoreLocal(destination)) {
		returnCode = fileStoreUploader.uploadFile(fileName, destination, false, destFileName);
		if (!FileUtilities.isSupported(returnCode)) {
		    logger.error("AttemptUpload: Upload attempt failed for file " + fileName);
		    uploadError(destFileName, destination, returnCode, file);		    
		}
		else {
		    setUploadAttempts(file, destination, UPLOAD_DONE);
		}
	    }
	    else {
		//update database about the same.
		//Note that when we go down the information that we have tried to upload since last 
		//database update is lost
		if (updateDB(destFileName, destination, UPLOAD_DONE)) {
		    setUploadAttempts(file, destination, UPLOAD_DONE);
		    if (destination.equals(StationNetwork.UPLOAD)) {
			UploadDoneMessage uploadDoneMessage = new UploadDoneMessage(getName(),
										RSController.LIB_SERVICE,
										destFileName,
										getName());
			sendUploadDoneMessage(uploadDoneMessage);
		    }
		    return true;
		}
		else
		    incrementUploadAttempts(file, destination);
	    }
	}
	return false;
    }

    protected void uploadError(String destFileName, String destFileStore, String error, FileToUpload file) {
	incrementUploadAttempts(file, destFileStore);
	int uploadAttempts = file.getUploadAttempts(destFileStore);
	logger.error("UploadError: file=" + destFileName + " error=" + error + "upload attempts=" + uploadAttempts + "fileStore=" + destFileStore);

	if (error != FileStoreUploader.ERROR_DB_FAILED)
	    updateDB(destFileName, destFileStore, uploadAttempts);

	if (error.equals(RSController.ERROR_SERVLET_CONNECTION_FAILED)) {
	    if (uploadAttempts != MAX_RETRY_ATTEMPTS) {
		recoverFromError(destFileStore, file);
	    } 
	} else if (error.equals(FileStoreUploader.ERROR_DB_FAILED)){
	    StatusCheckRequestMessage msg = new StatusCheckRequestMessage(RSController.ANON_SERVICE, 
									  RSController.LIB_SERVICE, 
									  "", "1");
	    if (sendMessage(msg) == -1)
	    	logger.error("UploadError: Could not send status check message to library service.");
	}
		
    }

    protected void recoverFromError(String destFileStore, FileToUpload file) {
	recoveryAttempted = false;

	synchronized(waitObject){
	    diagUtil.servletError(destFileStore, this);
	    while (!recoveryAttempted){
		try{
		    waitObject.wait();
		}catch(InterruptedException e){}
	    }
	}
	
	attemptUpload(destFileStore, file);
    }

    
    //Callback from diagutil. 
    public void attemptUpload(){
	synchronized(waitObject){
	    recoveryAttempted = true;
	    waitObject.notifyAll();
	}
    }

    protected boolean shouldRemoveFile(FileToUpload file) {
	boolean shouldRemove = true;
	int uploadAttempts;

	for (String destination : destinationStores) {
	    uploadAttempts = file.getUploadAttempts(destination);
	    if (uploadAttempts != UPLOAD_DONE && uploadAttempts != MAX_UPLOAD_ATTEMPTS && uploadAttempts != FILE_DELETED) {
		shouldRemove = false;
		break;
	    }
	}

	return shouldRemove;
    }

    /*
    public boolean updateDB(String fileName, String fileStore, int count) {
	return updateDB(fileName, fileStore, count, true);
    }
    */

    public boolean updateDB(String fileName, String fileStore, int count){ 
	if (medialib == null) {
	    medialib = MediaLib.getMediaLib(stationConfig, timeKeeper);
	    if (medialib == null) {
		logger.error("UpdateDB: Unable to connect to the database.");
		return false;
	    }
	}

	logger.info("UpdateDB: Updating fileStoreAttempts=" + count + " for " + fileStore + " and its same stores.");

	RadioProgramMetadata queryMetadata = new RadioProgramMetadata(fileName);
	RadioProgramMetadata updateMetadata = new RadioProgramMetadata(fileName);

	fileStoreUploader.setFileStoreField(fileStore, updateMetadata, count);	
	
	if (medialib.update(queryMetadata, updateMetadata, false) >= 0) {
	    return true;
	} else {
	    return false;
	}
    }

    public static boolean isAvailableAtConsumerStores(RadioProgramMetadata metadata){
	for (String store: StationNetwork.getConsumerStores()){
	    if (!isAvailableAtStore(metadata, store))
		return false;
	}
	return true;
    }

    public static boolean isAvailableAtStore(RadioProgramMetadata metadata, String store){
	if (store.equals(StationNetwork.AUDIO_PLAYOUT))
	    return FileStoreManager.isAvailableAtStoreState(metadata.getPlayoutStoreAttempts());
	else if (store.equals(StationNetwork.AUDIO_PREVIEW))
	    return FileStoreManager.isAvailableAtStoreState(metadata.getPreviewStoreAttempts());	
	else if (store.equals(StationNetwork.UPLOAD))
	    return FileStoreManager.isAvailableAtStoreState(metadata.getUploadStoreAttempts());	
	else if (store.equals(StationNetwork.ARCHIVER))
	    return FileStoreManager.isAvailableAtStoreState(metadata.getArchiveStoreAttempts());
	else if (store.equals(StationNetwork.ONLINETELEPHONY))
	    return FileStoreManager.isAvailableAtStoreState(metadata.getTelephonyStoreAttempts());
	else {
	    staticLogger.error("IsAvailableAtStore: Unknown store: "+store);
	    return false;
	}
    }

    public static boolean isAvailableAtStoreState(int state){
	return state == UPLOAD_DONE;
    }

    protected boolean isFileStoreLocal(String fileStore) {
	return stationConfig.getStationNetwork().isLocalStore(fileStore);
    }

    public void setIPCServer(IPCServer ipcServer) {
	this.ipcServer = ipcServer;
	fileStoreUploader.connectionUp(true);
    }

    public int sendUploadDoneMessage(UploadDoneMessage message) {
	return sendMessage(message);
    }

    protected int sendMessage(IPCMessage message) {
	if (ipcServer == null) {
	    logger.error("SendMessage: IPCServer is null.");
	    fileStoreUploader.connectionUp(false);
	    return -1;
	} else {
	    int retVal = ipcServer.handleOutgoingMessage(message);
	    if (retVal == -1) {
		logger.error("SendMessage: Unable to send upload done message.");
		fileStoreUploader.connectionUp(false);
	    }
	    return retVal;
	}
    }

    public void updateProgress(String file, long completed, long total) {}

    protected int[] getAllDestinationStoreAttempts(RadioProgramMetadata fileMetadata) {
	int[] uploadAttempts = new int[destinationStores.length];
	int i = 0;
	for (String store: destinationStores) {
	    uploadAttempts[i++] = getDestinationStoreUploadAttempts(fileMetadata, store);
	}
	return uploadAttempts;
    }

    protected int getDestinationStoreUploadAttempts(RadioProgramMetadata fileMetadata, String store) {
	if (store.equals(StationNetwork.AUDIO_PLAYOUT)) 
	    return fileMetadata.getPlayoutStoreAttempts();
	else if (store.equals(StationNetwork.AUDIO_PREVIEW)) 
	    return fileMetadata.getPreviewStoreAttempts();
	else if (store.equals(StationNetwork.UPLOAD)) 
	    return fileMetadata.getUploadStoreAttempts();
	else if (store.equals(StationNetwork.ARCHIVER)) 
	    return fileMetadata.getArchiveStoreAttempts();
	
	return -2;
    }

    public void clearAllFileStoreFields(RadioProgramMetadata metadata) {
	fileStoreUploader.clearAllFileStoreFields(metadata);
    }

    public void setFileStoreField(RadioProgramMetadata metadata, String fileStore, int count) {
	fileStoreUploader.setFileStoreField(fileStore, metadata, count);
    }

    public String getName() {
	return callerName;
    }

    protected synchronized void setUploadAttempts(FileToUpload file, String store, int attempts) {
	String[] stores =((StationNetwork)stationConfig.getStationNetwork()).getSameStores(store);
	for (String storeName : stores) {
	    file.setUploadAttempts(storeName, attempts); 
	}
    }

    protected synchronized void incrementUploadAttempts(FileToUpload file, String store) {
	int attempts = file.getUploadAttempts(store);
	setUploadAttempts(file, store, attempts+1);
    }

    
}

class FileToUpload {
    String fileName;
    Hashtable<String, Integer> uploadAttempts;

    public FileToUpload(String fileName, String[] destinationStores, int[] destinationStoreAttempts) {
	this.fileName = fileName;
	uploadAttempts = new Hashtable<String, Integer>();
	int i=0;
	for (String destination : destinationStores) {
	    uploadAttempts.put(destination, destinationStoreAttempts[i++]);
	}
    }

    public FileToUpload(String fileName, String[] destinationStores) {
	this.fileName = fileName;
	uploadAttempts = new Hashtable<String, Integer>();
	int i=0;
	for (String destination : destinationStores) {
	    uploadAttempts.put(destination, FileStoreManager.ZERO_ATTEMPTS);
	}
    }

    public synchronized String getFileName() {
	return fileName;
    }

    public synchronized int getUploadAttempts(String destinationStore) {
	return uploadAttempts.get(destinationStore);
    }

    public synchronized void incrementUploadAttempts(String destinationStore) {
	int attempts = uploadAttempts.get(destinationStore);
	uploadAttempts.put(destinationStore, attempts+1);
    }

    public synchronized void setUploadAttempts(String destinationStore, int attempts) {
	uploadAttempts.put(destinationStore, attempts);
    }

    
   
}