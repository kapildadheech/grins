package org.gramvaani.radio.medialib;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.timekeeper.*;

import java.util.*;

public class LibConnectionPool {
    ArrayList<MediaLib> availableConnections, busyConnections;
    int maxConnections;
    
    protected StationConfiguration stationConfiguration;
    protected TimeKeeper timeKeeper;
    
    public LibConnectionPool(StationConfiguration stationConfiguration, TimeKeeper timeKeeper) {
	this.stationConfiguration = stationConfiguration;
	this.timeKeeper = timeKeeper;
	
	availableConnections = new ArrayList<MediaLib>();
	busyConnections = new ArrayList<MediaLib>();
	
	// just one connection to the library is needed since we are now using lucene indexing, instead to fetching data from the database
	// maxConnections = stationConfiguration.getIntParam(StationConfiguration.MAX_SEARCH_SESSIONS);
	// for(int i=0; i<maxConnections; i++) 
	//    availableConnections.add(new MediaLib(stationConfiguration, timeKeeper));
	
	maxConnections = 1;
	availableConnections.add(MediaLib.getMediaLib(stationConfiguration, timeKeeper));
    }
    
    public MediaLib getConnectionFromPool() {
	return availableConnections.get(0);
    }

    public void returnConnectionToPool(MediaLib libConn) {
	// do nothing
    }

    //    public MediaLib getConnectionFromPool() {
    //	if(availableConnections.size() > 0) {
    //	    MediaLib libConn = availableConnections.remove(0);
    //	    busyConnections.add(libConn);
    //	    return libConn;
    //	} else {
    //	    return null;
    //	}
    //    }

    //    public void returnConnectionToPool(MediaLib libConn) {
    //	for(int i=0; i<busyConnections.size(); i++) {
    //	    if(busyConnections.get(i) == libConn) {
    //		busyConnections.remove(i);
    //		availableConnections.add(libConn);
    //		return;
    //	    }
    //	}
    //    }


}