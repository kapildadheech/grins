package org.gramvaani.radio.medialib;

import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.rscontroller.services.LibService;

import java.util.*;

public class NewItemFilter extends SearchFilter {

    public static final String titleKey = Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getTitleFieldName();
    public static final String stateKey = Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getStateFieldName();
    public static final String creationKey = Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getCreationTimeFieldName();
    //    public static final String sqlCommand = "(" + titleKey + " = '' or " + titleKey + " is null )";
    public static final String sqlCommand = "(" + creationKey + " > " + 
	(System.currentTimeMillis() - (long)7 * 24 * 60 * 60 * 1000) + ")";

    public static final String NEWITEM_ANCHOR = "NEWITEM_ANCHOR"; 
    boolean newItemActive = false;

    public NewItemFilter() {
    }

    public String getFilterName() {
	return SearchFilter.NEWITEM_FILTER;
    }

    public String getFilterLabel() {
	return "New items";
    }

    public String getFilterQueryClause() {
	return "";
    }

    public String getFilterWhereClause() {
	return sqlCommand;
    }

    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(RadioProgramMetadata.class)};
    }

    public String getSelectedText(String anchorID) {
	return "";
    }

    public void activateAnchor(String anchor, String anchorText) {
	if(anchor.equals(NEWITEM_ANCHOR)) {
	    newItemActive = true;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }

}
