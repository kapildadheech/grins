package org.gramvaani.radio.medialib;

import org.gramvaani.radio.app.providers.CacheProvider;

import java.util.*;

public class LicenseFilter extends SearchFilter {
    public String selectedLicense;

    public static final String licenseKey = Metadata.getClassName(RadioProgramMetadata.class) + "." + 
	RadioProgramMetadata.getLicenseFieldName();

    public static final String LICENSE_ANCHOR = "LICENSE_ANCHOR"; boolean licenseActive = false;

    public LicenseFilter() {
	this.selectedLicense = "";
    }

    public String getFilterName() {
	return SearchFilter.LICENSE_FILTER;
    }

    public String getFilterLabel() {
	return "License";
    }

    public String getSelectedText(String anchorID) {
	if(anchorID.equals(LICENSE_ANCHOR))
	    return selectedLicense;
	else
	    return "";
    }

    public String getFilterQueryClause() {
	StringBuilder str = new StringBuilder();
	if(!selectedLicense.equals("") && licenseActive) {
	    str.append(LICENSE_ANCHOR); str.append(":\""); str.append(selectedLicense); str.append("\"");
	}
	
	return str.toString();	
    }

    public String getFilterWhereClause() {
	return "";
    }

    public String[] getFilterJoinTables() {
	return new String[]{};
    }

    public void setSelectedLicense(String license) {
	this.selectedLicense = license;
    }

    public void activateAnchor(String anchor, String anchorText) {
	if(anchor.equals(LICENSE_ANCHOR)) {
	    licenseActive = true;
	    selectedLicense = anchorText;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }

    public static String[] getLicenses(CacheProvider cacheProvider) {
	Object[] licenses = cacheProvider.uniqueFromAnchors(RadioProgramMetadata.getDummyObject(""), RadioProgramMetadata.getLicenseFieldName());
	return getLicenses(licenses);
    }

    public static String[] getLicenses(MediaLib medialib) {
	Object[] licenses = medialib.unique(RadioProgramMetadata.getDummyObject(""), RadioProgramMetadata.getLicenseFieldName());
	return getLicenses(licenses);
    }

    private static String[] getLicenses(Object[] licenses) {
	ArrayList<String> arr = new ArrayList<String>();
	for(Object license: licenses)
	    arr.add((String)license);
	return arr.toArray(new String[]{});
    }

}


/*
    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(RadioProgramMetadata.class)};
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if(!selectedLicense.equals("") && licenseActive) {
	    str.append("instr("); str.append(licenseKey); str.append(", \'"); str.append(selectedLicense); str.append("\') != 0");
	}
	
	return str.toString();
    }


*/


