package org.gramvaani.radio.medialib;
import org.gramvaani.radio.rscontroller.messages.*;

public interface FileStoreUploadListener {
    public void updateProgress(String fileName, long uploadedLength, long totalLength);
    public int sendUploadDoneMessage(UploadDoneMessage message);
    public String getName();
}