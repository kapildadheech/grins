package org.gramvaani.radio.medialib;

import java.util.*;

public class Category extends Metadata implements Comparable<Category> {
    protected Integer _nodeID;
    protected String _nodeName;
    protected String _label;
    protected Integer _parentID;

    protected Category[] __children;

    public static final int ROOT_PARENT_ID = -2;

    public String dump() {
	return "Category: " + _nodeID + ":" + _nodeName + ":" + _label + ":" + _parentID;
    }

    protected Category _parent;         protected static String foreign_parent = "_nodeID", local_parent = "_parentID";

    protected Hashtable<String, Category> categoryTreeNodes;
    protected boolean categoryTreeNodesPopulated = false;

    public Category(String nodeName, String label, int parentID) {
	this._nodeID = new Integer(-1);
	this._nodeName = nodeName;
	this._label = label;
	this._parentID = parentID;

	this._parent = null;
	this.__children = null;

	categoryTreeNodes = new Hashtable<String, Category>();
    }

    public Category(String nodeName) {
	_nodeID = new Integer(-1);
	_nodeName = nodeName;
	_label = "";
	_parentID = new Integer(-1);

	_parent = null;
	__children = new Category[]{};;

	categoryTreeNodes = new Hashtable<String, Category>();
    }

    public Category() {
	_nodeID = new Integer(-1);
	_nodeName = "";
	_label = "";
	_parentID = new Integer(-1);

	_parent = null;
	__children = null;

	categoryTreeNodes = new Hashtable<String, Category>();
    }

    public Category(int nodeID){
	this();
	_nodeID = new Integer(nodeID);
    }

    public void setNodeID(int nodeID) {
	this._nodeID = new Integer(nodeID);
    }

    public void setNodeID(Integer nodeID) {
	this._nodeID = nodeID;
    }

    public void setNodeName(String nodeName) {
	this._nodeName = nodeName;
    }

    public void setParentID(int parentID) {
	this._parentID = new Integer(parentID);
    }

    public int getNodeID() {
	return _nodeID.intValue();
    }

    public String getNodeName() {
	return _nodeName;
    }

    public String getLabel() {
	return _label;
    }

    public void setLabel(String label){
	_label = label;
    }

    public int getParentID() {
	return _parentID.intValue();
    }

    public static String getParentIDFieldName(){
	return "_parentID";
    }

    public Category getParent() {
	return _parent;
    }

    public Category[] getChildren() {
	return __children;
    }

    public static Category getDummyObject(Integer nodeID) {
	Category category = new Category();
	category.setNodeID(nodeID);
	return category;
    }

    public static Category getDummyObjectByNonPrimaryKey(String nodeName) {
	Category category = new Category();
	category.setNodeName(nodeName);
	return category;
    }

    public String primaryKey() {
	return "_nodeID";
    } 

    public String unique() {
	return _nodeName;
    }

    public String uniqueNull() {
	return "";
    }

    public Category cloneObject() {
	return getDummyObject(new Integer(-1));
    }

    public Category[] populate__children(MediaLib medialib) {
	Category category = Category.getDummyObjectByNonPrimaryKey("");
	category.setParentID(_nodeID);
	Metadata[] children = medialib.get(category);
	__children = new Category[children.length];
	int i = 0;
	for (Metadata metadata: children)
	    __children[i++] = (Category)metadata;
	return __children;
    }

    public Category[] getAllChildren(MediaLib medialib) {
	if (!categoryTreeNodesPopulated) {
	    getAllChildrenHelper(this, medialib);
	    categoryTreeNodesPopulated = true;
	}
	return categoryTreeNodes.values().toArray(new Category[]{});
    }

    public Category[] getAllChildren() {
	if (!categoryTreeNodesPopulated) {
	    getAllChildrenHelper(this);	    
	    categoryTreeNodesPopulated = true;
	}
	return categoryTreeNodes.values().toArray(new Category[]{});
    }

    private void getAllChildrenHelper(Category category, MediaLib medialib) {
	Category[] immediateChildren = category.populate__children(medialib);
	if (immediateChildren != null && immediateChildren.length > 0) {
	    for (Category child: immediateChildren) {
		categoryTreeNodes.put(child.getNodeName(), child);
		getAllChildrenHelper(child, medialib);
	    }
	}
    }

    private void getAllChildrenHelper(Category category) {
	if (category.__children != null) {
	    for (Category child: category.__children) {
		categoryTreeNodes.put(child.getNodeName(), child);
		getAllChildrenHelper(child);
	    }
	}
    }

    public String getAllChildrenNamesCSV(MediaLib medialib) {
	Category[] children = getAllChildren(medialib);
	StringBuilder str = new StringBuilder();
	for (Category child: children) {
	    str.append(child.getNodeName()); str.append(",");
	}
	if (str.length() > 0)
	    return str.substring(0, str.length() - 1);
	else
	    return "";
    }

    public String toString() {
	return _label;
    }
    
    public int compareTo(Category c){
	return _label.compareTo(c.getLabel());
    }

    public void truncateFields(){

    }    
}
