package org.gramvaani.radio.medialib;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.providers.MetadataProvider;

import java.util.*;

public class TypeFilter extends SearchFilter {
    public String selectedType;

    public static final String TYPE_ANCHOR = "TYPE_ANCHOR"; boolean typeActive = false;
    static final String typeField =  Metadata.getClassName(RadioProgramMetadata.class) + "." + RadioProgramMetadata.getTypeFieldName();

    public TypeFilter() {
	this.selectedType = "";
    }

    public String getFilterName() {
	return SearchFilter.TYPE_FILTER;
    }

    public String getFilterLabel() {
	return "Type";
    }

    public String getFilterQueryClause() {
	/*
	StringBuilder str = new StringBuilder();
	if(!selectedType.equals("") && typeActive) {
	    str.append(TYPE_ANCHOR); str.append(":\""); str.append(selectedType); str.append("\"");
	}
	
	return str.toString();	
	*/
	return "";
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if(!selectedType.equals("") && typeActive) {
	    str.append(typeField); 
	    str.append("='"); str.append(selectedType); str.append("'");
	}
	return str.toString();
    }

    public String[] getFilterJoinTables() {
	//return new String[]{};

	return new String[]{Metadata.getClassName(RadioProgramMetadata.class)};
    }

    public String getSelectedText(String anchorID) {
	if(anchorID.equals(TYPE_ANCHOR))
	    return selectedType;
	else
	    return "";
    }

    public void setSelectedType(String type) {
	this.selectedType = type;
    }

    public void activateAnchor(String anchor, String anchorText) {
	if(anchor.equals(TYPE_ANCHOR)) {
	    typeActive = true;
	    selectedType = anchorText;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }

    public static String[] getTypes(CacheProvider cacheProvider) {
	Object[] types = cacheProvider.uniqueFromAnchors(RadioProgramMetadata.getDummyObject(""), RadioProgramMetadata.getTypeFieldName());
	return getTypes(types);
    }

    public static String[] getTypes(MetadataProvider metadataProvider) {
	Object[] types = metadataProvider.unique(RadioProgramMetadata.getDummyObject(""), RadioProgramMetadata.getTypeFieldName());
	return getTypes(types);
    }

    private static String[] getTypes(Object[] types) {
	ArrayList<String> arr = new ArrayList<String>();
	for(Object type: types)
	    arr.add((String)type);
	return arr.toArray(new String[]{});
	//	return new String[]{ArchiverService.BCAST_MIC};
    }

}

/*
    public static final String typeKey = Metadata.getClassName(RadioProgramMetadata.class) + "." + 
	RadioProgramMetadata.getTypeFieldName();

    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(RadioProgramMetadata.class)};
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if(!selectedType.equals("") && typeActive) {
	    str.append(typeKey); str.append(" = '"); str.append(selectedType); str.append("'");
	}
	
	return str.toString();
    }
*/


