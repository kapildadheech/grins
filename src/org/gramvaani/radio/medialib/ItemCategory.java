package org.gramvaani.radio.medialib;

import java.util.*;

public class ItemCategory extends Metadata {
    protected String _itemID;
    protected Integer _categoryNodeID;

    public String dump() {
	return "ItemCategory: " + _itemID + ":" + _categoryNodeID;
    }

    protected Category _category;               protected static String foreign_category = "_nodeID", local_category = "_categoryNodeID";

    public ItemCategory(String itemID, int categoryNodeID) {
	this._itemID = itemID;
	this._categoryNodeID = new Integer(categoryNodeID);
    }

    public ItemCategory(String itemID) {
	_itemID = itemID;
	_categoryNodeID = new Integer(-1);
    }

    public ItemCategory() {
	_itemID = "";
	_categoryNodeID = new Integer(-1);
    }

    public ItemCategory(int categoryNodeID){
	this();
	_categoryNodeID = categoryNodeID;
    }

    public void setItemID(String itemID) {
	this._itemID = itemID;
    }

    public String getItemID() {
	return _itemID;
    }

    public static String getItemIDFieldName(){
	return "_itemID";
    }

    public int getCategoryNodeID() {
	return _categoryNodeID.intValue();
    }

    public void setCategoryNodeID(int nodeID) {
	_categoryNodeID = new Integer(nodeID);
    }

    public static String getCategoryNodeIDFieldName() {
	return "_categoryNodeID";
    }

    public Category getCategory() {
	return _category;
    }

    public void setCategory(Category category) {
	_category = category;
    }

    public static ItemCategory getDummyObject(String itemID) {
	ItemCategory itemCategory = new ItemCategory();
	itemCategory.setItemID(itemID);
	return itemCategory;
    }

    public String unique() {
	return _itemID +":" + _categoryNodeID.toString();
    }

    public String uniqueNull() {
	return ":-1";
    }

    public ItemCategory cloneObject() {
	return getDummyObject("");
    }

    public void truncateFields(){

    }
}
