package org.gramvaani.radio.medialib;

public class TextFilter extends SearchFilter {
    public String selectedText;

    public static final String TITLE_ANCHOR = "TITLE_ANCHOR"; boolean titleActive = false;
    public static final String DESC_ANCHOR = "DESC_ANCHOR"; boolean descActive = false;
    //public static final String TAGS_ANCHOR = "TAGS_ANCHOR"; boolean tagsActive = false;
    public static final String CREATOR_ANCHOR = "CREATOR_ANCHOR"; boolean creatorActive = false;
    public static final String ENTITY_ANCHOR = "ENTITY_ANCHOR"; boolean entityActive = false;

    public TextFilter() {
	this.selectedText = "";
    }

    public String getFilterName() {
	return SearchFilter.TEXT_FILTER;
    }

    public String getFilterLabel() {
	return "";
    }

    public String getFilterQueryClause() {
	StringBuilder str = new StringBuilder();
	if(!selectedText.equals("")) {
	    if(titleActive) {
		str.append(TITLE_ANCHOR); str.append(":"); str.append(selectedText); str.append("* OR ");
	    }
	    if(descActive) {
		str.append(DESC_ANCHOR); str.append(":"); str.append(selectedText); str.append("* OR ");
	    }
	    /*
	    if(tagsActive) {
		str.append(TAGS_ANCHOR); str.append(":"); str.append(selectedText); str.append("* OR ");
	    }
	    */
	    if(creatorActive) {
		str.append(CREATOR_ANCHOR); str.append(":"); str.append(selectedText); str.append("* OR ");
	    }
	    if(entityActive) {
		str.append(ENTITY_ANCHOR); str.append(":"); str.append(selectedText); str.append("* OR ");
	    }

	    if(str.length() > 0)
		str = new StringBuilder(str.substring(0, str.length() - 3));
	}

	return str.toString();
    }

    public String getFilterWhereClause() {
	return "";
    }

    public String[] getFilterJoinTables() {
	return new String[]{};
    }

    public String getSelectedText(String anchorID) {
	return selectedText;
    }

    public void setSelectedText(String text) {
	this.selectedText = text;
    }

    public void activateAnchor(String anchor, String anchorText) {
	if(anchor.equals(TITLE_ANCHOR)) {
	    titleActive = true;
	    selectedText = anchorText;
	} else if(anchor.equals(DESC_ANCHOR)) {
	    descActive = true;
	    selectedText = anchorText;
	    /*
	} else if(anchor.equals(TAGS_ANCHOR)) {
	    tagsActive = true;
	    selectedText = anchorText;
	    */
	} else if(anchor.equals(CREATOR_ANCHOR)) {
	    creatorActive = true;
	    selectedText = anchorText;
	} else if(anchor.equals(ENTITY_ANCHOR)) {
	    entityActive = true;
	    selectedText = anchorText;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }

}


