package org.gramvaani.radio.medialib;

import java.util.*;

public class RelatedContent extends Metadata {
    public static String PARENT = "PARENT";
    public static String CHILD = "CHILD";
    public static String TOPIC = "TOPIC";

    protected String _itemID1;
    protected String _itemID2;
    protected String _relationship;

    public String dump() {
	return "RelatedContent: " + _itemID1 + ":" + _itemID2 + ":" + _relationship;
    }

    protected RadioProgramMetadata _item1; protected static String foreign_item1 = "_itemID", local_item1 = "_itemID1";
    protected RadioProgramMetadata _item2; protected static String foreign_item2 = "_itemID", local_item2 = "_itemID2";

    public RelatedContent(String itemID1, String itemID2, String relationship) {
	this._itemID1 = itemID1;
	this._itemID2 = itemID2;
	this._relationship = relationship;
    }

    public RelatedContent(String itemID1) {
	_itemID1 = itemID1;
	_itemID2 = "";
	_relationship = "";
    }

    public RelatedContent() {
	_itemID1 = "";
	_itemID2 = "";
	_relationship = "";
    }

    public void setItemID1(String itemID) {
	this._itemID1 = itemID;
    }

    public String getItemID1() {
	return _itemID1;
    }

    public String getItemID2() {
	return _itemID2;
    }

    public String getRelationship() {
	return _relationship;
    }

    public RadioProgramMetadata getItem1RadioProgram() {
	return _item1;
    }

    public RadioProgramMetadata getItem2RadioProgram() {
	return _item2;
    }

    public static RelatedContent getDummyObject(String itemID) {
	RelatedContent related = new RelatedContent();
	related.setItemID1(itemID);
	return related;
    }

    public RelatedContent cloneObject() {
	return getDummyObject("");
    }

    public void truncateFields(){

    }
}
