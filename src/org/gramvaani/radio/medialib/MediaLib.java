package org.gramvaani.radio.medialib;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.rscontroller.pipeline.RSPipeline;

import java.util.*;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.*;

public class MediaLib {
    static final long RECONNECT_WINDOW = 5000;          // do not reconnect if a reconnect request was made in the last 5 sec

    public static final String MAX_OP = "max";
    public static final String MIN_OP = "min";
    public static final String AVG_OP = "avg";
    public static final String COUNT_OP = "count";
    public static final String SUM_OP = "sum";

    public static final String ORDER_ASC  = "ASC";
    public static final String ORDER_DESC = "DESC";

    public static final String ERROR_ACCESS_DENIED = "ERROR_ACCESS_DENIED";
    public static final String ERROR_LINK	   = "ERROR_LINK";
    
    public static final int CONNECTION_VALIDITY_TIMEOUT = 5;

    protected StationConfiguration stationConfig;
    protected TimeKeeper timeKeeper;

    protected Connection databaseConn;
    protected LogUtilities logger;
    protected IPCServer ipcServer = null;
    protected boolean libraryActivated = false;
    protected DiagnosticUtilities diagUtil;
    protected Boolean dbDown = true;

    protected long lastConnectionTime;

    private long lastFilenameAllocationTime;
    private int lastFilenameAllocationCount;

    protected static final Hashtable<String, MediaLibListener> listeners;
    protected final int storeID;

    protected static MediaLibActivationListener activationListener = null;

    static {
	listeners = new Hashtable<String, MediaLibListener>();
    }

    public MediaLib(StationConfiguration stationConfig, TimeKeeper timeKeeper) {
	this.stationConfig = stationConfig;
	this.timeKeeper = timeKeeper;
	logger = new LogUtilities(RSController.MEDIA_LIB);
	logger.info("MediaLib: Initializing.");
	databaseConn = null;
	lastFilenameAllocationTime = 0;
	lastFilenameAllocationCount = 0;
	if (MediaLib.staticIPCServer != null) 
	    this.ipcServer = MediaLib.staticIPCServer;
	else 
	    logger.warn("MediaLib: StaticIPCServer found null.");

	//XXX: diagUtil does not seem to be used by medialib. Verify its need later.
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfig, ipcServer);
	lastConnectionTime = -1;
	storeID = stationConfig.getStationNetwork().getLocalStoreID();
    }

    public static void setActivationListener(MediaLibActivationListener listener) {
	activationListener = listener;
    }

    public boolean isConnected() {
	if (databaseConn != null) {
	    try {
		return databaseConn.isValid(CONNECTION_VALIDITY_TIMEOUT);
	    } catch(Exception e) { // XXX just fall through?
	    }
	}
	return false;
    }

    public synchronized int connect() {
	try{
	    if (databaseConn != null){
		if (databaseConn.isValid(CONNECTION_VALIDITY_TIMEOUT)){
		    logger.debug("Connect: Already connected.");
		    return 0;
		}
	    }
	} catch (Exception e){
	    //Exception only if timeout value is less than zero.
	    logger.error("Connect: ", e);
	}

	lastConnectionTime = System.currentTimeMillis();
	for (int i = 0; i < 3; i++){
	    try {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		databaseConn = DriverManager.getConnection("jdbc:mysql://" + 
							   stationConfig.getStringParam(StationConfiguration.LIB_DB_SERVER) + "/" +
							   stationConfig.getStringParam(StationConfiguration.LIB_DB_NAME),
							   stationConfig.getStringParam(StationConfiguration.LIB_DB_LOGIN),
							   stationConfig.getStringParam(StationConfiguration.LIB_DB_PASSWORD)
							   );
		break;
	    } catch(Exception e) {
		logger.error("Connect: Unable to connect to database.", e);
		if (i == 2){
		    notifyException(e);
		    return -1;
		}
	    }

	}

	dbDown = false;
	if (activationListener != null)
	    activationListener.mediaLibUp(this);

	logger.info("Connect: Successfully connected to database.");
	return 0;
    }

    protected boolean goAheadWithQuery() {
	try {

	    if ((databaseConn != null && !databaseConn.isClosed()) ||
	       (lastConnectionTime == -1 || (System.currentTimeMillis() - lastConnectionTime > RECONNECT_WINDOW))) {

		return true;

	    } else {
		return false;
	    }

	} catch(Exception e) {
	    if (lastConnectionTime == -1 || (System.currentTimeMillis() - lastConnectionTime > RECONNECT_WINDOW))
		return true;
	    else
		return false;
	}
    }
        
    public synchronized void setLibraryActivated(boolean libraryActivated) {
	this.libraryActivated = libraryActivated;
    }

    public synchronized boolean getLibraryActivated() {
	return libraryActivated;
    }

    private Connection databaseConnection() {
	try {
	    if (!databaseConn.isValid(CONNECTION_VALIDITY_TIMEOUT)) {
		logger.warn("DatabaseConnection: Found database connection closed.");
		connect();
	    }
	} catch(Exception e) {
	    logger.error("DatabaseConnection: cannot connect to database",e);
	    notifyException(e);
	}
	return databaseConn;
    }

    protected void notifyException (Exception e){
	if (dbDown)
	    return;
	dbDown = true;
	String message = e.toString();
	if (message.startsWith("java.sql.SQLException: Access denied for user"))
	    notifyDBError(ERROR_ACCESS_DENIED);
	if (message.startsWith("com.mysql.jdbc.exceptions.jdbc4.CommunicationsException: Communications link failure"))
	    notifyDBError(ERROR_LINK);
    }

    protected void notifyDBError(String error){
	MediaLibListener listener;
	boolean sendMessage = true;
	String lib = RSController.LIB_SERVICE;
	logger.debug("NotifyDBError: Notifying listeners of the error.");
	if (error.equals(ERROR_ACCESS_DENIED) || error.equals(ERROR_LINK)){
	    Enumeration<MediaLibListener> e = listeners.elements();
	    while (e.hasMoreElements()){
		listener = (MediaLibListener)e.nextElement();
		listener.activateMediaLib(false);
	    }
	}
	if (error.equals(ERROR_LINK)){
	    Enumeration<String> e = listeners.keys();
	    while (e.hasMoreElements()){
		String entity = (String)e.nextElement();
		if (lib.equals(entity)){
		    sendMessage = false;
		    logger.debug("Found lib service as one of the listeners. Not sending status check message.");
		}
	    }
	    if (sendMessage){
		StatusCheckRequestMessage msg = new StatusCheckRequestMessage(RSController.ANON_SERVICE, RSController.LIB_SERVICE, "", "1");
		if (ipcServer == null || ipcServer.handleOutgoingMessage(msg) < 0){
		    logger.error("NotifyDBError: Could not send status check message to library service.");
		}
	    }
	}
    }

    public static synchronized void registerListener(String name, MediaLibListener listener){
	listeners.put(name, listener);
    }

    public synchronized String newBulkItemFilename() {
	long currentTime;
	if (timeKeeper != null)
	    currentTime = timeKeeper.getRealTime();
	else
	    currentTime = System.currentTimeMillis();

	if (currentTime == lastFilenameAllocationTime)
	    lastFilenameAllocationCount++;
	else
	    lastFilenameAllocationCount = 0;

	String random = Integer.toString(storeID) + String.format("%03d",(int)(Math.random() * 1000));
	//String filename = currentTime + "." + lastFilenameAllocationCount + "." + random + "." + 
	//    stationConfig.getStringParam(StationConfiguration.STATION_NAME);

	long uniqueNumber = currentTime * 10L + lastFilenameAllocationCount; 	//One digit
	uniqueNumber = uniqueNumber * 10L + storeID; 				//One digit
	uniqueNumber = uniqueNumber * 1000L + (long)(Math.random() * 1000L);		//Three digits
	
	String filename = alphaNumEncode(uniqueNumber) + "." +stationConfig.getStringParam(StationConfiguration.STATION_NAME);

	lastFilenameAllocationTime = currentTime;

	return filename;
    }

    public static String alphaNumEncode(long num){
	int exp = 1;
	
	long base = 36;
	
	long div = base;
	while (num / div >= 1L){
	    div *= base;
	    exp++;
	}

	int numDigits = exp;
	int arr[] = new int[numDigits];

	div = base * 1L;
	for (int i = 0; i < numDigits; i++){
	    arr[numDigits - i - 1] = (int)(num % base);
	    num /= base;
	}

	StringBuilder str = new StringBuilder();
	for (int i: arr){
	    if (i < 10)
		str.append(i);
	    else
		str.append((char)(97+(i - 10)));
	}
	return str.toString();
    }

    public String findImportedFile(String fileName){
	try{
	    RadioProgramMetadata query = new RadioProgramMetadata(fileName);
	    RadioProgramMetadata program = getSingle(query);

	    if (program == null){
		String[] subStr = fileName.split("[.]");

		if (subStr.length < 4){
		    return null;
		}
		
		String prefix = "";
		for (int i = 0; i < subStr.length - 2; i++){
		    if (i > 0)
			prefix += ".";
		    prefix += subStr[i];
		}
		prefix += ".";
		
		for (String extension: FileUtilities.SUPPORTED_EXTNS){
		    program = getSingle(new RadioProgramMetadata(prefix + extension));
		    //if (program != null && program.getType().equals(LibService.TELEPHONY_TYPE))
		    if (program != null)
			break;
		}
	    }

	    if (program == null)
		return null;
	    else
		return program.getItemID();
	} catch (Exception e){
	    LogUtilities.getDefaultLogger().error("IsImportedFile: Encountered Exception.", e);
	}
	return null;
    }

    public boolean updateImportedFile(String programID, String originalID) {

	if (programID == null || originalID == null || programID.equals("") || originalID.equals("")) {
	    LogUtilities.getDefaultLogger().error("UpdateImportedFile: Null param: new: "+programID +" old: "+originalID);
	    return false;
	}

	RadioProgramMetadata newProgram = getSingle(new RadioProgramMetadata(programID));
	RadioProgramMetadata oldProgram = getSingle(new RadioProgramMetadata(originalID));
	
	if (newProgram == null || oldProgram == null){
	    LogUtilities.getDefaultLogger().error("UpdateImportedFile: Null programs: new: "+newProgram+ " old: "+oldProgram);
	    return false;
	}

	RadioProgramMetadata updatedProgram = newProgram.clone();
	updatedProgram.setTitle(oldProgram.getTitle());
	updatedProgram.setLanguage(oldProgram.getLanguage());
	updatedProgram.setDescription(oldProgram.getDescription());
	String type = "";
	if (oldProgram.getType().equals(RadioProgramMetadata.TELEPHONY_TYPE))
	    type = RadioProgramMetadata.IMPORTED_TELEPHONY_TYPE;
	else
	    type = oldProgram.getType();
	updatedProgram.setType(type);

	if (update(newProgram, updatedProgram) < 0){
	    LogUtilities.getDefaultLogger().error("UpdateImportedFile: Unable to update: "+newProgram);
	    return false;
	}
	
	for (Creator creator: get(Creator.getDummyObject(originalID))){
	    creator.setItemID(programID);
	    insert(creator);
	    LogUtilities.getDefaultLogger().debug("UpdateImportedFile: added creator: "+creator);
	}

	for (ItemCategory category: get(ItemCategory.getDummyObject(originalID))){
	    category.setItemID(programID);
	    insert(category);
	    LogUtilities.getDefaultLogger().debug("UpdateImportedFile: added category: "+category);
	}

	for (Tags tag: get(Tags.getDummyObject(originalID))){
	    tag.setItemID(programID);
	    insert(tag);
	    LogUtilities.getDefaultLogger().debug("UpdateImportedFile: added tag: "+tag);
	}
	
	return true;
    }


    public String getStreamUrl(String filename) {

	return "";
    }

    public String getDownloadUrl(String filename) {

	return "";
    }

    private Hashtable<String, Object> getColumnNamesAndValues(Metadata metadata) {
	Hashtable<String, Object> columnNamesAndValues = null;
	try {
	    columnNamesAndValues = Metadata.getColumnNamesAndValues(metadata);
	} catch(Exception e) {
	    logger.error("GetColumnNamesAndValues: Unable to parse metadata object.", e);
	    columnNamesAndValues = new Hashtable<String, Object>();
	}
	logger.debug("GetColumnNamesAndValues: Returning " + columnNamesAndValues.size() + " Column names and values");
	return columnNamesAndValues;
    }

    private Hashtable<String, Metadata[]> getSupplementaryColumnAndValues(Metadata metadata) {
	Hashtable<String, Metadata[]> supplementaryColumnAndValues = null;
	try {
	    supplementaryColumnAndValues = Metadata.getSupplementaryColumnAndValues(metadata);
	} catch(Exception e) {
	    logger.error("GetSupplementaryColumnAndValues: Unable to parse metadata object.", e);
	    supplementaryColumnAndValues = new Hashtable<String, Metadata[]>();
	}
	logger.debug("GetSupplementaryColumnAndValues: Returning " + supplementaryColumnAndValues.size() + " values");
	return supplementaryColumnAndValues;
    }

    private String[] getColumnNames(Metadata metadata) {

	String[] columnNames = null;
	try {
	    columnNames = Metadata.getColumnNames(metadata);
	} catch(Exception e) {
	    logger.error("GetColumnNames: Unable to parse metadata object.", e);
	    columnNames = new String[]{};
	}
	logger.debug("GetColumnNames: Returning " + columnNames.length + " columns.");
	return columnNames;
    }

    private Field[] getNestedFields(Metadata metadata) {
	Field[] nestedFields;
	try {
	    nestedFields = Metadata.getNestedFields(metadata);
	} catch(Exception e) {
	    logger.error("GetNestedFields: Unable to parse metadata object.", e);
	    nestedFields = new Field[]{};
	}
	logger.debug("GetNestedFields: Returning " + nestedFields.length + " nested fields.");
	return nestedFields;
    }

    private String getWhereClause(Metadata metadata) {
	String whereClauseStr =null;
	try {
	    whereClauseStr = Metadata.getWhereClause(metadata);
	} catch(Exception e) {
	    whereClauseStr = "";
	    logger.error("GetWhereClause: Unable to parse metadata object", e);
	}
	logger.debug("GetWhereClause: Returning '"+whereClauseStr+"'");
	return whereClauseStr;
    }

    public static String getTableName(Metadata metadata) {
	String tableName = metadata.getClass().getName();
	tableName = tableName.substring(tableName.lastIndexOf(".") + 1);
	return tableName;
    }

    private void initField(Metadata metadata, String fieldName, ResultSet rs) throws Exception {
	Object obj = rs.getObject(fieldName);
	Field field = metadata.getClass().getDeclaredField(fieldName);
	if (obj instanceof String) {
	    field.set(metadata, (String)obj);
	} else if (obj instanceof Integer) {
	    field.set(metadata, (Integer)obj);
	} else if (obj instanceof java.math.BigInteger || obj instanceof Long) {
	    field.set(metadata, new Long(obj.toString()));
	} else if (obj == null) {
	    if (field.getType() == String.class) {
		field.set(metadata, "");
	    } else if (field.getType() == Integer.class) {
		field.set(metadata, new Integer(-1));
	    } else if (field.getType() == Long.class) {
		field.set(metadata, new Long(-1));
	    } else {
		field.set(metadata, null);
	    }
	} else {
	    field.set(metadata, obj);
	}
    }
    
    private Object initJavaObj(ResultSet rs, String fieldName) throws Exception {
	Object obj = rs.getObject(fieldName);
	if (obj instanceof String) {
	    return obj;
	} else if (obj instanceof Integer) {
	    return obj;
	} else if (obj instanceof java.math.BigInteger || obj instanceof Long) {
	    return new Long(obj.toString());
	} else if (obj == null) {
	    return null;
	}

	return obj;
    }

    public int insert(Metadata metadata){
	return insert(metadata, true);
    }

    public int insert(Metadata metadata, boolean invalidateCache) {
	if (!goAheadWithQuery())
	    return -1;
	
	int retVal = 0;

	logger.debug("Insert: Inserting "+metadata);

	Hashtable<String, Object> colNamesAndValues = getColumnNamesAndValues(metadata);
	StringBuilder columns = new StringBuilder();
	StringBuilder values = new StringBuilder();
	Enumeration<String> e = colNamesAndValues.keys();
	while (e.hasMoreElements()) {
	    String column = e.nextElement();
	    Object value = colNamesAndValues.get(column);
	    if (Metadata.nullAllowed(metadata, column) || value != null && Metadata.notNull(value)) {
		columns.append(column); columns.append(",");
		values.append("'"); values.append(trim(value.toString())); values.append("',");
	    }
	}
	String colString = "", valString = "";
	if (columns.length() > 0) {
	    colString = columns.substring(0, columns.length() - 1);
	    valString = values.substring(0, values.length() - 1);
	    String tableName = getTableName(metadata);
	    String query = "insert into " + tableName + " (" + colString + ") values (" + valString + ")";
	    try {
		PreparedStatement stmt = databaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		logger.debug("Insert: "+query);
		stmt.executeUpdate();
		ResultSet resultSet = stmt.getGeneratedKeys();
		if (resultSet.next()){
		    retVal = resultSet.getInt(1);
		}
		try {
		    Hashtable<String, Metadata[]> supplementaryData = getSupplementaryColumnAndValues(metadata);
		    Enumeration<String> en = supplementaryData.keys();
		    while (en.hasMoreElements()) {
			String scol = en.nextElement();
			Metadata[] svals = supplementaryData.get(scol);
			if (svals != null && svals.length > 0) {
			    for (Metadata sval: svals) {
				insert(sval, invalidateCache);
			    }
			}
		    }
		    if (getLibraryActivated() && invalidateCache)
			sendInvalidateCacheMessage(getTableName(metadata), LibService.CACHE_INSERT, new String[]{metadata.unique()});
		} catch(Exception se) {
		    logger.error("Insert: Unable to insert supplementary data. query: " + query, se);
		}
		
	    } catch(Exception ex) {
		logger.error("Insert: Unable to insert. query: " + query, ex);
		notifyException(ex);
		return -1;
	    }
	    logger.info("Insert: Inserted " + metadata.dump()); 
	} else {
	    logger.error("Insert: Unable to insert since no columns found.");
	}
	return retVal;
    }

    public int update(Metadata queryMetadata, Metadata updateMetadata){
	return update(queryMetadata, updateMetadata, true);
    }

    private String getFileStoreFieldUpdateString(String field, String value) {
	StringBuilder updateString = new StringBuilder();
	updateString.append(field);
	updateString.append("=IF (");
	updateString.append(field);
	updateString.append("= '");
	updateString.append(new Integer(FileStoreManager.FILE_DELETED).toString());
	updateString.append("', '");
	updateString.append(new Integer(FileStoreManager.FILE_DELETED).toString());
	updateString.append("', '");
	updateString.append(value);
	updateString.append("')");
	return updateString.toString();
    }

    // does not update any field to ""
    public int update(Metadata queryMetadata, Metadata updateMetadata, boolean invalidateCache) {
	if (!goAheadWithQuery())
	    return -1;

	logger.debug("Update: Updating "+queryMetadata+" with "+updateMetadata);

	String whereClause = getWhereClause(queryMetadata);
	Hashtable<String, Object> colNamesAndValues = getColumnNamesAndValues(updateMetadata);
	Enumeration<String> e = colNamesAndValues.keys();
	StringBuilder updateClause = new StringBuilder();
	while (e.hasMoreElements()) {
	    String column = e.nextElement();
	    Object value = colNamesAndValues.get(column);
	    if (Metadata.nullAllowed(updateMetadata, column) || value != null && Metadata.notNull(value)) {
		if (updateMetadata instanceof RadioProgramMetadata && ((RadioProgramMetadata)updateMetadata).isFileStoreField(column)) {
		    String updateString = getFileStoreFieldUpdateString(column, value.toString());
		    updateClause.append(updateString);
		    updateClause.append(",");
		} else {
		    updateClause.append(column); updateClause.append("='"); updateClause.append(value.toString()); 
		    updateClause.append("',");
		}
	    }
	}
	
	int res = -1;

	String updateClauseStr = "";
	String sqlCommand = null;
	if (updateClause.length() > 0) {
	    updateClauseStr = updateClause.substring(0, updateClause.length() - 1);

	    String tableName = getTableName(updateMetadata);
	    try {
		sqlCommand = "update " + tableName + " set " + updateClauseStr;
		if (whereClause.length() > 0) {
		    sqlCommand = sqlCommand + " where " + whereClause;
		}
		logger.debug("update: " + sqlCommand);
		PreparedStatement stmt = databaseConnection().prepareStatement(sqlCommand);
		res = stmt.executeUpdate();
		String queryUnique = queryMetadata.unique();
		String updateUnique = updateMetadata.unique();
		
		/*
		if (getLibraryActivated() && !queryUnique.equals(queryMetadata.uniqueNull()) && 
		   !updateUnique.equals(updateMetadata.uniqueNull()) && !queryUnique.equals(updateUnique))
		    sendInvalidateCacheKeyMessage(getTableName(updateMetadata), LibService.CACHE_UPDATE, 
						  new String[]{queryUnique}, new String[]{updateUnique});
		else if (getLibraryActivated())
		    sendInvalidateCacheMessage(getTableName(updateMetadata), LibService.CACHE_UPDATE, new String[]{queryUnique});
		*/
		
		if (getLibraryActivated() && invalidateCache)
		    sendInvalidateCacheMessage(getTableName(updateMetadata), LibService.CACHE_UPDATE, new String[]{queryUnique});
		
	    } catch(Exception ex) {
		logger.error("Update: Unable to update. query: "+sqlCommand, ex);
		notifyException(ex);
		return -1;
	    }
	}

	logger.info("Update: Updated " + res + " " + updateMetadata.dump());
	return 0;
    }

    public int remove(Metadata metadata){
	return remove(metadata, true);
    }

    public int remove(Metadata metadata, boolean invalidateCache) {
	if (!goAheadWithQuery())
	    return -1;

	String whereClause = getWhereClause(metadata);
	String tableName = getTableName(metadata);
	String sqlCommand = "delete from " + getTableName(metadata);
	if (whereClause.length() > 0) {
	    sqlCommand = sqlCommand + " where " + whereClause;
	}
	
	try {
	    PreparedStatement stmt = databaseConnection().prepareStatement(sqlCommand);
	    logger.debug("remove: " + sqlCommand);
	    stmt.executeUpdate();
	    if (getLibraryActivated() && invalidateCache)
		sendInvalidateCacheMessage(getTableName(metadata), LibService.CACHE_DELETE, new String[]{metadata.unique()});
	} catch(Exception e) {
	    logger.error("Remove: Unable to remove. query: " + sqlCommand, e);
	    notifyException(e);
	    return -1;
	}
	
	logger.info("Remove: Removed " + metadata.dump());
	return 0;
    }

    public Object[] unique(Metadata metadata, String fieldName) {
	if (!goAheadWithQuery())
	    return new Object[]{};

	String whereClause = getWhereClause(metadata);
	String tableName = getTableName(metadata);
	String sqlCommand = "select distinct(" + fieldName + ") from " + tableName;
	ArrayList<Object> arr = new ArrayList<Object>();
	if (whereClause.length() > 0) {
	    sqlCommand = sqlCommand + " where " + whereClause;
	}
	sqlCommand = sqlCommand + " order by " + fieldName;
	try {
	    logger.debug("unique: " + sqlCommand);
	    Statement stmt = databaseConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(sqlCommand);
	    while (rs.next()) {
		Object javaobj = initJavaObj(rs, fieldName);
		if (javaobj != null)
		    arr.add(javaobj);
	    }
	    Object[] retarr = arr.toArray(new Object[]{});
	    for (Object retarrobj: retarr) {
		logger.debug("unique: got " + retarrobj);
	    }
	    return retarr;
	} catch(Exception ex) {
	    logger.error("Unique: Unable to execute select. Query:" + sqlCommand, ex);
	    notifyException(ex);
	    return new Object[]{};
	}
    }

    public Object mathOp(Metadata metadata, String fieldName, String operation) {
	if (!goAheadWithQuery())
	    return new Object();

	String whereClause = getWhereClause(metadata);
	String tableName = getTableName(metadata);
	String sqlCommand = "select " + operation + "(" + fieldName + ") from " + tableName;
	if (whereClause.length() > 0) {
	    sqlCommand = sqlCommand + " where " + whereClause;
	}
	try {
	    logger.debug("mathOp - " + operation + ":" + sqlCommand);
	    Statement stmt = databaseConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(sqlCommand);
	    rs.next();
	    Object javaobj = initJavaObj(rs, operation + "(" + fieldName + ")");
	    if (javaobj != null)
		return javaobj;
	} catch(Exception ex) {
	    logger.error("MathOp: Unable to execute select. Query:" + sqlCommand, ex);
	    notifyException(ex);
	}
	return new Object();
    }

    public Object mathOp(String tableName, String whereClause, String fieldName, String operation) {
	if (!goAheadWithQuery())
	    return new Object();

	String sqlCommand = "select " + operation + "(" + fieldName + ") from " + tableName + (whereClause.equals("") ? "" : " where " + whereClause);
	try {
	    logger.debug("mathOp - " + operation + ":" + sqlCommand);
	    Statement stmt = databaseConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(sqlCommand);
	    rs.next();
	    Object javaobj = initJavaObj(rs, operation + "(" + fieldName + ")");
	    if (javaobj != null)
		return javaobj;
	} catch(Exception ex) {
	    logger.error("MathOp: Unable to execute select. Query:" + sqlCommand, ex);
	    notifyException(ex);
	}
	return new Object();
    }

    @SuppressWarnings("unchecked") 
    public <T extends Metadata> T[] getAll(T metadata, String[] sortField, String[] order, String[] between, int limit){
	Class<? extends Metadata> klass = metadata.getClass();
	
	if (!goAheadWithQuery())
	    return (T[]) java.lang.reflect.Array.newInstance(klass, 0);	    

	int numFields;
	int numOrders;

	numFields = (sortField == null)?-1:sortField.length;
	numOrders = (order == null)? -1:order.length;

	if (numFields != numOrders){
	    logger.error("GetAll: Number of sortFields and orders do not match: fields: "+numFields+" orders: "+numOrders);
	    return null;
	}

	String whereClause = getWhereClause(metadata);
	String tableName   = getTableName(metadata);
	String sqlCommand  = "select * from " + tableName;
	
	String betweenClause = "";

	if (between != null){
	    if (between.length % 3 != 0){
		logger.error("GetAll: Number of parameters in 'between' clause is incorrect.");
		return null;
	    }
	    for (int i = 0; i < between.length/3; i++){
		whereClause += " "+between[3*i]+" between "+between[3*i+1]+" and "+between[3*i+2];
	    }
	}

	if (whereClause.length() > 0) {
	    sqlCommand += " where " + whereClause;
	}
	
	if (numFields > 0){
	    String orderClause = " order by ";

	    for (int i = 0; i < numFields; i++)
		orderClause += sortField[i] + " " + order[i] + ", ";
	    orderClause = orderClause.substring(0, orderClause.length() - 2);
	    sqlCommand += orderClause;
	}

	if (limit > -1)
	    sqlCommand += " limit " + limit;

	try{
	    logger.debug("GetAll: "+sqlCommand);
	    Statement stmt = databaseConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(sqlCommand);
	    ArrayList<T> selectRes = new ArrayList<T>();
	    Field[] nestedFields = getNestedFields(metadata);

	    while (rs.next()){
		T newRow = (T) metadata.cloneObject();
		String[] colNames = getColumnNames (metadata);
		for (String column: colNames)
		    initField(newRow, column, rs);
		selectRes.add(newRow);

		for (Field nestedField: nestedFields) {
		    String localKeyName = (String)(newRow.getClass().getDeclaredField("local" + nestedField.getName()).get(null));
		    String foreignKeyName = (String)(newRow.getClass().getDeclaredField("foreign" + nestedField.getName()).get(null));
		    Object keyVal = newRow.getClass().getDeclaredField(localKeyName).get(newRow);
		    Metadata nestedFieldValue = null;
		    try{
			Method m = nestedField.getType().getMethod("getDummyObject", new Class[]{keyVal.getClass()});
			nestedFieldValue = (Metadata)(m.invoke(null, new Object[]{keyVal}));
		    }catch(Exception e){
			logger.error("GetAll: Unable to get all nested fields. "+newRow.dump());
		    }
		    nestedField.set(newRow, getSingle(nestedFieldValue));
		}
	    }

	    T[] retArr = selectRes.toArray((T[])java.lang.reflect.Array.newInstance(klass, selectRes.size()));
	    
	    for (Metadata retElem: retArr){
		logger.debug("GetAll: select "+retElem.dump());
	    }

	    return retArr;

	} catch (Exception e){
	    logger.error("GetAll: Unable to execute select. Query:" + sqlCommand, e);
	    notifyException(e);
	    return (T[]) java.lang.reflect.Array.newInstance(klass, 0);
	}
    }

    @SuppressWarnings("unchecked") 
	public <T extends Metadata> T[] get(T metadata, String whereClauseIn, String[] sortField, String[] order, String[] between, int... limits){

	Class<? extends Metadata> klass = metadata.getClass();

	if (!goAheadWithQuery())
	    return (T[]) java.lang.reflect.Array.newInstance(klass, 0);	    

	int numFields;
	int numOrders;

	numFields = (sortField == null)?-1:sortField.length;
	numOrders = (order == null)? -1:order.length;

	if (numFields != numOrders){
	    logger.error("Get: Number of sortFields and orders do not match: fields: "+numFields+" orders: "+numOrders);
	    return null;
	}

	String whereClause;
	if (whereClauseIn != null && whereClauseIn.length() > 0)
	    whereClause = whereClauseIn;
	else
	    whereClause = getWhereClause(metadata);

	String tableName   = getTableName(metadata);
	String sqlCommand  = "select * from " + tableName;
	
	String betweenClause = "";

	if (between != null){
	    if (between.length % 3 != 0){
		logger.error("Get: Number of parameters in 'between' clause is incorrect.");
		return null;
	    }
	    for (int i = 0; i < between.length/3; i++){
		whereClause += " "+between[3*i]+" between "+between[3*i+1]+" and "+between[3*i+2];
	    }
	}

	if (whereClause.length() > 0) {
	    sqlCommand += " where " + whereClause;
	}
	
	if (numFields > 0){
	    String orderClause = " order by ";

	    for (int i = 0; i < numFields; i++)
		orderClause += sortField[i] + " " + order[i] + ", ";
	    orderClause = orderClause.substring(0, orderClause.length() - 2);
	    sqlCommand += orderClause;
	}

	if (limits.length == 1 && limits[0] > -1){
	    sqlCommand += " limit " + limits[0];
	} else if (limits.length == 2){
	    sqlCommand += " limit " + limits[0] + "," + limits[1];
	}

	try{
	    logger.debug("Get: "+sqlCommand);
	    Statement stmt = databaseConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(sqlCommand);
	    ArrayList<T> selectRes = new ArrayList<T>();

	    while (rs.next()){
		T newRow = (T) metadata.cloneObject();
		String[] colNames = getColumnNames (metadata);
		for (String column: colNames)
		    initField(newRow, column, rs);
		selectRes.add(newRow);
	    }

	    T[] retArr = selectRes.toArray((T[])java.lang.reflect.Array.newInstance(klass, selectRes.size()));
	    
	    for (Metadata retElem: retArr){
		logger.debug("Get: select "+retElem.dump());
	    }
	    return retArr;
	} catch (Exception e){
	    logger.error("Get: Unable to execute select. Query:" + sqlCommand, e);
	    notifyException(e);
	    return (T[]) java.lang.reflect.Array.newInstance(klass, 0);
	}
    }

    @SuppressWarnings("unchecked")
    private <T extends Metadata> T[] get(T metadata, int limit) {
	Class<? extends Metadata> klass = metadata.getClass();

	if (!goAheadWithQuery())
	    return (T[]) java.lang.reflect.Array.newInstance(klass, 0);	    
	
	String whereClause = getWhereClause(metadata);
	String tableName = getTableName(metadata);
	String sqlCommand = "select * from " + getTableName(metadata);
	if (whereClause.length() > 0) {
	    sqlCommand = sqlCommand + " where " + whereClause;
	}
	if (limit > -1) {
	    sqlCommand = sqlCommand + " limit " + limit;
	}

	try {
	    logger.debug("get: " + sqlCommand);
	    Statement stmt = databaseConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(sqlCommand);
	    ArrayList<T> selectRes = new ArrayList<T>();
	    while (rs.next()) {
		T newRow = (T) metadata.cloneObject();
		String[] colNames = getColumnNames(metadata);
		for (String column: colNames) {
		    initField(newRow, column, rs);
		}
		selectRes.add(newRow);
	    }
	    T[] retArr = selectRes.toArray((T[])java.lang.reflect.Array.newInstance(klass, selectRes.size()));
	    for (Metadata retElem: retArr) {
		logger.debug("Get: select " + retElem.dump());	
	    }
	    return retArr;
	} catch(Exception ex) {
	    logger.error("Get: Unable to execute select. Query:" + sqlCommand, ex);
	    notifyException(ex);
	    return (T[]) java.lang.reflect.Array.newInstance(klass, 0);
	}
    }

    public <T extends Metadata> T[] get(T metadata) {
	return get(metadata, -1);
    }

    public <T extends Metadata> T getSingle(T metadata) {
	T[] results =  get(metadata, 1);
	if (results.length > 0) {
	    return results[0];
	} else {
	    return null;
	}
    }

    // assumes that getDummyObject will get called with the local foreign key, which will most likely also be the primary key
    @SuppressWarnings("unchecked")
    public <T extends Metadata> T[] getAll(T metadata) {
	T[] results = get(metadata, -1);
	Field[] nestedFields = getNestedFields(metadata);
	try {
	    for (Metadata result: results) {
		for (Field nestedField: nestedFields) {
		    String localKeyName = (String)(result.getClass().getDeclaredField("local" + nestedField.getName()).get(null));
		    String foreignKeyName = (String)(result.getClass().getDeclaredField("foreign" + nestedField.getName()).get(null));
		    Object keyVal = result.getClass().getDeclaredField(localKeyName).get(result);

		    Method m = nestedField.getType().getMethod("getDummyObject", new Class[]{keyVal.getClass()});
		    Metadata nestedFieldValue = (Metadata)(m.invoke(null, new Object[]{keyVal}));
		    nestedField.set(result, getSingle(nestedFieldValue));
		}
	    }
	} catch(Exception ex) {
	    logger.error("GetAll: Unable to get all nested fields.", ex);
	}
	logger.debug("GetAll: Returning "+results.length+" results");
	return results;
    }

    @SuppressWarnings("unchecked")
    public <T extends Metadata> T[] selectAllFields(T metadata, String whereClause, int... limits) {
	Class<? extends Metadata> klass = metadata.getClass();

	if (!goAheadWithQuery())
	    return (T[]) java.lang.reflect.Array.newInstance(klass, 0);	    

	String tableName = getTableName(metadata);
	String sqlCommand = "select * from " + tableName;
	if (whereClause != null && !whereClause.equals(""))
	    sqlCommand += " where " + whereClause;
	
	/*
	if (limit > -1)
	    sqlCommand = sqlCommand + " limit " + limit;
	*/

	if (limits.length == 1 && limits[0] > -1){
	    sqlCommand = sqlCommand + " limit " + limits[0];
	} else if (limits.length == 2){
	    sqlCommand = sqlCommand + " limit " + limits[0] + "," + limits[1];
	}

	try {
	    logger.debug("SelectAllFields: " + sqlCommand);
	    Statement stmt = databaseConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(sqlCommand);
	    ArrayList<T> selectRes = new ArrayList<T>();

	    while (rs.next()){
		T newRow = (T) metadata.cloneObject();
		String[] colNames = getColumnNames (metadata);
		for (String column: colNames)
		    initField(newRow, column, rs);
		selectRes.add(newRow);
	    }

	    T[] retArr = selectRes.toArray((T[])java.lang.reflect.Array.newInstance(klass, selectRes.size()));
	    
	    for (Metadata retElem: retArr){
		logger.debug("Get: select "+retElem.dump());
	    }
	    return retArr;
	} catch (Exception e){
	    logger.error("Get: Unable to execute select with query:" + sqlCommand, e);
	    notifyException(e);
	    return (T[]) java.lang.reflect.Array.newInstance(klass, 0);
	}
    }

    public Object[] select(String joinTableNames, String whereClause, String fieldName, int limit) {
	if (!goAheadWithQuery())
	    return new Object[]{};

	String sqlCommand = "select distinct(" + fieldName + ") from " + joinTableNames + " where " + whereClause;

	//System.err.println("------------: "+ sqlCommand);

	if (limit > -1)
	    sqlCommand = sqlCommand + " limit " + limit;
	try {
	    logger.debug("select: " + sqlCommand);
	    Statement stmt = databaseConnection().createStatement();
	    ResultSet rs = stmt.executeQuery(sqlCommand);
	    ArrayList<Object> arr = new ArrayList<Object>();
	    while (rs.next()) {
		Object javaobj = initJavaObj(rs, fieldName.substring(fieldName.indexOf(".") + 1));
		if (javaobj != null)
		    arr.add(javaobj);
	    }
	    Object[] retarr = arr.toArray(new Object[]{});
	    for (Object retarrobj: retarr) {
		logger.debug("select: got " + retarrobj);
	    }
	    return retarr;
	} catch(Exception ex) {
	    logger.error("Get: Unable to execute select. Query:" + sqlCommand, ex);
	    notifyException(ex);
	    return new Object[]{};
	}
    }

    public ArrayList<Metadata> getSubFields(Metadata metadata) {
	Field[] nestedFields = getNestedFields(metadata);
	ArrayList<Metadata> arr = new ArrayList<Metadata>();
	for (Field nestedField: nestedFields) {
	    try {
		Object val = nestedField.get(metadata);
		if (val != null && val instanceof Metadata)
		    arr.add((Metadata)(val));
	    } catch(Exception e) {
		logger.error("getSubFields: Unable to get all fields", e);
	    }
	}
	return arr;
    }

    public ArrayList<Metadata[]> getAssocFields(Metadata metadata) {
	Field[] assocFields = null;
	ArrayList<Metadata[]> arr = new ArrayList<Metadata[]>();
	try {
	    assocFields = Metadata.getAssocFields(metadata);
	} catch(Exception e) {
	    logger.error("getAssocFields: Unable to get associated fields from metadata", e);
	    return arr;
	}

	for (Field assocField: assocFields) {
	    Object val;
	    try {
		if ((val = assocField.get(metadata)) instanceof Metadata[])
		    arr.add((Metadata[])(val));
	    } catch(Exception e) {
		logger.error("getAssocFields: Unable to get all fields", e);
	    }
	}
	return arr;
    }

    // not tested, and probably not required, because will have to do some specific stuff for fetching Category hierarchies
    public Metadata[] getAllRecurse(Metadata metadata) {
	Metadata[] results = get(metadata, -1);
	Field[] nestedFields = getNestedFields(metadata);
	try {
	    for (Metadata result: results) {
		for (Field nestedField: nestedFields) {
		    String localKeyName = (String)(result.getClass().getDeclaredField("local" + nestedField.getName()).get(null));
		    String foreignKeyName = (String)(result.getClass().getDeclaredField("foreign" + nestedField.getName()).get(null));
		    Object keyVal = result.getClass().getDeclaredField(localKeyName).get(result);

		    if (Metadata.nullAllowed(result, nestedField.getName()) || keyVal != null && Metadata.notNull(keyVal)) {
			Method m = nestedField.getType().getMethod("getDummyObject", new Class[]{keyVal.getClass()});
			Metadata nestedFieldValue = (Metadata)(m.invoke(null, new Object[]{keyVal}));
			
			Metadata[] nestedFieldPopulatedValue = getAllRecurse(nestedFieldValue);
			if (nestedFieldPopulatedValue != null && nestedFieldPopulatedValue.length > 0)
			    nestedField.set(result, nestedFieldPopulatedValue[0]);
			else
			    nestedField.set(result, null);
		    }
		}
	    }
	} catch(Exception ex) {
	    logger.error("GetAll: Unable to get all nested fields.", ex);
	}
	return results;
    }

    protected void sendInvalidateCacheMessage(String objectType, String actionType, String[] objectIDs) {
	if (ipcServer != null) {
	    if (!objectType.equals("HotKey") && !objectType.equals("Playlist") && !objectType.equals("PlaylistItem") &&
	       !objectType.equals("EncodedFiles")) {
		InvalidateCacheMessage invalidateMessage = new InvalidateCacheMessage(RSController.ANON_SERVICE, 
										      RSController.LIB_SERVICE,
										      new String[]{}, 
										      objectType, actionType, objectIDs);
		int retVal;
		if (ipcServer != null) {
		    retVal = ipcServer.handleOutgoingMessage(invalidateMessage);
		} else {
		    retVal = -1;
		}
		if (retVal == -1) {
		    logger.error("SendInvalidateCacheMessage: Unable to send message " + objectType + ":" + actionType);
		} else if (retVal == -2) {
		    logger.error("SendInvalidateCacheMessage: Unable to send message " + objectType + ":" + actionType);
		} else {
		    logger.info("SendInvalidateCacheMessage: Message sent " + objectType + ":" + actionType);
		}
	    } else {
		logger.info("SendInvalidateCacheMessage: Not sending the message.");
	    }
	} else {
	    logger.error("SendInvalidateCacheMessage: Unable to send message because ipcServer not set");
	}
    }

    public void sendInvalidateCacheKeyMessage(String objectType, String actionType, String[] oldObjectIDs, String[] newObjectIDs) {
	sendInvalidateCacheKeyMessage(objectType, actionType, oldObjectIDs, newObjectIDs, RSController.ANON_SERVICE);
    }

    public void sendInvalidateCacheKeyMessage(String objectType, String actionType, String[] oldObjectIDs, String[] newObjectIDs, String source) {
	if (ipcServer != null) {
	    if (!objectType.equals("HotKey") && !objectType.equals("Playlist") && !objectType.equals("PlaylistItem") &&
	       !objectType.equals("EncodedFiles")) {
		InvalidateCacheKeyMessage invalidateMessage = new InvalidateCacheKeyMessage(source, 
											    RSController.LIB_SERVICE,
											    new String[]{}, 
											    objectType, actionType, oldObjectIDs, newObjectIDs);
		int retVal = ipcServer.handleOutgoingMessage(invalidateMessage);
		if (retVal == -1) {
		    logger.error("SendInvalidateCacheKeyMessage: Unable to send message " + objectType + ":" + actionType);
		} else if (retVal == -2) {
		    logger.error("SendInvalidateCacheKeyMessage: Unable to send message " + objectType + ":" + actionType);
		} else {
		    logger.info("SendInvalidateCacheKeyMessage: Message sent " + objectType + ":" + actionType);
		}
	    }
	} else {
	    logger.error("SendInvalidateCacheKeyMessage: Unable to send message because ipcServer not set");
	}
    }
    
    private static IPCServer staticIPCServer = null;
    public static synchronized void setIPCServer(IPCServer ipc) {
	if (mediaLib != null)
	    mediaLib.ipcServer = ipc;

	staticIPCServer = ipc;
    }

    private static MediaLib mediaLib = null;
    public  synchronized static MediaLib getMediaLib(StationConfiguration stationConfig, TimeKeeper timeKeeper) {
	if (mediaLib == null) {
	    mediaLib = new MediaLib(stationConfig, timeKeeper);
	}
	
	if (mediaLib.goAheadWithQuery()) {
	    if (mediaLib.connect() > -1) {
		return mediaLib;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    private String trim(String str) {
	str = str.replace("'", "\'");
	return str;
    }

    public void updateFileMetadata(RadioProgramMetadata program, Hashtable<String, String> tags){
	String description = "";
	if (tags.get("artist") != null && tags.get("artist").length() > 0)
	    description += "artist: " + tags.get("artist") + "\n";
	if (tags.get("album") != null && tags.get("album").length() > 0)
	    description += "album: " + tags.get("album") + "\n";
	if (tags.get("comment") != null && tags.get("comment").length() > 0)
	    description += "comment: " + tags.get("comment") + "\n";
	if (tags.get("genre") != null && tags.get("genre").length() > 0)
	    description += "genre: " + tags.get("genre") + "\n";
	
	String title = null;

	if (shouldAssignTitle(tags.get("title")))
	    title = tags.get("title");

	if (description.length() > 0)
	    program.setDescription(StringUtilities.sanitize(description.trim()));
	
	if (title != null && title.length() > 0)
	    program.setTitle(StringUtilities.sanitize(title));

	program.setLength(Long.parseLong(tags.get(RSPipeline.DURATION)));
    }
    
    boolean shouldAssignTitle(String titleCandidate) {
	//ArrayList<String> rejectPatterns = new ArrayList<String>();
	
	//rejectPatterns.add("\s*Track\s*\d

	if(titleCandidate == null)
	    return false;
	if(titleCandidate.length() == 0)
	    return false;
	
	return true;
    }
}
