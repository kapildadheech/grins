package org.gramvaani.radio.medialib;

import org.gramvaani.utilities.*;

import java.util.*;

public class PlayoutHistory extends Metadata {
    protected String _itemID;
    protected String _stationName;
    protected Long _telecastTime;

    protected RadioProgramMetadata _program; protected static String foreign_program = "_itemID", local_program="_itemID";

    public String dump() {
	return "PlayoutHistory: " + _itemID + ":" + _stationName + ":" + _telecastTime;
    }

    public PlayoutHistory(String itemID, String stationName, long telecastTime) {
	this._itemID = itemID;
	this._stationName = stationName;
	this._telecastTime = new Long(telecastTime);
    }

    public PlayoutHistory(String playoutHistoryString) {
	try {
	    String[] splitStrs = playoutHistoryString.split(":");
	    _itemID = splitStrs[0];
	    _stationName = splitStrs[1];
	    _telecastTime = new Long(splitStrs[2]);
	} catch(Exception e) {
	    LogUtilities.getDefaultLogger().error("Unable to parse playoutHistory string", e);
	    _itemID = "";
	    _stationName = "";
	    _telecastTime = new Long(-1);
	}
    }

    public PlayoutHistory() {
	_itemID = "";
	_stationName = "";
	_telecastTime = new Long(-1);
    }

    public void setItemID(String itemID) {
	this._itemID = itemID;
    }

    public String getItemID() {
	return _itemID;
    }

    public static String getItemIDFieldName() {
	return "_itemID";
    }
    
    public String getStationName() {
	return _stationName;
    }

    public long getTelecastTime() {
	return _telecastTime.longValue();
    }

    public static String getTelecastTimeFieldName() {
	return "_telecastTime";
    }

    public static PlayoutHistory getDummyObject(String itemID) {
	PlayoutHistory playoutHistory = new PlayoutHistory();
	playoutHistory.setItemID(itemID);
	return playoutHistory;
    }

    public void setProgram(RadioProgramMetadata program){
	_program = program;
    }

    public RadioProgramMetadata getProgram(){
	return _program;
    }

    public String toString() {
	String retStr = _itemID + ":" + _stationName + ":" + _telecastTime;
	return retStr;
    }

    public String unique() {
	return _itemID + ":" + _telecastTime.toString();
    }

    public String uniqueNull() {
	return ":-1";
    }

    public PlayoutHistory cloneObject() {
	return getDummyObject("");
    }

    public static String getTelecastTimeField(){
	return "_telecastTime";
    }

    public void truncateFields(){

    }
}
