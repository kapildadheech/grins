package org.gramvaani.radio.medialib;

public class SearchResult {
    SearchFilter[] displayFilters;
    SearchFilter[] activeFilters;
    RadioProgramMetadata[] programs;
    int numSearchResults;

    public SearchResult(SearchFilter[] displayFilters, SearchFilter[] activeFilters, RadioProgramMetadata[] programs,
			int numSearchResults) {
	this.displayFilters = displayFilters;
	this.activeFilters = activeFilters;
	this.programs = programs;
	this.numSearchResults = numSearchResults;
    }

    public SearchFilter[] getDisplayFilters() {
	return displayFilters;
    }

    public SearchFilter[] getActiveFilters() {
	return activeFilters;
    }

    public RadioProgramMetadata[] getPrograms() {
	return programs;
    }

    public int numSearchResults() {
	return numSearchResults;
    }

    public void setDisplayFilters(SearchFilter[] displayFilters) {
	this.displayFilters = displayFilters;
    }

    public void setActiveFilters(SearchFilter[] activeFilters) {
	this.activeFilters = activeFilters;
    }

    public void setPrograms(RadioProgramMetadata[] programs) {
	this.programs = programs;
    }

    public void setNumSearchResults(int numSearchResults) {
	this.numSearchResults = numSearchResults;
    }

}
