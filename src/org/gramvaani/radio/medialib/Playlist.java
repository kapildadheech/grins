package org.gramvaani.radio.medialib;

import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.gui.SimulationWidget;
import org.gramvaani.radio.app.gui.PlaylistWidget;

import java.util.*;

public class Playlist extends Metadata implements Comparable<Playlist> {
    protected String _name;
    protected Integer _playlistID;
    protected Long _startTime;
    
    protected PlaylistItem[] __playlistItems;

    Hashtable<String, RadioProgramMetadata> programs;
    ArrayList<String> ordering;

    public Playlist(int playlistID) {
	_name = "";
	_playlistID = new Integer(playlistID);
	_startTime = new Long(-1);

	__playlistItems = null;

	programs = new Hashtable<String, RadioProgramMetadata>();
	ordering = new ArrayList<String>();
    }
    
    public String dump() {
	return "Playlist: " + _name + ":" + _playlistID + ":" + _startTime;
    }

    public void dummyInit() {
	_name = "dummy playlist";
	_playlistID =  new Integer(-1);
	_startTime = new Long(-1);
	__playlistItems = new PlaylistItem[10];
	for(int i = 0; i < 10; i++) {
	    String programID = "test" + i + ".flac";
	    RadioProgramMetadata metadata = new RadioProgramMetadata(programID);
	    __playlistItems[i] = new PlaylistItem(_playlistID.intValue(), programID, i);
	    metadata.setTitle(metadata.getItemID());
	    metadata.setLength(10000);
	    programs.put(metadata.getItemID(), metadata);
	    ordering.add(metadata.getItemID());
	}
    }

    public int getPlaylistID() {
	return _playlistID.intValue();
    }

    public void setPlaylistID(int playlistID) {
	_playlistID = new Integer(playlistID);
    }

    public String getName() {
	return _name;
    }

    public void setName(String name) {
	_name = name;
    }

    public long getStartTime() {
	return _startTime.longValue();
    }

    public void setStartTime(long startTime) {
	_startTime = new Long(startTime);
    }

    public String primaryKey() {
	return "_playlistID";
    }

    public String unique() {
	return "_name";
    }
    
    public static Playlist getDummyObject(int playlistID) {
	return new Playlist(playlistID);
    }

    public Playlist cloneObject() {
	return getDummyObject(-1);
    }

    public void clear__playlistItems() {
	__playlistItems = null;
    }

    public PlaylistItem[] populate__playlistItems(MediaLib medialib, CacheProvider cacheProvider) {
	if (__playlistItems != null)
	    return __playlistItems;

	String dummyString = "dummystring";
	PlaylistItem dummyItem = PlaylistItem.getDummyObject(-1);

	PlaylistItem playlistItemQuery = PlaylistItem.getDummyObject(_playlistID);
	PlaylistItem[] playlistItems = medialib.get(playlistItemQuery);
	
	
	ArrayList<PlaylistItem> itemsArr = new ArrayList<PlaylistItem>();

	for (PlaylistItem playlistItem: playlistItems) {
	    String programID = playlistItem.getItemID();
	    RadioProgramMetadata prog;
	    if (!programID.startsWith(SimulationWidget.GOLIVE_ITEMID) 
		&& !programID.startsWith(PlaylistWidget.GOLIVE_ITEMID)) {

		prog = cacheProvider.getSingle(RadioProgramMetadata.getDummyObject(programID));
	    } else {
		prog = RadioProgramMetadata.getDummyObject(programID);
		prog.setTitle("Go live!");
		cacheProvider.setMetadataCacheObject(prog);
	    }

	    if (prog != null) {
		programs.put(programID, prog);
		itemsArr.add(playlistItem);
	    } 
	}

	Collections.sort(itemsArr, new Comparator<PlaylistItem>(){
		public int compare(PlaylistItem p1, PlaylistItem p2){
		    return p1.getOrder() - p2.getOrder();
		}

		public boolean equals(Object obj){
		    return obj.equals(this);
		}
	    });

	ordering = new ArrayList<String>();

	for (PlaylistItem item: itemsArr)
	    ordering.add(item.getItemID());

	__playlistItems = itemsArr.toArray(new PlaylistItem[itemsArr.size()]);

	for (int i = 0; i < __playlistItems.length; i++)
	    __playlistItems[i].setOrder(i);
	
	return __playlistItems;
    }

    public RadioProgramMetadata getProgram(String programID) {
	return programs.get(programID);
    }

    public String[] getPrograms() {
	return ordering.toArray(new String[]{});
    }

    public PlaylistItem[] getPlaylistItems() {
	return __playlistItems;
    }

    // XXX is it necessary to call this
    public void addPrograms(ArrayList<RadioProgramMetadata> programList){
	for (RadioProgramMetadata metadata: programList){
	    programs.put(metadata.getItemID(), metadata);
	    ordering.add(metadata.getItemID());
	}
    }

    public int size(){
	return ordering.size();
    }

    public String toString() {
	return _name;
    }

    public int compareTo(Playlist p){
	return _name.compareTo(p.getName());
    }

    public void truncateFields(){

    }
}