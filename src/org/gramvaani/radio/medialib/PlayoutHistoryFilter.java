package org.gramvaani.radio.medialib;

public class PlayoutHistoryFilter extends SearchFilter {
    public long telecastStartTime, telecastEndTime;

    public static final String PLAYOUTHISTORY_ANCHOR = "PLAYOUTHISTORY_ANCHOR"; 
    boolean playoutHistoryActive = false;
    public static final String telecastKey = Metadata.getClassName(PlayoutHistory.class) + "." + PlayoutHistory.getTelecastTimeFieldName();

    public PlayoutHistoryFilter() {
	this.telecastStartTime = -1;
	this.telecastEndTime = -1;
    }

    public String getFilterName() {
	return SearchFilter.PLAYOUTHISTORY_FILTER;
    }

    public String getFilterLabel() {
	return "Playout history";
    }

    public String getFilterQueryClause() {
	return "";
    }

    public String getFilterWhereClause() {
	StringBuilder str = new StringBuilder();
	if (telecastStartTime != -1 && telecastEndTime != -1 && playoutHistoryActive) {
	    str.append(telecastKey); str.append(" > "); 
	    str.append(telecastStartTime); str.append(" and ");
	    str.append(telecastKey); str.append(" < "); str.append(telecastEndTime);
	    str.append(" and ");
	}
	str.append(Metadata.getClassName(PlayoutHistory.class)); str.append("."); str.append("_itemID = ");
	str.append(Metadata.getClassName(RadioProgramMetadata.class)); str.append("."); str.append(RadioProgramMetadata.getItemIDFieldName());
	
	return str.toString();
    }

    public String[] getFilterJoinTables() {
	return new String[]{Metadata.getClassName(PlayoutHistory.class), Metadata.getClassName(RadioProgramMetadata.class)};
    }

    public String getSelectedText(String anchorID) {
	return "";
    }

    public void setStartTime(long startTime) {
	this.telecastStartTime = startTime;
    }

    public long getStartTime() {
	return telecastStartTime;
    }

    public void setEndTime(long endTime) {
	this.telecastEndTime = endTime;
    }

    public long getEndTime() {
	return telecastEndTime;
    }

    public void activateAnchor(String anchor, String anchorText) {
	if(anchor.equals(PLAYOUTHISTORY_ANCHOR)) {
	    playoutHistoryActive = true;
	}
    }

    public RadioProgramMetadata[] filterResults(RadioProgramMetadata[] programs) {
	return programs;
    }

    public static long getMinTelecastTime(MediaLib medialib) {
	Object minTelecastTime = medialib.mathOp(new PlayoutHistory(), PlayoutHistory.getTelecastTimeFieldName(), MediaLib.MIN_OP);
	if(minTelecastTime instanceof Long) {
	    return ((Long)minTelecastTime).longValue();
	} else {
	    return -1;
	}
    }

    public static long getMaxTelecastTime(MediaLib medialib) {
	Object maxTelecastTime = medialib.mathOp(new PlayoutHistory(), PlayoutHistory.getTelecastTimeFieldName(), MediaLib.MAX_OP);
	if(maxTelecastTime instanceof Long) {
	    return ((Long)maxTelecastTime).longValue();
	} else {
	    return -1;
	}
    }

}
