package org.gramvaani.radio.upgrader.upgrader1;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.util.ArrayList;

public class UniqueMatchBeforeSelector implements PositionSelector {
    Pattern pattern;

    public UniqueMatchBeforeSelector(String pattern){
	this.pattern = Pattern.compile(pattern);
    }

    public UniqueMatchBeforeSelector(Pattern pattern){
	this.pattern = pattern;
    }

    public int[][] findPositions(ArrayList<String> lines) throws Exception{

	int[][] position = null;

	for (int i = 0; i < lines.size(); i++){
	    Matcher matcher = pattern.matcher(lines.get(i));
	    if (matcher.find()){
		if (position != null)
		    throw new Exception("Multiple matches for unique selector:"+pattern);
		
		position = new int[][] {{i, matcher.start()}};
	    }
	}
	return position;
    }
}
