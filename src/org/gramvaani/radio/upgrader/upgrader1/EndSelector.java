package org.gramvaani.radio.upgrader.upgrader1;

import java.util.ArrayList;

public class EndSelector implements PositionSelector {
    public int[][] findPositions(ArrayList<String> lines) throws Exception{
	return new int [][] {{lines.size() - 1, lines.get(lines.size() - 1).length()}};
    }
}
