package org.gramvaani.radio.upgrader.upgrader1;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.util.ArrayList;

public class MatchAfterSelector implements PositionSelector {
    Pattern pattern;

    public MatchAfterSelector(String pattern){
	this.pattern = Pattern.compile(pattern);
    }

    public MatchAfterSelector(Pattern pattern){
	this.pattern = pattern;
    }

    public int[][] findPositions(ArrayList<String> lines) throws Exception{

	ArrayList<int[]> positions = new ArrayList<int[]>();
	
	for (int i = 0; i < lines.size(); i++){
	    Matcher matcher = pattern.matcher(lines.get(i));
	    if (matcher.find()){
		positions.add(new int[] {i, matcher.end()});
	    }
	}
	return positions.toArray(new int[0][0]);
    }
}
