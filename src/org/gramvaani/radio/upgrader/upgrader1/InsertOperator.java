package org.gramvaani.radio.upgrader.upgrader1;

import java.util.ArrayList;

public class InsertOperator extends EditOperator {
    String text;
    int mode;

    public static final int IN_LINE = 0;
    public static final int END_OF_LINE = 1;
    public static final int BEGINNING_OF_LINE = 2;

    public InsertOperator(String text){
	this(text, IN_LINE);
    }

    public InsertOperator(String text, int mode){
	this.text = text;
	this.mode = mode;
    }

    public void onEdit(ArrayList<String> lines, int positions[][]) throws Exception{
	for (int[] position: positions){
	    String srcLine = lines.get(position[0]);
	    String newLine;

	    switch(mode){

	    case IN_LINE:
		String begin = srcLine.substring(0, position[1]);
		String end = srcLine.substring(position[1]);
		newLine = begin + text + end;
		break;

	    case BEGINNING_OF_LINE:
		newLine = text + srcLine;
		break;

	    case END_OF_LINE:
		newLine = srcLine + text;
		break;

	    default:
		throw new Exception("Unknown mode: "+mode);
	    }

	    lines.set(position[0], newLine);
	}

	System.err.println(lines);
    }
}
