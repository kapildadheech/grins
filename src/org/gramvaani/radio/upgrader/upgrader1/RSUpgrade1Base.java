package org.gramvaani.radio.upgrader.upgrader1;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.upgrader.UpgradeLoader;

import org.gramvaani.utilities.FileUtilities;

import java.util.*;
import java.io.*;

public abstract class RSUpgrade1Base implements RSUpgrade1 {
    
    protected String upgradeDirName;
    
    protected String srcBinVersion, destBinVersion, srcDBVersion, destDBVersion;
    
    protected StationConfiguration stationConfig;
    protected File upgradeDir;
    protected File upgraderDir;
    protected File backupDir, tmpDir;
    
    protected String binDir, backupDirPath, tmpDirPath;

    protected String dbHost, dbPort, dbLogin, dbPassword, dbName; 
    protected boolean shouldUpdateDB = false, dbBackedUp = false;

    protected static final String TMP_DIR_NAME  = "GV_UPGRADE_TMP";
    protected static final String BACKUP_DIR_NAME= "backup";

    protected static final String EXEC_MYSQLDUMP= "mysqldump";
    protected static final String EXEC_MYSQL	= "mysql";
    protected static final String DB_BACKUP_NAME= "GV_DB_BACKUP.sql";


    Hashtable<String, HashSet<String>> backedupFiles = new Hashtable<String, HashSet<String>>();
    Hashtable<String, ArrayList<String>> loadedFiles = new Hashtable<String, ArrayList<String>>();
    Hashtable<KnownFile, String[]> knownFiles = new Hashtable<KnownFile, String[]>();
    Hashtable<KnownDir, String[]> knownDirs = new Hashtable<KnownDir, String[]>();

    public RSUpgrade1Base(String dirName, StationConfiguration stationConfig){
	upgradeDirName = dirName;
	upgradeDir = new File(upgradeDirName);
	this.stationConfig = stationConfig;
    }

    public void setBinDir(String dir){
	binDir = dir;
    }

    public void init(){
	backupDirPath = binDir + "/" + BACKUP_DIR_NAME + "/" + this.toString();
	
	backupDir = new File(backupDirPath);

	if (!backupDir.exists()){
	    System.err.println("Creating backup dir: "+backupDir);
	    backupDir.mkdirs();
	}

	tmpDirPath = backupDirPath + "/" + TMP_DIR_NAME;
	
	tmpDir = new File(tmpDirPath);
	
	if (!tmpDir.exists()){
	    System.err.println("Creating tmp dir: "+tmpDir);
	    tmpDir.mkdirs();
	}
	
	String dbServer = stationConfig.getStringParam(StationConfiguration.LIB_DB_SERVER);
	dbHost = dbServer.split(":")[0];
	dbPort = dbServer.split(":")[1];

	dbName = stationConfig.getStringParam(StationConfiguration.LIB_DB_NAME);
	dbLogin  = stationConfig.getStringParam(StationConfiguration.LIB_DB_LOGIN);
	dbPassword= stationConfig.getStringParam(StationConfiguration.LIB_DB_PASSWORD);

	if (stationConfig.getLocalMachineID().equals(StationConfiguration.RSCONTROLLER_MACHINE))
	    shouldUpdateDB = true;

	initKnownFiles();
    }

    void initKnownFiles(){
	knownFiles.put(KnownFile.AUTOMATION_CONF, new String[]{binDir, "/tmp"});
	knownDirs.put(KnownDir.CLASSES, new String[]{binDir +"/classes"});
	knownDirs.put(KnownDir.BIN, new String[]{binDir, "/tmp"});
	knownDirs.put(KnownDir.LIB, new String[]{stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR)});
    }

    public String getSrcBinVersion(){
	return srcBinVersion;
    }

    public String getDestBinVersion(){
	return destBinVersion;
    }

    public String getSrcDBVersion(){
	return srcDBVersion;
    }

    public String getDestDBVersion(){
	return destDBVersion;
    }

    public String getUpgradeDescription(){
	return "";
    }

    public String toString(){
	return srcBinVersion + "-" + destBinVersion + "_" + srcDBVersion + "-" + destDBVersion;
    }

    public boolean needsNewUpgrader(){
	return (getUpgraderDir() != null);
    }

    File getUpgraderDir(){
	if (upgraderDir != null)
	    return upgraderDir;

	File files[] = upgradeDir.listFiles();

	for (File dir : files){
	    if (dir.isDirectory() && dir.getName().startsWith(UpgradeLoader.UPGRADER_DIR)){
		upgraderDir = dir;
		return upgraderDir;
	    }
	}
	
	return null;
    }

    public void copyUpgraderTo(String path){
	File srcDir = getUpgraderDir();
	if (srcDir == null){
	    System.err.println("Trying to copy non existing folder.");
	    return;
	}

	File destDir = new File(path + "/" +srcDir.getName());
	String destPath = destDir.getAbsolutePath();

	if (!destDir.exists())
	    destDir.mkdirs();
	
	for (File file: srcDir.listFiles()){
	    FileUtilities.copyFile(file.getAbsolutePath(), destPath + "/" + file.getName());
	}
    }

    void restoreFromBackup(){
	System.err.println("Restoring ..." + this);
	restoreFiles();
	restoreDB();
    }

    void restoreFiles(){
	try{
	    for (String filepath: backedupFiles.keySet()){
		System.err.println("Restoring: "+filepath);
		backedupFiles.get(filepath).add(filepath);
		for (String destPath: backedupFiles.get(filepath)){
		    System.err.println("copying: "+backupDir+"/"+filepath +" to "+destPath);
		    if (FileUtilities.copyFile(backupDir + "/" +filepath, destPath) < 0)			
			System.err.println("ERROR: Unable to restore file: " + destPath);
		}
	    }
	} catch (Exception e){
	    System.err.println("RestoreFiles: ");
	    e.printStackTrace();
	}

    }

    void restoreDB(){
	System.err.println("Restoring db...");
	String backupSql = backupDirPath + "/" +DB_BACKUP_NAME;

	try{
	    Process p = Runtime.getRuntime().exec(EXEC_MYSQL + " -h " + dbHost + " -P " + dbPort + " -u"+dbLogin+" -p"+dbPassword+" "+dbName);
	    BufferedReader in = new BufferedReader(new FileReader(backupSql));
	    BufferedWriter out = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));

	    String line;
	    while((line = in.readLine()) != null)
		out.write(line+"\n");
	    in.close();
	    out.close();
	    p.waitFor();

	} catch (Exception e){
	    System.err.println("RestoreDB: ");
	    e.printStackTrace();
	}
    }


    void backupFile(String filePath, ArrayList<String> additionalDests) throws Exception {
	if (backedupFiles.containsKey(filePath)){
	    backedupFiles.get(filePath).addAll(additionalDests);
	    return;
	}
	File file = (new File(filePath)).getAbsoluteFile();
	String dirName = backupDir + "/" + file.getParent();
	(new File(dirName)).mkdirs();
	if (FileUtilities.copyFile(filePath, dirName + "/" +file.getName()) < 0)
	    throw new Exception("Unable to backupFile: "+filePath);

	HashSet<String> dests = new HashSet<String> (additionalDests);
	dests.add(filePath);
	backedupFiles.put(filePath, dests);
    }

    ArrayList<String> loadFile(String fileName) throws Exception {
	if (loadedFiles.get(fileName) != null)
	    return loadedFiles.get(fileName);

	ArrayList<String> list = new ArrayList<String>();
	
	BufferedReader in = new BufferedReader(new FileReader(fileName));
	String line;
	while ((line = in.readLine()) != null){
	    list.add(line);
	}

	loadedFiles.put(fileName, list);

	return list;
    }

    protected String safeFilename(String name){
	//XXX: Make it windows safe, handle "c:" kind of things.
	return name;
    }

    protected void backupDB() throws Exception {
	if (dbBackedUp)
	    return;
	
	System.err.println("Backingup database...");
	Process process = Runtime.getRuntime().exec(EXEC_MYSQLDUMP + " -h " + dbHost + " -P " + dbPort + " -u "+dbLogin+" -p"+dbPassword+" "+dbName+" "+" -r "+backupDirPath + File.separator + DB_BACKUP_NAME);
	process.waitFor();
	
	dbBackedUp = true;
    }

    protected void updateDB(String sqlCommand) throws Exception{
	if (!shouldUpdateDB)
	    return;
	
	if (!dbBackedUp){
	    backupDB();
	}

	Process p = Runtime.getRuntime().exec(EXEC_MYSQL + " -h " + dbHost + " -P " + dbPort + " -u " + dbLogin+" -p"+dbPassword+" "+dbName);
	BufferedWriter out = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));
	out.write(sqlCommand + "\n");
	out.close();
	p.waitFor();
    }

    protected void editFile(KnownFile knownFile, EditOperator edit, PositionSelector selector) throws Exception {
	ArrayList<String> dests = new ArrayList<String>(Arrays.asList(knownFiles.get(knownFile)));

	for (int i = 0; i < dests.size(); i++)
	    dests.set(i, dests.get(i) + "/" + knownFile.fileName());

	String firstPath = dests.remove(0);
	editFile(firstPath, edit, selector, dests.toArray(new String[dests.size()]));
    }

    protected void editFile(String fileName, EditOperator edit, PositionSelector selector, String... additionalDests) throws Exception {
	
	File file = new File(fileName);
	String filePath = file.getCanonicalPath();

	ArrayList<String> linesList = loadFile(filePath);
	ArrayList<String> destList = new ArrayList<String>();

	for (String dest: additionalDests)
	    destList.add(dest);
	backupFile(filePath, destList);

	edit.edit(linesList, selector.findPositions(linesList));

	String tmpFilename = tmpDir + "/" + safeFilename(file.getName());

	BufferedWriter out = new BufferedWriter(new FileWriter(tmpFilename));
	for (String line: linesList){
	    out.write(line, 0, line.length());
	    out.newLine();
	}
	out.close();
	
	if (FileUtilities.copyFile(tmpFilename, filePath) < 0)
	    throw new Exception("Unable to update file: " + filePath);

	for (String destFilename: additionalDests)
	    if (FileUtilities.copyFile(tmpFilename, destFilename) < 0)
		throw new Exception("Unable to update file: "+ destFilename);
    }

    protected void copyFile(String fileName, String[] destPaths) throws Exception {
	String srcFileName = upgradeDirName + "/" + fileName;
	
	for (String path: destPaths){
	    if ((new File(path)).exists()){
		backupFile(path, new ArrayList<String>(Arrays.asList(destPaths)));
		break;
	    }
	}

	for (String path: destPaths)
	    if (FileUtilities.copyFile(srcFileName, path) < 0)
		throw new Exception("Unable to copy file: "+ srcFileName +" to "+ path);
    }

    protected void copyFile(String fileName, KnownDir knownDir, String subPath) throws Exception{
	fileName = (new File(fileName)).getName();
	ArrayList<String> dests = new ArrayList<String>();

	for (String destDir: knownDirs.get(knownDir)){
	    dests.add(destDir + "/" + subPath +"/" + fileName);
	}

	copyFile(fileName, dests.toArray(new String[dests.size()]));
    }

    protected void copyFile(String fileName, KnownDir knownDir) throws Exception {
	copyFile(fileName, knownDir, "");
    }

    public final boolean upgrade(){
	try{
	    if (onUpgrade()){
		System.err.println("Successfully applied upgrade: "+this);
		return true;
	    }
	} catch (Exception e){
	    System.err.println("Unable to upgrade: "+this);
	    e.printStackTrace();
	}

	System.err.println("Upgrade failed: "+this);
	restoreFromBackup();
	return false;
    }


    public abstract boolean onUpgrade() throws Exception;

    public enum KnownFile {
	AUTOMATION_CONF("automation.conf");

	private final String fileName;

	KnownFile(String fileName){
	    this.fileName = fileName;
	}

	String fileName(){
	    return fileName;
	}
    }

    public enum KnownDir {
	BIN, CLASSES, LIB;
    }
}
