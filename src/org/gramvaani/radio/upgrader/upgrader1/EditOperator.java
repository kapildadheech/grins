package org.gramvaani.radio.upgrader.upgrader1;

import java.util.ArrayList;

public abstract class EditOperator {
    public void edit(ArrayList<String> lines, int positions[][]) throws Exception {
	onEdit(lines, positions);
	relayout(lines);
    }

    void relayout(ArrayList<String> lines){

	ArrayList<String> newLines = new ArrayList<String>();
	for (String line: lines){
	    String toks[] = line.split("[\n]");
	    for (int i = 0; i < toks.length; i++ ){
		newLines.add(toks[i]);
		if (line.endsWith("\n") && i == toks.length - 1)
		    newLines.add("");
	    }
	}

	lines.clear();
	lines.addAll(newLines);
    }

    public abstract void onEdit(ArrayList<String> lines, int positions[][]) throws Exception;

}

