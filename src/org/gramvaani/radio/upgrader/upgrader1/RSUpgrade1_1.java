package org.gramvaani.radio.upgrader.upgrader1;

import org.gramvaani.radio.stationconfig.StationConfiguration;

public class RSUpgrade1_1 extends RSUpgrade1Base {
    
    public RSUpgrade1_1(String dir, StationConfiguration stationConfig){
	super(dir, stationConfig);
	
	srcBinVersion = "0.2.1";
	destBinVersion = "0.2.2";
	srcDBVersion = "1";
	destDBVersion = "1.1";

    }

    public String getUpgradeID(){
	return "upgrade";
    }

    public boolean onUpgrade() throws Exception {
	editFile("testfile.txt", new InsertOperator("myline\nasdf\n"), new EndSelector());
	//editFile("testfile.txt", new InsertOperator("afteline"), new UniqueMatchAfterSelector("line2"));
	//editFile("testfile.txt", new InsertOperator("before"), new UniqueMatchBeforeSelector("line2"));
	//editFile("testfile.txt", new SubstituteOperator("xxx", 0 , SubstituteOperator.WORD), new UniqueMatchAfterSelector("line2"), new String[]{"/tmp"});
	editFile("testfile.txt", new InsertOperator("BLOOP"), new MatchAfterSelector("line"), "/tmp/tf");
	editFile(KnownFile.AUTOMATION_CONF, new InsertOperator("\n#########"), new EndSelector());

	updateDB("insert into Trivia  (_itemID, _trivia) values ('as','asdf');");
	
	copyFile("trash.txt", KnownDir.BIN);

	return true;
    }
}

