package org.gramvaani.radio.upgrader.upgrader1;

public interface RSUpgrade1 {

    public String getSrcBinVersion();
    public String getDestBinVersion();
    public String getSrcDBVersion();
    public String getDestDBVersion();

    public String getUpgradeID();
    public String getUpgradeDescription();

    public boolean needsNewUpgrader();

    public void copyUpgraderTo(String path);

    public void init();

    public boolean upgrade();

    public void setBinDir(String name);
    
    
}