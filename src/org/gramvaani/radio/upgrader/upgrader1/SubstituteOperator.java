package org.gramvaani.radio.upgrader.upgrader1;

import java.util.regex.*;

import java.util.ArrayList;

import static java.lang.Character.isWhitespace;

public class SubstituteOperator extends EditOperator {

    public static final int ROW = 0;
    public static final int NEXT_WORD = 1;
    public static final int PREV_WORD = 2;
    public static final int MATCH_PATTERN = 3;
    public static final int WORD = 4;

    String text;
    int mode;
    Pattern pattern;
    int word;

    public SubstituteOperator(String text){
	this(text, NEXT_WORD);
    }

    public SubstituteOperator(String text, int mode){
	this.mode = mode;
	this.text = text;
    }

    public SubstituteOperator(String text, Pattern pattern, int mode){
	this.pattern = pattern;
	this.mode = mode;
	this.text = text;
    }

    public SubstituteOperator(String text, String pattern, int mode){
	this.pattern = Pattern.compile(pattern);
	this.mode = mode;
	this.text = text;
    }

    public SubstituteOperator(String text, int word, int mode){
	this.word = word;
	this.mode = mode;
	this.text = text;
    }

    public void onEdit(ArrayList<String> lines, int positions[][]) throws Exception{
	for (int[] pos: positions){
	    String srcLine = lines.get(pos[0]);
	    String newLine;
	    
	    int start, end, len;

	    switch(mode){

	    case ROW:
		newLine = text;
		break;

	    case NEXT_WORD:
		start = pos[1];
		len = srcLine.length();
		System.err.println("length: "+len + " line: "+srcLine + " start: "+start);
		while (isWhitespace(srcLine.charAt(start)) && start < len - 1){
		    System.err.println("is white: "+srcLine.charAt(start));
		    start++;
		}
		end = start;
		while (!isWhitespace(srcLine.charAt(end)) && end < len - 1)
		    end++;
		newLine = srcLine.substring(0, start) + text + srcLine.substring(end);
		break;
		

	    case PREV_WORD:
		end = pos[1];
		len = srcLine.length();
		while (isWhitespace(srcLine.charAt(end)) && end >= 0)
		    end--;
		start = end;
		while (!isWhitespace(srcLine.charAt(start)) && start >= 0)
		    start--;
		newLine = srcLine.substring(0, start) + text + srcLine.substring(end);
		break;
		
	    case MATCH_PATTERN:
		Matcher m = pattern.matcher(srcLine);
		if (!m.find())
		    throw new Exception("Substitute operator. Unable to find pattern"+pattern+" in line: "+srcLine);
		newLine = srcLine.substring(0, m.start()) + text + srcLine.substring(m.end());
		break;
		
	    case WORD:
		int[] wordExtent = getWordExtent(srcLine, word);
		newLine = srcLine.substring(0, wordExtent[0]) + text + srcLine.substring(wordExtent[1]);
		break;
	    default:
		throw new Exception("Unknown substitute operator mode: "+mode);
	    }

	    lines.set(pos[0], newLine);
	}

	System.err.println(lines);
    }

    int[] getWordExtent(String str, final int word) throws Exception{
	int start = 0;
	int len = str.length();
	
	int curWord = word + 1;
	while (curWord-- > 0){
	    while (isWhitespace(str.charAt(start)) && start < len - 1)
		start++;
	    int end = start;
	    while (!isWhitespace(str.charAt(end)) && end < len - 1)
		end++;
	    
	    if (curWord == 0)
		return new int[] {start, end};

	    start = end;
	}

	throw new Exception("Unable to find word: " + word);
    }
}
