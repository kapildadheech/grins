package org.gramvaani.radio.upgrader.upgrader1;

import org.gramvaani.radio.stationconfig.StationConfiguration;
import org.gramvaani.radio.upgrader.UpgradeLoader;
import org.gramvaani.utilities.FileUtilities;

import java.lang.reflect.Constructor;
import java.io.*;

import java.util.*;

public class RSUpgrader1 implements Runnable {
    String binDir;
    String upgradeBaseDirName;
    String automationFileNames[];
    String curBinVersion;
    String curDBVersion;

    String machineID;

    static final String UPGRADES_DIR = "upgrades";
    static final String VERSIONS_FILE = ".grins_version";
    static final String UPGRADE_VERSIONS_FILE = ".gu_version";
    static final String UPGRADE_PACKAGE_NAME = "org.gramvaani.radio.upgrader.upgrade1";
    static final String UPGRADE_CLASS_DIR = "upgrade";

    static final int RERUN_UPGRADER_EXIT_STATUS = 10;

    ArrayList<RSUpgrade1> loadedUpgrades = new ArrayList<RSUpgrade1>();
    ArrayList<String> upgraderList = new ArrayList<String>();

    StationConfiguration stationConfig;

    public RSUpgrader1(){
	initVars();
    }

    void initVars(){
	binDir = ".";
	upgradeBaseDirName = binDir + "/" + UPGRADES_DIR;

	try {
	    BufferedReader in = new BufferedReader(new FileReader(binDir + "/" + VERSIONS_FILE));
	    String binLine = in.readLine();
	    String dbLine = in.readLine();
	    in.close();
	    
	    curBinVersion = binLine.split(":")[1].trim();
	    curDBVersion = dbLine.split(":")[1].trim();

	} catch (Exception e){
	    e.printStackTrace();
	    System.err.println("Could not read current versions.");
	    rollBackUpgrade();
	}

	machineID = System.getProperty("LOCAL_VM_NAME");
	stationConfig = new StationConfiguration(machineID);
	stationConfig.readConfig("./"+StationConfiguration.CONFIG_FILE_NAME);
	System.err.println("Current version: "+curBinVersion + ",\t" + curDBVersion +"\n");
    }

    void loadAvailableUpgrades(){
	try{
	    File dir = new File(upgradeBaseDirName);
	    
	    if (!dir.exists()){
		System.err.println("Upgrade base dir does not exist.");
		rollBackUpgrade();
	    }

	    String dirNames[] = dir.list();
	    
	    for (String dirName: dirNames){
		if (isUpgradeDir(dirName)){
		    String upgraderDir;
		    if ((upgraderDir = getNewUpgrader(upgradeBaseDirName + "/" + dirName)) != null)
			addUpgrader(upgraderDir);
		    else
			addUpgrade(dirName);
		}
	    }

	} catch (Exception e){
	    e.printStackTrace();
	    System.err.println("Unable to load upgrades.");
	    rollBackUpgrade();
	}
    }

    String getNewUpgrader(String dirName) {
	File upgradeDir = new File(dirName);
	File files[] = upgradeDir.listFiles();

	for (File dir : files){
	    if (dir.isDirectory() && dir.getName().startsWith(UpgradeLoader.UPGRADER_DIR)){
		return dir.getAbsolutePath();
	    }
	}
	
	return null;
    }

    boolean isUpgradeDir(String dirName){
	try{
	    File dir = new File(upgradeBaseDirName+ "/" + dirName);
	    if (!dir.isDirectory()){
		System.err.println("Not a directory: "+dirName);
		return false;
	    }

	    return isUpgradeDirName(dirName);

	}catch(Exception e){
	    e.printStackTrace();
	    return false;
	}
    }

    static final String EXP_UPGRADE_DIR = "upg[0-9.]+";

    boolean isUpgradeDirName(String dirName){
	if (dirName.matches(EXP_UPGRADE_DIR)){
	    System.err.println("Found upgrade:\t"+dirName);
	    return true;
	} else {
	    return false;
	}
    }

    void addUpgrader(String name){
	System.err.println("Found upgrader:\t"+name);
	upgraderList.add(name);
    }

    void upgradeUpgrader() throws Exception{
	for (String dirName: upgraderList){
	    if (canLoadUpgrader(dirName)){
		copyUpgrader(dirName, UpgradeLoader.getInstance().getLoaderDir());
		System.err.println("Copied new upgrader: "+dirName);
		System.exit(RERUN_UPGRADER_EXIT_STATUS);
	    }
	}
    }

    boolean canLoadUpgrader(String path) throws Exception{
	File upgraderDir = new File(path);
	String parentDir = upgraderDir.getParent();
	String[] version = readUpgradeVersions(parentDir + "/" + UPGRADE_VERSIONS_FILE);
	
	return canExecuteUpgrade(version[0], version[2]);
    }

    void copyUpgrader(String srcPath, String destParent) throws Exception{
	File srcDir = new File(srcPath);
	if (srcDir == null){
	    System.err.println("Trying to copy non existing folder.");
	    return;
	}

	File destDir = new File(destParent + "/" +srcDir.getName());
	String destPath = destDir.getAbsolutePath();

	if (!destDir.exists())
	    destDir.mkdirs();
	
	for (File file: srcDir.listFiles()){
	    FileUtilities.copyFile(file.getAbsolutePath(), destPath + "/" + file.getName());
	}
    }

    String[] readUpgradeVersions(String file) throws Exception{
	BufferedReader in = new BufferedReader(new FileReader(file));
	String binLine = in.readLine().split(":")[1];
	String dbLine = in.readLine().split(":")[1];
	in.close();
	
	String ver[] = new String[4];
	
	String binVersions[] = binLine.split("-");
	String dbVersions[] = dbLine.split("-");
	
	ver[0] = binVersions[0].trim();
	ver[1] = binVersions[1].trim();
	ver[2] = dbVersions[0].trim();
	ver[3] = dbVersions[1].trim();
	
	return ver;
    }

    void addUpgrade(String name){
	try{
	    String upgradeDir = upgradeBaseDirName + "/" +name;
	    UpgradeClassLoader loader = new UpgradeClassLoader(upgradeDir);

	    File dir = new File(upgradeDir + "/" + UPGRADE_CLASS_DIR);

	    for (String filename: dir.list()){
		if (filename.endsWith(".class"))
		    loader.loadClass(UPGRADE_PACKAGE_NAME+"."+filename.split(".class")[0]);
	    }
	    
	    Class klass = loader.loadClass(UPGRADE_PACKAGE_NAME+"."+"RSUpgrade1_1");
	    Constructor constructor = klass.getConstructor(String.class, StationConfiguration.class);
	    RSUpgrade1 upgrade = (RSUpgrade1) constructor.newInstance(upgradeDir, stationConfig);
	    upgrade.setBinDir(binDir);
	    upgrade.init();
	    loadedUpgrades.add(upgrade);
	} catch (Exception e){
	    e.printStackTrace();
	    System.err.println("Unable to load upgrade.");
	}
    }

    int compareDotSepInts(String ver1, String ver2){
	String fields1[] = ver1.split("[.]");
	String fields2[] = ver2.split("[.]");

	if (fields1.length < fields2.length)
	    return compareDotSepInts(ver1 + ".0", ver2);
	else if (fields1.length > fields2.length)
	    return compareDotSepInts(ver1, ver2 + ".0");

	for (int i = 0; i < fields1.length; i++){
	    int diff = Integer.parseInt(fields1[i]) - Integer.parseInt(fields2[i]);
	    if (diff == 0)
		continue;
	    else
		return diff;
	    
	}

	return 0;
    }

    int compareBinVersions(RSUpgrade1 up1, RSUpgrade1 up2){
	return compareDotSepInts(up1.getSrcBinVersion(), up2.getSrcBinVersion());
    }

    int compareDBVersions(RSUpgrade1 up1, RSUpgrade1 up2){
	return compareDotSepInts(up1.getSrcDBVersion(), up2.getSrcDBVersion());
    }
    
    void orderUpgrades(){
	Comparator<RSUpgrade1> upgradeComparator = new Comparator<RSUpgrade1>(){

	    public int compare(RSUpgrade1 up1, RSUpgrade1 up2){
		int binComparison = compareBinVersions(up1, up2);
		int dbComparison = compareDBVersions(up1, up2);

		if (binComparison * dbComparison < 0){
		    System.err.println("Error comparing upgrades:" + up1 + "," + up2);
		}

		if (binComparison < 0 || dbComparison < 0){
		    return -1;
		} else if (binComparison == 0 && dbComparison == 0){
		    System.err.println("Error comparing upgrades:" + up1 + "," + up2);
		    return 0;
		} else {
		    return 1;
		}
	    }

	    public boolean equals(Object obj){
		return this.equals(obj);
	    }
	};

	Collections.sort(loadedUpgrades, upgradeComparator);
    }

    void rollBackUpgrade(){

    }

    boolean canExecuteUpgrade(String binVersion, String dbVersion){
	return (curBinVersion.equals(binVersion) && curDBVersion.equals(dbVersion));
    }

    boolean canExecuteUpgrade(RSUpgrade1 upgrade){
	return canExecuteUpgrade(upgrade.getSrcBinVersion(), upgrade.getSrcDBVersion());
    }

    boolean saveVersions(String binVersion, String dbVersion){
	try{
	    PrintWriter out = new PrintWriter(new FileWriter(binDir + "/" + VERSIONS_FILE));
	    out.println("binary: "+binVersion);
	    out.println("database: "+dbVersion);
	    out.close();
	    return true;
	} catch (Exception e){
	    System.err.println("Caught exception");
	    e.printStackTrace();
	    return false;
	}

    }

    public void run(){
	try{
	    loadAvailableUpgrades();
	    orderUpgrades();

	    upgradeUpgrader();

	    for (RSUpgrade1 upgrade: loadedUpgrades){
		
		if (!canExecuteUpgrade(upgrade)){
		    System.err.println("Skipping upgrade: "+upgrade);
		    continue;
		}
		
		/*
		  if (upgrade.needsNewUpgrader()){
		  System.err.println("Loading new upgrader.");
		  upgrade.copyUpgraderTo(UpgradeLoader.getInstance().getLoaderDir());
		  System.exit(RERUN_UPGRADER_EXIT_STATUS);
		  }
		*/ 
		
		boolean result = upgrade.upgrade();
		
		if (result){
		    curBinVersion = upgrade.getDestBinVersion();
		    curDBVersion  = upgrade.getDestDBVersion();
		    saveVersions(curBinVersion, curDBVersion);
		    System.err.println("saving versions: "+curBinVersion+","+curDBVersion);
		}
		
		upgradeUpgrader();
	    }

	} catch (Exception e){
	    System.err.println("Unable to upgrade.");
	    e.printStackTrace();
	}
    }

    class UpgradeClassLoader extends ClassLoader {
	String classDirName;
	
	Hashtable<String, Class<?>> classes = new Hashtable<String, Class<?>>();

	public UpgradeClassLoader(String dirName){
	    super(ClassLoader.getSystemClassLoader());
	    classDirName = dirName + "/" + UPGRADE_CLASS_DIR;
	}
	
	public Class findClass(String name){
	    if (classes.get(name) != null)
		return classes.get(name);
	    byte[] b = loadClassData(name);
	    Class klass = defineClass(null, b, 0, b.length);
	    classes.put(name, klass);
	    return klass;
	}
	
	private byte[] loadClassData(String name){
	    String baseFilename = name.split(UPGRADE_PACKAGE_NAME)[1].split("[.]")[1];

	    String fileName = classDirName + "/" + baseFilename + ".class";
	    
	    try{
		File classFile = new File(fileName);
		int len = (int)classFile.length();

		byte bytes[] = new byte[len];

		int read = 0;
		int numRead = 0;
		
		DataInputStream in = new DataInputStream(new FileInputStream(classFile));
		
		while(read < len && (numRead = in.read(bytes, read, len - read)) >= 0){
		    read += numRead;
		}

		in.close();
		return bytes;
	    } catch (Exception e){
		e.printStackTrace();
		return null;
	    }
	}
    }
}

