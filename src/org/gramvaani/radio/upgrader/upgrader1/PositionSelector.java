package org.gramvaani.radio.upgrader.upgrader1;

import java.util.ArrayList;

public interface PositionSelector {
    public int[][] findPositions(ArrayList<String> lines) throws Exception;
}
