package org.gramvaani.radio.upgrader;

import java.io.File;
import java.net.URL;

public class UpgradeLoader {
    public static final String UPGRADER_DIR = "upgrader";
    public static final String UPGRADER_NAME = "RSUpgrader";
    
    String packageName, loaderDirName;
    File loaderDir;
    
    static UpgradeLoader instance = null;

    public UpgradeLoader(){
	packageName = this.getClass().getPackage().getName();
	loaderDirName = "/" + packageName.replace(".", "/");
	
	URL url = UpgradeLoader.class.getResource(loaderDirName);
	
	loaderDir = new File(url.getFile());

    }

    public String getLoaderDir(){
	return loaderDir.getAbsolutePath();
    }

    public static UpgradeLoader getInstance(){
	if (instance != null)
	    return instance;
	
	instance = new UpgradeLoader();
	return instance;
    }

    String getLatestUpgrader(File dir){
	String[] files = dir.list();

	int latest = -1;

	for (String file: files){
	    int ver;
	    if ((ver = getUpgraderVersion(file)) > latest){
		latest = ver;
	    }
	}

	return UPGRADER_DIR + latest + "." + UPGRADER_NAME + latest;
    }

    int getUpgraderVersion(String name){

	if (!name.startsWith(UPGRADER_DIR))
	    return -1;

	String loaderPath = loaderDir.getAbsolutePath() + "/" + name;

	File dir = new File(loaderPath);
	
	if (!dir.exists() || !dir.isDirectory())
	    return -1;

	String version = name.split(UPGRADER_DIR)[1];

	int ver;

	try{
	    ver = Integer.parseInt(version);
	} catch (Exception e){
	    return -1;
	}

	File upgrader = new File(loaderPath + "/" + UPGRADER_NAME + ver + ".class");

	if (upgrader.exists())
	    return ver;
	else
	    return -1;
	
    }

    void upgrade(){

	try{
	    String upgraderName = getLatestUpgrader(loaderDir);

	    System.err.println("\nUpgrader: "+upgraderName);

	    Runnable upgrader = (Runnable) Class.forName(packageName + "." + upgraderName).newInstance();
	    
	    upgrader.run();

	} catch (Exception e){
	    System.err.println("Exception In Upgrade Loader:");
	    e.printStackTrace(System.err);
	    System.exit(0);
	}
    }
    
    public static void main(String args[]){
	UpgradeLoader loader = new UpgradeLoader();
	loader.upgrade();
    }

}