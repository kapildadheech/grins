package org.gramvaani.radio.telephonylib;

import java.util.*;
import java.io.*;

import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.radio.medialib.Entity;
import org.gramvaani.radio.app.providers.MetadataProvider;

public class RSCallerID {
    String callerID;
    String lineID;

    public static final String SIP        = "SIP";
    public static final String PSTN       = "PSTN";
    public static final String UNKNOWN    = "UNKNOWN";

    static HashSet<String> idTypes = new HashSet<String>();
    static {
	idTypes.add(SIP);
	idTypes.add(PSTN);
    }

    public static final String SEPARATOR = "|";
    static final String STANDARD_PREFIX = "+";
    static final String VALID_PSTN_REGEX = "\\" + STANDARD_PREFIX + "?\\d{3,15}";
    //XXX: rework on the sip valid username
    static final String VALID_SIP_REGEX = "^[a-zA-Z0-9]+([a-zA-Z0-9]|_|.|@)*";

    public static String internationalCallPrefix;
    public static String nationalCallPrefix;
    public static String localCountryCode;
    public static String localAreaCode;


    public RSCallerID(String lineID, String callerID) {
	this.lineID = lineID;
	this.callerID = callerID;
    }
    
    public RSCallerID(String idString) {
	callerID = UNKNOWN;
	lineID = UNKNOWN;
	
	//Not using String.split here because | is a special character in regex
	//so idString.split("|") doesnt work it would need to be 
	//idString.split("\|"). To avoid making the code dependent on exact seperator
	//I chose the below implementation
	if (idString != null && !idString.equals("")) {
	    int index = idString.indexOf(SEPARATOR);
	    
	    if (index != -1) {
		lineID = idString.substring(0, index);
		callerID = idString.substring(index + 1);
	    } else {
		lineID = UNKNOWN;
		callerID = idString;
	    }
	}
    }
    
    public RSCallerID() {
	callerID = UNKNOWN;
	lineID = UNKNOWN;
    }
    
    public void setCallerID(String id) {
	if (id == null)
	    callerID = UNKNOWN;
	else
	    callerID = id;
    }
    
    public void setLineID(String lineID) {
	this.lineID = lineID;
    }
    
    public String toString() {
	return lineID + SEPARATOR + callerID;
    }

    public String getCallerID() {
	return callerID;
    }

    public String getLineID() {
	return lineID;
    }

    public boolean equals(RSCallerID id) {
	if (id.getCallerID().endsWith(callerID))
	    return true;

	if (callerID.endsWith(id.getCallerID()))
	    return true;

	return false;
    }
    
    //Returns standardized string if it can standradize it
    //Returns null otherwise (this happens in case of local landline and mobile numbers)
    public String toStandardizedForm() {
	if (!validCode(internationalCallPrefix) ||
	   !validCode(nationalCallPrefix) ||
	   !validCode(localCountryCode) ||
	   !validCode(localAreaCode)) {
	    
	    return null;
	}

	if (shouldConvert()) {
	    if (callerID.startsWith(STANDARD_PREFIX)) 
		return callerID;
	    
	    if (callerID.startsWith(internationalCallPrefix)) {
		return STANDARD_PREFIX + callerID.substring(internationalCallPrefix.length());
	    }
	    
	    if (callerID.startsWith(nationalCallPrefix)) {
		return STANDARD_PREFIX + localCountryCode + callerID.substring(nationalCallPrefix.length());
	    }

	    //2 cases left: 1) local mobile number, local landline number
	    //Cant differentiate between the two without prefix codes
	    return null;
	} 
	
	return callerID;
    }

    //If the callerID can be standardized then it returns the id in standardized form
    //else it returns the string in the non standard form.
    public String toDBString() {
	String standardID = toStandardizedForm();
	
	if (standardID == null)
	    return callerID;
        else
	    return standardID;
    }

    //Returns callerID in national form (i.e. strips the country code from the number) if the number
    //has the local country code as prefix. Returns null otherwise.
    public String toNationalForm() {
	if (!validCode(internationalCallPrefix) ||
	   !validCode(nationalCallPrefix) ||
	   !validCode(localCountryCode)) {
	    return null;
	}

	String[] validPrefixes = new String[] {STANDARD_PREFIX + localCountryCode,
					       internationalCallPrefix + localCountryCode,
	                                       nationalCallPrefix};

	if (shouldConvert()) {

	    for (String prefix : validPrefixes) {
		if (callerID.startsWith(prefix)) {
		    return callerID.substring(prefix.length());
		}
	    }
	    
	} 
	
	return null;
    }

    boolean validCode(String code) {
	return (code != null && !code.equals(""));
    }

    public String toLocalForm() {
	if (!validCode(internationalCallPrefix) ||
	   !validCode(nationalCallPrefix) ||
	   !validCode(localCountryCode) ||
	   !validCode(localAreaCode)) {
	    return null;
	}

	String[] validPrefixes = new String[] {STANDARD_PREFIX + localCountryCode + localAreaCode,
					       internationalCallPrefix + localCountryCode + localAreaCode,
	                                       nationalCallPrefix + localAreaCode};

	if (shouldConvert()) {

	    for (String prefix : validPrefixes) {
		if (callerID.startsWith(prefix)) {
		    return callerID.substring(prefix.length());
		}
	    }
	    
	} 
	
	return null;
    }

    //Returns true if the number under consideration is a landline or a mobile number
    //Detected by checking if lineID is unknown
    protected boolean shouldConvert() {
	return (!lineID.equals(TelephonyLine.UNKNOWN));
    }

    public static String getIDType(String lineID) {
	if(lineID != null && !lineID.equals(TelephonyLine.UNKNOWN))
	    return PSTN;
	else //XXX assuming SIP
	    return SIP;
    }

    public static boolean isValidID(String idType, String idString) {
	if (idType == null || idString == null) {
	    return false;
	}

	if (idType.equals(SIP)) {

	    return (idString.matches(VALID_SIP_REGEX));

	} else if (idType.equals(PSTN)) {

	    return (idString.matches(VALID_PSTN_REGEX));
	}

	return false;
    }

    public static String[] getIDTypes() {
	return idTypes.toArray(new String[idTypes.size()]);
    }


    public static Entity[] getPotentialContacts(RSCallerID callerID, MetadataProvider metadataProvider) {
	String id = callerID.toStandardizedForm();
	String numberType = getIDType(callerID.getLineID());
	Entity[] potentialContacts;
	boolean doSuffixMatch;

	if (id != null) {
	    doSuffixMatch = false;

	    //Getting potential callers using standardized id
	    potentialContacts =  metadataProvider.getPotentialContacts(numberType, id, doSuffixMatch);
	    if (potentialContacts.length != 0)
		return potentialContacts;
	    
	    //Getting potential callers using national form id
	    id = callerID.toNationalForm();
	    if (id == null) 
		return new Entity[]{};
	    potentialContacts = metadataProvider.getPotentialContacts(numberType, id, doSuffixMatch);
	    if (potentialContacts.length != 0)
		return potentialContacts;
	    
	    //Getting potential callers using local form id
	    id = callerID.toLocalForm();
	    if (id == null) 
		return new Entity[]{};
	    potentialContacts = metadataProvider.getPotentialContacts(numberType, id, doSuffixMatch);
	    return potentialContacts;
	    
	} else {
	    //Getting potential callers for a local number in non standard form
	    //Using suffix matching
	    doSuffixMatch = true;
	    potentialContacts = metadataProvider.getPotentialContacts(numberType, callerID.getCallerID(), doSuffixMatch);
	    return potentialContacts;
	}
    }
}