package org.gramvaani.radio.telephonylib;

public class TelephonyLine {
    protected String lineID;
    protected String channelType;
    protected String channelSpecificID;
    protected String inPrefix;
    protected String outPrefix;
    
    public static final String UNKNOWN = "unknown";
    public static final int IN_PREFIX_LENGTH = 1;
    public static final String WILDCARD = "*";

    public TelephonyLine(String lineID, String channelType, String channelSpecificID, String inPrefix, String outPrefix) {
	this.lineID = lineID;
	this.channelType = channelType;
	this.channelSpecificID = channelSpecificID;
	this.inPrefix = inPrefix;
	this.outPrefix = outPrefix;
    }

    public TelephonyLine(String channelType, String channelSpecificID, String inPrefix) {
	this.channelType = channelType;
	this.channelSpecificID = channelSpecificID;
	this.inPrefix = inPrefix;
    }

    public String getID() {
	return lineID;
    }
    
    public void setID(String id) {
	lineID = id;
    }

    public String getChannelType() {
	return channelType;
    }

    public String getChannelSpecificID() {
	return channelSpecificID;
    }

    public String getInPrefix() {
	return inPrefix;
    }

    public String getOutPrefix() {
	return outPrefix;
    }

    public boolean equals(TelephonyLine line) {
	boolean isEqual = false;
	if ( (line.getChannelType().equals(this.channelType)) &&
	     (line.getChannelSpecificID().equals(this.channelSpecificID))) {
	    
	    if ( (line.getInPrefix().equals(this.inPrefix)) ||
		 (line.getInPrefix().equals(WILDCARD)) ||
		 (this.inPrefix.equals(WILDCARD)))
		isEqual = true;
	}

	return isEqual;
    }


}