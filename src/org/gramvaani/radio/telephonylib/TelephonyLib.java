package org.gramvaani.radio.telephonylib;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.rscontroller.messages.TelephonyEventMessage;
import org.gramvaani.radio.diagnostics.*;

import org.asteriskjava.live.*;
import org.asteriskjava.live.internal.*;
import org.asteriskjava.manager.*;
import org.asteriskjava.manager.action.*;
import org.asteriskjava.manager.response.*;
import org.asteriskjava.manager.event.*;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.util.*;
import java.io.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import org.apache.commons.codec.binary.Base64;

public class TelephonyLib implements AsteriskServerListener, PropertyChangeListener, 
				     ManagerEventListener, OriginateCallback, DiagnosticUtilitiesPingCallback { 
    
    protected static final String MONITOR_FORMAT = "wav";
    
    protected static final String CONTEXT_ALSA = "alsa";
    protected static final String CONTEXT_CONF = "conference";
    protected static final String CONTEXT_HOLD = "onhold";
    protected static final String CONTEXT_CONF_MONITOR = "monitor-conference";
    protected static final String CONTEXT_NOT_ACCEPTED = "default";

    protected static final String EXTN_DUMMY           = "11";
    protected static final String CONF_ROOMNO          = "31415";
    protected static final String CHANNEL_ALSA         = "ALSA";
    protected static final String DEFAULT_DEVICE       = "DEFAULT";
    protected static final String ALSA_CHAN_TYPE       = "ALSA_CHAN_TYPE";

    protected static String EXTN_PLAYOUT_DEFAULT = "default!default";
    protected static String EXTN_PREVIEW_DEFAULT = "plug:dsnoop!plug:dmix";
    protected static String EXTN_PLAYOUT = EXTN_PLAYOUT_DEFAULT;
    protected static String EXTN_PREVIEW = EXTN_PREVIEW_DEFAULT;
    
    protected static final int PRIORITY_ALSA = 1; 
    protected static final int MAX_CONNECTION_FAIL_COUNTS = 3;
    protected static final int CONNECT_RETRY_INTERVAL = 2000;

    public final static int OFFLINE = 1;
    public final static int ONLINE  = 2;

    public final static long ATTEMPT_TELEPHONY_UP_INTERVAL = 10000; //10 seconds.
    protected static final int MAX_CHANNEL_EXCEPTION_ATTEMPTS = 3;
    protected static final int CHANNEL_EXCEPTION_RETRY_INTERVAL = 100;

    public static final String ERROR_NO_CHANNEL = "NoSuchChannelException";

    //Channel variables
    public static final String PHONE_LINE_ID      = "PHONE_LINE_ID";
    public static final String GUID               = "GUID";
    public static final String DNID               = "CALLERID(DNID)";
    public static final String SIPURI             = "SIPURI";
    protected static final String CALLERID        = "CALLERID";
    static final String DONGLEIMEI                = "DONGLEIMEI";

    //Outgoing call status
    public static final String DIALING  = "DIALING";
    public static final String RINGING  = "RINGING";
    public static final String SUCCESS  = "SUCCESS";
    public static final String FAILURE  = "FAILURE";
    public static final String BUSY     = "BUSY";
    public static final String NOANSWER = "NOANSWER";

    //Causes for hangup
    public static final String CAUSE_SERVER_SYNC = "SERVER_SYNC"; //hanging up as a result of sync with asterisk server
    public static final String CAUSE_NORMAL      = "NORMAL";
    public static final String CAUSE_CHANUNAVAIL = "CHANUNAVAIL";
    public static final String CAUSE_NOANSWER    = "NOANSWER";
    public static final String CAUSE_UNKNOWN     = "UNKNOWN";

    public static final String MONITOR_ERROR     = "MONITOR_ERROR";
    
    //String for device-ports/channels
    public static final String DAHDI             = "DAHDI";
    public static final String SIP               = "SIP";
    public static final String MOBILE            = "Mobile";
    public static final String ALSA              = "ALSA";
    public static final String GTALK		 = "Gtalk";
    public static final String DONGLE            = "Dongle";

    static final long MANAGER_RESPONSE_TIMEOUT_MS	= 3000;

    //Strings for maintaining telephony status i.e. online/offline. Populated from station config.
    String offlineStatus = "";
    String onlineStatus = "";
    String statusFile = "";

    protected StationConfiguration stationConfig;
    protected AsteriskServer asteriskServer;
    protected LogUtilities logger;
    protected MediaLib medialib;
    protected int state, prevState, stateOnTelephonyDown;
    protected Hashtable<String, AsteriskChannel> channels;
    protected Hashtable<String, String> callMonitorFileMap;

    MeetMeRoom meetMeRoom;
    Object meetMeAssignmentLock = new Object();
    protected String[] externalChannelTypes = new String[] {SIP, DAHDI, MOBILE, GTALK, DONGLE};
    protected TelephonyConnectionChecker connectionChecker; 
    protected HashSet<String> outgoingCalls;

    protected boolean isConnected = false;
    String libDir;
    boolean previewChannelHungup = true;
    Object previewChannelLock = new Object();
    String previewChannelId = "";

    Object channelHoldLock = new Object();
    boolean channelOnHold = false;
    String channelOnHoldId = "";
    Object confLock = new Object();
    String confUserId = "____";
    Object redirectLock = new Object();
    String redirectChannelId = "";
    Object hangupLock = new Object();
    String hangupChannelId = "";

    String previewCall = "";
    boolean dialedOutInConf = false;

    DiagnosticUtilities diagUtil;
    Hashtable<String, PingRequest> pingRequests;
    Object pingLock = new Object();
    static final long SIP_STATUS_STALE_TIMEOUT = 60000;

    int prevCount =0, redirCount=0;

    public static final int POOL_THREAD_COUNT = 1;
    ExecutorService executor;

    ArrayList<AsteriskSMSListener> smsListeners = new ArrayList<AsteriskSMSListener>();
    
    public TelephonyLib(StationConfiguration stationConfig, MediaLib medialib) {
	this.stationConfig = stationConfig;	
	this.medialib = medialib;
	this.logger = new LogUtilities("TelephonyLib");
	this.asteriskServer = null;
	channels = new Hashtable<String, AsteriskChannel>();
	callMonitorFileMap = new Hashtable<String, String>();
	outgoingCalls = new HashSet<String>();
	pingRequests = new Hashtable<String, PingRequest>();
	state = OFFLINE;
	prevState = OFFLINE;
	stateOnTelephonyDown = OFFLINE;
	libDir = stationConfig.getStringParam(StationConfiguration.LIB_BASE_DIR);
	statusFile = stationConfig.getStringParam(StationConfiguration.TELEPHONY_STATUS_FILE);
	offlineStatus = stationConfig.getStringParam(StationConfiguration.TELEPHONY_STATUS_OFFLINE);
	onlineStatus = stationConfig.getStringParam(StationConfiguration.TELEPHONY_STATUS_ONLINE);
	setState(OFFLINE);
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfig, null);
	executor = Executors.newFixedThreadPool(POOL_THREAD_COUNT);
    }

    int connect() {
	int connectionAttempts = 0;
	if (isConnected()) {
	    logger.debug("Connect: Already connected");
	    return 0;
	} else {
	    if (asteriskServer != null && asteriskServer.getManagerConnection() != null) {
		logger.debug("Connect: Trying to log off.");
		try {
		    asteriskServer.getManagerConnection().logoff();
		} catch(Exception e) {
		    logger.warn("Connect: Unable to logoff from asterisk server before attempting reconnect");
		}
	    }

	    String server   = stationConfig.getStringParam(StationConfiguration.TELEPHONY_SERVER);
	    String userName = stationConfig.getStringParam(StationConfiguration.TELEPHONY_ADMIN);
	    String password = stationConfig.getStringParam(StationConfiguration.TELEPHONY_PASSWORD);
	    int port 	    = stationConfig.getIntParam(StationConfiguration.TELEPHONY_PORT);

	    logger.debug("Connect: Connecting to " + server + ", username: " + userName + " password: " + password);
	    asteriskServer = new DefaultAsteriskServer(server, port, userName, password);
	    if (asteriskServer.getManagerConnection() instanceof DefaultManagerConnection){
		DefaultManagerConnection connection = (DefaultManagerConnection) asteriskServer.getManagerConnection();
		connection.setDefaultResponseTimeout(MANAGER_RESPONSE_TIMEOUT_MS);
	    }

	    while (connectionAttempts < MAX_CONNECTION_FAIL_COUNTS) {
		try {
		    asteriskServer.addAsteriskServerListener(this);
		    break;
		} catch(ManagerCommunicationException e) {
		    logger.error("Connect: Could not connect to asterisk server.", e);
		    connectionAttempts++;
		    try{
			Thread.sleep(CONNECT_RETRY_INTERVAL);
		    } catch(Exception ex){}
		}
	    }

	    if (connectionAttempts == MAX_CONNECTION_FAIL_COUNTS) {
		declareTelephonyDown();
		return -1;
	    }
	    
	    asteriskServer.getManagerConnection().addEventListener(this);
	    
	    if (probeAsteriskConnectionActively()) {
		logger.debug("Connect: Connection successful.");
		isConnected = true;
		declareTelephonyUp();
		
		createConferenceRoom();

		if (connectionChecker == null) {
		    connectionChecker = new TelephonyConnectionChecker("TelephonyLib");
		    connectionChecker.start();
		} else {
		    logger.info("Connect: ConnectionChecker already initialized.");
		}

		/*
		if (regChecker == null){
		    regChecker = new ataRegistrationChecker("TelephonyLib");
		    regChecker.start();
		} else{
		    logger.info("Connect: RegistrationChecker already initialized.");
		}
		*/

		return 0;
	    } else {
		logger.debug("Connect: Connection failed.");
		connectionDown();
		return -1;
	    }
	}
	
    }

    protected boolean createConferenceRoom(){
	try{
	    synchronized(meetMeAssignmentLock) {
		meetMeRoom = asteriskServer.getMeetMeRoom(CONF_ROOMNO);
	    }
	} catch (Exception e){
	    logger.error("CreateConferenceRoom: Encountered exception.", e);
	    return false;
	}
	
	return true;
    }

    protected void addChannelToConference(AsteriskChannel channel) {
	if (channel == null){
	    logger.error("AddChannelToConference: Null channel.");
	    return;
	}
	//System.err.println("------------------redirecting to conf: "+channel.getName()+" id:"+channel.getId());

	synchronized (confLock){
	    confUserId = channel.getId();
	    redirectChannelAsync(channel, CONTEXT_CONF, CONF_ROOMNO, 1);
	    try{
		//System.err.println("---------------waiting for channel: "+confUserId+" to add to meetme");
		confLock.wait(10000);
	    } catch (Exception e){
		logger.error("AddChannelToConference: Encountered Exception", e);
	    }
	}
    }

    public boolean isConnected() {
	return isConnected;
    }

    protected boolean probeAsteriskConnectionActively() {
	PingAction pingAction = new PingAction();
	pingAction.setActionId(GUIDUtils.getGUID());
	logger.debug("ProbeAsteriskConnectionActively: Sending Ping: " + pingAction.toString());

	ManagerResponse response = sendActionToManager(pingAction);

	if (response != null && response instanceof PingResponse) {
	    logger.debug("ProbeAsteriskConnectionActively: Got response: " + response.toString());
	    return true;
	} else {
	    logger.warn("ProbeAsteriskConnectionActively: Returning false.");
	    return false;
	}

    }

    public boolean enableConf() {
	String linkedId = null;

	for (AsteriskChannel channel: asteriskServer.getChannels()){
	    if (isAlsaChannel(channel)){
		AsteriskChannel linkedChannel = channel.getLinkedChannel();
		if (linkedChannel != null){
		    linkedId = linkedChannel.getId();
		    addChannelToConference(linkedChannel);
		}
	    }
	}

	for (AsteriskChannel channel: asteriskServer.getChannels()){
	    if (!isPreviewChannel(channel) && !isLocalChannel(channel) && !isAlsaChannel(channel) &&!isDahdiPseudoChannel(channel)){
		if (linkedId == null || !linkedId.equals(channel.getId()))
		    addChannelToConference(channel);
	    }
	}

	//XXX: Test if this is correct
	if (previewConf() && monitorConf()) {
	    dialedOutInConf = false;
	    return true;
	} else {
	    return false;
	}

    }

    public boolean disableConf() {
	for (MeetMeUser user: meetMeRoom.getUsers()) {
	    try{
		AsteriskChannel channel = user.getChannel();
		if (isAlsaChannel(channel) || isLocalChannel(channel)) {
		    synchronized(confLock){
			confUserId = channel.getId();
			user.kick();
			try{
			    //System.err.println("-------waiting to kick out channel: "+confUserId+":"+channel.getName());
			    confLock.wait(10000);
			    //System.err.println("-------returning from kicking: "+channel.getId()+":"+channel.getName());
			} catch (Exception e){}
		    }
		} else {
		    holdCall(channel.getId());
		}
	    } catch (Exception e){
		logger.error("DisableConf: Encountered exception: ", e);
	    }
	}

	callMonitorFileMap.remove(CONF_ROOMNO);
	previewChannelHungup = true;
	return true;
    }

    public boolean destroyConf() {
	for (MeetMeUser user: meetMeRoom.getUsers()){
	    try{
		user.kick();
	    } catch (Exception e){
		logger.error("DestroyConf: Encountered exception: ", e);
		return false;
	    }
	}
	return true;
    }

    boolean disconnectConfFromAlsa(){
	for (MeetMeUser user: meetMeRoom.getUsers()){
	    AsteriskChannel channel = user.getChannel();
	    try{
		if (isAlsaChannel(channel))
		    user.kick();
	    } catch (Exception e){
		logger.error("DisconnectFromAlsa: Encountered exception: ", e);
	    }
	}

	return true;
    }

    public synchronized boolean goOffline() {
	if (state == ONLINE) {
	    setState(OFFLINE);
	    return true;
	} 
	return false;
    }

    public synchronized boolean goOnline() {
	//gracefully terminate all active calls 
	//these calls are using offline apps. No harm in that but we need the lines open for online telephony
	hangupChannelsGracefully(false);
	setState(ONLINE);
	return true;
    }

    public synchronized boolean goOnAir(boolean isConference) {
	logger.info("GoOnAir: isConference: "+isConference);
	if (isConference) {
	    for (MeetMeUser user: meetMeRoom.getUsers()) {
		try{
		    AsteriskChannel channel = user.getChannel();
		    if (isPreviewChannel(channel)) {
			//user.kick();
			hangupChannel(channel);
		    } else if (isPlayoutChannel(channel)) {
			logger.warn("GoOnAir: Conference already on air. Playout channel:" + channel.getName());
			return true;
		    }
		} catch (Exception e) {
		    logger.error("GoOnAir: Encountered exception:",e);
		}
	    }

	    return playoutConf();
	}
	
	AsteriskChannel previewCallChannel = getPreviewCallChannel();
	AsteriskChannel playoutCallChannel = getPlayoutCallChannel();

	if (previewCallChannel != null){
	    return redirectChannel(previewCallChannel, CONTEXT_ALSA, EXTN_PLAYOUT, PRIORITY_ALSA);
	} else if (playoutCallChannel != null) {
	    logger.warn("GoOnAir: Channel " + playoutCallChannel.getName() + " already on air.");
	    return true;
	} else {
	    logger.error("GoOnAir: Unable to locate previewCallChannel");
	    return false;
	}
    }

    public synchronized boolean goOffAir(boolean isConference) {
	if (isConference) {
	    for (MeetMeUser user: meetMeRoom.getUsers()) {
		try{
		    AsteriskChannel channel = user.getChannel();
		    if (isPlayoutChannel(channel)) {
			//user.kick();
			hangupChannel(channel);
		    } else if (isPreviewChannel(channel)) {
			logger.warn("GoOffAir: Conference already off air. Preview channel:" + channel.getName());
			return true;
		    }
		} catch (Exception e) {
		    logger.error("GoOffAir: Encountered exception:",e);
		}
	    }

	    return previewConf();
	}

	AsteriskChannel playoutCallChannel = getPlayoutCallChannel();
	AsteriskChannel previewCallChannel = getPlayoutCallChannel();
	
	if (playoutCallChannel != null){
	    return redirectChannel(playoutCallChannel, CONTEXT_ALSA, EXTN_PREVIEW, PRIORITY_ALSA);
	} else if (previewCallChannel != null) {
	    logger.warn("GoOffAir: Channel " + previewCallChannel.getName() + " already offline.");
	    return true;
	} else {
	    logger.error("GoOffAir: Unable to locate playoutCallChannel.");
	    return false;
	}
	    
    }

    public boolean holdCall(String callGUID) {
	if (callGUID == null) {
	    logger.error("HoldCall: null callGUID");
	    return false;
	}

	AsteriskChannel channel = channels.get(callGUID);
	
	if (channel == null){
	    logger.error("HoldCall: Cannot put on hold. Channel with guid "+callGUID + "not found.");
	    return false;
	}

	try {
	    channelOnHold = false;
	    channelOnHoldId = callGUID;
	    redirectChannelAsync(channel, CONTEXT_HOLD, EXTN_DUMMY, 1);
	} catch (Exception e){
	    logger.error("HoldCall: Encountered exception while redirecting channel.", e);
	    return false;
	}

	if (!channelOnHold){
	    synchronized(channelHoldLock){
		//System.err.println("holdCall: "+channelOnHoldId);
		try{
		    channelHoldLock.wait(2000);
		    //System.err.println("-------- returning from channel hold"+callGUID+":"+channel.getName());
		    channelOnHold = false;
		} catch (Exception e){
		    logger.error("HoldCall: Exception: ", e);
		}
	    }
	}

	return true;
    }

    public boolean muteCall(String callGUID){
	if (callGUID == null || callGUID.equals("")){
	    logger.error("MuteCall: null callGUID");
	    return false;
	}

	AsteriskChannel channel = channels.get(callGUID);

	if (channel == null){
	    logger.error("MuteCall: channel with guid: "+callGUID+" not found");
	    return false;
	}

	try{
	    for (MeetMeUser user: meetMeRoom.getUsers()){
		if (user.getChannel().getId().equals(callGUID)){
		    user.mute();
		}
	    }
	} catch (Exception e){
	    logger.error("MuteCall: Encountered exception.", e);
	    return false;
	}
	    
	return true;
    }

    public boolean unmuteCall(String callGUID){
	if (callGUID == null || callGUID.equals("")){
	    logger.error("UnmuteCall: null callGUID");
	    return false;
	}

	AsteriskChannel channel = channels.get(callGUID);

	if (channel == null){
	    logger.error("UnmuteCall: channel with guid: "+callGUID+" not found");
	    return false;
	}

	try{
	    for (MeetMeUser user: meetMeRoom.getUsers()){
		if (user.getChannel().getId().equals(callGUID)){
		    user.unmute();
		} else if (!isAlsaChannel(user.getChannel())){
		    user.mute();
		}
	    }
	} catch (Exception e){
	    logger.error("UnmuteCall: Encountered exception.", e);
	    return false;
	}

	return true;
    }

    protected void hangupChannelsGracefully(boolean hangupOutgoing) {
	String context = stationConfig.getStringParam(StationConfiguration.CONTEXT_GRACEFUL_HANGUP);
	String extn = stationConfig.getStringParam(StationConfiguration.EXTN_GRACEFUL_HANGUP);
	int priority = stationConfig.getIntParam(StationConfiguration.PRIORITY_GRACEFUL_HANGUP);

	if (!isConnected())
	    return;

	if (context == null || extn == null || priority == -1) {
	    logger.error("HangupChannelsGracefully: Information required for graceful hangup not available. context:" + context + " extn:" + extn + " priority:" + priority);
	    return;
	} else {
	    logger.info("HangupChannelsGracefully: Redirecting to context:" + context + " extn:" + extn + " priority:" + priority);
	}

	HashSet<String> channelsRedirected = new HashSet<String>();

	for (AsteriskChannel channelOnServer : asteriskServer.getChannels()) {
	    if (isExternalChannel(channelOnServer.getName()) &&
	       !channelsRedirected.contains(channelOnServer.getName())) {
		
		if (!hangupOutgoing && outgoingCalls.contains(channelOnServer.getId()))
		    continue;
		
		AsteriskChannel linkedChannel = channelOnServer.getLinkedChannel();
		
		try {
		    if (linkedChannel != null && isExternalChannel(linkedChannel.getName())) {
			
			logger.info("HangupChannelsGracefully: Hanging up " + channelOnServer.getName() + " and " + linkedChannel.getName() + " gracefully.");
			channelOnServer.redirectBothLegs(context, extn, priority);
			channelsRedirected.add(linkedChannel.getName());

		    } else {
	
			logger.info("HangupChannelsGracefully: Hanging up " + channelOnServer.getName() + " gracefully.");
			redirectChannelAsync(channelOnServer, context, extn, priority);

		    }
		} catch(Exception e) {

		    logger.error("HangupChannelsGracefully: Got error while redirecting channels. ChannelOnServer:" + channelOnServer + " linkedChannel:" + linkedChannel);

		} //end of try-catch block

		channelsRedirected.add(channelOnServer.getName());

	    } //If channel is external ends here.
	} //end of for loop
    }

    protected void updateServerStatus() {
	logger.info("UpdateServerStatus: Updating information.");
	Hashtable<String, AsteriskChannel> newChannels = new Hashtable<String, AsteriskChannel>();

	for (AsteriskChannel channelOnServer : asteriskServer.getChannels()) {
	    if (isExternalChannel(channelOnServer.getName())) {
		if (channels.get(channelOnServer.getId()) == null) {
		    newChannels.put(channelOnServer.getId(), channelOnServer);
		    channelOnServer.addPropertyChangeListener(AsteriskChannel.PROPERTY_LINKED_CHANNEL, this);
		    channelOnServer.addPropertyChangeListener(AsteriskChannel.PROPERTY_STATE, this);
		    
		    ChannelState channelState = channelOnServer.getState();
		    
		    //Assuming below that if a channel is up and is not linked to any channel
		    //then it is on MusicOnHold. Could not find a cleaner way to do this.
		    if (channelState == ChannelState.RING || channelState == ChannelState.RINGING 
		       || (channelState == ChannelState.UP && channelOnServer.getLinkedChannel() == null)) {
			
			RSCallerID id = getCallerIDForChannel(channelOnServer);
			for (TelephonyLibListener listener : listeners.values()) {
			    if (listener != null)
				listener.newCall(channelOnServer.getId(), id);
			}

		    }
		}
	    }
	}

	AsteriskChannel channelOnServer = null;

	for (AsteriskChannel channel : channels.values()) {
	    if ((channelOnServer = asteriskServer.getChannelById(channel.getId())) == null) {
		for (TelephonyLibListener listener : listeners.values()) {
		    if (listener != null)
			listener.callerHungup(channel.getId(), CAUSE_SERVER_SYNC);
		}
		outgoingCalls.remove(channel.getId());
		callMonitorFileMap.remove(channel.getId());
	    } else {
		newChannels.put(channelOnServer.getId(), channelOnServer);
	    }
	}

	channels.clear();
	channels = newChannels;
	
    }
    

    ManagerResponse sendActionToManager(ManagerAction action) {
	ManagerResponse response = null;

	if (asteriskServer == null || asteriskServer.getManagerConnection() == null)
	    return null;

	try {
	    response = asteriskServer.getManagerConnection().sendAction(action);
	    if (response instanceof ManagerError) {
		logger.error("SendActionToManager: Error in sending action: " + response.toString());
	    }
	} catch(IOException ioe) {
	    logger.error("SendActionToManager: Connection disrupted while trying to send action.", ioe);
	} catch(Exception e) {
	    logger.error("SendActionToManager: Exception occurred while trying to send action.", e);
	}

	return response;
    }


    public String[] getActiveTelephonyLines() {
	TelephonyLine[] phonelines = stationConfig.getAllTelephonyLines();
	ArrayList<String> activeLines = new ArrayList<String>();

	for (TelephonyLine line :phonelines) {
	    String channelType = line.getChannelType();

	    if (channelType.equals(SIP)) {
		String status = getSIPChannelStatus(line.getChannelSpecificID());
		if (status.equals(TelephonyEventMessage.PHONE_LINE_OK))
		    activeLines.add(line.getID());

	    } else if (channelType.equals(DAHDI)) {
		String status = getDahdiChannelStatus(line.getChannelSpecificID());
		if (status.equals(TelephonyEventMessage.PHONE_LINE_OK))
		    activeLines.add(line.getID());
	    } else if (channelType.equals(DONGLE)) {
		String status = getDongleChannelStatus(line.getChannelSpecificID());
		if (status.equals(TelephonyEventMessage.PHONE_LINE_OK))
		    activeLines.add(line.getID());
	    }
	}

	return activeLines.toArray(new String[]{});
    }

    public String[] getInactiveTelephonyLines() {
	TelephonyLine[] phonelines = stationConfig.getAllTelephonyLines();
	ArrayList<String> inactiveLines = new ArrayList<String>();

	for (TelephonyLine line :phonelines) {
	    String channelType = line.getChannelType();

	    if (channelType.equals(SIP)) {
		String status = getSIPChannelStatus(line.getChannelSpecificID());
		if (status.equals(TelephonyEventMessage.PHONE_LINE_ERROR))
		    inactiveLines.add(line.getID());

	    } else if (channelType.equals(DAHDI)) {
		String status = getDahdiChannelStatus(line.getChannelSpecificID());
		if (status.equals(TelephonyEventMessage.PHONE_LINE_ERROR))
		    inactiveLines.add(line.getID());
	    
	    } else if (channelType.equals(DONGLE)) {
		String status = getDongleChannelStatus(line.getChannelSpecificID());
		if (status.equals(TelephonyEventMessage.PHONE_LINE_ERROR))
		    inactiveLines.add(line.getID());
	    }
	}

	return inactiveLines.toArray(new String[]{});
    }

    public void pingResponse(String ip, boolean status) {
	synchronized(pingLock) {
	    logger.debug("Got ping response: " + ip + " " + status);
	    PingRequest request = new PingRequest(ip, status);
	    pingRequests.put(ip, request);
	    pingLock.notifyAll();
	}
    }

    String getSIPChannelStatus(String address) {
	String ip = address.split(":")[0];
	String retVal;
	
	synchronized(pingLock) {
	    PingRequest request = pingRequests.get(ip);
	    if (request != null) {
		if (request.getTestTime() + SIP_STATUS_STALE_TIMEOUT < System.currentTimeMillis()) {
		    pingRequests.remove(ip);
		} else {
		    if (request.getStatus()) 
			return TelephonyEventMessage.PHONE_LINE_OK;
		    else
			return TelephonyEventMessage.PHONE_LINE_ERROR;
		}
	    }

	    diagUtil.initiatePing(ip, this);
	    try {
		pingLock.wait(DiagnosticUtilities.PING_TIMEOUT);
	    } catch(InterruptedException e) { }
	    logger.debug("Timed out or notified!!");
	    if (pingRequests.get(ip) != null && pingRequests.get(ip).getStatus())
		retVal = TelephonyEventMessage.PHONE_LINE_OK;
	    else {
		if (pingRequests.get(ip) == null) 
		    pingRequests.put(ip, new PingRequest(ip, false));

		retVal = TelephonyEventMessage.PHONE_LINE_ERROR;
	    }
	}
	
	return retVal;
    }

    String getDahdiChannelStatus(String channelStr) {
	int channel;
	try {
	    channel =Integer.parseInt(channelStr);
	    return getDahdiChannelStatus(channel);
	} catch (Exception e) {
	    return TelephonyEventMessage.PHONE_LINE_ERROR;
	}
    }

    protected String getDahdiChannelStatus(int channel){
	try{
	    List<String> output = asteriskServer.executeCliCommand("dahdi show channel "+channel);
	    int alarmState = Integer.parseInt(output.get(11).split(" ")[1]);
	    if (alarmState == 0)
		return TelephonyEventMessage.PHONE_LINE_OK;
	    else
		return TelephonyEventMessage.PHONE_LINE_ERROR;
	} catch (Exception e){
	    logger.error("GetDahdiChannelStatus: Encountered exception:", e);
	    return TelephonyEventMessage.PHONE_LINE_ERROR;
	}
    }

    boolean isDongleInAvailableState(String dongleState) {
	return (dongleState.equals("Free") ||
		dongleState.equals("Dialing") ||
		dongleState.equals("Outgoing") ||
		dongleState.equals("Ring") ||
		dongleState.equals("Incoming"));
    }

    String getDongleChannelStatus(String imei) {
	logger.info("GetDongleChannelStatus: Checking status for imei:" + imei);
	for (String dongleEntry :getAllDongles()) {
	    logger.debug("GetDongleChannelStatus: DongleEntry - " + dongleEntry);
	    dongleEntry = dongleEntry.trim();
	    dongleEntry = dongleEntry.replaceAll("( )+", " ");
	    String dongleID = dongleEntry.split(" ")[0];
	    String dongleState = getDongleState(dongleID);
	    String dongleimei = getDongleImei(dongleID);
	    logger.info("GetDongleChannelStatus: dongleimei=" + dongleimei + " donglestate=" + dongleState);
	    
	    if(dongleimei != null && dongleState !=null && dongleimei.equals(imei) && isDongleInAvailableState(dongleState)) {
		logger.debug("GetDongleChannelStatus: returning ok.");
		return TelephonyEventMessage.PHONE_LINE_OK;
	    }
	}

	return TelephonyEventMessage.PHONE_LINE_ERROR;
    }

    List<String> getAllDongles() {
	try {
	    return asteriskServer.executeCliCommand("dongle show devices");
	} catch (Exception e) {
	    logger.error("GetAllDongles: Error executing CLI command.", e);
	    return new ArrayList<String>();
	}
    }
	
    String getDongleState(String dongleID) {
	try {
	    List<String> dongleStatus = asteriskServer.executeCliCommand("dongle show device state " + dongleID);
	    for (String kvPair :dongleStatus) {
		kvPair = kvPair.trim();
		kvPair = kvPair.replaceAll("( )+", " ");

		if (kvPair.startsWith("State")) {
		    String dongleState = kvPair.split(":")[1];
		    dongleState = dongleState.trim();
		    logger.debug("GetDongleState: Found state: " + kvPair + ". Returning " + dongleState);
		    return dongleState;
		}
	    }
	    return null;
	} catch (Exception e) {
	    logger.error("GetDongleState: Error executing CLI command.", e);
	    return null;
	}
    }

    String getDongleImei(String dongleID) {
	try {
	    List<String> dongleStatus = asteriskServer.executeCliCommand("dongle show device state " + dongleID);
	    for (String kvPair :dongleStatus) {
		kvPair = kvPair.trim();
		kvPair = kvPair.replaceAll("( )+", " ");

		if (kvPair.startsWith("IMEI")) {
		    String dongleimei = kvPair.split(":")[1];
		    dongleimei = dongleimei.trim();
		    logger.debug("GetDongleIMEI: Found imei: " + kvPair + ". Returning " + dongleimei);
		    return dongleimei;
		}
	    }
	    return null;
	} catch (Exception e) {
	    logger.error("GetDongleIMEI: Error executing CLI command.", e);
	    return null;
	}
    }

    public boolean executeCliCommand(String command) {
	try {
	    asteriskServer.executeCliCommand(command);
	    return true;
	} catch (Exception e) {
	    logger.error("ExecuteCliCommand: Error executing CLI command.", e);
	    return false;
	}
    }

    public String getDongleIDFromIMEI(String imei) {
	for (String dongleEntry :getAllDongles()) {
	    logger.debug("GetDongleIDFromIMEI: DongleEntry - " + dongleEntry);
	    dongleEntry = dongleEntry.trim();
	    dongleEntry = dongleEntry.replaceAll("( )+", " ");
	    String dongleimei = dongleEntry.split(" ")[9];
	    String dongleID = dongleEntry.split(" ")[0];
	    logger.debug("GetDongleIDFromIMEI: dongleimei=" + dongleimei + " dongleid=" + dongleID);
	    
	    if(dongleimei.equals(imei)) {
		logger.debug("GetDongleIDFromIMEI: returning last ID.");
		return dongleID;
	    }
	}
	
	return null;
    }

    public boolean acceptCall(String callGUID) {
	if (callGUID == null) {
	    logger.error("AcceptCall: null callGUID.");
	    return false;
	}

	if (medialib == null) {
	    logger.error("AcceptCall: Medialib is null.");
	    return false;
	}

	String filename = medialib.newBulkItemFilename();

	AsteriskChannel channel = channels.get(callGUID);

	if (channel != null) {
	    
	    if (!startMixMonitoringChannel(channel, filename, MONITOR_FORMAT)) {
		logger.error("AcceptCall: Could not start mix monitoring the channel.");
		return false;
	    }
	    
	    callMonitorFileMap.put(callGUID, filename + "." + MONITOR_FORMAT);
	    if (isConfActive()){
		logger.info("AcceptCall: adding " + channel.getName() + " to Conference");
		addChannelToConference(channel);
		unmuteCall(callGUID);
		dialedOutInConf = false;
		return true;
	    } else {
		logger.info("AcceptCall: joining " + channel.getName() + " to ALSA");
		return previewCall(callGUID);
	    }

	} else {
	    logger.error("AcceptCall: Could not accept: channel: " + channel);
	    return false;
	}
    }

    public synchronized boolean previewCall(String callGUID) {

	if (callGUID == null){
	    logger.error("PreviewCall: null callGUID.");
	    return false;
	}
	
	AsteriskChannel channel = channels.get(callGUID);
	
	if (channel == null) {
	    logger.error("PreviewCall: Could not locate call: "+callGUID);
	    return false;
	}

	//Unless we find a channel linked to preview channel we default to previewchannelhungup=true
	previewChannelHungup = true;

	for (AsteriskChannel oldChannel: asteriskServer.getChannels()){
	    
	    if (!oldChannel.getId().equals(callGUID)){
		AsteriskChannel linkedChannel = oldChannel.getLinkedChannel();
		if (linkedChannel == null)
		    continue;
	       
		if (isAlsaChannel(linkedChannel)) {
		    //System.err.println("--- preview call acq previewchannellock");
		    synchronized (previewChannelLock){
			previewChannelHungup = false;
			previewChannelId = linkedChannel.getId();
		    }
		    holdCall(oldChannel.getId());
		}
	    }
	}
 
	//System.err.println("----- abt to acq previewchannellock");
	synchronized (previewChannelLock) {
	    //System.err.println("---------prevChanHungup: "+previewChannelHungup);
	    if (!previewChannelHungup){
		try{
		    //System.err.println("------------- waiting for previewchannel to hangup");
		    previewChannelLock.wait(3000);
		} catch (Exception e){
		    logger.error("PreviewCall: Exception.", e);
		}
	    }
	    previewChannelHungup = false;
	}
	
	//System.err.println("-----------about to redirect." + EXTN_PREVIEW);

	try {
	    redirectChannel(channel, CONTEXT_ALSA, EXTN_PREVIEW, PRIORITY_ALSA);
	} catch(Exception e) {
	    logger.error("PreviewCall: Got exception while redirecting channel.", e);
	    return false;
	}
	
	//unpauseMixMonitoringChannel(channel);

	previewCall = callGUID;
	return true;
    }

    AsteriskChannel getPreviewChannel(){
	for (AsteriskChannel channel: asteriskServer.getChannels()) {
	    if (isPreviewChannel(channel)) {
		return channel;
	    }
	}
	return null;
    }

    AsteriskChannel getPreviewCallChannel(){
	AsteriskChannel previewChannel = getPreviewChannel();
	if (previewChannel != null)
	    return previewChannel.getLinkedChannel();
	else
	    return null;
    }

    AsteriskChannel getPlayoutChannel(){
	for (AsteriskChannel channel: asteriskServer.getChannels()){
	    if (isPlayoutChannel(channel)){
		return channel;
	    }
	}
	return null;
    }

    AsteriskChannel getPlayoutCallChannel(){
	AsteriskChannel playoutChannel = getPlayoutChannel();
	if (playoutChannel != null)
	    return playoutChannel.getLinkedChannel();
	else
	    return null;
    }

    boolean isDahdiPseudoChannel(AsteriskChannel channel) {
	return (channel.getName().startsWith("DAHDI/pseudo"));
    }

    boolean isLocalChannel(AsteriskChannel channel) {
	return isLocalChannel(channel.getName());
    }

    boolean isLocalChannel(String channelName) {
	return channelName.startsWith("Local/");
    }

    boolean isPreviewChannel(AsteriskChannel channel) {
	CallerId callerID = channel.getCallerId();
	if (callerID != null) {
	    String callerIDNumber = callerID.getNumber();
	    if (callerIDNumber != null && callerIDNumber.equals(EXTN_PREVIEW) && isAlsaChannel(channel))
		return true;
	}
	
	String extension = getChannelVariable(channel, ALSA_CHAN_TYPE);
	if (extension != null && extension.equals(EXTN_PREVIEW) && isAlsaChannel(channel))
	    return true;

	return false;
    }

    boolean isPlayoutChannel(AsteriskChannel channel) {
	CallerId callerID = channel.getCallerId();
	if (callerID != null) {
	    String callerIDNumber = callerID.getNumber();
	    if (callerIDNumber != null && callerIDNumber.equals(EXTN_PLAYOUT) && isAlsaChannel(channel))
		return true;
	}

	String extension = getChannelVariable(channel, ALSA_CHAN_TYPE);
	if (extension != null && extension.equals(EXTN_PLAYOUT) && isAlsaChannel(channel))
	    return true;

	return false;
    }

    boolean isDongleChannel(AsteriskChannel channel){
	return (channel.getName().startsWith(DONGLE + "/"));
    }

    boolean isAlsaChannel(AsteriskChannel channel){
	return isAlsaChannel(channel.getName());
    }

    boolean isAlsaChannel(String channelName){
	return (channelName.startsWith(ALSA + "/"));
    }

    boolean isConfActive() {
	return (meetMeRoom.getUsers().size() > 0);
    }
    
    protected boolean previewConf() {
	return connectConfToAlsa(EXTN_PREVIEW);
    }
    
    protected boolean playoutConf() {
	return connectConfToAlsa(EXTN_PLAYOUT);
    }

    protected boolean connectConfToAlsa(String extension) {
	OriginateAction action = new OriginateAction();
	String channelString = "Local/"+extension+"@"+CONTEXT_ALSA;
	action.setChannel(channelString);
	action.setContext(CONTEXT_CONF);
	action.setExten(CONF_ROOMNO);
	action.setPriority(1);
	action.setTimeout(30000L);
	action.setVariable(ALSA_CHAN_TYPE, extension);
	action.setAsync(true);
	
	try {
	    synchronized (confLock){
		asteriskServer.originateAsync(action, this);
		try{
		    confUserId = extension;
		    //System.err.println("------waiting for conf to alsa: "+confUserId);
		    confLock.wait(10000);
		    //System.err.println("------returning from conf to alsa");
		} catch (Exception e) {}
	    }
	} catch (Exception e){
	    logger.error("ConnectConfToAlsa: Encountered exception: ", e);
	    return false;
	}
	
	return true;
    }

    protected boolean monitorConf() {
	if (medialib == null) {
	    logger.error("MonitorConf: Medialib is null.");
	    return false;
	}

	String filename = medialib.newBulkItemFilename();
	String filepath = libDir + File.separator + filename;

	OriginateAction action = new OriginateAction();
	String channelString = "Local/"+CONF_ROOMNO+"@"+CONTEXT_CONF;
	action.setChannel(channelString);
	action.setContext(CONTEXT_CONF_MONITOR);
	action.setExten(EXTN_DUMMY);
	action.setPriority(1);
	action.setTimeout(30000L);
	action.setAsync(true);
	action.setVariable("MONITOR_FILE", filepath);
	action.setVariable("MONITOR_FORMAT", MONITOR_FORMAT);

	try {
	    synchronized (confLock){
		asteriskServer.originateAsync(action, this);
		try{
		    confUserId = channelString;
		    //System.err.println("---------waiting for conf to monitor"+confUserId);
		    confLock.wait(10000);
		    //System.err.println("---------returning from conf to monitor");
		} catch (Exception ex){}
	    }
	} catch (Exception e){
	    logger.error("monitorConf: Encountered exception: ", e);
	    return false;
	}
	
	callMonitorFileMap.put(CONF_ROOMNO, filename + "." + MONITOR_FORMAT);
	return true;
    }

    protected boolean holdCurrentAlsaChannel(){
	for (AsteriskChannel channel: asteriskServer.getChannels()){
	    if (isAlsaChannel(channel)){
		AsteriskChannel linkedChannel = channel.getLinkedChannel();
		if (linkedChannel != null){
		    return holdCall(linkedChannel.getId());
		}
	    }
	}

	return true;
    }

    public synchronized boolean hangupCall(String callGUID) {
	AsteriskChannel channel = channels.remove(callGUID);
	String filename = callMonitorFileMap.remove(callGUID);

	boolean isOutgoing = outgoingCalls.remove(callGUID);

	if (channel == null) 
	    return false;

	logger.info("HangupCall: Hanging up " + channel.getName());
	try {
	    return hangupChannel(channel);
	    
	} catch(Exception e) {
	    channels.put(callGUID, channel);
	    if (isOutgoing)
		outgoingCalls.add(callGUID);
	    callMonitorFileMap.put(callGUID, filename);
	    logger.error("HangupCall: Got exception while hanging up channel.", e);
	    return false;
	}
	    
    }

    String getOriginateChannelString(RSCallerID outgoingCall) {
	String originateChannelString = "";
	TelephonyLine phoneline = stationConfig.getTelephonyLine(outgoingCall.getLineID());
	String channelType = "SIP"; //default channel type
	if (phoneline != null) {
	    channelType = phoneline.getChannelType();
	}

	if (channelType.equals(SIP)) {
	    if (phoneline != null) {
		String outPrefix = phoneline.getOutPrefix();
		if(outPrefix.equals(TelephonyLine.WILDCARD)) 
		    outPrefix = "";
		
		originateChannelString = channelType + "/" + outPrefix + outgoingCall.getCallerID() + "@" + phoneline.getChannelSpecificID();
	    } else {
		originateChannelString = channelType + "/" + outgoingCall.getCallerID();
	    }
	} else if(channelType.equals(DONGLE)) {
	    originateChannelString = channelType + "/i:" + phoneline.getChannelSpecificID() + "/" + outgoingCall.getCallerID();
	} else if (channelType.equals(DAHDI)) {
	    originateChannelString = channelType + "/" + phoneline.getChannelSpecificID() + "/" + phoneline.getOutPrefix() + outgoingCall.getCallerID();
	}

	return originateChannelString;
    }	


    public synchronized boolean dial(RSCallerID outgoingCall, String guid) {
	//AsteriskChannel channel;
	OriginateAction originateAction = new OriginateAction();

	String originateString = getOriginateChannelString(outgoingCall);
	originateAction.setChannel(originateString);
	if (isConfActive()){
	    originateAction.setContext(CONTEXT_CONF);
	    originateAction.setExten(CONF_ROOMNO);
	    originateAction.setPriority(1);

	    dialedOutInConf = true;

	} else {

	    holdCurrentAlsaChannel();
 	    originateAction.setContext(CONTEXT_ALSA);
	    originateAction.setExten(EXTN_PREVIEW);
	    originateAction.setPriority(1);

	}

	originateAction.setTimeout(30000L);
	originateAction.setAsync(true);
	originateAction.setVariable(GUID, guid);
	originateAction.setVariable(CALLERID, outgoingCall.getCallerID());
	originateAction.setVariable(PHONE_LINE_ID, outgoingCall.getLineID());

	try{
	    asteriskServer.originateAsync(originateAction, this);
	} catch(Exception e) {
	    logger.error("Dial: Got exception.", e);
	    return false;
	}

	return true;
    }

    public void destroyAsteriskChannels() {
    	hangupChannelsGracefully(true);
    }

    public synchronized boolean destroyPhoneCalls() {

	if (!isConnected())
	    return false;

	ArrayList<AsteriskChannel> channelsToHangup = new ArrayList<AsteriskChannel> (channels.values());

	for (AsteriskChannel channel : channelsToHangup) {
	    try { 
		logger.debug("DestroyPhoneCalls: Hanging up " + channel.getName());
		hangupChannel(channel);
	    } catch(Exception e) {
		logger.error("DestroyPhoneCalls: Got exception while hanging up channel.", e);
		if (e instanceof ManagerCommunicationException)
		    return false;
	    }
	}
	
	clearCallRecords();
	return true;
    }

    protected void clearCallRecords() {
	channels.clear();
	outgoingCalls.clear();
	callMonitorFileMap.clear();
    }

    public void onManagerEvent(ManagerEvent event) {
	logger.debug("OnManagerEvent: " + event.toString());

	if (event instanceof DisconnectEvent) {
	    //Practically seen to be thrown only when asterisk server actually goes down
	    //not seen when network cable is disconnected
	    connectionDown();
	} else if (event instanceof HangupEvent) {
	    HangupEvent hangupEvent = (HangupEvent)event;
	    handleHangupEvent(hangupEvent);
	} else if (event instanceof MusicOnHoldEvent) {
	    MusicOnHoldEvent holdEvent = (MusicOnHoldEvent) event;
	    handleMusicOnHoldEvent(holdEvent);
	} else if (event instanceof MeetMeLeaveEvent) {
	    MeetMeLeaveEvent leaveEvent = (MeetMeLeaveEvent) event;
	    handleMeetMeLeaveEvent(leaveEvent);
	    
	} 
	/*
	else if (event instanceof AlarmEvent) {
	    AlarmEvent alarmEvent = (AlarmEvent) event;
	    handleAlarmEvent(alarmEvent);
	    
	} else if (event instanceof AlarmClearEvent) {
	    AlarmClearEvent clearEvent = (AlarmClearEvent) event;
	    handleAlarmClearEvent(clearEvent);

	} else if (event instanceof CdrEvent) {

	}
	*/
    }
    
    /*
    protected void handleAlarmEvent(AlarmEvent event){
	if (event.getAlarm().equals("Red Alarm")){
	    for (TelephonyLibListener listener: listeners.values())
		listener.sendDahdiChannelNotification(TelephonyEventMessage.DAHDI_ERROR, event.getChannel());
	} else {
	    logger.error("HandleAlarmEvent: Unknown event type:"+event);
	}
    }

    protected void handleAlarmClearEvent(AlarmClearEvent event){
	for (TelephonyLibListener listener: listeners.values())
	    listener.sendDahdiChannelNotification(TelephonyEventMessage.DAHDI_OK, event.getChannel());
    }
    */

    protected void handleMusicOnHoldEvent(MusicOnHoldEvent event){
	if (event.getUniqueId().equals(channelOnHoldId) && event.getState().equals("Start")){
	    synchronized (channelHoldLock){
		channelOnHold = true;
		channelHoldLock.notifyAll();
		//System.err.println("---------notified: mohevent:" + channelOnHoldId);
	    }
	}
    }
    
    
    protected void handleHangupEvent(HangupEvent hangupEvent) {
	logger.info("HandleHangupEvent: "+hangupEvent);

	if (hangupEvent.getUniqueId().equals(hangupChannelId)){
	    synchronized (hangupLock){
		hangupChannelId = "";
		hangupLock.notifyAll();
		//System.err.println("------------notifying hangup channel: " + hangupEvent.getChannel());
	    }
	}

	AsteriskChannel channel = channels.get(hangupEvent.getUniqueId());

	if (channel == null) {
	    logger.debug("HandleHangupEvent: Got hangup event for null channel.");
	    return;
	}
	
	logger.info("HandleHangupEvent: HUNGUP channel:" + channel.getName());

	if (channel.getId().equals(previewChannelId)){
	    synchronized (previewChannelLock){
		previewChannelHungup = true;
		previewChannelLock.notifyAll();
		//System.err.println("-------previewchannel closed." + prevCount++);
	    }
	}

	if (channel.getId().equals(redirectChannelId)){
	    synchronized (redirectLock){
		redirectChannelId = "__DUMMY__";
	    }
	}

	channels.remove(channel.getId());
	outgoingCalls.remove(channel.getId());
	
	String hangupCause = getHangupCause(hangupEvent);

	for (TelephonyLibListener listener : listeners.values()) {
	    if (listener != null)
		listener.callerHungup(channel.getId(), hangupCause);
	}

	callMonitorFileMap.remove(channel.getId());
	
    }
    
    void handleMeetMeLeaveEvent(final MeetMeLeaveEvent leaveEvent){
	executor.execute(new Runnable(){
		public void run(){
		    synchronized (confLock){
			if (leaveEvent.getUniqueId().equals(confUserId)){
			    confUserId = "__DUMMY__";
			    confLock.notifyAll();
			}
		    }
		}
	    });
    }

    protected String getHangupCause(HangupEvent hangupEvent) {
	String causeText = hangupEvent.getCauseTxt();

	if (causeText == null) {
	    return CAUSE_UNKNOWN;
	} else if (causeText.indexOf("no answer") != -1) {
	    return CAUSE_NOANSWER;
	} else if (causeText.indexOf("Normal Clearing") != -1) {
	    return CAUSE_NORMAL;
	} else if (causeText.indexOf("Channel not implemented") != -1) {
	    return CAUSE_CHANUNAVAIL;
	}

	return CAUSE_UNKNOWN;
    }

    boolean isSMS(AsteriskChannel channel) {
	String sms = getChannelVariable(channel, "SMS_BASE64");

	if(sms != null)
	    return true;
	else
	    return false;
    }

    void notifySMSListeners(AsteriskChannel channel) {
	String sms = getChannelVariable(channel, "SMS_BASE64");
	String decodedSMS = new String((new Base64()).decodeBase64(sms));
	String phoneNo = getChannelVariable(channel, "CALLERID(NUM)");
	String dongleID = getChannelVariable(channel, "DONGLENAME");

	logger.info("NotifySMSListener: message=" + decodedSMS + " callerid=" + phoneNo + ", dongle=" + dongleID);

	for(AsteriskSMSListener listener :smsListeners)
	    listener.newSMS(dongleID, decodedSMS, phoneNo);
    }

    public void onNewAsteriskChannel(AsteriskChannel channel) {
        logger.info("OnNewAsteriskChannel: " + channels.size() + ": " + channel.getName());

	if(isSMS(channel)) {
	    notifySMSListeners(channel);
	    return;
	}

	ChannelState channelState = channel.getState();
	
	if (state == OFFLINE && !outgoingCalls.contains(channel.getId())) {
	    logger.debug("OnNewAsteriskChannel: We are offline and " + channel.getName() + " is not outgoing.");
	    return;
	}

	if (channelState != ChannelState.HUNGUP) {
	    if (isExternalChannel(channel.getName())) {
		channels.put(channel.getId(), channel);
		channel.addPropertyChangeListener(AsteriskChannel.PROPERTY_LINKED_CHANNEL, this);
		channel.addPropertyChangeListener(AsteriskChannel.PROPERTY_STATE, this);
		logger.debug("OnNewAsteriskChannel: Added channel with id:" + channel.getId());
	    } else {
		logger.debug("OnNewAsteriskChannel: Ignoring internal channel");
	    }
	} else {
	    logger.info("OnNewAsteriskChannel: Ignoring hung channel");
	}
    }

    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
	String propertyName = propertyChangeEvent.getPropertyName();
	logger.info("PropertyChange: PropertyName: " + propertyName + " value:" + propertyChangeEvent.getNewValue());
	
	AsteriskChannel channel = (AsteriskChannel)propertyChangeEvent.getSource();

	if (propertyName.equals(AsteriskChannel.PROPERTY_STATE)) {
	    if (propertyChangeEvent.getNewValue() instanceof ChannelState) {
		ChannelState channelState = (ChannelState)(propertyChangeEvent.getNewValue());
		logger.debug("New state = " + channelState);
		if (channelState == ChannelState.HUNGUP) {

		} else if (channelState == ChannelState.RING || channelState == ChannelState.RINGING || 
			  (isDongleChannel(channel) && channelState == ChannelState.UP)) {

		    if (channel != null && isExternalChannel(channel.getName())) {
			
			if (state == OFFLINE && !outgoingCalls.contains(channel.getId())) {
			    logger.info("PropertyChange: We are offline and " + channel.getName() + " is not outgoing.");
			    return;
			}

			if (outgoingCalls.contains(channel.getId())) {
			    for (TelephonyLibListener listener : listeners.values()) {
				if (listener != null)
				    listener.dialNumberCallback(channel.getId(), RINGING, "");
			    }
			} else {
			    RSCallerID id = getCallerIDForChannel(channel);
			    for (TelephonyLibListener listener : listeners.values()) {
				if (listener != null)
				    listener.newCall(channel.getId(), id);
			    }
			}
		    } else {
			logger.info("PropertyChange: Internal channel");
		    }

		}
	    }
	} else if (propertyName.equals(AsteriskChannel.PROPERTY_LINKED_CHANNEL)) {
	    if (channel.getId().equals(redirectChannelId)){
		synchronized (redirectLock){
		    redirectChannelId = "";
		    redirectLock.notifyAll();
		    //System.err.println("-------------- notified redirects:" + channel.getName());
		}
	    }
	}
    }

     
    protected boolean isExternalChannel(String name) {
	for (String extChannelType : externalChannelTypes) {
	    if (name.indexOf(extChannelType) != -1) {
		return true;
	    }
	}

	return false;
    }

    public void onNewQueueEntry(AsteriskQueueEntry entry) {
        logger.info("OnNewQueueEntry: " + entry);
    }

    public void onNewAgent(AsteriskAgentImpl agent) {
        logger.info("OnNewAgent: " + agent);
    }

    public void onNewMeetMeUser(final MeetMeUser user) {
        logger.info("OnNewMeetMeUser: " + user);
	final AsteriskChannel channel = user.getChannel();
	final String name = channel.getName();

	if (!isLocalChannel(channel) && !isAlsaChannel(channel))
	    declareNewConferenceMember(channel.getId());

	try{

	    if (dialedOutInConf){
		unmuteCall(channel.getId());
	    } else {

		if (!isLocalChannel(channel) && !isAlsaChannel(channel) && !channel.getId().equals(previewCall)) {
		    user.mute();
		}
	    }

	} catch (Exception e){
	    logger.error("OnNewMeetMeUser: Encountered exception.", e);
	}

	executor.execute(new Runnable(){
		public void run(){
		    synchronized (confLock){
			boolean isAlsaConfUser = false;
			
			if (channel.getName().startsWith("Local/"+confUserId+"@"+CONTEXT_ALSA)) {
			    isAlsaConfUser = true;
			    
			} else {
			    String extension = getChannelVariable(channel, ALSA_CHAN_TYPE);
			    if (extension != null && extension.equals(confUserId))
				isAlsaConfUser = true;
			}
			
			if (channel.getId().equals(confUserId) 
			    || isAlsaConfUser 
			    || channel.getName().startsWith(confUserId)){

			    confUserId = "___DUMMY__";
			    confLock.notifyAll();
			}
		    }
		}
	    });
    }


    protected String getChannelVariable(AsteriskChannel channel, String name) {
	int attempts;
	String value = null;
	for (attempts = 0; attempts < MAX_CHANNEL_EXCEPTION_ATTEMPTS; attempts++) {
	    try {
		value = channel.getVariable(name);
		if (value != null)
		    break;
	    } catch (NoSuchChannelException channelException) {
		logger.warn("GetChannelVariable: Got channel exception. Channel: " + channel.getName() + " Attempt number: " + attempts, channelException);

	    } catch (Exception e) {
		logger.error("GetChannelVariable: Got exception when getting variable: " + channel.getName() + ".", e);
		return null;
	    }

	    try {
		Thread.sleep(CHANNEL_EXCEPTION_RETRY_INTERVAL);
	    } catch(Exception e) {}
	}

	return value;
    }

    /*
    protected boolean setChannelVariable(AsteriskChannel channel, String name, String value) {
	int attempts;

	for (attempts = 0; attempts < MAX_CHANNEL_EXCEPTION_ATTEMPTS; attempts++) {
	    try {
		channel.setVariable(name, value);
		return true;
	    } catch(NoSuchChannelException channelException) {
		logger.warn("SetChannelVariable: Got channel exception. Channel: " + channel.getName() + " Attempt number: " + attempts);

	    } catch(Exception e) {
		logger.error("SetChannelVariable: Got exception when setting variable: " + channel.getName() + ".", e);
		return false;
	    }

	    try {
		Thread.sleep(CHANNEL_EXCEPTION_RETRY_INTERVAL);
	    } catch(Exception e) {}
	}

	logger.error("SetChannelVariable: Could not set channel variable in " + attempts + " attempts. Giving up.");
	return false;
    }
    */

    protected boolean startMixMonitoringChannel(AsteriskChannel channel, String filename, String format) {
	int attempts;
	ManagerResponse response;

	String filepath = libDir + File.separator + filename;
	String commandString = "mixmonitor start " + channel.getName() + " " + filepath + "." + format +",b";

	CommandAction mixMonitorAction = new CommandAction(commandString);
	
	logger.info("StartMixMonitoringChannel: Sending CLI command: " + commandString);

	for (attempts = 0; attempts < MAX_CHANNEL_EXCEPTION_ATTEMPTS; attempts++) {
	    response = sendActionToManager(mixMonitorAction);
	    if ((response != null) && !(response instanceof ManagerError))
		return true;
	    else
		logger.warn("StartMixMonitoringChannel: Could not start mix monitoring in " + (attempts+1) + " attempts.");
	    
	    try {
		Thread.sleep(CHANNEL_EXCEPTION_RETRY_INTERVAL);
	    } catch(Exception e) {}
	}
	
	return false;
    }

    protected boolean hangupChannel(AsteriskChannel channel){
	try{
	    synchronized (hangupLock){
		hangupChannelId = channel.getId();
		channel.hangup();
		hangupLock.wait(10000);
	    }
	} catch (Exception e){
	    
	}
	return true;
    }

    protected boolean redirectChannel(AsteriskChannel channel, String context, String extn, int priority){
	try{
	    synchronized (redirectLock){
		redirectChannelId = channel.getId();
		boolean retVal = redirectChannelAsync(channel, context, extn, priority);
		if (!retVal)
		    return false;
		try{
		    //System.err.println("------------waiting for redirect channel: "+channel.getName());
		    redirectLock.wait(10000);
		    //System.err.println("------------returning from redirect channel: "+channel.getName());
		} catch (Exception ex){}
	    }
	} catch (Exception e){
	    logger.error("RedirectChannel: Encountered exception: ", e);
	    return false;
	}

	return true;
    }

    protected boolean redirectChannelAsync(AsteriskChannel channel, String context, String extn, int priority) {
	int attempts;
	for (attempts = 0; attempts < MAX_CHANNEL_EXCEPTION_ATTEMPTS; attempts++) {
	    try {
		//System.err.println("---redirecting: "+channel + " context: " + context + " extn: "+ extn + " prio: " + priority);
		channel.redirect(context, extn, priority);
		return true;

	    } catch(NoSuchChannelException channelException) {
		
		logger.warn("RedirectChannelAsync: Got channel exception. Channel: " + channel.getName() + " Attempt number: " + attempts);
		try {
		    Thread.sleep(CHANNEL_EXCEPTION_RETRY_INTERVAL);
		} catch(Exception e) {}

	    } catch(Exception e) {
		logger.error("RedirectChannelAsync: Got exception when redirecting channel: " + channel.getName() + ".", e);
		return false;
	    }
	}

	logger.error("RedirectChannelAsync: Could not redirect channel in " + MAX_CHANNEL_EXCEPTION_ATTEMPTS + " attempts.");
	return false;
    }

    protected RSCallerID getCallerIDForChannel(String callGUID) {
	AsteriskChannel channel = channels.get(callGUID);
	return getCallerIDForChannel(channel);
    }

    protected RSCallerID getCallerIDForChannel(AsteriskChannel channel) {
	RSCallerID id = null;

	if (channel != null) {
	    if (channel.getCallerId() != null && channel.getCallerId().getNumber() != null) {
		id = new RSCallerID();
		id.setCallerID(channel.getCallerId().getNumber());
		id.setLineID(getLineIDForIncomingChannel(channel));
	    } else {
		id = new RSCallerID(getChannelVariable(channel, PHONE_LINE_ID), getChannelVariable(channel, CALLERID));
	    }
	} 
	
	return id;
    }
    
    String getLineIDForIncomingChannel(AsteriskChannel channel) {
	String namePrefix = channel.getName().split("-")[0];
	String channelType = namePrefix.split("/")[0];
	String dnid, inPrefix;
	String channelSpecificID;
	
	channelSpecificID = getChannelSpecificID(channel);
	dnid = getChannelVariable(channel, DNID);

	if (dnid == null) {
	    logger.error("GetTelephonyLineForChannel: DNID must be set for all incoming calls.");
	    inPrefix = TelephonyLine.UNKNOWN;
	} else {
	    inPrefix = dnid.substring(0,TelephonyLine.IN_PREFIX_LENGTH);
	}

	TelephonyLine dummyline = new TelephonyLine(channelType, channelSpecificID, inPrefix);
	
	for (TelephonyLine line: stationConfig.getAllTelephonyLines()) {
	    if (line.equals(dummyline))
		return line.getID();
	}
	    
	return TelephonyLine.UNKNOWN;
    }

    String getChannelSpecificID(AsteriskChannel channel) {
	String namePrefix = channel.getName().split("-")[0];
	String channelType = namePrefix.split("/")[0];
	if (channelType.equals(SIP)) {
	    String sipuri = getChannelVariable(channel, SIPURI);
	    String[] tmpArr = sipuri.split("@");
	    String ipAddress = tmpArr[1];
	    return ipAddress;
	} else if(channelType.equals(DONGLE)) {
	    return getChannelVariable(channel, DONGLEIMEI);
	} else {
	    //add more channel types here as and when used
	    logger.error("GetChannelSpecificID: Unknown channel type: " + channelType);
	    return TelephonyLine.UNKNOWN;
	}
    }

    public boolean isConferenceOn() {
	synchronized(meetMeAssignmentLock) {
	    if (meetMeRoom == null) 
		return false;
	    else
		return (meetMeRoom.getUsers().size() > 0);
	}
    }

    
    public synchronized Hashtable<String,String> getAllCalls() {
	Hashtable<String,String> calls = new Hashtable<String,String>();
	
	for (AsteriskChannel channel: channels.values()) {
	    CallerId callerID = channel.getCallerId();
	    String number = null;
	    if (callerID != null) {
		number = callerID.getNumber();
	    }
	    
	    calls.put(channel.getId(), number);
	}

	return calls;
    }

    public synchronized String getOnAirCall() {
	AsteriskChannel playoutChannel = getPlayoutChannel();
	if (playoutChannel != null) {
	    AsteriskChannel linkedChannel = playoutChannel.getLinkedChannel();
	    if (linkedChannel != null) {
		return linkedChannel.getId();
	    }
	}

	return null;
    }

    public synchronized String getOffAirCall() {
	AsteriskChannel previewChannel = getPreviewChannel();
	if (previewChannel != null) {
	    AsteriskChannel linkedChannel = previewChannel.getLinkedChannel();
	    if (linkedChannel != null) {
		return linkedChannel.getId();
	    }
	}

	return null;
    }

    public synchronized boolean isOnAir() {
	return (getPlayoutChannel() != null);
    }

    public synchronized boolean isOffAir() {
	return (getPreviewChannel() != null);
    }

    public String getMonitorFile(String callGUID) {
	if (callGUID == null || callGUID.equals(""))
	    return null;

	synchronized(callMonitorFileMap){
	    return callMonitorFileMap.get(callGUID);
	}
    }

    public String getConferenceMonitorFile() {
	synchronized(callMonitorFileMap){
	    return callMonitorFileMap.get(CONF_ROOMNO);
	}
    }

    void declareTelephonyDown() {
	for (TelephonyLibListener listener : listeners.values()) {
	    if (listener != null)
		listener.telephonyDown();
	}
    }

    void declareTelephonyUp() {
	for (TelephonyLibListener listener : listeners.values()) {
	    if (listener != null)
		listener.telephonyUp(this);
	}
    }

    void declareNewConferenceMember(String guid) {
	for (TelephonyLibListener listener : listeners.values()) {
	    if (listener != null)
		listener.newConferenceMember(guid);
	}
    }

    void connectionUp() {
	/*
	if (regChecker != null) {
	    regChecker.enableRegCheck();
	}
	*/

	declareTelephonyUp();
    }

    void connectionDown() {
	if (connectionChecker != null) {
	    connectionChecker.setLinkDown();
	}

	/*
	if (regChecker != null) {
	    regChecker.disableRegCheck();
	}
	*/

	declareTelephonyDown();
	clearCallRecords();
	setState(OFFLINE);
    }
    
    void ataNotRegistered() {
	connectionDown();
    }

    void setState(int newState) {
	String status;

	prevState = state;
	state = newState;
	logger.info("SetState: oldState=" + prevState + " => newState=" + state);

	if ( (statusFile == null) || (offlineStatus == null) || (onlineStatus == null)) {
	    logger.error("SetState: Cannot write to telephony status file:" + statusFile + 
			 " offlineStatus:" + offlineStatus + " onlineStatus:" + onlineStatus);
	    return;
	}

	if (state == OFFLINE)
	    status = offlineStatus;
	else
	    status = onlineStatus;

	FileUtilities.writeToFile(statusFile, new String[]{status}, false);
    }

    String getTempGUIDFromErrorMessage(String message) {
	
	String guid = message.substring(message.indexOf("'") + 1);
	guid = guid.substring(0, guid.indexOf("'"));
	guid = guid.replaceAll("/","");

	return guid;
    }

    /*
    protected boolean sendDTMFOnChannel(AsteriskChannel channel, String dtmfString) {
	AsteriskChannel linkedChannel = channel.getLinkedChannel();
	int attempts;
	logger.info("SendDTMFOnChannel: sending dtmf: " + dtmfString);
	if (linkedChannel == null) {
	    logger.error("SendDTMFOnChannel: Linked channel for " + channel.getName() + " is null.");
	    return false;
	}

	for (attempts = 0; attempts < MAX_CHANNEL_EXCEPTION_ATTEMPTS; attempts++) {
	    try {
		channel.playDtmf(dtmfString);
		return true;
	    } catch(NoSuchChannelException channelException) {
		
		logger.warn("SendDTMFOnChannel: Got channel exception. Channel: " + channel.getName() + " Attempt number: " + attempts);
		try {
		    Thread.sleep(CHANNEL_EXCEPTION_RETRY_INTERVAL);
		} catch(Exception e) {}

	    } catch(Exception e) {
		logger.error("SendDTMFOnChannel: Got exception when redirecting channel: " + channel.getName() + ".", e);
		return false;
	    }
	}

	logger.error("SendDTMFOnChannel: Could not redirect channel in " + MAX_CHANNEL_EXCEPTION_ATTEMPTS + " attempts.");
	return false;
    }
    */

    /*
    protected String getChannelTypeNumber(String channelName) {
	for (String number : RSCallerID.ChannelTypes.keySet()) {
	    String typeString = RSCallerID.ChannelTypes.get(number);
	    if (channelName.startsWith(typeString + "/"))
		return number;
	}

	return null;
    }
    */

    public synchronized void setMediaLib(MediaLib medialib) {
	this.medialib = medialib;
    }

    /*OriginateCallback implementation*/
    public void onDialing(AsteriskChannel channel) {
	logger.info("OnDialing: " + channel.getName());

	if (isLocalChannel(channel) || isAlsaChannel(channel)){
	    return;
	}

	String guid = getChannelVariable(channel, GUID);

	outgoingCalls.add(channel.getId());

	for (TelephonyLibListener listener : listeners.values()) {
	    if (listener != null)
		listener.dialNumberCallback(channel.getId(), DIALING, guid);
	}
    }

    //XXX: Note endpointtype+extension is not unique for a call. 
    //The on failure event only returns this information and the guid supplied 
    //by us is lost. Unique call identification is not guaranteed when
    //updating internal structures in response to on failure. Avoid this as much
    //as possible.

    public void onFailure(LiveException e) {
	logger.error("OnFailure: " + e.getMessage());

	String callTempGUID = getTempGUIDFromErrorMessage(e.getMessage());
	
	if (isLocalChannel(callTempGUID) || isAlsaChannel(callTempGUID))
	    return;

	if (e.toString().contains(ERROR_NO_CHANNEL)) {
	    for (TelephonyLibListener listener : listeners.values()) {
		if (listener != null)
		    listener.dialNumberCallback(callTempGUID, FAILURE, ERROR_NO_CHANNEL);
	    } 
	} else {
	    for (TelephonyLibListener listener : listeners.values()) {
		if (listener != null)
		    listener.dialNumberCallback(callTempGUID, FAILURE, RSController.ERROR_UNKNOWN);
	    }
	}
    }

    public synchronized void onSuccess(AsteriskChannel channel) {
	logger.info("OnSuccess: " + channel.getName());

	String filename = "";
	if (isLocalChannel(channel) || isAlsaChannel(channel))
	    return;

	if (!outgoingCalls.contains(channel.getId())) {
	    onDialing(channel);
	}

	channels.put(channel.getId(), channel);

	if (medialib == null) {
	    logger.error("OnSuccess: Medialib is null. Cant monitor call.");
	} else {
	    filename = medialib.newBulkItemFilename();
	    if (!startMixMonitoringChannel(channel, filename, MONITOR_FORMAT))
		filename = null;
	}
	
	if (filename != null)
	    callMonitorFileMap.put(channel.getId(), filename + "." + MONITOR_FORMAT);

	for (TelephonyLibListener listener : listeners.values()) {
	    if (listener != null)
		listener.dialNumberCallback(channel.getId(), SUCCESS, "");
	}
	
    }

    public void onBusy(AsteriskChannel channel) {
	logger.info("OnBusy: " + channel.getName());

	if (isLocalChannel(channel) || isAlsaChannel(channel))
	    return;

	outgoingCalls.remove(channel.getId());

	for (TelephonyLibListener listener : listeners.values()) {
	    if (listener != null)
		listener.dialNumberCallback(channel.getId(), BUSY, "");
	}
    }

    public void onNoAnswer(AsteriskChannel channel) {
	logger.info("OnNoAnswer: " + channel.getName());

	/*
	Don't need to do this. Things are taken care of by hangup event.
	outgoingCalls.remove(channel.getId());

	for (TelephonyLibListener listener : listeners.values()) {
	    if (listener != null)
		listener.dialNumberCallback(channel.getId(), NOANSWER, "");
	}
	*/
    }

    /*Originate callback implementation ends here*/
    
    public class TelephonyConnectionChecker implements Runnable {
	long startTime;
	long refreshTime = TelephonyLib.ATTEMPT_TELEPHONY_UP_INTERVAL;
	AtomicBoolean isLinkUp = new AtomicBoolean(true);
	Object linkUpSyncObject = new Object();
	LogUtilities logger;
	protected Thread thread;
	int failCount;
	

	public TelephonyConnectionChecker(String logPrefix) {
	    logger = new LogUtilities(logPrefix + ":TelephonyConnectionChecker");
	    thread = new Thread(this, logPrefix + ":TelephonyConnectionChecker");
	    failCount = 0;
	}

	public void start() {
	    thread.start();
	}

	public void setLinkDown() {
	    isLinkUp.set(false);
	}

	public void run () {
	    while (true){
		startTime = System.currentTimeMillis();
		long timePassed;
		while ((timePassed = (System.currentTimeMillis() - startTime)) < refreshTime){
		    try{
			
			Thread.sleep(refreshTime - timePassed);
			
		    }catch (Exception e){
			logger.warn("Run: Thread Interrupted");
		    }
		}
		
		if (probeAsteriskConnectionActively()) {
		    if (isLinkUp.compareAndSet(false, true)) {
			logger.info("Run: Link up.");
			isConnected = true;
			if (stateOnTelephonyDown == ONLINE)
			    hangupChannelsGracefully(true);
			TelephonyLib.telephonyLib.connectionUp();
		    } else if (failCount > 0) {
			updateServerStatus();
		    }
		    failCount = 0;

		} else {
		    failCount++;
		    if (failCount >= MAX_CONNECTION_FAIL_COUNTS) {
			if (isLinkUp.compareAndSet(true, false)) {
			    logger.info("Run: Link down.");
			    isConnected = false;
			    stateOnTelephonyDown = state;
			    TelephonyLib.telephonyLib.connectionDown();
			}
		    }
		}
	    }
	}
    }

    protected static TelephonyLib telephonyLib = null;
    private static Hashtable<String, TelephonyLibListener> listeners = new Hashtable<String, TelephonyLibListener>();
    public static synchronized TelephonyLib getTelephonyLib(StationConfiguration stationConfig, MediaLib medialib, TelephonyLibListener listener) {
	if (telephonyLib == null) {
	    telephonyLib = new TelephonyLib(stationConfig, medialib);
	}

	if (listener != null)
	    listeners.put(listener.getName(), listener);

	if (telephonyLib.connect() != -1){
	    return telephonyLib;
	} else {
	    return null;
	}
    }
    
    public static TelephonyLib getTelephonyLib() {
	return telephonyLib;
    }

    public static void setOnairDevices(String captureDeviceName, String playbackDeviceName) {
	if (captureDeviceName.equals(DEFAULT_DEVICE)) {
	    EXTN_PLAYOUT = EXTN_PLAYOUT_DEFAULT;
	} else {
	    EXTN_PLAYOUT = captureDeviceName + "!" + playbackDeviceName;
	}
    }

    public static void setOffairDevices(String captureDeviceName, String playbackDeviceName) {
	if (captureDeviceName.equals(DEFAULT_DEVICE)) {
	    EXTN_PREVIEW = EXTN_PREVIEW_DEFAULT;
	} else {
	    EXTN_PREVIEW = captureDeviceName + "!" + playbackDeviceName;
	}
    }
   
    class PingRequest {
	String ip;
	boolean status;
	long testTime;

	public PingRequest(String ip, boolean status) {
	    this.status = status;
	    testTime = System.currentTimeMillis();
	}
	
	public void setStatus(boolean status) {
	    this.status = status;
	}

	public boolean getStatus() {
	    return status;
	}

	public long getTestTime() {
	    return testTime;
	}
    }

    public interface AsteriskSMSListener {
	public void newSMS(String deviceID, String message, String phoneNo);
    }

    public void addSMSListener(AsteriskSMSListener listener) {
	smsListeners.add(listener);
    }
}