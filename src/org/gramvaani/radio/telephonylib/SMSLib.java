package org.gramvaani.radio.telephonylib;

import org.gramvaani.radio.medialib.SMS;

public interface SMSLib { 
    public boolean isAvailable();
    public String getSMSLine();
    public String getLineType();
    public boolean sendSMS(SMS[] smses);
    public void cancelSMS(int[] smsIDs);
}

