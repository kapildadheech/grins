package org.gramvaani.radio.telephonylib;

import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.radio.medialib.SMS;
import org.gramvaani.radio.stationconfig.StationConfiguration;

import org.asteriskjava.live.*;
import org.asteriskjava.live.internal.*;
import org.asteriskjava.manager.*;
import org.asteriskjava.manager.action.*;
import org.asteriskjava.manager.response.*;
import org.asteriskjava.manager.event.*;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.*;

public class DongleSMSLib implements SMSLib, TelephonyLib.AsteriskSMSListener {
    static final int GET_TELEPHONYLIB_INTERVAL = 3000;
    static final int SEND_SMS_INTERVAL         = 10000;
    
    String smsLine;
    String lineType;
    String imei;
    String dongleID;
    SMSLibListener listener;
    LogUtilities logger;
    Thread smsSender;
    LinkedBlockingQueue<SMS> sendQueue;
    boolean doSend;
    TelephonyLib telephonyLib;
    StationConfiguration stationConfig;

    public DongleSMSLib(String smsLine, String lineParams, StationConfiguration stationConfig, SMSLibListener smsListener) {
	this.smsLine = smsLine;
	listener = smsListener;
	lineType = SMSLine.DONGLE_LINE_TYPE;
	imei = lineParams;
	this.stationConfig = stationConfig;

	logger = new LogUtilities(lineType + "SMSLib");
	logger.info("DongleSMSLib: smsLine=" + smsLine + ", imei=" + imei);
	sendQueue = new LinkedBlockingQueue<SMS>();
	getTelephonyLib(this);
	startSMSSender();
    }

    void getTelephonyLib(final DongleSMSLib dongleSMSLib) {
	Thread t = new Thread() {
		public void run() {
		    while(telephonyLib == null) {
			telephonyLib = TelephonyLib.getTelephonyLib();
			try {
			    Thread.sleep(GET_TELEPHONYLIB_INTERVAL);
			} catch(Exception e){}
		    }
		    telephonyLib.addSMSListener(dongleSMSLib);
		}
	    };

	t.start();
    }

    void startSMSSender() {
	smsSender = new Thread(lineType + "SMSLib:SMSSender") {
		public void run() {
		    sendMessages();
		}
	    };

	doSend = true;
	smsSender.start();
    }

    void stopSMSSender() {
	doSend = false;
	smsSender.interrupt();
    }

    void sendMessages() {
	SMS message;
	boolean success;
	while(doSend) {
	    try {
		message = sendQueue.take();
		logger.info("SendMessages: Sending message '" + message.getMessage() + "' to " + message.getPhoneNo());

		if(dongleID == null && telephonyLib != null)
		    dongleID = telephonyLib.getDongleIDFromIMEI(imei);

		if(dongleID == null) {
		    listener.smsSent(message.getID(), false);
		} else {
		    success = sendSMSToDevice(message);
		    listener.smsSent(message.getID(), success);
		}

		Thread.sleep(SEND_SMS_INTERVAL);
	    } catch (Exception e) {
		logger.warn("SendMessages: thread interrupted", e);
	    }
	}
    }

    boolean sendSMSToDevice(SMS message) {
	RSCallerID callerID = new RSCallerID(RSCallerID.PSTN, message.getPhoneNo());
	String countryCode = stationConfig.getStringParam(StationConfiguration.LOCAL_COUNTRY_CODE);

	String phoneNo = callerID.toStandardizedForm();

	//Standardized form wont convert a number start directly as 98.... 
	//since it can't figure out whether it's a mobile no or a landline no
	if(!phoneNo.startsWith("+"))
	    phoneNo = "+" + countryCode + phoneNo;

	String cliCommand = "dongle sms " + dongleID + " " + phoneNo + " \"" + message.getMessage() + "\"";
	logger.info("SendSMSToDevice: Sending clicommand: " + cliCommand);
	return telephonyLib.executeCliCommand(cliCommand);
    }

    public void newSMS(String deviceID, String message, String phoneNo) {
	if(dongleID == null && telephonyLib != null)
	    dongleID = telephonyLib.getDongleIDFromIMEI(imei);
	
	logger.debug("NewSMS: dongleID = " + dongleID + " deviceID = " + deviceID);
	if(dongleID != null && deviceID.equals(dongleID))
	    listener.newSMS(smsLine, message, phoneNo);
    }

    public boolean isAvailable() {
	return ((telephonyLib != null) && (telephonyLib.isConnected()));
    }

    public String getSMSLine() {
	return smsLine;
    }

    public String getLineType() {
	return lineType;
    }

    public boolean sendSMS(SMS[] smses) {
	if(!isAvailable())
	    return false;

	for(SMS sms : smses) {
	    sendQueue.add(sms);
	}
	return true;
    }

    public void cancelSMS(int[] smsIDs) {
	ArrayList<SMS> toDelete = new ArrayList<SMS>();
	stopSMSSender();

	for(int id: smsIDs) {
	    for(Iterator iter = sendQueue.iterator(); iter.hasNext(); ) {
		SMS sms = (SMS)iter.next();
		if(id == sms.getID()) {
		    logger.info("Cancelling sending '" + sms.getMessage() + "', " + sms.getPhoneNo());
		    toDelete.add(sms);
		}
	    }
	}

	for(int i=0; i<toDelete.size(); i++) {
	    SMS sms = toDelete.get(i);
	    sendQueue.remove(sms);
	}

	startSMSSender();
    }

}