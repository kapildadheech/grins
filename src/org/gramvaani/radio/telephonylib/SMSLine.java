package org.gramvaani.radio.telephonylib;

public class SMSLine {
    String lineID;
    String lineType;
    String lineParams;

    public static final String TOPEX_LINE_TYPE = "Topex";
    public static final String DONGLE_LINE_TYPE = "Dongle";

    public static final String PARAMS_SEPARATOR = ";";

    public SMSLine(String lineID, String lineType, String lineParams) {
	this.lineID = lineID;
	this.lineType = lineType;
	this.lineParams = lineParams;
    }

    public SMSLine(String lineType, String lineParams) {
	lineID = "";
	this.lineType = lineType;
	this.lineParams = lineParams;
    }

    public String getLineType() {
	return lineType;
    }

    public String getParams() {
	return lineParams;
    }
}