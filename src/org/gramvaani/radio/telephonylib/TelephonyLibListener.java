package org.gramvaani.radio.telephonylib;


public interface TelephonyLibListener {

    public void telephonyDown();
    public void telephonyUp(TelephonyLib telephonyLib);
    public String getName();

    public void newCall(String callGUID, RSCallerID callerID);
    public void callerHungup(String callGUID, String cause);
    public void dialNumberCallback(String callGUID, String state, String paramString);
    public void newConferenceMember(String callGUID);
    //public void sendDahdiChannelNotification(String channelEvent, Integer... channels);
    //public void sendATANotification(String ataEvent, String... channels);
    public void sendPhoneLineNotification(String event, String... phonelineIDs);
}