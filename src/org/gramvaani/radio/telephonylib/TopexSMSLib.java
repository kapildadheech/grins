package org.gramvaani.radio.telephonylib;

import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.radio.medialib.SMS;
import org.gramvaani.radio.stationconfig.StationConfiguration;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.*;
import java.util.regex.*;
import javax.net.ssl.*;
import java.net.*;
import java.io.*;
import org.apache.commons.codec.binary.Base64;

public class TopexSMSLib implements SMSLib {

    static int AVAILABILITY_CHECK_INTERVAL = 60000;
    static int RECEIVE_SMS_INTERVAL        = 60000;
    static int SEND_SMS_INTERVAL           = 10000;

    static String PROTOCOL = "https";
    static String SEND_URL_PATH = "/admin/forms/sms-send.html";
    static String SEND_PHONE_NO_PARAM = "number";
    static String SEND_MESSAGE_PARAM = "smsencode";
    static String SEND_URL_POSTFIX = "send=Send";

    static String RECV_URL_PATH = "/admin/forms/sms-read.html";

    static String REM_URL_PATH = "/admin/forms/sms-read.html";
    static String REM_ID_PARAM = "id";
    static String REM_URL_POSTFIX = "send=del";
    
    static String URL_ENCODING = "UTF-8";

    String lineType;
    String smsLine;
    String ipAddress;
    String username;
    String password;
    String outgoingPrefix;
    LogUtilities logger;
    Thread smsSender, smsReceiver, availabilityChecker;
    AtomicBoolean isAvailable;
    SMSLibListener listener;
    boolean doSend, doReceive, doCheck;
    HttpsURLConnection recvHttpsConn = null;

    
    LinkedBlockingQueue<SMS> sendQueue;
    
    public TopexSMSLib(String smsLine, String lineParams, StationConfiguration stationConfig, SMSLibListener smsListener) {
	lineType = SMSLine.TOPEX_LINE_TYPE;
	logger = new LogUtilities(lineType + "SMSLib");

	this.smsLine = smsLine;
	String[] params = lineParams.split(SMSLine.PARAMS_SEPARATOR);
	if(params.length != 4) {
	    logger.error("TopexSMSLib: insufficient parameters passed. Params=" + lineParams);
	    isAvailable = new AtomicBoolean(false);
	    return;
	}

	ipAddress = params[0];
	username = params[1];
	password = params[2];
	outgoingPrefix = params[3];
	listener = smsListener;

	logger.info("TopexSMSLib: smsLine=" + smsLine + ", ipAddress=" + ipAddress + ", username=" + username + ", password=" + password + ", outgoingPrefix=" + outgoingPrefix);
	sendQueue = new LinkedBlockingQueue<SMS>();
	isAvailable = new AtomicBoolean(true);

	disableHttpsVerifications();	
	startSMSSender();
	startSMSReceiver();
	startAvailabilityChecker();
    }

    void startSMSSender() {
	smsSender = new Thread(lineType + "SMSLib:SMSSender") {
		public void run() {
		    sendMessages();
		}
	    };

	doSend = true;
	smsSender.start();
    }

    void stopSMSSender() {
	doSend = false;
	smsSender.interrupt();
    }
	
    void startSMSReceiver() {
	smsReceiver = new Thread(lineType + "SMSLib:SMSReceiver") {
		public void run() {
		    receiveMessages();
		}
	    };

	doReceive = true;
	smsReceiver.start();
    }

    void stopSMSReceiver() {
	doReceive = false;
	smsReceiver.interrupt();
    }

    void startAvailabilityChecker() {
	availabilityChecker = new Thread(lineType + "SMSLib:AvailabilityChecker") {
		public void run() {
		    checkAvailability();
		}
	    };

	doCheck = true;
	availabilityChecker.start();
    }

    void stopAvailabilityChecker() {
	doCheck = false;
	availabilityChecker.interrupt();
    }

    public boolean sendSMS(SMS[] smses) {
	for(SMS sms : smses) {
	    sendQueue.add(sms);
	}
	return true;
    }

    public void cancelSMS(int[] smsIDs) {
	ArrayList<SMS> toDelete = new ArrayList<SMS>();
	stopSMSSender();

	for(int id: smsIDs) {
	    for(Iterator iter = sendQueue.iterator(); iter.hasNext(); ) {
		SMS sms = (SMS)iter.next();
		if(id == sms.getID()) {
		    logger.info("Cancelling sending '" + sms.getMessage() + "', " + sms.getPhoneNo());
		    toDelete.add(sms);
		}
	    }
	}

	for(int i=0; i<toDelete.size(); i++) {
	    SMS sms = toDelete.get(i);
	    sendQueue.remove(sms);
	}

	startSMSSender();
    }

    public boolean isAvailable() {
	return isAvailable.get();
    }

    public String getSMSLine() {
	return smsLine;
    }

    public String getLineType() {
	return lineType;
    }

    void sendMessages() {
	SMS message;
	boolean success;
	while(doSend) {
	    try {
		message = sendQueue.take();
		logger.info("SendMessages: Sending message '" + message.getMessage() + "' to " + message.getPhoneNo());
		success = sendSMSToDevice(message);
		logger.info("SendMessages: Success: " + success);
		listener.smsSent(message.getID(), success);
		Thread.sleep(SEND_SMS_INTERVAL);
	    } catch (Exception e) {
		logger.warn("SendMessages: thread interrupted", e);
	    }
	}
    }

    void receiveMessages() {
	while(doReceive) {
	    Message messages[] = receiveSMSFromDevice();
	    for(Message message : messages) {
		logger.info("ReceiveMessages: New SMS: '" + message.getMessage() + ", " + message.getPhoneNo());
		listener.newSMS(getSMSLine(), message.getMessage(), message.getPhoneNo());
	    }
	
	    try {
		Thread.sleep(RECEIVE_SMS_INTERVAL);
	    } catch (Exception e) {
		logger.warn("ReceiveMessages: thread interrupted", e);
	    }
	}
    }

    HttpsURLConnection getHttpsConnection(String urlString) {
	HttpsURLConnection httpsConn = null;
	logger.info("GetHttpsConnection: Preparing HttpsURLConnection for the URL: " + urlString);
	try {
	    URL url = new URL(urlString);
	    URLConnection conn = url.openConnection();
	    if(conn instanceof HttpsURLConnection) {
		httpsConn = (HttpsURLConnection)(conn);
		httpsConn.setRequestMethod("GET");
		String userpass = username + ":" + password;
		String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
		httpsConn.setRequestProperty ("Authorization", basicAuth);
		httpsConn.connect();
		return httpsConn;
	    } else {
		logger.error("GetHttpsConnection: Connection is not an instance of HTTPS connection.");
		throw new Exception();
	    }
	} catch (Exception e) {
	    logger.error("GetHttpsConnection: Got exception while making https connection to: " + urlString, e);
	    return null;
	} 	    
    }

    boolean sendSMSToDevice(SMS message) {
	HttpsURLConnection httpsConn;
	BufferedReader reader = null;
	URI uri = null;

	String phoneNoStr = message.getPhoneNo();
	phoneNoStr = phoneNoStr.replaceAll("^\\+", "00");
	String messageStr = message.getMessage();
	messageStr = messageStr.replaceAll("\\&", "n");
	String paramStr = SEND_PHONE_NO_PARAM + "=" + outgoingPrefix + phoneNoStr + "&" + SEND_MESSAGE_PARAM + "=" + messageStr + "&" + SEND_URL_POSTFIX;
	try {
	    uri = new URI(PROTOCOL, ipAddress, SEND_URL_PATH, paramStr, null);
	} catch (Exception e) {
	    logger.error("SendSMSToDevice: Could not create URI for sending message", e);
	    return false;
	}

	httpsConn = getHttpsConnection(uri.toString());
	if(httpsConn == null) {
	    return false;
	}

	try {
	    reader = new BufferedReader(new InputStreamReader(httpsConn.getInputStream()));
	    String line = null;
	    while ((line = reader.readLine()) != null) {
		logger.debug("SendSMSToDevice: " + line);
	    }
	    reader.close();
	    return true;
	} catch (Exception e) {
	    logger.error("SendSMSToDevice: Got exception while sending SMS.", e);
	    return false;
	}
    }

    Message[] receiveSMSFromDevice() {
	Message[] newMessages = new Message[0];
	URI uri = null;

	try {
	    uri = new URI(PROTOCOL, ipAddress, RECV_URL_PATH, null, null);
	} catch(Exception e) {
	    logger.error("ReceiveSMSFromDevice: Could not create URI for receiving SMS.", e);
	    return newMessages;
	}
	
	HttpsURLConnection recvHttpsConn = getHttpsConnection(uri.toString());
	BufferedReader reader = null;

	if(recvHttpsConn != null) {
	    try {
		reader = new BufferedReader(new InputStreamReader(recvHttpsConn.getInputStream()));
		String line = null;
		StringBuilder received = new StringBuilder();
		while ((line = reader.readLine()) != null) {
		    received.append(line);
		    received.append("\n");
		}
		reader.close();
		recvHttpsConn.disconnect();
		newMessages = parseAndRemoveReceivedSMSHtml(received.toString());
	    } catch(Exception e) {
		logger.error("ReceiveSMSFromDevice: Got exception when trying to receive SMS.", e);
		return newMessages;
	    } 
	}    
	return newMessages;
    }

    void removeSMSFromDevice(String smsID) {
	URI uri = null;
	String paramStr = REM_ID_PARAM + "=" + smsID + "&" + REM_URL_POSTFIX;
	try {
	    uri = new URI(PROTOCOL, ipAddress, REM_URL_PATH, paramStr, null);
	} catch(Exception e) {
	    logger.error("RemoveSMSFromDevice: Could not create URI for removing SMS.", e);
	    return;
	}

	HttpsURLConnection remHttpsConn = getHttpsConnection(uri.toString());
	BufferedReader reader = null;

	if(remHttpsConn != null) {
	    try {
		reader = new BufferedReader(new InputStreamReader(remHttpsConn.getInputStream()));
		String line = null;
		while ((line = reader.readLine()) != null) {
		    logger.debug("RemoveSMSFromDevice: "+ line);
		}
		reader.close();
	    } catch(Exception e) {
		logger.error("RemoveSMSFromDevice: Got exception when trying to receive SMS.", e);
	    } 
	}
    }

    Message getMessageFromHtml(String smsHtml) {
	String[] tmpArr = smsHtml.split("\\(");
	smsHtml = tmpArr[1];
	tmpArr = smsHtml.split("\\)");
	smsHtml = tmpArr[0];
	tmpArr = smsHtml.split("\\,");
	    
	try {
	    //Ignoring the dateTime from the Topex device.
	    //Leads to a max error of 1 minute (receive interval), which seems ok
	    //String dateTime = URLDecoder.decode(tmpArr[0].replaceAll("\\\"", ""), URL_ENCODING);
	    
	    String phoneNo = URLDecoder.decode(tmpArr[1].replaceAll("\\\"", ""), URL_ENCODING);
	    String messageTxt = URLDecoder.decode(tmpArr[2].replaceAll("\\\"", ""), URL_ENCODING);
	    return (new Message(messageTxt, phoneNo));
	} catch(Exception e) {
	    logger.error("GetMessageFromHtml: Got exception while decoding SMS", e);
	    return null;
	}
    }

    String getSMSIDFromHtml(String smsHtml) {
	String[] tmpArr = smsHtml.split("\\(");
	smsHtml = tmpArr[2];
	tmpArr = smsHtml.split("\\)");
	smsHtml = tmpArr[0];
	tmpArr = smsHtml.split("\\,");

	return tmpArr[1].replaceAll("\\\"","");
    }

    Message[] parseAndRemoveReceivedSMSHtml(String receivedHtml) {
	ArrayList<Message> messages = new ArrayList<Message>();

	Pattern smsPattern = Pattern.compile("<script>linie.*\n");
	Matcher smsMatcher = smsPattern.matcher(receivedHtml);
	while(smsMatcher.find()) {
	    String smsMatch = smsMatcher.group();
	    smsMatch = smsMatch.replaceAll("\\n", "");

	    Message message = getMessageFromHtml(smsMatch);
	    String smsID = getSMSIDFromHtml(smsMatch);
	    removeSMSFromDevice(smsID);
	    messages.add(message);
	}

	return messages.toArray(new Message[]{});
    }

    void checkAvailability() {
	while(doCheck) {
	    try {
		Thread.sleep(AVAILABILITY_CHECK_INTERVAL);
		//XXXX To Do
	    } catch(Exception e) {
		logger.warn("CheckAvailability: Thread interrupted");
	    }
	}
    }

    private void disableHttpsVerifications() {
        TrustManager[] trustAllCerts = new TrustManager[] {
	    new TrustAllTM()
	};
	
	SSLContext sc = null;
	try {
	    sc = SSLContext.getInstance("SSL");
	    sc.init(null, trustAllCerts, null);
	} catch(Exception e) {
	    logger.error("DisableHttpsVerifications: Got error while creating a ssl context.", e);
	}
	HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

	HostnameVerifier allHostsValid = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
		    return true;
		}
	    };
	HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
     }

    class Message {
	String phoneNo;
	String message;

	public Message(String message, String phoneNo) {
	    this.message = message;
	    this.phoneNo = phoneNo;
	}

	public String getPhoneNo() {
	    return phoneNo;
	}

	public String getMessage() {
	    return message;
	}
    }

    class TrustAllTM implements javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }
 
        public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
            return true;
        }
 
        public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
            return true;
        }
 
        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
	    throws java.security.cert.CertificateException
        {
            return;
        }
	
        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
	    throws java.security.cert.CertificateException
        {
            return;
        }
    }
}