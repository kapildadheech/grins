package org.gramvaani.radio.telephonylib;

public interface SMSLibListener { 
    public void smsSent(int smsID, boolean ack);
    public void newSMS(String smsLine, String message, String phoneNo);
}