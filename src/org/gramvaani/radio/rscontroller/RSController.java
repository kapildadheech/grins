package org.gramvaani.radio.rscontroller;

import gnu.getopt.Getopt;

import java.lang.reflect.*;
import java.util.*;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.utilities.singleinstance.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.rscontroller.messages.*;

public class RSController implements IPCServerCallback {

    //Process return codes
    public static final int MULTIPLE_INSTANCE_EXIT_CODE = -3;
    public static final int INVALID_INSTANCE_EXIT_CODE = -4;

    //Error codes
    
    public static final String ERROR_WRONG_STATE 	= "ERROR_WRONG_STATE";
    public static final String ERROR_RESOURCE_ACK_FAILED= "ERROR_RESOURCE_ACK_FAILED";
    public static final String ERROR_RESOURCE_NOT_FREE 	= "ERROR_RESOURCE_NOT_FREE";
    public static final String ERROR_RESOURCE_ROLE_NOT_FOUND = "ERROR_RESOURCE_ROLE_NOT_FOUND";
    public static final String ERROR_DB_FAILED 		= "ERROR_DB_FAILED";
    public static final String ERROR_IDLE_MONITOR_FAILED= "ERROR_IDLE_MONITOR_FAILED";
    public static final String ERROR_INVALID_PREEMT_REQUEST= "ERROR_INVALID_PREEMT_REQUEST";
    public static final String ERROR_NO_SPACE 		= "ERROR_NO_SPACE";
    public static final String ERROR_NOT_WRITABLE 	= "ERROR_NOT_WRITABLE";
    public static final String ERROR_SERVICE_DISCONNECTED= "ERROR_SERVICE_DISCONNECTED";
    public static final String ERROR_SERVICE_DOWN 	= "ERROR_SERVICE_DOWN";
    public static final String ERROR_SERVLET_CONNECTION_FAILED= "ERROR_SERVLET_CONNECTION_FAILED";
    public static final String ERROR_UNKNOWN 		= "ERROR_UNKNOWN";

    public static final String IPC_SERVER = "IPC_SERVER";

    public static final String ANON_SERVICE 	= "ANON_SERVICE";
    public static final String ARCHIVER_SERVICE = "ARCHIVER_SERVICE";
    public static final String AUDIO_SERVICE 	= "AUDIO_SERVICE";
    public static final String INDEX_SERVICE 	= "INDEX_SERVICE";
    public static final String LIB_SERVICE 	= "LIB_SERVICE";
    public static final String MEDIA_LIB 	= "MEDIA_LIB";
    public static final String MIC_SERVICE 	= "MIC_SERVICE";
    public static final String MONITOR_SERVICE 	= "MONITOR_SERVICE";
    public static final String OFFLINE_TELEPHONY_SERVICE = "OFFLINE_TELEPHONY_SERVICE";
    public static final String ONLINE_TELEPHONY_SERVICE  = "ONLINE_TELEPHONY_SERVICE";
    public static final String RESOURCE_MANAGER = "RESOURCE_MANAGER";
    public static final String RS_APP		= "RS_APP";
    public static final String RS_CONTROLLER	= "RS_CONTROLLER";
    public static final String RSC_SERVICE 	= "RSC_SERVICE";      //dummy service to keep all backend stuff together
    public static final String SERVLET 		= "SERVLET";
    public static final String SMS_SERVICE 	= "SMS_SERVICE";
    public static final String STREAMING_SERVICE= "STREAMING_SERVICE";
    public static final String UI_SERVICE 	= "UI_SERVICE";
    public static final String UPLOAD_SERVICE 	= "UPLOAD_SERVICE";   //dummy service to keep all backend stuff together


    public static final String[] SERVICES = new String[]{ARCHIVER_SERVICE, AUDIO_SERVICE, INDEX_SERVICE, 
							 LIB_SERVICE, MIC_SERVICE, MONITOR_SERVICE,
							 OFFLINE_TELEPHONY_SERVICE, ONLINE_TELEPHONY_SERVICE,
							 SMS_SERVICE, STREAMING_SERVICE, UI_SERVICE};

    protected IPCServer ipcServer;
    protected ResourceManager resourceManager;
    protected StationConfiguration stationConfig;
    protected Hashtable<String, IPCNode> localServices;
    protected HashSet<String> remoteServices;

    protected LogUtilities logger;
   

    public RSController(StationConfiguration stationConfig) {
	this.stationConfig = stationConfig;
	logger = new LogUtilities("RSController");
	
	if(!stationConfig.isValidInstance()) {
	    logger.error("Invalid instance. Cannot continue.");
	    System.exit(RSController.INVALID_INSTANCE_EXIT_CODE);
	}

	ArrayList<String> persistentQueueDestinations = new ArrayList<String>();
	Service[] services = stationConfig.getAllServices();
	for(Service service : services) {
	    persistentQueueDestinations.add(service.serviceInstanceName());
	}
	StationNetwork stationNetwork = stationConfig.getStationNetwork();
	String[] servletIPs = stationNetwork.getUniqueIPs();
	for(String servletIP : servletIPs) {
	    String servletName = stationNetwork.getAStoreNameOnMachine(servletIP);
	    if(!servletName.equals("")) {
		persistentQueueDestinations.add(SERVLET + "_" + servletName);
	    }
	}

	ipcServer = new IPCServerImpl(stationConfig.getIntParam(StationConfiguration.IPC_SERVER_PORT), this, 
				      stationConfig.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfig.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfig.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR),
				      persistentQueueDestinations);
	resourceManager = new ResourceManager(ipcServer, stationConfig, this);
	ipcServer.startServer();
	localServices = new Hashtable<String, IPCNode>();
	remoteServices = new HashSet<String>();
	logger.info("RSController: Initializing.");
	initLocalServices(ipcServer);
	logger.debug("RSController: Initialized. Leaving");
    }
    
    public void newIPCNode(String name, Boolean isRemote, String registrationID) {
	if (!name.equals(RESOURCE_MANAGER)){
	    logger.debug("NewIPCNode: Node name: "+name);
	    resourceManager.newService(name, isRemote, registrationID);
	}
    }

    public void nodeError(String name) {
	logger.error("NodeError: "+name);
	resourceManager.serviceError(name);
    }


    public void serverFailed() {
	// handle this. new IPCServerImpl
	logger.error("ServerFailed:");
	System.exit(-1);
    }


    protected void initLocalServices(final IPCServer ipcServer) {
	logger.debug("InitLocalServices: Entering");
	final String localMachineID = stationConfig.getLocalMachineID();
	logger.info("InitLocalServices: Local Machine ID: "+localMachineID);
	
	for(String SERVICE: SERVICES) {
	    Service[] services = stationConfig.getServices(SERVICE);
	    logger.debug("InitLocalServices: "+services.length+" services found of type "+SERVICE+".");
	    if(services.length > 0) {
		for(final Service service: services) {
		    Thread t = new Thread(){
			    public void run(){

				logger.debug("InitLocalServices: inst = " + service.serviceInstanceName() + ":" + service.machineID() + ":" + localMachineID);
				if(service.machineID().equals(localMachineID)) {
				    try {
					Class<?> localServiceClass = Class.forName(service.serviceClass());
					Constructor localServiceConstructor = localServiceClass.getConstructor(IPCServer.class, 
													       StationConfiguration.class,
													       String.class,
													       String.class);
					IPCNode localService = (IPCNode)(localServiceConstructor.newInstance(new Object[]{ipcServer, 
															  stationConfig,
															  service.serviceInstanceName(),
															  localMachineID}));
					localServices.put(service.serviceInstanceName(), localService);
					logger.debug("InitLocalServices: Local Service: "+service.serviceInstanceName());
					
				    } catch(Exception e) {
					logger.error("InitLocalServices: Could not instantiate service: " + service.serviceInstanceName(), e);
				    }
				    
				} else {
				    remoteServices.add(service.serviceInstanceName());
				    logger.debug("InitLocalServices: Remote Service: "+service.serviceInstanceName());
				}
			    }
			};
		    t.start();
		}
	    }
	}
	logger.debug("InitLocalServices: Leaving");
    }

    

    public boolean isRemoteService(String serviceName){
	return remoteServices.contains(serviceName);
    }

    public static void main(String args[]) {
	Getopt g = new Getopt("RSController", args, "c:");
	int c;
	String configFilePath = "./automation.conf";
	while((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    }
	}

	//LogUtilities.addShutdownHook();
	
	StationConfiguration stationConfig = new StationConfiguration();
	stationConfig.readConfig(configFilePath);

	if (!SingleInstanceManager.registerInstance(RSController.RS_CONTROLLER)) {
	    LogUtilities.getDefaultLogger().fatal("Another instance of RSCONTROLLER is already running.  Exiting.");
	    System.exit(MULTIPLE_INSTANCE_EXIT_CODE);
	}

	LogUtilities.logToFile(RSController.RS_CONTROLLER, stationConfig.getStringParam(StationConfiguration.LOG_FILE_SIZE));

	RSController rsController = new RSController(stationConfig);
    }

}
