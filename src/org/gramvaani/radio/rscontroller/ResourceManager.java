package org.gramvaani.radio.rscontroller;

import org.gramvaani.utilities.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.app.gui.DiagnosticsController;

import java.util.*;

public class ResourceManager implements IPCNode, LinkMonitorServerListener, DefaultUncaughtExceptionCallback, 
					DiagnosticUtilitiesConnectionCallback, DiagnosticUtilitiesPingCallback {

    protected IPCServer ipcServer;
    protected StationConfiguration stationConfig;

    private ServiceTable serviceTable;
    private ResourceTable resourceTable;

    // for acknowledgements
    public final static String SUCCESS_MSG = "SUCCESS";
    public final static String FAIL_MSG = "FAIL";

    // resource acquire/release messages
    public final static String  RES_HELD_MSG = "RES_HELD";
    public final static String  RES_FREE_MSG = "RES_FREE";
    public final static String  RES_ACQUIRE_MSG = "RES_ACQUIRE";
    public final static String  RES_RELEASE_MSG = "RES_RELEASE";

    // resource states
    public final static Integer RES_HELD    = 0;
    public final static Integer RES_FREE    = 1;
    public final static Integer RES_PREEMPT = 2;
    public final static Integer RES_SHARED  = 3;

    // types of resources
    public final static String  RES_SOUNDCARD_IN  = "SOUNDCARD_IN";
    public final static String  RES_SOUNDCARD_OUT = "SOUNDCARD_OUT";
    public final static String  RES_PHONELINE_IN  = "PHONELINE_IN";
    public final static String  RES_PHONELINE_OUT = "PHONELINE_OUT";
    public final static String  RES_SIPCHANNEL_IN = "SIPCHANNEL_IN";
    public final static String  RES_SIPCHANNEL_OUT= "SIPCHANNEL_OUT";
    public final static String[] RESOURCE_TYPES = new String[] {RES_SOUNDCARD_IN, RES_SOUNDCARD_OUT,
								RES_PHONELINE_IN, RES_PHONELINE_OUT,
								RES_SIPCHANNEL_IN, RES_SIPCHANNEL_OUT};
    
    public final static String PORT_JACK = "JACK";
    public final static String PORT_ALSA = "ALSA";
    public final static String PORT_WIN  = "WIN";
    public final static String PORT_DEFAULT = "DEFAULT";

    protected ArrayList<PendingRequest> pendingRequests = new ArrayList<PendingRequest>();
    protected LinkMonitorServer linkMonitor;
    protected RSController rsController;
    protected Hashtable<String,String[]> serviceInterestOfServices = new Hashtable<String,String[]>();
    protected Hashtable<String,String[]> serviceInterestOfWidgets = new Hashtable<String,String[]>();

    protected LogUtilities logger;
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected DiagnosticUtilities diagUtil;
    protected String erroredService;
    protected String resourceManagerID;
    

    Hashtable<String, Boolean> pingRequests;

    public ResourceManager(IPCServer ipcServer, StationConfiguration stationConfig, RSController rsController) {
	this.ipcServer = ipcServer;
	this.stationConfig = stationConfig;
	this.rsController = rsController;
	
	resourceManagerID = GUIDUtils.getGUID();
	serviceTable = new ServiceTable();
	resourceTable = new ResourceTable();
	logger = new LogUtilities(RSController.RESOURCE_MANAGER);
	logger.debug("ResourceManager: Entering");
	logger.info("ResourceManager: Initializing.");
	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "RSController");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);

	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfig, ipcServer);

	pingRequests = new Hashtable<String, Boolean>();
	//diagUtil.registerPingListener(this);

	ipcServer.registerIPCNode(RSController.RESOURCE_MANAGER, this, RSController.RESOURCE_MANAGER);
	linkMonitor = new LinkMonitorServer (this, stationConfig, "ResourceManager");
	linkMonitor.start();
	resourceConfig();
	logger.debug("ResourceManager: Leaving");
    }

    protected void resourceConfig() {
	for (String RESOURCE_TYPE: RESOURCE_TYPES) {
	    Resource[] resources = stationConfig.getResources(RESOURCE_TYPE);
	    for (Resource resource: resources) {
		logger.debug("ResourceConfig: Config resource:" + resource.resourceName() + ":" + resource.machineID());
		ResourceDetails resourceDetails = resourceTable.getResourceDetails(resource.resourceName());
		resourceDetails.setResourceType(RESOURCE_TYPE);
		resourceDetails.setMachineID(resource.machineID());
		resourceDetails.setState(RES_FREE);
	    }
	}
    }
    
    public int handleNonBlockingMessage(IPCMessage message){
	return 0;
    }

    public synchronized int handleIncomingMessage(IPCMessage message){
	logger.debug("HandleIncomingMessage: Entering. Received message from "+message.getSource()+" type: "+message.getMessageClass());
	if (message instanceof ServiceInterestMessage) {
	    ServiceInterestMessage serviceInterestMessage = (ServiceInterestMessage)message;
	    handleServiceInterest(serviceInterestMessage.getSource(), serviceInterestMessage.getWidgetName(), 
				  serviceInterestMessage.getServiceInterests());
	} else if (message instanceof ServiceReadyMessage) {
	    ServiceReadyMessage serviceReadyMessage = (ServiceReadyMessage)message;
	    handleServiceReady(serviceReadyMessage.getSource(), serviceReadyMessage.getStatus(), serviceReadyMessage.getMessageID(), true);
	    if (serviceReadyMessage.getStatus()) {
		ipcServer.sendPersistentMessages(serviceReadyMessage.getSource());
	    }
	} else if (message instanceof ResourceRequestMessage) {
	    ResourceRequestMessage resourceRequestMessage = (ResourceRequestMessage)message;
	    handleResourceRequest(resourceRequestMessage.getSource(), resourceRequestMessage.getResources(), 
				  resourceRequestMessage.getResourceRoles(), 
				  resourceRequestMessage.isPersistentRequest(), false, null);
    	} else if (message instanceof ResourceRequestSyncMessage) {
	    ResourceRequestSyncMessage resourceRequestMessage = (ResourceRequestSyncMessage)message;
	    handleResourceRequest(resourceRequestMessage.getSource(), resourceRequestMessage.getResources(), 
				  resourceRequestMessage.getResourceRoles(), 
				  resourceRequestMessage.isPersistentRequest(), 
				  true, resourceRequestMessage.getMessageID());
    	} else if (message instanceof ResourceReleaseMessage) {
	    ResourceReleaseMessage resourceReleaseMessage = (ResourceReleaseMessage)message;
	    handleResourceRelease(resourceReleaseMessage.getSource(), resourceReleaseMessage.getResources(), false, null);
	} else if (message instanceof ResourceReleaseSyncMessage) {
	    ResourceReleaseSyncMessage resourceReleaseMessage = (ResourceReleaseSyncMessage)message;
	    handleResourceRelease(resourceReleaseMessage.getSource(), resourceReleaseMessage.getResources(), 
				  true, resourceReleaseMessage.getMessageID());
	} 
	
	//ServiceErrorMessage not used any more.
	/*else if (message instanceof ServiceErrorMessage) {
	  ServiceErrorMessage serviceErrorMessage = (ServiceErrorMessage)message;
	  handleServiceError(serviceErrorMessage.getSource(), serviceErrorMessage.getError(), 
	  serviceErrorMessage.getErrorCode(), serviceErrorMessage.getPayload());
	  } */

	else if (message instanceof HeartbeatMessage){
	    HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
	    linkMonitor.handleMessage(heartbeatMessage);
	} else if (message instanceof PreemptResourceAckMessage){
	    PreemptResourceAckMessage preemptAck = (PreemptResourceAckMessage)message;
	    handlePreemptResourceAck(preemptAck.getSource(), preemptAck.getAck(), preemptAck.getResources());
	} else if (message instanceof TemperatureAlertMessage){
	    TemperatureAlertMessage tempAlert = (TemperatureAlertMessage)message;
	    handleTemperatureAlertMessage(tempAlert.getSource(), tempAlert.getMachineID());
	} else if (message instanceof MachinePingRequestMessage) {
	    MachinePingRequestMessage pingMessage = (MachinePingRequestMessage)message;
	    handleMachinePingRequestMessage(pingMessage.getMessageID(), pingMessage.getSource(), pingMessage.getMachineIDs());
	} else {
	    logger.debug("HandleIncomingMessage: Unknown Message type received.");
	}
	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }


    protected void sendResourceAckForPendingRequest(PendingRequest request){
	logger.debug("SendResourceAck: Entering. Sending to "+request.serviceName());
	String[] resources = request.resources();
	String serviceName = request.serviceName();
	String[] resourceRoles = request.resourceRoles();
	ArrayList<String> successfulResources = new ArrayList<String>();
	ArrayList<String> successfulResourceRoles = new ArrayList<String>();
	ArrayList<String> failedResources = new ArrayList<String>();
	ArrayList<String> failedResourceRoles = new ArrayList<String>();

	for (int i=0; i<resources.length; i++) {
	    String resourceName = resources[i];
	    ResourceDetails resourceDetails = resourceTable.getResourceDetails(resourceName);
	    resourceDetails.setState(RES_HELD);
	    resourceDetails.holdResource(serviceName);
	    successfulResources.add(resourceName);
	    successfulResourceRoles.add(resourceRoles[i]);
	    logger.info("SendResourceAck: Resource: "+resourceName+" held for service: "+serviceName);
	}
	
	sendResourceAck(serviceName, successfulResources, successfulResourceRoles, failedResources, 
			failedResourceRoles, request.isSyncRequest, request.messageID);

	logger.debug("SendResourceAck: Leaving");
    }

    protected void sendResourceNack(PendingRequest request){
    }

    public void serviceDown(String service){
	logger.error("ServiceDown: "+service);
	//setting current readiness of the service to its readiness when we detected node down
	//this ensures that service error is sent only if the service was ready then
	ServiceDetails serviceDetails = serviceTable.getServiceDetails(service);
	serviceDetails.setReady(serviceDetails.readinessAtNetworkDown());
	deactivateService(service);
    }

    public IPCServer attemptReconnect(){
	//not used
	return null;
    }


    public synchronized void handleUncaughtException(Thread t,Throwable ex){
	logger.error("HandleUncaughtException: "+t.getName());
    }

    public class PingThread implements Runnable {
	String messageID, source;
	String[] machineIDs;
	ResourceManager resourceManager;

	public PingThread(String messageID, String source, String[] machineIDs, ResourceManager resourceManager) {
	    this.messageID = messageID;
	    this.source = source;
	    this.machineIDs = machineIDs;
	    this.resourceManager = resourceManager;
	}

	public void init() {
	    Thread t = new Thread(this, "Diagnostics: Ping Thread");
	    t.start();
	}

	public void run() {
	    synchronized(pingSynch) {
		if (machineIDs.length == 0)
		    machineIDs = stationConfig.getMachines();
		pingRequests.clear();
		for (String machineID: machineIDs) {
		    pingRequests.put(stationConfig.getMachineIP(machineID), new Boolean(false));
		}
		Enumeration<String> e = pingRequests.keys();
		while (e.hasMoreElements()) {
		    String ipAddr = e.nextElement();
		    diagUtil.initiatePing(ipAddr, resourceManager);
		    try {
			pingSynch.wait(DiagnosticUtilities.PING_TIMEOUT);
		    } catch(InterruptedException exp) { }
		}
		
		Hashtable<String, Boolean> pingResponse = new Hashtable<String, Boolean>();
		for (String machineID: machineIDs)
		    pingResponse.put(machineID, pingRequests.get(stationConfig.getMachineIP(machineID)));
		
		MachinePingResponseMessage pingResponseMessage = new MachinePingResponseMessage(RSController.RESOURCE_MANAGER,
												source, messageID, pingResponse);
		if (ipcServer.handleOutgoingMessage(pingResponseMessage) < 0)
		    logger.error("HandleMachinePingRequest: Failed to send machinePingResponse.");
		else
		    logger.debug("HandleMachinePingResponse: machinePingResponse sent successfully.");
	    }
	}
    }

    Object pingSynch = new Object();
    protected void handleMachinePingRequestMessage(String messageID, String source, String[] machineIDs) {
	PingThread pingThread = new PingThread(messageID, source, machineIDs, this);
	pingThread.init();
    }

    public void pingResponse(String ipAddr, boolean status) {
	synchronized(pingSynch) {
	    pingRequests.put(ipAddr, new Boolean(status));
	    pingSynch.notifyAll();
	}
    }
    

    protected void handleTemperatureAlertMessage(String source, String machineID){
	//Should not reach here. Message is directly sent to UI service now.
    }
	    
    protected void handlePreemptResourceAck(String source, String ack, String[] ackedResources){
	logger.debug("HandlePreemptResourceAck: Entering. Source:"+source+" ack:"+ack+" firstResource:"+ackedResources[0]);
	ArrayList<String> pendingResources;
	ArrayList<String> pendingServices;
	boolean requestFound = false;
	for (int i=0; i<pendingRequests.size(); i++){
	    if (requestFound)
		break;
	    logger.debug("HandlePreemptResourceAck: Checking pending request of "+pendingRequests.get(i).serviceName());
	    pendingResources = pendingRequests.get(i).preemptResources();
	    pendingServices = pendingRequests.get(i).preemptServices();
	    for (int j=0; j<pendingResources.size(); j++)
		logger.debug("RES:"+pendingResources.get(j));
	    for (String resource : ackedResources){
		for (int j=0; j<pendingResources.size(); j++){
		    if (resource.equals(pendingResources.get(j))){
			requestFound = true; //because there can be only one request for a resource
			logger.debug("HandlePreemptResourceAck: Request corresponding to preempt Ack found.");
			if (ack.equals("FALSE")){
			    sendResourceNack(pendingRequests.get(i));
			}
			else {
			    logger.debug("HandlePreemptResourceAck: Resource found:"+pendingResources.get(j)+". Removing it from pending list.");
			    pendingResources.remove(j);
			    pendingServices.remove(j);
			    ResourceDetails resourceDetails = resourceTable.getResourceDetails(resource);
			    resourceDetails.releaseResource(source);
			    logger.debug("HandlePreemptResourceAck: "+pendingResources.size()+" more reqourse pending.");
			    if (pendingResources.size()==0){
				logger.debug("HandlePreemptResourceAck: All pending resources preempted. Sending Resource Ack");
				sendResourceAckForPendingRequest(pendingRequests.get(i));
				logger.debug("HandlePreemptResourceAck: Leaveing");
				return;
			    }
			    break;
			}
		    }
		}
	    }
	}
	logger.error("HandlePreemptResourceAck: No pending request corresponding to preempt resource ack found");	    
    }

    protected void handleServiceInterest(String source, String widgetName, String[] serviceInterests) {
	logger.info("HandleServiceInterest: Source = "+source+", provider = "+widgetName);
	if (source.equals(RSController.UI_SERVICE) && !widgetName.equals(""))
	    handleServiceInterestByWidget(widgetName, serviceInterests);
	else
	    handleServiceInterestByService(source, serviceInterests);
    }

    
    //XXX merge this with handleServiceInterestByWidget
    protected void notifyServiceInterestByWidget(String widgetName, String[] serviceInterests) {
	logger.debug("NotifyServiceInterestByWidget: Entering");
	ArrayList<String> readyServices = new ArrayList<String>();
	ArrayList<String> readyServicesID = new ArrayList<String>();
	ArrayList<String> nonreadyServices = new ArrayList<String>();
	serviceInterestOfWidgets.put(widgetName, serviceInterests);

	for (String serviceName: serviceInterests) {
	    ServiceDetails serviceDetails = serviceTable.getServiceDetails(serviceName);
	    if (serviceDetails.ready()){
		readyServices.add(serviceName);
		readyServicesID.add(serviceDetails.registrationID());
	    } else {
		nonreadyServices.add(serviceName);
	    }
	    serviceDetails.addInterestByWidget(widgetName);
	    logger.info("NotifyServiceInterestByWidget: Added "+widgetName+" to "+serviceName);
	}
	
	
	ServiceDetails uiServiceDetails = serviceTable.getServiceDetails(RSController.UI_SERVICE);
	if (uiServiceDetails.ready()){
	    readyServices.add(RSController.UI_SERVICE);
	    readyServicesID.add(uiServiceDetails.registrationID());
	}


	logger.debug("NotifyServiceInterestByWidget: "+readyServices.size()+" active services and "+nonreadyServices.size()+" inactive services found.");
	ServiceNotifyMessage serviceNotifyMessage = new ServiceNotifyMessage(RSController.RESOURCE_MANAGER, 
									     RSController.UI_SERVICE, widgetName,
									     readyServices.toArray(new String[]{}),
									     readyServicesID.toArray(new String[]{}),     
									     nonreadyServices.toArray(new String[]{}));

	if (ipcServer.handleOutgoingMessage(serviceNotifyMessage) < 0)
	    logger.error("NotifyServiceInterestByWidget: failed to send serviceNotifyMessage.");
	else
	    logger.debug("NotifyServiceInterestByWidget: serviceNotifyMessage sent successfully.");

	logger.debug("NotifyServiceInterestByWidget: Leaving");
    }

    //XXX merge this with handleServiceInterestByService
    protected void notifyServiceInterestByService(String requestingService, String[] serviceInterests) {
        logger.debug("NotifyServiceInterestByService: Entering");
	ArrayList<String> readyServices = new ArrayList<String>();
	ArrayList<String> readyServicesID = new ArrayList<String>();
	ArrayList<String> nonreadyServices = new ArrayList<String>();

	serviceInterestOfServices.put(requestingService, serviceInterests);
	
	for (String serviceName: serviceInterests) {
	    ServiceDetails serviceDetails = serviceTable.getServiceDetails(serviceName);
	    if (serviceDetails.ready()){
		readyServices.add(serviceName);
		readyServicesID.add(serviceDetails.registrationID());
	    } else {
		nonreadyServices.add(serviceName);
	    }
	    serviceDetails.addInterestByService(requestingService);
	    logger.info("NotifyServiceInterestByService: Added "+requestingService+" to "+serviceName);
	}
	

	ServiceDetails uiServiceDetails = serviceTable.getServiceDetails(RSController.UI_SERVICE);
	if (uiServiceDetails.ready()){
	    readyServices.add(RSController.UI_SERVICE);
	    readyServicesID.add(uiServiceDetails.registrationID());
	}

	logger.debug("NotifyServiceInterestByService: "+readyServices.size()+" active services and "+nonreadyServices.size()+" inactive services found.");
	ServiceNotifyMessage serviceNotifyMessage = new ServiceNotifyMessage(RSController.RESOURCE_MANAGER, 
									     requestingService, "",
									     readyServices.toArray(new String[]{}), 
									     readyServicesID.toArray(new String[]{}),
									     nonreadyServices.toArray(new String[]{}));
	if (ipcServer.handleOutgoingMessage(serviceNotifyMessage)<0)
	    logger.error("NotifyServiceInterestByService: Failed to send serviceNotifyMessage.");
	else
	    logger.debug("NotifyServiceInterestByService: serviceNotifyMessage sent successfully.");
    }

    protected void handleServiceInterestByWidget(String widgetName, String[] serviceInterests) {
	logger.debug("HandleServiceInterestByWidget: Entering");
	ArrayList<String> readyServices = new ArrayList<String>();
	ArrayList<String> readyServiceIDs = new ArrayList<String>();
	ArrayList<String> nonreadyServices = new ArrayList<String>();
	serviceInterestOfWidgets.put(widgetName, serviceInterests);

	for (String serviceName: serviceInterests) {
	    ServiceDetails serviceDetails = serviceTable.getServiceDetails(serviceName);
	    if (serviceDetails.ready()) {
		readyServices.add(serviceName);
		readyServiceIDs.add(serviceDetails.registrationID());
	    } else
		nonreadyServices.add(serviceName);
	    serviceDetails.addInterestByWidget(widgetName);
	    logger.info("HandleServiceInterestByWidget: Added "+widgetName+" to "+serviceName);
	}
	logger.debug("HandleServiceInterestByWidget: "+readyServices.size()+" active services and "+nonreadyServices.size()+" inactive services found.");
	ServiceAckMessage serviceAckMessage = new ServiceAckMessage(RSController.RESOURCE_MANAGER, 
								    RSController.UI_SERVICE, widgetName,
								    readyServices.toArray(new String[]{}), 
								    readyServiceIDs.toArray(new String[readyServiceIDs.size()]),
								    nonreadyServices.toArray(new String[]{}));
	if (ipcServer.handleOutgoingMessage(serviceAckMessage) < 0)
	    logger.error("HandleServiceInterestByWidget: failed to send serviceAckMessage.");
	else
	    logger.debug("HandleServiceInterestByWidget: serviceAckMessage sent successfully.");

	logger.debug("HandleServiceInterestByWidget: Leaving");
    }

    protected void handleServiceInterestByService(String requestingService, String[] serviceInterests) {
        logger.debug("HandleServiceInterestByService: Entering");
	ArrayList<String> readyServices = new ArrayList<String>();
	ArrayList<String> readyServiceIDs = new ArrayList<String>();
	ArrayList<String> nonreadyServices = new ArrayList<String>();
	serviceInterestOfServices.put(requestingService, serviceInterests);
	
	for (String serviceName: serviceInterests) {
	    ServiceDetails serviceDetails = serviceTable.getServiceDetails(serviceName);
	    if (serviceDetails.ready()) {
		readyServices.add(serviceName);
		readyServiceIDs.add(serviceDetails.registrationID());
	    } else
		nonreadyServices.add(serviceName);
	    serviceDetails.addInterestByService(requestingService);
	    logger.info("HandleServiceInterestByService: Added "+requestingService+" to "+serviceName);
	}
	logger.debug("HandleServiceInterestByService: "+readyServices.size()+" active services and "+nonreadyServices.size()+" inactive services found.");
	ServiceAckMessage serviceAckMessage = new ServiceAckMessage(RSController.RESOURCE_MANAGER, 
								    requestingService, "",
								    readyServices.toArray(new String[]{}), 
								    readyServiceIDs.toArray(new String[]{}),
								    nonreadyServices.toArray(new String[]{}));
	if (ipcServer.handleOutgoingMessage(serviceAckMessage)<0)
	    logger.error("HandleServiceInterestByService: Failed to send serviceAckMessage.");
	else
	    logger.debug("HandleServiceInterestByService: serviceAckMessage sent successfully.");
    }

    
    protected String shouldPreempt(String serviceName, ResourceDetails resourceDetails) {
	logger.debug("ShouldPreempt: Entering. ServiceName="+serviceName+", resourceName="+resourceDetails.getName());
	if (resourceDetails.getState() != RES_PREEMPT){
	    String serviceType = stationConfig.getServiceInstance(serviceName).serviceType();
	    if (serviceType.equals(RSController.AUDIO_SERVICE)) {
		Iterator<String> iter = resourceDetails.getResourceHolders().iterator();
		if (iter.hasNext()) {
		    String service = iter.next();
		    String type = stationConfig.getServiceInstance(service).serviceType();
		    if (type.equals(RSController.MONITOR_SERVICE)){
			logger.debug("ShouldPreempt: Leaving with service:"+service);
			return service;
		    }
		}
	    }
	    if (serviceType.equals(RSController.ONLINE_TELEPHONY_SERVICE)) {
		Iterator<String> iter = resourceDetails.getResourceHolders().iterator();
		if (iter.hasNext()) {
		    String service = iter.next();
		    String type = stationConfig.getServiceInstance(service).serviceType();
		    if (type.equals(RSController.AUDIO_SERVICE) ||
		       type.equals(RSController.MONITOR_SERVICE)) {
			logger.debug("ShouldPreempt: Leaving with service:"+service);
			return service;
		    }
		}
	    }
	}
	logger.debug("ShouldPreempt:Leaving");
	return "";
    }

    protected void handleResourceRequest(String serviceName, String[] resources, String[] resourceRoles, 
					 boolean isPersistentRequest, boolean isSyncRequest, String messageID) {
	boolean requesterIsHolder = false;
	logger.debug("HandleResourceRequest: Entering. serviceName="+serviceName);
	ArrayList<String> successfulResources  = new ArrayList<String>();
	ArrayList<String> failedResources = new ArrayList<String>();
	ArrayList<String> successfulResourceRoles = new ArrayList<String>();
	ArrayList<String> failedResourceRoles = new ArrayList<String>();
	int i =0;
	ArrayList<String> preemptServices = new ArrayList<String>();
	ArrayList<String> preemptResources = new ArrayList<String>();

	for (String resourceName: resources) {
	    ResourceDetails resourceDetails = resourceTable.getResourceDetails(resourceName);

	    if (resourceDetails.getState() == RES_FREE) {
		successfulResources.add(resourceName);
		successfulResourceRoles.add(resourceRoles[i]);
		logger.debug("HandleResourceRequest: "+resourceName+" available.");
	    } else if (resourceDetails.getState() == RES_HELD) {
		logger.debug("HandleResourceRequest: "+resourceName+" held.");
		
		for (String holder : resourceDetails.getResourceHolders()) {
		    if (serviceName.equals(holder)) {
			requesterIsHolder = true;
			break;
		    }
		}
		if (requesterIsHolder) {
		    logger.debug("HandleResourceRequest: Requesting service is one of the holders of the resource:" + resourceName);
		    successfulResources.add(resourceName);
		    successfulResourceRoles.add(resourceRoles[i]);
		} else {
		    logger.debug("HandleResourceRequest: Requestor not one of the resource holders. Calling should preempt.");
		    String preemptService = shouldPreempt(serviceName, resourceDetails);
		    if (preemptService != ""){
			logger.debug("HandleResourceRequest: Resource "+resourceName+" held by "+preemptService+" but can be preempted");
			preemptServices.add(preemptService);
			preemptResources.add(resourceName);
			
			//Assuming pre-emption would anyways be successful.
			successfulResources.add(resourceName);
			successfulResourceRoles.add(resourceRoles[i]);
			resourceDetails.setState(RES_PREEMPT);
		    }
		    else {
			failedResources.add(resourceName);
			failedResourceRoles.add(resourceRoles[i]);
			StringBuilder str = new StringBuilder();
			for (String name: resourceDetails.getResourceHolders())
			    str.append(name+",");
			logger.debug("HandleResourceRequest: "+resourceName+" unavailable. Held by: "+str);
		    }
		}
	    }
	    if (isPersistentRequest)
		resourceDetails.addPersistentRequest(serviceName);
	    i++;
	}

	if (failedResources.size() == 0) {
	    if (preemptResources.size() != 0){
		//Not all resources free but can be preempted.
		//Send preempt message and store the state information
		//On receiving acks for the preempts send resource ack
		for (int j=0;j<preemptResources.size();j++) {
		    logger.debug("Preempting resource:"+preemptResources.get(j));
		    
		    PreemptResourceMessage message = new PreemptResourceMessage(RSController.RESOURCE_MANAGER, preemptServices.get(j),
										new String[] {preemptResources.get(j)});
		    
		    if (ipcServer.handleOutgoingMessage(message) < 0)
			logger.error("HandleResourceRequest: Failed to send preempt resource message.");
		    else
			logger.debug("HandleResourceRequest: Preempt resource message sent successfully.");
		}
		PendingRequest request = new PendingRequest(serviceName, resources, resourceRoles, preemptServices, preemptResources, isSyncRequest, messageID);
		pendingRequests.add(request);
	    }else{//Got all the resources required.
		
		for (String resourceName: resources) {
		    ResourceDetails resourceDetails = resourceTable.getResourceDetails(resourceName);
		    resourceDetails.setState(RES_HELD);
		    resourceDetails.holdResource(serviceName);
		    logger.info("HandleResourceRequest: Resource: "+resourceName+" held for service: "+serviceName);
		}
		
		sendResourceAck(serviceName, successfulResources, successfulResourceRoles, 
				failedResources, failedResourceRoles, isSyncRequest, messageID);
	    }

	    PreemptBackgroundActivityMessage preemptMessage = new PreemptBackgroundActivityMessage(RSController.RESOURCE_MANAGER,
												   RSController.LIB_SERVICE,
												   serviceName);
	    if (ipcServer.handleOutgoingMessage(preemptMessage) < 0)
		logger.error("HandleResourceRequest: Failed to send Preempt BackGround Activity Message.");
	    else
		logger.debug("HandleResourceRequest: Preempt Background Activity message sent successfully.");

	    logger.debug("HandleResourceRequest: Leaving");
	} else {
	    sendResourceAck(serviceName, successfulResources, successfulResourceRoles, failedResources, 
			    failedResourceRoles, isSyncRequest, messageID);
	}

	logger.debug("HandleResourceRequest: Leaving");
    }

    protected void sendResourceAck(String serviceName, ArrayList<String> successfulResources, ArrayList<String> successfulResourceRoles, ArrayList<String> failedResources, ArrayList<String> failedResourceRoles, boolean isSyncRequest, String messageID) {
	if (isSyncRequest) {
	    ResourceAckSyncMessage resourceAckMessage = new ResourceAckSyncMessage(RSController.RESOURCE_MANAGER, 
										   serviceName,
										   successfulResources.toArray(new String[]{}),
										   successfulResourceRoles.toArray(new String[]{}),
										   failedResources.toArray(new String[]{}),
										   failedResourceRoles.toArray(new String[]{}), 
										   messageID);
	    
	    if (ipcServer.handleOutgoingMessage(resourceAckMessage) < 0)
		logger.error("SendResourceAck: Failed to send resource Ack Message.");
	    else
		logger.debug("SendResourceAck: Resource Ack message sent successfully.");
	} else {
	    ResourceAckMessage resourceAckMessage = new ResourceAckMessage(RSController.RESOURCE_MANAGER, serviceName,
									   successfulResources.toArray(new String[]{}),
									   successfulResourceRoles.toArray(new String[]{}),
									   failedResources.toArray(new String[]{}),
									   failedResourceRoles.toArray(new String[]{}));
	    
	    if (ipcServer.handleOutgoingMessage(resourceAckMessage) < 0)
		logger.error("SendResourceAck: Failed to send resource Ack Message.");
	    else
		logger.debug("SendResourceAck: Resource Ack message sent successfully.");
	}
    }

    protected void handleResourceRelease(String serviceName, String[] resources, boolean isSyncRelease, String messageID) {
	logger.debug("HandleResourceRelease: Entering. serviceName="+serviceName);
	for (String resourceName: resources) {
	    ResourceDetails resourceDetails = resourceTable.getResourceDetails(resourceName);
	    resourceDetails.releaseResource(serviceName);
	    resourceDetails.removePersistentRequest(serviceName);
	    if (resourceDetails.getNumResourceHolders() == 0)
	    	resourceDetails.setState(RES_FREE);
	    logger.info("HandleResourceRelease: Releasing resource: "+resourceName+" by service: " + serviceName);

	    if (resourceDetails.getState() == RES_FREE)
	        notifyPersistentRequesters(resourceDetails);
	}

	if (isSyncRelease) {
	    ResourceReleaseAckSyncMessage response = new ResourceReleaseAckSyncMessage(RSController.RESOURCE_MANAGER, 
										       serviceName,
										       resources,
										       new String[] {},
										       messageID);
	    if (ipcServer.handleOutgoingMessage(response) < 0)
		logger.error("HandleResourceRelease: Failed to send resource release Ack Message.");
	    else
		logger.debug("HandleResourceRelease: Resource release Ack message sent successfully.");
	}
	logger.debug("HandleResourceRelease: Leaving");
    }

    protected void notifyPersistentRequesters(ResourceDetails resourceDetail) {
	logger.debug("NotifyPersistentRequesters: Entering. Resource="+resourceDetail.getName());
	
	if (resourceDetail.getNumResourceHolders() == 0) {
	    if (resourceDetail.getNumPersistentRequesters() == 0)
		logger.debug("NotifyPersistentRequesters: No persistent requesters for the resource.");

	    Iterator<String> iter = resourceDetail.getPersistentRequesters().iterator();

	    // XXX Only allocate to one service. What to do if multiple services had requested?
	    if (iter.hasNext()) {
		String serviceName = iter.next();
		ResourceAckMessage resourceAckMessage = new ResourceAckMessage(RSController.RESOURCE_MANAGER, serviceName, new String[]{resourceDetail.getName()}, new String[]{resourceDetail.getResourceType()}, new String[]{}, new String[]{});
		resourceDetail.setState(RES_HELD);
		resourceDetail.holdResource(serviceName);
		logger.info("NotifyPersistentRequesters: Resource: "+resourceDetail.getName()+" held for persistent service: "+serviceName);
		if (ipcServer.handleOutgoingMessage(resourceAckMessage) < 0)
		    logger.error("NotifyPersistentRequesters: Unable to send resourceAckMessage to "+serviceName);
		else
		    logger.debug("NotifyPersistentRequesters: resourceAckMessage sent successfully.");
	    }
	}
	else
	    logger.debug("NotifyPersistentRequesters: Non zero entities hold the resource.");
	logger.debug("NotifyPersistentRequesters: Leaving");
    }

    protected void handleServiceError(String serviceName, String error, String errorCode, String payload) {
	logger.info("HandleServiceError: Received error message from service: "+serviceName+" Error: " + error + " Code: " + errorCode);
	ServiceDetails serviceDetails = serviceTable.getServiceDetails(serviceName);
	Iterator<String> iterator = serviceDetails.getInterestsByWidgets();
	while (iterator.hasNext()){
	    String interestedWidget = iterator.next();
	    ServiceErrorRelayMessage serviceErrorRelayMessage = new ServiceErrorRelayMessage(RSController.RESOURCE_MANAGER, 
											     RSController.UI_SERVICE, serviceName,
											     interestedWidget, error, errorCode, payload);

	    logger.debug("HandleServiceError: Forwarding error message to widget: "+interestedWidget);
	    if (ipcServer.handleOutgoingMessage(serviceErrorRelayMessage) < 0)
		logger.error("HandleServiceError: Unable to send ServiceErrorRelayMessage to "+interestedWidget);
	    else
		logger.debug("HandleServiceError: Error message forwarded successfully.");
	}

	iterator = serviceDetails.getInterestsByServices();
	while (iterator.hasNext()){
	    String interestedService = iterator.next();
	    ServiceErrorRelayMessage serviceErrorRelayMessage = new ServiceErrorRelayMessage(RSController.RESOURCE_MANAGER, 
											     interestedService, serviceName,
											     "", error, errorCode, payload);

	    logger.debug("HandleServiceError: Forwarding error message to service: "+interestedService);
	    if (ipcServer.handleOutgoingMessage(serviceErrorRelayMessage) < 0)
		logger.error("HandleServiceError: Unable to send ServiceErrorRelayMessage to "+interestedService);
	    else
		logger.debug("HandleServiceError: Service error message forwarded successfully.");
	}
    }

    public void handleServiceReady(String serviceName, boolean status, String messageID, boolean sendAck) {
	logger.debug("HandleServiceReady: serviceName="+serviceName+" status="+status);
	if (status) {
	    logger.info("HandleServiceReady: Service up: " + serviceName);

	    ServiceDetails serviceDetails = serviceTable.getServiceDetails(serviceName);
	    ServiceDetails uiServiceDetails = serviceTable.getServiceDetails(RSController.UI_SERVICE);
	    serviceDetails.setReady(true);
	    serviceDetails.setDisconnected(false);
	    
	    if (!serviceName.equals(RSController.UI_SERVICE)) {
		if (serviceTable.getServiceDetails(RSController.UI_SERVICE).ready()) {
		    ServiceActivateNotifyMessage notifyMessage = new ServiceActivateNotifyMessage(RSController.RESOURCE_MANAGER, serviceName, RSController.UI_SERVICE, uiServiceDetails.registrationID());
		    logger.debug("HandleServiceReady: UI_SERVICE is ready, sending notify message to " + serviceName);
		    if (ipcServer.handleOutgoingMessage(notifyMessage) < 0)
			logger.error("HandleServiceReady: Unable to send NotifyMessage to "+serviceName);
		    else
			logger.debug("HandleServiceReady: NotifyMessage sent successfully.");
		}
	    }

	    Iterator<String> iterator = serviceDetails.getInterestsByWidgets();
	    while (iterator.hasNext()){
		String interestedWidget = iterator.next();
		ServiceNotifyMessage serviceNotifyMessage = new ServiceNotifyMessage(RSController.RESOURCE_MANAGER, 
										     RSController.UI_SERVICE, 
										     interestedWidget,
										     new String[] {serviceName}, 
										     new String[] {serviceDetails.registrationID()},
										     new String[] {});

		logger.debug("HandleServiceReady: Notifying widget: "+interestedWidget);
		if (ipcServer.handleOutgoingMessage(serviceNotifyMessage) < 0)
		    logger.error("HandleServiceReady: Unable to send NotifyMessage to "+interestedWidget);
		else
		    logger.debug("HandleServiceReady: Notify sent successfully.");
	    }
	    
	    iterator = serviceDetails.getInterestsByServices();
	    while (iterator.hasNext()){
		String interestedService = iterator.next();
		ServiceNotifyMessage serviceNotifyMessage = new ServiceNotifyMessage(RSController.RESOURCE_MANAGER, 
										     interestedService, "",
										     new String[] {serviceName}, 
										     new String[] {serviceDetails.registrationID()},
										     new String[] {});

		logger.debug("HandleServiceReady: Notifying service: "+interestedService);
		if (ipcServer.handleOutgoingMessage(serviceNotifyMessage) < 0)
		    logger.error("HandleServiceReady: Unable to send Notify to "+interestedService);
		else
		    logger.debug("HandleServiceReady: Notify sent successfully.");
	    }

	    if (serviceName.equals(RSController.UI_SERVICE)) {
		ServiceActivateNotifyMessage notifyMessage = new ServiceActivateNotifyMessage(RSController.RESOURCE_MANAGER, IPCMessage.DEST_BCAST, serviceName, serviceDetails.registrationID());
		logger.debug("HandleServiceReady: UI_SERVICE ready, sending broadcast message");
		ipcServer.handleOutgoingMessage(notifyMessage);
	    }

	} else {

	    /* Service down. status == false */

	    logger.info("HandleServiceReady down: " + serviceName);

	    ServiceDetails serviceDetails = serviceTable.getServiceDetails(serviceName);
	    serviceDetails.setReady(false);
	    //we reach here when either the service is being deactivated or ready message was sent by
	    //service itself. In both cases setting disconnected to false is ok.
	    serviceDetails.setDisconnected(false);

	    if (serviceName.equals(RSController.UI_SERVICE)) {
		ServiceErrorRelayMessage errorMessage = new ServiceErrorRelayMessage(RSController.RESOURCE_MANAGER, IPCMessage.DEST_BCAST, RSController.UI_SERVICE, "", "UI Service down.",ServiceErrorRelayMessage.CONNECTION_ERROR, "");
		logger.info("HandleServiceReady: Deactivating UI_SERVICE, sending broadcast message");
		ipcServer.handleOutgoingMessage(errorMessage);
	    }

	    handleServiceError(serviceName, "Service not ready", ServiceErrorRelayMessage.CONNECTION_ERROR, "");

	    String[] releasedResources = resourceTable.removeService(serviceName);
	    for (String releasedResource: releasedResources) {
		if (resourceTable.getResourceDetails(releasedResource).getNumResourceHolders() == 0) {
		    logger.debug("HandleServiceReady: Releasing resource: "+releasedResource);
		    resourceTable.getResourceDetails(releasedResource).setState(RES_FREE);
	            notifyPersistentRequesters(resourceTable.getResourceDetails(releasedResource));
		} else {
		    logger.debug("HandleServiceReady: Resource: "+releasedResource+" not released. Num holders: "+resourceTable.getResourceDetails(releasedResource).getNumResourceHolders());
		}
	    }
	}

	if (sendAck) {
	    ServiceReadyAckMessage ackMessage = new ServiceReadyAckMessage(RSController.RESOURCE_MANAGER, serviceName, status, messageID);
	    ipcServer.handleOutgoingMessage(ackMessage);
	}

	logger.debug("HandleServiceReady: Leaving");
    }
    
    public void newService(String name, Boolean isRemote, String registrationID) {
	logger.info("NewService: Detected: "+name+", "+registrationID);

	if (name.contains(RSController.SERVLET)) {
	    ipcServer.sendPersistentMessages(name);
	    return;
	}

	boolean registerSuccess = true;
	ServiceDetails serviceDetails = serviceTable.getServiceDetails(name);
	boolean isNewInstance = false;

	if (serviceDetails.active()) {
	    //re-registration
	    logger.info("NewService: Re-registration");
	    
	    synchronized(diagUtil) {
		diagUtil.removeService(name);
	    }
	    
	    if (!serviceDetails.registrationID().equals(registrationID)) {
		//new instance
		logger.debug("NewService: It is a new instance");
		logger.warn("NewService: New instance of the service " + name + " detected. There may be multiple instances of the service running.");
		isNewInstance = true;
		
		if (serviceDetails.disconnected()) {
		    serviceDetails.setReady(serviceDetails.readinessAtNetworkDown());
		}

		deactivateService(name);
		
		serviceDetails.setActive(true);
		serviceDetails.setRegistrationID(registrationID);

	    } else {
		//restore its old readiness
		if (serviceDetails.readinessAtNetworkDown()) {
		    logger.debug("NewService: Restoring old readiness");
		    handleServiceReady(name, true, "", false);
		    ipcServer.sendPersistentMessages(name);
		} else {
		    logger.debug("NewService: Was not ready when it went down.");
		}
		//send service notify message for all services this service is interested in.
		notifyService(name);
	    }

	    if (isRemote)
		linkMonitor.addNode(name);
	} else {
	    if (registrationID.equals(serviceDetails.registrationID())) {
		//Cant register with old registration id when declared inactive by rscontroller
		registerSuccess = false;
	    } else {
		isNewInstance = true;
		serviceDetails.setActive(true);
		serviceDetails.setRegistrationID(registrationID);

		if (isRemote)
		    linkMonitor.addNode(name);
	    }
	}	
	//XXX this should be sent only when IndexService is ready to accept messages. 
		
	if (name.equals(RSController.LIB_SERVICE) && isNewInstance) {
	    /*
             StartCleanupMessage startCleanup = new StartCleanupMessage(RSController.RESOURCE_MANAGER, 
								       RSController.LIB_SERVICE, 
								       new String[]{StartCleanupMessage.ENCODED_FILES,
								       StartCleanupMessage.DELETED_FILES});
	    ipcServer.handleOutgoingMessage(startCleanup);
	    */
		    
	} else if (stationConfig.getServiceInstance(name).serviceType().equals(RSController.ARCHIVER_SERVICE) && isNewInstance) {
	    StartCleanupMessage startCleanup = new StartCleanupMessage(RSController.RESOURCE_MANAGER, 
								       name, 
								       new String[]{StartCleanupMessage.INCOMPLETE_ARCHIVING});
	    ipcServer.handleOutgoingMessage(startCleanup);
		    
	} else if (stationConfig.getServiceInstance(name).serviceType().equals(RSController.ONLINE_TELEPHONY_SERVICE) && isNewInstance) {
	    StartCleanupMessage startCleanup = new StartCleanupMessage(RSController.RESOURCE_MANAGER, 
								       name, 
								       new String[]{StartCleanupMessage.INCOMPLETE_TELEPHONY});
	    ipcServer.handleOutgoingMessage(startCleanup);
	}
	 
       

	RegistrationAckMessage msg = new RegistrationAckMessage(RSController.RESOURCE_MANAGER,
								name,
								resourceManagerID,
								registerSuccess);
	ipcServer.handleOutgoingMessage(msg);
    }

    public synchronized void serviceError(String name) {
	logger.error("ServiceError: "+name);

	//In case of servlet just do ipcnode cleaning up.
	//no other information about it is maintained
	if (name.contains(RSController.SERVLET)) {
	    ipcServer.disconnect(name); 
	    return;
	}

	//if (!name.equals(erroredService)){
	//    erroredService = name;
	ServiceDetails serviceDetails = serviceTable.getServiceDetails(name);
	
	linkMonitor.removeNode(name);
	ipcServer.disconnect(name);
	serviceDetails.setDisconnected(true);
	logger.debug("Setting readiness at network down = " + serviceDetails.ready());
	serviceDetails.setReadinessAtNetworkDown(serviceDetails.ready());
	if (serviceDetails.ready()) {
	    serviceDetails.setReady(false);
	    notifyAll(name);
	}
	if (serviceDetails.active()) {
	    diagUtil.rscConnectionError(name, this);
	}
	
	//erroredService = "";
	//}
	/*linkMonitor.removeNode(name);
	  deactivateService(name);*/
    }

    //Notify all the services/widgets interested in this service that
    //network connection to it is lost
    protected void notifyAll(String serviceName) {
	logger.debug("NotifyAll: serviceName="+serviceName);
	
	ServiceDetails serviceDetails = serviceTable.getServiceDetails(serviceName);
		
	//Informing all services when UIService connection goes down is not really needed 
	//as the services would not need to take any action on this
	/*if (serviceName.equals(RSController.UI_SERVICE)) {
	    ServiceNotifyMessage notifyMessage = new ServiceNotifyMessage(RSController.RESOURCE_MANAGER, IPCMessage.DEST_BCAST, "", new String[] {}, new String[] {RSController.UI_SERVICE});
	    logger.debug("NotifyAll: Connection to UI_SERVICE lost, broadcasting notify message");
	    if (ipcServer.handleOutgoingMessage(notifyMessage) < 0)
		logger.error("NotifyAll: Unable to send broadcast NotifyMessage.");
	    else
		logger.debug("NotifyAll: NotifyMessage sent successfully.");
		} else {*/
	    Iterator<String> iterator = serviceDetails.getInterestsByWidgets();
	    while (iterator.hasNext()){
		String interestedWidget = iterator.next();
		ServiceNotifyMessage serviceNotifyMessage = new ServiceNotifyMessage(RSController.RESOURCE_MANAGER, 
										     RSController.UI_SERVICE, 
										     interestedWidget,
										     new String[] {}, new String[] {},
										     new String[] {serviceName});
	    
		logger.debug("NotifyAll: Notifying widget: "+interestedWidget);
		if (ipcServer.handleOutgoingMessage(serviceNotifyMessage) < 0)
		    logger.error("NotifyAll: Unable to send NotifyMessage to "+interestedWidget);
		else
		    logger.debug("NotifyAll: Notify sent successfully.");
	    }
	    
	    //Right now on Lib service and Index service have two way soft dependencies, while no 
	    //other services have interdependencies. These two services can live without the 
	    //notification. Can be uncommented for generality later.

	    /*iterator = serviceDetails.getInterestsByServices();
	    while (iterator.hasNext()){
		String interestedService = iterator.next();
		ServiceNotifyMessage serviceNotifyMessage = new ServiceNotifyMessage(RSController.RESOURCE_MANAGER, 
										     interestedService, "",
										     new String[] {}, 
										     new String[] {serviceName});
		
		logger.debug("NotifyAll: Notifying service: "+interestedService);
		if (ipcServer.handleOutgoingMessage(serviceNotifyMessage) < 0)
		    logger.error("NotifyAll: Unable to send Notify to "+interestedService);
		else
		    logger.debug("NotifyAll: Notify sent successfully.");
	    }
	    }*/
    }

    //Notify the service about all the services it is interested in.
    //XXX needs optimization.
    protected void notifyService(String serviceName){
	
	if (serviceName.equals(RSController.UI_SERVICE)) {
	    Enumeration<String> widgetEnum = serviceInterestOfWidgets.keys();
	    while (widgetEnum.hasMoreElements()) {
		String widget = widgetEnum.nextElement();
		String[] interests = serviceInterestOfWidgets.get(widget);
		notifyServiceInterestByWidget(widget, interests);
	    }
	} else {
	    String[] serviceInterests = serviceInterestOfServices.get(serviceName);
	    if (serviceInterests != null) {
		notifyServiceInterestByService(serviceName, serviceInterests);
	    }
	}
    }

    protected void deactivateService(String name) {
	logger.info("DeactivateService: "+name);
	ServiceDetails serviceDetails = serviceTable.getServiceDetails(name);
	serviceDetails.setActive(false);
	if (serviceDetails.ready())
	    handleServiceReady(name, false, "", false);
	// diagnostics
    }

    public String getName() {
	return RSController.RESOURCE_MANAGER;
    }

    public void nodeUp(String serviceName){

    }

    public void nodeDown(String serviceName){
	logger.error("NodeDown: "+serviceName);
	serviceError(serviceName);
	//deactivateService(serviceName);
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	message.setSource(RSController.RESOURCE_MANAGER);
	return ipcServer.handleOutgoingMessage(message);
    }

    class ServiceDetails {
	String serviceName;
	String registrationID;
	boolean active;
	boolean ready;					
	boolean readinessAtNetworkDown;
	boolean disconnected;
	HashSet<String> serviceInterestsByWidgets;
	HashSet<String> serviceInterestsByServices;
	
	public ServiceDetails(String name) {
	    this.serviceName = name;
	    serviceInterestsByWidgets = new HashSet<String>();
	    serviceInterestsByServices = new HashSet<String>();
	    active = false;
	    ready = false;
	    readinessAtNetworkDown = false;
	    disconnected = false;
	}

	public synchronized void setDisconnected(boolean set) {
	    disconnected = set;
	}

	public synchronized boolean disconnected() {
	    return disconnected;
	}

	// nodes are widgets interested in a service
	public synchronized void addInterestByWidget(String nodeName) {
	    serviceInterestsByWidgets.add(nodeName);
	}

	public synchronized Iterator<String> getInterestsByWidgets() {
	    return serviceInterestsByWidgets.iterator();
	}

	public synchronized void addInterestByService(String nodeName) {
	    serviceInterestsByServices.add(nodeName);
	}

	public synchronized Iterator<String> getInterestsByServices() {
	    return serviceInterestsByServices.iterator();
	}

	public synchronized boolean active() {
	    return active;
	}

	public synchronized void setActive(boolean active) {
	    this.active = active;
	}

	public synchronized boolean ready() {
	    return ready;
	}

	public synchronized void setReady(boolean ready) {
	    this.ready = ready;
	}

	public synchronized boolean readinessAtNetworkDown(){
	    return readinessAtNetworkDown;
	}

	public synchronized void setReadinessAtNetworkDown(boolean set){
	    readinessAtNetworkDown = set;
	}

	public synchronized void setRegistrationID(String id){
	    registrationID = id;
	}

	public synchronized String registrationID(){
	    return registrationID;
	}

	public synchronized String getName(){
	    return serviceName;
	}
    }

    class ServiceTable {
	Hashtable<String, ServiceDetails> serviceTable;

	public ServiceTable() {
	    serviceTable = new Hashtable<String, ServiceDetails>();
	}

	public ServiceDetails getServiceDetails(String serviceName) {
	    ServiceDetails serviceDetails = serviceTable.get(serviceName);
	    if (serviceDetails == null) {
		serviceDetails = new ServiceDetails(serviceName);
		serviceTable.put(serviceName, serviceDetails);
	    }
	    return serviceDetails;
	}

	public Iterable<ServiceDetails> getAllServiceDetails(){
	    return serviceTable.values();
	}
    }

    class ResourceDetails {
	String resourceName;
	int state;
	String machineID;
	String resourceType;
	Set<String> resourceHolders;
	Set<String> persistentRequesters;
	LogUtilities logger;

	public ResourceDetails(String name) {
	    this.resourceName = name;
	    resourceHolders = new HashSet<String>();
	    persistentRequesters = new HashSet<String>();
	    logger = new LogUtilities("ResourceDetails:"+name);
	}

	public synchronized int getState() {
	    return state;
	}

	public synchronized void setState(int state) {
	    this.state = state;
	}
       
	public synchronized Iterable<String> getResourceHolders() {
	    return resourceHolders;
	}

	public synchronized int getNumResourceHolders() {
	    return resourceHolders.size();
	}

	public synchronized void holdResource(String serviceName) {
	    logger.debug("HoldResource: "+serviceName);
	    resourceHolders.add(serviceName);
	}

	public synchronized boolean releaseResource(String serviceName) {
	    if (resourceHolders.contains(serviceName)){
		logger.debug("ReleaseResource: "+serviceName);
		resourceHolders.remove(serviceName);
		return true;
	    }
	    return false;
	}

	public synchronized void removePersistentRequest(String serviceName) {
	    persistentRequesters.remove(serviceName);
	}

	public synchronized void addPersistentRequest(String serviceName) {
	    persistentRequesters.add(serviceName);
	}

	public synchronized Iterable<String> getPersistentRequesters() {
	    return persistentRequesters;
	}
	
	public synchronized int getNumPersistentRequesters() {
	    return persistentRequesters.size();
	}

	public String getName() {
	    return resourceName;
	}

	public String getMachineID() {
	    return machineID;
	}

	public void setMachineID(String machineID) {
	    this.machineID = machineID;
	}

	public String getResourceType() {
	    return resourceType;
	}

	public void setResourceType(String resourceType) {
	    this.resourceType = resourceType;
	}
    }

    class ResourceTable {
	Hashtable<String, ResourceDetails> resourceTable;

	public ResourceTable() {
	    resourceTable = new Hashtable<String, ResourceDetails>();
	}

	public ResourceDetails getResourceDetails(String resourceName) {
	    ResourceDetails resourceDetails = resourceTable.get(resourceName);
	    if (resourceDetails == null) {
		resourceDetails = new ResourceDetails(resourceName);
		resourceTable.put(resourceName, resourceDetails);
	    }
	    return resourceDetails;
	}

	public String[] removeService (String serviceName){
	    ArrayList<String> releasedResources = new ArrayList<String>();
	    for (ResourceDetails resourceDetail: resourceTable.values()){
		if (resourceDetail.releaseResource(serviceName)){
		    resourceDetail.removePersistentRequest(serviceName);
		    releasedResources.add(resourceDetail.getName());
		}
	    }
	    if (releasedResources.size() > 0) {
		return releasedResources.toArray(new String[]{});
	    } else {
		return new String[]{};
	    }
	}
	
    }

    class PendingRequest {
	String serviceName;
	String[] resources;
	String[] resourceRoles;
	ArrayList<String> preemptServices;
	ArrayList<String> preemptResources;
	boolean isSyncRequest;
	String messageID;

	PendingRequest(String serviceName, String[] resources, String[] resourceRoles, ArrayList<String> preemptServices, ArrayList<String> preemptResources, boolean isSyncRequest, String messageID) {
	    this.serviceName = serviceName;
	    this.resources = resources;
	    this.resourceRoles = resourceRoles;
	    this.preemptServices = preemptServices;
	    this.preemptResources = preemptResources;
	    this.isSyncRequest = isSyncRequest;
	    this.messageID = messageID;
	}

	public String serviceName(){
	    return serviceName;
	}

	public String[] resources(){
	    return resources;
	}

	public String[] resourceRoles(){
	    return resourceRoles;
	}

	public ArrayList<String> preemptResources(){
	    return preemptResources;
	}

	public ArrayList<String> preemptServices(){
	    return preemptServices;
	}
    }
	
}
