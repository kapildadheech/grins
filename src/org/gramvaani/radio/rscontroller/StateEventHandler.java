package org.gramvaani.radio.rscontroller;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface StateEventHandler {
    String[] states() default {};
    String[] events() default {};
    String[] stateEventList() default {};
    boolean  varargs() default false;
}