package org.gramvaani.radio.rscontroller;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EmitSignal {
    String value();
}