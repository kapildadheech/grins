package org.gramvaani.radio.rscontroller.servlets;

import org.gramvaani.radio.app.providers.MediaLibUploadProvider;
import org.gramvaani.radio.app.gui.DiagnosticsController;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.simpleipc.*;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.concurrent.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;

import org.gstreamer.*;
import org.gstreamer.elements.*;
import org.gstreamer.event.*;
import org.gstreamer.query.*;

public class MediaLibUploadServlet extends HttpServlet implements IPCNodeCallback, IPCNode, Runnable {
    StationConfiguration stationConfig;
    StationNetwork stationNetwork;
    LogUtilities logger;
    MediaLib medialib;
    IPCServerStub ipcServer = null;
    boolean ackReceived = false;
    UploadDoneMessage currentUploadDoneMessage;
    boolean servletRegistered = false;
    Object regSyncObject = new Object();
    Hashtable<String, String[]> httpRequestParams;
    LinkedBlockingQueue<IPCMessage> libMessageQueue;


    Object libMessagesSyncObjet = new Object();

    //httprequest params
    public static final String PARAM_DOWNLOAD_FILENAME 	= "PARAM_DOWNLOAD_FILENAME";
    public static final String PARAM_FILESTORE_NAME 	= "PARAM_FILESTORE_NAME";
    public static final String PARAM_DB_INSERT 		= "PARAM_DB_INSERT";
    public static final String PARAM_LIB_FILENAME 	= "PARAM_LIB_FILENAME";
    public static final String PARAM_UPLOADER_NAME 	= "PARAM_UPLOADER_NAME";

    static final String ERROR_NO_SPACE_STRING     = "No space left on device";
    static final String ERROR_NOT_WRITABLE_STRING = "Not writable";
    static final String ERROR_DB_INSERT_FAILED_STRING = "DB insert failed";
    static final String ERROR_MAX_EXCEEDED_STRING = "Posted content length of ";

    static final long MESSAGE_ACK_WAIT_TIME = 2000; //2 seconds
    static final int  MAX_RETRIES = 3; //trying to send the same message three times. Then giving up.
    static final long IPCNULL_RETRY_INTERVAL = 2000; //2 seconds

    String configDir = "";
    //String scriptsDir;
    String registrationID;
    Thread libCommunicationThread;
    String servletName;

    public void init() throws ServletException {
	processEnvFile();
	logger = new LogUtilities("MediaLibUploadServlet");
	stationConfig = new StationConfiguration();
	LogUtilities.logToFile(RSController.SERVLET, stationConfig.getStringParam(StationConfiguration.LOG_FILE_SIZE));

	String configFile = getServletContext().getInitParameter("configfile"); 
	if (configFile == null) 
	    configFile = getServletContext().getRealPath("automation.conf");

	if (configFile == null) {
	    logger.error("Init: Could not find path to configuration file. Cannot continue.");
	    System.exit(-1);
	}

	stationConfig.readConfig(configFile);

	//dumpProperties();
	stationNetwork = stationConfig.getStationNetwork();
	ipcServer = new IPCServerStub(stationConfig.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfig.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfig.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfig.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfig.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));

	MediaLib.setIPCServer(ipcServer);

	medialib = MediaLib.getMediaLib(stationConfig, null);
	if (medialib != null) {
	    medialib.setLibraryActivated(true);
	}

	httpRequestParams = new Hashtable<String, String[]>();
	libMessageQueue = new LinkedBlockingQueue<IPCMessage>();
	registrationID = GUIDUtils.getGUID();
	servletName = RSController.SERVLET + "_" + stationNetwork.getALocalStoreName();

	libCommunicationThread = new Thread(this);
	libCommunicationThread.start();
	

	try{
	    configDir = new File(configFile).getParentFile().getAbsolutePath();
	    //scriptsDir = configDir +"/WEB-INF/scripts";
	} catch (Exception e){
	    logger.error("Init: Unable to find config directory.");
	}

	Thread t = new Thread() {
		public void run() {
		    Gst.main();
		}
	    };
	t.start();

	Gst.init(getName(), new String[] {"--gst-disable-segtrap"});
    }

    void dumpProperties() {
	Properties properties = System.getProperties();
	for(String key : properties.stringPropertyNames())
	    logger.info("DumpProperties: " + key + " -> " + properties.getProperty(key));
    }

    void processEnvFile() {
	File envFile = new File(System.getProperty("grins.envfile"));

	try {
	    BufferedReader reader = new BufferedReader(new FileReader(envFile));
	    String line, key, value;
	    String[] keyVal, temp;
	    while((line = reader.readLine()) != null) {
		line.trim();
		if (line.equals("") || line.startsWith("#"))
		    continue;

		keyVal = line.split("=");
		if (keyVal.length > 2) {
		    System.err.println("ProcessEnvFile: Incorrectly formatted line: " + line);
		    continue;
		}

		keyVal[0].trim();
		temp = keyVal[0].split(" ");
		key = temp[temp.length - 1];
		value = keyVal[1].trim();
		value = value.replaceAll("\"", "");
		System.setProperty(key, value);
	    }

	    reader.close();
	    
	} catch (FileNotFoundException fe) {

	    System.err.println("Warning: ProcessEnvFile: Could not find the GRINS environment file at " + 
			       envFile.getPath() + " " + fe.toString());

	} catch (NullPointerException ne) {
	    System.err.println("Warning: ProcessEnvFile: Path to GRINS environment file not provided.");
	} catch (Exception e) {
	    System.err.println("Error: Caught exception: " + e.toString());
	}
    }

    public String getExtension(String filename) {
	return filename.substring(filename.lastIndexOf(".") + 1);
    }
    
    public String removeExtension(String filename){
	if (filename.matches(".*[.].{3}$"))
	    return filename.substring(0, filename.length()-4);
	else
	    return filename;
    }

    public long getTimekeeperTime() {
	return System.currentTimeMillis();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	if (isDownloadRequest(request)) {
	    handleDownloadRequest(request, response);
	    return;
	}
	
	boolean sendUploadDone = false;
	String libFilename = "";
	String fileStoreName;
	String storeDir;
	String filename, originalName;
	EncodedFiles encodedFileEntry = null;

	PrintWriter out = response.getWriter();
	ArrayList<String> outputseq = new ArrayList<String>();
	httpRequestParams.clear();

	response.setContentType("text/html");

	try {
	    getParams(request, outputseq);
	    fileStoreName = httpRequestParams.get(PARAM_FILESTORE_NAME)[0];
	    storeDir = stationNetwork.getStoreDir(fileStoreName);
	    String[] libFilenameArray = httpRequestParams.get(PARAM_LIB_FILENAME);
	    if (libFilenameArray != null)
		libFilename = libFilenameArray[0];

	    if (stationNetwork.isSameFileStore(fileStoreName, StationNetwork.UPLOAD)){
		preemptEncoding();
	    }
	    
	    MultipartRequest multipart = new MultipartRequest(request,
							      storeDir,
							      stationConfig.getIntParam(StationConfiguration.UPLOAD_MAX_FILESIZE),
							      "ISO-8859-1", new DefaultFileRenamePolicy());
	    
	    logger.info("DoPost: Servlet success");
	    getFilenames(multipart, outputseq);
	    

	    filename = multipart.getFilesystemName(MediaLibUploadProvider.UPLOAD_FILE_PARAM);
	    originalName = multipart.getOriginalFileName(MediaLibUploadProvider.UPLOAD_FILE_PARAM);
	    
	    if (medialib == null) {
		medialib = MediaLib.getMediaLib(stationConfig, null);
		if (medialib != null) {
		    medialib.setLibraryActivated(true); 
		    //we dont know if lib is active but we do this anyways to make medialib send cacheinvalidate
		} else {
		    throw new Exception(ERROR_DB_INSERT_FAILED_STRING);
		}
	    }

	    if (libFilename.equals("")) {
		libFilename = medialib.newBulkItemFilename() + "." + getExtension(filename);
	    } else {
		EncodedFiles encodedFile = EncodedFiles.getDummyObject();
		encodedFile.setNewItemID(libFilename);
		EncodedFiles[] encodedFilesMetadata = medialib.get(encodedFile);
		if (encodedFilesMetadata.length == 1) {
		    encodedFileEntry = encodedFilesMetadata[0];
		    logger.info("DoPost: Found an entry in encoded files with " + libFilename + " as newItemID.");
		} else if (encodedFilesMetadata.length > 1) {
		    logger.error("DoPost: Found " + encodedFilesMetadata.length + " encoded files entries with " + libFilename + " as newItemID.");
		}
	    }

	    
	    try {
		String completeLibFilename = storeDir + File.separator + libFilename;
		File libFile = new File(completeLibFilename);
		String checksum = null;

		/*
		if (libFile.exists()) {
		    logger.info("DoPost: File " + libFilename + " already exists in the file store");

		    checksum = FileUtilities.getSha1Hex(completeLibFilename);
		    if (checksum == null)
			checksum = "";

		    String uploadedChecksum = FileUtilities.getSha1Hex(storeDir + File.separator + filename);
		    if (uploadedChecksum == null) 
			uploadedChecksum = "";
		    
		    if (checksum.equals(uploadedChecksum)) {
			logger.debug("DoPost: Checksums of " + filename + " and " + libFilename + " are the same.");
			new File(storeDir + File.separator + filename).delete();
		    } else {
			logger.error("DoPost: libFilename " + libFilename + " and uploaded file " + filename + " are not the same.");
			new File(storeDir + File.separator + filename).delete();
			throw new Exception(MediaLibUploadProvider.ERROR_FILE_EXISTS);
		    }
		} else {
		*/
		    if (FileUtilities.changeFilename(filename, storeDir, libFilename, storeDir) == -1) {
			logger.error("DoPost: Failed to change file name.");
			throw new Exception(MediaLibUploadProvider.ERROR_FILE_COPY_FAILED);
		    }
		    //}
		
		
		String doDBInsert = httpRequestParams.get(PARAM_DB_INSERT)[0];
		if (doDBInsert.equals("true")) {
		    if (checksum == null) {
			checksum = FileUtilities.getSha1Hex(completeLibFilename);
			if (checksum == null)
			    checksum = "";
		    }


		    RadioProgramMetadata uploadedMetadata = new RadioProgramMetadata(libFilename, 0, "", 
										     LibService.UNKNOWN, 		    
										     getTimekeeperTime(), 
										     LibService.DONE_UPLOAD, 
										     LibService.MEDIA_NOT_INDEXED,
										     checksum,
										     LibService.UNKNOWN, 
										     LibService.UNKNOWN);
		    
		    uploadedMetadata.setTitle(StringUtilities.sanitize(removeExtension(originalName)));

		    clearAllFileStoreFields(uploadedMetadata);
		    setFileStoreField(fileStoreName, uploadedMetadata, FileStoreManager.UPLOAD_DONE);
		    long length = -1L;

		    try{
			Hashtable<String, String> fileMetadata = getMetadata(completeLibFilename);
			medialib.updateFileMetadata(uploadedMetadata, fileMetadata);
		    } catch (Exception e){
			logger.error("DoPost: Unable to get file metadata", e);
		    }

		    //XXX: Change this decision making based on who is the uploader
		    if (fileStoreName.equals(StationNetwork.UPLOAD)) 
			uploadedMetadata.setType(RadioProgramMetadata.UPLOAD_TYPE);

		    uploadedMetadata.truncateFields();
		    if (medialib.insert(uploadedMetadata) >= 0) {
			out.println("status=" + MediaLibUploadProvider.SUCCESS_MSG);
			out.println("libfilename=" + libFilename);

			//Inform lib service only if we are putting the file in upload filestore
			if (fileStoreName.equals(StationNetwork.UPLOAD))
			    sendUploadDone = true;

		    } else {
			throw new Exception(ERROR_DB_INSERT_FAILED_STRING);
		    }
		    
		    String oldProgramID;
		    if ((oldProgramID = medialib.findImportedFile(originalName)) != null){
			medialib.updateImportedFile(libFilename, oldProgramID);
		    }

		    // Dummy file uploaded here. It can be removed from the system.
		    if (originalName.equals(DiagnosticsController.DIAGNOSTICS_PLAYOUT_FILE)){
			logger.info("DoPost: Diagnostics playout file uploaded. Removing from system.");
			if (medialib.remove(uploadedMetadata) < 0)
			    throw new Exception(ERROR_DB_INSERT_FAILED_STRING);
			try{
			    File file = new File(storeDir+"/"+libFilename);
			    file.delete();

			    //do not inform lib service in case of dummy file upload.
			    sendUploadDone = false;
			} catch (Exception e) {
			    logger.error("DoPost: Unable to remove Diagnostics playout file:", e);
			}
		    }
		    

		} else { //doDBInsert = false
		    RadioProgramMetadata whereMetadata = new RadioProgramMetadata(libFilename);
		    RadioProgramMetadata updateMetadata = whereMetadata.clone();

		    setFileStoreField(fileStoreName, updateMetadata, FileStoreManager.UPLOAD_DONE);
		    boolean invalidate = shouldInvalidateCache(fileStoreName, encodedFileEntry);

		    if (medialib.update(whereMetadata, updateMetadata, invalidate) >= 0) {
			out.println("status=" + MediaLibUploadProvider.SUCCESS_MSG);
			out.println("libfilename=" + libFilename);

			//Inform lib service only if we are putting the file in upload filestore
			if (fileStoreName.equals(StationNetwork.UPLOAD))
			    sendUploadDone = true;
		    } else {
			throw new Exception(ERROR_DB_INSERT_FAILED_STRING);
		    }

		}
	
		
	    } catch(Exception e2) {
		throw e2;
	    }

	} catch(Exception e) {
	    logger.error("DoPost: Servlet error", e);

	    if (e.getMessage().startsWith(ERROR_NO_SPACE_STRING))
		out.println(MediaLibUploadProvider.ERROR_NO_SPACE);
	    else if (e.getMessage().startsWith(ERROR_NOT_WRITABLE_STRING))
		out.println(MediaLibUploadProvider.ERROR_NOT_WRITABLE);
	    else if (e.getMessage().startsWith(ERROR_DB_INSERT_FAILED_STRING))
		out.println(MediaLibUploadProvider.ERROR_DB_FAILED);
	    else if (e.getMessage().startsWith(ERROR_MAX_EXCEEDED_STRING))
		out.println(MediaLibUploadProvider.ERROR_MAX_EXCEEDED);
	    else if (e.getMessage().startsWith(MediaLibUploadProvider.ERROR_FILE_COPY_FAILED))
		out.println(MediaLibUploadProvider.ERROR_FILE_COPY_FAILED);
	    else if (e.getMessage().startsWith(MediaLibUploadProvider.ERROR_FILE_EXISTS))
		out.println(MediaLibUploadProvider.ERROR_FILE_EXISTS);

	    out.println("status=" + MediaLibUploadProvider.FAILURE_MSG);
	    return;
	}

	try {
	    int i;
	    for (i = 0; i < outputseq.size(); i++) {
		out.println(outputseq.get(i));
		logger.info("DoPost: "+outputseq.get(i));
	    }
	    out.flush();
	} catch(Exception e) {
	    logger.error("Do Post: Output error", e);
	}

	String uploaderName = httpRequestParams.get(PARAM_UPLOADER_NAME)[0];
	logger.info("Do Post: Uploader Name is " + uploaderName);
	if (sendUploadDone) {
	    sendUploadDoneMessage(libFilename, uploaderName);
	}
    }
 
    void preemptEncoding(){
	PreemptBackgroundActivityMessage preemptMessage = new PreemptBackgroundActivityMessage(RSController.SERVLET, RSController.LIB_SERVICE, "");
	libMessageQueue.add(preemptMessage);
    }

    private boolean shouldInvalidateCache(String fileStoreName, EncodedFiles encodedFileEntry) {
	if (encodedFileEntry != null)
	    return false;

	if (!fileStoreName.equals(StationNetwork.UPLOAD))
	    return true;
	
	String[] stores =((StationNetwork)stationConfig.getStationNetwork()).getSameStores(fileStoreName);
	for (String store : stores) {
	    if (!store.equals(fileStoreName))
		return true;
	}

	return false;
    }

    private void setFileStoreField(String fileStoreName, RadioProgramMetadata metadata, int count) {
	FileStoreUploader.setFileStoreField(fileStoreName, metadata, count);
    }

    private void getFilenames(MultipartRequest multipart, ArrayList<String> outputseq) throws IOException {
	Enumeration e = multipart.getFileNames();
	while (e.hasMoreElements()) {
	    String outputstr = "";
	    String filename = (String)(e.nextElement());
	    outputstr = "filename=" + filename + "; systemname=" + multipart.getFilesystemName(filename);
	    //logger.error("GetFileNames: " + outputstr);
	    outputseq.add(outputstr);
	}
    }

    private void getParams(HttpServletRequest request, ArrayList<String> outputseq) throws IOException {
	Enumeration e = request.getParameterNames();
	while (e.hasMoreElements()) {
	    String outputstr = "";
	    String paramName = (String)(e.nextElement());	    
	    outputstr = "parameter=" + paramName + "; parametervals=";
	    String[] paramVals = request.getParameterValues(paramName);
	    httpRequestParams.put(paramName, paramVals);
	    
	    for (String paramVal: paramVals) {
		outputstr = outputstr + paramVal + ",";
	    }

	    if (outputstr.endsWith(","))
		outputseq.add(outputstr.substring(0, outputstr.length() - 1));
	    else
		outputseq.add(outputstr);
	}
    }

    public Hashtable<String, String> getMetadata(final String fileName){
	final int MAX_WAIT_TIME = 5000;
	final Hashtable<String, String> retVals = new Hashtable<String, String>();
	
	try{
	    logger.debug("GetDuration: "+fileName);
	    final Pipeline pipe = new Pipeline("duration");
	    final Element fileSrc = ElementFactory.make("filesrc", "filesrc");
	    fileSrc.set("location", fileName);
	    final DecodeBin decodeBin = new DecodeBin("decodeBin");
	    final Element audioConvert = ElementFactory.make("audioconvert","audioconvert");
	    final Element fakeSink = ElementFactory.make("fakesink", "fakesink");
	    final long[] duration = new long []{-1L};
	    final boolean[] state = new boolean[]{false};

	    decodeBin.connect(new DecodeBin.NEW_DECODED_PAD(){
		    public void newDecodedPad(Element e, Pad pad, boolean last){
			decodeBin.link(audioConvert);
		    }
		});

	    pipe.addMany(fileSrc, decodeBin, audioConvert, fakeSink);
	    pipe.linkMany(fileSrc, decodeBin);
	    pipe.linkMany(audioConvert, fakeSink);

	    Bus bus = pipe.getBus();
	    bus.connect(new Bus.STATE_CHANGED(){
		    public void stateChanged(GstObject source, State old, State cur, State pending){
			if (source == decodeBin && cur == State.PLAYING && 
			   (pending == State.PLAYING || pending == State.VOID_PENDING)){

			    duration[0] = pipe.queryDuration().toMillis();
			    synchronized(decodeBin){
				state[0] = true;
				decodeBin.notifyAll();
			    }
			    
			}
			synchronized(decodeBin){
			    if (source == fileSrc && cur == State.NULL && pending == State.VOID_PENDING){
				decodeBin.notifyAll();
			    }
			}
		    }
		});
	    
	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			logger.error("GetDuration: "+source+" error: "+message+" for file: "+fileName);
			synchronized(decodeBin){
			    duration[0] = -1L;
			    state[0] = true;
			    decodeBin.notifyAll();
			}
		    }
		});

	    bus.connect(new Bus.TAG(){
		    public void tagsFound(GstObject source, TagList tagList){
			for (String name: tagList.getTagNames()){
			    retVals.put(name, concatList(tagList.getValues(name)));
			}
		    }
		});
	    
	    pipe.setState(State.PLAYING);

	    synchronized(decodeBin){
		while (!state[0]){
		    try{
			decodeBin.wait(MAX_WAIT_TIME);
			state[0] = true;
		    } catch (Exception e){
			logger.error("GetDuration: Thread interrupted.", e);
		    }
		}
	    }

	    logger.info("GetDuration: found duration: "+duration[0]);
	    
	    pipe.setState(State.NULL);

	    synchronized(decodeBin){
		while (pipe.getState() != State.NULL){
		    try{
			decodeBin.wait(MAX_WAIT_TIME);
		    } catch (Exception e){
			logger.error("GetDuration: Thread interrupted.", e);
		    }
		}
	    }
	    
	    //RSPipeline.DURATION
	    retVals.put("DURATION", Long.toString(duration[0]));

	    
	} catch (Exception e){
	    logger.error("GetDuration: Encountered exception: ", e);

	    retVals.put("DURATION", Long.toString(-1L));
	}
	
	return retVals;
    }

    boolean isDownloadRequest(HttpServletRequest request){
	if (request.getParameter(PARAM_DOWNLOAD_FILENAME) == null)
	    return false;
	else
	    return true;
    }

    void handleDownloadRequest(HttpServletRequest request, HttpServletResponse response){
	String fileName = request.getParameter(PARAM_DOWNLOAD_FILENAME);
	if (fileName == null){
	    logger.error("HandleDownloadRequest: Null filename.");
	    return;
	}
	String fileStoreName = request.getParameter(PARAM_FILESTORE_NAME);
	String dirName = stationNetwork.getStoreDir(fileStoreName);

	logger.info("HandleDownloadRequest: Serving file: "+dirName + File.separator + fileName);

	response.setContentType("application/octet-stream");

	InputStream in = null;
	try{
	    OutputStream out = response.getOutputStream();
	    in = new BufferedInputStream(new FileInputStream(dirName + File.separator + fileName));
	    byte buf[] = new byte[4 * 1024];
	    int count;
	    while ((count = in.read(buf)) != -1){
		out.write(buf, 0, count);
	    }
	    in.close();
	} catch (Exception e){
	    logger.error("HandleDownloadRequest: Encountered exception: ", e);
	}
	
    }

    protected static String concatList(List<Object> list){
	StringBuilder str = new StringBuilder();

	for (Object o: list)
	    str.append(o.toString() + ", ");
	if (str.length() >= 2)
	    str.delete(str.length()-2, str.length());

	return str.toString();
    }
    
    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	return 0;
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.info("HandleIncomingMessage: Message received from:" + message.getSource() + " type:" + message.getMessageClass());
	if (message instanceof UploadDoneAckMessage) {
	    UploadDoneAckMessage ackMessage = (UploadDoneAckMessage)message;
	    handleUploadDoneAckMessage(ackMessage);
	} else if (message instanceof StartCleanupMessage) {
	    StartCleanupMessage cleanupMessage = (StartCleanupMessage)message;
	    handleCleanup(cleanupMessage.getSource(), cleanupMessage.getCommands());
	} /*else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage ackMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(ackMessage);
	    }*/
	return 0;
    }

    /*protected synchronized void handleRegistrationAckMessage(RegistrationAckMessage ackMessage) {
	
	if (ackMessage.getAck().equals("true")) {
	    servletRegistered = true;
	    regSyncObject.notifyAll();
	    doCleanup();
	} else {
	    logger.info("HandleRegistrationAckMessage: Nack received registering again.");
	    registrationID = GUIDUtils.getGUID();
	    if (ipcServer.registerIPCNode(getName(), (IPCNode)this, registrationID) == -1) {
		logger.error("HandleRegistrationAckMessage: Unable to connect to IPCServer.");
		connectionError();
	    }
	}
	}*/

    protected synchronized void handleUploadDoneAckMessage(UploadDoneAckMessage ackMessage) {
	String ackedFileName = ackMessage.getFileName();
	String currentMessageFileName = currentUploadDoneMessage.getFileName();

	if (ackedFileName.equals(currentMessageFileName)) {
	    logger.info("HandleUploadDoneAckMessage: Ack received for file:" + ackedFileName);
	    ackReceived = true;
	    libCommunicationThread.interrupt();
	} else {
	    logger.warn("HandleUploadDoneAckMessage: Spurious ack received. AckFileName=" + ackedFileName + " CurrentFileName=" + currentMessageFileName);
	}
    }

    public String getName() {
	return servletName;
    }

    public void sendUploadDoneMessage(String fileName, String uploaderName) {
	UploadDoneMessage uploadDoneMessage = new UploadDoneMessage(getName(),
								    RSController.LIB_SERVICE,
								    fileName,
								    uploaderName);
	libMessageQueue.add(uploadDoneMessage);
    }


    public void run() {
	int retries;
	IPCMessage message;

	if (ipcServer.registerIPCNode(getName(), (IPCNode)this, registrationID) == -1) {
	    logger.error("Run: Unable to connect to IPCServer.");
	    connectionError();
	} 
	
	/*while (!servletRegistered) {
	    synchronized(regSyncObject) {
		if (!servletRegistered) {
		    try {
			regSyncObject.wait();
		    } catch(Exception e) {}
		}
	    }
	    }*/
	
	while (true) {
	    message = null;
	    while (message == null) {
		try {
		    message = libMessageQueue.take();
		} catch(InterruptedException e) {
		    logger.warn("Run: Interrupted while taking message from the queue.");
		}
	    }
	    
	    if (message instanceof UploadDoneMessage) 
		currentUploadDoneMessage = (UploadDoneMessage)message;

	    retries = 0;
	   
	    while (retries < MAX_RETRIES) {

		while (ipcServer == null) {
		    try {
			Thread.sleep(IPCNULL_RETRY_INTERVAL);
		    } catch(InterruptedException e) {}
		}

		if (ipcServer.handleOutgoingMessage(message) == -1) {
		    logger.error("Run: Unable to send upload done message.");
		    connectionError();
		} else {
		    
		    if (!(message instanceof UploadDoneMessage))
			break;
		    
		    try {
			Thread.sleep(MESSAGE_ACK_WAIT_TIME);
		    } catch(InterruptedException e) {
			if (ackReceived) {
			    logger.debug("Run: Ack message received for file: " + currentUploadDoneMessage.getFileName());
			    ackReceived = false;
			    break;
			} else {
			    //XXX: Should we be checking for unexpected interruption and sleeping again?
			    logger.warn("Run: Thread sleep interrupted.");
			}
		    }
		    logger.warn("Run: Ack message not received for file: " + currentUploadDoneMessage.getFileName());
		    retries++;
		}
	    } //end while

	    if (retries == MAX_RETRIES) {
		logger.error("Run: Failed to get ack message for file: " + currentUploadDoneMessage.getFileName() + ". Giving up.");
		retries = 0;
	    }
	}
    }

    public synchronized void connectionError() {
	    servletRegistered = false;
	if (ipcServer != null)
	    ipcServer.disconnect(getName());
	
	long interval = stationConfig.getIntParam(StationConfiguration.NETWORK_DOWN_RECONNECTION_INTERVAL) * 1000;
	while (true) {
	    if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
		MediaLib.setIPCServer(ipcServer);
		return;
	    }

	    try {
		Thread.sleep(interval);
	    } catch(InterruptedException e){}
	}
    }

    protected void clearAllFileStoreFields(RadioProgramMetadata metadata) {
	FileStoreUploader.clearAllFileStoreFields(metadata);
    }

    protected void handleCleanup(String source, String[] commands) {
	logger.info("HandleCleanup: From: " + source + " commands: " + StringUtilities.getCSVFromArray(commands));
	for (String command : commands) {
	    if (command.equals(StartCleanupMessage.ENCODED_FILES))
		doCleanup();
	}
    }

    protected void doCleanup() {
	String localStoreName = stationNetwork.getALocalStoreName();

	//Upload servlet need not do anything. Lib service will take care of everything
	if (stationNetwork.isSameFileStore(localStoreName, StationNetwork.UPLOAD))
	    return;

	RadioProgramMetadata whereMetadata = RadioProgramMetadata.getDummyObject("");
	//setFileStoreField(localStoreName, whereMetadata, FileStoreManager.UPLOAD_DONE);
	//setFileStoreField(StationNetwork.UPLOAD, whereMetadata, FileStoreManager.FILE_DELETED);
	
	if (medialib == null) {
	    medialib = MediaLib.getMediaLib(stationConfig, null);
	    if (medialib != null) {
		medialib.setLibraryActivated(true); 
		//we dont know if lib is active but we do this anyways to make medialib send cacheinvalidate
	    } else {
		logger.error("DoCleanup: Unable to connect to the database. Cleanup not done.");
		return;
	    }
	}

	String whereClause = buildWhereClause(localStoreName);
	Metadata[] metadata = (Metadata[])medialib.selectAllFields(whereMetadata, whereClause, -1);
	//Metadata[] metadata =  medialib.get(whereMetadata);
	RadioProgramMetadata[] programMetadata = new RadioProgramMetadata[metadata.length];
	
	for (int i = 0; i < metadata.length; i++) {
	    programMetadata[i] = (RadioProgramMetadata)metadata[i];
	}

	for (RadioProgramMetadata progMetadata : programMetadata) {

	    deleteFile(progMetadata.getItemID());
	
	}
    }

    protected String buildWhereClause(String localStoreName) {
	String whereClause = FileStoreManager.getFileStoreDBFieldName(localStoreName) + "=" + FileStoreManager.UPLOAD_DONE;
	whereClause += " and ";
	for (int i = 0; i < StationNetwork.stores.length; i++) {
	    String store = StationNetwork.stores[i];
	    if (store.equals(localStoreName))
		continue;
	    String destFieldName = FileStoreManager.getFileStoreDBFieldName(store);
	    whereClause += " " + destFieldName + "=" + FileStoreManager.FILE_DELETED + " ";
	    if (i+1 < StationNetwork.stores.length)
		whereClause += " or ";
	}
	
	logger.debug("BuildeWhereClause: WhereClause= " + whereClause);
	return whereClause;
    }

    protected void deleteFile(String fileName) {
	logger.info("DeleteFile: Removing " + fileName + " from the system.");
	String localStoreName = stationNetwork.getALocalStoreName();
	String dirName = stationNetwork.getLocalStoreDir();
	RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(fileName);

	PlayoutHistory playout = PlayoutHistory.getDummyObject(fileName);
	if (medialib.remove(playout, false) < 0) {
	    logger.error("DeleteFile: Could not remove playout history for:" + fileName);
	}

	Creator creator = Creator.getDummyObject(fileName);
	if (medialib.remove(creator, false) < 0) {
	    logger.error("DeleteFile: Could not remove creators for:" + fileName);
	}

	ItemCategory category = ItemCategory.getDummyObject(fileName);
	if (medialib.remove(category, false) < 0) {
	    logger.error("DeleteFile: Could not remove item categories for:" + fileName);
	}

	Tags tag = Tags.getDummyObject(fileName);
	if (medialib.remove(tag, false) < 0) {
	    logger.error("DeleteFile: Could not remove tags for:" + fileName);
	}
	
	//Do updation of metadata
	updateFileDeletedInDB(localStoreName, fileName);
	
	//Delete the old file
	deleteFileFromDisk(dirName + File.separator + fileName);
    }

    /*
    protected boolean replaceFile(String fileName, String newFileName, String storeName) {
	String dirName = stationConfig.getStationNetwork().getStoreDir(storeName);


	//Update the database to point to the new metadata entry
	RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(fileName);
	RadioProgramMetadata newMetadata = RadioProgramMetadata.getDummyObject(newFileName);
	
	PlayoutHistory[] history = metadata.populate__history(medialib);
	Creator[] creators = metadata.populate__creators(medialib);
	ItemCategory[] categories = metadata.populate__categories(medialib);
	Tags[] tags = metadata.populate__tags(medialib);
	
	for (PlayoutHistory hist: history){
	    PlayoutHistory newHistory = hist.clone();
	    newHistory.setItemID(newFileName);
	    if (medialib.update(hist, newHistory)<0)
		logger.error("ReplaceFile: Database update failed.");
	    else
		logger.debug("ReplaceFile: Database updated: "+hist.dump()+" with "+newHistory.dump());
	}
	
	for (Creator creator: creators){
	    Creator newCreator = creator.clone();
	    newCreator.setItemID(newFileName);
	    if (medialib.update(creator, newCreator)<0)
		logger.error("ReplaceFile: Database update failed.");
	    else 
		logger.debug("ReplaceFile: Database updated: "+creator.dump()+" with "+newCreator.dump());
	}
	
	for (ItemCategory category: categories){
	    ItemCategory newCategory = category.clone();
	    newCategory.setItemID(newFileName);
	    if (medialib.update(category, newCategory)<0)
		logger.error("ReplaceFile: Database update failed.");
	    else 
		logger.debug("ReplaceFile: Database updated: "+category.dump()+" with "+newCategory.dump());
	    
	}
	
	for (Tags tag: tags){
	    Tags newTag = tag.clone();
	    newTag.setItemID(newFileName);
	    if (medialib.update(tag, newTag)<0)
		logger.error("ReplaceFile: Database update failed.");
	    else 
		logger.debug("ReplaceFile: Database updated: "+tag.dump()+" with "+newTag.dump());
	    
	}
		
	PlaylistItem queryPlaylistItem = new PlaylistItem(-1);
	queryPlaylistItem.setItemID(fileName);
	Metadata playlistItems[] = medialib.get(queryPlaylistItem);
	for (Metadata itemMetadata: playlistItems){
	    PlaylistItem playlistItem = (PlaylistItem) itemMetadata;
	    PlaylistItem newPlaylistItem = playlistItem.clone();
	    newPlaylistItem.setItemID(newFileName);
	    if (medialib.update(playlistItem, newPlaylistItem) < 0)
		logger.error("ReplaceFile: Database update failed.");
	    else
		logger.debug("ReplaceFile: Database updated: "+playlistItem.dump()+" with "+newPlaylistItem.dump());
	}
	
	Hotkey queryHotkey = new Hotkey(fileName);
	Metadata hotkeys[] = medialib.get(queryHotkey);
	for (Metadata hotkeyMetadata: hotkeys){
	    Hotkey hotkey = (Hotkey) hotkeyMetadata;
	    Hotkey newHotkey = hotkey.clone();
	    newHotkey.setItemID(newFileName);
	    if (medialib.update(hotkey, newHotkey) < 0)
		logger.error("ReplaceFile: Database update failed.");
	    else
		logger.debug("ReplaceFile: Database updated: "+hotkey.dump()+" with "+newHotkey.dump());
	}
	

	//XXX: Do all the updates above in a transaction. Rollback the transaction on failure.
	//Do updation of old metadata and deletion of old only on success of the transaction
	updateFileDeletedInDB(storeName, fileName);

	//Delete the old file
	deleteFileFromDisk(dirName + File.separator + fileName);
	
	//Remove entry from encoded files
	removeEncodedFileEntry(fileName, newFileName);

	return true;
	//should return false when update transation fails. make the change when update is done in a transaction
    }
    */

    protected void updateFileDeletedInDB(String storeName, String fileName) {
	RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(fileName);
	RadioProgramMetadata updateMetadata = RadioProgramMetadata.getDummyObject(fileName);
	setFileStoreField(storeName, updateMetadata, FileStoreManager.FILE_DELETED);
	
	if (medialib.update(metadata, updateMetadata, false) >= 0) {
	    logger.debug("UpdateFileDeletedInDB: Database update successful");
	    Metadata[] programMetadatas = medialib.get(metadata);
	    if (programMetadatas.length == 0) {
		logger.error("UpdateFileDeletedInDB: Could not get program metadata for: " + fileName);
	    } else {
		metadata = (RadioProgramMetadata) programMetadatas[0];
		if ( (metadata.getPlayoutStoreAttempts() == FileStoreManager.FILE_DELETED) &&
		    (metadata.getPreviewStoreAttempts() == FileStoreManager.FILE_DELETED) &&
		    (metadata.getUploadStoreAttempts() == FileStoreManager.FILE_DELETED)) {
		
		    medialib.remove(metadata, false);
		}
	    }
	} else {
	    logger.error("UpdateFileDeletedInDB: Database update failed");
	}
    }

    protected void removeEncodedFileEntry(String fileName, String newFileName) {
	EncodedFiles encodedFile = EncodedFiles.getDummyObject();
	encodedFile.setNewItemID(newFileName);
	encodedFile.setOldItemID(fileName);
	Metadata[] encodedFilesMetadata = medialib.get(encodedFile);
	if (encodedFilesMetadata.length == 0) {
	    logger.warn("RemoveEncodedFileEntry: Could not find entry: " + fileName + "," + newFileName + " in EncodedFiles.");
	} else {
	    EncodedFiles encodedFileEntry = (EncodedFiles)encodedFilesMetadata[0];
	    if (medialib.remove(encodedFileEntry, false) < 0) {
		logger.error("RemoveEncodedFileEntry: Could not remove encoded file entry: " + fileName + "," + newFileName);
	    }
	}
    }

    protected void deleteFileFromDisk(String fileName) {
	try {
	    File oldFile = new File(fileName);
	    if (!oldFile.exists()) {
		logger.error("DeleteFile: File "+fileName+" does not exist.");
		return;
	    }
	    if (oldFile.delete()) {
		logger.info("DeleteFile: Deleted the old file: " + fileName);
	    } else {
		logger.error("DeleteFile: Unable to delete the old file: " + oldFile.getName());
	    }
	} catch(Exception e) {
	    logger.error("DeleteFile: Got exception in trying to delete file: " + fileName);
	}
    }
}