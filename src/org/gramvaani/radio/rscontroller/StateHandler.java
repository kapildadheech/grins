package org.gramvaani.radio.rscontroller;

import org.gramvaani.utilities.LogUtilities;

import java.util.*;
import java.lang.reflect.Method;

public class StateHandler<T extends Enum<T>, S extends Enum<S>, U extends Enum<U>> {
    Class<T> stateClass;
    Class<S> eventClass;
    Class<U> signalClass;

    Object obj;

    String stateEnumName;
    String eventEnumName;

    LogUtilities logger;
    T[] states;
    S[] events;
    T currentState;

    Hashtable<T, Hashtable<S, Method>> methodHash = new Hashtable<T, Hashtable<S, Method>>();
    Hashtable<T, Hashtable<S, Boolean>> allStatesFlag = new Hashtable<T, Hashtable<S, Boolean>>();
    Hashtable<T, Hashtable<S, Boolean>> allEventsFlag = new Hashtable<T, Hashtable<S, Boolean>>();
    Hashtable<U, Method> signalHash = new Hashtable<U, Method>();
    Hashtable<Method, Boolean> varargsFlag = new Hashtable<Method, Boolean>();

    public StateHandler(Object obj, Class<T> stateClass, Class<S> eventClass, Class<U> signalClass, T startState){
	this.stateClass = stateClass;
	this.eventClass = eventClass;
	this.signalClass= signalClass;
	this.obj 	= obj;

	stateEnumName = stateClass.getSimpleName();
	eventEnumName = eventClass.getSimpleName();

	logger = new LogUtilities("StateHandler:"+obj.getClass().getSimpleName()+":"+stateEnumName);
	
	states = stateClass.getEnumConstants();
	events = eventClass.getEnumConstants();

	currentState = startState;

	for (T state: states){
	    methodHash.put(state, new Hashtable<S, Method>());
	    allStatesFlag.put(state, new Hashtable<S, Boolean>());
	    allEventsFlag.put(state, new Hashtable<S, Boolean>());
	}

	if (states.length == 0 || events.length == 0){
	    logger.error("Init: Empty enum: "+stateEnumName+": "+states.length+" :"+eventEnumName+": "+events.length);
	    return;
	}

	for (Method m: obj.getClass().getDeclaredMethods()){
	    
	    m.setAccessible(true);

	    StateEventHandler handler;
	    try {
		if ((handler = m.getAnnotation(StateEventHandler.class)) != null){
		    varargsFlag.put(m, handler.varargs());
		    String[] stateEventList = handler.stateEventList();

		    if (stateEventList.length == 0) {
			addHandler(handler.states(), handler.events(), m);
		    } else {
			if (handler.states().length != 0 || handler.events().length != 0)
			    logger.warn("Error: Ignoring annotations not present in stateEventList");

			for (String stateEvent: stateEventList){
			    String[][] stateEvents = getStateEventsFromList(stateEvent, m.getName());
			    addHandler(stateEvents[0], stateEvents[1], m);
			}
		    }
		}

	    } catch (Exception e){
		logger.error("Init: Unable to handle eventhandler annotation for method: "+m, e);
	    }

	    EmitSignal signal;
	    try {
		if ((signal = m.getAnnotation(EmitSignal.class)) != null){
		    ArrayList<U> signalEnums = getSelectedEnums(new String[]{signal.value()}, signalClass, m.getName());
		    if (signalEnums.size() == 0){
			logger.error("Init: Unable to lookup correct handler for signal string: "+signal.value()
				     +" in "+signalClass.getSimpleName());
			continue;
		    }
		    U signalEnum = signalEnums.get(0);
		    signalHash.put(signalEnum, m);
		    logger.debug("Init: Adding signal: "+signalEnum+" method: "+m);
		}
	    } catch (Exception e){
		logger.error("Init: Unable to handle signal annotation for method: "+m, e);
	    }
	}

	//Use for debugging
	//checkCompleteness();
    }

    String[][] getStateEventsFromList(String list, String methodName){
	String str = list.replaceAll(" ", "");
	String[] tokens = str.split(":");
	String[][] stateEvents = new String[][]{tokens[0].split(","), tokens[1].split(",")};
	return stateEvents;
    }

    void addHandler(String[] statesList, String[] eventsList, Method m){
	ArrayList<T> selectedStates = null;
	ArrayList<S> selectedEvents = null;
	selectedStates = getSelectedEnums(statesList, stateClass, m.getName());
	selectedEvents = getSelectedEnums(eventsList, eventClass, m.getName());
	boolean allStates = false, allEvents = false;
	if (selectedStates.size() == states.length)
	    allStates = true;
	if (selectedEvents.size() == events.length)
	    allEvents = true;
	
	for (T state: selectedStates)
	    for (S event: selectedEvents)
		addHandler(state, event, m, allStates, allEvents);
    }

    void addHandler(T state, S event, Method method, boolean allStates, boolean allEvents){
	Method tmp = methodHash.get(state).get(event);

	if (tmp != null){
	    if (allStates && allEvents){
		return;
	    }

	    if (allStates && !allEvents){
		if (!allStatesFlag.get(state).get(event))
		    return;
	    }

	    if (!allStates && allEvents){
		if (!allStatesFlag.get(state).get(event) && !allEventsFlag.get(state).get(event))
		    return;
	    }

	    logger.debug("AddHandler: replacing: "+tmp + " with: "+method);
	}
	
	methodHash.get(state).put(event, method);
	allStatesFlag.get(state).put(event, allStates);
	allEventsFlag.get(state).put(event, allEvents);
	logger.debug("AddHandler: state: "+state+" event: "+event+" method: "+method.getName());
    }

    public void checkCompleteness(){
	boolean missing = false;

	for (T state: states)
	    for (S event: events)
		if (methodHash.get(state).get(event) == null){
		    logger.warn("CheckCompleteness: No handler for state: "+state+" event: "+event);
		    missing = true;
		} else {
		    logger.info("CheckCompleteness:"+stateEnumName+"."+state+":\t\t"+eventEnumName+"."+event+"\t\t:"+methodHash.get(state).get(event).getName());
		}

	if (!missing)
	    logger.info("CheckCompleteness: All handlers present"); 
    }

    public synchronized void handleEvent(S event, Object... args){
	Method m = methodHash.get(currentState).get(event);

	if (m == null){
	    logger.error("HandleEvent: No method registered for state: "+currentState+" for event: "+event);
	    return;
	}

	try {
	    Object retVal;

	    if (varargsFlag.get(m)){
		retVal = m.invoke(obj, new Object[]{args});
	    } else {
		retVal = m.invoke(obj, args);
	    }

	    if (retVal instanceof StateHandler.Transition){
		Transition transition = (Transition) retVal;
		T state;
		if ((state = transition.getState()) != null)
		    setState(state);
		U signal;
		if ((signal = transition.getSignal()) != null)
		    emitSignal(signal, transition.getSignalArgs());
	    }

	} catch (IllegalArgumentException iae) {
	    StringBuilder str = new StringBuilder();
	    for (Object o: args)
		str.append(o.getClass().getName() + ",");
	    logger.error("HandleEvent: For method: "+m +" supplied arguments: "+str.toString());
	} catch (Exception e){
	    logger.error("HandleEvent: Encountered exception at state: "+currentState+" event: "+event+" for function: "+m, e);
	}
    }

    public void emitSignal(U signal, Object... args){
	Method m = signalHash.get(signal);

	if (m == null){
	    logger.error("EmitSignal: No handler for signal: "+signal);
	    return;
	}

	try {
	    m.invoke(obj, args);
	} catch (Exception e){
	    logger.error("EmitSignal: Encountered exception:", e);
	}
    }
    
    public StateHandler.Transition transition(U signal, Object... args){
	return new Transition(null, signal, args);
    }

    public StateHandler.Transition transition(T state, U signal, Object... args){
	return new Transition(state, signal, args);
    }

    public StateHandler.Transition transition(T state){
	return new Transition(state, null, null);
    }
    
    void setState(T state){
	logger.debug("SetState: Curr: "+currentState + " next: "+state);
	currentState = state;
    }
    
    public synchronized T getState(){
	    return currentState;
    }

    @SuppressWarnings("unchecked")
    <U extends Enum<U>> ArrayList<U> getSelectedEnums(String[] names, Class<U> enumClass, String methodName){
	String enumName = enumClass.getSimpleName();
	ArrayList list = new ArrayList();
	U[] enums = enumClass.getEnumConstants();

	if (enums.length == 0){
	    logger.error("GetEnums: For method: "+methodName+": empty enum: "+enumName);
	    return (ArrayList<U>) list;
	}

	if (names.length == 0){
	    list.addAll(Arrays.asList(enums));
	    return (ArrayList<U>) list;
	}

	for (String name: names){
	    try{
		String[] nameTokens = name.split("[.]");
		if (nameTokens[0].equals(enumName)){
		    list.add(enums[0].valueOf(enumClass, nameTokens[1]));
		} else {
		    logger.debug("GetEnums: Skipping: "+name +" for "+methodName);
		}
	    } catch (Exception e){
		logger.error("GetEnums: Skipping "+name + " for "+methodName, e);
	    }
	}
	return (ArrayList<U>)list;
    }

    public class Transition {
	T state;
	U signal;
	Object[] signalArgs;

	public Transition(T state, U signal, Object[] signalArgs){
	    this.state = state;
	    this.signal = signal;
	    this.signalArgs = signalArgs;
	    if (signalArgs == null)
		this.signalArgs = new Object[]{};
	}

	T getState(){
	    return state;
	}

	U getSignal(){
	    return signal;
	}

	Object[] getSignalArgs(){
	    return signalArgs;
	}
    }
}