package org.gramvaani.radio.rscontroller.pipeline;

public interface PipelineListener {
    
    public void playDone(RSBin bin);
    public void gstError(RSBin bin, String message);

    public void playDone(RSBin bin, String key);
    public void gstError(RSBin bin, String key, String message);
}