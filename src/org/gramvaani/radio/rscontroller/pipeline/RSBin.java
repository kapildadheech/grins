package org.gramvaani.radio.rscontroller.pipeline;

public enum RSBin {
    
    ARCHIVER("archiver"),
	LAZY_ENCODER("lazyEncoder"),
	MIC_PLAYOUT("micPlayout"),
	MIC_PREVIEW("micPreview"),
	MIC_DIAGNOSTICS("micDiagnostics"),
	MONITOR("monitor"),
	MONITOR_DIAGNOSTICS("monitorDiagnostics"),
	STREAMING("streaming"),
	PLAYOUT("playout"),
	PREVIEW("preview"),
	ONLINE_TELEPHONY("onlineTelephony");

    private final String name;
    private boolean isInit = false;
    private double volume = 1.0;

    RSBin(String name){
	this.name = name;
    }

    public String getName(){
	return name;
    }

    public boolean isInit(){
	return isInit;
    }

    public void setInit(){
	isInit = true;
    }

    public void setVolume(double volume){
	this.volume = volume;
    }

    public double getVolume(){
	return volume;
    }
    
}