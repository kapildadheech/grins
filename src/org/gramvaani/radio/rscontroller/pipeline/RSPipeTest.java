package org.gramvaani.radio.rscontroller.pipeline;

import org.gramvaani.radio.stationconfig.StationConfiguration;

import java.util.Hashtable;

public class RSPipeTest implements PipelineListener{

    public RSPipeTest(){
	
	StationConfiguration stationConfig = new StationConfiguration();

	stationConfig.readConfig("automation.conf");

	RSPipeline.init(stationConfig);
	
	//RSPipeline.setInputFile(RSBin.PLAYOUT, "test.mp3");
	//RSPipeline.play(RSBin.PLAYOUT);
	//RSPipeline.setOutputFile(RSBin.ARCHIVER, "archivefile.wav");
	//RSPipeline.play(RSBin.MIC);
	//RSPipeline.play(RSBin.MONITOR);

	
	//RSPipeline.setInputFile(RSBin.PLAYOUT, "100k.mp3", "key1");
	//RSPipeline.play(RSBin.PREVIEW, "test.wav", "key1", 0 , 0.0, this);
	//RSPipeline.setInputFile(RSBin.PLAYOUT, "100k.mp3", "key1");
	
	//RSPipeline.play(RSBin.PLAYOUT, "100k.mp3", "key1", -1, this);

	//long duration = RSPipeline.getDuration("silence.wav");
	//long secs = duration / 1000;
	//long mins = secs/60;

	//System.out.println("duration: "+mins+":"+secs%60);

	//for(int i = 0; i < 20; i++)
	//  RSPipeline.getDuration("test2.mp3");

	//RSPipeline.getPeakFrequencies("MONITOR_DIAG_FILE");
	//RSPipeline.getLevels("100k.mp3");
	//RSPipeline.getLevels("silence.wav");
	/*
	Hashtable<String, String> tags = RSPipeline.getTags("test.mp3");

	for(String str: tags.keySet())
	    System.err.println("tags: "+str + ":" +tags.get(str));
	*/

	try{

	    //Thread.sleep(1000);
	    //RSPipeline.setInputFile(RSBin.PLAYOUT, "test2.mp3", "key2");
	    //RSPipeline.play(RSBin.PLAYOUT, "test2.mp3", "key2", this);

	    //Thread.sleep(10000);
	    //RSPipeline.pause(RSBin.PLAYOUT, "key2");
	    //Thread.sleep(5000);
	    //RSPipeline.play(RSBin.PLAYOUT, "test2.mp3", "key2", this);
	    //Thread.sleep(5000);
	    //RSPipeline.stop(RSBin.PLAYOUT, "key2");
	    //RSPipeline.play(RSBin.PLAYOUT, "test2.mp3", "key2", this);

	    //RSPipeline.play(RSBin.PLAYOUT, "test2.mp3", "key2");

	    //RSPipeline.play(RSBin.ARCHIVER);
	    /*
	    RSPipeline.setInputFile(RSBin.PREVIEW, "test2.mp3");
	    RSPipeline.play(RSBin.PREVIEW);
	    Thread.sleep(10000);
	    RSPipeline.pause(RSBin.PREVIEW);
	    Thread.sleep(5000);
	    RSPipeline.play(RSBin.PLAYOUT);
	    Thread.sleep(5000);
	    RSPipeline.play(RSBin.PREVIEW);
	    */

	    //System.err.println("supp: "+RSPipeline.isSampleRateSupported(RSPipeline.getCapsFromSource("ALSA", "hw:0"), 48001));

	}catch (Exception e){}
	//RSPipeline.stop(RSBin.PLAYOUT);
    }

    public void playDone(RSBin bin){
	System.exit(0);
    }

    public void gstError(RSBin bin, String message){
	System.exit(0);
    }

    public void playDone(RSBin bin, String key){
	System.exit(0);
    }

    public void gstError(RSBin bin, String key, String message){
	System.exit(0);
    }

    public static void main(String args[]){
	RSPipeTest p = new RSPipeTest();
	System.exit(0);
    }
}