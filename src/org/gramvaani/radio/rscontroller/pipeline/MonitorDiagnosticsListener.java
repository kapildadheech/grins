package org.gramvaani.radio.rscontroller.pipeline;

public interface MonitorDiagnosticsListener{
    public void handleFrequencyEvent(long time, int peakFrequency);
}