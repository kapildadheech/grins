package org.gramvaani.radio.rscontroller.pipeline;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.rscontroller.ResourceManager;
import org.gramvaani.radio.rscontroller.services.AudioService;
import org.gramvaani.radio.rscontroller.services.ArchiverService;
import org.gramvaani.radio.rscontroller.services.MonitorService;
import org.gramvaani.radio.rscontroller.services.MicService;
import org.gramvaani.radio.rscontroller.services.StreamingService;
import org.gramvaani.radio.rscontroller.services.OnlineTelephonyService;
import org.gramvaani.radio.app.gui.PlaylistWidget;

import org.gramvaani.utilities.*;

import org.gstreamer.*;
import org.gstreamer.event.*;
import org.gstreamer.elements.*;
import org.gstreamer.message.*;
import org.gstreamer.query.DurationQuery;
import org.gstreamer.query.PositionQuery;

import java.util.*;
import java.util.regex.*;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class RSPipeline {
    
    static StationConfiguration stationConfig = null;
    static final LogUtilities logger;
    static Pipeline pipe;
    static final Bus bus;

    static final Hashtable<RSBin, Bin> bins;
    static final Hashtable<GstObject, State> states;
    static final Hashtable<RSBin, PipelineListener> listeners;
    static final Hashtable<Bin, RSBin> rsBins;
    
    static final Hashtable<String, Bin> playoutSrcBins;
    static final Hashtable<String, PipelineListener> audioListeners;
    static final Hashtable<String, Bin> playBins;
    static final Hashtable<Bin, String> playBinKeys;
    static final Hashtable<Bin, PlayBinInfo> playBinInfos;
    static PlayBinPool playBinPool;
    static Hashtable<String, Hashtable<String, ResourceConfig>> resourceConfigs;

    static Bin playoutBin, previewBin, archiverBin, monitorBin, monitorDiagnosticsBin, micBin, lazyEncoderBin, onlineTelephonyBin;

    static final Bus.STATE_CHANGED stateHandler;

    static String resampler;
    //static String CAPS_STRING;
    static int numFileSrc = 0;
    static final int NUM_FADEOUT_STEPS = 10;

    //static final int NUM_FREQ_BANDS = 
    static final int FREQ_ANALYSIS_SAMPLE_RATE = 8000;
    static final int NUM_SPECTRUM_BANDS = FREQ_ANALYSIS_SAMPLE_RATE / 2;
    static final long SPECTRUM_INTERVAL_MILLIS = 1000; 
    static final long LEVEL_INTERVAL_MILLIS    = 1000;

    static final String ADDER  = "Adder";
    static final String AUDIOCONVERT = "AudioConvert";
    static final String BIN    = "Bin";
    static final String CAPS   = "Caps";
    static final String DECODE = "Decode";
    static final String DEINTERLEAVE= "Deinterleave";
    static final String FAKESINK= "FakeSink";
    static final String FILESINK= "FileSink";
    static final String LAME   = "Lame";
    static final String LEVEL  = "Level";
    static final String OGG    = "Ogg";
    static final String PIPELINE= "Pipeline";
    static final String QUEUE  = "Queue";
    static final String RESAMPLE = "Resample";
    static final String SHOUT  = "Shoutcast";
    static final String SINK   = "Sink";
    static final String SOURCE = "Source";
    static final String SPECTRUM= "Spectrum";
    static final String SPEEX  = "Speex";
    static final String TEE    = "Tee";
    static final String VOLUME = "Volume";
    static final String VORBIS = "Vorbis";

    public static final String MONITOR_DIAG_FILE = "MONITOR_DIAG_FILE";
    public static final String MIC_DIAG_FILE = "MIC_DIAG_FILE";
    public static final String DURATION = "DURATION";
    public static final String ERRORSTRING = "ERRORSTRING";

    public static final String PIPENAME = "RSPipeline";

    public static final String ERROR_NO_DATA_STRING 		= "Stream contains no data.";
    public static final String ERROR_DEVICE_BUSY_STRING		= "Could not open audio device for playback. Device is being used by another application.";
    public static final String ERROR_DEVICE_CANT_OPEN_STRING 	= "Could not open audio device for playback.";
    public static final String ERROR_UNKNOWN_FORMAT_STRING 	= "Could not determine type of stream.";
    public static final String ERROR_FILE_OPEN_STRING 		= "Could not open file "; //followed by file name.
    public static final String ERROR_DEVICE_FAULT_STRING	= "Could not get/set settings from/on resource.";
    public static final String ERROR_REC_DEVICE_BUSY_STRING	= "Could not open audio device for recording. Device is being used by another application.";
    public static final String ERROR_REC_DEVICE_CANT_OPEN_STRING= "Could not open audio device for recording.";
    public static final String ERROR_NO_SPACE_STRING		= "No space left on the resource.";
    public static final String ERROR_NO_FILE_STRING		= "Resource not found.";

    public static final String ERROR_NO_DATA 		= "ERROR_NO_DATA";
    public static final String ERROR_DEVICE_BUSY	= "ERROR_DEVICE_BUSY";
    public static final String ERROR_DEVICE_CANT_OPEN 	= "ERROR_DEVICE_CANT_OPEN";
    public static final String ERROR_UNKNOWN_FORMAT 	= "ERROR_UNKNOWN_FORMAT";
    public static final String ERROR_FILE_OPEN 		= "ERROR_FILE_OPEN";
    public static final String ERROR_DEVICE_FAULT	= "ERROR_DEVICE_FAULT";
    public static final String ERROR_REC_DEVICE_CANT_OPEN="ERROR_REC_DEVICE_CANT_OPEN";
    public static final String ERROR_REC_DEVICE_BUSY	= "ERROR_REC_DEVICE_BUSY";
    public static final String ERROR_NO_SPACE		= "ERROR_NO_SPACE";
    public static final String ERROR_NO_FILE		= "ERROR_NO_FILE";

    static {
	logger = new LogUtilities("RSPipeline");
	
	for (int i = 0; i < 3; i++){
	    try{
		logger.debug("RSPipeline: Gstreamer init.");
		Gst.init(PIPENAME, new String[] {"--gst-disable-segtrap"});
		pipe = new Pipeline("RSPipeline");
		pipe.setState(State.NULL);

		break;
	    }catch (Exception e){
		logger.error("Static: Unable to initialize gstreamer in attempt: "+(i+1), e);
	    }

	    if (i == 2){
		logger.fatal("Static: Unable to initialize gstreamer after 3 attempts");
		//System.exit(-1);
	    }
	}
	
	bus = pipe.getBus();
	
	bus.connect(new Bus.ERROR(){
		public void errorMessage(GstObject source, int code, String message){
		    handleError(source, message);
		}
	    });
	
	/*
	bus.connect(new Bus.MESSAGE(){
		public void busMessage(Bus bus, Message message){
		    //System.err.println("MSG: "+message.getSource()+":"+message.getType()+":"+message.getStructure());
		    if (message.getType() == MessageType.ELEMENT){
			
		    }
		}
	    });
	*/

	stateHandler = new Bus.STATE_CHANGED(){
		public void stateChanged(GstObject source, State old, State cur, State pending){
		    
		    String sourceName = source.getName();

		    if (sourceName.endsWith(BIN)){
			logger.info("StateChanged:"+source+":"+old+":"+cur+":"+pending);
		    }

		    //Is there a better way to handle bin moving to paused state while trying to play?
		    if (sourceName.endsWith(BIN) && 
		       old == State.READY && cur == State.PAUSED && pending == State.VOID_PENDING &&
		       states.get(source) == State.PLAYING){
			logger.debug("StateChanged: "+source.getName()+" reached paused, pushing to play.");
			setState((Element)source, State.PLAYING);
		    }
		    
		    if (sourceName.endsWith(BIN) &&
		       old == State.READY && cur == State.PAUSED && 
		       (sourceName.startsWith(RSBin.PLAYOUT.getName()) || sourceName.startsWith(RSBin.PREVIEW.getName()))){
			logger.debug("StateChanged: Bin: "+source+" reached paused, pushing to play.");
			//setState(getSink((Bin) source), State.PLAYING);
			setState(((Bin) source), State.PLAYING);
		    }

		    
		    if (isSourceBin(source, RSBin.ARCHIVER) && shouldPlay(source, cur, pending)){
			logger.debug("StateChanged: ArchiverBin reached paused, pushing to play.");
			//setState(getSink(RSBin.ARCHIVER), State.PLAYING);
			setState(getSink(findElementGsBin((Element)source)), State.PLAYING);

		    } else if (isSourceBin(source, RSBin.MONITOR) && shouldPlay(source, cur, pending)){

			logger.debug("StateChanged: MonitorBin reached paused, pushing to play.");
			//setState(getSink(RSBin.MONITOR),State.PLAYING);
			setState(getSink(findElementGsBin((Element)source)), State.PLAYING);

		    } else if (isSourceBin(source, RSBin.MONITOR_DIAGNOSTICS) && shouldPlay(source, cur, pending)){

			logger.debug("StateChanged: MonitorDiagnosticsBin reached paused, pushing to play.");
			setState(getSink(findElementGsBin((Element)source)), State.PLAYING);

		    } else if (isSourceBin(source, RSBin.MIC_PLAYOUT) && shouldPlay(source, cur, pending)){

			logger.debug("StateChanged: MicBin reached paused, pushing to play.");
			//setState(getSink(RSBin.MIC), State.PLAYING);
			//setState(getSource(bins.get(RSBin.MIC)), State.PLAYING);

			//setState(getSink(findElementGsBin((Element)source)), State.PLAYING);
			//setState(getSource(findElementGsBin((Element)source)), State.PLAYING);
			//setState((Element)source, State.PLAYING);
		    } else if(isSourceBin(source, RSBin.ONLINE_TELEPHONY) && shouldPlay(source, cur, pending)) {
			logger.info("StateChanged: OnlineTelephonyBin reached paused, pushing to play.");
			setState(getSink(findElementGsBin((Element)source)), State.PLAYING);
		    }

		}
	    };

	bus.connect(stateHandler);


	bus.connect(new Bus.TAG(){
		public void tagsFound(GstObject source, TagList tagList){
		    handleTags(source, tagList);
		}
	    });

	
	bins = new Hashtable<RSBin, Bin>();
	rsBins = new Hashtable<Bin, RSBin>();
	states = new Hashtable<GstObject, State>();
	listeners = new Hashtable<RSBin, PipelineListener>();
	playoutSrcBins = new Hashtable<String, Bin>();
	//playoutListeners = new Hashtable<String, PipelineListener>();
	audioListeners = new Hashtable<String, PipelineListener>();
	playBins = new Hashtable<String, Bin>();
	playBinKeys = new Hashtable<Bin, String>();
	playBinInfos = new Hashtable<Bin, PlayBinInfo>();
	
	Thread t = new Thread("RSPipeline"){
		public void run(){
		    logger.info("Entering Gst.main()");
		    for (int i = 0; i < 3; i++){
			try{
			    Gst.main();
			    break;
			} catch (Exception e) {
			    logger.error("Run: Gstreamer exception in Gst.main() during attempt: "+(i+1),e);
			    if (i == 2){
				logger.fatal("Run: Could not enter Gst.main() in 3 attempts.", e);
				//System.exit(-1);
			    }
			}
		    }
		    logger.info("Exiting from Gst.main()");
		}
	    };
	
	t.start();
    }

    static boolean isSourceBin(GstObject source, RSBin bin){
	return source.getName().equals(bin.getName()+BIN);
    }

    static boolean shouldPlay(GstObject source, State cur, State pending){
	return (cur == State.PAUSED && pending == State.PLAYING && states.get(source) == State.PLAYING);
    }

    public static synchronized void init(StationConfiguration stationConfiguration){
	if (stationConfig != null){
	    return;
	}
	logger.debug("Init: Initializing.");
	stationConfig = stationConfiguration;
	resampler = stationConfig.getStringParam(StationConfiguration.RESAMPLER);
	resourceConfigs = new Hashtable<String, Hashtable<String, ResourceConfig>>();
	//CAPS_STRING = "audio/x-raw-int, width=16, depth=16, rate="+stationConfig.getStringParam(StationConfiguration.SAMPLE_RATE)+", channels=2, endianness=1234, signed=true";

	for (String SERVICE: RSController.SERVICES){
	    Hashtable<String, ResourceConfig> serviceResourceConfigs = new Hashtable<String, ResourceConfig>();
	    Service services[] = stationConfig.getServices(SERVICE);
	    for (Service service: services){
		for (ServiceResource serviceResource: stationConfig.getServiceResources(service.serviceInstanceName())){
			ResourceConfig resourceConfig = stationConfig.getResourceConfig(serviceResource.resourceName());
			serviceResourceConfigs.put(serviceResource.resourceRole(), resourceConfig);
		}
	    }
	    
	    resourceConfigs.put(SERVICE, serviceResourceConfigs);
	}
	
	/*
	if (stationConfig.isLocalService(stationConfig.getProviderServiceInstance(RSApp.PLAYOUT_PROVIDER, AudioService.PLAYOUT)))
	    initPlayout();
	   
	if (stationConfig.isLocalService(stationConfig.getProviderServiceInstance(RSApp.PREVIEW_PROVIDER, AudioService.PREVIEW)))
	    initPreview();
	*/
	/*
	boolean playoutLocal = stationConfig.isLocalService(stationConfig.getProviderServiceInstance(RSApp.PLAYOUT_PROVIDER, AudioService.PLAYOUT));
	boolean previewLocal = stationConfig.isLocalService(stationConfig.getProviderServiceInstance(RSApp.PREVIEW_PROVIDER, AudioService.PREVIEW));

	if(playoutLocal || previewLocal)
	*/
	playBinPool = new PlayBinPool();

	if (stationConfig.isLocalService(stationConfig.getProviderServiceInstance(RSApp.ARCHIVER_PROVIDER, ArchiverService.BCAST_MIC)))
	    initArchiver();

	if (stationConfig.isLocalService(stationConfig.getProviderServiceInstance(RSApp.MONITOR_PROVIDER, MonitorService.MONITOR)))
	    initMonitor();
	
	if (stationConfig.isLocalService(stationConfig.getProviderServiceInstance(RSApp.MIC_PROVIDER, MicService.BCAST_MIC))) {
	    initMicPlayout();
	    //initMicPreview();
	}
	
	if (stationConfig.isLocalService(stationConfig.getProviderServiceInstance(RSApp.TELEPHONY_PROVIDER, OnlineTelephonyService.ONLINE_TELEPHONY))) {
	    initOnlineTelephony();
	}
    }
    
    public static synchronized void addPipelineListener(RSBin bin, PipelineListener listener){
	if (!bin.isInit())
	    switch(bin){
	    case PLAYOUT:
		//initPlayout();
		break;
	    case PREVIEW:
		//initPreview();
		break;
	    case ARCHIVER:
		initArchiver();
		break;
	    case MONITOR:
		initMonitor();
		break;
	    case MONITOR_DIAGNOSTICS:
		initMonitorDiagnostics();
		break;
	    case MIC_PLAYOUT:
		initMicPlayout();
		break;
	    case MIC_PREVIEW:
		logger.warn("AddPipelineListener: Preview facitility for mic service is not available in this distribution.");
		//initMicPreview();
		break;
	    case MIC_DIAGNOSTICS:
		initMicDiagnostics();
		break;
	    case ONLINE_TELEPHONY:
		initOnlineTelephony();
		break;
	    }
	listeners.put(bin, listener);
    }

    public static double linearToDB (double x){
	return 20.0 * Math.log10(x);
    }

    public static double dBToLinear (double x){
	return Math.pow (10.0, x/20.0);
    }

    protected static double volumeFromGain(double gain, double peak){
	if (gain > 60.0 || gain < -60.0)
	    gain = 0.0;
	if (peak < 0.0 || peak > 1.0)
	    peak = 1.0;
	
	if ( linearToDB(peak) + gain > 0.0 )
	    return (1.0/peak);
	
	return Math.min(dBToLinear(gain), 10.0);
	
    }

    protected static double clampVolume(double volume){
	if (volume < 0.0)
	    return 0.0;
	else if (volume > 10.0)
	    return 10.0;
	else
	    return volume;
    }

    protected static void handleTags(GstObject source, TagList tagList){
	logger.debug("HandleTags: received tags from "+source+": "+tagList);

	RSBin bin = findElementBin((Element) source);

	if (!(bin == RSBin.PLAYOUT || bin == RSBin.PREVIEW)){
	    return;
	}
	
	if (!tagList.getTagNames().get(0).equals("replaygain-track-gain"))
	    return;

	double gain = tagList.getDouble("replaygain-track-gain");
	double peak = tagList.getDouble("replaygain-track-peak");
	
	Bin gsBin = findElementGsBin((Element) source);

	Element volumeElement = getElementByType(gsBin, VOLUME);
	double volume = clampVolume(((Double)volumeElement.get("volume")) * volumeFromGain(gain, peak));
	
	volumeElement.set("volume", Double.toString(volume));
	
	logger.info("HandleTags: Set volume to " + volume);

	/* RGVolume element not available on windows currently.

	Element rgVolume = bins.get(bin).getElementByName(bin.getName()+"RGVolume");

	rgVolume.getStaticPad("sink").sendEvent(new TagEvent(tagList));
	*/
    }

    protected static void handleEOS(GstObject source){
	RSBin bin;
	PipelineListener listener;

 	if ((bin = rsBins.get(source)) != null){
	    logger.info("HandleEOS: EOS received from Bin: "+bin.getName());
	    if (bin != RSBin.PLAYOUT && bin != RSBin.PREVIEW){
		if ((listener = listeners.get(bin)) != null){
		    listener.playDone(bin);
		}
	    }
	}
	
	Bin gsBin = findElementGsBin((Element)source);
	bin = findElementBin(gsBin);

	if (gsBin == null){
	    logger.error("HandleEOS: EOS received from unknown playbin: "+source);
	    return;
	}

	String key = playBinKeys.get(gsBin);
	listener = audioListeners.get(key);
	if (listener != null)
	    listener.playDone(bin, key);
	
	removePlayBin(key);
	//setState(bin, State.NULL);
    }

    protected static void handleError(GstObject source, String message){
	logger.error("HandleError: "+source+": "+message);

	if (!(source instanceof Element)){
	    logger.error("HandleError: Error received from non-Element object.");
	    return;
	} 

	PipelineListener listener = null;

	Bin gsBin = findElementGsBin((Element) source);
	RSBin rsBin = findElementBin((Element) source);

	switch(rsBin){
	case PLAYOUT:
	case PREVIEW:
	    String key = playBinKeys.get(gsBin);
	    listener = audioListeners.get(key);

	    if (listener != null)
		listener.gstError(rsBin, key, errorType(message));

	    removePlayBin(key);
	    break;

	default:

	    notifyError(rsBin, errorType(message));
	}
	
    }

    protected static Bin getSrcBin(Element element){
	while (element != null && !playoutSrcBins.containsValue(element)){
	    element = (Element)element.getParent();
	}
	return (Bin)element;
    }

    /*
    protected static void disconnectSourceBin(final Bin srcBin, boolean force){
	final Bin topBin = (Bin) srcBin.getParent();
	final Pad srcPad = srcBin.getStaticPad("src");
	
        Element adderTmp = null;
	
	if (topBin != null)
	    adderTmp = topBin.getElementByName(RSBin.PLAYOUT.getName()+ADDER);
	
	final Element adder = adderTmp;

	final Pad adderPad = srcPad.getPeer();

	if (force && adderPad != null){
	    srcPad.unlink(adderPad);
	    adder.releaseRequestPad(adderPad);
	} else {
	    srcPad.setBlockedAsync(true, new Pad.PAD_BLOCKED(){
		    public void padBlocked(Pad pad, boolean blocked){
			Pad adderPad = srcPad.getPeer();
			if (adderPad != null){
			    srcPad.unlink(adderPad);
			    adder.releaseRequestPad(adderPad);
			}
		    }
		});
	}

	if (topBin != null){
	    topBin.remove(srcBin);
	}
    }
    */

    protected static RSBin findElementBin(Element element){
	Bin gsBin = findElementGsBin(element);
	if (gsBin == null)
	    return null;
	else
	    return rsBins.get(gsBin);
    }

    protected static Bin findElementGsBin(Element element){
	while ((element != null) && !element.getName().endsWith(BIN)){
	    element = (Element) element.getParent();
	}
	if (element == null)
	    return null;
	else 
	    return (Bin) element;
    }

    protected static void notifyError(RSBin bin, String message){
	if (bin == null){
	    logger.error("NotifyError: Null bin received, unable to locate listener");
	    return;
	}
	
	PipelineListener listener;
	if ((listener = listeners.get(bin)) != null)
	    listener.gstError(bin, message);
	else {
	    logger.error("NotifyError: Listener found null.");
	}
	
    }

    protected static String errorType(String message){
	if (message.startsWith(ERROR_NO_DATA_STRING))
	    return ERROR_NO_DATA;
	else if (message.startsWith(ERROR_DEVICE_BUSY_STRING))
	    return ERROR_DEVICE_BUSY;
	else if (message.startsWith(ERROR_DEVICE_CANT_OPEN_STRING))
	    return ERROR_DEVICE_CANT_OPEN;
	else if (message.startsWith(ERROR_UNKNOWN_FORMAT_STRING))
	    return ERROR_UNKNOWN_FORMAT;
	else if (message.startsWith(ERROR_FILE_OPEN_STRING))
	    return ERROR_FILE_OPEN;
	else if (message.startsWith(ERROR_DEVICE_FAULT_STRING))
	    return ERROR_DEVICE_FAULT;
	else if (message.startsWith(ERROR_REC_DEVICE_BUSY_STRING))
	    return ERROR_REC_DEVICE_BUSY;
	else if (message.startsWith(ERROR_REC_DEVICE_CANT_OPEN_STRING))
	    return ERROR_REC_DEVICE_CANT_OPEN;
	else if (message.startsWith(ERROR_NO_SPACE_STRING))
	    return ERROR_NO_SPACE;
	else if (message.startsWith(ERROR_NO_FILE_STRING))
	    return ERROR_NO_FILE;

	return "";
    }

    static synchronized ResourceConfig getResourceConfig(String service, String portRole){
	return resourceConfigs.get(service).get(portRole);
    }

    /*
    static boolean initPlayout(){
	String name = RSBin.PLAYOUT.getName();

	try {
	    logger.debug("InitPlayout: Initializaing Playout Bin.");
	    playoutBin = new Bin(name+BIN);

	    Element adder = ElementFactory.make("adder", name+ADDER);

	    String mixer = null, deviceName = null;
	 
	    ResourceConfig resourceConfig = getResourceConfig(RSController.AUDIO_SERVICE, AudioService.PLAYOUT);
	    mixer = resourceConfig.resourceMixer();
	    deviceName = resourceConfig.resourceDeviceName();
	    

	    Element playoutSink = getAudioSink(mixer, deviceName, name);

	    logger.debug("InitPlayout: Using sink mixer: "+mixer+" deviceName: "+deviceName);

	    //playoutBin.addMany(source, decodeBin, audioConvert, resample, rgVolume, volume, playoutSink);
	    //playoutBin.linkMany(source, decodeBin);
	    //playoutBin.linkMany(audioConvert, resample, rgVolume, volume, playoutSink);
	    
	    playoutBin.addMany(adder, playoutSink);
	    playoutBin.linkMany(adder, playoutSink);

	    pipe.add(playoutBin);

	    playoutBin.setState(State.NULL);
	    
	    
	    Bus bus = playoutBin.getBus();
	    
	  
	    //bus.connect(new Bus.STATE_CHANGED(){
	    //    public void stateChanged(GstObject source, State old, State cur, State pending){
	    //	stateHandler.stateChanged(source, old, cur, pending);
	    //    }
	    //    
	    //});
	    

	    bus.connect(new Bus.EOS(){
		    public void endOfStream(GstObject source){
			logger.info("EndOfStream: EOS received from: "+source);
			handleEOS(source);
		    }
		});
		
	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			handleError(source, message);
		    }
		});
		
	    bus.connect(new Bus.TAG(){
		    public void tagsFound(GstObject source, TagList tagList){
			handleTags(source, tagList);
		    }
		});
	    
	    bins.put(RSBin.PLAYOUT, playoutBin);
	    rsBins.put(playoutBin, RSBin.PLAYOUT);
	    RSBin.PLAYOUT.setInit();

	} catch (Exception e) {
	    logger.error("InitPlayout: Could not initialize.", e);
	    return false;
	}
	
	return true;
    }

    */

    
    static boolean initPreview(){

	String name = RSBin.PREVIEW.getName();
	try{
	    logger.debug("InitPreview: Initializing Preview Bin.");

	    Pipeline pipe = new Pipeline("previewPipeline");

	    previewBin = new Bin(name+BIN);
	    
	    final Element source = ElementFactory.make("filesrc", name+SOURCE);
	    final DecodeBin decodeBin = new DecodeBin (name+"Decode");
	    
	    final Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);
	    final Bin bin = playoutBin;
	    decodeBin.connect(new DecodeBin.NEW_DECODED_PAD(){
		    public void newDecodedPad(Element element, Pad pad, boolean last){
			decodeBin.link(audioConvert);
		    }
		});
	    
	    Element resample = ElementFactory.make(resampler, name+RESAMPLE);
	    //Element rgVolume = ElementFactory.make("rgvolume", name+"RGVolume");
	    Element volume = ElementFactory.make("volume", name+VOLUME);
	    
	    
	    String mixer = null, deviceName = null;
	    ResourceConfig resourceConfig = getResourceConfig(RSController.AUDIO_SERVICE, AudioService.PREVIEW);
	    mixer = resourceConfig.resourceMixer();
	    deviceName = resourceConfig.resourceDeviceName();
	    	    
	    Element previewSink = getAudioSink(mixer, deviceName, name);
	    
	    logger.debug("InitPreview: Using sink mixer: "+mixer+" deviceName: "+deviceName);

	    previewBin.addMany(source, decodeBin, audioConvert, resample, /*rgVolume,*/ volume, previewSink);
	    previewBin.linkMany(source, decodeBin);
	    previewBin.linkMany(audioConvert, resample, /*rgVolume,*/ volume, previewSink);

	    pipe.add(previewBin);
	    
	    /*
	    previewBin.setState(State.NULL);

	    bins.put(RSBin.PREVIEW, previewBin);
	    rsBins.put(previewBin, RSBin.PREVIEW);
	    */

	    Bus bus = pipe.getBus();

	    bus.connect(new Bus.EOS(){
		    public void endOfStream(GstObject source){
			handleEOS(source);
		    }
		});

	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			handleError(source, message);
		    }
		});

	    pipe.setState(State.NULL);

	    bins.put(RSBin.PREVIEW, pipe);
	    rsBins.put(pipe, RSBin.PREVIEW);


	    RSBin.PREVIEW.setInit();

	} catch (Exception e) {
	    logger.error("InitPreview: Could not initialize.", e);
	    return false;
	}

	return true;
    }

    static boolean initArchiver (){
	ResourceConfig resourceConfig = getResourceConfig(RSController.ARCHIVER_SERVICE, ArchiverService.BCAST_MIC);

	String portType = resourceConfig.resourceMixer();
	String portName = resourceConfig.resourceDeviceName();
	logger.debug("InitArchiver: portType="+portType+" portName="+portName);

	String codec = stationConfig.getStringParam(StationConfiguration.ARCHIVER_CODEC);

	String name = RSBin.ARCHIVER.getName();
	try {
	    logger.debug("InitArchiver: Initializing Archiver Bin.");
	    archiverBin = new Bin(name+BIN);
	    if (portType.equals(ResourceManager.PORT_JACK))
		portName = ArchiverService.clientName;

	    Element audioSrc = getAudioSource(portType, portName, name);
	    Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);
	    Element audioResample = ElementFactory.make(resampler, name+RESAMPLE);
	    Element capsFilter = ElementFactory.make("capsfilter", name+CAPS);
	    String archiverRate = stationConfig.getStringParam(StationConfiguration.ARCHIVER_SAMPLE_RATE);

	    capsFilter.set("caps", Caps.fromString("audio/x-raw-int, width=16, depth=16, rate="+archiverRate));

	    Element encoder = null;
	    if (codec.toLowerCase().equals("mp3")){
		encoder = ElementFactory.make("lame", name+"Lame");
	    } else if (codec.toLowerCase().equals("wav")) {
		encoder = ElementFactory.make("wavenc", name+"Wavenc");
	    }
	    Element archiverSink = ElementFactory.make("filesink", name+SINK);
	    
	    archiverBin.addMany(audioSrc, audioConvert, audioResample, capsFilter, encoder, archiverSink);
	    archiverBin.linkMany(audioSrc, audioConvert, audioResample, capsFilter, encoder, archiverSink);
	    
	    pipe.add(archiverBin);

	    archiverBin.setState(State.NULL);

	    bins.put(RSBin.ARCHIVER, archiverBin);
	    rsBins.put(archiverBin, RSBin.ARCHIVER);
	    RSBin.ARCHIVER.setInit();

	}catch (Exception e){
	    logger.error("InitArchiver: Could not initialize.", e);
	    return false;
	}

	return true;
    }

    static boolean initOnlineTelephony() {
	ResourceConfig resourceConfig = getResourceConfig(RSController.ONLINE_TELEPHONY_SERVICE, OnlineTelephonyService.OFFAIR_PLAYBACK_ROLE);

	String portType = resourceConfig.resourceMixer();
	String portName = resourceConfig.resourceDeviceName();
	logger.debug("InitTelephonyPreview: portType="+portType+" portName="+portName);

	String name = RSBin.ONLINE_TELEPHONY.getName();
	try {
	    logger.debug("InitOnlineTelephony: Initializing Online Telephony Bin.");
	    onlineTelephonyBin = new Bin(name+BIN);

	    Element filesrc = ElementFactory.make("filesrc", name+SOURCE);
	    final DecodeBin decodeBin = new DecodeBin (name+"Decode");
	    final Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);

	    decodeBin.connect(new DecodeBin.NEW_DECODED_PAD(){
		    public void newDecodedPad(Element element, Pad pad, boolean last){
			decodeBin.link(audioConvert);
		    }
		});

	    Element resample = ElementFactory.make(resampler, name+RESAMPLE);
	    Element audioResample = ElementFactory.make(resampler, name+RESAMPLE);

	    String mixer = resourceConfig.resourceMixer();
	    String deviceName = resourceConfig.resourceDeviceName();
	    Element audioSink = getAudioSink(mixer, deviceName, name);

	    onlineTelephonyBin.addMany(filesrc, decodeBin, audioConvert, resample, audioSink);
	    onlineTelephonyBin.linkMany(filesrc, decodeBin);
	    onlineTelephonyBin.linkMany(audioConvert, resample, audioSink);
	    
	    pipe.add(onlineTelephonyBin);
	    onlineTelephonyBin.setState(State.NULL);

	    bins.put(RSBin.ONLINE_TELEPHONY, onlineTelephonyBin);
	    rsBins.put(onlineTelephonyBin, RSBin.ONLINE_TELEPHONY);
	    RSBin.ONLINE_TELEPHONY.setInit();

	} catch (Exception e) {
	    logger.error("InitOnlineTelephony: Could not initialize.", e);
	    return false;
	}

	return true;
    }

    static boolean initMonitor(){

	ResourceConfig srcResourceConfig = getResourceConfig(RSController.MONITOR_SERVICE, MonitorService.BCAST_MIC);
	if (srcResourceConfig == null) {
	    logger.error("InitMonitor: No resource with role " + MonitorService.BCAST_MIC + " found.");
	    return false;
	}
	String srcMixer = srcResourceConfig.resourceMixer();
	String srcDeviceName = srcResourceConfig.resourceDeviceName();

	ResourceConfig sinkResourceConfig = getResourceConfig(RSController.MONITOR_SERVICE, MonitorService.PLAYOUT);
	if (sinkResourceConfig == null) {
	    logger.error("InitMonitor: No resource with role " + MonitorService.PLAYOUT + " found.");
	    return false;
	}

	String sinkMixer = sinkResourceConfig.resourceMixer();
	String sinkDeviceName = sinkResourceConfig.resourceDeviceName();

	Caps srcCaps = getCapsFromSource(srcMixer, srcDeviceName);
	Caps sinkCaps = getCapsFromSink(sinkMixer, sinkDeviceName);
	
	int monitorRate = stationConfig.getIntParam(StationConfiguration.MONITOR_SAMPLE_RATE);
	
	boolean monitorRateSupported = isSampleRateSupported(srcCaps, monitorRate) && isSampleRateSupported(sinkCaps, monitorRate);

	String name = RSBin.MONITOR.getName();
	try{
	    logger.debug("InitMonitor: Initializing Monitor Bin.");
	    monitorBin = new Bin(name+BIN);

	    if (srcMixer.equals(ResourceManager.PORT_JACK))
		srcDeviceName = MonitorService.clientName;

	    Element audioSrc = getAudioSource(srcMixer, srcDeviceName, name);
	    Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);
	    Element audioResampler = ElementFactory.make(resampler, name+RESAMPLE);
	    Element capsFilter = ElementFactory.make("capsfilter", name+CAPS);
	    Element sink = getAudioSink(sinkMixer, sinkDeviceName, name);

	    if (monitorRateSupported){
		capsFilter.set("caps", Caps.fromString("audio/x-raw-int, rate="+Integer.toString(monitorRate)));
	    }

	    monitorBin.addMany(audioSrc, audioConvert, audioResampler, capsFilter, sink);
	    monitorBin.linkMany(audioSrc, audioConvert, audioResampler, capsFilter, sink);
	    
	    pipe.add(monitorBin);

	    bins.put(RSBin.MONITOR, monitorBin);
	    rsBins.put(monitorBin, RSBin.MONITOR);
	    RSBin.MONITOR.setInit();

	}catch (Exception e){
	    logger.error("InitMonitor: Could not initialize.", e);
	    return false;
	}

	return true;
    }

    static boolean initMonitorDiagnostics(){
	ResourceConfig srcResourceConfig = getResourceConfig(RSController.MONITOR_SERVICE, MonitorService.BCAST_MIC);
	String srcMixer = srcResourceConfig.resourceMixer();
	String srcDeviceName = srcResourceConfig.resourceDeviceName();

	ResourceConfig sinkResourceConfig = getResourceConfig(RSController.MONITOR_SERVICE, MonitorService.PLAYOUT);
	String sinkMixer = sinkResourceConfig.resourceMixer();
	String sinkDeviceName = sinkResourceConfig.resourceDeviceName();

	String name = RSBin.MONITOR_DIAGNOSTICS.getName();
	try{
	    logger.debug("InitMonitorDiagnostics: Initializing Monitor Diagnostics Bin.");

	    Pipeline pipe = new Pipeline("monitorDiagnosticsPipe");
	    monitorDiagnosticsBin = new Bin(name+BIN);

	    if (srcMixer.equals(ResourceManager.PORT_JACK))
		srcDeviceName = MonitorService.clientName;

	    Element audioSrc = getAudioSource(srcMixer, srcDeviceName, name);
	    Element audioConvert1 = ElementFactory.make("audioconvert", name+AUDIOCONVERT+1);
	    Element capsFilter = ElementFactory.make("capsfilter", name+CAPS);
	    capsFilter.set("caps", Caps.fromString("audio/x-raw-int, rate=44100"));
	    Element tee = ElementFactory.make("tee", name+TEE);
	    Element q1 = ElementFactory.make("queue", name+QUEUE+0);
	    Element q2 = ElementFactory.make("queue", name+QUEUE+1);
	    Element wavEnc = ElementFactory.make("wavenc", name+"Wavenc");
	    Element fileSink = ElementFactory.make("filesink", name+FILESINK);
	    fileSink.set("location", MONITOR_DIAG_FILE);

	    Element audioConvert2 = ElementFactory.make("audioconvert", name+AUDIOCONVERT+2);
	    Element sink = getAudioSink(sinkMixer, sinkDeviceName, name);

	    monitorDiagnosticsBin.addMany(audioSrc, audioConvert1, capsFilter, tee, q1, q2, wavEnc, fileSink, audioConvert2, sink);
	    monitorDiagnosticsBin.linkMany(audioSrc, audioConvert1, capsFilter, tee, q1, audioConvert2, sink);
	    monitorDiagnosticsBin.linkMany(q2, wavEnc, fileSink);

	    Pad teePad = tee.getRequestPad("src%d");
	    teePad.link(q2.getStaticPad("sink"));
	    

	    pipe.add(monitorDiagnosticsBin);

	    //bins.put(RSBin.MONITOR_DIAGNOSTICS, monitorDiagnosticsBin);
	    //rsBins.put(monitorDiagnosticsBin, RSBin.MONITOR_DIAGNOSTICS);

	    bins.put(RSBin.MONITOR_DIAGNOSTICS, pipe);
	    rsBins.put(pipe, RSBin.MONITOR_DIAGNOSTICS);
	    RSBin.MONITOR_DIAGNOSTICS.setInit();

	}catch (Exception e){
	    logger.error("InitMonitorDiagnostics: Could not initialize.", e);
	    return false;
	}

	return true;
    }


    static boolean initMicPlayout(){
	ResourceConfig srcResourceConfig = getResourceConfig(RSController.MIC_SERVICE, MicService.BCAST_MIC);
	String srcMixer = srcResourceConfig.resourceMixer();
	String srcDeviceName = srcResourceConfig.resourceDeviceName();

	ResourceConfig sinkResourceConfig = getResourceConfig(RSController.MIC_SERVICE, MicService.PLAYOUT);
	String sinkMixer = sinkResourceConfig.resourceMixer();
	String sinkDeviceName = sinkResourceConfig.resourceDeviceName();

	Caps srcCaps = getCapsFromSource(srcMixer, srcDeviceName);
	Caps sinkCaps = getCapsFromSink(sinkMixer, sinkDeviceName);
	
	int monitorRate = stationConfig.getIntParam(StationConfiguration.MONITOR_SAMPLE_RATE);
	
	boolean monitorRateSupported = isSampleRateSupported(srcCaps, monitorRate) && isSampleRateSupported(sinkCaps, monitorRate);

	String name = RSBin.MIC_PLAYOUT.getName();

	try{
	    logger.debug("InitMic: Initializing Mic Bin.");
	    
	    Pipeline pipe = new Pipeline("micPipeline");

	    micBin = new Bin(name+BIN);
	    
	    if (srcMixer.equals(ResourceManager.PORT_JACK))
		srcDeviceName = MicService.clientName;

	    Element audioSrc = getAudioSource(srcMixer, srcDeviceName, name);
	    Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);
	    Element volume = ElementFactory.make("volume", name+VOLUME);
	    Element resample = ElementFactory.make(resampler, name+RESAMPLE);
	    Element capsFilter = ElementFactory.make("capsfilter", name+CAPS);
	    Element audioSink = getAudioSink(sinkMixer, sinkDeviceName, name, "false");


	    if (monitorRateSupported){
		capsFilter.set("caps", Caps.fromString("audio/x-raw-int, rate="+Integer.toString(monitorRate)));
	    }
	    
	    micBin.addMany(audioSrc, audioConvert, volume, resample, capsFilter, audioSink);
	    micBin.linkMany(audioSrc, audioConvert, volume, resample, capsFilter, audioSink);

	    pipe.add(micBin);

	    pipe.getBus().connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			handleError(source, message);
		    }
		});


	    bins.put(RSBin.MIC_PLAYOUT, pipe);
	    rsBins.put(pipe, RSBin.MIC_PLAYOUT);

	    RSBin.MIC_PLAYOUT.setInit();

	}catch (Exception e){
	    logger.error("InitMic: Could not initialize.", e);
	    return false;
	}

	return true;
    }

    static boolean initMicDiagnostics(){
	ResourceConfig srcResourceConfig = getResourceConfig(RSController.MIC_SERVICE, MicService.BCAST_MIC);
	String srcMixer = srcResourceConfig.resourceMixer();
	String srcDeviceName = srcResourceConfig.resourceDeviceName();

	ResourceConfig sinkResourceConfig = getResourceConfig(RSController.MIC_SERVICE, MicService.PLAYOUT);
	String sinkMixer = sinkResourceConfig.resourceMixer();
	String sinkDeviceName = sinkResourceConfig.resourceDeviceName();

	String name = RSBin.MIC_DIAGNOSTICS.getName();

	try{
	    logger.debug("InitMicDiagnostics: Initializing MicDiagnostics Bin.");
	    Pipeline pipe = new Pipeline("micDiagnosticsPipe");

	    micBin = new Bin(name+BIN);
	    
	    if (srcMixer.equals(ResourceManager.PORT_JACK))
		srcDeviceName = MicService.clientName;

	    Element audioSrc = getAudioSource(srcMixer, srcDeviceName, name);
	    Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);
	    Element volume = ElementFactory.make("volume", name+VOLUME);
	    Element resample = ElementFactory.make(resampler, name+RESAMPLE);
	    Element audioSink = getAudioSink(sinkMixer, sinkDeviceName, name, "false");

	    Element tee = ElementFactory.make("tee", name+TEE);
	    Element q1 = ElementFactory.make("queue", name+QUEUE+0);
	    Element q2 = ElementFactory.make("queue", name+QUEUE+1);
	    Element wavEnc = ElementFactory.make("wavenc", name+"Wavenc");
	    Element fileSink = ElementFactory.make("filesink", name+FILESINK);
	    fileSink.set("location", MIC_DIAG_FILE);

	    micBin.addMany(audioSrc, audioConvert, tee, q1, volume, resample, audioSink, 
			   q2, wavEnc, fileSink);
	    micBin.linkMany(audioSrc, audioConvert, tee, q1, volume, resample, audioSink);
	    micBin.linkMany(q2, wavEnc, fileSink);

	    Pad teePad = tee.getRequestPad("src%d");
	    teePad.link(q2.getStaticPad("sink"));

	    pipe.add(micBin);

	    bins.put(RSBin.MIC_DIAGNOSTICS, pipe);
	    rsBins.put(pipe, RSBin.MIC_DIAGNOSTICS);
	    RSBin.MIC_DIAGNOSTICS.setInit();

	}catch (Exception e){
	    logger.error("InitMicDiagnostics: Could not initialize.", e);
	    return false;
	}

	return true;
    }

    public static boolean initStreaming(String streamName, String desc, String host, int port, String protocol, 
					String mountpoint, String username, String password, 
					int sampleRate, int quality, boolean stereo,
					int bitrate, String codec, String mixerName, String deviceName){

	String name = RSBin.STREAMING.getName();

	try {
	    final Pipeline pipe = new Pipeline(name+PIPELINE);

	    final Bin streamingBin = new Bin(name+BIN);
	
	    Element audioSrc = getAudioSource(mixerName, deviceName, name);
	    Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);
	    Element channelsCaps = ElementFactory.make("capsfilter", name + "channels"+CAPS);
	    Element resample = ElementFactory.make(resampler, name+RESAMPLE);
	    Element capsFilter= ElementFactory.make("capsfilter", name+CAPS);
	    if (sampleRate > -1)
		capsFilter.set("caps", Caps.fromString("audio/x-raw-int, rate="+sampleRate));
	    Element oggMux   = ElementFactory.make("oggmux", name+OGG);
	    Element encoder;

	    if (codec.equals(StreamingService.CODEC_OGV)){
		encoder = ElementFactory.make("vorbisenc", name+VORBIS);
		encoder.set("max-bitrate", bitrate);
	    } else if (codec.equals(StreamingService.CODEC_OGS)) {
		encoder = ElementFactory.make("speexenc", name+SPEEX);
		encoder.set("bitrate", bitrate);
		encoder.set("vbr", "true");
	    } else {
		encoder = ElementFactory.make("lame", name+LAME);
		encoder.set("vbr", 4);
		encoder.set("vbr-max-bitrate", bitrate);
		int mode = 1; //joint stereo;
		if (!stereo)
		    mode = 3; //mono
		encoder.set("mode", mode);
		encoder.set("quality", quality);
	    }

	    Element shoutSink = ElementFactory.make("shout2send", name+SINK);
	    
	    shoutSink.set("streamname", streamName);
	    shoutSink.set("description", desc);
	    shoutSink.set("ip", host);
	    shoutSink.set("port", Integer.toString(port));
	    int protocolNum;
	    if (protocol.toLowerCase().equals("icy"))
		protocolNum = 2;
	    else
		protocolNum = 3;
	    shoutSink.set("protocol", Integer.toString(protocolNum));
	    shoutSink.set("mount", mountpoint);
	    shoutSink.set("username", username);
	    shoutSink.set("password", password);
	    //shoutSink.set("qos", "true");
	    
	    shoutSink.connect("connection-problem", new Closure(){
		    public void invoke(Element e, int err){
			PipelineListener listener = listeners.get(RSBin.STREAMING);
			if (listener != null){
			    String error = "";
			    if (err == -2)
				error = StreamingService.STREAMING_ERROR_CONNECTION;
			    else if (err == -3)
				error = StreamingService.STREAMING_ERROR_LOGIN;
			    else if (err == -4)
				error = StreamingService.STREAMING_ERROR_SOCKET;
			    else
				error = StreamingService.STREAMING_ERROR_UNKNOWN;

			    listener.gstError(RSBin.STREAMING, error);
			}
		    }
		});

	    int numChannels = 1;
	    
	    if (stereo){
		Caps srcCaps = getCapsFromSource(mixerName, deviceName);
		if (getMaxChannels(srcCaps) > 1)
		    numChannels = 2;
	    }
	    
	    channelsCaps.set("caps", 
			     Caps.fromString("audio/x-raw-int, channels="+numChannels+
					     "; audio/x-raw-float, channels="+numChannels));

	    if (encoder.getName().endsWith(LAME)){
		streamingBin.addMany(audioSrc, resample, capsFilter, audioConvert, channelsCaps, encoder, shoutSink);
		streamingBin.linkMany(audioSrc, resample, capsFilter, audioConvert, channelsCaps, encoder, shoutSink);
	    } else {
		streamingBin.addMany(audioSrc, resample, capsFilter, audioConvert, channelsCaps, encoder, oggMux, shoutSink);
		streamingBin.linkMany(audioSrc, resample, capsFilter, audioConvert, channelsCaps, encoder, oggMux, shoutSink);
	    }

	    pipe.add(streamingBin);
	    
	    pipe.getBus().connect(new Bus.EOS(){
		    public void endOfStream(GstObject source){
			setState(RSBin.STREAMING, State.NULL);
		    }
		});

	    bins.put(RSBin.STREAMING, pipe);
	    rsBins.put(pipe, RSBin.STREAMING);
	    RSBin.STREAMING.setInit();
	    
	} catch (Exception e){
	    logger.error("InitStreaming: Could not initialize.", e);
	    return false;
	}

	return true;
    }

    /*
    static int playBinCount = 0;
    static Bin newPlayBin(RSBin bin, String fileName, long fadeoutStartTime, double gain){
	
	String name = bin.getName()+(playBinCount++);
	Bin playBin = null;

	try{
	    logger.debug("InitPreview: Initializing Preview Bin.");
	    //Pipeline playPipe = new Pipeline(name+PIPELINE);
	    playBin = new Bin(name+BIN);
	    
	    final Element source = ElementFactory.make("filesrc", name+SOURCE);
	    final DecodeBin decodeBin = new DecodeBin (name+"Decode");
	    
	    final Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);
	    

	    decodeBin.connect(new DecodeBin.NEW_DECODED_PAD(){
		    public void newDecodedPad(Element element, Pad pad, boolean last){
			decodeBin.link(audioConvert);
		    }
		});

	    Element resample = ElementFactory.make(resampler, name+RESAMPLE);
	    //Element rgVolume = ElementFactory.make("rgvolume", name+"RGVolume");
	    Element volume = ElementFactory.make("volume", name+VOLUME);

	    Double binVolume = bin.getVolume() * dBToLinear(gain);

	    volume.set("volume", Double.toString(binVolume));
	    logger.debug("SETTING VOLUME TO: "+binVolume);
	    
	    String mixer = null, deviceName = null;
	    String resourceType = null;
	    if (bin == RSBin.PLAYOUT)
		resourceType = AudioService.PLAYOUT;
	    else
		resourceType = AudioService.PREVIEW;

	    ResourceConfig resourceConfig = getResourceConfig(RSController.AUDIO_SERVICE, resourceType);
	    mixer = resourceConfig.resourceMixer();
	    deviceName = resourceConfig.resourceDeviceName();
	    	    
	    Element playSink = getAudioSink(mixer, deviceName, name);
	    
	    logger.info("NewPlayBin: Using sink mixer: "+mixer+" deviceName: "+deviceName + " type: "+resourceType);

	    playBin.addMany(source, decodeBin, audioConvert, resample, volume, playSink);
	    playBin.linkMany(source, decodeBin);
	    playBin.linkMany(audioConvert, resample, volume, playSink);
	    
	    playBin.setState(State.NULL);

	    pipe.add(playBin);
	    //playPipe.add(playBin);


	    if (playBinCount == 1){
	    Bus bus = playBin.getBus();
	    //Bus bus = playPipe.getBus();
		
		
		Bus.STATE_CHANGED stateChanged = new Bus.STATE_CHANGED(){
			public void stateChanged(GstObject source, State old, State cur, State pending){
			    stateHandler.stateChanged(source, old, cur, pending);
			}
		    };
		
		bus.connect(stateChanged);
		
		Bus.EOS eos = new Bus.EOS(){
			public void endOfStream(GstObject source){
			    logger.info("EndOfStream: EOS received from: "+source);
			    handleEOS(source);
			}
		    };
		
		bus.connect(eos);
		
		Bus.ERROR error = new Bus.ERROR(){
			public void errorMessage(GstObject source, int code, String message){
			    handleError(source, message);
			}
		    };
		
		bus.connect(error);
		
		Bus.TAG tag = new Bus.TAG(){
			public void tagsFound(GstObject source, TagList tagList){
			    handleTags(source, tagList);
			}
		    };
		
		bus.connect(tag);
	    }

	    bin.setInit();
	    
	    rsBins.put(playBin, bin);
	    
	    
	    PlayBinInfo binInfo = new PlayBinInfo(bin, playBin, fadeoutStartTime);
	    playBinInfos.put(playBin, binInfo);
	    
	    return playBin;
	} catch (Exception e) {
	    logger.error("InitPreview: Could not initialize.", e);
	    return null;
	}

    }

    */

    static void connectPlayBinBus(Bin playBin) {
	Bus bus = playBin.getBus();
	
	Bus.STATE_CHANGED stateChanged = new Bus.STATE_CHANGED(){
		public void stateChanged(GstObject source, State old, State cur, State pending){
		    stateHandler.stateChanged(source, old, cur, pending);
		}
	    };
	
	bus.connect(stateChanged);
	
	Bus.EOS eos = new Bus.EOS(){
		public void endOfStream(GstObject source){
		    logger.info("EndOfStream: EOS received from: "+source);
		    handleEOS(source);
		}
	    };
	
	bus.connect(eos);
	
	Bus.ERROR error = new Bus.ERROR(){
		public void errorMessage(GstObject source, int code, String message){
		    handleError(source, message);
		}
	    };
	
	bus.connect(error);
	
	Bus.TAG tag = new Bus.TAG(){
		public void tagsFound(GstObject source, TagList tagList){
		    handleTags(source, tagList);
		}
	    };
	
	bus.connect(tag);
    }

    static Bin newPlayBin(RSBin bin, int id) {
	String name = bin.getName() + (id);
	Bin playBin = null;

	try{
	    playBin = new Bin(name+BIN);
	    
	    final Element source = ElementFactory.make("filesrc", name+SOURCE);
	    final DecodeBin decodeBin = new DecodeBin (name+"Decode");
	    final Element audioConvert = ElementFactory.make("audioconvert", name+AUDIOCONVERT);

	    decodeBin.connect(new DecodeBin.NEW_DECODED_PAD(){
		    public void newDecodedPad(Element element, Pad pad, boolean last){
			decodeBin.link(audioConvert);
		    }
		});

	    Element resample = ElementFactory.make(resampler, name+RESAMPLE);
	    Element volume = ElementFactory.make("volume", name+VOLUME);
	    
	    String mixer = null, deviceName = null;
	    String resourceType = null;
	    if (bin == RSBin.PLAYOUT)
		resourceType = AudioService.PLAYOUT;
	    else
		resourceType = AudioService.PREVIEW;

	    ResourceConfig resourceConfig = getResourceConfig(RSController.AUDIO_SERVICE, resourceType);
	    mixer = resourceConfig.resourceMixer();
	    deviceName = resourceConfig.resourceDeviceName();
	    	    
	    Element playSink = getAudioSink(mixer, deviceName, name);

	    playBin.addMany(source, decodeBin, audioConvert, resample, volume, playSink);
	    playBin.linkMany(source, decodeBin);
	    playBin.linkMany(audioConvert, resample, volume, playSink);
	    playBin.setState(State.NULL);

	    bin.setInit();
	    return playBin;
	} catch (Exception e) {
	    logger.error("NewPlayBin: Could not initialize.", e);
	    return null;
	}

    }


    static void initLazyEncoder(){
	try{

	}catch (Exception e){
	    logger.error("InitLazyEncoder: Could not initialize.", e);
	}
    }

    static final Pattern capsPattern = Pattern.compile("(channels=[^,\\[]*[,])|(channels=[^,]*[\\[][^\\]]*[\\]])");
    static final Pattern numPattern = Pattern.compile("\\d+");

    static int getMaxChannels(Caps caps){
	String capsString = caps.toString();
	Matcher capsMatcher = capsPattern.matcher(capsString);
	int maxChannels = 1;

	while (capsMatcher.find()){
	    String str = capsString.substring(capsMatcher.start(), capsMatcher.end());
	    Matcher m = numPattern.matcher(str);
	    while (m.find()){
		int numChannels = Integer.parseInt(str.substring(m.start(), m.end()));
		if (numChannels > maxChannels)
		    maxChannels = numChannels;
	    }
	}
	
	return maxChannels;
    }

    static Caps getCapsFromSink(String mixer, String deviceName){
	final String name = "GetCapsFromSink";
	final Object lock = new Object();

	try{
	    Pipeline capsPipe = new Pipeline(name);
	    
	    Element fakeSrc = ElementFactory.make("fakesrc", name+"FAKESRC");
	    fakeSrc.set("num-buffers", "1");
	    Element sink = getAudioSink(mixer, deviceName, name);
	    
	    capsPipe.addMany(fakeSrc, sink);
	    capsPipe.linkMany(fakeSrc, sink);

	    Bus bus = capsPipe.getBus();

	    bus.connect(new Bus.EOS(){
		    public void endOfStream(GstObject source){
			synchronized(lock){
			    lock.notifyAll();
			}
		    }
		});

	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			synchronized(lock){
			    lock.notifyAll();
			}
		    }

		});

	    synchronized(lock){
		capsPipe.setState(State.PLAYING);
		lock.wait();
	    }

	    Pad pad = sink.getStaticPad("sink");
	    Caps caps = pad.getCaps();
	    
	    capsPipe.setState(State.NULL);
	    return caps;

	} catch (Exception e){
	    logger.error("GetCapsFromSink: Encountered exception:", e);
	    return null;
	}
	
    }


    static Caps getCapsFromSource(String mixer, String deviceName){
	final String name = "GetCapsFromSource";
	final Object lock = new Object();

	try{
	    Pipeline capsPipe = new Pipeline(name);
	    
	    Element source = getAudioSource(mixer, deviceName, name);
	    source.set("num-buffers", "1");
	    Element fakeSink = ElementFactory.make("fakesink", name+"FAKESINK");

	    capsPipe.addMany(source, fakeSink);
	    capsPipe.linkMany(source, fakeSink);

	    Bus bus = capsPipe.getBus();

	    bus.connect(new Bus.EOS(){
		    public void endOfStream(GstObject source){
			synchronized(lock){
			    lock.notifyAll();
			}
		    }
		});


	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			synchronized(lock){
			    lock.notifyAll();
			}
		    }

		});

	    synchronized(lock){
		capsPipe.setState(State.PLAYING);
		lock.wait();
	    }
	    
	    Pad pad = source.getStaticPad("src");
	    Caps caps = pad.getCaps();
	    capsPipe.setState(State.NULL);

	    return caps;

	} catch (Exception e){
	    logger.error("GetCapsFromSource: Encountered exception:", e);
	    return null;
	}
	
    }

    static boolean isSampleRateSupported(Caps caps, int rate){
	Caps rateCaps = Caps.fromString("audio/x-raw-int, rate=" + Integer.toString(rate));
	return (!caps.intersect(rateCaps).isEmpty());
    }

    static Element getAudioSource(String portType, String portName, String name){
	Element audioSrc = null;
	try {
	    if (portType.equals(ResourceManager.PORT_JACK)){
		audioSrc = ElementFactory.make("jackaudiosrc", name+SOURCE);
		if (portName.matches("in_.*"))
		    portName = portName.substring(3);
		audioSrc.set("name", portName);
		audioSrc.set("connect", "0");
	    }
	    else if (portType.equals(ResourceManager.PORT_ALSA)) {
		audioSrc = ElementFactory.make("alsasrc", name+SOURCE);
		audioSrc.set("device", portName);
		int latencyTime = stationConfig.getIntParam(StationConfiguration.ALSASRC_LATENCY_TIME);
		int bufferTime = stationConfig.getIntParam(StationConfiguration.ALSASRC_BUFFER_TIME);
		if (latencyTime > 0){
		    audioSrc.set ("latency-time", latencyTime);
		    logger.debug("GetAudioSource: Using latency-time="+latencyTime);
		}else{
		    logger.debug("GetAudioSource: Using default latency-time.");
		}
		if (bufferTime > 0){
		    audioSrc.set ("buffer-time", bufferTime);
		    logger.debug("GetAudioSource: Using buffer-time="+bufferTime);
		}else{
		    logger.debug("GetAudioSource: Using default buffer-time.");
		}
	    } 
	    else if (portType.equals(ResourceManager.PORT_WIN)) {
		audioSrc = ElementFactory.make("dshowaudiosrc", name+SOURCE);
		audioSrc.set("device", portName);
	    }
	    else if (portType.equals(ResourceManager.PORT_DEFAULT)) {
		String operatingSystem = stationConfig.getOperatingSystem();
		if (operatingSystem.equals(StationConfiguration.LINUX)) {
		    audioSrc = ElementFactory.make("alsasrc", name+SOURCE);
		} else {
		    audioSrc = ElementFactory.make("dshowaudiosrc", name+SOURCE);
		}
	    }
	} catch (Exception e) {
	    logger.error("GetAudioSource: Gstreamer exception while constructing audio source.", e);
	}
	return audioSrc;
    }

    static Element getAudioSink(String mixer, String deviceName, String name){
	return getAudioSink(mixer, deviceName, name, null);
    }

    static Element getAudioSink(String mixer, String deviceName, String name, String syncParam){
	Element sink = null;
	
	try{
	    if (mixer.equals (ResourceManager.PORT_JACK)){
		sink = ElementFactory.make("jackaudiosink", name+SINK);
		//sink.set ("name", deviceName);
	    }
	    else if (mixer.equals (ResourceManager.PORT_ALSA)){
		sink = ElementFactory.make("alsasink", name+SINK);
		sink.set ("device", deviceName);


		int latencyTime = stationConfig.getIntParam(StationConfiguration.ALSASINK_LATENCY_TIME);
		int bufferTime = stationConfig.getIntParam(StationConfiguration.ALSASINK_BUFFER_TIME);
		String sync = (syncParam == null)?stationConfig.getStringParam(StationConfiguration.ALSASINK_SYNC):syncParam;
		if (latencyTime > 0){
		    sink.set ("latency-time", latencyTime);
		    logger.debug("GetAudioSink: Using latency-time="+latencyTime);
		}else{
		    logger.debug("GetAudioSink: Using default latency-time.");
		}
		if (bufferTime > 0){
		    sink.set ("buffer-time", bufferTime);
		    logger.debug("GetAudioSink: Using buffer-time="+bufferTime);
		}else{
		    logger.debug("GetAudioSink: Using default buffer-time.");
		}
		if (sync != null){
		    sink.set ("sync", sync);
		    logger.debug("GetAudioSink: Using sync="+sync);
		}else{
		    logger.debug("GetAudioSink: Using default sync.");
		}
	    } 
	    else if (mixer.equals(ResourceManager.PORT_WIN)) {
		sink = ElementFactory.make("directsoundsink", name+SINK);
		int latencyTime = stationConfig.getIntParam(StationConfiguration.DIRECTSOUNDSINK_LATENCY_TIME);
		int bufferTime = stationConfig.getIntParam(StationConfiguration.DIRECTSOUNDSINK_BUFFER_TIME);
		String sync = stationConfig.getStringParam(StationConfiguration.DIRECTSOUNDSINK_SYNC);
		logger.debug("GetAudioSink: Using latency-time="+latencyTime+" buffer-time="+bufferTime+" sync="+sync);
		if (latencyTime > 0)
		    sink.set ("latency-time", latencyTime);
		if (bufferTime > 0)
		    sink.set ("buffer-time", bufferTime);
		if (sync != null)
		    sink.set ("sync", sync);

		sink.set("deviceguid", deviceName);
	    }
	    else if (mixer.equals(ResourceManager.PORT_DEFAULT)) {
		String operatingSystem = stationConfig.getOperatingSystem();
		if (operatingSystem.equals(StationConfiguration.LINUX)) {
		    sink = ElementFactory.make("alsasink", name+SINK);
		} else {
		    sink = ElementFactory.make("directsoundsink", name+SINK);
		}
	    }
	} catch (Exception e){
	    logger.error("GetAudioSink: Gstreamer exception while constructing audio sink.", e);
	}
	return sink;
    }

    static Element getSource(Bin bin){
	/*
	String srcName = bin.getName().split(BIN)[0]+SOURCE;
	return bin.getElementByName(srcName);
	*/
	return getElementByType(bin, SOURCE);
    }

    static Element getSink(Bin bin){
	/*
	String sinkName = bin.getName().split(BIN)[0]+SINK;
	return bin.getElementByName(sinkName);
	*/
	return getElementByType(bin, SINK);
    }
    
    static Element getElementByType(Bin bin, String type){
	String elementName;
	if (bin.getName().endsWith(PIPELINE)){
	    String pipeName = bin.getName().split(PIPELINE)[0];
	    elementName = pipeName + type;
	    bin = (Bin)bin.getElementByName(pipeName + BIN);
	} else {
	    elementName = bin.getName().split(BIN)[0]+type;
	}
	return bin.getElementByName(elementName);
    }

    public static void setInputFile(Bin gsBin, String fileName){ //, String key){
	logger.info("SetInputFile: Bin: "+gsBin.getName()+" setting input file to: "+fileName);
	
	Element src = getSource(gsBin);
	File file = new File(fileName);

	if (!file.exists()){
	    logger.error("SetInputFile: Bin: "+gsBin.getName()+" File does not exist: "+fileName);
	}
	
	try{
	    if (src.getFactory().getName().equals("filesrc")){
		src.set("location", fileName);		
	    }else{
		logger.error("SetInputFile: Element not a filesrc.");
	    }
	}catch(Exception e){
	    logger.error("SetInputFile: Gstreamer exception while setting input file.", e);
	}

	//Element volume = bins.get(bin).getElementByName(bin.getName()+VOLUME);
	//volume.set("volume", "1.0");
    }


    public static void setInputFile(RSBin bin, String fileName){
	Bin gsBin = bins.get(bin);
	setInputFile(gsBin, fileName);
    }

    public static void setOutputFile(RSBin bin, String fileName){
	Element sink = getSink(bins.get(bin));
	
	try{
	    if (sink.getFactory().getName().equals("filesink")){
		sink.set("location", fileName);
	    }else{
		logger.error("SetOutputFile: Element not a filesink.");
	    }
	}catch(Exception e){
	    logger.error("SetOutputFile: Gstreamer exception while setting output file.", e);
	}
    }

    static boolean setState(RSBin bin, State state){
	Bin binElement = bins.get(bin);
	if (binElement == null || state == null)
	    return false;
	states.put((GstObject)binElement, state);
	return setState(binElement, state);
    }

    static boolean setState(Element element, State state){
	try{
	    if (element.setState(state) == StateChangeReturn.FAILURE){
		logger.error("SetState: Could not set state of "+element.getName() +" to "+state);
		return false;
	    } else {
		logger.info("SetState: Setting state of "+element.getName()+" to "+state);
		return true;
	    }
	}catch(Exception e){
	    logger.error("SetState: Gstreamer exception while changing state.", e);
	    return false;
	}
    }

    public static boolean isPaused(String key) {
	Bin bin = playBins.get(key);
	if (bin == null)
	    return false;
	else
	    return true;
    }


    static boolean firstPlay = true;
    public static boolean play(RSBin rsBin, String fileName, String key, 
			       long fadeoutStartTime, long position, double gain, PipelineListener listener){

	String name = rsBin.getName();
	Bin bin = playBins.get(key);
	
	boolean resume = true;
	if (bin == null){
	    bin = playBinPool.getFreeBin(rsBin);

	    Element volume = getElementByType(bin, VOLUME);
	    Double binVolume = rsBin.getVolume() * dBToLinear(gain);
	    volume.set("volume", Double.toString(binVolume));


	    rsBins.put(bin, rsBin);
	    PlayBinInfo binInfo = new PlayBinInfo(rsBin, bin, fadeoutStartTime);
	    playBinInfos.put(bin, binInfo);
	    playBins.put(key, bin);
	    playBinKeys.put(bin, key);
	    audioListeners.put(key, listener);
	    setInputFile(bin, fileName);

	    pipe.add(bin);

	    if(firstPlay) {
		connectPlayBinBus(bin);
		firstPlay = false;
	    }

	    resume = false;
	}
	
	final Bin gsBin = bin;

	if (fadeoutStartTime > 0){
	    final PlayBinInfo info = playBinInfos.get(bin);
	    info.setPlayStartTime(System.currentTimeMillis());

	    TimerTask fadeoutTask = new TimerTask(){
		    public void run(){
			final Element volumeElement = getElementByType(gsBin, VOLUME);
			if (volumeElement == null)
			    return;
			final double startVolume = (Double) volumeElement.get("volume");
			final long stepTime = Math.max(PlaylistWidget.FADEOUT_DURATION / NUM_FADEOUT_STEPS, 350);
			final double deltaVolume = startVolume / (PlaylistWidget.FADEOUT_DURATION / stepTime);
			
			for (double volume = startVolume; volume > 0; volume -= deltaVolume){
			    try{
				Thread.sleep(stepTime);
			    } catch (Exception e){}
			    volumeElement.set("volume", Double.toString(volume));
			}
		    }
		};
	    Timer timer = new Timer("FadeoutTimer:"+key);
	    //timer.schedule(fadeoutTask, info.getFadeoutStartTime());
	    
	    info.setTimer(timer);
	}
	
	boolean retVal = true;

	if (!resume){
	    retVal = setState(bin, State.PLAYING);
	} else {
	    retVal = setState(getSink(bin), State.PLAYING);
	}

	if (retVal && (position > 0)) {
	    bin.getState();
	    RSPipeline.seekToPosition (rsBin,(int) position, key);
	}
	    

	/*
	Thread t = new Thread(){
		public void run(){
		    try{
			Thread.sleep(5000);
			System.err.println("dump dot file");
		    } catch (Exception e){}
		    pipe.toDotFile("playing");
		}
	    };
	t.start();
	*/

	return retVal;

    }

    public static boolean pause(RSBin rsBin, String key){
	Bin bin = playBins.get(key);
	
	PlayBinInfo info = playBinInfos.get(bin);

	if (info.getTimer() != null)
	    info.getTimer().cancel();

	long stopTime = System.currentTimeMillis();
	long fadeoutStartTime = Math.max(0, info.getFadeoutStartTime() - (stopTime - info.getPlayStartTime()));
	info.setFadeoutStartTime(fadeoutStartTime);

	
	if (bin != null)
	    return setState(getSink(bin), State.PAUSED);


	return false;
    }

    public static boolean stop(RSBin rsBin, String key){
	Bin bin = playBins.get(key);
	
	boolean retVal = false;
	if (bin != null)
	    retVal = setState(bin, State.NULL);
	
	removePlayBin(key);
	return retVal;
    }
    
    static final int GAIN_CHANGE_TIME_MS = 500;
    static final int GAIN_CHANGE_FREQ    = 5;

    //Changes the gain of the GSBin (i.e. volume element)
    public static boolean changePipeGain(final RSBin rsBin, final double deltaGain) {
	Thread t = new Thread(){
		public void run(){
		    double gainIncrement = deltaGain / GAIN_CHANGE_FREQ;

		    for (int i = 0; i < GAIN_CHANGE_FREQ; i++){
			for (PlayBinInfo binInfo: playBinInfos.values()){
			    if (binInfo.getRSBin() == rsBin){
				Bin gsBin = binInfo.getGSBin();
				Element volumeElement = getElementByType(gsBin, VOLUME);
				double currentVolume = (Double)volumeElement.get("volume");
				double netVolume = dBToLinear(gainIncrement + linearToDB(currentVolume));
				
				netVolume = clampVolume(netVolume);
				
				try {
				    volumeElement.set("volume", Double.toString(netVolume));
				    logger.debug("ChangePipeVolume: Pipe volume set to " + netVolume);
				} catch (Exception e){
				    logger.error("Set Volume: Could not set volume on bin: "+gsBin+" due to exception: ",e);
				}
			    }
			    
			}

			try{
			    Thread.sleep(GAIN_CHANGE_TIME_MS/GAIN_CHANGE_FREQ);
			} catch (Exception e){}

		    }
		}
	    };
	t.start();
	return true;
    }

    //Changes the volume field in RSBin and then changes the volume in the pipeline
    public static boolean changeGain(final RSBin rsBin, final double deltaGain) {
	final double currentVolume = rsBin.getVolume();
	double newVolume = dBToLinear(linearToDB(currentVolume) + deltaGain);

	newVolume = clampVolume(newVolume);
	
	rsBin.setVolume(newVolume);
	return changePipeGain(rsBin, deltaGain);
    }

    public static boolean setGain (RSBin rsBin, double gain){
	double newVolume = clampVolume (dBToLinear (gain));
	rsBin.setVolume(newVolume);
	return true;
    }

    public static boolean seekToPosition (RSBin rsBin , int position, String key){
	Bin gsBin = playBins.get(key);

	boolean retVal = gsBin.seek(1.0, Format.TIME, SeekFlags.ACCURATE | SeekFlags.FLUSH, SeekType.SET, (long)position * 1000L * 1000L, SeekType.NONE, -1L);
	gsBin.getState();
	setState(gsBin, State.PLAYING);

	return retVal;
    }

    public static int getPosition(RSBin rsBin, String key){
	Bin gsBin = playBins.get(key);
	
	PositionQuery positionQuery = new PositionQuery(Format.TIME);
	if (gsBin.query(positionQuery)){
	    int retVal = (int) (positionQuery.getPosition() / (1000 * 1000));
	    return retVal;
	} else {
	    return -1;
	}
    }

    static void removePlayBin(String key){
	if (key == null)
	    return;

	Bin gsBin = playBins.remove(key);
	audioListeners.remove(key);

	if (gsBin == null){
	    return;
	}
	setState(gsBin, State.NULL);
	//if (gsBin.getName().endsWith(BIN))
	pipe.remove(gsBin);
	rsBins.remove(gsBin);
	playBinKeys.remove(gsBin);
	playBinPool.setBinFree(gsBin);
	PlayBinInfo binInfo = playBinInfos.remove(gsBin);
	if (binInfo.getTimer() != null)
	    binInfo.getTimer().cancel();

    }

    public static boolean play(final RSBin bin){
	if (setState(bin,State.PLAYING)){
	    return true;
	}else {
	    setState(bin, State.NULL);
	    return false;
	}
    }

    public static boolean pause(RSBin bin){
	return setState(bin, State.PAUSED);
    }

    public static boolean stop(RSBin bin){
	return setState(bin, State.NULL);
    }

    public static boolean mute(RSBin bin){
	Bin binElement = bins.get(bin);
	Element volume = binElement.getElementByName(bin.getName()+VOLUME);
	try{
	    volume.set("mute", "true");
	    logger.debug("Mute: Muted bin: "+bin);
	    return true;
	}catch(Exception e){
	    logger.error("Mute: Could not mute bin "+bin);
	    return false;
	}
    }

    public static boolean unmute(RSBin bin){
	Bin binElement = bins.get(bin);
	Element volume = binElement.getElementByName(bin.getName()+VOLUME);
	try{
	    volume.set("mute", "false");
	    logger.debug("Unmute: Unmuted bin: "+bin);
	    return true;
	}catch(Exception e){
	    logger.error("Unmute: Could not unmute bin "+bin);
	    return false;
	}
    }
    
    public static boolean setVolume(RSBin bin, float vol){
	Bin binElement = bins.get(bin);
	if (binElement == null)
	    return false;
	Element volume = binElement.getElementByName(bin.getName()+VOLUME);
	if (volume == null)
	    return false;
	try{
	    volume.set("volume", vol);
	    logger.debug("SetVolume: Set volume of bin "+bin+" to "+vol);
	    return true;
	}catch(Exception e){
	    logger.error("SetVolume: Could not set Volume on bin "+bin);
	    return false;
	}
    }

    
    public static boolean isPlaying(RSBin bin){
	Bin binElement = bins.get(bin);
	if (binElement == null)
	    return false;
	return (binElement.getState() == State.PLAYING);
    }
    

    /*
    public static boolean isSinkPlaying(RSBin bin){
	Element sink = getSink(bins.get(bin));
	Pad sinkPad = sink.getStaticPad("sink");
	System.err.println("caps: "+sinkPad.getNegotiatedCaps());
	return false;
    }
    */

    public static Hashtable<String, String> getMetadata(final String fileName){
	final int MAX_WAIT_TIME = 5000;
	final Hashtable<String, String> retVals = new Hashtable<String, String>();

	try{
	    logger.debug("GetMetadata: "+fileName);
	    final Pipeline pipe = new Pipeline("duration");
	    final Element fileSrc = ElementFactory.make("filesrc", "filesrc");
	    fileSrc.set("location", fileName);
	    final DecodeBin decodeBin = new DecodeBin("decodeBin");
	    final Element audioConvert = ElementFactory.make("audioconvert","audioconvert");
	    final Element fakeSink = ElementFactory.make("fakesink", "fakesink");
	    final long[] duration = new long []{-1L};
	    final boolean[] state = new boolean[]{false};
	    final String[] errorMessage = new String[]{""};

	    decodeBin.connect(new DecodeBin.NEW_DECODED_PAD(){
		    public void newDecodedPad(Element e, Pad pad, boolean last){
			decodeBin.link(audioConvert);
		    }
		});

	    pipe.addMany(fileSrc, decodeBin, audioConvert, fakeSink);
	    pipe.linkMany(fileSrc, decodeBin);
	    pipe.linkMany(audioConvert, fakeSink);

	    Bus bus = pipe.getBus();
	    bus.connect(new Bus.STATE_CHANGED(){
		    public void stateChanged(GstObject source, State old, State cur, State pending){
			if (source == decodeBin && cur == State.PLAYING && 
			   (pending == State.PLAYING || pending == State.VOID_PENDING)){

			    duration[0] = pipe.queryDuration().toMillis();
			    synchronized(decodeBin){
				state[0] = true;
				decodeBin.notifyAll();
			    }
			    
			}
			synchronized(decodeBin){
			    if (source == fileSrc && cur == State.NULL && pending == State.VOID_PENDING){
				decodeBin.notifyAll();
			    }
			}
		    }
		});
	    
	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			logger.error("GetMetadata: "+source+" error: "+message+" for file: "+fileName);
			errorMessage[0] = message;
			synchronized(decodeBin){
			    duration[0] = -1L;
			    state[0] = true;
			    decodeBin.notifyAll();
			}
		    }
		});

	    bus.connect(new Bus.TAG(){
		    public void tagsFound(GstObject source, TagList tagList){
			for (String name: tagList.getTagNames()){
			    retVals.put(name, concatList(tagList.getValues(name)));
			}
		    }
		});
	    
	    pipe.setState(State.PLAYING);

	    synchronized(decodeBin){
		while (!state[0]){
		    try{
			decodeBin.wait(MAX_WAIT_TIME);
			state[0] = true;
		    } catch (Exception e){
			logger.error("GetMetadata: Thread interrupted.", e);
		    }
		}
	    }

	    if (errorMessage[0].equals("")) {
		logger.info("GetMetadata: file: "+fileName+" found duration: " + duration[0]);
	    } else {
		logger.error("GetMetadata: Got error. Duration:" + duration[0]);
	    }

	    pipe.setState(State.NULL);

	    synchronized(decodeBin) {
		while (pipe.getState() != State.NULL) {
		    try {
			decodeBin.wait(MAX_WAIT_TIME);
		    } catch (Exception e){
			logger.error("GetMetadata: Thread interrupted.", e);
		    }
		}
	    }

	    retVals.put(DURATION, Long.toString(duration[0]));
	    retVals.put(ERRORSTRING, errorMessage[0]);
	    
	} catch (Exception e){
	    logger.error("GetMetadata: Encountered exception: ", e);

	    retVals.put(DURATION, Long.toString(-1L));
	    retVals.put(ERRORSTRING, e.getMessage());
	}
	
	return retVals;
    }

    public static long getDuration (String fileName){
	return Long.parseLong(getMetadata(fileName).get(DURATION));
    }

    public static int[] getPeakFrequencies(String fileName){
	final int MAX_WAIT_TIME = 5000;
	long interval = Math.min(getDuration(fileName)/5, SPECTRUM_INTERVAL_MILLIS);

	logger.info("GetPeakFrequencies: Getting frequencies for file: " + fileName);
	try{
	    final Pipeline pipe = new Pipeline("peakFrequency");
	    
	    final Element fileSrc = ElementFactory.make("filesrc", "filesrc");
	    fileSrc.set("location", fileName);
	    final DecodeBin decodeBin = new DecodeBin("decode");
	    final Element resample = ElementFactory.make(resampler, resampler);
	    
	    decodeBin.connect(new DecodeBin.NEW_DECODED_PAD(){
		    public void newDecodedPad(Element e, Pad pad, boolean last){
			decodeBin.link(resample);
		    }
		});

	    final Element capsFilter = ElementFactory.make("capsfilter", "capsfilter");
	    capsFilter.set("caps", Caps.fromString("audio/x-raw-int, rate=" + FREQ_ANALYSIS_SAMPLE_RATE));

	    final Element spectrum = ElementFactory.make("spectrum", "spectrum");
	    spectrum.set("bands", NUM_SPECTRUM_BANDS);
	    spectrum.set("interval", Long.toString(/*SPECTRUM_INTERVAL_MILLIS*/interval * 1000L * 1000L));

	    final Element fakeSink = ElementFactory.make("fakesink", "fakesink");
	    
	    pipe.addMany(fileSrc, decodeBin, resample, capsFilter, spectrum, fakeSink);
	    pipe.linkMany(fileSrc, decodeBin);
	    pipe.linkMany(resample, capsFilter, spectrum, fakeSink);
	    
	    Bus bus = pipe.getBus();

	    final ArrayList<Integer> peaksList = new ArrayList<Integer>();

	    bus.connect(new Bus.MESSAGE(){
		    public void busMessage(Bus bus, Message message){
			if (message.getType() == MessageType.ELEMENT){
			    float[] magnitudes = getMagnitudes(message);
			    int peakIndex = -1;
			    float peakValue = -61.0f;
			    for (int i = 0; i < magnitudes.length; i++)
				if (magnitudes[i] > peakValue){
				    peakIndex = i;
				    peakValue = magnitudes[i];
				}
			    
			    peaksList.add(peakIndex);
			} 
		    }
		});

	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			synchronized(decodeBin){
			    logger.error("BusError: " + message);
			    decodeBin.notifyAll();
			}
		    }
		});

	    bus.connect(new Bus.EOS(){
		    public void endOfStream(GstObject source){
			synchronized(decodeBin){
			    decodeBin.notifyAll();
			}
		    }
		});


	    synchronized(decodeBin){
		pipe.setState(State.PLAYING);
		
		try{
		    decodeBin.wait(MAX_WAIT_TIME);
		} catch (Exception e){
		    logger.error("GetPeakFrequencies: Got exception at decodebin wait.");
		}
	    }

	    pipe.setState(State.NULL);

	    int[] retVals = new int[peaksList.size()];

	    for (int i = 0; i < retVals.length; i++){
		retVals[i] = peaksList.get(i);
	    }

	    return retVals;
	} catch (Exception e) {

	    logger.error("Unable to calculate peak frequencies for: "+fileName, e);
	    return new int[] {};
	}

    }

    static float[] getMagnitudes(Message message){
	final int STRIDE = 5;
	final int FLOAT_SIZE = 4;
	final int bufferSize = 8 + STRIDE * FLOAT_SIZE * (NUM_SPECTRUM_BANDS - 1);

	byte[] bytes = message.getStructure().getValue("magnitude").data[0].v_pointer.getPointer(0).getByteArray(0, bufferSize);
	ByteBuffer buff = ByteBuffer.wrap(bytes);
	buff.order (ByteOrder.LITTLE_ENDIAN);

	float[] magnitudes = new float [NUM_SPECTRUM_BANDS];
	
	for (int i = 0; i < NUM_SPECTRUM_BANDS; i++){
	    magnitudes[i] = buff.getFloat(FLOAT_SIZE * ( 1 + STRIDE*i));
	}

	return magnitudes;
    }

    public static int[] getLevels(String fileName){
	final int MAX_WAIT_TIME = 5000;
	
	try{
	    logger.debug("GetLevels: For fileName: "+fileName);
	    
	    final Pipeline pipe = new Pipeline("Levels");
	    final Element fileSrc = ElementFactory.make("filesrc", "filesrc");
	    fileSrc.set("location", fileName);

	    final DecodeBin decodeBin = new DecodeBin("decode");
	    final Element audioConvert = ElementFactory.make("audioconvert", "audioconvert");
	    
	    decodeBin.connect(new DecodeBin.NEW_DECODED_PAD(){
		    public void newDecodedPad(Element e, Pad pad, boolean last){
			decodeBin.link(audioConvert);
		    }
		});
	    

	    long interval = Math.min(getDuration(fileName)/3, LEVEL_INTERVAL_MILLIS);

	    final Element level = ElementFactory.make("level", "level");
	    level.set("interval", Long.toString(interval * 1000L * 1000L));

	    final Element fakeSink = ElementFactory.make("fakesink", "fakesink");

	    pipe.addMany(fileSrc, decodeBin, audioConvert, level, fakeSink);
	    pipe.linkMany(fileSrc, decodeBin);
	    pipe.linkMany(audioConvert, level, fakeSink);

	    final Bus bus = pipe.getBus();

	    bus.connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int code, String message){
			logger.error("GetLevels: Received error: "+message);
			synchronized(decodeBin){
			    decodeBin.notifyAll();
			}
		    }
		});

	    bus.connect(new Bus.EOS(){
		    public void endOfStream(GstObject source){
			synchronized(decodeBin){
			    decodeBin.notifyAll();
			}
		    }
		});

	    final ArrayList<Double> levelList = new ArrayList<Double>();
	    final int[] numChannels = new int[] {1}; 		//Init to 1 as fallback
	    final boolean[] channelsDetected = new boolean[] {false};

	    bus.connect(new Bus.STATE_CHANGED(){
		    public void stateChanged(GstObject source, State old, State cur, State pending){
			if (source == decodeBin && cur == State.PLAYING && 
			   (pending == State.VOID_PENDING)){
			    numChannels[0] = decodeBin.getSrcPads().get(0).getCaps().getStructure(0).getInteger("channels");
			    channelsDetected[0] = true;

			}
		    }
		});

	    bus.connect(new Bus.MESSAGE(){
		    public void busMessage(Bus bus, Message message){
			if (message.getType() == MessageType.ELEMENT && message.getStructure().hasField("rms")){

			    if (!channelsDetected[0]){
				return;
			    }

			    levelList.add(getMaxLevel(message, numChannels[0]));
			}
		    }
		});


	    synchronized(decodeBin){
		pipe.setState(State.PLAYING);
		long startTime = System.currentTimeMillis();
		decodeBin.wait(MAX_WAIT_TIME);
	    }

	    pipe.setState(State.NULL);

	    int[] retVals = new int[levelList.size()];

	    for (int i = 0; i < levelList.size(); i++){
		retVals[i] = (int) levelList.get(i).longValue();
	    }

	    return retVals;
	} catch (Exception e){
	    logger.error("GetLevels: Unable to calculate levels for: "+fileName, e);
	    return new int[]{};
	}
	
    }

    static double getMaxLevel(Message message, int numChannels){

	final int STRIDE = 1;
	final int DOUBLE_SIZE = 8;
	final int PADDING_SIZE = 4;

	final int bufferSize = (PADDING_SIZE + DOUBLE_SIZE) * numChannels + (STRIDE * DOUBLE_SIZE) * (numChannels - 1);

	byte[] bytes = message.getStructure().getValue("rms").data[0].v_pointer.getPointer(0).getByteArray(0, bufferSize);
	ByteBuffer buff = ByteBuffer.wrap(bytes);
	buff.order (ByteOrder.LITTLE_ENDIAN);

	double[] levels = new double [numChannels];
	
	for (int i = 0; i < numChannels; i++){
	    levels[i] = buff.getDouble((i+1)*PADDING_SIZE + i * (STRIDE * DOUBLE_SIZE) + i * DOUBLE_SIZE);
	}
	
	double maxLevel = Double.NEGATIVE_INFINITY;

	for (double level: levels){
	    maxLevel = (level > maxLevel) ? level : maxLevel;
	}

	return maxLevel;
    }
    
    protected static String concatList(List<Object> list){
	StringBuilder str = new StringBuilder();
	for (Object o: list)
	    str.append(o.toString() + ", ");

	if (str.length() >= 2)
	    str.delete(str.length()-2, str.length());
	return str.toString();
    }

}

class PlayBinPool {
    Hashtable<Bin, Boolean> playoutPool;
    Hashtable<Bin, Boolean> previewPool;

    static int MAX_BINS_PLAYOUT = 10;
    static int MAX_BINS_PREVIEW = 1;

    public PlayBinPool() {
	Bin bin;

	playoutPool = new Hashtable<Bin, Boolean>();
	previewPool = new Hashtable<Bin, Boolean>();

	for(int i = 0; i < MAX_BINS_PLAYOUT; i++) {
	    bin = RSPipeline.newPlayBin(RSBin.PLAYOUT, i);
	    playoutPool.put(bin, true);
	}

	for(int i = 0; i < MAX_BINS_PREVIEW; i++) {
	    bin = RSPipeline.newPlayBin(RSBin.PREVIEW, i);
	    previewPool.put(bin, true);
	}
    }

    Bin getFreeBin(RSBin rsBin) {
	Hashtable<Bin, Boolean> pool;

	if(rsBin == RSBin.PLAYOUT)
	    pool = playoutPool;
	else
	    pool = previewPool;

	for(Bin bin: pool.keySet()) {
	    if(pool.get(bin)) {
		pool.put(bin, false);
		return bin;
	    }
	}

	return null;
    }

    void setBinFree(Bin bin) {
	if(playoutPool.get(bin) != null) {
	    playoutPool.put(bin, true);
	    return;
	} else if(previewPool.get(bin) != null) {
		previewPool.put(bin, true);
		return;
	}
    }
}


class PlayBinInfo {
    Bus.STATE_CHANGED stateListener;
    Bus.EOS eosListener;
    Bus.ERROR errorListener;
    Bus.TAG tagListener;

    double volume = 1.0;
    RSBin rsBin;
    Bin gsBin;
    
    long fadeoutStartTime = -1;
    long playStartTime;
    long playedDuration = 0;
    
    Timer timer;

    //  PlayBinInfo(Bus.STATE_CHANGED stateListener, Bus.EOS eosListener, Bus.ERROR errorListener, Bus.TAG tagListener){

    public PlayBinInfo(RSBin rsBin, Bin gsBin, long fadeoutStartTime){
	this.rsBin = rsBin;
	this.gsBin = gsBin;
	this.fadeoutStartTime = fadeoutStartTime;
    }
    
    public Bus.STATE_CHANGED getStateListener(){
	return stateListener;
    }
    
    public void setStateListener(Bus.STATE_CHANGED stateListener){
	this.stateListener = stateListener;
    }
    
    public Bus.EOS getEOSListener(){
	return eosListener;
    }

    public void setEOSListener(Bus.EOS eosListener){
	this.eosListener = eosListener;
    }
    
    public Bus.ERROR getErrorListener(){
	return errorListener;
    }
    
    public void setErrorListener(Bus.ERROR errorListener){
	this.errorListener = errorListener;
    }
    
    public Bus.TAG getTagListener(){
	return tagListener;
    }

    public void setTagListener(Bus.TAG tagListener){
	this.tagListener = tagListener;
    }
    
    public double getVolume(){
	return volume;
    }

    public void setVolume(double volume){
	this.volume = volume;
    }

    public RSBin getRSBin(){
	return rsBin;
    }

    public Bin getGSBin(){
	return gsBin;
    }

    public long getPlayStartTime(){
	return playStartTime;
    }
    
    public void setPlayStartTime(long playStartTime){
	this.playStartTime = playStartTime;
    }

    public long getFadeoutStartTime(){
	return fadeoutStartTime;
    }

    public void setFadeoutStartTime(long fadeoutStartTime){
	this.fadeoutStartTime = fadeoutStartTime;
    }

    public Timer getTimer(){
	return timer;
    }

    public void setTimer(Timer timer){
	this.timer = timer;
    }
}
