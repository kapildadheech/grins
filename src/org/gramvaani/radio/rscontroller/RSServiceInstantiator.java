package org.gramvaani.radio.rscontroller;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.singleinstance.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.stationconfig.*;

import java.util.*;
import java.lang.reflect.*;

import gnu.getopt.Getopt;

public class RSServiceInstantiator {

    public static void main(String args[]) {
	Hashtable<String, IPCNode> localServices = new Hashtable<String, IPCNode>();
	LogUtilities logger = new LogUtilities("RSServiceInstantiator");

	Getopt g = new Getopt("IndexService", args, "c:m:");
	int c;
	String configFilePath = "./automation.conf";
	String machineID = "127.0.0.1";
	while((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c':
		configFilePath = g.getOptarg();
		break;
	    case 'm':
		machineID = g.getOptarg();
		break;
	    }
	}

	//LogUtilities.addShutdownHook();

	logger.debug("Using machine ID: "+machineID);
	StationConfiguration stationConfiguration = new StationConfiguration(machineID);
	stationConfiguration.readConfig(configFilePath);

	if(!stationConfiguration.isValidInstance()) {
	    logger.error("Invalid instance. Cannot continue.");
	    System.exit(RSController.INVALID_INSTANCE_EXIT_CODE);
	}
	
	LogUtilities.logToFile(machineID, stationConfiguration.getStringParam(StationConfiguration.LOG_FILE_SIZE));

	ArrayList<Service> localServiceList = new ArrayList<Service>();

	for(String SERVICE: RSController.SERVICES) {
	    if(SERVICE.equals(RSController.UI_SERVICE))
		continue;

	    Service[] services = stationConfiguration.getServices(SERVICE);
	    logger.debug("InitLocalServices: "+services.length+" services found of type "+SERVICE+".");
	    for(Service service: services) {
		logger.debug(service.machineID());
		if(service.machineID().equals(machineID)) {
		    localServiceList.add(service);
		} 
	    }
	}
	
	String instanceKey = StringUtilities.getCSVFromIterable(localServiceList);
	
	if (!SingleInstanceManager.registerInstance(instanceKey)) {
	    LogUtilities.getDefaultLogger().fatal("Another instance of RSI is already running with key"+instanceKey+".  Exiting.");
	    System.exit(RSController.MULTIPLE_INSTANCE_EXIT_CODE);
	}

	
	for (Service service: localServiceList){
	    try {
		String serviceInstanceName = service.serviceInstanceName();
		Class<?> localServiceClass = Class.forName(service.serviceClass());
		Constructor localServiceConstructor = localServiceClass.getConstructor(StationConfiguration.class,
										       String.class,
										       String.class);
		IPCNode localService = (IPCNode)(localServiceConstructor.newInstance(new Object[]{stationConfiguration,
												  serviceInstanceName,
												  machineID}));
		localServices.put(serviceInstanceName, localService);
		logger.debug("InitLocalServices: Local Service: " + serviceInstanceName);
	    } catch(Exception e) {
		logger.error("InitLocalServices: Could not instantiate service: " + service.serviceInstanceName(), e);
	    }
	}
    }

}