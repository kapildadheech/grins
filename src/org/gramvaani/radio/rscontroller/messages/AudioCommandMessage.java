package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.StringUtilities;

import java.util.Hashtable;

public class AudioCommandMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, session = session id, command = particular command (play, pause, stop), fileid = fileid

    public AudioCommandMessage(String source, String dest, String widgetName, 
			       String sessionId, String command, 
			       String fileId, String playType, Hashtable<String,String> options) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	if (sessionId == null)
	    sessionId = "";
	params.put("sessionid", sessionId);
	params.put("fileid", fileId);
	params.put("audiocommand", command);
	params.put("playtype", playType);
	params.put("options", StringUtilities.getCSVFromHashtable(options));
    }
   
    public AudioCommandMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getSessionID() {
	return params.get("sessionid");
    }

    public String getCommand() {
	return params.get("audiocommand");
    }
    
    public String getFileID() {
	return params.get("fileid");
    }

    public String getPlayType() {
	return params.get("playtype");
    }

    public Hashtable<String, String> getOptions(){
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }

    public static boolean isAudioCommandMessage(String messageString) {
	if(messageString.indexOf("\naudiocommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
