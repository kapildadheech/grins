package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ServiceReadyAckMessage extends IPCSyncResponseMessage {

    // source = uiService, dest = resourceManager, widgetName = widgetName, service = interested services
    public ServiceReadyAckMessage(String source, String dest, boolean servicestatus, String messageID) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("ack", "true");
	params.put("servicestatus", new Boolean(servicestatus).toString());
	setMessageID(messageID);
    }
   
    public ServiceReadyAckMessage(String messageString) {
	super(messageString);
    }

    public boolean getStatus() {
	return Boolean.parseBoolean(params.get("servicestatus"));
    }

    public static boolean isServiceReadyAckMessage(String messageString) {
	if((messageString.indexOf("\nservicestatus:") != -1) 
	   && (messageString.indexOf("\nack:") != -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}
