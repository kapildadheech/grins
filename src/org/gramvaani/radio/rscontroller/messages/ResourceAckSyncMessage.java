
package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ResourceAckSyncMessage extends IPCSyncResponseMessage {

    // source = resourceManager, dest = serviceName, success = successfull resources, failed = failed resources
    public ResourceAckSyncMessage(String source, String dest, String[] successResources, String[] successfulResourceRoles,
				  String[] failedResources, String[] failedResourceRoles, String messageID) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("syncsuccessfulresources", StringUtilities.getCSVFromArray(successResources));
	params.put("syncsuccessfulresourceroles", StringUtilities.getCSVFromArray(successfulResourceRoles));
	params.put("syncfailedresources", StringUtilities.getCSVFromArray(failedResources));	
	params.put("syncfailedresourceroles", StringUtilities.getCSVFromArray(failedResourceRoles));
	setMessageID(messageID);
    }
   
    public ResourceAckSyncMessage(String messageString) {
	super(messageString);
    }

    public String[] getSuccessfulResources() {
	return StringUtilities.getArrayFromCSV(params.get("syncsuccessfulresources"));
    }

    public String[] getFailedResources() {
	return StringUtilities.getArrayFromCSV(params.get("syncfailedresources"));
    }

    public String[] getSuccessfulResourceRoles() {
	return StringUtilities.getArrayFromCSV(params.get("syncsuccessfulresourceroles"));
    }

    public String[] getUnsuccessfulResourceRoles() {
	return StringUtilities.getArrayFromCSV(params.get("syncfailedresourceroles"));
    }

    public static boolean isResourceAckSyncMessage(String messageString) {
	if(messageString.indexOf("\nsyncsuccessfulresources:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}