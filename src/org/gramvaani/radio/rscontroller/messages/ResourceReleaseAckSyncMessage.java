package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ResourceReleaseAckSyncMessage extends IPCSyncResponseMessage {

    // source = serviceName, dest = resourceManager, resources = resources
    public ResourceReleaseAckSyncMessage(String source, String dest, String[] successfulResources, String[] failedResources, String messageID) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("syncsuccessfulrelease", StringUtilities.getCSVFromArray(successfulResources));
	params.put("syncfailedrelease", StringUtilities.getCSVFromArray(failedResources));
	setMessageID(messageID);
    }
   
    public ResourceReleaseAckSyncMessage(String messageString) {
	super(messageString);
    }

    public String[] getSuccessfulResources() {
	return StringUtilities.getArrayFromCSV(params.get("syncsuccessfulrelease"));
    }
    
    public String[] getFailedResources() {
	return StringUtilities.getArrayFromCSV(params.get("syncfailedrelease"));
    }

    public static boolean isResourceReleaseAckSyncMessage(String messageString) {
	if(messageString.indexOf("\nsyncsuccessfulrelease:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}