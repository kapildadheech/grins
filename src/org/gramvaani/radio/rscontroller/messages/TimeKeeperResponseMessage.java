package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;

public class TimeKeeperResponseMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService, realTime = realTime
    public TimeKeeperResponseMessage(String source, String dest, long realTime) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("realtime", Long.toString(realTime));
    }

    public TimeKeeperResponseMessage(String messageString) {
	super(messageString);
    }

    public long getRealTime() {
	return Long.parseLong(params.get("realtime"));
    }

    public static boolean isTimeKeeperResponseMessage(String messageString) {
	if(messageString.indexOf("\nrealtime:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
