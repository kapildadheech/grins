package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class PreemptResourceAckMessage extends IPCMessage {

    // source = serviceName, dest = resourceManager, ack=true/false, resources = resources
    public PreemptResourceAckMessage(String source, String dest, String ack, String[] resources) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("preemptresourceack",ack);
	params.put("preemptresource", StringUtilities.getCSVFromArray(resources));
    }
   
    public PreemptResourceAckMessage(String messageString) {
	super(messageString);
    }

    public String[] getResources() {
	return StringUtilities.getArrayFromCSV(params.get("preemptresource"));
    }

    public String getAck() {
	return params.get("preemptresourceack");
    }

    public static boolean isPreemptResourceAckMessage(String messageString) {
	if(messageString.indexOf("\npreemptresourceack:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}