package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.StringUtilities;

import java.util.Hashtable;

public class TelephonyEventMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, event = particular event (newcall, callhungup), options = options specific to the command

    //Events
    public static final String NEW_CALL 	= "NEW_CALL";
    public static final String CALLER_HUNG_UP 	= "CALLER_HUNG_UP";
    public static final String OUTGOING_CALL_UPDATE = "OUTGOING_CALL_UPDATE";
    public static final String ATA_REGISTERED 	= "ATA_REGISTERED";
    public static final String ATA_UNREGISTERED	= "ATA_UNREGISTERED";
    public static final String PHONE_LINE_OK	= "PHONE_LINE_OK";
    public static final String PHONE_LINE_ERROR	= "PHONE_LINE_ERROR";


    //Options
    public static final String CALL_GUID 	= "CALL_GUID";
    public static final String CALLER_ID 	= "CALLER_ID";
    public static final String CALL_STATE 	= "CALL_STATE";
    public static final String HANGUP_CAUSE 	= "HANGUP_CAUSE";
    public static final String TEMP_CALL_GUID 	= "TEMP_CALL_GUID"; //used for outgoing calls till actual GUID is obtained
    public static final String ERROR 		= "ERROR";
    public static final String MONITOR_FILE_NAME= "MONITOR_FILE_NAME";
    public static final String CHANNEL_NO 	= "CHANNEL_NO";
    public static final String ATA_ID 	        = "ATA_ID";
    public static final String PHONE_LINE_ID    = "PHONE_LINE_ID";

    public TelephonyEventMessage(String source, String dest, String widgetName, 
				 String event, Hashtable<String,String> options) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("widgetname",widgetName);
	params.put("telephonyevent", event);
	params.put("options", StringUtilities.getCSVFromHashtable(options));
    }
   
    public TelephonyEventMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getEvent() {
	return params.get("telephonyevent");
    }
    
    public Hashtable<String,String> getOptions(){
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }

    public static boolean isTelephonyEventMessage(String messageString) {
	if(messageString.indexOf("\ntelephonyevent:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}