package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class RegisterMessage extends IPCMessage {

    public RegisterMessage(String source, String registrationID) {
	super(source, "");
	messageType = IPCMessage.REGISTER_MESSAGE;
	params.put("registrationid",registrationID);
    }
   
    public RegisterMessage(String messageString) {
	super(messageString);
    }

    public String getRegistrationID() {
	return params.get("registrationid");
    }

    public static boolean isRegisterMessage(String messageString) {
	if(messageString.indexOf("\nregistrationid:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }


    
}