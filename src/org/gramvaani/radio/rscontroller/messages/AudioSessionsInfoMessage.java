package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.StringUtilities;

import java.util.ArrayList;

public class AudioSessionsInfoMessage extends IPCMessage {

    // general use: source = AudioService, dest = UIService
    public AudioSessionsInfoMessage(String source, String dest, String[] sessionIDs, 
				    String[] sessionStates, String[] programIDs, Integer[] positions) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("sessionids", StringUtilities.getCSVFromArray(sessionIDs));
	params.put("sessionstates", StringUtilities.getCSVFromArray(sessionStates));
	params.put("programids", StringUtilities.getCSVFromArray(programIDs));
	params.put("positions", StringUtilities.getCSVFromArray(positions));
    }
   
    public AudioSessionsInfoMessage(String messageString) {
	super(messageString);
    }

    public String[] getSessionIDs() {
	return StringUtilities.getArrayFromCSV(params.get("sessionids"));
    }

    public String[] getSessionStates() {
	return StringUtilities.getArrayFromCSV(params.get("sessionstates"));
    }

    public String[] getProgramIDs() {
	return StringUtilities.getArrayFromCSV(params.get("programids"));
    }

    public Integer[] getPositions() {
	ArrayList<Integer> list = new ArrayList<Integer>();
	String[] positionStr = StringUtilities.getArrayFromCSV(params.get("positions"));

	for (String str: positionStr){
	    try{
		list.add(Integer.parseInt(str));
	    } catch (Exception e){
		logger.error("Couldn't parse position: "+str, e);
		list.add(-1);
	    }
	}

	return list.toArray(new Integer[]{});
    }

    public static boolean isAudioSessionsInfoMessage(String messageString) {
	if((messageString.indexOf("\nsessionids:") != -1) &&
	   (messageString.indexOf("\nsessionstates:") != -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}