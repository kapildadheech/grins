
package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ServiceNotifyMessage extends IPCMessage {

    // source = resourceManager, dest = uiService, widgetName = widgetName, activatedServices, inactivatedServices
    public ServiceNotifyMessage(String source, String dest, String widgetName, String[] activatedServices, String[] activatedIDs, String[] inactivatedServices) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname", widgetName);
	params.put("activatedservices", StringUtilities.getCSVFromArray(activatedServices));
	params.put("activatedids", StringUtilities.getCSVFromArray(activatedIDs)); 
	params.put("inactivatedservices", StringUtilities.getCSVFromArray(inactivatedServices));
    }
   
    public ServiceNotifyMessage(String messageString) {
	super(messageString);
    }

    public String[] getActivatedServices() {
	return StringUtilities.getArrayFromCSV(params.get("activatedservices"));
    }
    
    public String[] getActivatedIDs(){
	return StringUtilities.getArrayFromCSV(params.get("activatedids"));
    }

    public String[] getInactivatedServices() {
	return StringUtilities.getArrayFromCSV(params.get("inactivatedservices"));
    }

    public static boolean isServiceNotifyMessage(String messageString) {
	if(messageString.indexOf("\nactivatedids:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }


}