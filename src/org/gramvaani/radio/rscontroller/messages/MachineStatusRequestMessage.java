package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.StringUtilities;

public class MachineStatusRequestMessage extends IPCMessage {

    public MachineStatusRequestMessage(String source, String dest, String widgetName, 
					String command, String[] options){

	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	params.put("machinestatuscommand", command);
	params.put("options", StringUtilities.getCSVFromArray(options));
    }
   
    public MachineStatusRequestMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getCommand() {
	return params.get("machinestatuscommand");
    }
    
    public String[] getOptions(){
	return StringUtilities.getArrayFromCSV(params.get("options"));
    }

    public static boolean isMachineStatusRequestMessage(String messageString) {
	if(messageString.indexOf("\nmachinestatuscommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
