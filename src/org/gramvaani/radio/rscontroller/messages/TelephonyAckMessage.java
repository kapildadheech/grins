package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

import java.util.Hashtable;

public class TelephonyAckMessage extends IPCMessage {
    // general use: sent from TelephonyService to widget

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String PROGRAM_ID = "PROGRAM_ID";
    public static final String TELECAST_TIME = "'TELECAST_TIME";

    public TelephonyAckMessage(String source, String dest, String widgetName, 
			       Hashtable<String,String> options, String ack, 
			       String command, String... error) {

	super(source, dest);

	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("options", StringUtilities.getCSVFromHashtable(options));
	params.put("telephonyack", ack);
	params.put("command", command);
	params.put("error", StringUtilities.getCSVFromArray(error));
    }
   
    public TelephonyAckMessage(String messageString) {
	super(messageString);
    }

    public String[] getError() {
	String errorString = params.get("error");
	if(errorString != null)
	    return StringUtilities.getArrayFromCSV(errorString);
	else
	    return (new String[] {});
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public Hashtable<String,String> getOptions() {
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }

    public String getAck() {
	return params.get("telephonyack");
    }
    
    public String getCommand() {
	return params.get("command");
    }

    public static boolean isTelephonyAckMessage(String messageString) {
	if(messageString.indexOf("\ntelephonyack:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
