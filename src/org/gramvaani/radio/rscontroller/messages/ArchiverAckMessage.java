package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class ArchiverAckMessage extends IPCMessage {

    // general use: sent from ArchiverService to widget

    public ArchiverAckMessage(String source, String dest, String widgetName, 
			      String ack, String command) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("archiverack", ack);
	params.put("command", command);
	params.put("error","");
    }
   
    public ArchiverAckMessage(String source, String dest, String widgetName, 
			      String ack, String command, String[] error) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("archiverack", ack);
	params.put("command", command);
	params.put("error",StringUtilities.getCSVFromArray(error));
    }

    public ArchiverAckMessage(String messageString) {
	super(messageString);
    }

    public String[] getError() {
	return StringUtilities.getArrayFromCSV(params.get("error"));
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getAck() {
	return params.get("archiverack");
    }

    public String getCommand() {
	return params.get("command");
    }

    public static boolean isArchiverAckMessage(String messageString) {
	if(messageString.indexOf("\narchiverack:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}