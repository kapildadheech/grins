package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ServiceReadyMessage extends IPCSyncMessage {

    // source = uiService, dest = resourceManager, widgetName = widgetName, service = interested services
    public ServiceReadyMessage(String source, String dest, boolean serviceStatus) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	//params.put("servicestatus", new Boolean(serviceStatus).toString());
	params.put("servicestatus", Boolean.toString(serviceStatus));
    }
   
    public ServiceReadyMessage(String messageString) {
	super(messageString);
    }

    public boolean getStatus() {
	return Boolean.parseBoolean(params.get("servicestatus"));
    }

    /*
    public void setOptions(Hashtable<String, String> options){
	params.put("options", StringUtilities.getCSVFromHashtable(options));
    }

    public Hashtable<String,String> getOptions(){
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }
    */

    public static boolean isServiceReadyMessage(String messageString) {
	if((messageString.indexOf("\nservicestatus:") != -1) 
	   && (messageString.indexOf("\nack:") == -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}
