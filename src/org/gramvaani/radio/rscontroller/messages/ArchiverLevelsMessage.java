package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class ArchiverLevelsMessage extends IPCMessage {

    // general use: sent from MonitorService to widget

    public ArchiverLevelsMessage(String source, String dest, String widgetName, 
			   String ack, String[] frequencies) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("archiverlevelsack", ack);
	params.put("archiverlevels", StringUtilities.getCSVFromArray(frequencies));
    }
   
    public ArchiverLevelsMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getAck() {
	return params.get("archiverlevelsack");
    }

    public String[] getLevels() {
	return StringUtilities.getArrayFromCSV(params.get("archiverlevels"));
    }

    public static boolean isArchiverLevelsMessage(String messageString) {
	if(messageString.indexOf("\narchiverlevels:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }
}