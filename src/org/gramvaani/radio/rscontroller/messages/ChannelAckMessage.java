package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;

public class ChannelAckMessage extends IPCMessage {

    // general use: sent from ChannelService to client

    public ChannelAckMessage(String source, String dest, String ack, String command) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("channelack", ack);
	params.put("command", command);
    }
   
    public ChannelAckMessage(String messageString) {
	super(messageString);
    }

    public String getAck() {
	return params.get("channelack");
    }

    public String getCommand() {
	return params.get("command");
    }

    public static boolean isChannelAckMessage(String messageString) {
	if(messageString.indexOf("\nchannelack:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}