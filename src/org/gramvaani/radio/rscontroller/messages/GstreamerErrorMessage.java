package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;

public class GstreamerErrorMessage extends IPCMessage {

    // general use: sent from Archiver, Mic and Monitor Services to widget

    public GstreamerErrorMessage(String source, String dest, String widgetName, String error) {
	super(source, dest);
	messageType = IPCMessage.ERROR_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("gstreamererror", error);
    }
   
    public GstreamerErrorMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getError() {
	return params.get("gstreamererror");
    }

    public static boolean isGstreamerErrorMessage(String messageString) {
	if(messageString.indexOf("\ngstreamererror:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
