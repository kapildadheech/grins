package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class StatusCheckRequestMessage extends IPCMessage {

    public StatusCheckRequestMessage(String source, String dest, String widgetName, String id) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname", widgetName);
	params.put("statuscheckrequestid", id);
    }
   
    public StatusCheckRequestMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getID() {
	return params.get("statuscheckrequestid");
    }

    public static boolean isStatusCheckRequestMessage(String messageString) {
	if(messageString.indexOf("\nstatuscheckrequestid:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}