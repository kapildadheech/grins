package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class AudioErrorMessage extends IPCMessage {

    // general use: sent from AudioService to widget

    public AudioErrorMessage(String source, String dest, String widgetName, 
			     String sessionId, String itemId, String[] options, String error) {
	super(source, dest);
	messageType = IPCMessage.ERROR_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("sessionid", sessionId);
	params.put("itemid", itemId);
	params.put("options", StringUtilities.getCSVFromArray(options));
	params.put("audioerror", error);
    }
   
    public AudioErrorMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getSessionID() {
	return params.get("sessionid");
    }

    public String getItemID() {
	return params.get("itemid");
    }

    public String[] getOptions() {
	return StringUtilities.getArrayFromCSV(params.get("options"));
    }

    public String getError() {
	return params.get("audioerror");
    }

    public static boolean isAudioErrorMessage(String messageString) {
	if(messageString.indexOf("\naudioerror:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
