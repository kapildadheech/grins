package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.StringUtilities;

import java.util.Hashtable;

public class TelephonyCommandMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, command = particular command (goonline, gooffline, accept), options = options specific to the command

    //commands
    public static final String DIAL         = "DIAL";
    public static final String ACCEPT_CALL  = "ACCEPT_CALL";
    public static final String PREVIEW_CALL = "PREVIEW_CALL";
    public static final String HOLD_CALL    = "HOLD_CALL";
    public static final String HANGUP_CALL  = "HANG_UP";
    public static final String MUTE_CALL    = "MUTE_CALL";
    public static final String UNMUTE_CALL  = "UNMUTE_CALL";

    public static final String ENABLE_CONF  = "ENABLE_CONF";
    public static final String DISABLE_CONF = "DISABLE_CONF";

    public static final String GO_ONAIR     = "GO_ONAIR";
    public static final String GO_OFFAIR    = "GO_OFFAIR";
    public static final String GO_ONLINE    = "GO_ONLINE";
    public static final String GO_OFFLINE   = "GO_OFFLINE";
    public static final String DISCONNECT   = "DISCONNECT";

    public static final String DESTROY_ASTERISK_CHANNELS = "DESTROY_ASTERISK_CHANNELS";

    //options
    public static final String CALL_GUID      = "CALL_GUID";
    public static final String CALLER_ID      = "CALLER_ID";
    public static final String TEMP_CALL_GUID = "TEMP_CALL_GUID";
    public static final String IS_CONFERENCE  = "IS_CONFERENCE";
    public static final String MONITOR_FILE_NAME  = "MONITOR_FILE_NAME";    
    
    //boolean values
    public static final String TRUE           = "TRUE";
    public static final String FALSE          = "FALSE";


    public TelephonyCommandMessage(String source, String dest, String widgetName, 
				   String command, Hashtable<String, String> options) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	params.put("telephonycommand", command);
	params.put("options", StringUtilities.getCSVFromHashtable(options));
    }
   
    public TelephonyCommandMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getCommand() {
	return params.get("telephonycommand");
    }
    
    public Hashtable<String,String> getOptions(){
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }

    public static boolean isTelephonyCommandMessage(String messageString) {
	if(messageString.indexOf("\ntelephonycommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}