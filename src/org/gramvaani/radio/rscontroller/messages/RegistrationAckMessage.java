package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

public class RegistrationAckMessage extends IPCMessage {

    public RegistrationAckMessage(String source, String dest, String registrationID, Boolean ack) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("registrationid", registrationID);
	params.put("ack", ack.toString());
    }
   
    public RegistrationAckMessage(String messageString) {
	super(messageString);
    }

    public String getRegistrationID() {
	return params.get("registrationid");
    }
    
    public boolean getAck(){
	return Boolean.parseBoolean(params.get("ack"));
    }

    public static boolean isRegistrationAckMessage(String messageString) {
	if ((messageString.indexOf("\nregistrationid:") != -1) && 
	   (messageString.indexOf("\nack:") != -1)){
	    return true;
	} else {
	    return false;
	}
    }
}