package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ResourceRequestMessage extends IPCMessage {

    // source = serviceName, dest = resourceManager, resources = resources
    public ResourceRequestMessage(String source, String dest, String[] resources, String[] resourceRoles, 
				  boolean persistentRequest) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("resourcerequest", StringUtilities.getCSVFromArray(resources));
	params.put("resourcerole", StringUtilities.getCSVFromArray(resourceRoles));
	params.put("persistentrequest", (new Boolean(persistentRequest)).toString());
    }
   
    public ResourceRequestMessage(String messageString) {
	super(messageString);
    }

    public String[] getResources() {
	return StringUtilities.getArrayFromCSV(params.get("resourcerequest"));
    }

    public String[] getResourceRoles() {
	return StringUtilities.getArrayFromCSV(params.get("resourcerole"));
    }

    public boolean isPersistentRequest() {
	return Boolean.parseBoolean(params.get("persistentrequest"));
    }

    public static boolean isResourceRequestMessage(String messageString) {
	if(messageString.indexOf("\nresourcerequest:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }


    
}
