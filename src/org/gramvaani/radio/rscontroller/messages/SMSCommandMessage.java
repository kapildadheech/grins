package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.StringUtilities;

import java.util.Hashtable;

public class SMSCommandMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, command = particular command (send, getpending, cancelSend, receive), options = options specific to the command

    //commands
    public static final String SEND           = "SEND";
    public static final String SEND_PENDING   = "GET_PENDING";
    public static final String CANCEL_SEND    = "CANCEL_SEND";

    //options
    public static final String PHONE_NOS      = "PHONE_NOS";
    public static final String MESSAGE        = "MESSAGE";
    public static final String SMSLINE        = "SMSLINE";
    public static final String SMS_IDS        = "SMS_IDS";
    
    //boolean values
    public static final String TRUE           = "TRUE";
    public static final String FALSE          = "FALSE";


    public SMSCommandMessage(String source, String dest, String widgetName, 
			     String command, Hashtable<String, String> options) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	params.put("smscommand", command);
	params.put("options", StringUtilities.getCSVFromHashtable(options));
    }
   
    public SMSCommandMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getCommand() {
	return params.get("smscommand");
    }
    
    public Hashtable<String,String> getOptions(){
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }

    public static boolean isSMSCommandMessage(String messageString) {
	if(messageString.indexOf("\nsmscommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}