package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;

import java.util.*;

public class MachinePingRequestMessage extends IPCSyncMessage {
    
    // general use: source = serviceName
    public MachinePingRequestMessage(String source, String dest, String[] machineIDCSV) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("machinepingrequest", StringUtilities.getCSVFromArray(machineIDCSV));
    }
    
    public MachinePingRequestMessage(String messageString) {
	super(messageString);
    }

    public String[] getMachineIDs() {
	return StringUtilities.getArrayFromCSV(params.get("machinepingrequest"));
    }

    public static boolean isMachinePingRequestMessage(String messageString) {
	if(messageString.indexOf("\nmachinepingrequest:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
