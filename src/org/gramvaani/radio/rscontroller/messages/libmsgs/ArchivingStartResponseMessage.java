package org.gramvaani.radio.rscontroller.messages.libmsgs;

import org.gramvaani.simpleipc.*;

public class ArchivingStartResponseMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService, sessionID = sessionID, filename = filename
    public ArchivingStartResponseMessage(String source, String dest, String filename, String status) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("archivefilename", filename);
	params.put("status", status);
    }
   
    public ArchivingStartResponseMessage(String messageString) {
	super(messageString);
    }

    public String getStatus() {
	return params.get("status");
    }

    public String getArchiveFilename() {
	return params.get("archivefilename");
    }

    public static boolean isArchivingStartResponseMessage(String messageString) {
	if(messageString.indexOf("\narchivefilename:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
