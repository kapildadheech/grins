package org.gramvaani.radio.rscontroller.messages.libmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.medialib.*;

public class RadioProgramGetAudioMessage extends IPCMessage {
    public static String STREAMING = "STREAMING";
    public static String DOWNLOAD = "DOWNLOAD";

    // general use: source = serviceName, dest = libService, itemID, statUpdate
    public RadioProgramGetAudioMessage(String source, String dest, String filename, String method) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("getaudiofilename", filename);
	params.put("method", method);
    }
   
    public RadioProgramGetAudioMessage(String messageString) {
	super(messageString);
    }

    public String getAudioFilename() {
	return params.get("getaudiofilename");
    }

    public String getMethod() {
	return params.get("method");
    }

    public static boolean isRadioProgramGetAudioMessage(String messageString) {
	if(messageString.indexOf("\ngetaudiofilename:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
