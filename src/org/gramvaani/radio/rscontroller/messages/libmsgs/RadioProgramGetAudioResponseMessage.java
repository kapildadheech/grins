package org.gramvaani.radio.rscontroller.messages.libmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.medialib.*;

public class RadioProgramGetAudioResponseMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService, getAudioUrl, getAudioUrlStatus
    public RadioProgramGetAudioResponseMessage(String source, String dest, String getAudioUrl, String getAudioUrlStatus) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("getaudiourl", getAudioUrl);
	params.put("getaudiostatus", getAudioUrlStatus);
    }
   
    public RadioProgramGetAudioResponseMessage(String messageString) {
	super(messageString);
    }

    public String getAudioUrl() {
	return params.get("getaudiourl");
    }

    public String getAudioUrlStatus() {
	return params.get("getaudiostatus");
    }

    public static boolean isRadioProgramGetAudioResponseMessage(String messageString) {
	if(messageString.indexOf("\ngetaudiourl:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
