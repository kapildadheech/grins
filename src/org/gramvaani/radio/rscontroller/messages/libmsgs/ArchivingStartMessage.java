package org.gramvaani.radio.rscontroller.messages.libmsgs;

import org.gramvaani.simpleipc.*;

public class ArchivingStartMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService, archiveType = BCAST_MIC
    public ArchivingStartMessage(String source, String dest, String archiveType, String language, String filename,
				 String archivingType) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("archivetype", archiveType);
	params.put("language", language);
	params.put("archivefilename", filename);
	params.put("archivingtype", archivingType);
    }
   
    public ArchivingStartMessage(String messageString) {
	super(messageString);
    }

    public String getArchiveType() {
	return params.get("archivetype");
    }

    public String getLanguage() {
	return params.get("language");
    }

    public String getFilename() {
	return params.get("archivefilename");
    }

    public String getArchivingType() {
	return params.get("archivingtype");
    }

    public static boolean isArchivingStartMessage(String messageString) {
	if(messageString.indexOf("\narchivetype:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
