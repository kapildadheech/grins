package org.gramvaani.radio.rscontroller.messages.libmsgs;

import org.gramvaani.simpleipc.*;

public class ArchivingDoneResponseMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService, archiveType = BCAST_MIC
    public ArchivingDoneResponseMessage(String source, String dest, String filename, String libfilename, String status, String error) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("archivedoldfilename", filename);
	params.put("archivedlibfilename", libfilename);
	params.put("archivedfilestatus", status);
	params.put("error", error);
    }
   
    public ArchivingDoneResponseMessage(String source, String dest, String filename, String libfilename, String status) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("archivedoldfilename", filename);
	params.put("archivedlibfilename", libfilename);
	params.put("archivedfilestatus", status);
	params.put("error", "");
    }

    public ArchivingDoneResponseMessage(String messageString) {
	super(messageString);
    }

    public String getArchivedFilename() {
	return params.get("archivedoldfilename");
    }

    public String getError() {
	return params.get("error");
    }

    public String getArchivedLibFilename() {
	return params.get("archivedlibfilename");
    }

    public String getArchivedFileStatus() {
	return params.get("archivedfilestatus");
    }

    public void setStartTime(long startTime){
	params.put("starttime", Long.toString(startTime));
    }

    public long getStartTime(){
	return Long.parseLong(params.get("starttime"));
    }

    public static boolean isArchivingDoneResponseMessage(String messageString) {
	if(messageString.indexOf("\narchivedfilestatus:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
