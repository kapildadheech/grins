package org.gramvaani.radio.rscontroller.messages.libmsgs;

import org.gramvaani.simpleipc.*;

public class ArchivingDoneMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService, archiveType = BCAST_MIC
    public ArchivingDoneMessage(String source, String dest, String type, String language, String filename, String archivingType) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("archivedfilename", filename);
	params.put("archtype", type);
	params.put("archlanguage", language);
	params.put("archivingtype", archivingType);
    }
   
    public ArchivingDoneMessage(String messageString) {
	super(messageString);
    }

    public String getArchivedFilename() {
	return params.get("archivedfilename");
    }

    public String getArchType() {
	return params.get("archtype");
    }

    public String getArchLanguage() {
	return params.get("archlanguage");
    }

    public String getArchivingType() {
	return params.get("archivingtype");
    }

    public static boolean isArchivingDoneMessage(String messageString) {
	if(messageString.indexOf("\narchivedfilename:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
