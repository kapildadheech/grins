package org.gramvaani.radio.rscontroller.messages.libmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.medialib.*;

public class RadioProgramStatUpdateMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService, itemID, statUpdate
    public RadioProgramStatUpdateMessage(String source, String dest, PlayoutHistory statUpdate) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("radioprogramstatupdate", statUpdate.toString());
    }
   
    public RadioProgramStatUpdateMessage(String messageString) {
	super(messageString);
    }

    public PlayoutHistory getStatUpdate() {
	return new PlayoutHistory(params.get("radioprogramstatupdate"));
    }

    public static boolean isRadioProgramStatUpdateMessage(String messageString) {
	if(messageString.indexOf("\nradioprogramstatupdate:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
