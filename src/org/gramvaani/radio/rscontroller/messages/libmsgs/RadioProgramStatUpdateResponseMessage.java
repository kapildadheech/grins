package org.gramvaani.radio.rscontroller.messages.libmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.medialib.*;

public class RadioProgramStatUpdateResponseMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService, statupdatestatus
    public RadioProgramStatUpdateResponseMessage(String source, String dest, String statUpdateStatus) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("radioprogramstatupdatestatus", statUpdateStatus);
    }
   
    public RadioProgramStatUpdateResponseMessage(String messageString) {
	super(messageString);
    }

    public String getStatUpdateStatus() {
	return params.get("radioprogramstatupdatestatus");
    }

    public static boolean isRadioProgramStatUpdateResponseMessage(String messageString) {
	if(messageString.indexOf("\nradioprogramstatupdatestatus:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
