package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;

public class ServiceErrorMessage extends IPCMessage {
    public static String NOT_FOUND = "404";
    public static String SERVICE_ERROR = "400";
    public static String CONNECTION_ERROR = "401";

    // general use: source = serviceName, dest = resourceManager, error = error details
    public ServiceErrorMessage(String source, String dest, String error, String code, String payload) {
	super(source, dest);
	messageType = IPCMessage.ERROR_MESSAGE;
	params.put("serviceerror", error);
	params.put("errorcode", code);
	params.put("payload", payload);
    }
   
    public ServiceErrorMessage(String messageString) {
	super(messageString);
    }

    public String getError() {
	return params.get("serviceerror");
    }

    public String getErrorCode() {
	return params.get("errorcode");
    }

    public String getPayload() {
	return params.get("payload");
    }

    public static boolean isServiceErrorMessage(String messageString) {
	if((messageString.indexOf("\nserviceerror:") != -1) &&
	   (messageString.indexOf("\nservicename:") == -1)) {//To distinguish from ServiceErrorRelayMessage

	    return true;
	} else {
	    return false;
	}
    }

}
