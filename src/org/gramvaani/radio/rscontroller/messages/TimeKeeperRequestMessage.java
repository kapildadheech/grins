package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;

public class TimeKeeperRequestMessage extends IPCMessage {

    // general use: source = serviceName, dest = libService
    public TimeKeeperRequestMessage(String source, String dest) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("timekeeper", "");
    }

    public TimeKeeperRequestMessage(String messageString) {
	super(messageString);
    }

    public static boolean isTimeKeeperRequestMessage(String messageString) {
	if(messageString.indexOf("\ntimekeeper:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
