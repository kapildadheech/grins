package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class UploadDoneAckMessage extends IPCMessage {

    public UploadDoneAckMessage(String source, String dest, String fileName) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("uploadackfilename", fileName);
    }
   
    public UploadDoneAckMessage(String source, String dest, String fileName, String widgetName) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("uploadackfilename", fileName);
	params.put("widgetname", widgetName);
    }

    public UploadDoneAckMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getFileName() {
	return params.get("uploadackfilename");
    }

    public static boolean isUploadDoneAckMessage(String messageString) {
	if(messageString.indexOf("\nuploadackfilename:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }
}