package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.StringUtilities;

public class DeleteProgramsMessage extends IPCMessage {

    public DeleteProgramsMessage(String source, String dest, String[] programs) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("programstodelete", StringUtilities.getCSVFromArray(programs));
    }
   
    public DeleteProgramsMessage(String messageString) {
	super(messageString);
    }

    public String[] getPrograms() {
	return StringUtilities.getArrayFromCSV(params.get("programstodelete"));
    }

    public static boolean isDeleteProgramsMessage(String messageString) {
	if(messageString.indexOf("\nprogramstodelete:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
