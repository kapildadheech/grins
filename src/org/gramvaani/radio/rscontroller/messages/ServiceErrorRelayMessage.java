package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;

public class ServiceErrorRelayMessage extends IPCMessage {
    public static String NOT_FOUND = "404";
    public static String SERVICE_ERROR = "400";
    public static String CONNECTION_ERROR = "401";

    // general use: source = resourceManager, dest = ui, serviceName = service, widgetName = widget, error = error details
    public ServiceErrorRelayMessage(String source, String dest, String serviceName, String widgetName, String error, String errorCode, String payload) {
	super(source, dest);
	messageType = IPCMessage.ERROR_MESSAGE;
	params.put("servicename", serviceName);
	params.put("widgetname", widgetName);
	params.put("serviceerror", error);
	params.put("errorcode", errorCode);
	params.put("payload", payload);
    }
   
    public ServiceErrorRelayMessage(String messageString) {
	super(messageString);
    }

    public String getServiceName() {
	return params.get("servicename");
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getError() {
	return params.get("serviceerror");
    }

    public String getErrorCode() {
	return params.get("errorcode");
    }

    public String getPayload() {
	return params.get("payload");
    }

    public static boolean isServiceErrorRelayMessage(String messageString) {
	if(messageString.indexOf("\nserviceerror:") != -1 && messageString.indexOf("\nwidgetname:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
