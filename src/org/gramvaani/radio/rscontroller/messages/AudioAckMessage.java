package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.rscontroller.services.AudioService;

public class AudioAckMessage extends IPCMessage {
    public static final String TELECAST_TIME = "telecasttime";
    // general use: sent from AudioService to widget

    public AudioAckMessage(String source, String dest, String widgetName, 
			   String sessionId, String itemId, String[] options, 
			   String ack, long telecastTime, String command, String[] error) {

	super(source, dest);

	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("sessionid", sessionId);
	params.put("itemid", itemId);
	params.put("options", StringUtilities.getCSVFromArray(options));
	params.put("audioack", ack);
	params.put(TELECAST_TIME, Long.toString(telecastTime));
	params.put("command", command);
	params.put("error", StringUtilities.getCSVFromArray(error));
    }
   
    public AudioAckMessage(String source, String dest, String widgetName, 
			   String sessionId, String itemId, String[] options, String ack, long telecastTime, String command) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("sessionid", sessionId);
	params.put("itemid", itemId);
	params.put("options", StringUtilities.getCSVFromArray(options));
	params.put("audioack", ack);
	params.put(TELECAST_TIME, Long.toString(telecastTime));
	params.put("command", command);
	params.put("error", "");

	if (command.equals(AudioService.PLAY_DONE_MSG))
	    setPersistent(true);
    }

    public AudioAckMessage(String messageString) {
	super(messageString);
	if (params.get("command").equals(AudioService.PLAY_DONE_MSG))
	    setPersistent(true);
    }

    public String[] getError() {
	String errorString = params.get("error");
	if(errorString != null)
	    return StringUtilities.getArrayFromCSV(errorString);
	else
	    return (new String[] {});
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getSessionID() {
	return params.get("sessionid");
    }

    public String getItemID() {
	return params.get("itemid");
    }

    public String[] getOptions() {
	return StringUtilities.getArrayFromCSV(params.get("options"));
    }

    public String getAck() {
	return params.get("audioack");
    }
    
    public long getTelecastTime(){
	return Long.parseLong(params.get(TELECAST_TIME));
    }

    public String getCommand() {
	return params.get("command");
    }

    public static boolean isAudioAckMessage(String messageString) {
	if(messageString.indexOf("\naudioack:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
