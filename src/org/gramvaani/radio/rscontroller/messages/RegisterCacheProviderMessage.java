package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

public class RegisterCacheProviderMessage extends IPCMessage {

    public RegisterCacheProviderMessage(String source, String dest, String widgetName) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname", widgetName);
	params.put("cacheprovider", "dummy");
    }
   
    public RegisterCacheProviderMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public static boolean isRegisterCacheProviderMessage(String messageString) {
	if(messageString.indexOf("\ncacheprovider:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}