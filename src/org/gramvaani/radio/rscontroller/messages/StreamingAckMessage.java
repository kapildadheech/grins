package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class StreamingAckMessage extends IPCMessage {

    // general use: sent from StreamingService to widget

    public StreamingAckMessage(String source, String dest, String widgetName, 
			       boolean ack, String command) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("streamingack", Boolean.toString(ack));
	params.put("command", command);
	if (widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("error","");
    }
    
    public StreamingAckMessage(String source, String dest, String widgetName, 
			       boolean ack, String command, String[] error) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("streamingack", Boolean.toString(ack));
	params.put("command", command);
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("error",StringUtilities.getCSVFromArray(error));
    }
   
    public StreamingAckMessage(String messageString) {
	super(messageString);
    }

    public String[] getError() {
	return StringUtilities.getArrayFromCSV(params.get("error"));
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public boolean getAck() {
	return Boolean.parseBoolean(params.get("streamingack"));
    }

    public String getCommand() {
	return params.get("command");
    }

    public static boolean isStreamingAckMessage(String messageString) {
	if ((messageString.indexOf("s:"+RSController.STREAMING_SERVICE) != -1) &&
	   (messageString.indexOf("\nstreamingack:") != -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}