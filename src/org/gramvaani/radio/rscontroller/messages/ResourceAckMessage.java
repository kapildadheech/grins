package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ResourceAckMessage extends IPCMessage {

    // source = resourceManager, dest = serviceName, success = successfull resources, failed = failed resources
    public ResourceAckMessage(String source, String dest, String[] successResources, String[] successfulResourceRoles,
			      String[] failedResources, String[] failedResourceRoles) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("successfulresources", StringUtilities.getCSVFromArray(successResources));
	params.put("successfulresourceroles", StringUtilities.getCSVFromArray(successfulResourceRoles));
	params.put("failedresources", StringUtilities.getCSVFromArray(failedResources));	
	params.put("failedresourceroles", StringUtilities.getCSVFromArray(failedResourceRoles));
    }
   
    public ResourceAckMessage(String messageString) {
	super(messageString);
    }

    public String[] getSuccessfulResources() {
	return StringUtilities.getArrayFromCSV(params.get("successfulresources"));
    }

    public String[] getFailedResources() {
	return StringUtilities.getArrayFromCSV(params.get("failedresources"));
    }

    public String[] getSuccessfulResourceRoles() {
	return StringUtilities.getArrayFromCSV(params.get("successfulresourceroles"));
    }

    public String[] getUnsuccessfulResourceRoles() {
	return StringUtilities.getArrayFromCSV(params.get("failedresourceroles"));
    }

    public static boolean isResourceAckMessage(String messageString) {
	if(messageString.indexOf("\nsuccessfulresources:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}