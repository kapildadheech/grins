package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

public class DiagnosticsMessage extends IPCMessage {

    public DiagnosticsMessage(String source, String dest, String message) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("diagnosticsmessage", message);
    }
   
    public DiagnosticsMessage(String messageString) {
	super(messageString);
    }

    public String getMessage(){
	return params.get("diagnosticsmessage");
    }

    public static boolean isDiagnosticsMessage(String messageString) {
	if(messageString.indexOf("\ndiagnosticsmessage:") != -1){
	    return true;
	} else {
	    return false;
	}
    }
}