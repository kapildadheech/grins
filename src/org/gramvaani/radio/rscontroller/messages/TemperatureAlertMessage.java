package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

public class TemperatureAlertMessage extends IPCMessage {

    // general use: source = service/rsc, dest = rsc/app, machineID
    public TemperatureAlertMessage(String source, String dest, String machineID, String[] affectedServices) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("alertmachineid", machineID);
	params.put("affectedservices", StringUtilities.getCSVFromArray(affectedServices));
    }
   
    public TemperatureAlertMessage(String source, String dest, String machineID){
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("alertmachineid", machineID);
	params.put("affectedservices","");
    }

    public TemperatureAlertMessage(String messageString) {
	super(messageString);
    }

    public String getMachineID() {
	return params.get("alertmachineid");
    }
    
    public String[] getAffectedServices(){
	return StringUtilities.getArrayFromCSV(params.get("affectedservices"));
    }

    public static boolean isTemperatureAlertMessage(String messageString) {
	if(messageString.indexOf("\nalertmachineid:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}