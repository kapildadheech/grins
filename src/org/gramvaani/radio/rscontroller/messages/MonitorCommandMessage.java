package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;

public class MonitorCommandMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, command = particular command (start, stop), 

    public MonitorCommandMessage(String source, String dest, String widgetName, 
				 String command, String srcPortRole, String sinkPortRole, String language) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	params.put("monitorcommand", command);
	params.put("srcportrole", srcPortRole);
	params.put("sinkportrole", sinkPortRole);
	params.put("language", language);
    }
   
    public MonitorCommandMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getCommand() {
	return params.get("monitorcommand");
    }
    
    public String getSrcPortRole() {
	return params.get("srcportrole");
    }
    
    public String getSinkPortRole() {
	return params.get("sinkportrole");
    }

    public String getLanguage() {
	return params.get("language");
    }

    public static boolean isMonitorCommandMessage(String messageString) {
	if(messageString.indexOf("\nmonitorcommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
