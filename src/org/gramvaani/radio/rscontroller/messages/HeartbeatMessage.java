package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.IPCMessage;

public class HeartbeatMessage extends IPCMessage {

    public HeartbeatMessage (String source, String dest, String sequenceNumber){
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("heartbeatsequence", sequenceNumber);
    }

    public HeartbeatMessage (String messageString){
	super(messageString);
    }

    public String getHeartbeatSequence(){
	return params.get("heartbeatsequence");
    }

    public static boolean isHeartbeatMessage(String messageString) {
	if((messageString.indexOf("\nheartbeatsequence:") != -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}