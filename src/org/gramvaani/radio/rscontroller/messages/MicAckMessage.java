package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class MicAckMessage extends IPCMessage {

    // general use: sent from MicService to widget

    public MicAckMessage(String source, String dest, String widgetName, 
			   String ack, String command) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("micack", ack);
	params.put("command", command);
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("error","");
    }
    
    public MicAckMessage(String source, String dest, String widgetName, 
			 String ack, String command, String[] error) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("micack", ack);
	params.put("command", command);
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("error",StringUtilities.getCSVFromArray(error));
    }
   
    public MicAckMessage(String messageString) {
	super(messageString);
    }

    public String[] getError() {
	return StringUtilities.getArrayFromCSV(params.get("error"));
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getAck() {
	return params.get("micack");
    }

    public String getCommand() {
	return params.get("command");
    }

    public static boolean isMicAckMessage(String messageString) {
	if((messageString.indexOf("s:"+RSController.MIC_SERVICE) != -1) &&
	   (messageString.indexOf("\nmicack:") != -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}