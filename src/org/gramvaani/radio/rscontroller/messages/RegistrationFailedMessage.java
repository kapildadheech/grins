package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

public class RegistrationFailedMessage extends IPCMessage {

    // general use: source = service/rsc, dest = rsc/app, machineID
    public RegistrationFailedMessage(String source, String dest, String registrationID) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("failedregistrationid", registrationID);
    }
   
    public RegistrationFailedMessage(String messageString) {
	super(messageString);
    }

    public String getRegistrationID() {
	return params.get("registrationid");
    }
    
    public static boolean isRegistrationFailedMessage(String messageString) {
	if(messageString.indexOf("\nfailedregistrationid:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }
}