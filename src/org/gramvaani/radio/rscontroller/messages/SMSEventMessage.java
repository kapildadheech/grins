package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.StringUtilities;

import java.util.Hashtable;

public class SMSEventMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, event = particular event (newsms), options = options specific to the command
    public static final String SMSLIB_OK        = "SMSLIB_OK";
    public static final String SMSLIB_ERROR     = "SMSLIB_ERROR";

    //Events
    public static final String SMS_RECEIVED 	= "SMS_RECEIVED";

    //Options
    public static final String PHONE_NO 	= "PHONE_NO";
    public static final String MESSAGE          = "MESSAGE";

    public SMSEventMessage(String source, String dest, String widgetName, 
			   String event, Hashtable<String,String> options) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("widgetname",widgetName);
	params.put("smsevent", event);
	params.put("options", StringUtilities.getCSVFromHashtable(options));
    }
   
    public SMSEventMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getEvent() {
	return params.get("smsevent");
    }
    
    public Hashtable<String,String> getOptions(){
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }

    public static boolean isSMSEventMessage(String messageString) {
	if(messageString.indexOf("\nsmsevent:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}