package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

public class InvalidateCacheKeyMessage extends IPCMessage {

    public InvalidateCacheKeyMessage(String source, String dest, String[] widgetList, String objectType,
				     String actionType, String[] oldObjectIDs, String[] newObjectIDs) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetlist", StringUtilities.getCSVFromArray(widgetList));
	params.put("invalidateobjecttype",objectType);
	params.put("invalidateactiontype", actionType);
	params.put("invalidateoldobjects", StringUtilities.getCSVFromArray(oldObjectIDs));
	params.put("invalidatenewobjects", StringUtilities.getCSVFromArray(newObjectIDs));
	setPersistent(true);
    }
   
    public InvalidateCacheKeyMessage(String messageString) {
	super(messageString);
	setPersistent(true);
    }

    public String[] getWidgetList() {
	return StringUtilities.getArrayFromCSV(params.get("widgetlist"));
    }

    public String getObjectType() {
	return params.get("invalidateobjecttype");
    }

    public String[] getOldObjectIDs() {
	return StringUtilities.getArrayFromCSV(params.get("invalidateoldobjects"));
    }

    public String[] getNewObjectIDs() {
	return StringUtilities.getArrayFromCSV(params.get("invalidatenewobjects"));
    }

    public String getActionType() {
	return params.get("invalidateactiontype");
    }

    public static boolean isInvalidateCacheKeyMessage(String messageString) {
	if(messageString.indexOf("\ninvalidateoldobjects:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}