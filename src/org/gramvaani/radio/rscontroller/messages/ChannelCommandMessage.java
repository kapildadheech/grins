package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;

public class ChannelCommandMessage extends IPCMessage {

    // general use: source, dest, command = connectports, inport, outport

    public ChannelCommandMessage(String source, String dest, String command,
			       String inPort, String outPort) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("channelcommand", command);
	params.put("inport", inPort);
	params.put("outport", outPort);
    }
   
    public ChannelCommandMessage(String messageString) {
	super(messageString);
    }

    public String getCommand() {
	return params.get("channelcommand");
    }

    public String getInPort() {
	return params.get("inport");
    }

    public String getOutPort() {
	return params.get("outport");
    }

    public static boolean isChannelCommandMessage(String messageString) {
	if(messageString.indexOf("\nchannelcommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
