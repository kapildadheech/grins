package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.StringUtilities;

public class MachineStatusResponseMessage extends IPCMessage {

    public MachineStatusResponseMessage(String source, String dest, String widgetName, 
					String command, String[] results){

	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	params.put("command", command);
	params.put("machinestatusresults", StringUtilities.getCSVFromArray(results));
    }
   
    public MachineStatusResponseMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getCommand() {
	return params.get("command");
    }
    
    public String[] getResults(){
	return StringUtilities.getArrayFromCSV(params.get("machinestatusresults"));
    }

    public static boolean isMachineStatusResponseMessage(String messageString) {
	if(messageString.indexOf("\nmachinestatusresults:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
