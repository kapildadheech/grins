package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class StartCleanupMessage extends IPCMessage {
    public static String ENCODED_FILES = "ENCODED_FILES";
    public static String INCOMPLETE_ARCHIVING = "INCOMPLETE_ARCHIVING";
    public static String DELETED_FILES = "DELETED_FILES";
    public static String INCOMPLETE_TELEPHONY = "INCOMPLETE_TELEPHONY";

    // source = resourceManager, dest = serviceName, commands = clean up commands
    public StartCleanupMessage(String source, String dest, String[] commands) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("cleanupcommands", StringUtilities.getCSVFromArray(commands));
    }
   
    public StartCleanupMessage(String messageString) {
	super(messageString);
    }

    public String[] getCommands() {
	return StringUtilities.getArrayFromCSV(params.get("cleanupcommands"));
    }

    public static boolean isStartCleanupMessage(String messageString) {
	if(messageString.indexOf("\ncleanupcommands:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}