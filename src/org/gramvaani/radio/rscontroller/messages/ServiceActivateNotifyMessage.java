package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ServiceActivateNotifyMessage extends IPCMessage {

    // source = resourceManager, dest = uiService, widgetName = widgetName, activatedServices, inactivatedServices
    public ServiceActivateNotifyMessage(String source, String dest, String serviceName, String registrationID) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("activatedservice", serviceName);
	params.put("registrationid", registrationID);
    }

    public ServiceActivateNotifyMessage(String messageString) {
	super(messageString);
    }
   
    public String getActivatedServiceName() {
	return params.get("activatedservice");
    }

    public String getRegistrationID(){
	return params.get("registrationid");
    }

    public static boolean isServiceActivateNotifyMessage(String messageString) {
	if((messageString.indexOf("\nactivatedservice:") != -1) &&
	   (messageString.indexOf("\nregistrationid:") != -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}
