package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class PreemptResourceMessage extends IPCMessage {

    // source = serviceName, dest = resourceManager, resources = resources
    public PreemptResourceMessage(String source, String dest, String[] resources) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("preemptresource", StringUtilities.getCSVFromArray(resources));
    }
   
    public PreemptResourceMessage(String messageString) {
	super(messageString);
    }

    public String[] getResources() {
	return StringUtilities.getArrayFromCSV(params.get("preemptresource"));
    }

    public static boolean isPreemptResourceMessage(String messageString) {
	if((messageString.indexOf("\npreemptresource:") != -1) && 
	   (messageString.indexOf("\npreemptresourceack:") == -1)){
	    return true;
	} else {
	    return false;
	}
    }

}