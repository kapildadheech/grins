package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ResourceReleaseMessage extends IPCMessage {

    // source = serviceName, dest = resourceManager, resources = resources
    public ResourceReleaseMessage(String source, String dest, String[] resources) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("resourcerelease", StringUtilities.getCSVFromArray(resources));
    }
   
    public ResourceReleaseMessage(String messageString) {
	super(messageString);
    }

    public String[] getResources() {
	return StringUtilities.getArrayFromCSV(params.get("resourcerelease"));
    }

    public static boolean isResourceReleaseMessage(String messageString) {
	if(messageString.indexOf("\nresourcerelease:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}