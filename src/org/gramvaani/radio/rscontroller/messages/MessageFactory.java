package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.LogUtilities;
import org.gramvaani.radio.rscontroller.messages.libmsgs.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.*;

public class MessageFactory {

    // should pick up message classes from configuration file
    // message factory can then be a part of simpleipc
    public static IPCMessage createMessage(String messageString) {
	//LogUtilities.getDefaultLogger().debug("MessageFactory: " + messageString);
	if (messageString == null || messageString.equals("")) 
	    return null;
	if (messageString.indexOf("\nt:0")!=-1) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RegisterMessage.");
	    return new IPCMessage(messageString);
	} else if (ServiceErrorMessage.isServiceErrorMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ServiceError");
	    return new ServiceErrorMessage(messageString);
	} else if (ServiceErrorRelayMessage.isServiceErrorRelayMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ServiceErrorRelay");
	    return new ServiceErrorRelayMessage(messageString);
	} else if (ResourceRequestMessage.isResourceRequestMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ResourceRequest");
	    return new ResourceRequestMessage(messageString);
	} else if (ResourceReleaseMessage.isResourceReleaseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ResourceRelease");
	    return new ResourceReleaseMessage(messageString);
	} else if (ResourceAckMessage.isResourceAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ResourceAck");
	    return new ResourceAckMessage(messageString);
	} else if (ServiceInterestMessage.isServiceInterestMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ServiceInterest");
	    return new ServiceInterestMessage(messageString);
	} else if (ServiceAckMessage.isServiceAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ServiceAck");
	    return new ServiceAckMessage(messageString);
	} else if (ServiceNotifyMessage.isServiceNotifyMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ServiceNotify");
	    return new ServiceNotifyMessage(messageString);
	} else if (AudioCommandMessage.isAudioCommandMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: AudioCommand");
	    return new AudioCommandMessage(messageString);
	} else if (AudioAckMessage.isAudioAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: AudioAck");
	    return new AudioAckMessage(messageString);
	} else if (AudioErrorMessage.isAudioErrorMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: AudioError");
	    return new AudioErrorMessage(messageString);
	} else if (ChannelCommandMessage.isChannelCommandMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ChannelCommand");
	    return new ChannelCommandMessage(messageString);
	} else if (ChannelAckMessage.isChannelAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ChannelAck");
	    return new ChannelAckMessage(messageString);
	} else if (ArchiverCommandMessage.isArchiverCommandMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ArchiverCommand");
	    return new ArchiverCommandMessage(messageString);
	} else if (ArchiverAckMessage.isArchiverAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ArchiverAck");
	    return new ArchiverAckMessage(messageString);
	} else if (ArchiverLevelsMessage.isArchiverLevelsMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ArchiverLevels");
	    return new ArchiverLevelsMessage(messageString);
	} else if (GstreamerErrorMessage.isGstreamerErrorMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: GstreamerError");
	    return new GstreamerErrorMessage(messageString);
	} else if (HeartbeatMessage.isHeartbeatMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: Heartbeat");
	    return new HeartbeatMessage(messageString);
	} else if (ArchivingStartMessage.isArchivingStartMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ArchivingStart");
	    return new ArchivingStartMessage(messageString);
	} else if (ArchivingStartResponseMessage.isArchivingStartResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ArchivingStartResponse");
	    return new ArchivingStartResponseMessage(messageString);
	} else if (TimeKeeperRequestMessage.isTimeKeeperRequestMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: TimeKeeperRequest");
	    return new TimeKeeperRequestMessage(messageString);
	} else if (TimeKeeperResponseMessage.isTimeKeeperResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: TimeKeeperResponse");
	    return new TimeKeeperResponseMessage(messageString);
	} else if (RadioProgramStatUpdateMessage.isRadioProgramStatUpdateMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RadioProgramStatUpdate");
	    return new RadioProgramStatUpdateMessage(messageString);
	} else if (RadioProgramStatUpdateResponseMessage.isRadioProgramStatUpdateResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RadioProgramStatResponseUpdate");
	    return new RadioProgramStatUpdateResponseMessage(messageString);
	} else if (RadioProgramGetAudioMessage.isRadioProgramGetAudioMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RadioProgramGetAudio");
	    return new RadioProgramGetAudioMessage(messageString);
	} else if (RadioProgramGetAudioResponseMessage.isRadioProgramGetAudioResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RadioProgramGetAudioResponse");
	    return new RadioProgramGetAudioResponseMessage(messageString);
	} else if (ArchivingDoneMessage.isArchivingDoneMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ArchivingDone");
	    return new ArchivingDoneMessage(messageString);
	} else if (ArchivingDoneResponseMessage.isArchivingDoneResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ArchivingDoneResponse");
	    return new ArchivingDoneResponseMessage(messageString);
	} else if (ServiceActivateNotifyMessage.isServiceActivateNotifyMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ServiceActivateNotify");
	    return new ServiceActivateNotifyMessage(messageString);
	} else if (PreemptBackgroundActivityMessage.isPreemptBackgroundActivityMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: PreemptBackgroundActivity");
	    return new PreemptBackgroundActivityMessage(messageString);
	} else if (MicCommandMessage.isMicCommandMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MicCommandMessage");
	    return new MicCommandMessage(messageString);
	} else if (MicAckMessage.isMicAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MicAckMessage");
	    return new MicAckMessage(messageString); 
	} else if (MicLevelsMessage.isMicLevelsMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MicLevelsMessage");
	    return new MicLevelsMessage(messageString);
	} else if (MonitorCommandMessage.isMonitorCommandMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MonitorCommandMessage");
	    return new MonitorCommandMessage(messageString);
	} else if (MonitorAckMessage.isMonitorAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MonitorAckMessage");
	    return new MonitorAckMessage(messageString); 
	} else if (MonitorFrequencyMessage.isMonitorFrequencyMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MonitorFrequencyMessage");
	    return new MonitorFrequencyMessage(messageString);
	} else if (StartCleanupMessage.isStartCleanupMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: StartCleanupMessage");
	    return new StartCleanupMessage(messageString);
	} else if (AddAnchorTermMessage.isAddAnchorTermMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: AddAnchorTermMessage");
	    return new AddAnchorTermMessage(messageString);
	} else if (AddAnchorTermResponseMessage.isAddAnchorTermResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: AddAnchorTermResponseMessage");
	    return new AddAnchorTermResponseMessage(messageString);
	} else if (RemoveProgramMessage.isRemoveProgramMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RemoveProgramMessage");
	    return new RemoveProgramMessage(messageString);
	} else if (RemoveProgramResponseMessage.isRemoveProgramResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RemoveProgramResponseMessage");
	    return new RemoveProgramResponseMessage(messageString);
	} else if (UpdateIndexMessage.isUpdateIndexMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: UpdateIndexMessage");
	    return new UpdateIndexMessage(messageString);
	} else if (RefreshIndexMessage.isRefreshIndexMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RefreshIndexMessage");
	    return new RefreshIndexMessage(messageString);
	} else if (UpdateProgramMessage.isUpdateProgramMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: UpdateProgramMessage");
	    return new UpdateProgramMessage(messageString);
	} else if (UpdateProgramResponseMessage.isUpdateProgramResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: UpdateProgramResponseMessage");
	    return new UpdateProgramResponseMessage(messageString);
	} else if (AnchorSearchMessage.isAnchorSearchMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: AnchorSearchMessage");
	    return new AnchorSearchMessage(messageString);
	} else if (AnchorSearchResponseMessage.isAnchorSearchResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: AnchorSearchResponseMessage");
	    return new AnchorSearchResponseMessage(messageString);
	} else if (ProgramSearchMessage.isProgramSearchMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ProgramSearchMessage");
	    return new ProgramSearchMessage(messageString);
	} else if (ProgramSearchResponseMessage.isProgramSearchResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ProgramSearchResponseMessage");
	    return new ProgramSearchResponseMessage(messageString);
	} else if (PreemptResourceMessage.isPreemptResourceMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: PreemptResourceMessage");
	    return new PreemptResourceMessage(messageString);
	} else if (PreemptResourceAckMessage.isPreemptResourceAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: PreemptResourceAckMessage");
	    return new PreemptResourceAckMessage(messageString);
	} else if (ServiceReadyAckMessage.isServiceReadyAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ServiceReadyAckMessage");
	    return new ServiceReadyAckMessage(messageString);
	} else if (ServiceReadyMessage.isServiceReadyMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ServiceReadyMessage");
	    return new ServiceReadyMessage(messageString);
	} else if (InvalidateCacheMessage.isInvalidateCacheMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: InvalidateCacheMessage");
	    return new InvalidateCacheMessage(messageString);
	} else if (InvalidateCacheKeyMessage.isInvalidateCacheKeyMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: InvalidateCacheKeyMessage");
	    return new InvalidateCacheKeyMessage(messageString);
	} else if (RegisterCacheProviderMessage.isRegisterCacheProviderMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RegisterCacheProviderMessage");
	    return new RegisterCacheProviderMessage(messageString);
	} else if (RegisterCacheProviderAckMessage.isRegisterCacheProviderAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RegisterCacheProviderAckMessage");
	    return new RegisterCacheProviderAckMessage(messageString);
	} else if (FlushIndexMessage.isFlushIndexMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: FlushIndexMessage");
	    return new FlushIndexMessage(messageString);
	} else if (FlushIndexResponseMessage.isFlushIndexResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: FlushIndexResponseMessage");
	    return new FlushIndexResponseMessage(messageString);
	} else if (TemperatureAlertMessage.isTemperatureAlertMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: TemperatureAlertMessage");
	    return new TemperatureAlertMessage(messageString);
	} else if (RegistrationAckMessage.isRegistrationAckMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: RegistrationAckMessage");
	    return new RegistrationAckMessage(messageString);
	} else if (StatusCheckRequestMessage.isStatusCheckRequestMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: StatusCheckRequestMessage");
	    return new StatusCheckRequestMessage(messageString);
	} else if (StatusCheckResponseMessage.isStatusCheckResponseMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: StatusCheckResponseMessage");
	    return new StatusCheckResponseMessage(messageString);
	} else if (GraphStatsMessage.isGraphStatsMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: GraphStatsMessage");
	    return new GraphStatsMessage(messageString);
	} else if (GraphStatsResponseMessage.isGraphStatsResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: GraphStatsResponseMessage");
	    return new GraphStatsResponseMessage(messageString);
	} else if (DiagnosticsMessage.isDiagnosticsMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: DiagnosticsMessage");
	    return new DiagnosticsMessage(messageString);
	} else if (MachineStatusRequestMessage.isMachineStatusRequestMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MachineStatusRequestMessage");
	    return new MachineStatusRequestMessage(messageString);
	} else if (MachineStatusResponseMessage.isMachineStatusResponseMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MachineStatusResponseMessage");
	    return new MachineStatusResponseMessage(messageString);
	} else if (MachinePingRequestMessage.isMachinePingRequestMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MachinePingRequestMessage");
	    return new MachinePingRequestMessage(messageString);
	} else if (MachinePingResponseMessage.isMachinePingResponseMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: MachinePingResponseMessage");
	    return new MachinePingResponseMessage(messageString);
	} else if (UploadDoneMessage.isUploadDoneMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: UploadDoneMessage");
	    return new UploadDoneMessage(messageString);
	} else if (UploadDoneAckMessage.isUploadDoneAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: UploadDoneAckMessage");
	    return new UploadDoneAckMessage(messageString);
	} else if (AudioSessionsInfoMessage.isAudioSessionsInfoMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: AudioSessionsInfoMessage");
	    return new AudioSessionsInfoMessage(messageString);
	} else if (DeleteProgramsMessage.isDeleteProgramsMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: DeleteProgramsMessage");
	    return new DeleteProgramsMessage(messageString);
	} else if (TelephonyCommandMessage.isTelephonyCommandMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: TelephonyCommandMessage");
	    return new TelephonyCommandMessage(messageString);
	} else if (TelephonyAckMessage.isTelephonyAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: TelephonyAckMessage");
	    return new TelephonyAckMessage(messageString);
	} else if (TelephonyEventMessage.isTelephonyEventMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: TelephonyEventMessage");
	    return new TelephonyEventMessage(messageString);
	} else if (ResourceRequestSyncMessage.isResourceRequestSyncMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ResourceRequestSyncMessage");
	    return new ResourceRequestSyncMessage(messageString);
	} else if (ResourceReleaseSyncMessage.isResourceReleaseSyncMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ResourceReleaseSyncMessage");
	    return new ResourceReleaseSyncMessage(messageString);
	} else if (ResourceReleaseAckSyncMessage.isResourceReleaseAckSyncMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ResourceReleaseAckSyncMessage");
	    return new ResourceReleaseAckSyncMessage(messageString);
	} else if (ResourceAckSyncMessage.isResourceAckSyncMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: ResourceAckSyncMessage");
	    return new ResourceAckSyncMessage(messageString);
	} else if (TelephonyInfoMessage.isTelephonyInfoMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: TelephonyInfoMessage");
	    return new TelephonyInfoMessage(messageString);
	} else if (SyncFileStoreMessage.isSyncFileStoreMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: SyncFileStoreMessage");
	    return new SyncFileStoreMessage(messageString);
	} else if (StreamingCommandMessage.isStreamingCommandMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: StreamingCommandMessage");
	    return new StreamingCommandMessage(messageString);
	} else if (StreamingAckMessage.isStreamingAckMessage(messageString)){
	    LogUtilities.getDefaultLogger().debug("MessageFactory: StreamingAckMessage");
	    return new StreamingAckMessage(messageString);
	} else if(SMSCommandMessage.isSMSCommandMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: SMSCommandMessage");
	    return new SMSCommandMessage(messageString);
	} else if(SMSAckMessage.isSMSAckMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: SMSAckMessage");
	    return new SMSAckMessage(messageString);
	} else if(SMSEventMessage.isSMSEventMessage(messageString)) {
	    LogUtilities.getDefaultLogger().debug("MessageFactory: SMSEventMessage");
	    return new SMSEventMessage(messageString);
	} else {
	    LogUtilities.getDefaultLogger().error("MessageFactory: Unknown Message Type: " + messageString);
	    return new IPCMessage(messageString);
	}
    }

}
