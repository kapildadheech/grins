package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

public class RegisterCacheProviderAckMessage extends IPCMessage {

    public RegisterCacheProviderAckMessage(String source, String dest, String widgetName, String success) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname", widgetName);
	params.put("cacheregisterstatus", success);
    }
   
    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getStatus() {
	return params.get("cacheregisterstatus");
    }

    public RegisterCacheProviderAckMessage(String messageString) {
	super(messageString);
    }

    public static boolean isRegisterCacheProviderAckMessage(String messageString) {
	if(messageString.indexOf("\ncacheregisterstatus:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}