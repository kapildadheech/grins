package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class AnchorSearchMessage extends IPCSyncMessage {

    // general use: source = serviceName, dest = libService
    public AnchorSearchMessage(String source, String dest, String anchorText) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("anchorsearchterm", anchorText);
    }
   
    public AnchorSearchMessage(String messageString) {
	super(messageString);
    }

    public String getAnchorText() {
	return params.get("anchorsearchterm");
    }

    public static boolean isAnchorSearchMessage(String messageString) {
	if(messageString.indexOf("\nanchorsearchterm:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
