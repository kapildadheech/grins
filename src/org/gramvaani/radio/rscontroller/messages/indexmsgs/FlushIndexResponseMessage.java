package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class FlushIndexResponseMessage extends IPCSyncResponseMessage {

    public FlushIndexResponseMessage(String source, String dest, String messageID, int response) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("flushindexresponse", (new Integer(response)).toString());
	setMessageID(messageID);
    }
   
    public FlushIndexResponseMessage(String messageString) {
	super(messageString);
    }

    public int getReturnValue() {
	return Integer.parseInt(params.get("flushindexresponse"));
    }

    public static boolean isFlushIndexResponseMessage(String messageString) {
	if(messageString.indexOf("\nflushindexresponse:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
