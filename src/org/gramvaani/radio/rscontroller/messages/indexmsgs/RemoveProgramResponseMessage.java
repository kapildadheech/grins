package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class RemoveProgramResponseMessage extends IPCSyncResponseMessage {

    public RemoveProgramResponseMessage(String source, String dest, String messageID, int[] response) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("removeprogramresponselength", Integer.toString(response.length));
	for(int i = 0; i < response.length; i++)
	    params.put("removeprogramresponse"+i, Integer.toString(response[i]));
	setMessageID(messageID);
    }
   
    public RemoveProgramResponseMessage(String messageString) {
	super(messageString);
    }

    public int getLength(){
	return Integer.parseInt(params.get("removeprogramresponselength"));
    }

    public int[] getReturnValue() {
	int[] retVals = new int[getLength()];
	for (int i = 0; i < retVals.length; i++)
	    retVals[i] = Integer.parseInt(params.get("removeprogramresponse"+i));
	return retVals;
    }

    public static boolean isRemoveProgramResponseMessage(String messageString) {
	if(messageString.indexOf("\nremoveprogramresponselength:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
