package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class AddAnchorTermMessage extends IPCSyncMessage {

    // general use: source = serviceName, dest = libService
    public AddAnchorTermMessage(String source, String dest, String anchorID, String filterID, String term) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("anchorid", anchorID);
	params.put("filterid", filterID);
	params.put("addanchorterm", term);
    }
   
    public AddAnchorTermMessage(String messageString) {
	super(messageString);
    }

    public String getAnchorID() {
	return params.get("anchorid");
    }

    public String getFilterID() {
	return params.get("filterid");
    }

    public String getAnchorTerm() {
	return params.get("addanchorterm");
    }

    public static boolean isAddAnchorTermMessage(String messageString) {
	if(messageString.indexOf("\naddanchorterm:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
