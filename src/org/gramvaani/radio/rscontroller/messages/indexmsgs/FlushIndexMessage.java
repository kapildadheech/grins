package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;

import java.util.*;

public class FlushIndexMessage extends IPCSyncMessage {

    // general use: source = serviceName

    public FlushIndexMessage(String source, String dest) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("flushindex", "dummy");
	params.put("flushserially", "false");
    }
    
    public FlushIndexMessage(String messageString) {
	super(messageString);
    }

    public void setFlushSerially(boolean set) {
	params.put("flushserially", new Boolean(set).toString());
    }

    public boolean flushSerially() {
	return Boolean.parseBoolean(params.get("flushserially"));
    }

    public static boolean isFlushIndexMessage(String messageString) {
	if(messageString.indexOf("\nflushindex:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
