package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class GraphStatsMessage extends IPCSyncMessage {

    // general use: source = serviceName, dest = libService
    public GraphStatsMessage(String source, String dest, String queryString, String whereClause, String joinTableStr,
			     long minTelecastTime, long maxTelecastTime, int graphType) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("statssearchterm", queryString);
	params.put("whereclause", whereClause);
	params.put("jointablestr", joinTableStr);
	params.put("mintelecasttime", (new Long(minTelecastTime)).toString());
	params.put("maxtelecasttime", (new Long(maxTelecastTime)).toString());
	params.put("graphtype", (new Integer(graphType)).toString());
    }
   
    public GraphStatsMessage(String messageString) {
	super(messageString);
    }

    public String getQueryString() {
	return params.get("statssearchterm");
    }

    public String getWhereClause() {
	return params.get("whereclause");
    }

    public String getJoinTableStr() {
	return params.get("jointablestr");
    }

    public long getMinTelecastTime() {
	return Long.parseLong(params.get("mintelecasttime"));
    }

    public long getMaxTelecastTime() {
	return Long.parseLong(params.get("maxtelecasttime"));
    }

    public int getGraphType() {
	return Integer.parseInt(params.get("graphtype"));
    }

    public static boolean isGraphStatsMessage(String messageString) {
	if(messageString.indexOf("\nstatssearchterm:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
