package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;

import java.util.*;

public class UpdateProgramMessage extends IPCSyncMessage {
    //RadioProgramMetadata[] programMetadata = null;
    String[] programIDs = null;

    // general use: source = serviceName, dest = libService
    public UpdateProgramMessage(String source, String dest, String[] programIDs) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	String prefix = "program";
	params.put("updateprogramprefix", prefix);
	params.put("updateprogramlength", Integer.toString(programIDs.length));
	params.put("updateserially", "false");
	for(int i = 0; i < programIDs.length; i++){
	    /*
	    try {
		Metadata.addMetadataToHashtable(programMetadata[i], prefix+i, params);
	    } catch(Exception e) {
		LogUtilities.getDefaultLogger().error("UpdateProgramMessage: Unable to parse metadata", e);
	    }
	    */
	    params.put(prefix+i, programIDs[i]);
	}
	this.programIDs = programIDs;
    }
   
    public UpdateProgramMessage(String messageString) {
	super(messageString);
    }

    public void setUpdateSerially(Boolean set) {
	params.put("updateserially", set.toString());
    }

    public boolean getUpdateSerially() {
	return Boolean.parseBoolean(params.get("updateserially"));
    }

    public String getPrefix() {
	return params.get("updateprogramprefix");
    }

    public int getLength() {
	return Integer.parseInt(params.get("updateprogramlength"));
    }

    public String[] getProgramIDs() {
	if(programIDs != null)
	    return programIDs;
	programIDs = new String[getLength()];
	
	String prefix = params.get("updateprogramprefix");

	for(int i = 0; i < programIDs.length; i++){
	    /*
	    programMetadata[i] = RadioProgramMetadata.getDummyObject("");
	    try {
		Metadata.getMetadataFromHashtable(programMetadata[i], params.get("updateprogramprefix")+i, params);
	    } catch(Exception e) {
		LogUtilities.getDefaultLogger().error("getProgramMetadata: unable to parse metadata", e);
	    }
	    */
	    
	    programIDs[i] = params.get(prefix+i);
	}
	return programIDs;
    }

    public static boolean isUpdateProgramMessage(String messageString) {
	if(messageString.indexOf("\nupdateprogramprefix:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
