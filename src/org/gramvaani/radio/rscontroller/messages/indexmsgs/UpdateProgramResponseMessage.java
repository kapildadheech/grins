package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class UpdateProgramResponseMessage extends IPCSyncResponseMessage {

    public UpdateProgramResponseMessage(String source, String dest, String messageID, int[] response) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("updateprogramresponselength", Integer.toString(response.length));
	for(int i = 0; i < response.length; i++)
	    params.put("updateprogramresponse"+i, Integer.toString(response[i]));
	setMessageID(messageID);
    }
   
    public UpdateProgramResponseMessage(String messageString) {
	super(messageString);
    }

    public int getLength(){
	return Integer.parseInt(params.get("updateprogramresponselength"));
    }

    public int[] getReturnValue() {
	int[] retVals = new int[getLength()];
	for (int i = 0; i < retVals.length; i++)
	    retVals[i] = Integer.parseInt(params.get("updateprogramresponse"+i));
	return retVals;
    }

    public static boolean isUpdateProgramResponseMessage(String messageString) {
	if(messageString.indexOf("\nupdateprogramresponselength:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
