package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;

import java.util.*;

public class UpdateIndexMessage extends IPCSyncMessage {

    // general use: source = serviceName
    public UpdateIndexMessage(String source, String dest) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("updateindex", "dummy");
    }
   
    public UpdateIndexMessage(String messageString) {
	super(messageString);
    }

    public static boolean isUpdateIndexMessage(String messageString) {
	if(messageString.indexOf("\nupdateindex:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
