package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

import java.util.*;

public class AnchorSearchResponseMessage extends IPCSyncResponseMessage {
    Hashtable<String, ArrayList<String>> searchResponse = null;

    public AnchorSearchResponseMessage(String source, String dest, Hashtable<String, ArrayList<String>> searchResponse, String messageID) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	setMessageID(messageID);
	params.put("anchorsearchresponse", "dummy");
	setSearchResponse(searchResponse, params, "_anchor_");
	this.searchResponse = searchResponse;
    }
   
    public AnchorSearchResponseMessage(String messageString) {
	super(messageString);
    }

    public Hashtable<String, ArrayList<String>> getResults() {
	if(searchResponse == null)
	    searchResponse = getSearchResponse(params, "_anchor_");
	return searchResponse;
    }

    public static boolean isAnchorSearchResponseMessage(String messageString) {
	if(messageString.indexOf("\nanchorsearchresponse:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

    private void setSearchResponse(Hashtable<String, ArrayList<String>> searchResponse, Hashtable<String, String> params, String prefix) {
	Enumeration<String> e = searchResponse.keys();
	while(e.hasMoreElements()) {
	    String filterName = e.nextElement();
	    ArrayList<String> anchors = searchResponse.get(filterName);
	    StringBuilder str = new StringBuilder();
	    for(int i = 0; i < anchors.size(); i++) {
		str.append(",");
		str.append(anchors.get(i));
	    }
	    if(str.length() > 0)
		params.put(prefix + filterName, str.substring(1));
	    else
		params.put(prefix + filterName, "");
	}
    }

    private Hashtable<String, ArrayList<String>> getSearchResponse(Hashtable<String, String> params, String prefix) {
	Hashtable<String, ArrayList<String>> searchResponse = new Hashtable<String, ArrayList<String>>();
	Enumeration<String> e = params.keys();
	while(e.hasMoreElements()) {
	    String paramKey = e.nextElement();
	    if(paramKey.startsWith(prefix)) {
		String paramVal = params.get(paramKey);
		String filterName = paramKey.split(prefix)[1];
		ArrayList<String> arr = new ArrayList<String>();
		String[] anchors = paramVal.split(",");
		for(String anchor: anchors)
		    arr.add(anchor);
		searchResponse.put(filterName, arr);
	    }
	}
	return searchResponse;
    }

}
