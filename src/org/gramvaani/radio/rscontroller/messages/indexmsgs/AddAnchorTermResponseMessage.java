package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class AddAnchorTermResponseMessage extends IPCSyncResponseMessage {

    public AddAnchorTermResponseMessage(String source, String dest, String messageID, int response) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("addanchorresponse", (new Integer(response)).toString());
	setMessageID(messageID);
    }
   
    public AddAnchorTermResponseMessage(String messageString) {
	super(messageString);
    }

    public int getReturnValue() {
	return Integer.parseInt(params.get("addanchorresponse"));
    }

    public static boolean isAddAnchorTermResponseMessage(String messageString) {
	if(messageString.indexOf("\naddanchorresponse:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
