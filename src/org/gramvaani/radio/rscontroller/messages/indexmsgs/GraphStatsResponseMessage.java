package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

import java.util.*;

public class GraphStatsResponseMessage extends IPCSyncResponseMessage {

    public GraphStatsResponseMessage(String source, String dest, 
				     long minTelecastTime, long maxTelecastTime, String[] xLabels, long[] dataItems,
				     long approxBinInterval, long maxDataItem,
				     String messageID) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	setMessageID(messageID);
	params.put("statsdataitems", StringUtilities.getCSVFromArray(dataItems));
	params.put("statsxlabels", StringUtilities.getCSVFromArray(xLabels));
	params.put("mintelecasttime", (new Long(minTelecastTime)).toString());
	params.put("maxtelecasttime", (new Long(maxTelecastTime)).toString());
	params.put("approxbininterval", (new Long(approxBinInterval)).toString());
	params.put("maxdataitem", (new Long(maxDataItem)).toString());
    }
   
    public GraphStatsResponseMessage(String messageString) {
	super(messageString);
    }

    public long getMinTelecastTime() {
	return Long.parseLong(params.get("mintelecasttime"));
    }

    public long getMaxTelecastTime() {
	return Long.parseLong(params.get("maxtelecasttime"));
    }

    public String[] getXLabels() {
	return StringUtilities.getArrayFromCSV(params.get("statsxlabels"));
    }

    public long[] getDataItems() {
	return StringUtilities.getLongArrayFromCSV(params.get("statsdataitems"));
    }

    public long getApproxBinInterval() {
	return Long.parseLong(params.get("approxbininterval"));
    }

    public long getMaxDataItem() {
	return Long.parseLong(params.get("maxdataitem"));
    }

    public static boolean isGraphStatsResponseMessage(String messageString) {
	if(messageString.indexOf("\nstatsdataitems:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
