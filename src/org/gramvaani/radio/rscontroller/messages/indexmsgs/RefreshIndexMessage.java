package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class RefreshIndexMessage extends IPCSyncMessage {

    // general use: source = serviceName
    public RefreshIndexMessage(String source, String dest) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("refreshindex", "");
    }
   
    public RefreshIndexMessage(String messageString) {
	super(messageString);
    }

    public static boolean isRefreshIndexMessage(String messageString) {
	if(messageString.indexOf("\nrefreshindex:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
