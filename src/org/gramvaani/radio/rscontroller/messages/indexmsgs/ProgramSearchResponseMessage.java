package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

import java.util.*;

public class ProgramSearchResponseMessage extends IPCSyncResponseMessage {
    String[] searchResponse = null;

    public ProgramSearchResponseMessage(String source, String dest, String[] searchResponse, int numSearchResults, 
					String messageID) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	setMessageID(messageID);
	params.put("programsearchresponse", setSearchResponse(searchResponse));
	params.put("numsearchresults", (new Integer(numSearchResults)).toString());
	this.searchResponse = searchResponse;
    }
   
    public ProgramSearchResponseMessage(String messageString) {
	super(messageString);
    }

    public String[] getResults() {
	if(searchResponse == null)
	    searchResponse = getSearchResponse(params);
	return searchResponse;
    }

    public int getNumSearchResults() {
	return Integer.parseInt(params.get("numsearchresults"));
    }

    public static boolean isProgramSearchResponseMessage(String messageString) {
	if(messageString.indexOf("\nprogramsearchresponse:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

    private String setSearchResponse(String[] searchResponse) {
	StringBuilder str = new StringBuilder();
	for(String program: searchResponse) {
	    str.append(",");
	    str.append(program);
	}
	if(str.length() > 0)
	    return str.substring(1);
	else
	    return "";
    }

    private String[] getSearchResponse(Hashtable<String, String> params) {
	String searchResponseStr = params.get("programsearchresponse");
	return StringUtilities.getArrayFromCSV(searchResponseStr);
    }

}
