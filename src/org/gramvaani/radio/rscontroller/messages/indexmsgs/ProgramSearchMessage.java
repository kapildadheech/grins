package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;

public class ProgramSearchMessage extends IPCSyncMessage {

    // general use: source = serviceName, dest = libService
    public ProgramSearchMessage(String source, String dest, String queryString, String whereClause, String joinTableStr,
				int startingOffset, int maxResults) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("programsearchterm", queryString);
	params.put("whereclause", whereClause);
	params.put("jointablestr", joinTableStr);
	params.put("startingoffset", (new Integer(startingOffset)).toString());
	params.put("maxresults", (new Integer(maxResults)).toString());
    }
   
    public ProgramSearchMessage(String messageString) {
	super(messageString);
    }

    public String getQueryString() {
	return params.get("programsearchterm");
    }

    public String getWhereClause() {
	return params.get("whereclause");
    }

    public String getJoinTableStr() {
	return params.get("jointablestr");
    }

    public int getStartingOffset() {
	return Integer.parseInt(params.get("startingoffset"));
    }

    public int getMaxResults() {
	return Integer.parseInt(params.get("maxresults"));
    }

    public static boolean isProgramSearchMessage(String messageString) {
	if(messageString.indexOf("\nprogramsearchterm:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
