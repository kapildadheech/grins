package org.gramvaani.radio.rscontroller.messages.indexmsgs;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;

import java.util.*;

public class RemoveProgramMessage extends IPCSyncMessage {
    //RadioProgramMetadata[] programMetadata = null;
    String[] programIDs = null;

    // general use: source = serviceName, dest = indexService
    public RemoveProgramMessage(String source, String dest, String[] programIDs) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	String prefix = "program";
	params.put("removeprogramprefix", prefix);
	params.put("removeprogramlength", Integer.toString(programIDs.length));
	params.put("removeserially", "false");
	for (int i = 0; i < programIDs.length; i++){
	    /*
	    try {
		Metadata.addMetadataToHashtable(programMetadata[i], prefix+i, params);
	    } catch(Exception e) {
		LogUtilities.getDefaultLogger().error("RemoveProgramMessage: Unable to parse metadata", e);
	    }
	    */
	    params.put(prefix+i, programIDs[i]);
	}
	this.programIDs = programIDs;
    }
   
    public RemoveProgramMessage(String messageString) {
	super(messageString);
    }

    public void setRemoveSerially(Boolean set) {
	params.put("removeserially",set.toString());
    }

    public boolean getRemoveSerially() {
	return Boolean.parseBoolean(params.get("removeserially"));
    }

    public String getPrefix() {
	return params.get("removeprogramprefix");
    }

    public int getLength(){
	return Integer.parseInt(params.get("removeprogramlength"));
    }

    public String[] getProgramIDs() {
	if(programIDs != null)
	    return programIDs;
	programIDs = new String[getLength()];
	String prefix = params.get("removeprogramprefix");

	for(int i = 0; i < programIDs.length; i++){
	    /*
	    programMetadata[i] = RadioProgramMetadata.getDummyObject("");
	    try {
		Metadata.getMetadataFromHashtable(programMetadata[i], params.get("removeprogramprefix")+i, params);
	    } catch(Exception e) {
		LogUtilities.getDefaultLogger().error("getProgramMetadata: unable to parse metadata", e);
	    }
	    */
	    programIDs[i] = params.get(prefix+i);
	}
	return programIDs;
    }

    public static boolean isRemoveProgramMessage(String messageString) {
	if(messageString.indexOf("\nremoveprogramprefix:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
