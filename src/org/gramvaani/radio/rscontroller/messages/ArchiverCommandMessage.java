package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;

public class ArchiverCommandMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, command = particular command (start, stop), 

    public ArchiverCommandMessage(String source, String dest, String widgetName, 
				  String command, String portRole, String language, String archivingType,
				  boolean addToDatabase) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	params.put("archivercommand", command);
	params.put("portrole", portRole);
	params.put("language", language);
	params.put("archivingtype", archivingType);
	params.put("archiverdatabase", (new Boolean(addToDatabase)).toString());
    }
   
    public ArchiverCommandMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getCommand() {
	return params.get("archivercommand");
    }
    
    public String getPortRole() {
	return params.get("portrole");
    }

    public String getLanguage() {
	return params.get("language");
    }

    public String getArchivingType() {
	return params.get("archivingtype");
    }

    public boolean addToDatabase() {
	return Boolean.parseBoolean(params.get("archiverdatabase"));
    }

    public static boolean isArchiverCommandMessage(String messageString) {
	if(messageString.indexOf("\narchivercommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
