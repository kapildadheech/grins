package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class UploadDoneMessage extends IPCMessage {

    public UploadDoneMessage(String source, String dest, String fileName, String uploaderName) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("uploadedfilename", fileName);
	params.put("uploadername", uploaderName);
    }
   
    public UploadDoneMessage(String messageString) {
	super(messageString);
    }

    public void setWidgetName(String name) {
	params.put("widgetname", name);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getFileName() {
	return params.get("uploadedfilename");
    }

    public String getUploaderName() {
	return params.get("uploadername");
    }

    public static boolean isUploadDoneMessage(String messageString) {
	if(messageString.indexOf("\nuploadedfilename:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }
}