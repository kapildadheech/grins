package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class SyncFileStoreMessage extends IPCMessage {
    
    public static final String SYNC_FILE_STORE = "SYNC_FILE_STORE";

    // source = UIService, dest = serviceName
    public SyncFileStoreMessage(String source, String dest, String command) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("syncfilestorecommand", command);
    }
   
    public SyncFileStoreMessage(String messageString) {
	super(messageString);
    }

    public static boolean isSyncFileStoreMessage(String messageString) {
	if(messageString.indexOf("\nsyncfilestorecommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}