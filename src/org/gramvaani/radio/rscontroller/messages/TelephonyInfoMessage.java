package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.StringUtilities;

import java.util.Hashtable;

public class TelephonyInfoMessage extends IPCMessage {

    protected static final String TRUE = "true";
    protected static final String FALSE = "false";

    // general use: source = TelephonyService, dest = UIService
    public TelephonyInfoMessage(String source, String dest, 
				Hashtable<String,String> calls, 
				boolean isConferenceOn, String onAirCall, String offAirCall) {

	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("calls", StringUtilities.getCSVFromHashtable(calls));
	if(isConferenceOn)
	    params.put("isconferenceon",TRUE);
	else
	    params.put("isconferenceon",FALSE);

	if(onAirCall != null)
	    params.put("onaircall", onAirCall);
	else
	    params.put("onaircall", "");

	if(offAirCall != null)
	    params.put("offaircall", offAirCall);
	else
	    params.put("offaircall", "");
    }
   
    public TelephonyInfoMessage(String messageString) {
	super(messageString);
    }

    public Hashtable<String,String> getCalls() {
	return StringUtilities.getHashtableFromCSV(params.get("calls"));
    }

    public boolean isConferenceOn() {
	String isOn = params.get("isconferenceon");
	if(isOn.equals(TRUE))
	    return true;
	else 
	    return false;
    }

    public String getOnairCall() {
	return params.get("onaircall");
    }

    public String getOffairCall() {
	return params.get("offaircall");
    }

    public static boolean isTelephonyInfoMessage(String messageString) {
	if((messageString.indexOf("\ncalls") != -1) &&
	   (messageString.indexOf("\nisconferenceon") != -1) &&
	   (messageString.indexOf("\nonaircall") != -1) &&
	   (messageString.indexOf("\noffaircall") != -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}