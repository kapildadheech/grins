package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ServiceInterestMessage extends IPCMessage {

    // source = uiService, dest = resourceManager, widgetName = widgetName, service = interested services
    public ServiceInterestMessage(String source, String dest, String widgetName, String... serviceInterests) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname", widgetName);
	params.put("serviceinterests", StringUtilities.getCSVFromArray(serviceInterests));
    }
   
    public ServiceInterestMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String[] getServiceInterests() {
	return StringUtilities.getArrayFromCSV(params.get("serviceinterests"));
    }

    public static boolean isServiceInterestMessage(String messageString) {
	if(messageString.indexOf("\nserviceinterests:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }


}
