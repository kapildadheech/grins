package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

public class InvalidateCacheMessage extends IPCMessage {

    public InvalidateCacheMessage(String source, String dest, String[] widgetList, String objectType,
				  String actionType, String[] objectIDs) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetlist", StringUtilities.getCSVFromArray(widgetList));
	params.put("invalidateobjecttype",objectType);
	params.put("invalidateactiontype", actionType);
	params.put("invalidateobjects", StringUtilities.getCSVFromArray(objectIDs));
    }
   
    public InvalidateCacheMessage(String messageString) {
	super(messageString);
    }

    public String[] getWidgetList() {
	return StringUtilities.getArrayFromCSV(params.get("widgetlist"));
    }

    public String getObjectType() {
	return params.get("invalidateobjecttype");
    }

    public String[] getObjectIDs() {
 	return StringUtilities.getArrayFromCSV(params.get("invalidateobjects"));
    }

    public String getActionType() {
	return params.get("invalidateactiontype");
    }

    public static boolean isInvalidateCacheMessage(String messageString) {
	if(messageString.indexOf("\ninvalidateobjects:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}