package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class MonitorAckMessage extends IPCMessage {

    // general use: sent from MonitorService to widget

    public MonitorAckMessage(String source, String dest, String widgetName, 
			   String ack, String command) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("monitorack", ack);
	params.put("command", command);
	params.put("error", "");
    }
   
    public MonitorAckMessage(String source, String dest, String widgetName, 
			     String ack, String command, String[] error) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("monitorack", ack);
	params.put("command", command);
	params.put("error", StringUtilities.getCSVFromArray(error));
    }

    public MonitorAckMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getAck() {
	return params.get("monitorack");
    }

    public String getCommand() {
	return params.get("command");
    }

    public String[] getError() {
	return StringUtilities.getArrayFromCSV(params.get("error"));
    }
	
    public static boolean isMonitorAckMessage(String messageString) {
	if(messageString.indexOf("\nmonitorack:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}