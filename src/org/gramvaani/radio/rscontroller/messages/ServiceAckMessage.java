package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ServiceAckMessage extends IPCMessage {

    // source = resourceManager, dest = uiService, widgetName = widgetName, activeServices = active services, inactiveServices = inactive services
    public ServiceAckMessage(String source, String dest, String widgetName, String[] activeServices, String[] activeServiceIDs, String[] inactiveServices) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	params.put("widgetname", widgetName);
	params.put("activeservices", StringUtilities.getCSVFromArray(activeServices));
	params.put("activeserviceids", StringUtilities.getCSVFromArray(activeServiceIDs));
	params.put("inactiveservices", StringUtilities.getCSVFromArray(inactiveServices));
    }
   
    public ServiceAckMessage(String messageString) {
	super(messageString);
    }

    public String[] getActiveServices() {
	return StringUtilities.getArrayFromCSV(params.get("activeservices"));
    }
    
    public String[] getActiveServiceIDs() {
	return StringUtilities.getArrayFromCSV(params.get("activeserviceids"));
    }

    public String[] getInactiveServices() {
	return StringUtilities.getArrayFromCSV(params.get("inactiveservices"));
    }

    public static boolean isServiceAckMessage(String messageString) {
	if(messageString.indexOf("\nactiveservices:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

    
}
