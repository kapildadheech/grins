package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ResourceRequestSyncMessage extends IPCSyncMessage {

    // source = serviceName, dest = resourceManager, resources = resources
    public ResourceRequestSyncMessage(String source, String dest, String[] resources, String[] resourceRoles, 
				  boolean persistentRequest) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("syncresourcerequest", StringUtilities.getCSVFromArray(resources));
	params.put("syncresourcerole", StringUtilities.getCSVFromArray(resourceRoles));
	params.put("persistentrequest", (new Boolean(persistentRequest)).toString());
    }
   
    public ResourceRequestSyncMessage(String messageString) {
	super(messageString);
    }

    public String[] getResources() {
	return StringUtilities.getArrayFromCSV(params.get("syncresourcerequest"));
    }

    public String[] getResourceRoles() {
	return StringUtilities.getArrayFromCSV(params.get("syncresourcerole"));
    }

    public boolean isPersistentRequest() {
	return Boolean.parseBoolean(params.get("persistentrequest"));
    }

    public static boolean isResourceRequestSyncMessage(String messageString) {
	if(messageString.indexOf("\nsyncresourcerequest:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }


    
}