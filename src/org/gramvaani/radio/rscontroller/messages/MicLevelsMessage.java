package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class MicLevelsMessage extends IPCMessage {

    // general use: sent from MonitorService to widget

    public MicLevelsMessage(String source, String dest, String widgetName, 
			   String ack, String[] frequencies) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("miclevelsack", ack);
	params.put("miclevels", StringUtilities.getCSVFromArray(frequencies));
    }
   
    public MicLevelsMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getAck() {
	return params.get("miclevelsack");
    }

    public String[] getLevels() {
	return StringUtilities.getArrayFromCSV(params.get("miclevels"));
    }

    public static boolean isMicLevelsMessage(String messageString) {
	if(messageString.indexOf("\nmiclevels:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }
}