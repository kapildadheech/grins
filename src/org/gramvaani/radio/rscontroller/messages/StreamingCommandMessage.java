package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.StringUtilities;

import java.util.Hashtable;

public class StreamingCommandMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, command = particular command (start, stop), 

    public static final String CMD_START = "CMD_START";
    public static final String CMD_UPDATE= "CMD_UPDATE";
    public static final String CMD_STOP  = "CMD_STOP";

    public static final String OPT_NAME = "OPT_NAME";
    public static final String OPT_DESC = "OPT_DESC";
    public static final String OPT_HOST = "OPT_HOST";
    public static final String OPT_PORT = "OPT_PORT";
    public static final String OPT_PROTOCOL = "OPT_PROTOCOL";
    public static final String OPT_MOUNTPOINT = "OPT_MOUNTPOINT";
    public static final String OPT_USERNAME = "OPT_USERNAME";
    public static final String OPT_PASSWORD = "OPT_PASSWORD";
    public static final String OPT_SAMPLERATE = "OPT_SAMPLERATE";
    public static final String OPT_QUALITY = "OPT_QUALITY";
    public static final String OPT_BITRATE = "OPT_BITRATE";
    public static final String OPT_STEREO = "OPT_STEREO";
    public static final String OPT_CODEC = "OPT_CODEC";

    public StreamingCommandMessage(String source, String dest, String widgetName, 
				   String command, Hashtable<String, String> options){
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	params.put("streamingcommand", command);
	params.put("options", StringUtilities.getCSVFromHashtable(options));
    }
   
    public StreamingCommandMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getCommand() {
	return params.get("streamingcommand");
    }
    
    public Hashtable<String,String> getOptions(){
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }

    public static boolean isStreamingCommandMessage(String messageString) {
	if (messageString.indexOf("\nstreamingcommand:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
