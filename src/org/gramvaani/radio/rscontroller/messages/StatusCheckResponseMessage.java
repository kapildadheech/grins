package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class StatusCheckResponseMessage extends IPCMessage {
    
    public static final String TRUE = "TRUE";
    public static final String FALSE = "FALSE";
    
    // source = serviceName, dest = resourceManager, resources = resources
    public StatusCheckResponseMessage(String source, String dest, String widgetName, String id, String response) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname", widgetName);
	params.put("statuscheckresponseid", id);
	params.put("response", response);
    }
   
    public StatusCheckResponseMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName(){
	return params.get("widgetname");
    }

    public String getID() {
	return params.get("statuscheckrequestid");
    }

    public String getResponse(){
	return params.get("response");
    }

    public Hashtable<String, String> getOptions(){
	return StringUtilities.getHashtableFromCSV(params.get("options"));
    }

    public void setOptions(Hashtable<String, String> options){
	params.put("options", StringUtilities.getCSVFromHashtable(options));
    }

    public static boolean isStatusCheckResponseMessage(String messageString) {
	if(messageString.indexOf("\nstatuscheckresponseid:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }
}