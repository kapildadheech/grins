package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class MonitorFrequencyMessage extends IPCMessage {

    // general use: sent from MonitorService to widget

    public MonitorFrequencyMessage(String source, String dest, String widgetName, 
			   String ack, String[] frequencies) {
	super(source, dest);
	messageType = IPCMessage.ACK_MESSAGE;
	if(widgetName != null)
	    params.put("widgetname",widgetName);
	params.put("freqack", ack);
	params.put("frequencies", StringUtilities.getCSVFromArray(frequencies));
    }
   
    public MonitorFrequencyMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getAck() {
	return params.get("freqack");
    }

    public String[] getFrequencies() {
	return StringUtilities.getArrayFromCSV(params.get("frequencies"));
    }

    public static boolean isMonitorFrequencyMessage(String messageString) {
	if(messageString.indexOf("\nfrequencies:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }
}