package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;

import java.util.*;

public class MachinePingResponseMessage extends IPCSyncResponseMessage {

    public MachinePingResponseMessage(String source, String dest, String messageID, Hashtable<String, Boolean> pingResponses) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;

	ArrayList<String> arr = new ArrayList<String>();
	Enumeration<String> e = pingResponses.keys();
	while(e.hasMoreElements()) {
	    String machineID = e.nextElement();
	    arr.add(machineID + ":" + pingResponses.get(machineID).toString());
	}

	params.put("machinepingresponse", StringUtilities.getCSVFromArray(arr.toArray(new String[]{})));
	setMessageID(messageID);
    }
   
    public MachinePingResponseMessage(String messageString) {
	super(messageString);
    }

    public Hashtable<String, Boolean> getMachineConnStatus() {
	String[] machineConnStatuses = StringUtilities.getArrayFromCSV(params.get("machinepingresponse"));
	Hashtable<String, Boolean> machineConn = new Hashtable<String, Boolean>();
	for(String machineConnStatus: machineConnStatuses) {
	    String[] a = machineConnStatus.split(":");
	    String machineID = a[0];
	    String status = a[1];
	    machineConn.put(machineID, new Boolean(status));
	}
	
	return machineConn;
    }

    public static boolean isMachinePingResponseMessage(String messageString) {
	if(messageString.indexOf("\nmachinepingresponse:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}
