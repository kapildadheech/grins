package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;

public class PreemptBackgroundActivityMessage extends IPCMessage {

    // general use: source = resourceManager, dest = libService, activatedService = new service
    public PreemptBackgroundActivityMessage(String source, String dest, String activatedService) {
	super(source, dest);
	messageType = IPCMessage.NOTIFICATION_MESSAGE;
	params.put("activatedservice", activatedService);
    }
   
    public PreemptBackgroundActivityMessage(String messageString) {
	super(messageString);
    }

    public String getActivatedService() {
	return params.get("activatedservice");
    }

    public static boolean isPreemptBackgroundActivityMessage(String messageString) {
	if(messageString.indexOf("\nactivatedservice:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}