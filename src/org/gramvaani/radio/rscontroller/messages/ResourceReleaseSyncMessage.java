package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import java.util.*;
import org.gramvaani.utilities.*;

public class ResourceReleaseSyncMessage extends IPCSyncMessage {

    // source = serviceName, dest = resourceManager, resources = resources
    public ResourceReleaseSyncMessage(String source, String dest, String[] resources) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("syncresourcerelease", StringUtilities.getCSVFromArray(resources));
    }
   
    public ResourceReleaseSyncMessage(String messageString) {
	super(messageString);
    }

    public String[] getResources() {
	return StringUtilities.getArrayFromCSV(params.get("syncresourcerelease"));
    }

    public static boolean isResourceReleaseSyncMessage(String messageString) {
	if(messageString.indexOf("\nsyncresourcerelease:") != -1) {
	    return true;
	} else {
	    return false;
	}
    }

}