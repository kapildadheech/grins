package org.gramvaani.radio.rscontroller.messages;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;

public class MicCommandMessage extends IPCMessage {

    // general use: source, dest, widget = widgetName, command = particular command (start, stop), 

    public MicCommandMessage(String source, String dest, String widgetName, 
			     String command, String srcPortRole, String sinkPortRole, String language) {
	super(source, dest);
	messageType = IPCMessage.SERVICE_COMMAND;
	params.put("widgetname",widgetName);
	params.put("command", command);
	params.put("srcportrole", srcPortRole);
	params.put("sinkportrole",sinkPortRole);
	params.put("language", language);
    }
   
    public MicCommandMessage(String messageString) {
	super(messageString);
    }

    public String getWidgetName() {
	return params.get("widgetname");
    }

    public String getCommand() {
	return params.get("command");
    }
    
    public String getSrcPortRole() {
	return params.get("srcportrole");
    }
    
    public String getSinkPortRole() {
	return params.get("sinkportrole");
    }

    public String getLanguage() {
	return params.get("language");
    }

    public static boolean isMicCommandMessage(String messageString) {
	if((messageString.indexOf("\nd:"+RSController.MIC_SERVICE) != -1) &&
	   (messageString.indexOf("\ncommand:") != -1)) {
	    return true;
	} else {
	    return false;
	}
    }

}
