package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.*;
import org.gramvaani.radio.rscontroller.messages.libmsgs.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.rscontroller.pipeline.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;
import org.gramvaani.radio.medialib.*;

import org.gramvaani.radio.app.RSApp;

import java.io.*;
import java.util.*;

import gnu.getopt.*;

public class ArchiverService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, PipelineListener, 
					DefaultUncaughtExceptionCallback, DiagnosticUtilitiesConnectionCallback {
    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected String machineID;
    protected String serviceInstanceName;
    protected LinkMonitorClient linkMonitor = null;
    protected DiagnosticUtilities diagUtil;
    

    public final static String BCAST_MIC = "mic";

    //messages
    public final static String START_MSG = "START_MSG";
    public final static String START_DIAGNOSTICS_MSG = "START_DIAGNOSTICS_MSG";
    public final static String STOP_MSG  = "STOP_MSG";
    public final static String FORCE_STOP_MSG = "FORCE_MSG";

    //return vals
    public final static String SUCCESS = "SUCCESS";
    public final static String FAILURE = "FAILURE";

    public static final String UNKNOWN = "Unknown";
    
    //state related
    protected final static int CONFIGURING = -1;
    protected final static int LISTENING = 0;
    protected final static int ARCHIVING = 1;
    protected final static int CONNECT_SENT = 2;

    protected int state = CONFIGURING;

    public final static int RECORDING = 0;
    public final static int PLAYOUT = 1;
    public final static String ARCHIVING_TYPE[] = new String[]{"RECORDING", "PLAYOUT"};

    //protected ArchivePipe alsaArchivePipe, jackArchivePipe, winArchivePipe, archivePipe = null;
    protected RSBin bin;

    public static final String clientName = "ArchiverService";
    public static final String jackPortName = RSPipeline.PIPENAME+":in_"+clientName;

    protected String widget;
    protected Hashtable<String, ResourceConfig> resourceConfigs;
    protected LogUtilities logger;
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected String registrationID;
    protected String resourceManagerID = "";
    protected String uiServiceID = "";

    protected TimeKeeper timeKeeper;
    protected FileStoreManager fileStoreManager;
    protected MediaLib medialib;
    protected boolean networkDown;
    protected boolean isArchiverServiceReady = false;

    private String fileName;
    private boolean usingJack;
    private long startTime = -1L;
    private boolean isIndexServiceActive = false;

    private boolean startDiagnostics = false;
    private int MAX_DIAGNOSTICS_LEVELS = 10;

    private String uiRegistrationID = "";

    public ArchiverService(IPCServer ipcServer, StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.ipcServer = ipcServer;
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;
	logger = new LogUtilities(serviceInstanceName);
	logger.info("ArchiverService: Initializing.");
	logger.debug("ArchiverService: Initializing with serviceInstanceName="+serviceInstanceName+" machineID="+machineID);
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	registrationID = GUIDUtils.getGUID();
	archiverConfig();
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	MediaLib.setIPCServer(ipcServer);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null)
	    medialib.setLibraryActivated(true);
	fileStoreManager = new FileStoreManager(stationConfiguration, serviceInstanceName, ipcServer,
						new String[] {StationNetwork.UPLOAD}, 
						timeKeeper,
						StationNetwork.ARCHIVER,
						getName());

	ipcServer.registerIPCNode(serviceInstanceName, this, registrationID);
	initialize();
	logger.debug("ArchiverService: Leaving");
    }

    public ArchiverService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;
	logger = new LogUtilities(serviceInstanceName);
	logger.info("ArchiverService: Initializing.");
	logger.debug("ArchiverService: Initializing with serviceInstanceName: "+serviceInstanceName+" machineID: "+machineID);
	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "ArchiverService");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);
	
	registrationID = GUIDUtils.getGUID();
	archiverConfig();
        ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	MediaLib.setIPCServer(ipcServer);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null)
	    medialib.setLibraryActivated(true);
	fileStoreManager = new FileStoreManager(stationConfiguration, serviceInstanceName, ipcServer,
						new String[] {StationNetwork.UPLOAD}, 
						timeKeeper,
						StationNetwork.ARCHIVER,
						getName());
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	if (ipcServer.registerIPCNode(serviceInstanceName, this, registrationID) == -1) {
	    logger.fatal("ArchiverService: Unable to connect to ipcServer.");
	    connectionError();
	}
	else
	    logger.debug("ArchiverService: Connected to ipcServer.");
	linkMonitor = new LinkMonitorClient(this, serviceInstanceName, stationConfiguration);
	linkMonitor.start();
	initialize();
	logger.debug("ArvcherService: Leaving");
    }

    public void serviceDown(String service){
	stopArchiving();
	setState(CONFIGURING);
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
    }

    public synchronized IPCServer attemptReconnect(){
	//if (ipcServer != null) {
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    timeKeeper.setIPCServer(ipcServer);
	    fileStoreManager.setIPCServer(ipcServer);
	    MediaLib.setIPCServer(ipcServer);
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }
    

    protected void initReadinessCheck() {
	setState(LISTENING);

	if (sendReadyMessage(true) == -1) {
	    logger.error("InitReadinessCheck: Unable to send Ready message");
	    connectionError();
	} else
	    logger.debug("InitReadinessCheck: Ready message sent.");

	ServiceInterestMessage serviceInterestMessage = new ServiceInterestMessage(serviceInstanceName, 
										   RSController.RESOURCE_MANAGER, "", 
										   RSController.INDEX_SERVICE);
	if (sendMessage(serviceInterestMessage) == -1) {
	    logger.error("InitReadinessCheck: Unable to send service interest message");
	    connectionError();
	}
    }

    protected int sendReadyMessage(boolean readiness) {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(serviceInstanceName, RSController.RESOURCE_MANAGER,
								   readiness);
	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isArchiverServiceReady = readiness;
		logger.debug("SendReadyMessage: Ready messsage sent. isArchiverServiceReady=" + isArchiverServiceReady);
		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Message timeout exception.");
	    return -1;
	}
    }

    
    protected void initialize(){
	logger.debug("Initialize: Entering");
	String codec = stationConfiguration.getStringParam(StationConfiguration.ARCHIVER_CODEC);

	RSPipeline.init(stationConfiguration);
	bin = RSBin.ARCHIVER;

	RSPipeline.addPipelineListener(bin, this);
	logger.debug("Initialize: Leaving");
    }

    protected void archiverConfig() {
	logger.debug("ArchiverConfig: Entering");
	ServiceResource[] serviceResources = stationConfiguration.getServiceResources(serviceInstanceName);
	ResourceConfig resourceConfig = null;
	resourceConfigs = new Hashtable<String, ResourceConfig>();
	for (ServiceResource resource: serviceResources){
	    resourceConfig = stationConfiguration.getResourceConfig(resource.resourceName());
	    resourceConfigs.put(resource.resourceRole(), resourceConfig);
	    logger.debug("ArchiverConfig: Resource role: "+resource.resourceRole()+" name: "+resource.resourceName());
	}
	logger.debug("ArchiverConfig: Leaving");
    }

    public synchronized void connectionError() {
	logger.error("ConnectionError:");
	
	reconnectOrExit();
	logger.debug("ConnectionError: Leaving");
    }

    private void reconnectOrExit() {
	logger.debug("ReconnectOrExit: Entering");
	networkDown = true;
	timeKeeper.setIPCServer(null);
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);
	
    }
    
    public synchronized void handleUncaughtException(Thread t, Throwable ex){
	logger.error("HandleUncaughtException: Caught exception:", ex);
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering. From: "+message.getSource()+" Type: "+message.getMessageClass());
	boolean serviceReady = false;

	if (message instanceof ArchiverCommandMessage){
	    ArchiverCommandMessage cmdMessage = (ArchiverCommandMessage) message;
	    this.widget = cmdMessage.getWidgetName();
	    handleArchiverCommandMessage (cmdMessage.getSource(),
					  cmdMessage.getWidgetName(),
					  cmdMessage.getCommand(),
					  cmdMessage.getPortRole(),
					  cmdMessage.getLanguage(),
					  cmdMessage.getArchivingType(),
					  cmdMessage.addToDatabase());
	} else if (message instanceof SyncFileStoreMessage) {
	    syncFileStore();
	} else if (message instanceof ServiceErrorRelayMessage){
	    ServiceErrorRelayMessage errorRelayMessage = (ServiceErrorRelayMessage) message;
	    if (errorRelayMessage.getServiceName().equals(RSController.UI_SERVICE)){
		logger.error("HandleIncomingMessage: ServiceErrorRelayMessage received for UI Service.");
		stopArchiving();
		setState(LISTENING);
	    } else if (errorRelayMessage.getServiceName().equals(RSController.INDEX_SERVICE)) {
		    isIndexServiceActive = false;
	    }
	} else if (message instanceof ArchivingStartResponseMessage) {
	    // XXX ignore for now. Flash success or error to UI.

	} else if (message instanceof ArchivingDoneResponseMessage) {
	    
	    ArchivingDoneResponseMessage archivingMessage = (ArchivingDoneResponseMessage)message;
	    archivingMessage.setSource(archivingMessage.getDest());
	    archivingMessage.setDest(RSController.UI_SERVICE);
	    archivingMessage.setStartTime(startTime);
	    if (sendMessage(archivingMessage) == -1){
		logger.error("HandleArchivingDoneResponseMessage: Unable to send message to UI_SERVICE");
		connectionError();
	    }
	    else
		logger.debug("HandleArchivingDoneResponseMessage: Message sent successfully to UI_SERVICE");

	} else if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	} else if (message instanceof ServiceActivateNotifyMessage) {
	    ServiceActivateNotifyMessage notifyMessage = (ServiceActivateNotifyMessage)message;
	    logger.debug("HandleIncomingMessage: ServiceActivateNotifyMessage received for "+notifyMessage.getActivatedServiceName());
	    if (notifyMessage.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		timeKeeper.sync();
		uiServiceConnected(notifyMessage.getRegistrationID());
	    }
	} else if (message instanceof ServiceAckMessage) {
	    ServiceAckMessage ackMessage = (ServiceAckMessage)message;
	    for (String service: ackMessage.getActiveServices()) {
		if (service.equals(RSController.INDEX_SERVICE))
		    isIndexServiceActive = true;
	    }
	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    for (String service: notifyMessage.getActivatedServices()) {
		if (service.equals(RSController.INDEX_SERVICE))
		    isIndexServiceActive = true;
	    }
	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();

	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(RSController.UI_SERVICE)){
		    uiServiceConnected(activatedIDs[i]);
		    break;
		}
	    }
	    
	} else if (message instanceof TimeKeeperResponseMessage){
	    timeKeeper.handleTimeKeeperResponseMessage((TimeKeeperResponseMessage)message);

	} else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());

	} else if (message instanceof MachineStatusRequestMessage){
	    MachineStatusRequestMessage requestMessage = (MachineStatusRequestMessage)message;
	    handleMachineStatusRequestMessage(requestMessage.getWidgetName(), requestMessage.getCommand(), requestMessage.getOptions());
	} else if (message instanceof StartCleanupMessage) {
	    StartCleanupMessage cleanupMessage = (StartCleanupMessage)message;
	    handleCleanup(cleanupMessage.getSource(), cleanupMessage.getCommands());
	}
    
	if (serviceReady && state() == CONFIGURING) {
	    setState(LISTENING);
	    if (sendReadyMessage(true) == -1) {
		reconnectOrExit();
	    }
	    else
		logger.debug("HandleIncomingMessage: sendReadyMessage succeeded.");
	}
	logger.debug("HandleIncomingMEssage: Leaving");
	return 0;
    }
    
    protected void uiServiceConnected(String newID){
	logger.debug("UIServiceConnected: OldID: "+uiServiceID+" newID: "+newID);
	if (!uiServiceID.equals("") && !(uiServiceID.equals(newID))){
	    stopArchiving();
	    setState(LISTENING);
	}
	uiServiceID = newID;
    }
    
    protected void handleRegistrationAckMessage(boolean ack, String id){
	String oldID = resourceManagerID;
	resourceManagerID = id;
	if (ack) {
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);

	    if (networkDown)
		networkDown = false;
	    if (oldID.equals("")) {
		initReadinessCheck();
	    } else if (!oldID.equals(id)) {
		logger.error("HandleRegistrationAckMessage: RSController went down. Reseting state and releasing resources");
		stopArchiving();
		setState(CONFIGURING);
		initReadinessCheck();
	    } else if (!isArchiverServiceReady) {
		initReadinessCheck();
	    }
	} else {
	    resetService();
	}
    }

    protected void resetService(){
	stopArchiving();
	setState(CONFIGURING);
	resourceManagerID = "";
	registrationID = GUIDUtils.getGUID();
	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	if (ipcServer.handleOutgoingMessage(message) == -1)
	    connectionError();
    }

    private void unableToDeliverMessage(String widgetName, String command) {
	logger.debug("UnableToDeliverMessage: Entering");
	logger.error("UnableToDeliverMessage: To widget: "+widgetName+" Command: "+command);
	stopArchiving();
	setState(LISTENING);
	ArchiverAckMessage ackMessage = new ArchiverAckMessage(getName(), RSController.UI_SERVICE, widgetName, FAILURE, command);
	int retVal = sendMessage(ackMessage);
	if (retVal == -1) {
	    logger.error("UnableToDeliverMessage: Unable to deliver NACK.");
	    reconnectOrExit();
	    return;
	}
	else
	    logger.debug("UnableToDeliverMessage: NACK delivered.");
	logger.debug("UnableToDeliverMessage: Leaving");
    }


    protected void handleArchiverCommandMessage (String source, String widgetName, String command,
						 String portRole, String language, String archivingType, boolean addToDatabase){
	logger.debug("HandleArchiverCommandMessage: Entering. From: "+source+":"+widgetName+" command: "+command);
	boolean archivingFailed = false;
	ResourceConfig resourceConfig = resourceConfigs.get(portRole);
	if (resourceConfig == null){
	    logger.error("HandleArchiverCommandMessage: Error in configuration: Not found portRole: " + portRole);
	    
	    ArchiverAckMessage ackMessage = new ArchiverAckMessage(getName(), RSController.UI_SERVICE, 
								   widgetName, FAILURE, command,
								   new String[] {RSController.ERROR_RESOURCE_ROLE_NOT_FOUND,portRole});
	    if (sendMessage(ackMessage) == -1){
		logger.error("HandleArchiverCommandMessage: Unable to send Nack");
		connectionError();
	    }
	    else
		logger.error("HandleArchiverCommandMessage: Nack sent successfully.");
	    
	}
    

	String portType = resourceConfig.resourceMixer();
	String portName = resourceConfig.resourceDeviceName();
	logger.debug("HandleArchiverCommandMessage: portType="+portType+" portName="+portName);
	switch(state()){

	case LISTENING:

	    if (command.equals(ArchiverService.STOP_MSG)){
		logger.error("HandleArchiverCommandMessage: Not Archiving: "+portType+": "+portName);
	    }
	    else if (command.equals(ArchiverService.START_MSG) || command.equals(ArchiverService.START_DIAGNOSTICS_MSG)){
		logger.info("HandleArchiverCommandMessage: Start archiving: "+portType+": "+portName);
		fileName = null;

		if (command.equals(START_DIAGNOSTICS_MSG))
		    startDiagnostics = true;
		else
		    startDiagnostics = false;

		if (startArchiving (portType, portName, fileName = getFileName())){
			ArchiverAckMessage ackMessage = new ArchiverAckMessage(getName(), RSController.UI_SERVICE, 
									       widgetName, SUCCESS, command);
			startTime = timeKeeper.getRealTime();
			int retVal = sendMessage(ackMessage);
			if (retVal == -1){
 			    logger.error("HandleArchiverCommandMessage: Unable to send ACK.");
			    connectionError();
			    return;
			} else if (retVal == -2) {
			    logger.error("HandleArchiverCommandMessage: Unable to send ACK.");
			    stopArchiving();
			    return;
			} else {
			    logger.info("HandleArchiverCommandMessage: Archiving started.");
			    setState (ARCHIVING);
			}		       
		} else {
		    archivingFailed = true;
		}

		if (archivingFailed){
		    logger.error("HandleArchiverCommandMessage: Archiving Failed.");
		    ArchiverAckMessage ackMessage = new ArchiverAckMessage(getName(), RSController.UI_SERVICE, widgetName, FAILURE, command);
		    int retVal = sendMessage(ackMessage);
		    if (retVal == -1) {
			logger.error("HandleArchiverCommandMessage: Could not send NACK.");
			reconnectOrExit();
			return;
		    }
		} else if (fileName != null && addToDatabase) {
		    handleArchivingStart(serviceInstanceName, RadioProgramMetadata.MIC_TYPE, language, fileName, archivingType);
		}
		
	    }
	    break;

	case ARCHIVING:
	    if (command.equals(ArchiverService.START_MSG) || command.equals(ArchiverService.START_DIAGNOSTICS_MSG)){
		logger.error("HandleArchiverCommandMessage: Cannot start till current archiving is done.");
	    }
	    else if (command.equals(ArchiverService.STOP_MSG)){
		stopArchiving();
		setState(LISTENING);
		ArchiverAckMessage ackMessage = new ArchiverAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
		int retVal = sendMessage(ackMessage);
		if (retVal == -1){
		    logger.error("HandleArchiverCommandMessage: Unable to send ACK.");
		    reconnectOrExit();
		    return;
		}
		else
		    logger.debug("HandleArchiverCommandMessage: ACK sent");


		if (startDiagnostics){
		    startDiagnostics(stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR)+"/"+fileName);
		    startDiagnostics = false;
		}

		if (fileName != null && addToDatabase) {
		    handleArchivingDone(serviceInstanceName, fileName, RadioProgramMetadata.MIC_TYPE, language, archivingType);
		    /*ArchivingDoneMessage archivingMessage = new ArchivingDoneMessage(serviceInstanceName,
										     RSController.LIB_SERVICE,
										     BCAST_MIC, language, 
										     fileName, 
										     archivingType);
		    if (sendMessage(archivingMessage)==-1)
			logger.error("HandleArchiverCommandMessage: Unable to send message to LIB_SERVICE");
		    else
		    logger.debug("HandleArchiverCommandMessage: Message sent successfully to LIB_SERVICE");*/
		} 

	    } else if (command.equals(ArchiverService.FORCE_STOP_MSG)) {
		stopArchiving();
		setState(LISTENING);
	    }
		
	    break;

	default:
	    logger.error("HandleArchiverCommandMessage: Ignoring command: "+command+" since in state: "+state);
	}
	logger.debug("HandleArchiverCommandMessage: Leaving");
    }

    protected void handleMachineStatusRequestMessage(String widget, String command, String[] options){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = ((StationNetwork)stationConfiguration.getStationNetwork()).getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), RSController.UI_SERVICE, widget, command, new String[] {dirName, used, available});
	    
	    if (sendMessage(responseMessage) == -1){
		logger.error("HandleMachineStatusRequestMessage: Unable to send responseMessage.");
		reconnectOrExit();
	    } else {
		logger.debug("HandleMachineStatusRequestMessage: Response sent.");
	    }
	}
    }


    protected void startDiagnostics(String fileName){
	int[] allLevels = RSPipeline.getLevels(fileName);
	
	int numLevels = Math.min (allLevels.length, MAX_DIAGNOSTICS_LEVELS);
	
	String[] levelStrings = new String[numLevels];
	
	for (int i = 0; i < numLevels; i++)
	    levelStrings[i] = Integer.toString(allLevels[i]);
	
	ArchiverLevelsMessage levelsMessage = new ArchiverLevelsMessage(serviceInstanceName,
									RSController.UI_SERVICE,
									RSApp.ARCHIVER_PROVIDER,
									SUCCESS, levelStrings);
	
	if (sendMessage(levelsMessage) == -1)
	    logger.error("StartDiagnostics: Unable to send message to ARCHIVER_PROVIDER");
	else
	    logger.debug("StartDiagnostics: Message sent successfully to ARCHIVER_PROVIDER");
	
	try{
	    (new File(fileName)).delete();
	} catch (Exception e){
	    logger.error("StartDiagnostics: Unable to delete diagnostics temp file: "+fileName, e);
	}
	
    }

    protected String getFileName () {
	return "archivefile."+System.currentTimeMillis() + "." + stationConfiguration.getStringParam(StationConfiguration.ARCHIVER_CODEC);
    }

    protected void handleArchivingStart(String source, String archiveType, String language, String filename, String archivingType) {
	logger.debug("HandleArchivingStart: Entering. archiveType="+archiveType+" language="+language+" filename="+filename);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib == null) {
	    logger.error("HandleArchivingStart: Unable to update database. Database not connected.");
	    return;
	} else {
	    medialib.setLibraryActivated(true);
	}

	if (archiveType.equals(ArchiverService.BCAST_MIC)) {
	    RadioProgramMetadata metadata = new RadioProgramMetadata(filename, 0, "", archiveType, timeKeeper.getRealTime(), 
								     archivingType.equals(ArchiverService.ARCHIVING_TYPE[ArchiverService.RECORDING]) ? 
								     LibService.START_ARCHIVING_RECORD : LibService.START_ARCHIVING_PLAYOUT, 
								     LibService.MEDIA_NOT_INDEXED, "", language,
								     stationConfiguration.getStringParam(StationConfiguration.STATION_NAME));
	    metadata.setTitle("Live recording " + StringUtilities.getDateTimeString(metadata.getCreationTime()));

	    if (medialib.insert(metadata, false) > -1) {
		logger.debug("HandleArchivingStart: Metadata insert successful.");
	    } else {
		logger.debug("HandleArchivingStart: Metadata insert failed.");
	    }
	} else
	    logger.warn("HandleArchivingStart: Unknown ArchiveType.");

    }

    protected void handleArchivingDone(String source, String filename, String archType, String archLanguage, String archivingType) {
	logger.debug("HandleArchivingDone: Entering");

	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib == null) {
	    logger.error("HandleArchivingDone: Unable to update database. Database not connected.");
	    return;
	} else {
	    medialib.setLibraryActivated(true);
	}

	String localStoreDir = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.ARCHIVER);

	final String libFilename = medialib.newBulkItemFilename() + "." + stationConfiguration.getStringParam(StationConfiguration.ARCHIVER_CODEC);
	
	logger.debug("HandleArchivingDone: source="+source+" filename="+filename+" libFilename="+libFilename + " localStoreDir=" + localStoreDir);

	RadioProgramMetadata dummyObject = RadioProgramMetadata.getDummyObject(filename);

	RadioProgramMetadata archivedMetadata = medialib.getSingle(dummyObject);

	String completeFileName = localStoreDir + File.separator + filename;

	String checksum = FileUtilities.getSha1Hex(completeFileName);
	
	if (checksum == null)
	    checksum = "";

	if (archivedMetadata == null) {
	    logger.debug("HandleArchivingDone: No archived metadata found. Inserting metadata.");
	    archivedMetadata = new RadioProgramMetadata(filename, 0, "", archType, timeKeeper.getRealTime(), 
							archivingType.equals(ArchiverService.ARCHIVING_TYPE[ArchiverService.RECORDING]) ?
							LibService.DONE_ARCHIVING_RECORD : 
							LibService.DONE_ARCHIVING_PLAYOUT, 
							LibService.MEDIA_NOT_INDEXED, checksum, archLanguage,
							stationConfiguration.getStringParam(StationConfiguration.STATION_NAME));
	    archivedMetadata.setTitle("Live recording "+StringUtilities.getDateTimeString(archivedMetadata.getCreationTime()));
	    long length = -1L;
	    
	    try{
		length = Long.parseLong(RSPipeline.getMetadata(completeFileName).get(RSPipeline.DURATION));
	    } catch (Exception e){
		logger.error("HandleArchivingDone: Unable to find file length.", e);
	    }
	    archivedMetadata.setLength(length);
	    if (medialib.insert(archivedMetadata, false) > -1) {
		logger.debug("HandleArchivingDone: Metadata insert successful.");
	    } else {
		logger.debug("HandleArchivingDone: Metadata insert failed.");
		return;
	    }

	} else {
	    logger.debug("HandleArchivingDone: Found previously inserted archive metadata.");

	    long length = -1L;
	    
	    try{
		length = Long.parseLong(RSPipeline.getMetadata(localStoreDir + "/" + filename).get(RSPipeline.DURATION));
	    } catch (Exception e){
		logger.error("HandleArchivingDone: Unable to find file length.", e);
	    }
	    archivedMetadata.setLength(length);
	    archivedMetadata.setChecksum(checksum);
	}

	logger.debug("HandleArchivingDone: Updating metadata");

	archivedMetadata.setFilename(libFilename);
	fileStoreManager.clearAllFileStoreFields(archivedMetadata);
	fileStoreManager.setFileStoreField(archivedMetadata, StationNetwork.ARCHIVER, FileStoreManager.UPLOAD_DONE);

	archivedMetadata.setState(archivingType.equals(ArchiverService.ARCHIVING_TYPE[ArchiverService.RECORDING]) ?
						       LibService.DONE_ARCHIVING_RECORD: LibService.DONE_ARCHIVING_PLAYOUT);

	ArchivingDoneResponseMessage archivingResponse = null;
	UpdateProgramMessage updateMessage = null;
	FlushIndexMessage flushMessage = null;
	try {
	    if (FileUtilities.changeFilename(filename, localStoreDir,
					    libFilename, localStoreDir) >= 0 &&
	       medialib.update(dummyObject, archivedMetadata, false) > -1) {
		
		if (archivingType.equals(ArchiverService.ARCHIVING_TYPE[ArchiverService.PLAYOUT])) {
		    PlayoutHistory playoutHistory = new PlayoutHistory(libFilename, 
								       stationConfiguration.getStringParam(StationConfiguration.STATION_NAME),
								       archivedMetadata.getCreationTime());
		    medialib.insert(playoutHistory, false);
		}

		updateMessage = new UpdateProgramMessage(serviceInstanceName, 
							 RSController.INDEX_SERVICE,
							 new String[]{libFilename});
		flushMessage = new FlushIndexMessage (serviceInstanceName, RSController.INDEX_SERVICE);
		archivingResponse = new ArchivingDoneResponseMessage(serviceInstanceName,
								     serviceInstanceName,
								     filename, libFilename, SUCCESS);
		handleIncomingMessage(archivingResponse);
		sendInvalidateCacheMessage(MediaLib.getTableName(dummyObject), 
					   LibService.CACHE_INSERT, 
					   new String[]{libFilename});
	    } else {
		throw new Exception("MediaLib update failed");
	    }
	} catch(Exception e) {
	    logger.error("HandleArchivingDone: Could not update database. Database not connected.");
	    archivingResponse = new ArchivingDoneResponseMessage(serviceInstanceName,
								 serviceInstanceName,
								 filename, libFilename, FAILURE);
	    handleIncomingMessage(archivingResponse);
	    return;
	}

	if (updateMessage != null){
	    final UpdateProgramMessage upMessage = updateMessage;
	    final FlushIndexMessage flushIndexMessage = flushMessage;
	    Thread t = new Thread("HandleArchivingDone:UpdateProgram"){
		    public void run(){
			try{
			    sendSyncCommand(upMessage);
			    logger.info("ArchivingDone: Added new entries in index: "+libFilename);
			} catch (Exception e){
			    logger.error("ArchivingDone: Unable to send UpdateProgramMessage for index update.",e);
			}
			
			try{
			    sendSyncCommand(flushIndexMessage);
			} catch (Exception e) {
			    logger.error("ArchivingDone: Unable to send FlushIndexMessage to Index Service", e);
			}
			if (shouldSendFileToLib()) 
			    pushFileToLib(libFilename, true);
		    }
		};
	    t.start();
	}
	logger.debug("HandleArchivingDone: Leaving");

    }

    protected void syncFileStore() {
	fileStoreManager.syncStoresManually();
    }

    protected void handleCleanup(String source, String[] commands) {
	logger.info("HandleCleanup: Entering. source = " + source + " command length = " + commands.length);

	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib == null) {
	    logger.error("HandleCleanup: Unable to update database. Database not connected.");
	} else {
	    medialib.setLibraryActivated(true);
	    ArrayList<String> oldDataList = new ArrayList<String>();
	    ArrayList<String> newDataList = new ArrayList<String>();
	    
	    for (String command: commands) {
		logger.info("HandleCleanup: Entering. source = " + source + " command = " + command);
		if (command.equals(StartCleanupMessage.INCOMPLETE_ARCHIVING)) {
		    
		    int[] incompleteStates = new int[]{LibService.START_ARCHIVING_PLAYOUT, LibService.START_ARCHIVING_RECORD};
		    int[] completeStates = new int[]{LibService.DONE_ARCHIVING_PLAYOUT, LibService.DONE_ARCHIVING_RECORD};
		    for (int i = 0; i < incompleteStates.length; i++) {
			
			RadioProgramMetadata incompleteMetadata = RadioProgramMetadata.getDummyObject("");
			incompleteMetadata.setState(incompleteStates[i]);
			
			Metadata[] incompleteProgs = medialib.get(incompleteMetadata);
			for (Metadata incompleteProg: incompleteProgs) {
			    RadioProgramMetadata incompleteProgram = (RadioProgramMetadata)incompleteProg;
			    
			    String filename = incompleteProgram.getItemID();
			    String libFilename = medialib.newBulkItemFilename() + "." + 
				stationConfiguration.getStringParam(StationConfiguration.ARCHIVER_CODEC);
			    logger.debug("HandleCleanup: Archiving: filename="+filename+" libFilename="+libFilename);
			    
			    RadioProgramMetadata archivedMetadata = new RadioProgramMetadata(libFilename);
			    archivedMetadata.setState(completeStates[i]);
			    fileStoreManager.clearAllFileStoreFields(archivedMetadata);
			    fileStoreManager.setFileStoreField(archivedMetadata, StationNetwork.ARCHIVER, FileStoreManager.UPLOAD_DONE);
			    long length = -1L;
			    String storeDir = ((StationNetwork)stationConfiguration.getStationNetwork()).getStoreDir(StationNetwork.ARCHIVER);
			    
			    String completeFileName = storeDir + File.separator + filename;

			    String checksum = FileUtilities.getSha1Hex(completeFileName);
			    if (checksum == null)
				checksum = "";

			    try {
				Hashtable<String, String> fileMetadata = RSPipeline.getMetadata(completeFileName);
				length = Long.parseLong(fileMetadata.get(RSPipeline.DURATION));
				String errorMessage = fileMetadata.get(RSPipeline.ERRORSTRING);
				if (errorMessage.contains("Resource not found")) {
				    logger.error("HandleCleanup: File " + filename + " not found in directory " + storeDir + ". Removing it's entry from the database.");
				    medialib.remove(incompleteProgram, false);
				    PlayoutHistory queryPlayoutHistory = new PlayoutHistory();
				    queryPlayoutHistory.setItemID(incompleteProgram.getItemID());
				    medialib.remove(queryPlayoutHistory, false);
				    oldDataList.add(filename);
				    continue;
				}
			    } catch (Exception e){
				logger.error("HandleCleanup: Unable to calculate file length", e);
			    }
	
			    archivedMetadata.setLength(length);
			    archivedMetadata.setChecksum(checksum);

			    ArchivingDoneResponseMessage archivingResponse = null;
			    String baseDir = stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR);
			    try {
				RadioProgramMetadata queryProgram = new RadioProgramMetadata(incompleteProgram.getItemID());
				if (FileUtilities.changeFilename(filename, baseDir, libFilename, baseDir) >= 0 &&
				    medialib.update(queryProgram, archivedMetadata) > -1) {
				    
				    if (i == 0) {
					PlayoutHistory playoutHistory = new PlayoutHistory(libFilename, 
											   stationConfiguration.getStringParam(StationConfiguration.STATION_NAME),
											   incompleteProgram.getCreationTime());
					medialib.insert(playoutHistory, true);
				    }
				    
				    oldDataList.add(filename);
				    newDataList.add(libFilename);
				    
				} else {
				    throw new Exception("HandleCleanup: Archiving: Unable to rename and update medialib.");
				}
			    } catch(Exception e) {
				logger.error("HandleCleanup: Archiving: Failed. File: "+baseDir+"/"+filename, e);
			    }
			} //for all metadata loop
		    } //For incomplete states loop
		}
	    }
	    
	    updatePrograms(oldDataList, newDataList);
	    fileStoreManager.handleCleanup();

	    if (shouldSendFileToLib()) { 
		for (String libFilename : newDataList) {
		    pushFileToLib(libFilename, false);
		}
		fileStoreManager.triggerUpload();
	    }

	} //medialib != null
	
	/*setState(LISTENING);
	if (sendReadyMessage(true) == -1) {
	    logger.error("HandleCleanup: Unable to send Ready message");
	    connectionError();
	}
	else
	logger.debug("HandleCleanup: Ready message sent.");*/
    }

    protected void updatePrograms(final ArrayList<String> oldDataList, final ArrayList<String> newDataList){
	Thread t = new Thread("UpdateIndex Thread"){
		public void run(){
		    int totalItems = oldDataList.size();
		    logger.info("UpdateIndex: Updating Index for: "+totalItems);
		    int numItemsPerMessage = 5;
		    ArrayList<String> removeProgramList = new ArrayList<String>();
		    
		    if (isIndexServiceActive){
			for (int i = 0; i < totalItems; i += numItemsPerMessage){
			    int numItems;
			    if (i <= totalItems - numItemsPerMessage)
				numItems = numItemsPerMessage;
			    else
				numItems = totalItems % numItemsPerMessage;
			    String[] array = new String[numItems];			
			    
			    
			    for (int j = 0; j < numItems; j++){
				array[j] = oldDataList.get(i+j);
			    }
			    
			    RemoveProgramMessage removeMessage = new RemoveProgramMessage(serviceInstanceName,
											  RSController.INDEX_SERVICE,
											  array);
			    RemoveProgramResponseMessage removeResponse;
			    
			    try{
				removeResponse = (RemoveProgramResponseMessage) sendSyncCommand(removeMessage);
				logger.info("UpdateIndex: Removed entries: "+array.length);
			    } catch (Exception e){
				logger.error("UpdateIndex: Unable to send RemoveProgramMessage.", e);
				//connectionError();
			    }
			    
			    
			    //XXX confirm that index need not be flushed between update and remove.
			    
			    
			    for (int j = 0; j < numItems; j++){
				array[j] = newDataList.get(i+j);
			    }
			    
			    UpdateProgramMessage updateMessage = new UpdateProgramMessage(serviceInstanceName, 
											  RSController.INDEX_SERVICE,
											  array);
			    UpdateProgramResponseMessage updateResponse;
			    try{
				updateResponse = (UpdateProgramResponseMessage) sendSyncCommand(updateMessage);
				logger.info("UpdateIndex: Added new entries: "+array.length);
			    } catch (Exception e){
				logger.error("UpdateIndex: Unable to send UpdateProgramMessage.",e);
				//connectionError();
			    }
			    
			    
			}
			
			if (totalItems > 0){
			    FlushIndexMessage flushMessage = new FlushIndexMessage(serviceInstanceName, 
										   RSController.INDEX_SERVICE);
			    FlushIndexResponseMessage responseMessage = null;
			    try{
				responseMessage = (FlushIndexResponseMessage) sendSyncCommand(flushMessage);
			    } catch (Exception e) {
				logger.error("UpdateIndex: Unable to send flush message", e);
			    }
			    
			    if (responseMessage == null)
				logger.error("UpdateIndex: Unable to send flush message.");
			    else if (responseMessage.getReturnValue() < 0)
				logger.error("UpdateIndex: Unable to flush index with error: "+responseMessage.getReturnValue());
			}
		    
		    } //if index service active.
		}//run()
	    };
	t.start();
    }

    //Send file to lib store if there is atleast one consumer store that does not have the file
    protected boolean shouldSendFileToLib() {
	StationNetwork stationNetwork = stationConfiguration.getStationNetwork();
	String[] localStores = stationNetwork.getSameStores(StationNetwork.ARCHIVER);
	boolean playoutFound = false;
	boolean previewFound = false;
	boolean uploadFound = false;

	for (String store : localStores) {
	    if (store.equals(StationNetwork.AUDIO_PLAYOUT))
		playoutFound = true;
	    else if (store.equals(StationNetwork.AUDIO_PREVIEW)) 
		previewFound = true;
	    else if (store.equals(StationNetwork.UPLOAD))
		uploadFound = true;
	}

	if (playoutFound && previewFound && uploadFound)
	    return false;
	else 
	    return true;
    }

    protected void pushFileToLib(String fileName, boolean triggerUpload) {
	fileStoreManager.addFileToUpload(fileName);
	if (triggerUpload)
	    fileStoreManager.triggerUpload();
       
    }

    protected boolean startArchiving (String portType, String portName, String fileName){
	logger.debug("StartArchiving: Entering. Archival starting.");
	PreemptBackgroundActivityMessage preemptMessage = new PreemptBackgroundActivityMessage(serviceInstanceName,
											       RSController.LIB_SERVICE,
											       serviceInstanceName);
	sendMessage(preemptMessage);

	String localStoreDir = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.ARCHIVER);
	fileName = localStoreDir+"/"+fileName;
	RSPipeline.setOutputFile(bin, fileName);
	boolean retval = RSPipeline.play(bin);

	logger.debug("StartArchiving: Leaving with retval="+retval);
	return retval;
    }

    protected void stopArchiving (){
	logger.debug("StopArchiving: Entering");
	if (state == ARCHIVING || state == CONNECT_SENT){
	    logger.info("StopArchiving: Stopping Archival.");
	    if (!RSPipeline.stop(bin)) {
		logger.error("StopArchiving: Unable to stop archiving.");
	    }
	    else
		logger.debug("StopArchiving: Archiving stopped.");
	}
	else
	    logger.warn("StopArchiving: Attempting to stop archiving while in state: "+state);
	logger.debug("StopArchiving: Leaving");
    }

    protected int state(){
	return state;
    }

    protected void setState(int state){
	int previousState = this.state;
	this.state = state;
	logger.info("SetState: Previous state: "+previousState+" Current state: "+state);
    }

    public String getName() {
	return serviceInstanceName;
    }    

    protected void sendInvalidateCacheMessage(String objectType, String actionType, String[] objectIDs) {
	if (ipcServer != null) {
	    InvalidateCacheMessage invalidateMessage = new InvalidateCacheMessage(RSController.ARCHIVER_SERVICE, 
										  RSController.LIB_SERVICE,
										  new String[]{}, 
										  objectType, actionType, objectIDs);
	    int retVal = ipcServer.handleOutgoingMessage(invalidateMessage);
	    if (retVal == -1) {
		logger.error("SendInvalidateCacheMessage: Unable to send message " + objectType + ":" + actionType);
		connectionError();
	    } else if (retVal == -2) {
		logger.error("SendInvalidateCacheMessage: Unable to send message " + objectType + ":" + actionType);
	    } else {
		logger.info("SendInvalidateCacheMessage: Message sent " + objectType + ":" + actionType);
	    }
	} else {
	    logger.error("SendInvalidateCacheMessage: Unable to send message because ipcServer not set");
	    connectionError();
	}
    }

    protected IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null){
	    return null;
	}
	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
	    //XXX is this behavior correct?
	    //connectionError();
	    logger.error("SendSyncCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	}
	return responseMessage;
    }

    protected int sendMessage(IPCMessage message) {
	
	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else 
	    return -1;
    }

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");
	
	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }

    /* Pipeline Callbacks */

    public void playDone(RSBin bin){
	logger.error("PlayDone: Gstreamer pipeline has stopped");
    }

    public void gstError(RSBin bin, String error){
	logger.error("GstError: "+error+" received for "+bin.getName());
	GstreamerErrorMessage archiverErrorMessage = new GstreamerErrorMessage(serviceInstanceName,
									     RSController.UI_SERVICE,
									     widget,
									     error);
	
	if (sendMessage(archiverErrorMessage) == -1){
	    logger.error("GstError: Unable to send GstError message.");
	    reconnectOrExit();
	} else {
	    logger.debug("GstError: GstError message sent.");
	}

	stopArchiving();
	setState(LISTENING);
    }

    public void playDone(RSBin bin, String key){

    }

    public void gstError(RSBin bin, String key, String error){

    }


    public static void main(String args[]) {
	Getopt g = new Getopt("AudioService", args, "cn:");
	int c;
	String configFilePath = "./automation.conf";
	String serviceInstanceName = StationConfiguration.ARCHIVER_SERVICE_DEFAULT_INSTANCE;
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    case 'n':
		serviceInstanceName = g.getOptarg();
		break;
	    }
	}
	
	StationConfiguration stationConfiguration = new StationConfiguration();
	stationConfiguration.readConfig(configFilePath);
	String machineID = stationConfiguration.getServiceInstanceMachineID(RSController.ARCHIVER_SERVICE, serviceInstanceName);
	stationConfiguration.setLocalMachineID(machineID);
	ArchiverService archiverService = new ArchiverService(stationConfiguration, serviceInstanceName, machineID);
    }

}
