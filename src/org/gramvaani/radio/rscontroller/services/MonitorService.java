package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.libmsgs.*;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.rscontroller.pipeline.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;

import java.io.IOException;
import java.io.*;
import java.util.*;

import org.gstreamer.*;
import org.gstreamer.elements.*;
import org.gstreamer.event.*;
import gnu.getopt.*;

public class MonitorService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, 
				       DefaultUncaughtExceptionCallback, DiagnosticUtilitiesConnectionCallback, 
				       PipelineListener {
    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected String machineID;
    protected String serviceInstanceName;
    protected LinkMonitorClient linkMonitor = null;
    protected DiagnosticUtilities diagUtil;
    
    protected String registrationID;
    protected String resourceManagerID = "";
    protected String uiServiceID = "";

    public final static String BCAST_MIC = "mic";
    public final static String PLAYOUT = "playout";
    public final static String MONITOR = "monitor";

    //messages
    public final static String START_MSG = "START_MSG";
    public final static String START_DIAGNOSTICS_MSG = "START_DIAGNOSTICS_MSG";
    public final static String STOP_MSG  = "STOP_MSG";
    public final static String STOP_DIAGNOSTICS_MSG = "STOP_DIAGNOSTICS_MSG";
    public final static String SUCCESS = "SUCCESS";
    public final static String FAILURE = "FAILURE";
    public final static String WAITING = "WAITING";

    //state related
    protected final static int CONFIGURING = -1;
    protected final static int LISTENING = 0;
    protected final static int RESOURCE_REQUESTED = 1;
    protected final static int MONITORING = 2;
    protected final static int CONNECT_SENT = 3;
    protected int state = CONFIGURING;

    static int NUM_PEAK_FREQUENCIES = 5;

    //protected MonitorPipe alsaMonitorPipe, jackMonitorPipe, winMonitorPipe, monitorPipe = null;
    protected RSBin bin;

    public static final String clientName = "MonitorService";
    protected String jackPortName = RSPipeline.PIPENAME+":in_"+clientName;

    protected String widget;
    protected Hashtable<String, ResourceConfig> resourceConfigs;
    protected LogUtilities logger;
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected boolean networkDown = false;
    protected String monitorCommand = null;
    protected long startTime;

    private String srcPortType, srcPortName, sinkPortType, sinkPortName, widgetName, command, sinkPortRole;

    private boolean usingJack, resourceAcquired = false;
    protected boolean isMonitorServiceReady = false;
    protected boolean analyzedRecording = false;
    protected static final String FREQ_ANALYSIS_SCRIPT = "gst_spectrum.py";
    
    public MonitorService(IPCServer ipcServer, StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.ipcServer = ipcServer;
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;
	logger = new LogUtilities(serviceInstanceName);
	logger.debug("MonitorService: Entering.");
	logger.info("MonitorService: Initializing.");
	logger.debug("MonitorService: Initializing with serviceInstanceName="+serviceInstanceName+" machineID="+machineID);
	registrationID = GUIDUtils.getGUID();
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	monitorConfig();
	ipcServer.registerIPCNode(serviceInstanceName, this, registrationID);
	initialize();
	logger.debug("MonitorService: Leaving");
    }

    public MonitorService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;
	logger = new LogUtilities(serviceInstanceName);
	logger.debug("MonitorService: Entering.");	
	logger.info("MonitorService: Initializing.");
	logger.debug("MonitorService: Initializing with serviceInstanceName="+serviceInstanceName+" machineID="+machineID);
	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "MonitorService");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);
	registrationID = GUIDUtils.getGUID();
	monitorConfig();

        ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	if (ipcServer.registerIPCNode(serviceInstanceName, this, registrationID) == -1) {
	    logger.fatal("MonitorService: Unable to connect to ipcServer.");
	    connectionError();
	}
	else
	    logger.debug("MonitorService: Connected to ipcServer.");
		
	linkMonitor = new LinkMonitorClient(this, serviceInstanceName, stationConfiguration);
	linkMonitor.start();
	initialize();
	logger.debug("MonitorService: Leaving");
    }

    public void serviceDown(String service){
	logger.error("Giving up at monitor.");
	stopMonitoring();			
	setState(CONFIGURING);
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
    }

    public synchronized IPCServer attemptReconnect(){
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }

    
    protected void initReadinessCheck() {
	logger.debug("InitReadinessCheck: Entering");
	logger.debug("InitReadinessCheck: UsingJack=false.");
	setState(LISTENING);
	sendReadyMessage(true);
    }

    protected int sendReadyMessage(boolean readiness) {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(serviceInstanceName, 
								   RSController.RESOURCE_MANAGER,
								   readiness);
	
	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isMonitorServiceReady = readiness;
		logger.debug("SendReadyMessage: Ready messsage sent. isMonitorServiceReady=" + isMonitorServiceReady);
		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Message timeout exception.");
	    return -1;
	}
    }


    protected IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null){
	    return null;
	}
	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
	    logger.error("SendSyncCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	    connectionError();
	}
	return responseMessage;
    }
    
    protected void initialize(){
	logger.debug("Initialize: Entering");

	RSPipeline.init(stationConfiguration);
	bin = RSBin.MONITOR;
	
	logger.debug("Initialize: Leaving");
    }

    protected void monitorConfig() {
	logger.debug("MonitorConfig: Entering");
	ServiceResource[] serviceResources = stationConfiguration.getServiceResources(serviceInstanceName);
	ResourceConfig resourceConfig = null;
	resourceConfigs = new Hashtable<String, ResourceConfig>();
	for (ServiceResource resource: serviceResources){
	    resourceConfig = stationConfiguration.getResourceConfig(resource.resourceName());
	    resourceConfigs.put(resource.resourceRole(), resourceConfig);
	    
	    logger.debug("MonitorConfig: Resource role: "+resource.resourceRole()+" name: "+resource.resourceName());
	}
	logger.debug("MonitorConfig: Leaving");
    }

    public synchronized void connectionError() {
	logger.error("ConnectionError:");
	reconnectOrExit();
    }

    private void reconnectOrExit() {
	logger.error("ReconnectOrExit:");
	networkDown = true;
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);
    }
    
    public synchronized void handleUncaughtException(Thread t,Throwable ex){
	logger.debug("HandleUncaughtException: Exiting..");
	//System.exit(-1);
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering. From: "+message.getSource()+" Type: "+message.getMessageClass());
	boolean serviceReady = false;

	if (message instanceof MonitorCommandMessage){
	    MonitorCommandMessage cmdMessage = (MonitorCommandMessage) message;
	    this.widget = cmdMessage.getWidgetName();
	    handleMonitorCommandMessage (cmdMessage.getSource(),
					 cmdMessage.getWidgetName(),
					 cmdMessage.getCommand(),
					 cmdMessage.getSrcPortRole(),
					 cmdMessage.getSinkPortRole(),
					 cmdMessage.getLanguage());
	} else if (message instanceof ResourceAckMessage) {
	    String[] successfulResources = ((ResourceAckMessage)message).getSuccessfulResources();
	    String[] unsuccessfulResources = ((ResourceAckMessage)message).getFailedResources();
	    String[] successfulResourceRoles = ((ResourceAckMessage)message).getSuccessfulResourceRoles();
	    String[] unsuccessfulResourceRoles = ((ResourceAckMessage)message).getUnsuccessfulResourceRoles();
	    handleResourceAckMessage(successfulResources, successfulResourceRoles, unsuccessfulResources, unsuccessfulResourceRoles);
	} else if (message instanceof ServiceErrorRelayMessage){
	    ServiceErrorRelayMessage errorRelayMessage = (ServiceErrorRelayMessage) message;
	    if (errorRelayMessage.getServiceName().equals(RSController.UI_SERVICE)){
		logger.error("HandleIncomingMessage: ServiceErrorRelayMessage received for UI Service.");
		stopMonitoring();
		setState(LISTENING);
	    }
	} else if (message instanceof PreemptResourceMessage) {
	    PreemptResourceMessage prMessage= (PreemptResourceMessage) message;
	    String[] resources = prMessage.getResources();
	    handlePreemptResourceMessage(resources);
	}else if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	} else if (message instanceof ServiceActivateNotifyMessage) {
	    ServiceActivateNotifyMessage notifyMessage = (ServiceActivateNotifyMessage)message;
	    logger.info("HandleIncomingMessage: ServiceActivateNotifyMessage received for "+notifyMessage.getActivatedServiceName());
	    if (notifyMessage.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		// timeKeeper.sync();
		uiServiceConnected(notifyMessage.getRegistrationID());
	    }
	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();

	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(RSController.UI_SERVICE)){
		    uiServiceConnected(activatedIDs[i]);
		}
	    }

	} else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());

	} else if (message instanceof MachineStatusRequestMessage){
	    MachineStatusRequestMessage requestMessage = (MachineStatusRequestMessage)message;
	    handleMachineStatusRequestMessage(requestMessage.getWidgetName(), requestMessage.getCommand(), requestMessage.getOptions());
	}
    
	if (serviceReady && state() == CONFIGURING) {
	    setState(LISTENING);
	    sendReadyMessage(true);
	}
	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }
    
    protected void handleMachineStatusRequestMessage(String widget, String command, String[] options){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = stationConfiguration.getStationNetwork().getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), RSController.UI_SERVICE, widget, command, new String[] {dirName, used, available});
	    
	    if (sendMessage(responseMessage) == -1){
		logger.error("HandleMachineStatusRequestMessage: Unable to send responseMessage.");
		reconnectOrExit();
	    } else {
		logger.debug("HandleMachineStatusRequestMessage: Response sent.");
	    }
	}
    }

    protected void uiServiceConnected(String newID){
	logger.debug("UIServiceConnected: OldID: " + uiServiceID + " newID: " + newID);
	if (!uiServiceID.equals("") && !(uiServiceID.equals(newID))){
	    stopMonitoring();
	    setState(LISTENING);
	}
	uiServiceID = newID;
	//startMonitoring();
    }

    protected void startMonitoring() {
	logger.info("StartMonitoring: Starting monitoring.");
	MonitorCommandMessage message = new MonitorCommandMessage(serviceInstanceName, serviceInstanceName, 
								  "", START_MSG, BCAST_MIC,
								  PLAYOUT, getLanguage());
	handleIncomingMessage(message);
    }

    protected String getLanguage() {
	return stationConfiguration.getStringParam(StationConfiguration.DEFAULT_LANGUAGE);
    }

    protected void handleRegistrationAckMessage(boolean ack, String id){
	String oldID = resourceManagerID;
	resourceManagerID = id;
	if (ack) {
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);
	    if (oldID.equals("")) {
		initReadinessCheck();
	    } else if (!oldID.equals(id)){
		logger.error("HandleRegistrationAckMesage: RSController went down.");
		stopMonitoring();
		setState(CONFIGURING);
		initReadinessCheck();
	    } else if (!isMonitorServiceReady) {
		initReadinessCheck();
	    }
	    startMonitoring();
	} else {
	    resetService();
	}
    }

    public void gstError(RSBin bin, String error, String error1){
	//Not used
    }

    public void playDone(RSBin bin){
	logger.error("PlayDone: Gstreamer pipeline has stopped");
    }
    
    public void playDone(RSBin bin, String key){
	//Not used
    }

    public synchronized void gstError(RSBin bin, String error){
	logger.error("GstError: "+error+" received for "+bin.getName());
	GstreamerErrorMessage monitorErrorMessage = new GstreamerErrorMessage(serviceInstanceName,
									      RSController.UI_SERVICE,
									      widget,
									      error);
	
	if (sendMessage(monitorErrorMessage) == -1){
	    logger.error("GstError: Unable to send GstError message.");
	    reconnectOrExit();
	} else {
	    logger.debug("GstError: GstError message sent.");
	}

	stopMonitoring();
	setState(LISTENING);
    }

    protected void resetService(){
	stopMonitoring();
	setState(CONFIGURING);
	registrationID = GUIDUtils.getGUID();
	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	if (ipcServer.handleOutgoingMessage(message) == -1)
	    connectionError();
	else
	    initReadinessCheck();
    }

    private void unableToDeliverMessage(String widgetName, String command) {
	logger.debug("UnableToDeliverMessage: Entering");
	logger.error("UnableToDeliverMessage: To widget: "+widgetName+" Command: "+command);
	stopMonitoring();
	setState(LISTENING);
	MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, widgetName, FAILURE, command);
	int retVal = sendMessage(ackMessage);
	if (retVal == -1) {
	    logger.error("UnableToDeliverMessage: Unable to deliver NACK.");
	    reconnectOrExit();
	    return;
	}
	else
	    logger.debug("UnableToDeliverMessage: NACK delivered.");
	logger.debug("UnableToDeliverMessage: Leaving");
    }

    protected void handlePreemptResourceMessage(String[] resources) {
	logger.debug("HandlePreemptResourceMessage: Entering");
	switch(state()){
	case MONITORING:
	    String heldResource = resourceConfigs.get(sinkPortRole).resourceName();
	    boolean preempted = false;
	    for (String resource : resources) {
		if (heldResource.equals(resource)){
		    pauseMonitoring();
		    setState(RESOURCE_REQUESTED);  
		    preempted = true;
		    logger.debug("HandlePreemptResourceMessage: resource:"+heldResource+" preempted.");
		}
		else{
		    logger.error("HandlePreemptResourceMessage: Preempt request received for a resource not held:"+resource);
		    preempted = false;
		}
	    }
	    if (preempted){
		PreemptResourceAckMessage message = new PreemptResourceAckMessage(serviceInstanceName, RSController.RESOURCE_MANAGER, "TRUE", new String[]{heldResource});
		
		int retVal = sendMessage(message);
		if (retVal < 0){
		    logger.error("HandlePreemptResourceMessage: Unable to send Preempt ACK.");
		    connectionError();
		    return;
		}
		
		logger.debug("HandlePreemptResourceMessage: Monitoring waiting.");
		
		MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, widgetName, WAITING, command, new String[] {RSController.ERROR_RESOURCE_ACK_FAILED, heldResource});
		retVal = sendMessage(ackMessage);
		if (retVal == -1) {
		    logger.error("HandlePreemptResourceMessage: Could not send ACK to UI Service.");
		    reconnectOrExit();
		    return;
		}
	    }
	    break;
	default:
	    logger.error("HandlePreemptResourceMessage: Preempt request received in the wrong state:"+state());
	}
    }

    protected void handleResourceAckMessage(String[] successfulResources,String[] successfulResourceRoles,String[] unsuccessfulResources,String[] unsuccessfulResourceRoles) {
	boolean monitoringFailed=false;
	boolean monitoringPending=false;
	boolean ack = false;
	boolean found = false;
	if (state != RESOURCE_REQUESTED) {
	    logger.error("HandleResourceAckMessage: Resource ack received in the wrong state:"+state);
	    return;
	}

	String requestedResource = resourceConfigs.get(sinkPortRole).resourceName();
	for (String resource : successfulResources){
	    if (resource.equals(requestedResource)){
		logger.debug("HandleResourceAckMessage: Resource:"+requestedResource+" received as successful resource");
		ack = true;
		found = true;
	    }
	}
	if (!found ){
	    for (String resource : unsuccessfulResources){
		if (resource.equals(requestedResource)){
		    logger.debug("HandleResourceAckMessage: Resource:"+requestedResource+" received as failed resource.");
		    ack = false;
		    found = true;
		}
	    }
	}
	
	if (!found) {
	    logger.error("HandleResourceAckMessage: Resource:"+requestedResource+" did not appear in successful or failed resources.");
	    return;
	}
	if (ack == true) {
	    logger.info("HandleResourceAckMessage: Positive Ack received: "+srcPortType+": "+srcPortName+", "+sinkPortName);
	    resourceAcquired = true;
	    if (srcPortType.equals(ResourceManager.PORT_ALSA) || srcPortType.equals(ResourceManager.PORT_WIN)) {
		if (startMonitoring (srcPortType, srcPortName, sinkPortName)){
		    logger.info("HandleResourceAckMessage: Monitoring started.");
		    setState (MONITORING);

		    MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
		    int retVal = sendMessage(ackMessage);
		    if (retVal == -1){
			logger.error("HandleResourceAckMessage: Unable to send ACK.");
			connectionError();
			return;
		    } else if (retVal == -2) {
			logger.error("HandleResourceAckMessage: Unable to send ACK.");
			//stopMonitoring();
			return;
		    } 		       
		}
		else {
		    monitoringFailed = true;
		}
	    }
	}
	else { //ack==false
	    monitoringFailed = false;
	    monitoringPending = true;
	    logger.error("HandleResourceAckMessage: Resource ack failed.");
	}

	if (monitoringFailed){
	    logger.error("HandleResourceAckMessage: Monitoring Failed.");
	    MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, widgetName, FAILURE, command);
	    int retVal = sendMessage(ackMessage);

	    releaseResources();

	    if (retVal == -1) {
		logger.error("HandleResourceAckMessage: Could not send NACK.");
		reconnectOrExit();
		return;
	    }
	}
	if (monitoringPending){
	    logger.debug("HandleResourceAckMessage: Monitoring waiting.");
	    MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, widgetName, WAITING, command, new String[]{RSController.ERROR_RESOURCE_ACK_FAILED, requestedResource});
	    int retVal = sendMessage(ackMessage);
	    if (retVal == -1) {
		logger.error("HandleResourceAckMessage: Could not send NACK.");
		reconnectOrExit();
		return;
	    }
	}
    }
	
    

    protected void handleMonitorCommandMessage (String source, String widgetName, String command,
						String srcPortRole, String sinkPortRole, String language){
	logger.debug("HandleMonitorCommandMessage: Entering. From: "+source+":"+widgetName+" command: "+command);
	boolean monitoringFailed = false;
	this.sinkPortRole = sinkPortRole;
	ResourceConfig resourceConfig = resourceConfigs.get(srcPortRole);
	if (resourceConfig == null){
	    logger.error("HandleMonitorCommandMessage: Error in configuration: Not found portRole: " + srcPortRole);
	    MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, 
								 widgetName, FAILURE, command,
								 new String[] {RSController.ERROR_RESOURCE_ROLE_NOT_FOUND,srcPortRole});
	    if (sendMessage(ackMessage) == -1){
		logger.error("HandleMonitorCommandMessage: Unable to send Nack");
		connectionError();
	    }
	    else
		logger.error("HandleMonitorCommandMessage: Nack sent successfully.");

	    return;
	}

	if (command.equals(START_MSG))
	    bin = RSBin.MONITOR;
	else if (command.equals(START_DIAGNOSTICS_MSG))
	    bin = RSBin.MONITOR_DIAGNOSTICS;
	
	this.command = command;
	this.widgetName = widgetName;
	srcPortType = resourceConfig.resourceMixer();
	srcPortName = resourceConfig.resourceDeviceName();
	logger.debug("HandleMonitorCommandMessage: srcPortType="+srcPortType+" srcPortName="+srcPortName);

	resourceConfig = resourceConfigs.get(sinkPortRole);
	if (resourceConfig == null){
	    logger.error("HandleMonitorCommandMessage: Error in configuration: Not found portRole: " + sinkPortRole);
	    MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, 
								 widgetName, FAILURE, command,
								 new String[] {RSController.ERROR_RESOURCE_ROLE_NOT_FOUND, sinkPortRole});
	    if (sendMessage(ackMessage) == -1){
		logger.error("HandleMonitorCommandMessage: Unable to send Nack");
		connectionError();
	    }
	    else
		logger.error("HandleMonitorCommandMessage: Nack sent successfully.");

	    return;
	}

	sinkPortName = resourceConfig.resourceDeviceName();
	sinkPortType = resourceConfig.resourceMixer();
	logger.debug("HandleMonitorCommandMessage: sinkPortName="+sinkPortName+" sinkPortRole="+sinkPortRole);

	switch(state()){

	case LISTENING:

	    if (command.equals(MonitorService.STOP_MSG) || command.equals(MonitorService.STOP_DIAGNOSTICS_MSG)){
		logger.error("HandleMonitorCommandMessage: Stop command received when not monitoring: "+srcPortType+": "+srcPortName+", "+sinkPortName);
	    }
	    else if (command.equals(MonitorService.START_MSG) || command.equals(MonitorService.START_DIAGNOSTICS_MSG)){
		ResourceRequestMessage resourceRequestMessage = new ResourceRequestMessage(serviceInstanceName,
											   RSController.RESOURCE_MANAGER,
											   new String[] {resourceConfig.resourceName()}, 
											   new String[] {sinkPortRole}, 
											   true);
		if (sendMessage(resourceRequestMessage) == -1) {
		    logger.error("HandleMonitorCommandMessage: Unable to send ResourceRequest message.");
		    reconnectOrExit();
		}
		else {
		    logger.debug("HandleMonitorCommandMessage: ResourceResquest message sent.");
		    setState(RESOURCE_REQUESTED);
		    monitorCommand = command;
		}
	    }
	
	    break;

	case MONITORING:
	    if (command.equals(MonitorService.START_MSG) || command.equals(MonitorService.START_DIAGNOSTICS_MSG)){
		logger.warn("HandleMonitorCommandMessage: Already monitoring. Command:" + command);
	    }
	    else if (command.equals(MonitorService.STOP_MSG) || command.equals(STOP_DIAGNOSTICS_MSG)){
		setState(LISTENING);
		stopMonitoring();
		MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
		int retVal = sendMessage(ackMessage);
		if (retVal == -1){
		    logger.error("HandleMonitorCommandMessage: Unable to send ACK.");
		    reconnectOrExit();
		    return;
		}
		else
		    logger.debug("HandleMonitorCommandMessage: ACK sent");

	    }
		
	    break;
	case RESOURCE_REQUESTED:
	    if (command.equals(MonitorService.START_MSG) || command.equals(MonitorService.START_DIAGNOSTICS_MSG)){
		logger.warn("HandleMonitorCommandMessage: Already in resource requested state. Command:" + command);
	    }
	    else if (command.equals(MonitorService.STOP_MSG) || command.equals(STOP_DIAGNOSTICS_MSG)){
		setState(LISTENING);
		stopMonitoring();
		MonitorAckMessage ackMessage = new MonitorAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
		int retVal = sendMessage(ackMessage);
		if (retVal == -1){
		    logger.error("HandleMonitorCommandMessage: Unable to send ACK.");
		    reconnectOrExit();
		    return;
		}
		else
		    logger.debug("HandleMonitorCommandMessage: ACK sent");

	    }
	    break;
	}
	
	logger.debug("HandleMonitorCommandMessage: Leaving");
    }

    protected boolean startMonitoring (String portType, String srcPortName, String sinkPortName){
	/*
	if (portType.equals(ResourceManager.PORT_JACK)) {
	    monitorPipe = jackMonitorPipe;
	} else if (portType.equals(ResourceManager.PORT_ALSA)) {
	    monitorPipe = alsaMonitorPipe;
	} else {
	    monitorPipe = winMonitorPipe;
	}

	boolean retval=monitorPipe.start(srcPortName, sinkPortName);
	*/

	startTime = System.currentTimeMillis();
	analyzedRecording = false;
	/*if (bin == RSBin.MONITOR)
	    RSPipeline.initMonitor();
	else if (bin == RSBin.MONITOR_DIAGNOSTICS)
	RSPipeline.initMonitorDiagnostics();*/

	RSPipeline.addPipelineListener(bin, this);
	
	boolean retval = RSPipeline.play(bin);
	
	logger.debug("StartMonitoring: Leaving with retval="+retval);
	return retval;
    }

    public void pauseMonitoring(){
	logger.debug("PauseMonitoring: Entering");

	if (!RSPipeline.stop(bin)) {
	    logger.error("PauseMonitoring: Unable to stop monitoring.");
	}
	else
	    logger.debug("PauseMonitoring: Monitoring stopped.");

	logger.debug("PauseMonitoring: Leaving");
    }

    protected void stopMonitoring() {
	logger.debug("StopMonitoring: Entering");

	if (!RSPipeline.stop(bin)) {
	    logger.error("StopMonitoring: Unable to stop monitoring.");
	} else {
	    logger.debug("StopMonitoring: Monitoring stopped.");
	    if (bin == RSBin.MONITOR_DIAGNOSTICS && !analyzedRecording){
		analyzeRecording(System.currentTimeMillis() - startTime);
		analyzedRecording = true;
	    }
	}

	releaseResources();
	logger.debug("StopMonitoring: Leaving");
    }
    
    protected synchronized void releaseResources() {
	if (!resourceAcquired)
	    return;

	ResourceConfig resourceConfig = resourceConfigs.get(sinkPortRole);
	ResourceReleaseMessage resourceReleaseMessage = new ResourceReleaseMessage(serviceInstanceName,
										   RSController.RESOURCE_MANAGER,
										   new String[] {resourceConfigs.get(sinkPortRole).resourceName()});
	if (sendMessage(resourceReleaseMessage) == -1) {
	    logger.error("ReleaseResources: Unable to send ResourceRelease message.");
	    reconnectOrExit();
	    return;
	}
	else
	    logger.debug("ReleaseResources: ResourceRelease message sent.");
	
	resourceAcquired = false;
    }

    protected void analyzeRecording(long minDuration){
	int[] peakFrequencies = RSPipeline.getPeakFrequencies(RSPipeline.MONITOR_DIAG_FILE);
	logger.info("AnalyzeRecording: length=" + peakFrequencies.length + " peakfrequencies=" + peakFrequencies[0]);
	int length = Math.min(peakFrequencies.length, NUM_PEAK_FREQUENCIES);
	String[] freqStrings = new String[length];
	
	for (int i = 0; i < length; i++)
	    freqStrings[i] = Integer.toString(peakFrequencies[i]);

	MonitorFrequencyMessage freqMessage = new MonitorFrequencyMessage(getName(), RSController.UI_SERVICE, RSApp.MONITOR_PROVIDER, SUCCESS, freqStrings);
	if (sendMessage(freqMessage) == -1){
	    logger.error("AnalyzeRecording: Unable to send frequency message");
	    reconnectOrExit();
	} else {
	    logger.debug("AnalyzeRecording: Frequency message sent.");
	}

	try{
	    (new File(RSPipeline.MONITOR_DIAG_FILE)).delete();
	} catch (Exception e){
	    logger.error("Analyze Recording: Unable to remove file: ", e);
	}
    }
    

    /*
    protected void analyzeRecording(long minDuration){
	final double duration = 1.0*minDuration/(2*1000);
	logger.info("AnalyzeRecording: Analyzing with period: "+minDuration);
	Thread t = new Thread(){
		public void run(){
		    MonitorFrequencyMessage freqMessage = null;
		    try{
			String scriptsDir = stationConfiguration.getStringParam(StationConfiguration.SCRIPTS_DIR);
			Process process = Runtime.getRuntime().exec(scriptsDir+"/"+FREQ_ANALYSIS_SCRIPT+" " +RSPipeline.MONITOR_DIAG_FILE+" "+duration);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String[] freqStrings = reader.readLine().replace('[',' ').replace(']',' ').split(", ");
			String[] magStrings = reader.readLine().replace('[',' ').replace(']',' ').split(", ");
			process.destroy();
			
			freqMessage = new MonitorFrequencyMessage(getName(), RSController.UI_SERVICE, RSApp.MONITOR_PROVIDER, SUCCESS, freqStrings);
			if (sendMessage(freqMessage) == -1){
			    logger.error("AnalyzeRecording: Unable to send frequency message");
			    reconnectOrExit();
			} else {
			    logger.debug("AnalyzeRecording: Frequency message sent.");
			}
			
		    } catch (Exception e) {
			logger.error("AnalyzeRecording: Could not run script to calculate peak frequency.", e);
			
			freqMessage = new MonitorFrequencyMessage(getName(), RSController.UI_SERVICE, RSApp.MONITOR_PROVIDER, FAILURE, new String[] {});
			if (sendMessage(freqMessage) == -1){
			    logger.error("AnalyzeRecording: Unable to send frequency failed message");
			    reconnectOrExit();
			} else {
			    logger.debug("AnalyzeRecording: Frequency failed message sent.");
			}
			
		    }
		}
	    };
	t.start();
    }
    */

    protected int state(){
	return state;
    }

    protected void setState(int state){
	int previousState = this.state;
	this.state = state;
	logger.info("SetState: Previous state: "+previousState+" Current state: "+state);
    }

    public String getName() {
	return serviceInstanceName;
    }    

    protected int sendMessage(IPCMessage message) {

	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");

	return sendMessage(message);
    }

    public static void main(String args[]) {
	Getopt g = new Getopt("MonitorService", args, "cn:");
	int c;
	String configFilePath = "./automation.conf";
	String serviceInstanceName = StationConfiguration.MONITOR_SERVICE_DEFAULT_INSTANCE;
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    case 'n':
		serviceInstanceName = g.getOptarg();
		break;
	    }
	}
	StationConfiguration stationConfiguration = new StationConfiguration();
	stationConfiguration.readConfig(configFilePath);
	String machineID = stationConfiguration.getServiceInstanceMachineID(RSController.MONITOR_SERVICE, serviceInstanceName);
	stationConfiguration.setLocalMachineID(machineID);

	MonitorService monitorService = new MonitorService(stationConfiguration, serviceInstanceName, machineID);
							   
													  
    }

}

/*
class MonitorPipe implements Runnable, Bus.ERROR{
    Thread thread;
    Pipeline pipe;
    String portType, clientName, srcPortName, sinkPortName;
    Element audioSrc, audioConvert, audioSink;
    LogUtilities logger;

    public MonitorPipe(String portType, String clientName, String logPrefix){
	this.portType = portType;
	this.clientName = clientName;
	logger = new LogUtilities(logPrefix+":MonitorPipe");
	logger.debug("MonitorPipe: Creating new pipeline. ClientName="+clientName);
	thread = new Thread(this, clientName);
	thread.start();
	while (true){
	    try{
		Thread.sleep(500);
		break;
	    }catch (Exception e){
		logger.debug("MonitorPipe: ThreadInterrupted.");
	    }
	}
    }

    public void run(){
	logger.debug("Run: Entering");
	
	int attempt = 0;
	String[] args;
	while (attempt < 3){
	    try{
		args = Gst.init("RSController", new String[] {});
		break;
	    }catch(Exception e){
		attempt++;
		logger.error("Run: Gst Init failed.",e);
	    }
	}
	if (attempt == 3){
	    logger.fatal("Run: Unable to initialize Gstreamer.");
	    //System.exit(-1);
	}

	pipe = new Pipeline ("MonitorPipeline");
	try {
	    if (portType.equals(ResourceManager.PORT_JACK)){
		audioSrc = ElementFactory.make("jackaudiosrc",clientName);
		audioSrc.set("connect", "0");
	    }
	    else if (portType.equals(ResourceManager.PORT_ALSA)) {
		audioSrc = ElementFactory.make("alsasrc","alsasrc");
	    } 
	    else if (portType.equals(ResourceManager.PORT_WIN)) {
		audioSrc = ElementFactory.make("dshowaudiosrc", "dshowaudiosrc");
	    }
	    audioConvert = ElementFactory.make("audioconvert", "audioconvert");
	    audioSink = ElementFactory.make("alsasink", "alsasink");
	}catch(Exception e){
	    logger.error("Run: Could not create all gstreamer elements.", e);
	}
	pipe.addMany(audioSrc, audioConvert, audioSink);
	pipe.linkMany(audioSrc, audioConvert, audioSink);
	pipe.setState(State.NULL);

	pipe.getBus().connect(this);
	
	logger.debug("Run: Leaving");
	logger.info("Run: Gstreamer entering Gst.main.");
	Gst.main();
	
	logger.error("Run: Error in Gstreamer: Exited from Gst.main.");
	logger.fatal("Run: Error in Gstreamer: Exited from Gst.main.");
    }

    public void errorMessage(GstObject source, int errorCode, String error){
	logger.error("BusError: "+error);
    }

    public synchronized boolean start(String srcPortName, String sinkPortName){
	logger.debug("Start: Entering srcPortName="+srcPortName+" sinkPortName="+sinkPortName);
	if (portType.equals(ResourceManager.PORT_JACK)){
	    if (srcPortName.matches("in_.*"))
		this.srcPortName = srcPortName.substring(3);
	    else
		this.srcPortName = srcPortName;
	    audioSrc.set("name", this.srcPortName);
	}
	else
	    audioSrc.set("device",srcPortName);

	audioSink.set("device", sinkPortName);

	StateChangeReturn stateChangeReturn = pipe.setState(State.PLAYING);

	if (stateChangeReturn == StateChangeReturn.FAILURE){
	    logger.error("Start: Leaving. Gstreamer: Unable to start pipeline.");
	    return false;
	} else {
	    logger.info("Start: Leaving. Gstreamer: Started pipeline successfully.");
	    return true;
	}
    }
    
    
    public synchronized boolean stop(){
	if (pipe.setState(State.NULL) == StateChangeReturn.FAILURE){
	    logger.error("Stop: Gstreamer: Unable to stop pipeline.");
	    return false;
	} else {
	    logger.info("Stop: Gstreamer: Stopped pipeline successfully.");
	    return true;
	}
    }

}

*/