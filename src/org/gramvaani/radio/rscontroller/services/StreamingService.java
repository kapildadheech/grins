package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.rscontroller.pipeline.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;
import org.gramvaani.radio.app.RSApp;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.gstreamer.*;
import org.gstreamer.elements.*;
import org.gstreamer.event.*;

import gnu.getopt.*;


public class StreamingService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, 
					 DefaultUncaughtExceptionCallback, DiagnosticUtilitiesConnectionCallback, 
					 PipelineListener {
    

    public static final String CODEC_MP3 = "MP3";
    public static final String CODEC_OGV = "Ogg/Vorbis";
    public static final String CODEC_OGS = "Ogg/Speex";

    public static final String STREAMING_ERROR_CONNECTION = "STREAMING_ERROR_CONNECTION";
    public static final String STREAMING_ERROR_LOGIN 	  = "STREAMING_ERROR_LOGIN";
    public static final String STREAMING_ERROR_SOCKET 	  = "STREAMING_ERROR_SOCKET";
    public static final String STREAMING_ERROR_RECONNECTION= "STREAMING_ERROR_RECONNECTION";
    public static final String STREAMING_ERROR_UNKNOWN	  = "STREAMING_ERROR_UNKNOWN";

    IPCServer ipcServer;
    StationConfiguration stationConfig;
    String serviceInstanceName;
    String machineID, widgetName;
    String registrationID, resourceManagerID, uiServiceID ="";
    LinkMonitorClient linkMonitor;
    LogUtilities logger;
    DiagnosticUtilities diagUtil;
    DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;

    
    Hashtable<String, ResourceConfig> resourceConfigs;
    RSBin bin;
    int state;
    boolean networkDown = false;
    boolean isStreamingServiceReady = false;
    StateHandler<States, Events, Signals> stateHandler;

    static final int CONFIGURING = -1;

    static final String SRC_PORT_ROLE = "streaming_capture";

    public StreamingService(IPCServer ipcServer, StationConfiguration stationConfig, String serviceInstanceName, String machineID){
	this.ipcServer = ipcServer;
	this.stationConfig = stationConfig;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;

	logger = new LogUtilities(serviceInstanceName);

	logger.debug("StreamingService: Initializing.");
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfig, ipcServer);
	registrationID = GUIDUtils.getGUID();
	ipcServer.registerIPCNode(serviceInstanceName, this, registrationID);
	streamingConfig();
	initializePipeline();
    }

    public StreamingService(StationConfiguration stationConfig, String serviceInstanceName, String machineID){
	this.stationConfig = stationConfig;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;

	logger = new LogUtilities(serviceInstanceName);

	logger.debug("StreamingService: Initializing.");
	registrationID = GUIDUtils.getGUID();

	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, serviceInstanceName);
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);
	
	ipcServer = new IPCServerStub(stationConfig.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfig.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfig.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfig.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfig.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfig, ipcServer);

	if (ipcServer.registerIPCNode(serviceInstanceName, this, registrationID) == -1) {
	    logger.fatal("StreamingService: Unable to connect to ipcServer.");
	    connectionError();
	}
	else 
	    logger.debug("StreamingService: Connected to ipcServer.");
	
	linkMonitor = new LinkMonitorClient(this, serviceInstanceName, stationConfig);
	linkMonitor.start();
	streamingConfig();
	initializePipeline();
    }

    String portType, deviceName;
    void streamingConfig() {
	ServiceResource[] serviceResources = stationConfig.getServiceResources(serviceInstanceName);
	ResourceConfig resourceConfig = null;
	resourceConfigs = new Hashtable<String, ResourceConfig>();
	for (ServiceResource resource: serviceResources){
	    resourceConfig = stationConfig.getResourceConfig(resource.resourceName());
	    resourceConfigs.put(resource.resourceRole(), resourceConfig);
	    logger.debug("StreamingConfig: Resource role: "+resource.resourceRole()+" name: "+resource.resourceName());
	}
	
	resourceConfig = resourceConfigs.get(SRC_PORT_ROLE);
	if (resourceConfig != null){
	    portType = resourceConfig.resourceMixer();
	    deviceName = resourceConfig.resourceDeviceName();
	} else {
	    logger.error("StreamingConfig: Unable to get resource config");
	}

	stateHandler = new StateHandler<States, Events, Signals>(this, 
								 States.class, Events.class, Signals.class, 
								 States.READY);
    }

    void initializePipeline(){
	RSPipeline.init(stationConfig);
	bin = RSBin.STREAMING;
	RSPipeline.addPipelineListener(bin, this);
    }

    /*
    void disableService(){
	
	stopStreaming();
    }
    */

    void setState(int state){
	int previousState = this.state;
	this.state = state;
	logger.info("SetState: Previous state: "+previousState+" Current state: "+state);
    }

    int sendMessage(IPCMessage message) {
	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else 
	    return -1;
    }
    
    int sendReadyMessage(boolean readiness) {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(serviceInstanceName, RSController.RESOURCE_MANAGER,
								   readiness);
	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isStreamingServiceReady = readiness;
		logger.debug("SendReadyMessage: Ready messsage sent. isStreamingServiceReady=" + isStreamingServiceReady);
		return 0;
	    }
	} catch (Exception e) {
	    logger.error("SendReadyMessage: Message timeout exception.");
	    return -1;
	}
    }

    IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null){
	    logger.error("SendSyncCommand: Null IPCServer");
	    return null;
	}
	
	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
	    logger.error("SendSyncCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	}

	return responseMessage;
    }

    /* Message Handlers */
    
    @MessageHandler
    void handleRegistrationAckMessage(RegistrationAckMessage msg){
	String id = msg.getRegistrationID();
	String oldID = resourceManagerID;
	boolean sendMessage = false;
	resourceManagerID = id;
	
	if (msg.getAck()) {
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);	    

	    if (networkDown)
		networkDown = false;

	    if (!isStreamingServiceReady) {
		sendReadyMessage(true);
	    }

	} else {
	    resetService();
	}
    }

    void resetService(){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	sendMessage(message);
    }

    @MessageHandler
    void handleMachineStatusRequestMessage(MachineStatusRequestMessage msg){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	String command = msg.getCommand();

	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = stationConfig.getStationNetwork().getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), 
											    RSController.UI_SERVICE, 
											    msg.getWidgetName(), 
											    msg.getCommand(), 
											    new String[] {dirName, 
													  used, 
													  available});
	    
	    if (sendMessage(responseMessage) == -1){
		logger.error("HandleMachineStatusRequestMessage: Unable to send responseMessage.");
		connectionError();
	    } else {
		logger.debug("HandleMachineStatusRequestMessage: Response sent.");
	    }
	}

    }

    @MessageHandler
    void handleServiceNotifyMessage(ServiceNotifyMessage msg){
	String[] activatedServices = msg.getActivatedServices();
	String[] activatedIDs = msg.getActivatedIDs();

	for (int i = 0; i < activatedServices.length; i++){
	    if (activatedServices[i].equals(RSController.UI_SERVICE)){
		uiServiceConnected(activatedIDs[i]);
	    }
	}
    }

    void uiServiceConnected(String newID){
	logger.debug("UIServiceConnected: OldID: " + uiServiceID + " newID: " + newID);
	if (!uiServiceID.equals("") && !(uiServiceID.equals(newID))){
	    stateHandler.handleEvent(Events.RESET);
	}
	uiServiceID = newID;
    }

    @MessageHandler
     void handleServiceActivateNotifyMessage(ServiceActivateNotifyMessage msg){
	if (msg.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		uiServiceConnected(msg.getRegistrationID());
	}
    }

    @MessageHandler
    void handleServiceErrorRelayMessage(ServiceErrorRelayMessage msg){
	if (msg.getServiceName().equals(RSController.UI_SERVICE)){
	    disableService();
	}
    }
    
    @MessageHandler
    void handleStreamingCommandMessage(StreamingCommandMessage msg){
	this.widgetName = msg.getWidgetName();
	String command = msg.getCommand();

	if (command == null || command.equals("")){
	    logger.error("HandleStreamingCommandMessage: Null command");
	    return;
	}

	if (command.equals(StreamingCommandMessage.CMD_START)){
	    stateHandler.handleEvent(Events.CMD_START, msg.getOptions());

	} else if (command.equals(StreamingCommandMessage.CMD_UPDATE)){
	    stateHandler.handleEvent(Events.CMD_UPDATE, msg.getOptions());

	} else if (command.equals(StreamingCommandMessage.CMD_STOP)){
	    stateHandler.handleEvent(Events.CMD_STOP);

	}
    }

    static final int MAX_VORBIS_BITRATE = 250001;

    Hashtable<String, String> currentOptions;

    @StateEventHandler(events="Events.CMD_START")
    StateHandler.Transition startStreaming(Hashtable<String, String> options){
	if (options == null){
	    logger.error("StartStreaming: Null options");
	    return null;
	}

	currentOptions = options;

	String name = options.get(StreamingCommandMessage.OPT_NAME);
	String desc = options.get(StreamingCommandMessage.OPT_DESC);
	String host = options.get(StreamingCommandMessage.OPT_HOST);
	if (host == null || host.equals(""))
	    host = "localhost";

	int port = 8000;
	try {
	    port = Integer.parseInt(options.get(StreamingCommandMessage.OPT_PORT));
	} catch (Exception e){}

	String protocol = options.get(StreamingCommandMessage.OPT_PROTOCOL);
	String mountpoint = options.get(StreamingCommandMessage.OPT_MOUNTPOINT);
	String username = options.get(StreamingCommandMessage.OPT_USERNAME);
	if (username == null || username.equals(""))
	    username = "source";
	String password = options.get(StreamingCommandMessage.OPT_PASSWORD);
	if (password == null || password.equals(""))
	    password = "hackme";

	int sampleRate = -1;
	try {
	    sampleRate = Integer.parseInt(options.get(StreamingCommandMessage.OPT_SAMPLERATE));
	} catch (Exception e){}

	int quality  	= 9 - Integer.parseInt(options.get(StreamingCommandMessage.OPT_QUALITY));
	boolean stereo  = Boolean.parseBoolean(options.get(StreamingCommandMessage.OPT_STEREO));
	int bitrate     = Integer.parseInt(options.get(StreamingCommandMessage.OPT_BITRATE));
	String codec    = options.get(StreamingCommandMessage.OPT_CODEC);
	
	if (codec.equals(StreamingService.CODEC_OGV)){
	    bitrate *= 1000;
	    if (bitrate > MAX_VORBIS_BITRATE)
		bitrate = MAX_VORBIS_BITRATE;
	} else if (codec.equals(StreamingService.CODEC_OGS)) {

	} else if (codec.equals(StreamingService.CODEC_MP3)){

	}

	RSPipeline.initStreaming(name, desc, host, port, protocol,
				 mountpoint, username, password, 
				 sampleRate, quality, stereo,
				 bitrate, codec, portType, deviceName);

	RSPipeline.play(RSBin.STREAMING);
	startConnectionTimer();
	return stateHandler.transition(States.PLAYING);
    }

    static final int CONNECTION_CHECK_TIMEOUT_MS = 5000;
    void startConnectionTimer(){
	Thread t = new Thread("ConnectionTimer"){
		public void run(){
		    try {
			Thread.sleep(CONNECTION_CHECK_TIMEOUT_MS);
			if (RSPipeline.isPlaying(RSBin.STREAMING)){
			    stateHandler.handleEvent(Events.PLAYING_TIMEOUT);
			}
		    } catch (Exception e){
			logger.error("StartConnetionTimer: Exception", e);
		    }
		}
	    };
	t.start();
    }

    @StateEventHandler(events="Events.PLAYING_TIMEOUT")
    StateHandler.Transition playingTimeout(){
	return stateHandler.transition(States.PLAYING, Signals.ACK, StreamingCommandMessage.CMD_START, true);
    }

    @StateEventHandler(events="Events.CMD_UPDATE")
    StateHandler.Transition updateStream(Hashtable<String, String> options){
	stopStreaming();
	return startStreaming(options);
    }

    Object stopLock = new Object();
    boolean cancelReconnection = false;

    @StateEventHandler(events="Events.CMD_STOP")
    StateHandler.Transition stopStreaming(){
	Thread t = new Thread(){
		public void run(){
		    RSPipeline.stop(RSBin.STREAMING);
		}
	    };
	synchronized(stopLock){
	    t.start();
	}
	cancelReconnection = true;
	return stateHandler.transition(States.READY, Signals.ACK, StreamingCommandMessage.CMD_STOP, true);
    }

    @StateEventHandler(states="States.PLAYING", events={"Events.CONNECTING_ERR","Events.PLAYING_ERR"})
    StateHandler.Transition tryReconnect(String error){
	startReconnecting();
	return stateHandler.transition(States.RECONNECTING, Signals.STREAM_ERROR, error);
    }

    static final int RECONNECT_ATTEMPTS = 10;
    static final int RECONNECT_INTERVAL_MS = 5000;

    void startReconnecting(){
	cancelReconnection = false;
	Thread t = new Thread("ReconnectingThread"){
		public void run(){
		    boolean reconnected = false;

		    for (int i = 0; i < RECONNECT_ATTEMPTS; i++){
			if (cancelReconnection)
			    return;

			try{
			    Thread.sleep(RECONNECT_INTERVAL_MS);
			    if (RSPipeline.isPlaying(RSBin.STREAMING)){
				stateHandler.handleEvent(Events.RECONNECTING_SUCCEEDED);
				reconnected = true;
				break;
			    } else {
				startStreaming(currentOptions);
			    }
			} catch (Exception e){
			    logger.error("Exception: ", e);
			}
		    }

		    if (!reconnected)
			stateHandler.handleEvent(Events.CONNECTING_ERR);
		}
	    };
	t.start();
    }

    @StateEventHandler(states="States.RECONNECTING", events="Events.CONNECTING_ERR")
    StateHandler.Transition reconnectError(String error){
	return stateHandler.transition(States.READY, Signals.STREAM_ERROR, STREAMING_ERROR_RECONNECTION);
    }

    @StateEventHandler(events="Events.RECONNECTING_SUCCEEDED")
    StateHandler.Transition reconnectSucceeded(){
	return stateHandler.transition(States.PLAYING, Signals.ACK, StreamingCommandMessage.CMD_START, true);
    }

    @StateEventHandler(stateEventList={"States.READY: Events.PLAYING_ERR, Events.CONNECTING_ERR"})
    StateHandler.Transition playingError(String error){
	disableService();
	return stateHandler.transition(States.READY, Signals.STREAM_ERROR, error);
    }

    @StateEventHandler(events="Events.RESET")
    StateHandler.Transition disableService(){
	stopStreaming();
	return stateHandler.transition(States.READY);
    }

    @StateEventHandler(states="States.READY", events="Events.CMD_UPDATE")
    void updateSettings(Hashtable<String, String> options){
	if (options == null){
	    logger.error("UpdateSettings: Null options");
	    return;
	}
	currentOptions = options;
    }

    @StateEventHandler(stateEventList={"States.READY: Events.CMD_STOP",
				       "States.PLAYING: Events.CMD_START"}, varargs=true)
    void ignoreEvent(Object... args){
	logger.debug("IgnoreEvent: ignoring in state: "+stateHandler.getState());
    }

    @EmitSignal("Signals.ACK")
    void sendStreamingAck(String command, Boolean ack){
	StreamingAckMessage ackMessage = new StreamingAckMessage(getName(), RSController.UI_SERVICE, widgetName,
								 ack, command);
	sendMessage(ackMessage);
    }

    @EmitSignal("Signals.STREAM_ERROR")
    void sendStreamingError(String error){
	GstreamerErrorMessage streamingErrorMessage = new GstreamerErrorMessage(serviceInstanceName,
										RSController.UI_SERVICE,
										widgetName,
										error);
	
	if (sendMessage(streamingErrorMessage) == -1){
	    logger.error("SendStreamingError: Unable to send GstError message.");
	    connectionError();
	} else {
	    logger.debug("SendStreamingError: GstError message sent.");
	}

    }
    
    /* End of Message Handlers */
    

    /* IPCNode API */
    
    public int handleIncomingMessage(IPCMessage message){
	logger.debug("HandleIncomingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	MessageHandlerUtility.handleMessage(this, message);
	logger.debug("HandleIncomingMessage: Leaving.");
	return 0;
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;

    }

    public String getName(){
	return serviceInstanceName;
    }

    public void connectionError(){
	networkDown = true;
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);
    }


    /* End of IPCNode */


    /* PipelineListener API */

    public void playDone(RSBin bin){}

    public void gstError(RSBin bin, String error){
	logger.error("GstError: "+error+" received for "+bin.getName());

	if (error.equals(STREAMING_ERROR_CONNECTION))
	    stateHandler.handleEvent(Events.CONNECTING_ERR, error);
	else
	    stateHandler.handleEvent(Events.PLAYING_ERR, error);
    }

    public void playDone(RSBin bin, String key){}
    public void gstError(RSBin bin, String key, String message){}

    /* End of PipelineListener API */

    /* LinkMonitorClientListener API */

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage heartbeatMessage){
	return sendMessage(heartbeatMessage);
    }

    /* End of LinkMonitorClientListener API */

    /* DiagnosticUtilitiesConnectionCallback API */

    public synchronized IPCServer attemptReconnect() {
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }

    public void serviceDown(String service){
	stateHandler.handleEvent(Events.RESET);
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
    }

    /* End of DiagnosticUtilitiesConnectionCallback API */


    /* DefaultUncaughtExceptionCallback API */

    public void handleUncaughtException(Thread t, Throwable e){
	logger.error("HandleUncaughtException: Error", e);
    }

    /* End of DefaultUncaughtExceptionCallback API */

    public static void main(String args[]) {
	Getopt g = new Getopt("StreamingService", args, "c:n:");
	int c;
	String configFilePath = "./automation.conf";
	String serviceInstanceName = StationConfiguration.STREAMING_SERVICE_DEFAULT_INSTANCE;
	String machineID = "127.0.0.1";
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    case 'n':
		serviceInstanceName = g.getOptarg();
		break;
	    }
	}
	
	StationConfiguration stationConfiguration = new StationConfiguration();
	stationConfiguration.readConfig(configFilePath);
	String localMachineID = stationConfiguration .getServiceInstanceMachineID(RSController.STREAMING_SERVICE, serviceInstanceName);
	stationConfiguration.setLocalMachineID(localMachineID);
	StreamingService streamingService = new StreamingService(stationConfiguration, serviceInstanceName, localMachineID);
    }

}

enum States {
    READY,
    PLAYING,
    RECONNECTING;
}

enum Events {
    CMD_START, 
    CMD_STOP, 
    CMD_UPDATE,
    PLAYING_TIMEOUT,
    PLAYING_ERR,
    CONNECTING_ERR,
    RECONNECTING_SUCCEEDED,
    RESET;
}

enum Signals {
    ACK, STREAM_ERROR;
}