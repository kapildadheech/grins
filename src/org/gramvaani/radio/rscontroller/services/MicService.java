package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.rscontroller.pipeline.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;
import org.gramvaani.radio.app.RSApp;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.gstreamer.*;
import org.gstreamer.elements.*;
import org.gstreamer.event.*;

import gnu.getopt.*;

public class MicService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, 
				   DefaultUncaughtExceptionCallback, DiagnosticUtilitiesConnectionCallback, PipelineListener {
    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected String serviceInstanceName;
    protected String machineID;
    protected LinkMonitorClient linkMonitor = null;
    protected boolean usingJack;
    protected DiagnosticUtilities diagUtil;
    
    protected String registrationID;
    protected String resourceManagerID = "";
    protected String uiServiceID = "";

    public final static String BCAST_MIC 	= "mic";
    public final static String PLAYOUT          = "playout";
    public final static String PREVIEW          = "preview";

    //messages
    public final static String ENABLE_MSG 	= "ENABLE_MSG";
    public final static String ENABLE_DIAG_MSG  = "ENABLE_DIAG_MSG";
    public final static String DISABLE_MSG  	= "DISABLE_MSG";
    public final static String MUTE_MSG 	= "MUTE_MSG";
    public final static String UNMUTE_MSG 	= "UNMUTE_MSG";
    public final static String VOLUME_MSG 	= "VOLUME_MSG";
    public final static String SUCCESS 		= "SUCCESS";
    public final static String FAILURE 		= "FAILURE";

    //state related
    protected final static int NOT_READY        = -1;
    protected final static int DISABLED 	= 0;
    protected final static int ENABLED 		= 1;
    protected final static int CONNECT_SENT 	= 2;
    protected final static int RESOURCE_REQUESTED = 3;
    protected int state = NOT_READY;

    //protected MicPipe alsaMicPipe, jackMicPipe, winMicPipe, micPipe = null;
    protected RSBin bin;

    public static final String clientName = "MicService";
    public static final String jackOutPortName = RSPipeline.PIPENAME+":out_"+clientName;
    public static final String jackInPortName = RSPipeline.PIPENAME+":in_"+clientName;

    protected String srcPortType, srcPortName, sinkPortType, sinkPortName, widgetName, command, sinkPortRole;
    protected boolean resourceAcquired = false;
    protected boolean isMicServiceReady = false;

    protected String widget;
    protected Hashtable<String, ResourceConfig> resourceConfigs;
    protected LogUtilities logger;
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected boolean networkDown = false;

    static final int MAX_DIAGNOSTICS_LEVELS = 10;
    protected boolean analyzedRecording = false;
    
    public MicService(IPCServer ipcServer, StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.ipcServer = ipcServer;
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;
	logger = new LogUtilities(serviceInstanceName);
	logger.info("MicService: Initializing.");
	logger.debug("MicService: Initializing with  serviceInstanceName="+this.serviceInstanceName+" machineID="+machineID);
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	registrationID = GUIDUtils.getGUID();
	micConfig();
	ipcServer.registerIPCNode(serviceInstanceName, this, registrationID);
	initialize();
	logger.debug("MicService: Done Initializing.");
    }

    public MicService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;
	logger = new LogUtilities(serviceInstanceName);
	logger.info("MicService: Initializing.");
	logger.debug("MicService: Initializing with  serviceInstanceName="+this.serviceInstanceName+" machineID="+machineID);
	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "MicService");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);
	
	registrationID = GUIDUtils.getGUID();
	micConfig();
	ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	if (ipcServer.registerIPCNode(serviceInstanceName, this, registrationID) == -1) {
	    logger.fatal("MicService: Unable to connect to ipcServer.");
	    connectionError();
	}
	else 
	    logger.debug("MicService: Connected to ipcServer.");
	
	linkMonitor = new LinkMonitorClient(this, serviceInstanceName, stationConfiguration);
	linkMonitor.start();
	initialize();
	/*if (sendReadyMessage() == -1) {
	    logger.fatal("MicService: Unable to send Ready message");
	    //System.exit(-1);
	    }*/
	logger.debug("MicService: Leaving");
    }
    


    public void serviceDown(String service){
	disableMicService();
	setState(NOT_READY);
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
    }

    public synchronized IPCServer attemptReconnect(){
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }

    protected void initialize(){
	RSPipeline.init(stationConfiguration);
	bin = RSBin.MIC_PLAYOUT;
	RSPipeline.addPipelineListener(bin, this);
    }

    protected void initReadinessCheck() {
	setState(DISABLED);
	
	if (sendReadyMessage(true) == -1) {
	    logger.error("InitReadinessCheck: Unable to send Ready message");
	    connectionError();
	} else
	    logger.debug("InitReadinessCheck: Ready message sent.");
    }

    protected void micConfig() {
	ServiceResource[] serviceResources = stationConfiguration.getServiceResources(serviceInstanceName);
	ResourceConfig resourceConfig = null;
	resourceConfigs = new Hashtable<String, ResourceConfig>();
	for (ServiceResource resource: serviceResources){
	    resourceConfig = stationConfiguration.getResourceConfig(resource.resourceName());
	    resourceConfigs.put(resource.resourceRole(), resourceConfig);
	    logger.debug("MicConfig: Resource role: "+resource.resourceRole()+" name: "+resource.resourceName());
	}
    }

    protected int sendReadyMessage(boolean readiness) {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(serviceInstanceName, RSController.RESOURCE_MANAGER,
								   readiness);
	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isMicServiceReady = readiness;
		logger.debug("SendReadyMessage: Ready messsage sent. isMicServiceReady=" + isMicServiceReady);
		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Message timeout exception.");
	    return -1;
	}
    }

    protected IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null){
	    return null;
	}
	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
	    logger.error("SendSyncCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	}
	return responseMessage;
    }

    public synchronized void connectionError() {
	logger.error("ConnectionError:");
	reconnectOrExit();
	logger.debug("ConnectionError: Leaving");
    }

    private void reconnectOrExit() {
	logger.error("ReconnectOrExit:");
	networkDown = true;
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);
    }
    
    public synchronized void handleUncaughtException(Thread t,Throwable ex){
	logger.error("HandleUncaughtException: Exception: ", ex);
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering. From: "+message.getSource()+" Type: "+message.getMessageClass());
	
	boolean serviceReady=false;
	if (message instanceof MicCommandMessage){
	    MicCommandMessage cmdMessage = (MicCommandMessage) message;
	    this.widgetName = cmdMessage.getWidgetName();
	    handleMicCommandMessage (cmdMessage.getSource(),
				     cmdMessage.getWidgetName(),
				     cmdMessage.getCommand(),
				     cmdMessage.getSrcPortRole(),
				     cmdMessage.getSinkPortRole(),
				     cmdMessage.getLanguage());
	} else if (message instanceof ResourceAckMessage){
	    String[] successfulResources = ((ResourceAckMessage)message).getSuccessfulResources();
	    String[] unsuccessfulResources = ((ResourceAckMessage)message).getFailedResources();
	    String[] successfulResourceRoles = ((ResourceAckMessage)message).getSuccessfulResourceRoles();
	    String[] unsuccessfulResourceRoles = ((ResourceAckMessage)message).getUnsuccessfulResourceRoles();
	    handleResourceAckMessage(successfulResources, successfulResourceRoles, unsuccessfulResources, unsuccessfulResourceRoles);
	} else if (message instanceof ServiceErrorRelayMessage){
	    ServiceErrorRelayMessage errorRelayMessage = (ServiceErrorRelayMessage) message;
	    if (errorRelayMessage.getServiceName().equals(RSController.UI_SERVICE)){
		logger.error("HandleIncomingMessage: ServiceErrorRelayMessage received for UI Service");
		disableMicService();
		setState(DISABLED);
	    }
	} else if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	} else if (message instanceof ServiceActivateNotifyMessage) {
	    ServiceActivateNotifyMessage notifyMessage = (ServiceActivateNotifyMessage)message;
	    if (notifyMessage.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		logger.info("HanldeIncomingMessage: ServiceActiveNotify message for UI Service received.");
		// timeKeeper.sync();
		uiServiceConnected(notifyMessage.getRegistrationID());
	    }
	} else if (message instanceof ServiceNotifyMessage && usingJack) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();

	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(RSController.UI_SERVICE)){
		    uiServiceConnected(activatedIDs[i]);
		} 
	    }
	    
	} else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());

	} else if (message instanceof MachineStatusRequestMessage){
	    MachineStatusRequestMessage requestMessage = (MachineStatusRequestMessage)message;
	    handleMachineStatusRequestMessage(requestMessage.getWidgetName(), requestMessage.getCommand(), requestMessage.getOptions());
	} else {
	    logger.error("HandleIncomingMessage: Unknown message type");

	}
	
	if (serviceReady && state() == NOT_READY) {
	    logger.debug("HandleIncomingMessage: Mic Service Ready");
	    setState(DISABLED);
	    if (sendReadyMessage(true) == -1) {
		logger.error("HandleIncomingMessage: Failed to send ready message");
		reconnectOrExit();
	    }
	    else
		logger.debug("HandleIncomingMessage: sendReadyMessage succeeded.");
	}

	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }
    

    protected void uiServiceConnected(String newID){
	logger.debug("UIServiceConnected: OldID: " + uiServiceID + " newID: " + newID);
	if (!uiServiceID.equals("") && !(uiServiceID.equals(newID))){
	    disableMicService();
	    setState(DISABLED);
	}
	uiServiceID = newID;
    }

    protected void handleRegistrationAckMessage(boolean ack, String id){
	String oldID = resourceManagerID;
	resourceManagerID = id;
	if (ack) {
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);
	    if (networkDown)
		networkDown = false;
	    if (oldID.equals("")){
		initReadinessCheck();
	    } else if (!oldID.equals(id)){
		logger.error("HandleRegistrationAckMessage: RSController went down. Releasing resources.");
		disableMicService();
		setState(NOT_READY);
		initReadinessCheck();
	    } else if (!isMicServiceReady) {
		initReadinessCheck();
	    }
	} else {
	    resetService();
	}
    }

    protected void resetService(){
	disableMicService();
	setState(NOT_READY);
	resourceManagerID = "";
	registrationID = GUIDUtils.getGUID();
	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	if (ipcServer.handleOutgoingMessage(message) == -1)
	    connectionError();
    }

    private void unableToDeliverMessage(String widgetName, String command) {
	logger.debug("UnableToDeliverMessage: Enterning");
	logger.error("UnableToDeliverMessage: To widget: "+widgetName+" Command: "+command);
	disableMicService();
	setState(DISABLED);
	MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, FAILURE, command);
	int retVal = sendMessage(ackMessage);
	if (retVal == -1) {
	    logger.error("UnableToDeliverMessage: Unable to deliver NACK.");
	    reconnectOrExit();
	    return;
	}
	logger.debug("UnableToDeliverMessage: Leaving");
    }

    protected void handleResourceAckMessage(String[] successfulResources,String[] successfulResourceRoles,String[] unsuccessfulResources,String[] unsuccessfulResourceRoles) {
	boolean enableFailed=false;
	boolean ack = false;
	boolean found = false;
	String error = "";
	if (state != RESOURCE_REQUESTED) {
	    logger.warn("HandleResourceAckMessage: Resource ack received in the wrong state:"+state);
	    return;
	}

	String requestedResource = resourceConfigs.get(sinkPortRole).resourceName();
	for (String resource : successfulResources){
	    if (resource.equals(requestedResource)){
		logger.debug("HandleResourceAckMessage: Resource:"+requestedResource+" received as successful resource");
		ack = true;
		found = true;
	    }
	}
	if (!found ){
	    for (String resource : unsuccessfulResources){
		if (resource.equals(requestedResource)){
		    logger.debug("HandleResourceAckMessage: Resource:"+requestedResource+" received as failed resource.");
		    ack = false;
		    found = true;
		}
	    }
	}
	
	if (!found)
	    logger.error("HandleResourceAckMessage: Resource:"+requestedResource+" did not appear in successful or failed resources.");
	if (ack == true) {
	    logger.info("HandleResourceAckMessage: Positive Ack received: "+sinkPortType+":"+sinkPortName);
	    resourceAcquired = true;
	    if (sinkPortType.equals(ResourceManager.PORT_ALSA) || sinkPortType.equals(ResourceManager.PORT_WIN) ||
		sinkPortType.equals(ResourceManager.PORT_DEFAULT)){

		if (enableMicService(sinkPortType, srcPortName, sinkPortName)){
		    MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, 
								 widgetName, SUCCESS, command);

		    int retVal = sendMessage(ackMessage);
		    if (retVal == -1){
			logger.error("HandleResourceAckMessage: Unable to send ACK.");
			connectionError();
			return;
		    } else if (retVal == -2) {
			logger.error("HandleResourceAckMessage: Unable to send ACK.");
			disableMicService();
			return;
		    } else {
			logger.info("HandleResourceAckMessage: Enabled. ACK message sent.");
			setState (ENABLED);
		    }		       
		}
		else {
		    enableFailed = true;
		}
	    }
	} else {
	    logger.error("HandleResourceAckMessage: Unable to acquire resources");
	    enableFailed = true;
	    error = RSController.ERROR_RESOURCE_ACK_FAILED;
	}

	if (enableFailed){
	    logger.error("HandleResourceAckMessage: Enable Failed.");
	    String[] errorMessage;
	    if (!error.equals("")) {
		errorMessage = new String[] {error};
	    } else {
		errorMessage = new String[] {};
	    }
	    MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, 
							 widgetName, FAILURE, command,
							 errorMessage);
	    int retVal = sendMessage(ackMessage);
	    if (retVal == -1) {
		logger.error("HandleResourceAckMessage: Could not send NACK.");
		reconnectOrExit();
		return;
	    }
	    else
		logger.debug("HandleResourceAckMessage: NACK sent Successfully.");
    	}
    }


    protected void handleMachineStatusRequestMessage(String widget, String command, String[] options){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = ((StationNetwork)stationConfiguration.getStationNetwork()).getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), RSController.UI_SERVICE, widget, command, new String[] {dirName, used, available});
	    
	    if (sendMessage(responseMessage) == -1){
		logger.error("HandleMachineStatusRequestMessage: Unable to send responseMessage.");
		reconnectOrExit();
	    } else {
		logger.debug("HandleMachineStatusRequestMessage: Response sent.");
	    }
	}
    }


    public void gstError(RSBin bin, String key, String message){
	//Not used
    }

    public void playDone(RSBin bin){
	logger.error("PlayDone: Gstreamer pipeline has stopped");
    }

    //XXX: Race condition here. We dont know which listener should be receiving this error.
    //XXX: same applies to archiver service also
    public void gstError(RSBin bin, String error){
	logger.error("GstError: "+error+" received for "+bin.getName());
	GstreamerErrorMessage micErrorMessage = new GstreamerErrorMessage(serviceInstanceName,
									  RSController.UI_SERVICE,
									  widgetName,
									  error);
	
	if (sendMessage(micErrorMessage) == -1){
	    logger.error("GstError: Unable to send GstError message.");
	    reconnectOrExit();
	} else {
	    logger.debug("GstError: GstError message sent.");
	}

	disableMicService();
	setState(DISABLED);
    }

    public void playDone(RSBin bin, String key){
	//Not used
    }


    protected void handleMicCommandMessage (String source, String widgetName, String command,
					    String srcPortRole, String sinkPortRole, String language){
	logger.debug("HandleMicCommandMessage: Entering. From: "+source+":"+widgetName+" command: "+command+" srcPortRole="+srcPortRole+" sinkPortRole="+sinkPortRole+" language="+language);
	boolean enableFailed = false;
	ResourceConfig resourceConfig = resourceConfigs.get(srcPortRole);
	if (resourceConfig == null){
	    logger.error("HandleMicCommandMessage: Error in configuration: Not found srcPortRole: " + srcPortRole);
	    MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, 
							 FAILURE, command,
							 new String[] {RSController.ERROR_RESOURCE_ROLE_NOT_FOUND, srcPortRole});
	    int retVal = sendMessage(ackMessage);
	    if (retVal == -1) {
		logger.error("HandleMicCommandMessage: Could not send NACK.");
		reconnectOrExit();
	    }
	    else
		logger.debug("HandleMicCommandMessage: NACK sent Successfully.");
	    
	    return;
	}
	srcPortType = resourceConfig.resourceMixer();
	srcPortName = resourceConfig.resourceDeviceName();
	this.command = command;
	this.sinkPortRole = sinkPortRole;
	resourceConfig = resourceConfigs.get(sinkPortRole);
	if (resourceConfig == null){
	    logger.fatal("HandleMicCommandMessage: Error in configuration: Not found sinkPortRole: " + sinkPortRole);
	    MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, 
							 FAILURE, command,
							 new String[] {RSController.ERROR_RESOURCE_ROLE_NOT_FOUND, sinkPortRole});
	    int retVal = sendMessage(ackMessage);
	    if (retVal == -1) {
		logger.error("HandleMicCommandMessage: Could not send NACK.");
		reconnectOrExit();
		return;
	    }
	    else
		logger.debug("HandleMicCommandMessage: NACK sent Successfully.");

	    return;
	}
	sinkPortType = resourceConfig.resourceMixer();
	sinkPortName = resourceConfig.resourceDeviceName();

	logger.debug("HandleMicCommandMessage: srcPortType="+srcPortType+" srcPortName="+srcPortName+" sinkPortType="+sinkPortType+" sinkPortName="+sinkPortName);

	switch(state()){
	    
	case DISABLED:
	    if (command.equals(MicService.ENABLE_MSG) || command.equals(MicService.ENABLE_DIAG_MSG)){
		logger.info("HandleMicCommandMessage: Enable message: "+srcPortType+":"+srcPortName+" "+sinkPortType+":"+sinkPortName);
		ResourceRequestMessage resourceRequestMessage = new ResourceRequestMessage(serviceInstanceName,
											   RSController.RESOURCE_MANAGER,
											   new String[] {resourceConfig.resourceName()}, 
											   new String[] {sinkPortRole},
											   false);

		if (command.equals(MicService.ENABLE_DIAG_MSG)) {
		    bin = RSBin.MIC_DIAGNOSTICS;
		    analyzedRecording = false;
		} else if (sinkPortRole.equals(PLAYOUT)) {
		    bin = RSBin.MIC_PLAYOUT;
		} else {
		    logger.error("HandleMicCommandMessage: Unknown sink port role received: " + sinkPortRole);
		    return;
		    //bin = RSBin.MIC_PREVIEW;
		}

		if (sendMessage(resourceRequestMessage) == -1) {
		    logger.error("HandleMicCommandMessage: Unable to send ResourceRequest message.");
		    reconnectOrExit();

		} else {
		    logger.debug("HandleMicCommandMessage: ResourceResquest message sent.");
		    setState(RESOURCE_REQUESTED);
		}

		
	    }else if (command.equals(MicService.MUTE_MSG)
		     ||command.equals(MicService.UNMUTE_MSG)
		     ||command.equals(MicService.VOLUME_MSG)
		     ||command.equals(MicService.DISABLE_MSG)) {
		logger.error("HandleMicCommandMessage: "+command+" received when Mic is disabled.");
		MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, 
							     widgetName, FAILURE, command,
							     new String[] {RSController.ERROR_WRONG_STATE,Integer.toString(state())});
		int retVal = sendMessage(ackMessage);
		if (retVal == -1) {
		    logger.error("HandleMicCommandMessage: Could not send NACK.");
		    reconnectOrExit();
		    return;
		}
		else
		    logger.debug("HandleMicCommandMessage: NACK sent Successfully.");
	    }
	    break;

	case ENABLED:
	    if (command.equals(MicService.ENABLE_MSG) || command.equals(MicService.ENABLE_DIAG_MSG)){
		logger.error("HandleMicCommandMessage: Already enabled");
		//XXX send nack
	    }
	    else if (command.equals(MicService.DISABLE_MSG)){
		setState(DISABLED);
		disableMicService();
		logger.info("HandleMicCommandMessage: MicSerive disabled.");
		MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
		int retVal = sendMessage(ackMessage);
		if (retVal == -1){
		    logger.error("HandleMicCommandMessage: Unable to send ACK.");
		    reconnectOrExit();
		    return;
		}
		else
		    logger.debug("HandleMicCommandMessage: ACK sent successfully.");
	    }else if (command.equals(MicService.MUTE_MSG)){
		if (RSPipeline.mute(bin)){
		    logger.info("HandleMicCommandMessage: Mic muted.");
		    MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
		    int retVal = sendMessage(ackMessage);
		    if (retVal == -1){
			logger.error("HandleMicCommandMessage: Unable to send ACK.");
			connectionError();
			return;
		    } else if (retVal == -2) {
		        logger.error("HandleMicCommandMessage: Unable to send ACK.");
		        disableMicService();
		        return;
		    } else {
			logger.debug("HandleMicCommandMessage: ACK sent successfully.");
		        setState (ENABLED);
		    }
		}
	    }else if (command.equals(MicService.UNMUTE_MSG)){
		if (RSPipeline.unmute(bin)){
		    logger.info("HandleMicCommandMessage: Mic unmuted.");
		    MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
		    int retVal = sendMessage(ackMessage);
		    if (retVal == -1){
			logger.error("HandleMicCommandMessage: Unable to send ACK.");
			connectionError();
			return;
		    } else if (retVal == -2) {
		        logger.error("HandleMicCommandMessage: Unable to send ACK.");
		        disableMicService();
		        return;
		    } else {
			logger.debug("HandleMicCommandMessage: ACK sent successfully.");
		        setState (ENABLED);
		    }
		}
	    }else if (command.substring(0,10).equals(MicService.VOLUME_MSG)){
		String[] vals = command.split(":");
		float vol;
		try{
		    vol = Float.parseFloat(vals[1]);
		    if (RSPipeline.setVolume(bin, vol)){
			logger.info("HandleMicCommandMessage: Volumed changed to "+vals[1]);
			MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
			int retVal = sendMessage(ackMessage);
			if (retVal == -1){
			    logger.error("HandleMicCommandMessage: Unable to send ACK.");
			    connectionError();
			    return;
			} else if (retVal == -2) {
			    logger.error("HandleMicCommandMessage: Unable to send ACK.");
			    disableMicService();
			    return;
			} else {
			    logger.debug("HandleMicCommandMessage: ACK sent successfully.");
			    setState (ENABLED);
			}
		    }
		}catch(Exception e){
		    logger.error("HandleMicCommandMessage: Volume value "+vals[1]+" not parsable.");
		    MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, FAILURE, command);
		    int retVal = sendMessage(ackMessage);
		    if (retVal == -1){
		        logger.error("HandleMicCommandMessage: Unable to send NACK.");
			connectionError();
			return;
		    } else if (retVal == -2) {
			logger.error("HandleMicCommandMessage: Unable to send NACK.");
			disableMicService();
			return;
		    } else {
			logger.debug("HandleMicCommandMessage: NACK sent successfully.");
			setState (ENABLED);
		    }
		}		    
	    }
		
	    break;
	case NOT_READY:
	    logger.error("HandleMicCommandMessage: Mic Command message received when Mic Service is not ready");
	    //XXX: send nack
	    break;
	case CONNECT_SENT:
	    logger.warn("HandleMicCommandMessage: Mic Command message received in connect sent state. No action taken.");
	    //XXX: send nack
	    break;
	case RESOURCE_REQUESTED:
	    if (command.equals(DISABLE_MSG)) {
		logger.debug("HandleMicCommandMessage: Disable command received in resource requested state.");
		setState(DISABLED);
		sendResourceReleaseMessage();
		logger.info("HanldeMicCommandMessage: MicSerive disabled.");
		MicAckMessage ackMessage = new MicAckMessage(getName(), RSController.UI_SERVICE, widgetName, SUCCESS, command);
		int retVal = sendMessage(ackMessage);
		if (retVal == -1){
		    logger.error("HandleMicCommandMessage: Unable to send ACK.");
		    reconnectOrExit();
		    return;
		}
		else
		    logger.debug("HandleMicCommandMessage: ACK sent successfully.");
		return;
	    } else {
		logger.error("HandleMicCommandMessage: Command: " + command + " recevied in resource requested state.");
	    }
	    break;
	}
    }
   
    //XXX: When resource release message fails due to connection error
    //and we recover before resetting ourselves RSC and we would be in inconsistent 
    //states
    protected void sendResourceReleaseMessage() {
	ResourceReleaseMessage resourceReleaseMessage = new ResourceReleaseMessage(serviceInstanceName,
										   RSController.RESOURCE_MANAGER,
										   new String[] {resourceConfigs.get(sinkPortRole).resourceName()});
	if (sendMessage(resourceReleaseMessage) == -1) {
	    logger.error("SendResourceReleaseMessage: Unable to send ResourceRelease message.");
	    reconnectOrExit();
	    return;
	}
	else
	    logger.debug("SendResourceReleaseMessage: ResourceRelease message sent.");
    }

    protected void releaseResources() {
	if (resourceAcquired) {
	    sendResourceReleaseMessage();
	    resourceAcquired = false;
	}
    }

    protected boolean enableMicService (String portType, String portName, String sinkPortName){
	boolean retval;
	logger.info("EnableMicService: Entering. Enabling starting :"+portType+" "+portName+" "+sinkPortName);

	if (!bin.isInit())
	    RSPipeline.addPipelineListener(bin, this);

	retval = RSPipeline.play(bin);
	logger.debug("EnableMicService: Leaving with return value "+retval);
	return retval;
    }

    protected void disableMicService() {
	logger.debug("DisableMicService: Entering");
	
	if (!RSPipeline.stop(bin)) {
	    // XXX kill process
	    logger.error("DisableMicService: Unable to stop capturing.");
	}else{
	    if (bin == RSBin.MIC_DIAGNOSTICS && !analyzedRecording){
		analyzeRecording();
		analyzedRecording = true;
	    }

	    releaseResources();
	    logger.debug("DisableMicService: Pipe stopped successfully.");
	}
	
	logger.debug("DisableMicService: Leaving");
    }

    protected void analyzeRecording(){
	
	int[] levels = RSPipeline.getLevels(RSPipeline.MIC_DIAG_FILE);
	
	int length = Math.min(levels.length, MAX_DIAGNOSTICS_LEVELS);
	
	String[] levelStrings = new String[length];

	for (int i = 0; i < length; i++)
	    levelStrings[i] = Integer.toString(levels[i]);

	MicLevelsMessage levelsMessage = new MicLevelsMessage(getName(), RSController.UI_SERVICE, RSApp.MIC_PROVIDER, SUCCESS, levelStrings);
	
	if (sendMessage(levelsMessage) == -1){
	    logger.error("AnalyzeRecording: Could not send MicLevelMessage.");
	    reconnectOrExit();
	} else {
	    logger.debug("AnalyzeRecording: Mic levels message sent.");
	}

	try{
	    (new File(RSPipeline.MIC_DIAG_FILE)).delete();
	} catch (Exception e){
	    logger.error("AnalyzeRecording: Unable to remove MIC_DIAG_FILE.");
	}
    }

    protected int state(){
	return state;
    }

    protected void setState(int state){
	int previousState = this.state;
	this.state = state;
	logger.info("SetState: Previous state: "+previousState+" Current state: "+state);
    }

    public String getName() {
	return serviceInstanceName;
    }    

    protected int sendMessage(IPCMessage message) {
	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else 
	    return -1;
    }

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");
	
	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }

    public static void main(String args[]) {
	Getopt g = new Getopt("MicService", args, "c:n:");
	int c;
	String configFilePath = "./automation.conf";
	String serviceInstanceName = StationConfiguration.MIC_SERVICE_DEFAULT_INSTANCE;
	String machineID = "127.0.0.1";
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    case 'n':
		serviceInstanceName = g.getOptarg();
		break;
	    }
	}
	
	StationConfiguration stationConfiguration = new StationConfiguration();
	stationConfiguration.readConfig(configFilePath);
	String localMachineID = stationConfiguration .getServiceInstanceMachineID(RSController.MIC_SERVICE, serviceInstanceName);
	stationConfiguration.setLocalMachineID(localMachineID);
	MicService micService = new MicService(stationConfiguration, serviceInstanceName, localMachineID);
    }

}
