package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.telephonylib.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.pipeline.*;
import org.gramvaani.radio.app.RSApp;

import gnu.getopt.Getopt;
import java.util.*;
import java.io.File;

import static org.gramvaani.utilities.StringUtilities.getCSVFromIterable;
import static org.gramvaani.utilities.StringUtilities.getCSVFromArray;

public class OnlineTelephonyService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, 
					       DefaultUncaughtExceptionCallback, DiagnosticUtilitiesConnectionCallback, 
					       DiagnosticUtilitiesAsteriskCallback, TelephonyLibListener, MediaLibListener,
					       PipelineListener {

    //States
    public final static int CONFIGURING = 0;
    public final static int OFFLINE	= 1;
    public final static int ONLINE 	= 2;

    public static final String ERROR_PBX_UNREACHABLE = "ERROR_PBX_UNREACHABLE"; 

    public static final String ONLINE_TELEPHONY = "ONLINE_TELEPHONY";
    public static final String ONAIR_PLAYBACK_ROLE = "onair_playback";
    public static final String OFFAIR_PLAYBACK_ROLE = "offair_playback";
    public static final String ONAIR_CAPTURE_ROLE = "onair_capture";
    public static final String OFFAIR_CAPTURE_ROLE = "offair_capture";


    protected final static String WIDGET = RSApp.TELEPHONY_PROVIDER;

    public static final String CALLER = "caller";
    public static final String NO_NAME = "noname";

    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected LogUtilities logger;
    protected String registrationID;
    protected String machineID;
    protected String serviceInstanceName;
    protected DiagnosticUtilities diagUtil;
    protected TimeKeeper timeKeeper;
    protected FileStoreManager fileStoreManager;
    protected LinkMonitorClient linkMonitor = null;
    protected Hashtable<String, ResourceConfig> resourceConfigs;
    protected boolean isOnlineTelephonyServiceReady = false;
    protected String resourceManagerID = "";
    protected String uiServiceID = "";
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected TelephonyLib telephonyLib;
    protected MediaLib medialib;
    protected int state = CONFIGURING;
    protected boolean isIndexServiceActive = false;
    protected static boolean attemptingTelephonyUp = false;
    protected static Object attemptingTelephonySyncObject = new Object();
    protected Hashtable<String, Boolean> heldResources = new Hashtable<String, Boolean>();

    protected RSBin bin;
    protected String incomingRingCallGUID = "";
    protected String outgoingRingCallGUID = "";
    protected static final String RING_INCOMING_CALL = "ring_incoming_call.mp3";
    protected static final String RING_OUTGOING_CALL = "ring_outgoing_call.mp3";
    protected static final String TONE_SEARCHING_OUTGOING_CALL = "tone_searching.mp3";
    protected static final String RING_PLAYBACK = "RING_PLAYBACK";

    public OnlineTelephonyService(IPCServer ipcServer, StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.ipcServer = ipcServer;
	this.machineID = machineID;
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	logger = new LogUtilities(RSController.ONLINE_TELEPHONY_SERVICE);
	logger.debug("OnlineTelephonyService: Entering. Initializing with machineID:"+machineID);
	logger.info("OnlineTelephonyService: Initializing");
	registrationID = GUIDUtils.getGUID();

	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	onlineTelephonyConfig();

	MediaLib.setIPCServer(ipcServer);
	MediaLib.registerListener(RSController.ONLINE_TELEPHONY_SERVICE, this);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null)
	    medialib.setLibraryActivated(true);

	telephonyLib = TelephonyLib.getTelephonyLib(stationConfiguration, medialib, this);

	fileStoreManager = new FileStoreManager(stationConfiguration, serviceInstanceName, ipcServer,
						new String[] {StationNetwork.UPLOAD}, 
						timeKeeper,
						StationNetwork.ONLINETELEPHONY,
						getName());

	ipcServer.registerIPCNode(RSController.ONLINE_TELEPHONY_SERVICE, this, registrationID);

	initRSPipeline();
	logger.debug("OnlineTelephonyService: Leaving");
    }


    public OnlineTelephonyService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.stationConfiguration = stationConfiguration;
	this.machineID = machineID;
	this.serviceInstanceName = serviceInstanceName;
        ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));

	logger = new LogUtilities(serviceInstanceName);
	logger.debug("OnlineTelephonyService: Entering. Initializing with machineID:"+machineID);
	logger.info("OnlineTelephonyService: Initializing.");

	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "OnlineTelephonyService");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);
	registrationID = GUIDUtils.getGUID();

	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	onlineTelephonyConfig();

	linkMonitor = new LinkMonitorClient(this, RSController.ONLINE_TELEPHONY_SERVICE, stationConfiguration);
	linkMonitor.start();

	MediaLib.setIPCServer(ipcServer);
	MediaLib.registerListener(RSController.ONLINE_TELEPHONY_SERVICE, this);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null)
	    medialib.setLibraryActivated(true);

	telephonyLib = TelephonyLib.getTelephonyLib(stationConfiguration, medialib, this);

	fileStoreManager = new FileStoreManager(stationConfiguration, serviceInstanceName, ipcServer,
						new String[] {StationNetwork.UPLOAD}, 
						timeKeeper,
						StationNetwork.ONLINETELEPHONY,
						getName());

	if (ipcServer.registerIPCNode(RSController.ONLINE_TELEPHONY_SERVICE, this, registrationID) == -1) {
	    logger.error("OnlineTelephonyService: Unable to connect to ipcServer.");
	    connectionError();
	}
	
	initRSPipeline();
	logger.debug("OnlineTelephonyService: Leaving");
    }

    void initRSPipeline() {
	RSPipeline.init(stationConfiguration);
	bin = RSBin.ONLINE_TELEPHONY;
	RSPipeline.addPipelineListener(bin, this);
    }

    protected void onlineTelephonyConfig() {
	ServiceResource[] serviceResources = stationConfiguration.getServiceResources(serviceInstanceName);
	ResourceConfig resourceConfig, playbackResource, captureResource;
	resourceConfigs = new Hashtable<String, ResourceConfig>();
	for (ServiceResource resource: serviceResources){
	    resourceConfig = stationConfiguration.getResourceConfig(resource.resourceName());
	    resourceConfigs.put(resource.resourceRole(), resourceConfig);
	    logger.info("OnlineTelephonyConfig: Resource role: "+resource.resourceRole()+" name: "+resource.resourceName()+" devicename: "+resourceConfig.resourceDeviceName());
	}

	playbackResource = resourceConfigs.get(ONAIR_PLAYBACK_ROLE);
	captureResource = resourceConfigs.get(ONAIR_CAPTURE_ROLE);
	if (playbackResource != null && captureResource != null) {
	    TelephonyLib.setOnairDevices(captureResource.resourceDeviceName(), playbackResource.resourceDeviceName());
	} else {
	    logger.error("OnlineTelephonyConfig: Cannot set onair device. playbackResource=" + playbackResource + " captureResource=" + captureResource);
	}

	playbackResource = resourceConfigs.get(OFFAIR_PLAYBACK_ROLE);
	captureResource = resourceConfigs.get(OFFAIR_CAPTURE_ROLE);
	if (playbackResource != null && captureResource != null) {
	    TelephonyLib.setOffairDevices(captureResource.resourceDeviceName(), playbackResource.resourceDeviceName());
	} else {
	    logger.error("OnlineTelephonyConfig: Cannot set offair device. playbackResource=" + playbackResource + " captureResource=" + captureResource);
	}
    }
    

    public synchronized void connectionError() {
	logger.error("ConnectionError: ");
	timeKeeper.setIPCServer(null);
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering. From: "+message.getSource()+" Type: "+message.getMessageType());

	if (message instanceof TimeKeeperResponseMessage) {
	    timeKeeper.handleTimeKeeperResponseMessage((TimeKeeperResponseMessage)message);
	} else if (message instanceof SyncFileStoreMessage) {
	    syncFileStore();
	} else if (message instanceof ServiceErrorRelayMessage) {
	    ServiceErrorRelayMessage errorRelayMessage = (ServiceErrorRelayMessage) message;
	    if (errorRelayMessage.getServiceName().equals(RSController.UI_SERVICE)){
		logger.error("HandleIncomingMessage: ServiceErrorRelayMessage received for UI Service");
		if (state != CONFIGURING) {
		    disableTelephony();
		    setState(OFFLINE);
		}
	    } else if (errorRelayMessage.getServiceName().equals(RSController.LIB_SERVICE)){
		logger.debug("HandleIncomingMessage: ServiceErrorRelayMessage received for "+RSController.LIB_SERVICE);
		if (isOnlineTelephonyServiceReady && !checkStatus()) {
		    disableTelephony();
		    setState(CONFIGURING);
		    sendReadyMessage(false);
		}
	    } else if (errorRelayMessage.getServiceName().equals(RSController.INDEX_SERVICE)) {
		isIndexServiceActive = false;
	    }
	} else if (message instanceof ServiceActivateNotifyMessage) {
	    ServiceActivateNotifyMessage notifyMessage = (ServiceActivateNotifyMessage)message;
	    if (notifyMessage.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		timeKeeper.sync();
		uiServiceConnected(notifyMessage.getRegistrationID());
	    }
	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    String[] services = notifyMessage.getActivatedServices();
	    String[] ids = notifyMessage.getActivatedIDs();

	    for (int i=0; i<services.length; i++){
		String service = services[i];
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (!isOnlineTelephonyServiceReady && checkStatus()) {
			setState(OFFLINE);
			sendReadyMessage(true);
		    }
		} else if (service.equals(RSController.INDEX_SERVICE)) {
		    isIndexServiceActive = true;
		} else if (service.equals(RSController.UI_SERVICE)){
		    uiServiceConnected(ids[i]);
		}
	    } 

	    services = notifyMessage.getInactivatedServices();
	    for (String service : services){
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (isOnlineTelephonyServiceReady && !checkStatus()) {
			disableTelephony();
			setState(CONFIGURING);
			sendReadyMessage(false);
		    }
		} else if (service.equals(RSController.INDEX_SERVICE))
		    isIndexServiceActive = false;
	    } 

	} else if (message instanceof RegistrationAckMessage) {

	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());

	} else if (message instanceof MachineStatusRequestMessage){
	    // do nothing

	} else if (message instanceof TelephonyCommandMessage) {
	    handleTelephonyCommandMessage((TelephonyCommandMessage)message);
           
	} else if (message instanceof ServiceAckMessage) {
	    ServiceAckMessage ackMessage = (ServiceAckMessage) message;
	    for (String service: ackMessage.getActiveServices()) {
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (!isOnlineTelephonyServiceReady && checkStatus()) {
			setState(OFFLINE);			
			sendReadyMessage(true);
		    }
		} else if (service.equals(RSController.INDEX_SERVICE))
		    isIndexServiceActive = true;
	    }
	    
	    for (String service: ackMessage.getInactiveServices()) {
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (isOnlineTelephonyServiceReady && !checkStatus()){
			disableTelephony();
			setState(CONFIGURING);
			sendReadyMessage(false);
		    }
		} else if (service.equals(RSController.INDEX_SERVICE))
		    isIndexServiceActive = false;
	    }
	    
	} else if (message instanceof StartCleanupMessage) {
	    StartCleanupMessage cleanupMessage = (StartCleanupMessage)message;
	    handleCleanup(cleanupMessage.getSource(), cleanupMessage.getCommands());
	} else if (message instanceof StatusCheckRequestMessage) {
	    StatusCheckRequestMessage requestMessage = (StatusCheckRequestMessage) message;
	    handleStatusCheckMessage(requestMessage.getSource(), requestMessage.getWidgetName());
	} 

	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }

    protected void handleStatusCheckMessage(String source, String widgetName){
	String response;

	if (checkStatus())
	    response = StatusCheckResponseMessage.TRUE;
	else
	    response = ERROR_PBX_UNREACHABLE;

	StatusCheckResponseMessage responseMessage = new StatusCheckResponseMessage(serviceInstanceName, source, widgetName, "", response);
	
	Hashtable<String, String> options = new Hashtable<String, String>();
	String oklines = getCSVFromArray(telephonyLib.getActiveTelephonyLines());
	options.put(TelephonyEventMessage.PHONE_LINE_OK, getCSVFromArray(telephonyLib.getActiveTelephonyLines()));
	options.put(TelephonyEventMessage.PHONE_LINE_ERROR, getCSVFromArray(telephonyLib.getInactiveTelephonyLines()));
	responseMessage.setOptions(options);
	//System.err.println("opts: "+options);
	sendMessage(responseMessage);
    }

    protected void syncFileStore() {
	fileStoreManager.syncStoresManually();
    }

    protected void handleCleanup(String source, String[] commands) {
	logger.info("HandleCleanup: Entering. source: " + source + " command length: " + commands.length);

	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib == null) {
	    logger.error("HandleCleanup: Unable to update database. Database not connected.");
	    return;
	} else {
	    medialib.setLibraryActivated(true);
	}
	
	ArrayList<String> oldDataList = new ArrayList<String>();
	ArrayList<String> newDataList = new ArrayList<String>();
	    
	for (String command: commands) {
	    logger.info("HandleCleanup: Entering. source = " + source + " command = " + command);
	    if (command.equals(StartCleanupMessage.INCOMPLETE_TELEPHONY)) {
		RadioProgramMetadata incompleteMetadata = RadioProgramMetadata.getDummyObject("");
		incompleteMetadata.setState(LibService.START_TELEPHONY);
			
		Metadata[] incompleteProgs = medialib.get(incompleteMetadata);
		for (Metadata incompleteProg: incompleteProgs) {
		    RadioProgramMetadata program = (RadioProgramMetadata)incompleteProg;
			    
		    String filename = program.getItemID();
		    logger.debug("HandleCleanup: Changing state of: "+filename);

		    File programFile = new File(stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR) + File.separator + filename);
		    if (!programFile.exists()) {
			logger.error("HandleCleanup: File " + filename + " not found in telephony store. Removing entry from the database.");
			medialib.remove(program, false);
			PlayoutHistory queryPlayoutHistory = new PlayoutHistory();
			queryPlayoutHistory.setItemID(program.getItemID());
			medialib.remove(queryPlayoutHistory, false);
			oldDataList.add(filename);
			continue;
		    }

		    updateCallDone(filename, false, false);
		    
		    newDataList.add(filename);

		} //for all metadata loop
	    }
	}

	updatePrograms(oldDataList, newDataList);
	fileStoreManager.handleCleanup();

	if (shouldSendFileToLib()) {
	    for (String file: newDataList) {
		pushFileToLib(file, false);
	    }
	    fileStoreManager.triggerUpload();
	}
	    
    }

    protected void updatePrograms(final ArrayList<String> oldDataList, final ArrayList<String> newDataList){
	Thread t = new Thread("UpdateIndex Thread"){
		public void run(){
		    int totalItems = oldDataList.size();
		    logger.info("UpdateIndex: Updating Index for: "+totalItems);
		    int numItemsPerMessage = 5;
		    ArrayList<String> removeProgramList = new ArrayList<String>();
		    
		    if (isIndexServiceActive){
			for (int i = 0; i < totalItems; i += numItemsPerMessage){
			    int numItems;
			    if (i <= totalItems - numItemsPerMessage)
				numItems = numItemsPerMessage;
			    else
				numItems = totalItems % numItemsPerMessage;
			    String[] array = new String[numItems];			
			    
			    
			    for (int j = 0; j < numItems; j++){
				array[j] = oldDataList.get(i+j);
			    }
			    
			    RemoveProgramMessage removeMessage = new RemoveProgramMessage(serviceInstanceName,
											  RSController.INDEX_SERVICE,
											  array);
			    RemoveProgramResponseMessage removeResponse;
			    
			    try{
				removeResponse = (RemoveProgramResponseMessage) sendSyncCommand(removeMessage);
				logger.info("UpdateIndex: Removed entries: "+array.length);
			    } catch (Exception e){
				logger.error("UpdateIndex: Unable to send RemoveProgramMessage.", e);
			    }
			    
			    
			    //XXX confirm that index need not be flushed between update and remove.
			    
			    
			    for (int j = 0; j < numItems; j++){
				array[j] = newDataList.get(i+j);
			    }
			    
			    UpdateProgramMessage updateMessage = new UpdateProgramMessage(serviceInstanceName, 
											  RSController.INDEX_SERVICE,
											  array);
			    UpdateProgramResponseMessage updateResponse;
			    try{
				updateResponse = (UpdateProgramResponseMessage) sendSyncCommand(updateMessage);
				logger.info("UpdateIndex: Added new entries: "+array.length);
			    } catch (Exception e){
				logger.error("UpdateIndex: Unable to send UpdateProgramMessage.",e);
			    }
			    
			    
			}
			
			if (totalItems > 0){
			    FlushIndexMessage flushMessage = new FlushIndexMessage(serviceInstanceName, 
										   RSController.INDEX_SERVICE);
			    FlushIndexResponseMessage responseMessage = null;
			    try{
				responseMessage = (FlushIndexResponseMessage) sendSyncCommand(flushMessage);
			    } catch (Exception e) {
				logger.error("UpdateIndex: Unable to send flush message", e);
			    }
			    
			    if (responseMessage == null)
				logger.error("UpdateIndex: Unable to send flush message.");
			    else if (responseMessage.getReturnValue() < 0)
				logger.error("UpdateIndex: Unable to flush index with error: "+responseMessage.getReturnValue());
			}
		    
		    } //if index service active.
		}//run()
	    };
	t.start();
    }

    protected synchronized void handleTelephonyCommandMessage(TelephonyCommandMessage commandMessage) {
	String command = commandMessage.getCommand();
	Hashtable<String,String> options = commandMessage.getOptions();

	//System.err.println("--------------------------------------------------");
	logger.info("HandleTelephonyCommandMessage: command:" + command +":"+commandMessage.getLocalSequence());
	logger.debug("HandleTelephonyCommandMessage: options:");

	for (String optionKey : options.keySet()) {
	    logger.debug("HandleTelephonyCommandMessage: " + optionKey + " => " + options.get(optionKey));
	}

	if (command.equals(TelephonyCommandMessage.GO_ONLINE)) {
	    handleGoOnlineCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.GO_OFFLINE)) {
	    handleGoOfflineCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.ACCEPT_CALL)) {
	    handleAcceptCallCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.PREVIEW_CALL)) {
	    handlePreviewCallCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.HOLD_CALL)) {
	    handleHoldCallCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.HANGUP_CALL)) {
	    handleHangUpCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.MUTE_CALL)) {
	    handleMuteCallCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.UNMUTE_CALL)) {
	    handleUnmuteCallCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.GO_ONAIR)) {
	    handleGoOnAirCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.GO_OFFAIR)) {
	    handleGoOffAirCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.DIAL)) {
	    handleDialCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.ENABLE_CONF)) {
	    handleEnableConfCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.DISABLE_CONF)) {
	    handleDisableConfCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.DISCONNECT)) {
	    handleDisconnectCommand(commandMessage);
	} else if (command.equals(TelephonyCommandMessage.DESTROY_ASTERISK_CHANNELS)) {
	    handleDestroyAsteriskChannelsCommand();
	}

    }

    protected void handleDestroyAsteriskChannelsCommand() {
	if (telephonyLib != null) {
	    telephonyLib.destroyAsteriskChannels();
	} else {
	    logger.warn("HandleDestroyAsteriskChannelsCommand: TelephonyLib is null");
	}
    }


    protected synchronized void handleDialCommand(TelephonyCommandMessage commandMessage) {
	boolean success = false;
	Hashtable<String,String> options = commandMessage.getOptions();
	String idString = options.get(TelephonyCommandMessage.CALLER_ID);
	String tempGuid  = options.get(TelephonyCommandMessage.TEMP_CALL_GUID);
	
	RSCallerID callerID = new RSCallerID(idString);

	if (state == CONFIGURING) {
	    logger.error("HandleDialCommand: Attempting to dial a number in wrong state: " + state);
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, TelephonyAckMessage.FALSE, RSController.ERROR_WRONG_STATE);
	    return;
	}

	String resourceRole = getResourceRoleToRequest(commandMessage.getCommand()); 
	if (!testResourceConfig(resourceRole)) {
	    reportResourceConfigFailed(commandMessage, resourceRole);
	    return;
	}

	if (!requestResource(resourceRole)) {
	    reportResourceRequestFailed(commandMessage, resourceRole);
	    return;
	}

	if (telephonyLib != null && telephonyLib.dial(callerID, tempGuid)) {
	    logger.info("HandleDialCommand: Dialed " + idString);
	    success = true;
	} else {
	    logger.error("HandleDialCommand: Unable to dial number. telephonyLib: " + telephonyLib);
	}

	if (success) {
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, TelephonyAckMessage.TRUE);
	} else {
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}

    }

    protected synchronized void handleGoOnAirCommand(TelephonyCommandMessage commandMessage) {
	boolean success = false;
	String filename;
	Hashtable<String,String> options = commandMessage.getOptions();

	boolean isConference = false;
	if (options.get(TelephonyCommandMessage.IS_CONFERENCE) == null)
	    isConference = false;
	else if (options.get(TelephonyCommandMessage.IS_CONFERENCE).equals(TelephonyCommandMessage.TRUE))
	    isConference = true;

	String resourceRole = getResourceRoleToRequest(commandMessage.getCommand()); 
	if (!testResourceConfig(resourceRole)) {
	    reportResourceConfigFailed(commandMessage, resourceRole);
	    return;
	}

	if (!requestResource(resourceRole)) {
	    reportResourceRequestFailed(commandMessage, resourceRole);
	    return;
	}

	if (isConference) {
	    filename = telephonyLib.getConferenceMonitorFile();
	} else {
	    filename = getOffairCallMonitorFile();
	}

	if (telephonyLib != null && telephonyLib.goOnAir(isConference)) {
	    logger.debug("HandleGoOnAirCommand: Telephony on air.");
	    success = true;
	} else {
	    logger.error("HandleGoOnAirCommand: Unable to go on air. telephonyLib: " + telephonyLib);
	}

	String[] toRelease = getResourceRolesToRelease(commandMessage.getCommand(), success);
	if (toRelease != null)
	    releaseResources(toRelease);

	if (success) {
	    if (filename != null) {
		Long telecastTime = timeKeeper.getRealTime();
		PlayoutHistory playoutHistory = new PlayoutHistory(filename, 
								   stationConfiguration.getStringParam(StationConfiguration.STATION_NAME),
								   telecastTime);
		medialib.insert(playoutHistory, false);
		options.put(TelephonyAckMessage.PROGRAM_ID, filename);
		options.put(TelephonyAckMessage.TELECAST_TIME, telecastTime.toString());
	    } else {
		logger.warn("HandleGoOnAirCommand: Got filename null. Not inserting playout history.");
	    }
	    
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.TRUE);
	} else {
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
    }

    protected String getOffairCallMonitorFile() {
	String offairGUID = telephonyLib.getOffAirCall();
	if (offairGUID != null)
	    return telephonyLib.getMonitorFile(offairGUID);
	else
	    return null;
    }


    protected synchronized void handleGoOffAirCommand(TelephonyCommandMessage commandMessage) {
	boolean success = false;
	Hashtable<String, String> options = commandMessage.getOptions();

	boolean isConference;
	if (options.get(TelephonyCommandMessage.IS_CONFERENCE).equals(TelephonyCommandMessage.TRUE)) 
	    isConference = true;
	else
	    isConference = false;

	String resourceRole = getResourceRoleToRequest(commandMessage.getCommand()); 
	if (!testResourceConfig(resourceRole)) {
	    reportResourceConfigFailed(commandMessage, resourceRole);
	    return;
	}

	if (!requestResource(resourceRole)) {
	    reportResourceRequestFailed(commandMessage, resourceRole);
	    return;
	}

	if (telephonyLib != null && telephonyLib.goOffAir(isConference)) {
	    logger.debug("HandleGoOffAirCommand: Telephony off air.");
	    success = true;
	} else {
	    logger.error("HandleGoOffAirCommand: Unable to go off air. telephonyLib: " + telephonyLib);
	}

	String[] toRelease = getResourceRolesToRelease(commandMessage.getCommand(), success);
	if (toRelease != null)
	    releaseResources(toRelease);

	if (success) {
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.TRUE);
	} else {
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
    }

    protected synchronized void handleGoOnlineCommand(TelephonyCommandMessage commandMessage) {
	if (state == CONFIGURING) {
	    logger.error("HandleGoOnlineCommand: Attempting to go online in wrong state: " + state);
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, TelephonyAckMessage.FALSE, RSController.ERROR_WRONG_STATE);
	    return;
	}
	    
	if (telephonyLib != null && telephonyLib.goOnline()) {
	    logger.debug("HandleGoOnlineCommand: Telephony online.");
	    setState(ONLINE);

	    sendTelephonyAckMessage(commandMessage.getCommand(), null, TelephonyAckMessage.TRUE);

	} else {
	    logger.error("HandleGoOnlineCommand: Unable to go online. telephonyLib: " + telephonyLib);
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
    }

    protected synchronized void handleGoOfflineCommand(TelephonyCommandMessage commandMessage) {
	if (state == OFFLINE || state == CONFIGURING) {
	    logger.error("HandleGoOfflineCommand: Attempting to go online in wrong state: " + state);
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, TelephonyAckMessage.FALSE, RSController.ERROR_WRONG_STATE);

	    return;
	}
	    
	if (telephonyLib != null && telephonyLib.goOffline()) {
	    logger.debug("HandleGoOfflineCommand: Telephony offline.");
	    setState(OFFLINE);
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, TelephonyAckMessage.TRUE);
	} else {
	    logger.error("HandleGoOfflineCommand: Unable to go offline. telephonyLib:" + telephonyLib);
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
    }

    protected void handleEnableConfCommand(TelephonyCommandMessage commandMessage) {
	boolean success = false;
	Hashtable<String,String> options = new Hashtable<String, String>();
	String resourceRole = getResourceRoleToRequest(commandMessage.getCommand()); 
	if (!testResourceConfig(resourceRole)) {
	    reportResourceConfigFailed(commandMessage, resourceRole);
	    return;
	}

	if (!requestResource(resourceRole)) {
	    reportResourceRequestFailed(commandMessage, resourceRole);
	    return;
	}

	if (telephonyLib != null && telephonyLib.enableConf()) {
	    success = true;
	    logger.debug("HandleEnableConfCommand: Enabled conference.");
	}

	String[] toRelease = getResourceRolesToRelease(commandMessage.getCommand(), success);
	if (toRelease != null)
	    releaseResources(toRelease);

	if (success) {
	    insertCallInfo(null, true, LibService.START_TELEPHONY);
	    String filename = telephonyLib.getConferenceMonitorFile();
	    options.put(TelephonyCommandMessage.MONITOR_FILE_NAME, filename);
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, TelephonyAckMessage.TRUE);
	} else {
	    logger.error("HandleEnableConfCommand: telephonyLib: "+telephonyLib);
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
	
    }


    protected void sendUpdateIndexMessage(String itemID) {
	final String libFilename = itemID;
	final UpdateProgramMessage updateMessage = new UpdateProgramMessage(serviceInstanceName, 
									    RSController.INDEX_SERVICE,
									    new String[]{libFilename});
	final FlushIndexMessage flushMessage = new FlushIndexMessage (serviceInstanceName, RSController.INDEX_SERVICE);
	
	
	Thread t = new Thread("SendUpdateIndexMessage: UpdateProgram"){
		public void run(){
		    try{
			sendSyncCommand(updateMessage);
			logger.info("SendUpdateIndexMessage: Added new entries in index: " + libFilename);
		    } catch (Exception e){
			logger.error("SendUpdateIndexMessage: Unable to send UpdateProgramMessage for index update.", e);
		    }
		    
		    try{
			sendSyncCommand(flushMessage);
		    } catch (Exception e) {
			logger.error("SendUpdateIndexMessage: Unable to send FlushIndexMessage to Index Service", e);
		    }
		}
	    };
	t.start();
    }

    protected boolean disableConference() {
	boolean success = false;
	String filename = telephonyLib.getConferenceMonitorFile();

	if (telephonyLib != null && telephonyLib.disableConf()) {

	    logger.debug("DisableConference: Disabled conference.");
	    if (filename != null) {
		updateCallDone(filename, true, true);

		if (shouldSendFileToLib()) 
		    pushFileToLib(filename, true);
	    } else {
		logger.warn("DisableConference: Conference monitor filename is null.");
	    }

	    return true;
	} else {
	    logger.error("DisableConference: Failed to disable conference.");
	    return false;
	}
    }

    protected synchronized void handleDisableConfCommand(TelephonyCommandMessage commandMessage) {
	boolean success = disableConference();

	String[] toRelease = getResourceRolesToRelease(commandMessage.getCommand(), success);
	if (toRelease != null)
	    releaseResources(toRelease);

	if (success) {
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, TelephonyAckMessage.TRUE);
	} else {
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
    }

    protected void uiServiceConnected(String newID) {
	logger.info("UIServiceConnected: OldID: "+uiServiceID+" newID: "+newID);

	if (!uiServiceID.equals("") && !(uiServiceID.equals(newID))){
	    disableTelephony();
	} else if (uiServiceID.equals(newID)) {
	
	    TelephonyInfoMessage infoMessage;
	    
	    if (telephonyLib != null) {
		Hashtable<String,String> calls = telephonyLib.getAllCalls();
		boolean isConferenceOn = telephonyLib.isConferenceOn();
		String onairCall = telephonyLib.getOnAirCall();
		String offairCall = telephonyLib.getOffAirCall();
		
		infoMessage = new TelephonyInfoMessage(serviceInstanceName, RSController.UI_SERVICE,
						       calls, isConferenceOn, onairCall, offairCall);
		
		if (sendMessage(infoMessage) == -1) {
		    logger.error("UIServiceConnected: Unable to send AudioSessionsInfoMessage");
		}
		
		/*
		ArrayList<String> registeredATAs = telephonyLib.getRegisteredATAs();
		sendATANotification(TelephonyEventMessage.ATA_REGISTERED, registeredATAs.toArray(new String[]{}));

		ArrayList<Integer> activeDahdi = telephonyLib.getActiveDahdiChannels();
		sendDahdiChannelNotification(TelephonyEventMessage.DAHDI_OK, activeDahdi.toArray(new Integer[]{}));
		*/
		String[] activePhoneLines = telephonyLib.getActiveTelephonyLines();
		sendPhoneLineNotification(TelephonyEventMessage.PHONE_LINE_OK, activePhoneLines);
	    }
	}
	
	uiServiceID = newID;
    }

    protected synchronized void handleAcceptCallCommand(TelephonyCommandMessage commandMessage) {
	boolean success = false;
	Hashtable<String,String> options = commandMessage.getOptions();
	String callGUID = options.get(TelephonyCommandMessage.CALL_GUID);
	
	if (state == CONFIGURING || state == OFFLINE || callGUID == null) {
	    if (callGUID == null)
		logger.error("HandleAcceptCallCommand: null callGUID");
	    else
		logger.error("HandleAcceptCallCommand: Accept call command received in wrong state:" + state + " callGIUD:" + callGUID);

	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_WRONG_STATE);
	    return;
	}

	String resourceRole = getResourceRoleToRequest(commandMessage.getCommand()); 
	if (!testResourceConfig(resourceRole)) {
	    reportResourceConfigFailed(commandMessage, resourceRole);
	    return;
	}

	if (!requestResource(resourceRole)) {
	    reportResourceRequestFailed(commandMessage, resourceRole);
	    return;
	}
	
	if (telephonyLib != null && telephonyLib.acceptCall(callGUID)) {
	    success = true;
	    stopIncomingRing();
	    logger.debug("HandleAcceptCallCommand: Call accepted:" + callGUID);
	}

	String[] toRelease = getResourceRolesToRelease(commandMessage.getCommand(), success);
	if (toRelease != null)
	    releaseResources(toRelease);

	if (success) {
	    insertCallInfo(callGUID, false, LibService.START_TELEPHONY);
	    String filename = telephonyLib.getMonitorFile(callGUID);
	    if (filename == null)
		logger.error("HandleAcceptCallCommand: Could not find monitor file name for callGUID: " + callGUID);
	    else
		options.put(TelephonyCommandMessage.MONITOR_FILE_NAME, filename);
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.TRUE);
	    
	} else {
	    logger.error("HandleAcceptCallCommand: Unable to accept call. telephonyLib: " + telephonyLib + " callGUID: " + callGUID);

	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
    }

    protected String getResourceRoleToRequest(String action) {
	String roleToRequest = null;
	if (action.equals(TelephonyCommandMessage.ACCEPT_CALL) ||
	    action.equals(TelephonyCommandMessage.PREVIEW_CALL) ||
	    action.equals(TelephonyCommandMessage.GO_OFFAIR) ||
	    action.equals(TelephonyCommandMessage.DIAL) ||
	    action.equals(TelephonyCommandMessage.ENABLE_CONF) ||
	    action.equals(RING_PLAYBACK)) {

	    roleToRequest = OFFAIR_PLAYBACK_ROLE;

	} else if (action.equals(TelephonyCommandMessage.GO_ONAIR)) {

	    roleToRequest = ONAIR_PLAYBACK_ROLE;

	} else {
	    logger.warn("GetResourceToRequest: Unable to find the resource role to request. action:" + action);	    
	    return "";
	}

	if (isResourceHeld(roleToRequest))
	    return "";
	else 
	    return roleToRequest;

    }

    
    protected String[] getResourceRolesToRelease(String action, boolean success) {
	if (success) {
	    if (action.equals(TelephonyCommandMessage.ACCEPT_CALL) ||
	       action.equals(TelephonyCommandMessage.PREVIEW_CALL) ||
	       action.equals(TelephonyCommandMessage.GO_OFFAIR) ||
	       action.equals(TelephonyCommandMessage.DIAL) ||
	       action.equals(TelephonyCommandMessage.ENABLE_CONF)) {
		
		if (isResourceHeld(ONAIR_PLAYBACK_ROLE)) {
		    return new String[]{ONAIR_PLAYBACK_ROLE};
		} else {
		    return null;
		}

	    } else if (action.equals(TelephonyCommandMessage.GO_ONAIR)) {

		if (isResourceHeld(OFFAIR_PLAYBACK_ROLE)) {
		    return new String[]{OFFAIR_PLAYBACK_ROLE};
		} else {
		    return null;
		}

	    } else if (action.equals(TelephonyCommandMessage.HANGUP_CALL) ||
		      action.equals(TelephonyCommandMessage.HOLD_CALL) ||
		      action.equals(TelephonyCommandMessage.DISABLE_CONF)) {

		if (isResourceHeld(ONAIR_PLAYBACK_ROLE)) {
		    return new String[]{ONAIR_PLAYBACK_ROLE};
		} else if (isResourceHeld(OFFAIR_PLAYBACK_ROLE)) {
		    return new String[]{OFFAIR_PLAYBACK_ROLE};
		} else {
		    return null;
		}

	    } else if (action.equals(TelephonyEventMessage.CALLER_HUNG_UP)) {

		if (isResourceHeld(ONAIR_PLAYBACK_ROLE) && !telephonyLib.isOnAir())
		    return new String[]{ONAIR_PLAYBACK_ROLE};
		else if (isResourceHeld(OFFAIR_PLAYBACK_ROLE) && !telephonyLib.isOffAir())
		    return new String[]{OFFAIR_PLAYBACK_ROLE};
		else 
		    return null;

	    } else {
		return null;
	    }
	} else {
	    return getResourceRolesToReleaseOnFailure(action);
	}

    }


    protected String[] getResourceRolesToReleaseOnFailure(String command) {
	HashSet<String> roles = new HashSet<String>();
	if (isResourceHeld(ONAIR_PLAYBACK_ROLE)) {
	    if (telephonyLib == null || !telephonyLib.isOnAir()) {
		roles.add(ONAIR_PLAYBACK_ROLE);
	    }
	}

	if (isResourceHeld(OFFAIR_PLAYBACK_ROLE)) {
	    if (telephonyLib == null || !telephonyLib.isOffAir()) {
		roles.add(OFFAIR_PLAYBACK_ROLE);
	    }
	}

	if (roles.size() == 0) 
	    return null;
	else
	    return roles.toArray(new String[]{});
    }
    

    protected boolean isResourceHeld(String role) {
	Boolean isHeld = heldResources.get(role);
	if (isHeld == null) 
	    return false;

	return isHeld;
    }

    protected boolean testResourceConfig(String resourceRole) {
	if (resourceRole.equals(""))
	    return true;
	ResourceConfig resourceConfig = resourceConfigs.get(resourceRole);
	return (resourceConfig != null);
    }


    protected boolean reportResourceConfigFailed(TelephonyCommandMessage commandMessage, String resourceRole) {
	logger.error("ReportResourceConfigFailed: Could not find the resource " + resourceRole + " in resourceConfig");
	return sendTelephonyAckMessage(commandMessage.getCommand(), commandMessage.getOptions(), 
				       TelephonyAckMessage.FALSE,
				       new String[] {RSController.ERROR_RESOURCE_ROLE_NOT_FOUND, resourceRole});
	
    }

    protected boolean reportResourceRequestFailed(TelephonyCommandMessage commandMessage, String resourceRole) {
	ResourceConfig resourceConfig = resourceConfigs.get(resourceRole);
	logger.error("ReportResourceRequestFailed: Resource request failed for" + resourceConfig.resourceName() + "," + resourceConfig.resourceDeviceName());
	return sendTelephonyAckMessage(commandMessage.getCommand(), commandMessage.getOptions(), 
				       TelephonyAckMessage.FALSE,
				       new String[] {RSController.ERROR_RESOURCE_NOT_FREE, resourceRole});
	
    }

    protected boolean requestResource(String resourceRole) {
	if (resourceRole.equals(""))
	    return true;

	ResourceConfig config = resourceConfigs.get(resourceRole);

	if (config == null)
	    return false;

	ResourceRequestSyncMessage message = new ResourceRequestSyncMessage(serviceInstanceName,
									    RSController.RESOURCE_MANAGER,
									    new String[] {config.resourceName()}, 
									    new String[] {resourceRole}, 
									    false);

	IPCSyncResponseMessage response = sendSyncCommand(message);
	
	if (response == null) 
	    return false;

	String[] successfulResources = ((ResourceAckSyncMessage)response).getSuccessfulResources();

	for (String resources: successfulResources)
	    if (resources.equals(config.resourceName())) {
		heldResources.put(resourceRole, true);
		return true;
	    }
	
	return false;
    }

    protected boolean releaseResources(String[] resourceRoles) {
	boolean success = true;
	ArrayList<String> resources = new ArrayList<String>();

	for (String role: resourceRoles) {
	    ResourceConfig config = resourceConfigs.get(role);
	    if (config == null) {
		continue;
	    }
	    resources.add(config.resourceName());
	}

	if (resources.size() == 0)
	    return false;

	ResourceReleaseSyncMessage message = new ResourceReleaseSyncMessage(serviceInstanceName,
									    RSController.RESOURCE_MANAGER,
									    resources.toArray(new String[]{}));
	
	IPCSyncResponseMessage response = sendSyncCommand(message);
	
	if (response == null) {
	    return false;
	}
	
	String[] successfulResources = ((ResourceReleaseAckSyncMessage)response).getSuccessfulResources();
	
	for (String resource : successfulResources) {
	    for (String role : resourceRoles) {
		ResourceConfig config = resourceConfigs.get(role);
		if (config != null && config.resourceName().equals(resource)) {
		    heldResources.put(role, false);
		    break;
		}
	    }
	}

	if (successfulResources.length != resources.size())
	    return false;
	else
	    return true;
    }

    protected boolean sendTelephonyAckMessage(String command, Hashtable<String, String> options, String ack, String... error) {
	TelephonyAckMessage ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE, 
								 WIDGET, options,
								 ack, command, error);

	if (sendMessage(ackMessage) == -1)
	    return false;
	else
	    return true;
    }

    boolean sendTelephonyEventMessage(String event, Hashtable<String, String> options) {

	TelephonyEventMessage eventMessage = new TelephonyEventMessage(serviceInstanceName, RSController.UI_SERVICE,
								       WIDGET, event,
								       options);
	if (sendMessage(eventMessage) == -1) 
	    return false;
	else
	    return true;
    }

    public void newConferenceMember(String guid) {
	if (telephonyLib == null){
	    logger.error("NewConferenceMember: TelephonyLib is null.");
	    return;
	}
    }

    protected boolean insertCallInfo(String guid, boolean isConference, int state) {
	String filename;
	if (medialib == null) {
	    logger.error("InsertCallInfo: Medialib is null.");
	    return false;
	}
	
	if (isConference) {
	    filename = telephonyLib.getConferenceMonitorFile();
	} else {
	    filename = telephonyLib.getMonitorFile(guid);
	}
	
	insertRadioProgramMetadata(filename, state, isConference);
	return true;
    }


    protected RadioProgramMetadata insertRadioProgramMetadata(String filename, int state, boolean isConference) {
	if (medialib == null) {
	    logger.error("InsertRadioProgramMetadata: Medialib is null.");
	    return null;
	}
	
	RadioProgramMetadata metadata = new RadioProgramMetadata(filename, 0, "", RadioProgramMetadata.TELEPHONY_TYPE, 
								 timeKeeper.getRealTime(), 
								 state, 
								 LibService.MEDIA_NOT_INDEXED, "",
								 stationConfiguration.getStringParam(StationConfiguration.DEFAULT_LANGUAGE),
								 stationConfiguration.getStringParam(StationConfiguration.STATION_NAME));
	String timeString = StringUtilities.getDateTimeString(metadata.getCreationTime());

	if (isConference)
	    metadata.setTitle("Telephone conference " + timeString);
	else
	    metadata.setTitle("Telephone conversation " + timeString);

	if (medialib.insert(metadata, false) > -1) {
	    logger.debug("InsertRadioProgramMetadata: Metadata insert successful.");
	    return metadata;
	} else {
	    logger.debug("InsertRadioProgramMetadata: Metadata insert failed.");
	    return null;
	}
    }

    protected void handlePreviewCallCommand(TelephonyCommandMessage commandMessage) {
	boolean success = false;
	Hashtable<String, String> options = commandMessage.getOptions();
	String callGUID = options.get(TelephonyCommandMessage.CALL_GUID);
	
	if (state == CONFIGURING || callGUID == null){
	    if (callGUID == null)
		logger.error("HandlePreviewCallCommand: null callGUID");
	    else
		logger.error("HandlePreviewCallCommand: Received previewCall command in wrong state: "+state+" callGUID: "+callGUID);
	    TelephonyAckMessage ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE, 
								     commandMessage.getWidgetName(), options,
								     TelephonyAckMessage.FALSE, 
								     commandMessage.getCommand(),
								     RSController.ERROR_WRONG_STATE);
	    sendMessage(ackMessage);
	    return;
	}

	String resourceRole = getResourceRoleToRequest(commandMessage.getCommand()); 
	if (!testResourceConfig(resourceRole)) {
	    reportResourceConfigFailed(commandMessage, resourceRole);
	    return;
	}

	if (!requestResource(resourceRole)) {
	    reportResourceRequestFailed(commandMessage, resourceRole);
	    return;
	}
	
	if (telephonyLib != null && telephonyLib.previewCall(callGUID)) {
	    logger.debug("HandlePreviewCallCommand: Previewing call: "+callGUID);
	    success = true;
	} else {
	    logger.error("HandlePreviewCallCommand: Unable to preview: telephonyLib: "+telephonyLib);
	}

	String[] toRelease = getResourceRolesToRelease(commandMessage.getCommand(), success);
	if (toRelease != null)
	    releaseResources(toRelease);

	if (success) {
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.TRUE);
	    
	} else {
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
    }

    protected synchronized void handleHoldCallCommand(TelephonyCommandMessage commandMessage) {
	boolean success = false;
	Hashtable<String, String> options = commandMessage.getOptions();
	String callGUID = options.get(TelephonyCommandMessage.CALL_GUID);
	
	if (state == CONFIGURING || callGUID == null){
	    if (callGUID == null)
		logger.error("HandleHoldCallCommand: null callGUID.");
	    else
		logger.error("HandleHoldCallCommand: Hold call command received in wrong state: "+ state + " callGUID: "+ callGUID);
	    TelephonyAckMessage ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE,
								     commandMessage.getWidgetName(), options,
								     TelephonyAckMessage.FALSE, 
								     commandMessage.getCommand(),
								     RSController.ERROR_WRONG_STATE);
	    sendMessage(ackMessage);
	    return;
	}

	if (telephonyLib != null && telephonyLib.holdCall(callGUID)) {
	    logger.debug("HandleHoldCallCommand: Call put on hold:"+callGUID);
	    success = true;
	} else {
	    logger.error("HandleHoldCallCommand: Unable to hold call. telephonyLib: "+telephonyLib);
	}

	String[] toRelease = getResourceRolesToRelease(commandMessage.getCommand(), success);
	if (toRelease != null)
	    releaseResources(toRelease);

	if (success) 
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, TelephonyAckMessage.TRUE);
	else 
	    sendTelephonyAckMessage(commandMessage.getCommand(), options, TelephonyAckMessage.FALSE, 
				    RSController.ERROR_UNKNOWN);
    }

    protected synchronized void handleHangUpCommand(TelephonyCommandMessage commandMessage) {
	boolean success = false;
	Hashtable<String,String> options = commandMessage.getOptions();
	String callGUID = options.get(TelephonyCommandMessage.CALL_GUID);

	if (state == CONFIGURING || callGUID == null) {
	    if (callGUID == null)
		logger.error("HandleHangupCallCommand: null callGUID.");
	    else
		logger.error("HandleHangupCallCommand: Hangup call command received in wrong state:" + state + " callGIUD:" + callGUID);

	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_WRONG_STATE);
	    
	    return;
	}

	String filename = telephonyLib.getMonitorFile(callGUID);

	if (telephonyLib != null && telephonyLib.hangupCall(callGUID)) {
	    logger.debug("HandleHangupCallCommand: Call hung up:" + callGUID);
	    success = true;
	    if(incomingRingCallGUID.equals(callGUID)) {
		stopIncomingRing();
	    }
	    if(outgoingRingCallGUID.equals(callGUID)) {
		stopOutgoingRing();
	    }
	}

	String[] toRelease = getResourceRolesToRelease(commandMessage.getCommand(), success);
	if (toRelease != null)
	    releaseResources(toRelease);

	if (success) {
	    if (filename != null) {
		updateCallDone(filename, false, true);

		if (shouldSendFileToLib()) 
			pushFileToLib(filename, true);
	    } else {
		logger.debug("HandleHangupCallCommand: filename is null");
	    }

	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.TRUE);
	    
	} else {
	    logger.error("HandleHangupCallCommand: Unable to hangup call. telephonyLib: " + telephonyLib);

	    sendTelephonyAckMessage(commandMessage.getCommand(), options, 
				    TelephonyAckMessage.FALSE, RSController.ERROR_UNKNOWN);
	}
    }

    protected boolean updateCallDone(String filename, boolean isConference, boolean updateIndex) {
	if (medialib == null) {
	    logger.error("UpdateCallDone: Unable to update database. Database not connected.");
	    return false;
	} 
	
	RadioProgramMetadata dummyProgram = new RadioProgramMetadata(filename);
	RadioProgramMetadata program = medialib.getSingle(dummyProgram);
	
	if (program == null) {

	    program = insertRadioProgramMetadata(filename, LibService.DONE_TELEPHONY, isConference);
	    if (program == null){
		logger.error("UpdateCallDone: Unable to update: "+filename);
		return false;
	    }

	}
	
	FileStoreUploader.clearAllFileStoreFields(program);
	FileStoreUploader.setFileStoreField(StationNetwork.ONLINETELEPHONY, program, FileStoreManager.UPLOAD_DONE);

	long length = -1L;
	String localStoreDir = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.ONLINETELEPHONY);
	String completeFileName = localStoreDir + File.separator + filename;
	String checksum = FileUtilities.getSha1Hex(completeFileName);
	if (checksum == null)
	    checksum = "";

	try {
	    length = Long.parseLong(RSPipeline.getMetadata(completeFileName).get(RSPipeline.DURATION));
	} catch (Exception e){
	    logger.error("UpdateCallDone: Unable to find file length.", e);
	}
	program.setLength(length);
	program.setChecksum(checksum);
	program.setState(LibService.DONE_TELEPHONY);

	if (medialib.update(dummyProgram, program) > -1) {
	    logger.debug("UpdateCallDone: Database update successfull.");
	   
	    if (updateIndex)
		sendUpdateIndexMessage(filename);
	    
	    return true;
	} else {
	    logger.error("UpdateCalldone: Database update failed.");
	    return false;
	}
    }
    
    protected synchronized void handleMuteCallCommand(TelephonyCommandMessage commandMessage) {
	Hashtable<String,String> options = commandMessage.getOptions();
	String callGUID = options.get(TelephonyCommandMessage.CALL_GUID);

	if (state == CONFIGURING || callGUID == null) {
	    if (callGUID == null)
		logger.error("HandleMuteCallCommand: null callGUID.");
	    else
		logger.error("HandleMuteCallCommand: Mute call command received in wrong state:" + state + " callGIUD:" + callGUID);
	    TelephonyAckMessage ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE, 
								     commandMessage.getWidgetName(), options,
								     TelephonyAckMessage.FALSE, 
								     commandMessage.getCommand(),
								     RSController.ERROR_WRONG_STATE);
	    
	    sendMessage(ackMessage);
	    return;
	}
	
	TelephonyAckMessage ackMessage;
	if (telephonyLib != null && telephonyLib.muteCall(callGUID)) {
	    logger.debug("HandleMuteCallCommand: Call muted:"+callGUID);
	    ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE,
								     commandMessage.getWidgetName(), options,
								     TelephonyAckMessage.TRUE,
								     commandMessage.getCommand());
	} else {
	    logger.error("HandleMuteCallCommand: Unable to mute call. telephonyLib: "+telephonyLib);
	    ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE, 
								     commandMessage.getWidgetName(), options,
								     TelephonyAckMessage.FALSE, 
								     commandMessage.getCommand(),
								     RSController.ERROR_UNKNOWN);
	    
	}

	sendMessage(ackMessage);

    }

    protected synchronized void handleUnmuteCallCommand(TelephonyCommandMessage commandMessage) {
	Hashtable<String,String> options = commandMessage.getOptions();
	String callGUID = options.get(TelephonyCommandMessage.CALL_GUID);

	if (state == CONFIGURING || callGUID == null) {
	    if (callGUID == null)
		logger.error("HandleUnmuteCallCommand: null callGUID.");
	    else
		logger.error("HandleUnmuteCallCommand: Unmute call command received in wrong state:" + state + " callGIUD:" + callGUID);
	    TelephonyAckMessage ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE, 
								     commandMessage.getWidgetName(), options,
								     TelephonyAckMessage.FALSE, 
								     commandMessage.getCommand(),
								     RSController.ERROR_WRONG_STATE);
	    
	    sendMessage(ackMessage);
	    return;
	}
	
	TelephonyAckMessage ackMessage;
	if (telephonyLib != null && telephonyLib.unmuteCall(callGUID)) {
	    logger.debug("HandleUnmuteCallCommand: Call unmuted:"+callGUID);
	    ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE,
								     commandMessage.getWidgetName(), options,
								     TelephonyAckMessage.TRUE,
								     commandMessage.getCommand());
	} else {
	    logger.error("HandleUnmuteCallCommand: Unable to unmute call. telephonyLib: "+telephonyLib);
	    ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE, 
								     commandMessage.getWidgetName(), options,
								     TelephonyAckMessage.FALSE, 
								     commandMessage.getCommand(),
								     RSController.ERROR_UNKNOWN);
	}

	sendMessage(ackMessage);
    }


    protected synchronized void handleDisconnectCommand(TelephonyCommandMessage commandMessage) {
	if (state == CONFIGURING){
	    TelephonyAckMessage ackMessage = new TelephonyAckMessage(serviceInstanceName, RSController.UI_SERVICE,
								     commandMessage.getWidgetName(), null,
								     TelephonyAckMessage.FALSE,
								     commandMessage.getCommand());
	    sendMessage(ackMessage);
	    return;
	} 

	if (telephonyLib != null) {
	    logger.debug("HandleDisconnectCommand: About to disc.");

	    disableTelephony();
	    
	    sendTelephonyAckMessage(commandMessage.getCommand(), null, TelephonyAckMessage.TRUE);
	}

	setState(OFFLINE);
    }


    protected synchronized void handleRegistrationAckMessage(boolean ack, String id){
	boolean sendMessages = false;
	String oldID = resourceManagerID;
	resourceManagerID = id;

	if (ack) {
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);

	    if (!checkMedialibStatus()) {
		logger.error("HandleRegistrationAckMessage: Medialib status check failed.");
		StatusCheckRequestMessage msg = new StatusCheckRequestMessage(serviceInstanceName, 
									      RSController.LIB_SERVICE, 
									      "", "1");
		sendMessage(msg);
		return;
	    }

	    if (!checkTelephonyStatus()) {
		telephonyDown();
		return;
	    }
	    
	    telephonyLib.setMediaLib(medialib);
	    if (oldID.equals("")) {
		sendMessages = true;

	    } else if (!resourceManagerID.equals(oldID)){
		logger.error("HandleRegistrationAckMessage: RSController went down. Dropping all phone calls");
		disableTelephony();
		isOnlineTelephonyServiceReady = false;
		sendMessages = true;

	    } else if (!isOnlineTelephonyServiceReady) {
		if (sendReadyMessage(true) == -1)
		    connectionError();
	    }

	    if (sendMessages) {
		ServiceInterestMessage serviceInterestMessage = 
		    new ServiceInterestMessage(serviceInstanceName, 
					       RSController.RESOURCE_MANAGER,
					       "", new String[]{RSController.LIB_SERVICE});
		sendMessage(serviceInterestMessage);
	    }

	} else {
	    resetService();
	}
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }

    protected void resetService(){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	isOnlineTelephonyServiceReady = false;
	disableTelephony();

	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	sendMessage(message);
    }

    protected void disableTelephony() {
	
	if (telephonyLib != null) {
	    telephonyLib.goOffline();
	    disableConference();
	    telephonyLib.destroyPhoneCalls();
	}

	releaseAllResources();
    }


    protected boolean checkTelephonyStatus() {
	if (telephonyLib == null)
	    return false;
	
	if (telephonyLib.isConnected()) {
	    if (medialib != null) {
		telephonyLib.setMediaLib(medialib);
	    }
	    return true;
	} else
	    return false;
    }

    protected boolean checkMedialibStatus() {
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib == null)
	    return false;

	medialib.setLibraryActivated(true);
	if (telephonyLib != null)
	    telephonyLib.setMediaLib(medialib);

	return true;
    }

    protected boolean checkStatus() {
	return (checkMedialibStatus() && checkTelephonyStatus());
    }

    public synchronized void activateMediaLib(boolean activate) {
	if (activate){
	} else {
	    if (isOnlineTelephonyServiceReady) {
		logger.error("ActivateMediaLib: Database connection error.");
		setState(CONFIGURING);
		disableTelephony();
		StatusCheckRequestMessage msg = new StatusCheckRequestMessage(serviceInstanceName, 
									      RSController.LIB_SERVICE, 
									      "", "1");
		sendMessage(msg);
		sendReadyMessage(false);
	    }
	}
    }

    protected int sendReadyMessage(boolean readiness) {

	ServiceReadyMessage readyMessage = new ServiceReadyMessage(serviceInstanceName, 
								   RSController.RESOURCE_MANAGER, 
								   readiness);

	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isOnlineTelephonyServiceReady = readiness;
		logger.info("SendReadyMessage: Ready messsage sent. isServiceReady: " + isOnlineTelephonyServiceReady);

		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Caught exception.", e);
	    return -1;
	}
    }

    protected IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) {
	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null){
	    return null;
	}
	try {
	    if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
		logger.error("SendSyncMessage: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	    }
	} catch (IPCSyncMessageTimeoutException e) {
	    logger.error("SendSyncMessage: Sync message timed out.");
	}
	return responseMessage;
    }

    protected int sendMessage(IPCMessage ipcMessage) {

	if (ipcServer == null)
	    return -1;

	int retVal = ipcServer.handleOutgoingMessage(ipcMessage);
	if (retVal == -1) {
	    logger.error("SendMessage: Unable to send message " + ipcMessage.getClass());
	    connectionError();
	} else if (retVal == -2) {
	    logger.error("SendMessage: Unable to send message " + ipcMessage.getClass());
	    // Do nothing
	}
	return retVal;
    }

    public String getName() {
	return serviceInstanceName;
    }    

    public void newCall(String callGUID, RSCallerID callerID) {
	if (state == ONLINE /*|| state == ONCALL*/) {
	    if(shouldPlayIncomingRing()) {
		startIncomingRing(callGUID);
	    }

	    Hashtable <String, String> options = new Hashtable<String, String>();
	    options.put(TelephonyEventMessage.CALL_GUID, callGUID);
	    options.put(TelephonyEventMessage.CALLER_ID, callerID.toString());

	    sendTelephonyEventMessage(TelephonyEventMessage.NEW_CALL, options);

	} else {
	    logger.info("Got new call when we are not online.");
	}
    }

    public void dialNumberCallback(String callGUID, String state, String paramString) {
	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(TelephonyEventMessage.CALL_GUID, callGUID);
	options.put(TelephonyEventMessage.CALL_STATE, state);       
	
	if (state.equals(TelephonyLib.DIALING)) {
	    startOutgoingRing(state, callGUID);
	    if (paramString.equals("")) {
		logger.error("DialNumberCallback: Temporary guid must be provided when updating state of an outgoing call to 'Dialing'");
	    } else {
		options.put(TelephonyEventMessage.TEMP_CALL_GUID, paramString);
	    }
	} else if (state.equals(TelephonyLib.SUCCESS)) {
	    stopOutgoingRing();
	    insertCallInfo(callGUID, false, LibService.START_TELEPHONY);
	    String filename = telephonyLib.getMonitorFile(callGUID);
	    options.put(TelephonyEventMessage.MONITOR_FILE_NAME, filename);
	} else if (state.equals(TelephonyLib.RINGING)) {
	    stopOutgoingRing();
	    startOutgoingRing(state, callGUID);
	}

	sendTelephonyEventMessage(TelephonyEventMessage.OUTGOING_CALL_UPDATE, options);
    }

    public synchronized void callerHungup(String callGUID, String cause) {
	logger.info("CallerHungup: callGUID: " + callGUID + " cause: " + cause);
	logger.debug("CallerHungup: Sending event message to provider.");

	String filename = telephonyLib.getMonitorFile(callGUID);
	if (filename != null) {
	    updateCallDone(filename, false, true);
	    
	    if (shouldSendFileToLib()) 
		pushFileToLib(filename, true);
	} else 
	    logger.debug("CallerHungup: filename is null");
	
	if(incomingRingCallGUID.equals(callGUID)) {
	    stopIncomingRing();
	}

	if(outgoingRingCallGUID.equals(callGUID)) {
	    stopOutgoingRing();
	}

	String[] toRelease = getResourceRolesToRelease(TelephonyEventMessage.CALLER_HUNG_UP, true);
	if (toRelease != null)
	    releaseResources(toRelease);

	Hashtable<String,String> options = new Hashtable<String,String>();
	options.put(TelephonyEventMessage.CALL_GUID, callGUID);
	options.put(TelephonyEventMessage.HANGUP_CAUSE, cause);
	
	sendTelephonyEventMessage(TelephonyEventMessage.CALLER_HUNG_UP, options);

    }

    
    public synchronized void sendPhoneLineNotification(String event, String... phonelineIDs){
	if (phonelineIDs.length == 0){
	    logger.warn("SendPhoneLineNotification: Not sending notification for null phonelineIDs");
	    return;
	}

	if (uiServiceID.equals("")) {
	    logger.debug("SendPhoneLineNotification: Not sending notification as UI service is not registered.");
	    return;
	}

	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(TelephonyEventMessage.PHONE_LINE_ID, getCSVFromArray(phonelineIDs));

	sendTelephonyEventMessage(event, options);
    }

    /*
    public synchronized void sendATANotification(String ataEvent, String... ataIDs){
	if (ataIDs.length == 0){
	    logger.warn("SendATANotification: Not sending notification for null ataIDs");
	    return;
	}

	if (uiServiceID.equals("")) {
	    logger.debug("SendATANotification: Not sending notification as UI service is not registered.");
	    return;
	}

	Hashtable<String, String> options = new Hashtable<String, String>();

	options.put(TelephonyEventMessage.ATA_ID, getCSVFromArray(ataIDs));

	sendTelephonyEventMessage(ataEvent, options);
    }
    */

    /*
    public synchronized void sendDahdiChannelNotification(String channelEvent, Integer... channels){
	if (channels.length == 0){
	    logger.warn("SendDahdiChannelNotification: Not sending notification for null channels");
	    return;
	}

	if (uiServiceID.equals("")) {
	    logger.debug("SendATANotification: Not sending notification as UI service is not registered.");
	    return;
	}

	Hashtable<String, String> options = new Hashtable<String, String>();

	options.put(TelephonyEventMessage.CHANNEL_NO, getCSVFromArray(channels));

	sendTelephonyEventMessage(channelEvent, options);
    }
    */

    public static void main(String args[]) {
	Getopt g = new Getopt("OnlineTelephonyService", args, "c:m:");
	int c;
	String configFilePath = "./automation.conf";
	String machineID = "127.0.0.1";
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c':
		configFilePath = g.getOptarg();
		break;
	    case 'm':
		machineID = g.getOptarg();
		break;
	    }
	}

	StationConfiguration stationConfiguration = new StationConfiguration(machineID);
	stationConfiguration.readConfig(args[0]);

	OnlineTelephonyService telephonyService = new OnlineTelephonyService(stationConfiguration,
									     stationConfiguration.getServiceInstanceOnMachine(RSController.ONLINE_TELEPHONY_SERVICE, machineID), 
									     machineID);

    }

    //Send file to lib store if there is atleast one consumer store that does not have the file
    protected boolean shouldSendFileToLib() {
	StationNetwork stationNetwork = stationConfiguration.getStationNetwork();
	String[] localStores = stationNetwork.getSameStores(StationNetwork.ONLINETELEPHONY);
	boolean playoutFound = false;
	boolean previewFound = false;
	boolean uploadFound = false;

	for (String store : localStores) {
	    if (store.equals(StationNetwork.AUDIO_PLAYOUT))
		playoutFound = true;
	    else if (store.equals(StationNetwork.AUDIO_PREVIEW)) 
		previewFound = true;
	    else if (store.equals(StationNetwork.UPLOAD))
		uploadFound = true;
	}

	if (playoutFound && previewFound && uploadFound)
	    return false;
	else 
	    return true;
    }

    protected void pushFileToLib(String fileName, boolean triggerUpload) {
	fileStoreManager.addFileToUpload(fileName);

	if (triggerUpload)
	    fileStoreManager.triggerUpload();
       
    }

    public void serviceDown(String service){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	isOnlineTelephonyServiceReady = false;
	disableTelephony();
    }

    public synchronized IPCServer attemptReconnect(){
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    timeKeeper.setIPCServer(ipcServer);
	    fileStoreManager.setIPCServer(ipcServer);
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }

    public synchronized void handleUncaughtException(Thread t, Throwable ex){
	logger.debug("HandleUncaughtException:");
    }

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");

	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }
    
    protected void releaseAllResources() {
	ArrayList<String> toRelease = new ArrayList<String>();

	for (String role: resourceConfigs.keySet()) {
	    if (isResourceHeld(role)) {
		toRelease.add(role);
	    }
	}

	releaseResources(toRelease.toArray(new String[] {}));
    }

    public synchronized void telephonyDown() {
	telephonyLib = null;
	setState(CONFIGURING);
	releaseAllResources();
	if (isOnlineTelephonyServiceReady) {
	    if (sendReadyMessage(false) == -1) {
		connectionError();
		return;
	    }
	}

	diagUtil.telephonyError(this);
    }

    public synchronized void telephonyUp(TelephonyLib lib) {
	logger.info("TelephonyUp:");
	telephonyLib = lib;
	setState(OFFLINE);
	telephonyLib.goOffline();
	if (!isOnlineTelephonyServiceReady && !resourceManagerID.equals("") && checkStatus()) {
	    telephonyLib.setMediaLib(medialib);
	    if (sendReadyMessage(true) == -1)
		connectionError();
	    
	}
	    
	if (!uiServiceID.equals("")){
	    /*
	    ArrayList<String> registeredATAs = telephonyLib.getRegisteredATAs();
	    sendATANotification(TelephonyEventMessage.ATA_REGISTERED, registeredATAs.toArray(new String[]{}));

	    ArrayList<Integer> activeDahdi = telephonyLib.getActiveDahdiChannels();
	    sendDahdiChannelNotification(TelephonyEventMessage.DAHDI_OK, activeDahdi.toArray(new Integer[]{}));
	    */
	    String[] activePhoneLines = telephonyLib.getActiveTelephonyLines();
	    sendPhoneLineNotification(TelephonyEventMessage.PHONE_LINE_OK, activePhoneLines);
	}
    }

    
    public void attemptTelephonyUp() {
	Thread t = new Thread(serviceInstanceName + ":AttemptTelephonyUp") {
		public void run() {
		    TelephonyLib.getTelephonyLib(stationConfiguration, null, null);
		}
	    };
	t.start();
    }
    
    
    protected synchronized void setState(int newState) {
	logger.info("SetState: " + state + " => " + newState);
	state = newState;
    }

    //Methods playing rings and tones in the preview headphones.
    boolean playFile(String fileName) {
	RSPipeline.setInputFile(bin, fileName);
	return RSPipeline.play(bin);
    }

    void startIncomingRing(String callGUID) {
	String localStoreDir = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.ONLINETELEPHONY);
	String fileName = localStoreDir+"/"+RING_INCOMING_CALL;
	String resourceRole = getResourceRoleToRequest(RING_PLAYBACK);

	if (requestResource(resourceRole)) {
	    if(playFile(fileName)) {
		incomingRingCallGUID = callGUID;
		logger.debug("StartIncomingRing: Ring started.");
	    }
	} else {
	    logger.error("StartIncomingRing: Resource request failed.");
	}
    }

    void stopRing() {
	RSPipeline.stop(bin);
    }

    void stopIncomingRing() {
	stopRing();
	incomingRingCallGUID = "";
	logger.debug("StopIncomingRing: Stopped.");
    }

    void startOutgoingRing(String state, String callGUID) {
	String localStoreDir = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.ONLINETELEPHONY);
	String fileName;
	if(state.equals(TelephonyLib.DIALING)) {
	    fileName = localStoreDir+"/"+TONE_SEARCHING_OUTGOING_CALL;
	} else {
	    fileName = localStoreDir+"/"+RING_OUTGOING_CALL;
	}

	String resourceRole = getResourceRoleToRequest(RING_PLAYBACK);
	if (requestResource(resourceRole)) {
	    if(playFile(fileName)) {
		outgoingRingCallGUID = callGUID;
		logger.debug("StartOutgoingRing: Ring started.");
	    }
	} else {
	    logger.error("StartOutgoingRing: Resource request failed.");
	}
    }

    void stopOutgoingRing() {
	stopRing();
	outgoingRingCallGUID = "";
	logger.info("StopOutgoingRing: Stopped.");
    }

    boolean isIncomingRingPlaying() {
	return (!incomingRingCallGUID.equals(""));
    }

    boolean isOutgoingRingPlaying() {
	return (!outgoingRingCallGUID.equals(""));
    }

    boolean shouldPlayIncomingRing() {
	return (!telephonyLib.isOnAir() && !telephonyLib.isOffAir() && !isOutgoingRingPlaying() && !isIncomingRingPlaying());
    }

    boolean shouldPlayOutgoingRing() {
	return (!telephonyLib.isOnAir() && !telephonyLib.isOffAir() && !isOutgoingRingPlaying() && !isIncomingRingPlaying());
    }

    //Pipeline listener methods
    public void playDone(RSBin bin){
	logger.debug("PlayDone: " + bin.getName());
    }

    public void gstError(RSBin bin, String error){
	logger.error("GstError: "+error+" received for "+bin.getName());
	if(isIncomingRingPlaying()) {
	    stopIncomingRing();
	}

	if(isOutgoingRingPlaying()) {
	    stopOutgoingRing();
	}
    }

    public void playDone(RSBin bin, String key){
    }
    public void gstError(RSBin bin, String key, String error){
    }


}
