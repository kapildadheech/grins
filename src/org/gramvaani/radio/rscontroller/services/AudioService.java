package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.libmsgs.*;
import org.gramvaani.utilities.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.medialib.PlayoutHistory;
import org.gramvaani.radio.rscontroller.pipeline.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;
import org.gramvaani.radio.app.gui.DiagnosticsController;

import java.util.*;
import java.io.File;

import org.gstreamer.*;
import org.gstreamer.elements.*;
import org.gstreamer.event.*;

import gnu.getopt.Getopt;

public class AudioService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, DefaultUncaughtExceptionCallback, DiagnosticUtilitiesConnectionCallback {
    public final static String SUCCESS = "SUCCESS";
    public final static String FAILURE = "FAILURE";

    // service state
    protected final static int LISTENING = 0;
    protected final static int PLAY = 1;
    protected final static int PAUSE = 2;
    protected final static int STOP = 3;

    // commands
    public final static String PLAY_MSG      = "PLAY";
    public final static String PAUSE_MSG     = "PAUSE";
    public final static String STOP_MSG      = "STOP";
    public final static String FORCE_STOP_MSG= "FORCESTOP";
    public final static String PLAY_DONE_MSG = "PLAYDONE";
    public final static String VOL_UP_MSG    = "VOL_UP";
    public final static String VOL_DOWN_MSG  = "VOL_DOWN";
    public final static String VOL_CHANGED_MSG= "VOL_CHANGED";
    public final static String SEEK_MSG	     = "SEEK";
    public final static String SET_GOLIVE_MSG= "SET_GOLIVE";
    
    //audio command options
    public final static String OPTION_FADEOUT_START = "FADEOUT_START";
    public final static String OPTION_GAIN	    = "GAIN";
    public final static String OPTION_GUID	    = "GUID";
    public final static String OPTION_GOLIVE	    = "GOLIVE";
    public final static String OPTION_UPDATE_HISTORY= "UPDATE_HISTORY";
    public final static String OPTION_POSITION	    = "POSITION";

    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected String serviceInstanceName;
    protected String machineID;
    protected LinkMonitorClient linkMonitor = null;
    protected SessionTable sessionTable;

    // resource state
    protected final static int RESOURCE_FREE      = 0;
    protected final static int RESOURCE_REQUESTED = 1;
    protected final static int RESOURCE_ACQUIRED  = 2;
    
    // resource types and resource names
    public final static String PLAYOUT = "PLAYOUT";
    public final static String PREVIEW = "PREVIEW";
    
    protected AudioResourceTable audioResourceTable;
    protected LogUtilities logger;

    protected String mixer, deviceName;

    protected TimeKeeper timeKeeper;
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected DiagnosticUtilities diagUtil;
    protected String registrationID;
    protected String resourceManagerID = "";
    protected String uiServiceID = "";
    protected boolean networkDown = false;
    protected boolean isAudioServiceReady = false;
    
    public final static double DELTA_GAIN = 6.0;
    protected final static double DEFAULT_GOLIVE_BG_GAIN = -12.0;

    public AudioService(IPCServer ipcServer, StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.ipcServer = ipcServer;
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;
	this.sessionTable = new SessionTable(serviceInstanceName);
	this.audioResourceTable = new AudioResourceTable();
	logger = new LogUtilities(serviceInstanceName);
	logger.debug("AudioService: Entering. Initializing with serviceInstanceName="+serviceInstanceName+" machineId="+machineID);
	logger.info("AudioService: Initializing.");
	registrationID = GUIDUtils.getGUID();
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	audioConfig();
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	ipcServer.registerIPCNode(serviceInstanceName, this, registrationID);
	logger.debug("AudioService: Leaving");
    }

    public AudioService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	this.machineID = machineID;
	this.sessionTable = new SessionTable(serviceInstanceName);
	this.audioResourceTable = new AudioResourceTable();
	logger = new LogUtilities(serviceInstanceName);
	logger.debug("AudioService: Entering. Initializing with serviceInstanceName="+serviceInstanceName+" machineId="+machineID);
	logger.info("AudioService: Initializing.");
	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "AudioService");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);
	registrationID = GUIDUtils.getGUID();
	audioConfig();
        ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER), 
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	linkMonitor = new LinkMonitorClient(this, serviceInstanceName, stationConfiguration);
	linkMonitor.start();
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	if (ipcServer.registerIPCNode(serviceInstanceName, this, registrationID) == -1) {
	    logger.error("AudioService: Unable to connect Audio Service");
	    connectionError();
	}
	else
	    logger.debug("AudioService: Connected to ipcServer.");
	
    }

    public synchronized IPCServer attemptReconnect(){
	/*
	ipcServer = IPCServerStub.initConnection(
						 stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER), 
						 stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
						 stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY), 
						 this,
						 1,
						 stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
						 registrationID);
	*/
	//if (ipcServer != null) {
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    timeKeeper.setIPCServer(ipcServer);
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }

    public void serviceDown(String service){
	destroyAudioSessions();
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
    }

    protected void audioConfig() {
	logger.debug("AudioConfig: Entering");
	ServiceResource[] serviceResources = stationConfiguration.getServiceResources(serviceInstanceName);
	for (ServiceResource serviceResource: serviceResources) {
	    logger.debug("AudioConfig: Config service resource:" + serviceResource.resourceName() + ":" + serviceResource.resourceRole());
	    audioResourceTable.addAudioResource(new AudioResource(serviceResource.resourceName(), serviceResource.resourceRole()));
	    ResourceConfig resourceConfig = stationConfiguration.getResourceConfig(serviceResource.resourceName());
	    mixer = resourceConfig.resourceMixer();
	    deviceName = resourceConfig.resourceDeviceName();
	}

	RSPipeline.init(stationConfiguration);

	logger.debug("AudioConfig: Leaving");
    }

    protected int sendReadyMessage() {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(serviceInstanceName, RSController.RESOURCE_MANAGER,
	 						   true);
	//Added while below to avoid null pointer exceptions when in n/w down state
	/*if (ipcServer != null) {
	    int retVal = ipcServer.handleOutgoingMessage(readyMessage);
	    if (retVal != -1) {
		isAudioServiceReady = true;
	    }
	    return retVal;
	}
	else
	return -1;*/

	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isAudioServiceReady = true;
		logger.debug("SendReadyMessage: Ready messsage sent. isAudioServiceReady=" + isAudioServiceReady);
		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Message timeout exception.");
	    return -1;
	}
    }

    protected IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null){
	    logger.error("SendSyncCommand: IPCServer is null.");
	    return null;
	}
	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
	    logger.error("SendSyncCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	}
	return responseMessage;
    }

    public synchronized void connectionError() {
	logger.error("ConnectionError:");
	networkDown = true;
	timeKeeper.setIPCServer(null);
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);
    }

    public synchronized void handleUncaughtException(Thread t,Throwable ex){
	logger.error(t.getName());
	logger.debug("HandleUncaughtException: Exiting..");
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering. From: " + message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof ResourceAckMessage) {
	    String[] successfulResources = ((ResourceAckMessage)message).getSuccessfulResources();
	    String[] unsuccessfulResources = ((ResourceAckMessage)message).getFailedResources();
	    String[] successfulResourceRoles = ((ResourceAckMessage)message).getSuccessfulResourceRoles();
	    String[] unsuccessfulResourceRoles = ((ResourceAckMessage)message).getUnsuccessfulResourceRoles();
	    handleResourceAckMessage(successfulResources, successfulResourceRoles, unsuccessfulResources, unsuccessfulResourceRoles);
	} else if (message instanceof AudioCommandMessage) {
	    AudioCommandMessage audioCommandMessage = (AudioCommandMessage)message;
	    handleAudioCommandMessage(audioCommandMessage.getCommand(), audioCommandMessage.getPlayType(),
				      audioCommandMessage.getWidgetName(), audioCommandMessage.getSessionID(),
				      audioCommandMessage.getFileID(), audioCommandMessage.getOptions());
	} else if (message instanceof ServiceErrorMessage) {
	    ServiceErrorMessage serviceErrorMessage = (ServiceErrorMessage)message;
	    if (serviceErrorMessage.getErrorCode() == ServiceErrorMessage.NOT_FOUND) {
		IPCMessage messagePayload = IPCMessage.deserialize(serviceErrorMessage.getPayload(), 
								   stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY));

		if (messagePayload != null && messagePayload instanceof AudioAckMessage) {
		    String sessionID = ((AudioAckMessage)messagePayload).getSessionID();
		    if (!sessionID.equals("") && sessionTable.getAudioSession(sessionID) != null) {
			sessionTable.getAudioSession(sessionID).unableToDeliverAck((AudioAckMessage)messagePayload);
		    }
		}
	    }
	} else if (message instanceof ServiceErrorRelayMessage) {
	    ServiceErrorRelayMessage errorRelayMessage = (ServiceErrorRelayMessage) message;
	    if (errorRelayMessage.getServiceName().equals(RSController.UI_SERVICE)){
		logger.debug("HandleIncomingMessage: ServiceErrorRelayMessage received for UI Service. Resetting Audio Service.");
		destroyAudioSessions();
	    }

	} else if (message instanceof RadioProgramStatUpdateResponseMessage) {
	    // XXX ignore for now. Flash success or error to UI.

	} else if (message instanceof TimeKeeperResponseMessage) {
	    timeKeeper.handleTimeKeeperResponseMessage((TimeKeeperResponseMessage)message);

	} else if (message instanceof HeartbeatMessage && linkMonitor != null){
	    HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
	    linkMonitor.handleMessage(heartbeatMessage);

	} else if (message instanceof ServiceActivateNotifyMessage) {
	    ServiceActivateNotifyMessage notifyMessage = (ServiceActivateNotifyMessage)message;
	    if (notifyMessage.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		logger.debug("HandleIncomingMessage: ServiceActivateNotifyMessage received for UI Service.");
		timeKeeper.sync();
		uiServiceConnected(notifyMessage.getRegistrationID());
	    }

	} else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());

	} else if (message instanceof MachineStatusRequestMessage){
	    MachineStatusRequestMessage requestMessage = (MachineStatusRequestMessage)message;
	    handleMachineStatusRequestMessage(requestMessage.getWidgetName(), requestMessage.getCommand(), requestMessage.getOptions());
	} else if (message instanceof ServiceNotifyMessage){
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage) message;
	    String[] activatedServices = notifyMessage.getActivatedServices();
	    String[] activatedIDs = notifyMessage.getActivatedIDs();

	    for (int i = 0; i < activatedServices.length; i++){
		if (activatedServices[i].equals(RSController.UI_SERVICE)){
		    uiServiceConnected(activatedIDs[i]);
		    break;
		}
	    }
	} else if (message instanceof PreemptResourceMessage) {
	    PreemptResourceMessage prMessage= (PreemptResourceMessage) message;
	    String[] resources = prMessage.getResources();
	    handlePreemptResourceMessage(resources);
	}

	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }

    protected void uiServiceConnected(String newID){
	logger.debug("UIServiceConnected: OldID: "+uiServiceID+" newID: "+newID);
	if (!uiServiceID.equals("") && !(uiServiceID.equals(newID))){
	    destroyAudioSessions();
	} 

	ArrayList<String> sessionIDs = new ArrayList<String>();
	ArrayList<String> sessionStates = new ArrayList<String>();
	ArrayList<String> programIDs = new ArrayList<String>();
	ArrayList<Integer> positions = new ArrayList<Integer>();

	//Assuming all sessions have the same playtype
	for (AudioSession session : sessionTable.getAudioSessions()) {
	    sessionIDs.add(session.getSessionID());
	    sessionStates.add(((Integer)session.getState()).toString());
	    programIDs.add(session.getProgramID());
	    positions.add(session.getPosition());
	}

	uiServiceID = newID;
	AudioSessionsInfoMessage infoMessage = new AudioSessionsInfoMessage(serviceInstanceName, RSController.UI_SERVICE, sessionIDs.toArray(new String[]{}), sessionStates.toArray(new String[]{}), programIDs.toArray(new String[]{}), positions.toArray(new Integer[]{}));

	if (sendMessage(infoMessage) < 0) {
	    logger.error("UIServiceConnected: Unable to send AudioSessionsInfoMessage");
	    connectionError();
	}

    }

    protected void handlePreemptResourceMessage(String[] resources) {
	ArrayList<AudioSession> sessions = sessionTable.getAudioSessions();
	boolean success = false;

	if (sessions.size() == 0) {
	    logger.error("HandlePreemptResourceMessage: Got preempt resource message when we are not holding any.");
	    
	    //although there is an error somewhere, letting RM carry on giving the resource to the requesting service
	    success = true;
	} else {
	    AudioSession session = sessions.get(0);
	    if (session.getResourceType().equals(PLAYOUT)) {
		logger.error("HandlePreemptResourceMessage: Got preempt resource message for playout service.");
		success = false;
	    } else if (!session.getResourceName().equals(resources[0])) {
		logger.error("HandlePreemptResourceMessage: Got preempt resource message for resource." + resources[0] + " not held by us. We have:" + session.getResourceName());
		success = false;
	    } else {
		session.stop();
		success = true;
	    }
	}

	PreemptResourceAckMessage message;
	if (success) {
	    message = new PreemptResourceAckMessage(serviceInstanceName, RSController.RESOURCE_MANAGER, "TRUE", resources);
	} else {
	    message = new PreemptResourceAckMessage(serviceInstanceName, RSController.RESOURCE_MANAGER, "FALSE", resources);
	}
	
	int retVal = sendMessage(message);
	if (retVal < 0){
	    logger.error("HandlePreemptResourceMessage: Unable to send Preempt ACK.");
	    connectionError();
	    return;
	}

    }

    protected void handleRegistrationAckMessage(boolean ack, String id){
	logger.debug("HandleRegistrationAckMessage: ack="+ack+" id="+id+" resourceManagerID="+resourceManagerID);
	if (ack) {

	    String oldID = resourceManagerID;
	    resourceManagerID = id;
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);

	    if (networkDown)
		networkDown = false;
	    if (oldID.equals("")) {
		if (sendReadyMessage() == -1)
		    connectionError();
	    } else if (!resourceManagerID.equals(oldID)){
		logger.error("HandleRegistrationAckMessage: RSController went down. Destroying all audio sessions.");
		destroyAudioSessions();
		if (sendReadyMessage() == -1)
		    connectionError();
	    } else if (!isAudioServiceReady) {
		if (sendReadyMessage() == -1)
		    connectionError();
	    }

	} else {
	    resetService();
	}
    }

    protected void resetService(){
	destroyAudioSessions();
	resourceManagerID = "";
	registrationID = GUIDUtils.getGUID();
	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	if (ipcServer.handleOutgoingMessage(message) == -1)
	    connectionError();
    }

    protected void handleResourceAckMessage(String[] successfulResources, String[] successfulResourceRoles, String[] unsuccessfulResources,
					    String[] unsuccessfulResourceRoles) {
	logger.debug("HandleResourceAckMessage: Entering");

	AudioSession[] audioSessions;
	if (successfulResources.length > 0) {
	    logger.debug("HandleResourceAckMessage: resource = " + successfulResources[0] 
			 + ", session = " 
			 + audioResourceTable.getAudioResource(successfulResourceRoles[0]).getSessionIDs());

	    synchronized(audioResourceTable){
		audioSessions = sessionTable.getAudioSessions(audioResourceTable.getAudioResource(successfulResourceRoles[0]).getSessionIDs());
	    }

	    if (audioSessions.length > 0) {
		for (AudioSession audioSession: audioSessions){

		    audioSession.resourceAck(AudioService.SUCCESS);
		}
	    } else {
		// unexpected message
		logger.error("HandleResourceAckMessage: Unexpected Message received.");
	    }
	} else if (unsuccessfulResources.length > 0) {
	    logger.debug("HandleResourceAckMessage: unsuccessfulResource="+unsuccessfulResources[0]);
	    synchronized(audioResourceTable){
		audioSessions = sessionTable.getAudioSessions(audioResourceTable.getAudioResource(unsuccessfulResourceRoles[0]).getSessionIDs());
	    }

	    if (audioSessions.length > 0) {
		for (AudioSession audioSession: audioSessions)
		    audioSession.resourceAck(AudioService.FAILURE);
	    } else {
		logger.error("HandleResourceAckMessage: Unexpected Message received.");
	    }
	} else {
	    logger.error("HandleResourceAckMessage: Corrupt Ack Received.");
	}
	logger.debug("HandleResourceAckMessage: Leaving.");
    }

    protected void handleAudioCommandMessage(String command, String playType, 
					     String widgetName, String sessionID, 
					     String fileID, Hashtable<String, String> options) {
	
	logger.debug("HandleAudioCommandMessage: Entering " + command + " : " + playType + " : " + fileID + " : " + sessionID + ":");

	AudioResource audioResource = audioResourceTable.getAudioResource(playType);
	
	String ackOptions[];

	String guid = options.get(AudioService.OPTION_GUID);
	if (guid == null)
	    ackOptions = new String[]{};
	else
	    ackOptions = new String[]{guid};

	if (audioResource == null) {
	    logger.error("HandleAudioCommandMessage: Could not find the specified resource in resourceConfig");
	    AudioAckMessage audioAckMessage = new AudioAckMessage(serviceInstanceName, 
								  RSController.UI_SERVICE,
								  widgetName, sessionID, fileID, 
								  ackOptions, AudioService.FAILURE,
								  -1L, command,
								  new String[] {RSController.ERROR_RESOURCE_ROLE_NOT_FOUND,playType});
	    if (sendMessage(audioAckMessage) == -1) {
		logger.error("Play: Unable to send NACK.");
		connectionError();
	    }
	    else
		logger.debug("Play: NACK sent.");
	    
	    return;
	}

	String resourceName = audioResource.getResourceName();    
	if (command.equals(AudioService.PLAY_MSG)) {
	    AudioSession audioSession;
	    if (sessionID.equals("")) {
		audioSession = new AudioSession(playType, resourceName, mixer, deviceName, widgetName, this, serviceInstanceName);
		sessionTable.add(audioSession);
	    } else {
		audioSession = sessionTable.getAudioSession(sessionID);
	    }
	    
	    if (audioSession != null){
		long fadeoutStartTime = -1;
		double gain = 0.0;
		String fadeoutStr = "", gainStr = "";
		
		try{

		    fadeoutStr = options.get(AudioService.OPTION_FADEOUT_START);
		    if (fadeoutStr != null)
			fadeoutStartTime = Long.parseLong(fadeoutStr);

		    gainStr = options.get(AudioService.OPTION_GAIN);
		    if (gainStr != null)
			gain = Double.parseDouble(gainStr);

		} catch (Exception e){
		    logger.error("HandleAudioCommandMessage: Unable to parse play options. fadeoutString=" + fadeoutStr + " gainString=" + gainStr, e);
		}

		guid = options.get(AudioService.OPTION_GUID);
		if (guid == null)
		    guid = "";

		boolean goLiveActive = false;
		
		String opt = options.get(AudioService.OPTION_GOLIVE);
		try {
		    if (opt != null)
			goLiveActive = Boolean.parseBoolean(opt);
		} catch(Exception e) {
		    logger.error("HandleAudioCommandMessage: Unable to parse goLiveActive option.", e);
		}

		boolean updateHistory = true;
		opt = options.get(AudioService.OPTION_UPDATE_HISTORY);

		if (opt != null) {
		    try{
			updateHistory = Boolean.parseBoolean(opt);
		    } catch (Exception e){
			logger.error("HandleAudioCommandMessage: Unable to parse updateHistory option.", e);
		    }
		}

		long position = -1;
		opt = options.get(AudioService.OPTION_POSITION);
		if (opt != null){
		    try{
			position = Long.parseLong(opt);
		    } catch (Exception e){
			logger.error("HandleAudioCommandMessage: Unable to parse position option.", e);
		    }
		}

		audioSession.setGUID(guid);
		audioSession.play(fileID, fadeoutStartTime, gain, goLiveActive, updateHistory, position);
		logger.debug("HandleAudioCommandMessage: audioSession playing.");
	    } else {
		logger.error("HandleAudioCommandMessage: Could not locate audio session: "+sessionID+" for command: "+command);
	    }

	} else if (command.equals(AudioService.PAUSE_MSG)) {
	    AudioSession audioSession = sessionTable.getAudioSession(sessionID);
	    if (audioSession != null) {
		audioSession.pause();
		logger.debug("HandleAudioCommandMessage: audioSession paused.");
	    } else {
		//XXX: Send a nack saying pause failed. 
		String error;
		if (audioSession == null)
		    error = "for unlocatable audioSession.";
		else
		    error = "in state: "+audioSession.getState();
		logger.error("HandleAudioCommandMessage: Corrupt Message received. Command: "+command+" received "+error);
	    }
	} else if (command.equals(AudioService.STOP_MSG)) {
	    AudioSession audioSession = sessionTable.getAudioSession(sessionID);
	    if (audioSession != null) {
		audioSession.stop();
		logger.debug("HandleAudioCommandMessage: audioSession stopped.");
	    } else {
		//XXX: Send a nack saying stop failed. 
		String error;
		if (audioSession == null)
		    error = "for unlocatable audioSession.";
		else
		    error = "in state: "+audioSession.getState();
		logger.error("HandleAudioCommandMessage: Corrupt Message received. Command: "+command+" received "+error);
	    }
	} else if (command.equals(AudioService.VOL_UP_MSG) || command.equals(AudioService.VOL_DOWN_MSG) || command.equals(AudioService.VOL_CHANGED_MSG)){
	    String gain = "";
	    if (command.equals(AudioService.VOL_CHANGED_MSG))
		gain = options.get(AudioService.OPTION_GAIN);
	    changeVolume(playType, command, gain, sessionID);

	} else if (command.equals(AudioService.FORCE_STOP_MSG)) {

	    destroyAudioSessions();

	} else if (command.equals(AudioService.SET_GOLIVE_MSG)) {
	    setGoLive(playType, sessionID, options.get(AudioService.OPTION_GOLIVE));
	} else if (command.equals(AudioService.SEEK_MSG)) {
	    seekToPosition(playType, sessionID, options.get(AudioService.OPTION_POSITION));
	} else {
	    logger.error("HandleAudioCommandMessage: Unknown command: "+command);
	}

	logger.debug("HandleAudioCommandMessage: Leaving");
    }

    protected void handleMachineStatusRequestMessage(String widget, String command, String[] options){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = ((StationNetwork)stationConfiguration.getStationNetwork()).getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), RSController.UI_SERVICE, widget, command, new String[] {dirName, used, available});
	    
	    if (sendMessage(responseMessage) == -1){
		logger.error("HandleMachineStatusRequestMessage: Unable to send responseMessage.");
		connectionError();
	    } else {
		logger.debug("HandleMachineStatusRequestMessage: Response sent.");
	    }
	}
    }

    protected void setGoLive(String playType, String sessionID, String active) {
	if (playType.equals(PLAYOUT)) {
	    AudioSession session = sessionTable.getAudioSession(sessionID);
	    if (session == null) {
		logger.error("SetGoLive: Session found null.");
		return;
	    }
	    
	    boolean goLive;
	    try {
		goLive = Boolean.parseBoolean(active);
	    } catch(Exception e) {
		logger.error("SetGoLive: Unable to parse go live active field: " + active);
		return;
	    }
	    session.setGoLive(goLive);

	} else {
	    logger.warn("SetGoLive: Set golive message received for wrong playtype: " + playType);
	}
    }

    protected void changeVolume(String playType, String command, String gainStr, String sessionID){
	AudioSession session = sessionTable.getAudioSession(sessionID);	

	if (session == null) {
	    logger.error("ChangeVolume: Could not locate session: "+sessionID);
	    return;
	}

	session.changeVolume(command, gainStr);
    }

    protected void seekToPosition (String playType, String sessionID, String position){
	AudioSession session = sessionTable.getAudioSession (sessionID);
	
	if (session == null){
	    logger.error("SeekToPostion: Could not locate session: "+sessionID);
	    return;
	}

	session.seekToPosition (Integer.parseInt(position));
    }

    protected void destroyAudioSessions(){
	for (AudioSession session: sessionTable.getAudioSessions()){
	    session.destroySession();
	}

	if (RSBin.PLAYOUT.isInit())
	    RSPipeline.setGain(RSBin.PLAYOUT, 0.0);
    }

    public String getName() {
	return serviceInstanceName;
    }

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    //XXX replace the code inside the function by a call to sendMessage
    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");

	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }


    public static void main(String args[]) {
	Getopt g = new Getopt("AudioService", args, "cn:");
	int c;
	String configFilePath = "./automation.conf";
	String serviceInstanceName = StationConfiguration.AUDIO_SERVICE_DEFAULT_INSTANCE;
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    case 'n':
		serviceInstanceName = g.getOptarg();
		break;
	    }
	}
	
	StationConfiguration stationConfiguration = new StationConfiguration();
	stationConfiguration.readConfig(configFilePath);
	String machineID = stationConfiguration.getServiceInstanceMachineID(RSController.AUDIO_SERVICE, serviceInstanceName);
	stationConfiguration.setLocalMachineID(machineID);
	AudioService audioService = new AudioService(stationConfiguration, serviceInstanceName, machineID);
    }

    protected int sendMessage(IPCMessage ipcMessage) {

	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(ipcMessage);
	else
	    return -1;
    }

    protected void setResourceStatus(String resourceType, int resourceStatus) {
	audioResourceTable.getAudioResource(resourceType).setResourceStatus(resourceStatus);
    }

    protected int getResourceStatus(String resourceType) {
	return audioResourceTable.getAudioResource(resourceType).getResourceStatus();
    }

    protected void destroySession(String sessionID) {
	logger.debug("DestroySession: " + sessionID);
	sessionTable.remove(sessionID);
    }

    protected void addResourceSessionID(String resourceType, String sessionID) {
	synchronized(audioResourceTable){
	    audioResourceTable.getAudioResource(resourceType).addSessionID(sessionID);
	}
    }

    protected void removeResourceSessionID(String resourceType, String sessionID){
	synchronized (audioResourceTable){
	    audioResourceTable.getAudioResource(resourceType).removeSessionID(sessionID);
	}
    }

    protected StationConfiguration getStationConfiguration() {
	return stationConfiguration;
    }

    protected TimeKeeper getTimeKeeper() {
	return timeKeeper;
    }
        
    class AudioSession implements PipelineListener{
	protected String sessionID;
	protected int serviceState;
	protected String resourceName;
	protected String resourceType;
	protected String widgetName;
	protected AudioService audioService;
	
	protected String fileID;
	protected long fadeoutStartTime, position;
	protected double gain;
	protected boolean goLiveActive, updateHistory;
	protected boolean prevGoLiveActive;
	protected String guid;

	//protected PlayPipeline pipeline;
	protected RSBin bin;
	protected LogUtilities logger;

	public AudioSession(String resourceType, String resourceName, String mixer, String deviceName, String widgetName, AudioService audioService, String logPrefix) {
	    sessionID = GUIDUtils.getGUID();
	    serviceState = AudioService.LISTENING;
	    this.resourceName = resourceName;
	    this.resourceType = resourceType;
	    this.widgetName = widgetName;
	    this.audioService = audioService;
	    goLiveActive = false;
	    logger = new LogUtilities(logPrefix+":AudioSession: "+sessionID);

	    if (resourceType.equals(PLAYOUT)){
		bin = RSBin.PLAYOUT;
	    } else {
		bin = RSBin.PREVIEW;
	    }

	    RSPipeline.addPipelineListener(bin, this);
	}

	public void setGUID(String guid) {
	    this.guid = guid;
	}

	public String getResourceType() {
	    return resourceType;
	}

	public String getResourceName() {
	    return resourceName;
	}

	public String getProgramID() {
	    return fileID;
	}

	public int getState(){
	    return serviceState;
	}

	public void changeVolume(String command, String gainStr) {	    
	    double gain;

	    if (command.equals(AudioService.VOL_UP_MSG)) {
		RSPipeline.changeGain(bin, DELTA_GAIN);
	    } else if (command.equals(AudioService.VOL_DOWN_MSG)) {
		RSPipeline.changeGain(bin, -DELTA_GAIN);		
	    } else if (command.equals(AudioService.VOL_CHANGED_MSG)){
		try {
		    gain = Double.parseDouble(gainStr);
		} catch(NumberFormatException e) {
		    logger.error("ChangeVolume: Could not parse gain value:" + gainStr);
		    gain = 0.0;
		}

		double deltaGain = gain - RSPipeline.linearToDB(bin.getVolume());
		if (!goLiveActive)
		    RSPipeline.changeGain(bin, deltaGain);		
	    } else {
		logger.warn("ChangeVolume: Unknown command: " + command);
	    }
	    logger.info("ChangeVolume: RSBin volume set to: " + bin.getVolume());
	}
	
	public void seekToPosition (int position){
	    RSPipeline.seekToPosition(bin, position, sessionID);
	}

	public int getPosition() {
	    return RSPipeline.getPosition(bin, sessionID);
	}

	public boolean getGoLive() {
	    return goLiveActive;
	}

	public void setGoLive(boolean active) {
	    if (goLiveActive == active) {
		return;

	    } else {

		double goLiveBGGain = stationConfiguration.getDoubleParam(StationConfiguration.GOLIVE_BG_GAIN);
		if (goLiveBGGain == -1.0) {
			goLiveBGGain = DEFAULT_GOLIVE_BG_GAIN;
		}
		if (active) {
		    RSPipeline.changePipeGain(bin, goLiveBGGain);
		} else {
		    RSPipeline.changePipeGain(bin, -goLiveBGGain);
		}
		prevGoLiveActive = goLiveActive;
		goLiveActive = active;
	    }
	}

	public void play(String fileID, long fadeoutStartTime, double gain, boolean goLiveActive, boolean updateHistory, long position) {

	    logger.debug("Play: Entering");
	    logger.info("Play:" + fileID);
	    this.fileID = fileID;
	    this.fadeoutStartTime = fadeoutStartTime;
	    this.gain = gain;
	    prevGoLiveActive = this.goLiveActive;
	    this.goLiveActive = goLiveActive;
	    this.updateHistory = updateHistory;
	    this.position = position;

	    int status = audioService.getResourceStatus(resourceType);
	    // Preview allows only one stream at a time. Hence fail if status != RESOURCE_FREE.
	    if (resourceType.equals(PREVIEW) && status != RESOURCE_FREE) {

		logger.error("Play: Required resource not free.");
		AudioAckMessage audioAckMessage = new AudioAckMessage(serviceInstanceName, 
								      RSController.UI_SERVICE,
								      widgetName, sessionID, fileID, new String[]{guid}, 
								      AudioService.FAILURE,
								      -1L, AudioService.PLAY_MSG, 
								      new String[]{RSController.ERROR_RESOURCE_NOT_FREE});
		if (audioService.sendMessage(audioAckMessage) == -1) {
		    logger.error("Play: Unable to send NACK.");
		    reconnectOrExit();
		}
		else
		    logger.debug("Play: NACK sent.");

	    } else if (status == RESOURCE_ACQUIRED && resourceType.equals(PLAYOUT)) {

		// Multi streams allowed, and we have already acquired resource.

		resourceAck(ResourceManager.SUCCESS_MSG);

	    } else if (status == RESOURCE_FREE) {
		// For both playout and preview.

		logger.debug("Play: Required resource is free. Sending resource request to RM: " + resourceType);
		audioService.setResourceStatus(resourceType, RESOURCE_REQUESTED);
		audioService.addResourceSessionID(resourceType, sessionID);
		ResourceRequestMessage resourceRequestMessage = new ResourceRequestMessage(serviceInstanceName,
											   RSController.RESOURCE_MANAGER,
											   new String[] {resourceName}, 
											   new String[] {resourceType}, 
											   false);

			if (audioService.sendMessage(resourceRequestMessage) == -1) {
			    logger.error("Play: Unable to send ResourceRequest message.");
			    reconnectOrExit();
			}
			else {
			    logger.debug("Play: ResourceRequest message sent.");
			}

	    } else if (status == RESOURCE_REQUESTED && resourceType.equals(PLAYOUT)){
		// Wait for the response to request already sent.

		audioService.addResourceSessionID(resourceType, sessionID);
	    }
	    logger.debug("Play: Leaving");
	}

	public void playDone(RSBin bin){

	}

	public void gstError(RSBin bin, String error){

	}

	public void playDone(RSBin bin, String key){
	    logger.info("PlayDone: Finished playing "+fileID);
	    //resourceRelease();
	    destroySession();
	    AudioAckMessage audioAckMessage = new AudioAckMessage(serviceInstanceName, 
								  RSController.UI_SERVICE,
								  widgetName, sessionID, 
								  fileID, new String[]{guid}, 
								  AudioService.SUCCESS, -1L,
								  AudioService.PLAY_DONE_MSG);
	    if (audioService.sendMessage(audioAckMessage) == -1) {
		logger.error("PlayDone: Unable to send Play Done ACK.");
		reconnectOrExit();
	    }
	    else
		logger.debug("PlayDone: Play done ACK sent.");
	    
	}

	public void gstError(RSBin bin, String key, String error){
	    AudioErrorMessage audioErrorMessage = new AudioErrorMessage(serviceInstanceName,
									RSController.UI_SERVICE,
									widgetName, sessionID, fileID, new String[]{guid},
									error);

	    if (audioService.sendMessage(audioErrorMessage) == -1){
		logger.error("GstError: Unable to send GstError message.");
		reconnectOrExit();
	    } else {
		logger.debug("GstError: GstError message sent.");
	    }
	}

	public void pause() {
	    // pause playing
	    logger.debug("Pause: Entering");
	    logger.info("Pause:");
	    
	    //XXX: Test to make sure we are in Playing state else return nack with appropriate error

	    audioService.removeResourceSessionID(resourceType, sessionID);
	    RSPipeline.pause(bin, sessionID);
	    serviceState = AudioService.PAUSE;
	    sessionTable.setSessionState(sessionID, AudioService.PAUSE);
	    
	    if (!sessionTable.isSessionActive()){
		audioService.setResourceStatus(resourceType, RESOURCE_FREE);
		ResourceReleaseMessage resourceReleaseMessage = new ResourceReleaseMessage(serviceInstanceName,
											   RSController.RESOURCE_MANAGER,
											   new String[] {resourceName});
		if (audioService.sendMessage(resourceReleaseMessage) == -1) {
		    logger.error("Pause: Unable to send ResourceRelease message.");
		    reconnectOrExit();
		    return;
		}
		else
		    logger.debug("Pause: ResourceRelease message sent.");
	    }
	    
	    logger.info("Pause: State set to PAUSE.");
	    AudioAckMessage audioAckMessage = new AudioAckMessage(serviceInstanceName, 
								  RSController.UI_SERVICE,
								  widgetName, sessionID, fileID, new String[]{guid}, AudioService.SUCCESS, -1L,
								  AudioService.PAUSE_MSG);
	    if (audioService.sendMessage(audioAckMessage) == -1) {
		logger.error("Pause: Unable to send ACK.");
		    RSPipeline.stop(bin, sessionID);
		    reconnectOrExit();
	    }
	    else
		logger.debug("Pause: Ack message sent.");
	    
	    logger.debug("Pause: Leaving");
	}

	public void stop() {
	    // stop playing
	    logger.debug("Stop: Entering");
	    logger.info("Stop:");
	    //resourceRelease();
	    destroySession();
	    AudioAckMessage audioAckMessage = new AudioAckMessage(serviceInstanceName, 
								  RSController.UI_SERVICE,
								  widgetName, sessionID, fileID, new String[]{guid}, AudioService.SUCCESS, -1L,
								  AudioService.STOP_MSG);
	    if (audioService.sendMessage(audioAckMessage) == -1) {
		logger.error("Stop: Unable to send ACK.");
		reconnectOrExit();
	    }
	    else
		logger.debug("Stop: ACK sent to UI Service");
	    logger.debug("Stop: Leaving");
	}

	public void resourceAck(String status) {
	    logger.debug("ResourceAck: Entering. Status="+status);
	    boolean result;
	    double pipeGain = gain;	    
	    boolean fromPauseToPlay;
	    if (serviceState == AudioService.LISTENING || serviceState == AudioService.PAUSE) {
		if (status.equals (ResourceManager.SUCCESS_MSG)) {
		    audioService.setResourceStatus(resourceType, AudioService.RESOURCE_ACQUIRED);
		    logger.info("ResourceAck: Resource acquired: "+resourceType);

		    fromPauseToPlay = RSPipeline.isPaused(sessionID);
		    
		    if (goLiveActive && resourceType.equals(PLAYOUT) && !fromPauseToPlay) {
			double tmpGain = stationConfiguration.getDoubleParam(StationConfiguration.GOLIVE_BG_GAIN);
			if (tmpGain == -1.0) {
			    tmpGain = DEFAULT_GOLIVE_BG_GAIN;
			}
			pipeGain += tmpGain;
		    }

		    String filename = audioService.getStationConfiguration().getStringParam(StationConfiguration.LIB_BASE_DIR) + System.getProperty("file.separator") +fileID;
		    result = RSPipeline.play(bin, filename, sessionID, fadeoutStartTime, position, pipeGain, this);
		    
		    int prevState = serviceState;
		    sessionTable.setSessionState(sessionID, AudioService.PLAY);

		    String ack;
		    boolean releaseResource = false;
		    long telecastTime = audioService.getTimeKeeper().getRealTime();
		    if (result){
			logger.debug("ResourceAck: pipeline started/resumed successfully.");

			//When going from pause to play gain provided to RSPipeline is ignored to 
			//preserve replaygain volume. Now if goLiveActive has changed since last setting
			//from false to true then we need to explicitly call setGoLive for ourselves
			//There should not be a case where we are going from pause to play and 
			//prevGoLiveActive=true and goLiveActive=false
			if (fromPauseToPlay && goLiveActive && !prevGoLiveActive) {
			    goLiveActive = false; //to make the setGoLive call actually do its work
			    setGoLive(true);
			}

			ack = AudioService.SUCCESS;
			serviceState = AudioService.PLAY;
			logger.info("ResourceAck: State set to PLAY. prev state was " + prevState);

			if (updateHistory && resourceType.equals(PLAYOUT) 
			   && !fileID.equals(DiagnosticsController.DIAGNOSTICS_PLAYOUT_FILE)
			   && !fileID.equals(DiagnosticsController.DIAGNOSTICS_PREVIEW_FILE) && 
			   prevState != AudioService.PAUSE) {

			    RadioProgramStatUpdateMessage statMessage = new RadioProgramStatUpdateMessage(serviceInstanceName,
													  RSController.LIB_SERVICE,
													  new PlayoutHistory(fileID, audioService.getStationConfiguration().getStringParam(StationConfiguration.STATION_NAME), telecastTime));
			    int retVal = audioService.sendMessage(statMessage);
			    //XXX:Should there be a check for retval == -1?
			    if (retVal < 0)
				logger.error("ResourceAck: Unable to send StatisticsUpdate message.");
			    else
				logger.debug("ResourceAck: StatisticsUpdate message sent.");
			}
		    }
		    else{
			ack = AudioService.FAILURE;
			logger.error("ResourceAck: Unable to PLAY. State unchanged.");
			releaseResource = true;
		    }

		    AudioAckMessage audioAckMessage = new AudioAckMessage(serviceInstanceName, 
									  RSController.UI_SERVICE,
									  widgetName, sessionID, fileID, new String[]{guid},
									  ack, telecastTime,
									  AudioService.PLAY_MSG);

		    int retVal = audioService.sendMessage(audioAckMessage);
		    //XXX: Should there be a check for retval == -1?
		    if (retVal < 0) {
			logger.error("ResourceAck: Unable to send ACK.");
			if (ack == AudioService.SUCCESS)
			    releaseResource = true;
		    }
		    else
			logger.debug("ResourceAck: ACK sent.");

		    logger.debug("ResourceAck: releaseResource="+releaseResource);

		    //XXX Here destroy session is independent from resource release. Verify whether this is corr.

		    if (releaseResource) {
			destroySession();
			if (ack == AudioService.SUCCESS) {
			    //pipeline.stop();
			    RSPipeline.stop(bin);
			    serviceState = AudioService.LISTENING;
			    logger.info("ResourceAck: State changed to LISTENING.");
			}
			if (retVal != -1 && !sessionTable.isSessionActive()) {
			    ResourceReleaseMessage resourceReleaseMessage = new ResourceReleaseMessage(serviceInstanceName,
												       RSController.RESOURCE_MANAGER,
												       new String[] {resourceName});
			    if (audioService.sendMessage(resourceReleaseMessage) == -1) {
				retVal = -1;
				logger.error("ResourceAck: Unable to send ResourceRelease message.");				
			    }
			    else
				logger.debug("ResourceAck: ResourceRelease message sent.");
			}
		    }

		    if (retVal == -1) {
			reconnectOrExit();
		    }
		} else {
		    //Resource could not be acquired.
		    AudioAckMessage audioAckMessage = new AudioAckMessage(serviceInstanceName, 
									  RSController.UI_SERVICE,
									  widgetName, sessionID, fileID, new String[]{guid}, 
									  AudioService.FAILURE, -1L,
									  AudioService.PLAY_MSG,
									  new String[] {RSController.ERROR_RESOURCE_ACK_FAILED});
		    if (audioService.sendMessage(audioAckMessage) == -1) {
			logger.error("ResourceAck: Unable to send ACK="+AudioService.FAILURE);
		        reconnectOrExit();
			return;
		    }
		    else
			logger.debug("ResourceAck: ACK="+AudioService.FAILURE+" sent.");
		    destroySession();
		}
		
	    }
	    else{
		logger.error("ResourceAck: Ack received in the wrong state. Service state: "+serviceState+" resource status: "+status);
	    }
	    logger.debug("ResourceAck: Leaving");
	}

	public void unableToDeliverAck(AudioAckMessage message) {
	    logger.debug("UnableToDeliverAck: Entering");
	    logger.error("UnableToDeliverAck: To: "+message.getDest());
	    if (message.getCommand().equals(AudioService.PLAY_MSG)) {
		destroySession();
	    }
	    serviceState = AudioService.LISTENING;
	    logger.info("State changed to LISTENING.");
	}
	

	public String getSessionID() {
	    return sessionID;
	}

	protected void reconnectOrExit() {
	    audioService.connectionError();
	}

	private void destroySession() {
	    audioService.removeResourceSessionID(resourceType, sessionID);
	    RSPipeline.stop(bin, sessionID);
	    audioService.destroySession(sessionID);	    

	    if (!sessionTable.isSessionActive()){
		ResourceReleaseMessage resourceReleaseMessage = new ResourceReleaseMessage(serviceInstanceName,
											   RSController.RESOURCE_MANAGER,
											   new String[] {resourceName});
		
		audioService.setResourceStatus(resourceType, AudioService.RESOURCE_FREE);

		if (audioService.sendMessage(resourceReleaseMessage) == -1) {
		    logger.error("DestroySession: Unable to send ResourceRelease message.");
		    RSPipeline.stop(bin, sessionID);
		    reconnectOrExit();
		    return;
		}
		else{
		    logger.debug("DestroySession: ResourceRelease message sent");
		}
	    }
	}
    }

    class SessionTable {
	Hashtable<String, AudioSession> sessionTable;
	Hashtable<String, Integer> statusTable;

	LogUtilities logger;

	//	public void dump() {
	//	    for (AudioSession session: sessionTable.values()) {
	//		logger.debug("SessionTable: " + session.getSessionID());
	//	    }
	//	}

	public SessionTable(String logPrefix) {
	    sessionTable = new Hashtable<String, AudioSession>();
	    statusTable  = new Hashtable<String, Integer>();
	    logger = new LogUtilities(logPrefix+":SessionTable");
	}

	public AudioSession getAudioSession(String sessionID) {
	    return sessionTable.get(sessionID);
	}

	public AudioSession[] getAudioSessions(String[] sessionIDs){
	    ArrayList<AudioSession> list = new ArrayList<AudioSession>();
	    for (String id: sessionIDs){
		AudioSession session = sessionTable.get(id);
		if (session != null)
		    list.add(session);
	    }
	    return list.toArray(new AudioSession[]{});
	}

	public void remove(String sessionID){
	    logger.debug("Remove: "+sessionID);
	    sessionTable.remove(sessionID);
	    statusTable.remove(sessionID);
	}

	public void add(AudioSession audioSession) {
	    logger.debug("Add: "+audioSession.getSessionID());
	    sessionTable.put(audioSession.getSessionID(), audioSession);
	    statusTable.put(audioSession.getSessionID(), AudioService.PLAY);
	}

	public ArrayList<AudioSession> getAudioSessions(){
	    ArrayList<AudioSession> audioSessions = new ArrayList<AudioSession>();
	    for (AudioSession session: sessionTable.values()){
		audioSessions.add(session);
	    }
	    return audioSessions;
	}

	public int getNumSessions(){
	    return sessionTable.size();
	}

	public void setSessionState(String sessionID, int state){
	    statusTable.put(sessionID, state);
	}

	public int getSessionState(String sessionID) {
	    return statusTable.get(sessionID);
	}

	public boolean isSessionActive(){
	    for (Integer status: statusTable.values()){
		if (status.equals(AudioService.PLAY))
		    return true;
	    }
	    return false;
	}
	
    }

    class AudioResource {
	protected String resourceName;
	protected String resourceType;
	protected int resourceStatus;
	protected ArrayList<String> sessionIDs;

	public AudioResource(String resourceName, String resourceType) {
	    this.resourceName = resourceName;
	    this.resourceType = resourceType;
	    resourceStatus = AudioService.RESOURCE_FREE;
	    sessionIDs = new ArrayList<String>();
	}

	public void addSessionID(String sessionID) {
	    sessionIDs.add(sessionID);
	}

	public void removeSessionID(String sessionID){
	    sessionIDs.remove(sessionID);
	}
	
	public String[] getSessionIDs() {
	    return sessionIDs.toArray(new String[sessionIDs.size()]);
	}

	public void setResourceStatus(int resourceStatus) {
	    this.resourceStatus = resourceStatus;
	}

	public String getResourceName() {
	    return resourceName;
	}

	public String getResourceType() {
	    return resourceType;
	}

	public int getResourceStatus() {
	    return resourceStatus;
	}
    }

    class AudioResourceTable {
	Hashtable<String, AudioResource> audioResourceTable;

	public AudioResourceTable() {
	    audioResourceTable = new Hashtable<String, AudioResource>();
	}

	public void addAudioResource(AudioResource audioResource) {
	    audioResourceTable.put(audioResource.getResourceType(), audioResource);
	}

	public AudioResource getAudioResource(String resourceType) {
	    return audioResourceTable.get(resourceType);
	}

	//	public AudioResource getAudioResourceByName(String resourceName) {
	//	    return audioResourceTableByName.get(resourceName);
	//	}
    }

    /*
    class PlayPipeline implements Runnable, DecodeBin.NEW_DECODED_PAD, Bus.ASYNC_DONE{
	protected Thread thread;
	protected Pipeline pipe;
	protected PlayBin playBin;
	protected ClockTime pausedPosition;
	protected final LogUtilities logger;
	protected String fileName;
	protected AudioSession listener;
	boolean isAsyncDone = false, isSeeking = false;

	Element fileSrc, audioConvert, speexResample, volume, audioSink;
	DecodeBin decodeBin;
	Element testSrc;
	Bin source;

	public PlayPipeline (String mixer, String deviceName, String logPrefix, String resampler, AudioSession parent){
	    logger = new LogUtilities(logPrefix+"PlayPipeline");
	    logger.debug("PlayPipeline: mixer="+mixer+" deviceName="+deviceName+" logPrefix="+logPrefix+" Resampler="+resampler);
	    listener = parent;
	    int attempt = 0;
	    String[] args;
	    while (attempt < 3){
		try{
		    args = Gst.init("RSController", new String[] {});
		    break;
		}catch(Exception e){
		    attempt++;
		    logger.error("PlayPipeline: Gst Init failed.",e);
		}
	    }
	    if (attempt == 3){
		logger.fatal("PlayPipeline: Unable to initialize Gstreamer.");
	    }
	    try { 
		pipe 	= new Pipeline ("AudioServicePipeline");
		fileSrc 	= ElementFactory.make("filesrc", "filesrc");
		decodeBin 	= new DecodeBin ("decodebin");
		decodeBin.connect(this);
		audioConvert= ElementFactory.make("audioconvert", "audioconvert");
		speexResample=ElementFactory.make(resampler, resampler);
		volume 	= ElementFactory.make("volume", "volume");
		
		logger.debug("Playpipeline: Mixer: "+ mixer + "," +deviceName + "," + volume.get("volume"));
		
		if (mixer.equals (ResourceManager.PORT_JACK)){
		    audioSink = ElementFactory.make("jackaudiosink", "jackaudiosink");
		    audioSink.set ("name", deviceName);
		}
		else if (mixer.equals (ResourceManager.PORT_ALSA)){
		    audioSink = ElementFactory.make("alsasink", "alsasink");
		    audioSink.set ("device", deviceName);
		} 
		else if (mixer.equals(ResourceManager.PORT_WIN)) {
		    audioSink = ElementFactory.make("autoaudiosink", "autoaudiosink");
		}
	    } catch(Exception e){
		logger.error("PlayPipeline: Failed to create all gstreamer elements.", e);
	    }
	    
	    pipe.addMany  (fileSrc,decodeBin, audioConvert,speexResample,volume,audioSink);
	    pipe.linkMany (fileSrc,decodeBin);
	    pipe.linkMany (audioConvert,speexResample,volume,audioSink);
	    pipe.getBus().connect(this);
	    pipe.getBus().connect(new Bus.ERROR(){
		    public void errorMessage(GstObject source, int errorCode, String error){
			logger.error(error);
		    }
		});
	    pipe.getBus().connect(new Bus.EOS(){
		    public void endOfStream (GstObject source){
			logger.info("EndOfStream: Finished playing: "+fileName);
			listener.playDone(fileName);
		    }
		});
	    
	    pipe.setState (State.NULL);

	    thread = new Thread (this, logPrefix+":PlaypipelineThread");
	    thread.start ();
	    try{
		Thread.sleep(200);
	    }
	    catch(Exception e){
	    }

	}

	public void newDecodedPad (Element element, Pad pad, boolean last){
	    pipe.linkMany(decodeBin, audioConvert);
	}

	protected synchronized boolean isAsyncDone(){
	    return isAsyncDone;
	}

	protected synchronized void setAsyncDone (boolean flag){
	    isAsyncDone = flag;
	}

	public synchronized void asyncDone(GstObject source){
	    if (isSeeking && source.getName().equals("AudioServicePipeline")){
		setAsyncDone(true);
		isSeeking = false;
		notifyAll();
	    }
	}

	public void run() {
	    logger.info("Run: Gstreamer about to enter Gst.main.");
	    Gst.main();
	    logger.error("Run: Gstreamer exited Gst.main.");
	}

	public synchronized boolean play(String fileID) {
	    fileSrc.set("location", fileID);
	    fileName = fileID;
	    
	    if (pipe.setState(State.PLAYING) == StateChangeReturn.FAILURE){
		logger.error("Play: Could not set Gstreamer pipeline to PLAYING state.");
		return false;
	    }
	    else{
		logger.info ("Play: Gstreamer pipeline set to PLAYING state.");
		try {
		    logger.debug("Play: AudioSink Caps: "+audioSink.getStaticPad("sink").getCaps());
		} catch(Exception e) {
		    logger.warn("Play: StaticPad not found" + e.getMessage());
		}
		return true;
	    }
	}
	
	public synchronized boolean pause() {
	    easeVolume(0.0f, 1000);
	    pausedPosition = pipe.queryPosition();
	    
	    if (pipe.setState(State.NULL) == StateChangeReturn.FAILURE){
		logger.error("Pause: Could not set Gstreamer pipeline to PAUSED state.");
		    return false;
	    } else {
		logger.info("Pause: Gstreamer pipeline set to NULL state.");
		return true;
	    }
	}

	public synchronized boolean resume() {
	    logger.debug("resume: " + fileName);
	    pipe.pause();
	    isSeeking = true;
	    while (!pipe.seek(1.0, Format.TIME, SeekFlags.FLUSH|SeekFlags.ACCURATE, SeekType.SET, pausedPosition.toNanos(), SeekType.NONE, -1)){}
	    while (isSeeking){
		try{
		    wait();
		}catch(Exception e){}
	    }
	    logger.debug("Resume: Seeked to "+pausedPosition.toNanos()+":"+pipe.queryPosition().toNanos());
	    if (pipe.setState(State.PLAYING) == StateChangeReturn.FAILURE){
		logger.error("Resume: Could not set Gstreamer pipeline to PLAYING state.");
		return false;
	    } else {
		easeVolume(1.0f, 500);
		logger.info("Resume: Gstreamer pipeline set to PLAYING state.");
		return true;
	    }
	}

	public synchronized boolean stop() {
	    pipe.stop();
	    Gst.quit();
	    return true;
	}

	private void easeVolume (float stopVolume, int duration){
	    final int numIntervals = 20;

	    int sleepTime = duration / numIntervals;
	    float currentVolume = Float.parseFloat(volume.get("volume").toString());
	    if (currentVolume == stopVolume)
		return;
	    float deltaVolume = (stopVolume - currentVolume)/numIntervals;

	    for (int i = 0; i < numIntervals; i++){
		try{
		    Thread.sleep(sleepTime);
		}catch(Exception e){
		}
		volume.set("volume", Float.toString(currentVolume));
		currentVolume += deltaVolume;
	    }
	}

    }
    */

}

