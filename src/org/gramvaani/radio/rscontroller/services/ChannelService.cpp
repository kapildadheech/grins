#include <jni.h>
#include "org_gramvaani_radio_rscontroller_services_ChannelService.h"
#include <jack/jack.h>
#include <iostream>

using namespace std;


class GCString {
private:
  JNIEnv *env;
  jstring javaString;
  const char *charPtr;
  jboolean isCopy;

public:

  GCString (JNIEnv *env, jstring javaString){
    this->env = env;
    this->javaString = javaString;
    charPtr = env->GetStringUTFChars(javaString, &isCopy);
  }
  
  const char *getPtr(){
    return charPtr;
  }

  ~GCString(){
    env->ReleaseStringUTFChars(javaString, charPtr);
  }

};

class ChannelServiceStub {

private:
  JNIEnv *env;
  jobject self;
  jclass cls;

  jack_client_t *jackClient;
  const char *jackClientName;

  static ChannelServiceStub *stub;


  ChannelServiceStub (JNIEnv *env, jobject self){
    setContext(env, self);
  }

  void setContext (JNIEnv *env, jobject self){
    this->env  = env;
    this->self = self;
  }

  int getIntField (const char *fieldName){
    jfieldID fieldID = getFieldID (fieldName, "I");
    return this->env->GetIntField (this->self, fieldID);
  }

  jfieldID getFieldID (const char *fieldName, const char *type){
    return this->env->GetFieldID (this->cls, fieldName, type);
  }
  
  const char *getPersistentString(jstring string){
    jboolean isCopy;
    return this->env->GetStringUTFChars(string, &isCopy);
  }

  jobjectArray packStringArray (const char **strings){
    jobjectArray ret;
    int count = 0;
    if(strings != NULL)
	for ( ; strings[count] != NULL; count++);
    ret = (jobjectArray)env->NewObjectArray(count,
					     env->FindClass("java/lang/String"),
					     env->NewStringUTF(""));
    for (int i = 0; i < count; i++){
      env->SetObjectArrayElement(ret,
				  i, env->NewStringUTF(strings[i]));
    }

    return ret;
  }
  
  
public:

  static ChannelServiceStub *getChannelServiceStub(JNIEnv *env, jobject self){

    ChannelServiceStub *stub = ChannelServiceStub::stub;
    if (stub != NULL){
      stub->setContext (env, self);
      return stub;
    }
    else{
      return ((ChannelServiceStub::stub = new ChannelServiceStub(env, self)));
    }
  }

  jint openJackClient (jstring clientName){
    jackClientName = getPersistentString (clientName);

    if ((jackClient = jack_client_new(jackClientName)) == NULL){
      cerr<<"Unable to connect to jack"<<endl;
      return -1;
    }
    else {
      cout<<"Connected to jackd"<<endl;
      return 0;
    }

  }

  jint connectPorts (jstring inPort, jstring outPort){
    GCString inputPort  (env, inPort);
    GCString outputPort (env, outPort);

    if (jack_connect (jackClient, inputPort.getPtr(), outputPort.getPtr()) ==0){
      cout<<"Connected ports."<<endl;
      return 0;
    }
    else {
      cerr<<"Unable to connect ports: "<<endl;
      return -1;
    }
  }

  jint disconnectPorts (jstring inPort, jstring outPort){
    GCString inputPort  (env, inPort);
    GCString outputPort (env, outPort);
    
    jack_port_t *inPort_t = jack_port_by_name (jackClient, inputPort.getPtr());
    
    if (jack_port_connected_to (inPort_t, outputPort.getPtr())){
      if (jack_disconnect (jackClient, inputPort.getPtr(), outputPort.getPtr())==0){
	cout<<"Disconnected ports."<<endl;
	return 0;
      }
      else {
	cerr<<"Disconnect failed."<<endl;
	return -1;
      }
    }
    else {
      cout <<"Ports not connected."<<endl;
      return 0;
    }

  }

  jobjectArray listInputPorts (){
    const char **ports = NULL;
    
    if ((ports = jack_get_ports (jackClient, NULL, NULL, JackPortIsOutput)) == NULL){
      cerr << "Couldn't list input ports" << endl;
    }

    return packStringArray (ports);
  }

  jobjectArray listOutputPorts (){
    const char **ports = NULL;
    
    if ((ports = jack_get_ports (jackClient, NULL, NULL, JackPortIsInput)) == NULL){
      cerr << "Couldn't list output ports" << endl;
    }
    
    return packStringArray (ports);
  }

};

ChannelServiceStub *ChannelServiceStub::stub;

JNIEXPORT void JNICALL Java_org_gramvaani_radio_rscontroller_services_ChannelService_initJackJNI
(JNIEnv *env, jobject self){

  ChannelServiceStub *stub= ChannelServiceStub::getChannelServiceStub(env, self);

}

JNIEXPORT jint JNICALL Java_org_gramvaani_radio_rscontroller_services_ChannelService_openJackClient (JNIEnv * env, jobject self, jstring jackClientName){

  ChannelServiceStub *stub = ChannelServiceStub::getChannelServiceStub(env, self);
  return stub->openJackClient(jackClientName);

}

JNIEXPORT jint JNICALL Java_org_gramvaani_radio_rscontroller_services_ChannelService_connectPorts
(JNIEnv *env, jobject self, jstring inPort, jstring outPort){

  ChannelServiceStub *stub = ChannelServiceStub::getChannelServiceStub(env, self);
  return stub->connectPorts(inPort, outPort);
}

JNIEXPORT jint JNICALL Java_org_gramvaani_radio_rscontroller_services_ChannelService_disconnectPorts
(JNIEnv *env, jobject self, jstring inPort, jstring outPort){
  ChannelServiceStub *stub = ChannelServiceStub::getChannelServiceStub(env, self);
  return stub->disconnectPorts(inPort, outPort);
}

JNIEXPORT jobjectArray JNICALL Java_org_gramvaani_radio_rscontroller_services_ChannelService_listInputPorts
(JNIEnv *env, jobject self){
  ChannelServiceStub *stub = ChannelServiceStub::getChannelServiceStub(env, self);
  return stub->listInputPorts();
}

JNIEXPORT jobjectArray JNICALL Java_org_gramvaani_radio_rscontroller_services_ChannelService_listOutputPorts
(JNIEnv *env, jobject self){
  ChannelServiceStub *stub = ChannelServiceStub::getChannelServiceStub(env, self);
  return stub->listOutputPorts();
}

