package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.app.providers.RSProvider;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.libmsgs.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.utilities.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.app.gui.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;

import java.util.*;

public class UIService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, 
				  DiagnosticUtilitiesConnectionCallback, DiagnosticUtilitiesSystemCallback,
				  DefaultUncaughtExceptionCallback {
    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected String machineID;
    protected Hashtable<String,RSProvider> providers;
    protected Hashtable<Class<?>,Object> controllers;
    protected LinkMonitorClient linkMonitor;
    protected LogUtilities logger;
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected DiagnosticUtilities diagUtil;
    protected TimeKeeper timeKeeper;
    protected String registrationID;
    protected String resourceManagerID = "";
    protected ControlPanel controlPanel;
    protected boolean isUIServiceReady = false;
    protected boolean isNewInstance = true;
    protected boolean msgSent = false;
    public UIService(StationConfiguration stationConfiguration, String machineID) {
	this.stationConfiguration = stationConfiguration;
	this.machineID = machineID;
	providers = new Hashtable <String,RSProvider>();
	controllers = new Hashtable<Class<?>, Object>();

        ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	logger = new LogUtilities(RSController.UI_SERVICE);
	logger.info("UIService: Initializing.");
	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "RSApp");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);
	registrationID = GUIDUtils.getGUID();
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	timeKeeper = TimeKeeper.getLocalTimeKeeper(ipcServer, this, this, stationConfiguration);
	linkMonitor = new LinkMonitorClient(this, RSController.UI_SERVICE, stationConfiguration);
    }

    public synchronized void registerService(){
	logger.debug("RegisterService: Registering UI service.");
	linkMonitor.start();

	if (ipcServer.registerIPCNode(RSController.UI_SERVICE, this, registrationID) == -1) {
	    logger.error("Unable to connect to ipcServer.");
	    connectionError();
	}
    }

    protected void sendCleanupMessage() {
	StartCleanupMessage startCleanup = new StartCleanupMessage(RSController.UI_SERVICE, 
								   RSController.LIB_SERVICE, 
								   new String[]{StartCleanupMessage.ENCODED_FILES,
										StartCleanupMessage.DELETED_FILES});
	sendMessage(startCleanup);

	StationNetwork stationNetwork = stationConfiguration.getStationNetwork();
	String[] servletIPs = stationNetwork.getUniqueIPs();
	if (servletIPs.length > 1) { 
	    //this condition ensures it is not a single machine setup
	    for (String servletIP : servletIPs) {
		String servletName = stationNetwork.getAStoreNameOnMachine(servletIP);
		if (!servletName.equals("")) {
		    startCleanup = new StartCleanupMessage(RSController.UI_SERVICE, 
							   RSController.SERVLET + "_" + servletName, 
							   new String[]{StartCleanupMessage.ENCODED_FILES});
		    
		    sendMessage(startCleanup);
		}
	    }
	}
    }

    protected int sendReadyMessage() {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(RSController.UI_SERVICE, RSController.RESOURCE_MANAGER,
								   true);
	//return ipcServer.handleOutgoingMessage(readyMessage);
	
	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isUIServiceReady = true;
		logger.debug("SendReadyMessage: Ready messsage sent. isUIServiceReady=" + isUIServiceReady);
		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Message timeout exception.");
	    return -1;
	}
    }

    public void setControlPanel(ControlPanel controlPanel) {
	this.controlPanel = controlPanel;
    }

    public synchronized IPCServer attemptReconnect(){
	/*
	ipcServer = IPCServerStub.initConnection(
						 stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER), 
						 stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
						 stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY), 
						 this,
						 1,
						 stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
						 registrationID);

	*/
	//if (ipcServer != null){
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    timeKeeper.setIPCServer(ipcServer);
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }

   
    public synchronized void handleUncaughtException(Thread t, Throwable ex){
	logger.error(t.getName());
	logger.debug("HandleUncaughtException: Exiting..");
	//System.exit(-1);
    }

    public synchronized void registerProvider (RSProvider provider){
	logger.debug("RegisterProvider: Registering provider: "+provider);
	providers.put(provider.getName(), provider);
    }

    public synchronized void registerController (Object controller){
	logger.debug("RegisterController: Registering controller: "+controller);
	controllers.put(controller.getClass(), controller);
    }

    @SuppressWarnings("unchecked")
    public synchronized <T> T getController(Class<T> controllerClass){
	return (T) controllers.get(controllerClass);
    }

    public void serviceDown(String service){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	resetUI();
    }

    public void connectionError() {
	logger.error("ConnectionError:");
	enableUI(false);
	if (ipcServer != null)
	    ipcServer.disconnect(RSController.UI_SERVICE);
	diagUtil.serviceConnectionError(this);
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }


    //XXX synchronize??
    public int handleIncomingMessage(IPCMessage message) {

	// read message and send to appropriate provider
	logger.debug("HandleIncomingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());

	if (message instanceof TimeKeeperRequestMessage) {
		TimeKeeperResponseMessage timeKeeperMessage = new TimeKeeperResponseMessage(RSController.UI_SERVICE, message.getSource(), timeKeeper.getLocalTime());
		sendMessage(timeKeeperMessage);
		return 0;
	} else if (message instanceof HeartbeatMessage){
	    HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
	    linkMonitor.handleMessage(heartbeatMessage);
	    return 0;
	} else if (message instanceof TemperatureAlertMessage){
	    TemperatureAlertMessage tempAlertMessage = (TemperatureAlertMessage)message;
	    handleTemperatureAlertMessage(tempAlertMessage.getSource(), tempAlertMessage.getMachineID(), tempAlertMessage.getAffectedServices());
	} else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());
	} else if (message instanceof MachineStatusRequestMessage){
	    MachineStatusRequestMessage requestMessage = (MachineStatusRequestMessage)message;
	    handleMachineStatusRequestMessage(requestMessage.getWidgetName(), requestMessage.getCommand(), requestMessage.getOptions());
	} else if (message instanceof AudioSessionsInfoMessage) {
	    AudioSessionsInfoMessage infoMessage = (AudioSessionsInfoMessage)message;
	    handleAudioSessionsInfoMessage(infoMessage);
	} else if (message instanceof TelephonyInfoMessage) {
	    TelephonyInfoMessage infoMessage = (TelephonyInfoMessage)message;
	    handleTelephonyInfoMessage(infoMessage);
	} else if (message instanceof InvalidateCacheKeyMessage) {
	    RSProvider provider = providers.get(RSApp.CACHE_PROVIDER);
	    provider.receiveMessage(message);
	} else if (message.getParam("widgetlist") != null || message.getParam("widgetname") != null) {
	    String list = message.getParam("widgetlist");
	    String[] providerList = null;
	    if (list == null) {
		String providerName = message.getParam("widgetname");
		if (providerName != null)		    
		    providerList = new String[]{providerName};
	    } else {
		providerList = StringUtilities.getArrayFromCSV(list);
	    }
	    if (providerList != null){
		for (String providerName: providerList) {
		    RSProvider provider;
		    if ((provider = providers.get(providerName)) != null){
			if (message instanceof ServiceErrorRelayMessage){
			    provider.receiveErrorMessage(message);
			} else {
			    provider.receiveMessage(message);
			}
		    }
		}
	    }

	} else {
	    
	    if (message instanceof ArchivingDoneResponseMessage) {
		providers.get(RSApp.ARCHIVER_PROVIDER).receiveMessage(message);
	    }
	    
	}
	return 0;
    }

    protected void handleAudioSessionsInfoMessage(AudioSessionsInfoMessage message) {
	String source = message.getSource();
	String[] sessionIDs = message.getSessionIDs();
	String[] sessionStates = message.getSessionStates();

	logger.debug("HandleAudioSessionsInfoMessage: sessionIDs=" + sessionIDs.length + " sessionStates=" + sessionStates.length);
	RSProvider provider;
	if (source.equals(getProviderServiceInstance(RSApp.PLAYOUT_PROVIDER, AudioService.PLAYOUT))) {
	    provider = providers.get(RSApp.PLAYOUT_PROVIDER);
	} else {
	    provider = providers.get(RSApp.PREVIEW_PROVIDER);
	}
	
	if (provider != null)
	    provider.receiveMessage(message);
    }

    protected void handleTelephonyInfoMessage(TelephonyInfoMessage message) {
	String source = message.getSource();
	
	logger.debug("HandleTelephonyInfoMessage: source:" + source);

	RSProvider provider = providers.get(RSApp.TELEPHONY_PROVIDER);

	if (provider != null)
	    provider.receiveMessage(message);
    }

    protected void handleRegistrationAckMessage(boolean ack, String id){
	boolean sendMessage = false;
	String oldID = resourceManagerID;
	resourceManagerID = id;
	if (ack) {
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);
	    if (oldID.equals("")){
		sendMessage = true;
	    } else if (!oldID.equals(id)) {
		logger.error("HandleRegistrationAckMessage: RSController went down.");
		resetUI();
		sendMessage = true;
	    } else if (!isUIServiceReady) {
		sendMessage = true;
	    }
	    if (sendMessage) {
		if (sendReadyMessage() == -1) {
		    connectionError();
		    return;
		}
	    }
	    if (isNewInstance) {
		isNewInstance = false;
		sendCleanupMessage();
	    }
	    enableUI(true);
	} else {
	    resetUI();
	    resetService();
	}
    }

    protected void resetUI() {
	
	ServiceErrorRelayMessage message = new ServiceErrorRelayMessage(RSController.UI_SERVICE, 
									RSController.UI_SERVICE, 
									RSController.RESOURCE_MANAGER,
									"", "", "", "");
	for (RSProvider provider : providers.values()){
	    provider.receiveErrorMessage(message);
	}
    }

    protected void enableUI(boolean enable){
	ServiceNotifyMessage message;
	if (enable) {
	    message = new ServiceNotifyMessage(RSController.UI_SERVICE,
					       RSController.UI_SERVICE, 
					       "",
					       new String[] {RSController.RESOURCE_MANAGER}, 
					       new String[] {resourceManagerID},
					       new String[] {});
	} else {
	    message = new ServiceNotifyMessage(RSController.UI_SERVICE,
					       RSController.UI_SERVICE, 
					       "",
					       new String[] {}, 
					       new String[] {},
					       new String[] {RSController.RESOURCE_MANAGER});
	}
	synchronized(this){
	    for (RSProvider provider: providers.values())
		provider.receiveMessage(message);
	}
    }

    protected void resetService(){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	RegisterMessage registerMessage = new RegisterMessage(RSController.UI_SERVICE, registrationID);
	if (ipcServer.handleOutgoingMessage(registerMessage) == -1) {
	    logger.error("Unable to connect to ipcServer.");
	    connectionError();
	}else{
	   
	}
    }

    protected void handleMachineStatusRequestMessage(String widget, String command, String[] options){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = ((StationNetwork)stationConfiguration.getStationNetwork()).getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), RSController.UI_SERVICE, widget, command, new String[] {dirName, used, available});
	    
	    handleIncomingMessage(responseMessage);
	}
    }

    public void restartMachines() {
	diagUtil.restartMachines(this);
    }

    public void shutdownMachines() {
	diagUtil.shutdownMachines(this);
    }

    public void restartDone(String[] successfulMachines, String[] failedMachines) {
	String message = "Restart Completed.";
	if (successfulMachines.length != 0) {
	    message += " Machines successfully restarted: ";
	    for (String machine : successfulMachines) 
		message += (machine + " ");
	}

	if (failedMachines.length != 0) {
	    message += " Machines failed: ";
	    for (String machine : failedMachines) 
		message += (machine + " ");
	}

	logger.info(message);
    }

    public void shutdownDone(String[] successfulMachines, String[] failedMachines) {
	String message = "Shutdown Completed.";

	if (successfulMachines.length != 0) {
	    message += " Machines successfully shutdown: ";
	    for (String machine : successfulMachines) 
		message += (machine + " ");
	}

	if (failedMachines.length != 0) {
	    message += " Machines failed: ";
	    for (String machine : failedMachines) 
		message += (machine + " ");
	}

	logger.info(message);
    }



    protected void handleTemperatureAlertMessage(String source, String machineID, String[] affectedServices){
	String machineIP = stationConfiguration.getMachineIP(machineID);
	logger.error("HandleTemperatureAlertMessage: High temperature detected at "+machineID+"("+machineIP+")");
	logger.error("HandleTemperatureAlertMessage: Affected services:");
	for (String service: affectedServices){
	    logger.error("HandleTemperatureAlertMessage: "+service);
	}

	//Report to the UI about the error. 
    }

    public int sendMessage(IPCMessage message) {

	if (ipcServer == null)
	    return -1;

	int retVal = ipcServer.handleOutgoingMessage(message);
	if (retVal == -1) {
	    connectionError();
	    logger.error("SendMessage: Unable to send message to: " + message.getDest() + " Type: " + message.getMessageClass());
	    return -1;
	} else {
	    logger.info("SendMessage: Message sent to: " + message.getDest() + " Type: " + message.getMessageClass());
	}

	return 0;
    }

    public int sendCommand(String source, String dest, String[][] args) {
	IPCMessage message = new IPCMessage();
	message.setSource(RSController.UI_SERVICE);
	message.setDest(dest);
	message.setMessageType(IPCMessage.SERVICE_COMMAND);
	message.addParam("widget",source);

	for (String[] arg: args)
	    message.addParam(arg[0], arg[1]);

	
	if (ipcServer == null){
	    logger.error("SendCommand: Cant send message to: "+message.getDest()+" as ipcServer is null.");
	    return -1;
	}
	    
	if (ipcServer.handleOutgoingMessage(message) == -1) {
	    connectionError();
	    logger.error("SendCommand: Unable to send message to: "+dest + " Provider: " + source);
	    return -1;
	} else {
	    logger.info("SendCommand: Message sent to: " + dest + " Provider: " + source);
	    return 0;
	}
    }

    public int sendCommand(IPCMessage message){
	
	message.setSource(RSController.UI_SERVICE);
	if (message.getDest().equals(RSController.UI_SERVICE)) {
	    handleIncomingMessage(message);
	    return 0;
	}

	if (ipcServer == null){
	    logger.error("SendCommand: Cant send message to: "+message.getDest()+" as ipcServer is null.");
	    return -1;
	}
	if (ipcServer.handleOutgoingMessage(message) == -1) {
	    connectionError();
	    logger.error("SendCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	    return -1;
	} else {
	    logger.info("SendCommand: Message sent to: " + message.getDest() + " Type: " + message.getMessageClass());
	    return 0;
	}
    }

    public IPCSyncResponseMessage sendSyncCommand(String source, String dest, String[][] args) throws IPCSyncMessageTimeoutException {
	IPCSyncMessage message = new IPCSyncMessage();
	message.setSource(RSController.UI_SERVICE);
	message.setDest(dest);
	message.setMessageType(IPCMessage.SERVICE_COMMAND);
	message.addParam("widget",source);

	for (String[] arg: args)
	    message.addParam(arg[0], arg[1]);

	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null)
	    return null;

	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
	    connectionError();
	    logger.error("SendCommand: Unable to send message to: "+dest + " Provider: " + source);
	} else {
	    logger.info("SendCommand: Message sent to: " + dest + " Provider: " + source);
	}

	return responseMessage;
    }

    public IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message, long timeout) throws IPCSyncMessageTimeoutException {
	message.setSource(RSController.UI_SERVICE);
	IPCSyncResponseMessage responseMessage = null;
	if (ipcServer == null)
	    return null;

	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message, timeout)) == null) {
	    connectionError();
	    logger.error("SendCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	}
	return responseMessage;
    }

    public IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	return sendSyncCommand(message, -1);
    }
    
    public String getName() {
	return RSController.UI_SERVICE;
    }

    public void linkUp(){

    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage: " + message.getHeartbeatSequence());

	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }

    public String getLanguage() {
	return stationConfiguration.getStringParam(StationConfiguration.DEFAULT_LANGUAGE);
    }

    public String getProviderServiceInstance(String providerName, String providerRole) {
	return stationConfiguration.getProviderServiceInstance(providerName, providerRole);
    }

    public StationConfiguration getStationConfiguration() {
	return stationConfiguration;
    }

    public TimeKeeper getTimeKeeper() {
	return timeKeeper;
    }

    public LogUtilities getLogger() {
	return logger;
    }

    public IPCServer getIPCServer() {
	return ipcServer;
    }

}
