package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.telephonylib.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.app.*;

import gnu.getopt.Getopt;
import java.util.*;
import java.io.File;
import java.lang.reflect.Constructor;

public class SMSService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, DefaultUncaughtExceptionCallback, 
				   DiagnosticUtilitiesConnectionCallback, MediaLibListener, SMSLibListener 
{

    public final static int CONFIGURING = 0;
    public final static int READY       = 1;

    public static final String SMS_STR  = "SMS";
    final static String WIDGET = RSApp.SMS_PROVIDER;

    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected LogUtilities logger;
    protected String registrationID;
    protected String machineID;
    protected String serviceInstanceName;
    protected DiagnosticUtilities diagUtil;
    protected TimeKeeper timeKeeper;
    protected LinkMonitorClient linkMonitor = null;
    protected boolean isSMSServiceReady = false;
    protected String resourceManagerID = "";
    protected String uiServiceID = "";
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected MediaLib medialib;
    protected int state = CONFIGURING;
    protected Hashtable <String, SMSLib> smsLibs; 
    
    public SMSService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.machineID = machineID;
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;

	ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));

	logger = new LogUtilities(RSController.SMS_SERVICE);
	logger.info("SMSService: Initializing");
	registrationID = GUIDUtils.getGUID();

	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);

	linkMonitor = new LinkMonitorClient(this, RSController.SMS_SERVICE, stationConfiguration);
	linkMonitor.start();

	MediaLib.setIPCServer(ipcServer);
	MediaLib.registerListener(RSController.SMS_SERVICE, this);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null)
	    medialib.setLibraryActivated(true);

	initSMSLibs();
	ipcServer.registerIPCNode(RSController.SMS_SERVICE, this, registrationID);
    }

    void initSMSLibs() {
	smsLibs = new Hashtable<String, SMSLib>();
	for(String lineID : stationConfiguration.getAllSMSLineIDs()) {
	    SMSLine line = stationConfiguration.getSMSLine(lineID);
	    String lineType = line.getLineType();
	    try {
		Class<?> smsLibClass = Class.forName("org.gramvaani.radio.telephonylib." + lineType + "SMSLib");
		Constructor<?> smsLibConstructor = smsLibClass.getConstructor(String.class, String.class, StationConfiguration.class, SMSLibListener.class);
		SMSLib smsLib = (SMSLib)smsLibConstructor.newInstance(new Object[]{lineID, line.getParams(), stationConfiguration, this});
		smsLibs.put(lineID, smsLib);
	    } catch(Exception e) {
		logger.error("Could not find SMSLib for line id: " + lineID + " type: " + lineType, e);
	    }
	}
    }

    protected synchronized void setState(int newState) {
	logger.info("SetState: " + state + " => " + newState);
	state = newState;
    }

    public synchronized void connectionError() {
	logger.error("ConnectionError: ");
	timeKeeper.setIPCServer(null);
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);
    }

    protected IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) {
	IPCSyncResponseMessage responseMessage = null;
	if (ipcServer == null){
	    return null;
	}
	try {
	    if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
		logger.error("SendSyncMessage: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	    }
	} catch (IPCSyncMessageTimeoutException e) {
	    logger.error("SendSyncMessage: Sync message timed out.");
	}
	return responseMessage;
    }

    protected int sendMessage(IPCMessage ipcMessage) {
	if (ipcServer == null)
	    return -1;

	int retVal = ipcServer.handleOutgoingMessage(ipcMessage);
	if (retVal == -1) {
	    logger.error("SendMessage: Unable to send message " + ipcMessage.getClass());
	    connectionError();
	} else if (retVal == -2) {
	    logger.error("SendMessage: Unable to send message " + ipcMessage.getClass());
	}
	return retVal;
    }

    protected int sendReadyMessage(boolean readiness) {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(serviceInstanceName, RSController.RESOURCE_MANAGER, readiness);

	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isSMSServiceReady = readiness;
		logger.info("SendReadyMessage: Ready messsage sent. isServiceReady: " + isSMSServiceReady);
		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Caught exception.", e);
	    return -1;
	}
    }

    protected void uiServiceConnected(String newID) {
    }

    void handleSMSCommandMessage(SMSCommandMessage command) {
	logger.info("HandleSMSCommandMessage: " + command.getCommand());
	Hashtable<String, String> options = command.getOptions();
	
	boolean retval = false;
	if(command.getCommand().equals(SMSCommandMessage.SEND)) {
	    retval = sendSMS(options);
	} else if(command.getCommand().equals(SMSCommandMessage.CANCEL_SEND)) {
	    retval = cancelSMS(options);
	}

	sendAckMessage(command.getCommand(), command.getOptions(), retval);
    }

    boolean sendSMS(Hashtable<String, String> options) {
	String smsLine = options.get(SMSCommandMessage.SMSLINE);
	String message = options.get(SMSCommandMessage.MESSAGE);
	String phoneNoStr = options.get(SMSCommandMessage.PHONE_NOS);
	String[] phoneNos = StringUtilities.getArrayFromCSV(phoneNoStr);

	SMSLib smsLib = smsLibs.get(smsLine);
	if(smsLib == null || !smsLib.isAvailable()) {
	    logger.error("SMSLib for the line " + smsLine + " is not available.");
	    addSMSToDatabase(smsLine, message, phoneNos, SMS.OUTGOING, SMS.FAILED);
	    return false;
	}

	SMS[] smses = addSMSToDatabase(smsLine, message, phoneNos, SMS.OUTGOING, SMS.PENDING);
	boolean sent = smsLib.sendSMS(smses);
	if(!sent) {
	    updateSMSStatus(smses, SMS.PENDING, SMS.FAILED);
	}

	return sent;
    }

    SMS[] addSMSToDatabase(String smsLine, String message, String[] phoneNos, int type, int status) {
	message = StringUtilities.sanitize(message);
	message = StringUtilities.trim(message);
	ArrayList<SMS> smses = new ArrayList<SMS>();
	Long dateTime = System.currentTimeMillis();
	for(String phoneNo : phoneNos) {
	    SMS sms = new SMS(smsLine, phoneNo, message, dateTime, type, status);
	    smses.add(sms);
	    medialib.insert(sms, false);
	}
	
	return smses.toArray(new SMS[smses.size()]);
    }

    void updateSMSStatus(int[] smsIDs, int prevStatus, int newStatus) {
	SMS[] smses = new SMS[smsIDs.length];

	for(int i=0; i<smsIDs.length; i++)
	    smses[i] = new SMS(smsIDs[i]);

	updateSMSStatus(smses, prevStatus, newStatus);
    }

    void updateSMSStatus(SMS[] smses, int prevStatus, int newStatus) {
	for(SMS sms: smses) {
	    sms.setStatus(prevStatus);
	    SMS oldSMS = medialib.getSingle(sms);

	    if(oldSMS != null) {
		SMS newSMS = oldSMS.cloneObject();
		newSMS.setStatus(newStatus);
		newSMS.setDateTime(System.currentTimeMillis());
		medialib.update(oldSMS, newSMS, false);
	    } else {
		logger.error("UpdateSMSStatus: Could not find SMS in the database.: " + sms);
	    } 
	}
    }

    boolean cancelSMS(Hashtable<String, String> options) {
	String smsIDsCSV = options.get(SMSCommandMessage.SMS_IDS);
	String[] smsIDs = StringUtilities.getArrayFromCSV(smsIDsCSV);
	int[] smsIDsInt = new int[smsIDs.length];

	Hashtable<String, ArrayList<Integer>> lineToSMSMap = new Hashtable<String, ArrayList<Integer>>();

	for(int i=0; i<smsIDs.length; i++) {
	    smsIDsInt[i] = (new Integer(smsIDs[i])).intValue();

	    SMS sms = new SMS(smsIDsInt[i]);
	    SMS fetchedSMS = medialib.getSingle(sms);
	    String smsLine = fetchedSMS.getSMSLine();

	    ArrayList<Integer> lineSMSes = lineToSMSMap.get(smsLine);
	    if(lineSMSes == null) {
		lineSMSes = new ArrayList<Integer>();
		lineToSMSMap.put(smsLine, lineSMSes);
	    }
	    
	    lineSMSes.add(smsIDsInt[i]);
	}
	
	for(String smsLine: lineToSMSMap.keySet()) {
	    SMSLib smsLib = smsLibs.get(smsLine);
	    ArrayList<Integer> lineSMSes = lineToSMSMap.get(smsLine);
	    if(smsLib != null) {
		smsLib.cancelSMS(integerArrayListToIntArray(lineSMSes));
	    }
	}

	updateSMSStatus(smsIDsInt, SMS.PENDING, SMS.CANCELLED);
	return true;
    }

    int[] integerArrayListToIntArray(ArrayList<Integer> inList) {
	int[] outArr = new int[inList.size()];
	int i=0;
	for(Iterator<Integer> iter=inList.iterator(); iter.hasNext();) {
	    outArr[i++] = (iter.next()).intValue();
	}
	
	return outArr;
    }

    void sendAckMessage(String command, Hashtable<String,String> options, boolean retval) {
	String ack;
	if(retval)
	    ack = SMSAckMessage.TRUE;
	else
	    ack = SMSAckMessage.FALSE;

	SMSAckMessage ackMessage = new SMSAckMessage(serviceInstanceName, RSController.UI_SERVICE,
						     WIDGET, options, ack, command, RSController.ERROR_UNKNOWN);

	sendMessage(ackMessage);
    }

    void disableSMS() {
    }

    protected boolean checkStatus() {
	return (checkMedialibStatus() && checkSMSLibStatus());
    }

    protected boolean checkSMSLibStatus() {
	//do getAvailableSMSLibs and see if any libs are available. if atleast one is available return true else return false.
	return true;
    }

    protected String[] getAvailableSMSLibs() {
	return (new ArrayList<String>()).toArray(new String[]{});
    }

    protected String[] getUnavailableSMSLibs() {
	return (new ArrayList<String>()).toArray(new String[]{});
    }

    protected boolean checkMedialibStatus() {
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib == null)
	    return false;

	medialib.setLibraryActivated(true);
	return true;
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }

    void handleStatusCheckMessage(String source, String widgetName) {
	String response;

	if (checkStatus())
	    response = StatusCheckResponseMessage.TRUE;
	else
	    response = StatusCheckResponseMessage.FALSE;

	StatusCheckResponseMessage responseMessage = new StatusCheckResponseMessage(serviceInstanceName, source, widgetName, "", response);
	
	Hashtable<String, String> options = new Hashtable<String, String>();
	options.put(SMSEventMessage.SMSLIB_OK, StringUtilities.getCSVFromArray(getAvailableSMSLibs()));
	options.put(SMSEventMessage.SMSLIB_ERROR, StringUtilities.getCSVFromArray(getUnavailableSMSLibs()));
	responseMessage.setOptions(options);
	//System.err.println("opts: "+options);
	sendMessage(responseMessage);
    }

    protected synchronized void handleRegistrationAckMessage(boolean ack, String id){
	boolean sendMessages = false;
	String oldID = resourceManagerID;
	resourceManagerID = id;

	if (ack) {
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);

	    if (!checkMedialibStatus()) {
		logger.error("HandleRegistrationAckMessage: Medialib status check failed.");
		StatusCheckRequestMessage msg = new StatusCheckRequestMessage(serviceInstanceName, RSController.LIB_SERVICE, "", "1");
		sendMessage(msg);
		return;
	    }

	    if ((oldID.equals("")) || (!resourceManagerID.equals(oldID))) {
		sendMessages = true;
	    } else if (!isSMSServiceReady) {
		if (sendReadyMessage(true) == -1)
		    connectionError();
	    }

	    if (sendMessages) {
		ServiceInterestMessage serviceInterestMessage = new ServiceInterestMessage(serviceInstanceName, RSController.RESOURCE_MANAGER,
											   "", new String[]{RSController.LIB_SERVICE});
		sendMessage(serviceInterestMessage);
	    }

	} else {
	    resetService();
	}
    }

    protected void resetService(){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	isSMSServiceReady = false;
	disableSMS();

	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	sendMessage(message);
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: From: "+message.getSource()+" Type: "+message.getMessageType());

	if (message instanceof TimeKeeperResponseMessage) {
	    timeKeeper.handleTimeKeeperResponseMessage((TimeKeeperResponseMessage)message);
	} else if (message instanceof ServiceErrorRelayMessage) {
	    ServiceErrorRelayMessage errorRelayMessage = (ServiceErrorRelayMessage)message;
	    if (errorRelayMessage.getServiceName().equals(RSController.LIB_SERVICE)){
		logger.debug("HandleIncomingMessage: ServiceErrorRelayMessage received for "+RSController.LIB_SERVICE);
		if (isSMSServiceReady && !checkStatus()) {
		    disableSMS();
		    setState(CONFIGURING);
		    sendReadyMessage(false);
		}
	    } 
	} else if (message instanceof ServiceActivateNotifyMessage) {
	    ServiceActivateNotifyMessage notifyMessage = (ServiceActivateNotifyMessage)message;
	    if (notifyMessage.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		timeKeeper.sync();
		uiServiceConnected(notifyMessage.getRegistrationID());
	    }
	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    String[] services = notifyMessage.getActivatedServices();
	    String[] ids = notifyMessage.getActivatedIDs();

	    for (int i=0; i<services.length; i++){
		String service = services[i];
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (!isSMSServiceReady && checkStatus()) {
			setState(READY);
			sendReadyMessage(true);
		    }
		} if (service.equals(RSController.UI_SERVICE)){
		    uiServiceConnected(ids[i]);
		}
	    } 

	    services = notifyMessage.getInactivatedServices();
	    for (String service : services){
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (isSMSServiceReady && !checkStatus()) {
			disableSMS();
			setState(CONFIGURING);
			sendReadyMessage(false);
		    }
		} 
	    } 

	} else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());
	} else if (message instanceof MachineStatusRequestMessage){
	    // do nothing
	} else if (message instanceof SMSCommandMessage) {
	    handleSMSCommandMessage((SMSCommandMessage)message);
	} else if (message instanceof ServiceAckMessage) {
	    ServiceAckMessage ackMessage = (ServiceAckMessage) message;
	    for (String service: ackMessage.getActiveServices()) {
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (!isSMSServiceReady && checkStatus()) {
			setState(READY);			
			sendReadyMessage(true);
		    }
		} 
	    }
	    
	    for (String service: ackMessage.getInactiveServices()) {
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (isSMSServiceReady && !checkStatus()){
			disableSMS();
			setState(CONFIGURING);
			sendReadyMessage(false);
		    }
		} 
	    }
	    
	} else if (message instanceof StatusCheckRequestMessage) {
	    StatusCheckRequestMessage requestMessage = (StatusCheckRequestMessage) message;
	    handleStatusCheckMessage(requestMessage.getSource(), requestMessage.getWidgetName());
	} 

	return 0;
    }

    public synchronized void smsSent(int smsID, boolean ack) {
	if(ack)
	    updateSMSStatus(new int[]{smsID}, SMS.PENDING, SMS.SENT);
	else
	    updateSMSStatus(new int[]{smsID}, SMS.PENDING, SMS.FAILED);
    }
    
    public synchronized void newSMS(String lineID, String message, String phoneNo) {
	SMSLine line = stationConfiguration.getSMSLine(lineID);
	//Works because Topex always reports the country code in addition to the mobile number.
	//This work around will have to change if Topex changes its behavior.
	if (line.getLineType().equals(SMSLine.TOPEX_LINE_TYPE)) {
	    if(phoneNo.startsWith(stationConfiguration.getStringParam(StationConfiguration.LOCAL_COUNTRY_CODE)))
		phoneNo = "+" + phoneNo;
	}

	if(isValidVote(message)) {
	    logger.info("GRINS_USAGE:VOTE_RECEIVED:" + phoneNo + ":" + message);
	    addVoteToDatabase(lineID, message, phoneNo);
	} else {
	    logger.info("GRINS_USAGE:SMS_RECEIVED:" + phoneNo + ":" + message);
	    addSMSToDatabase(lineID, message, new String[]{phoneNo}, SMS.INCOMING, SMS.RECEIVED);
	}
    }

    void addVoteToDatabase(String lineID, String message, String phoneNo) {
	message = StringUtilities.sanitize(message);
	message = StringUtilities.trim(message);
	String[] messageArr = message.split(" ");
	String keyword = messageArr[0];
	String optionCode = messageArr[1];

	Poll poll = getPollFromKeyword(keyword);
	PollOption option = getPollOption(poll, optionCode);
	int optionID;
	if(option == null)
	    optionID = -1;
	else
	    optionID = option.getID();

	Vote vote = new Vote(lineID, phoneNo, System.currentTimeMillis(), message, poll.getID(), optionID, optionCode);
	medialib.insert(vote, false);
    }

    String[] getAllPollKeywords() {
	Object[] keywordObjs = medialib.unique(Poll.getDummyObject(-1), Poll.getKeywordFieldName());

	ArrayList<String> keywordList = new ArrayList<String>();
	for (Object keyword: keywordObjs)
	    keywordList.add((String)keyword);

	return keywordList.toArray(new String[keywordList.size()]);
    }

    Poll[] getActivePolls() {
	Poll dummyPoll = Poll.getDummyObject(-1);
	dummyPoll.setState(Poll.RUNNING);
       
	return medialib.get(dummyPoll);
    }

    Poll getPollFromKeyword(String keyword) {
	Poll[] activePolls = getActivePolls();

	for(Poll poll :activePolls)
	    if(keyword.equalsIgnoreCase(poll.getKeyword()))
		return poll;

	return null;
    }

    PollOption[] getPollOptions(Poll poll) {
	PollOption dummyOption = PollOption.getDummyObject(-1);
	dummyOption.setPollID(poll.getID());
	
	return medialib.get(dummyOption);
    }

    PollOption getPollOption(Poll poll, String optionCode) {
	for(PollOption option :getPollOptions(poll))
	    if(optionCode.equalsIgnoreCase(option.getPollOptionCode()))
		return option;

	return null;
    }

    boolean isValidVote(String message) {
	message = StringUtilities.sanitize(message);
	message = StringUtilities.trim(message);

	String[] messageArr = message.split(" ");
	if(messageArr.length < 2)
	    return false;
	
	String keyword = messageArr[0];
	String optionCode = messageArr[1];

	Poll poll = getPollFromKeyword(keyword);
	if(poll == null)
	    return false;

	if(poll.getPollType() == Poll.OPEN_OPTIONS)
	    return true;

	PollOption option = getPollOption(poll, optionCode);
	if(option != null)
	    return true;
	else 
	    return false;
    }

    public String getName() {
	return serviceInstanceName;
    }    

    public synchronized void handleUncaughtException(Thread t, Throwable ex){
	logger.debug("HandleUncaughtException:");
    }

    public void linkUp(){
	logger.debug("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");

	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }

    public void serviceDown(String service){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	isSMSServiceReady = false;
	disableSMS();
    }

    public synchronized IPCServer attemptReconnect(){
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    timeKeeper.setIPCServer(ipcServer);
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }

    public synchronized void activateMediaLib(boolean activate) {
	if (activate){
	} else {
	    if (isSMSServiceReady) {
		logger.error("ActivateMediaLib: Database connection error.");
		setState(CONFIGURING);
		disableSMS();
		StatusCheckRequestMessage msg = new StatusCheckRequestMessage(serviceInstanceName, RSController.LIB_SERVICE, "", "1");
		sendMessage(msg);
		sendReadyMessage(false);
	    }
	}
    }

    public static void main(String args[]) {
	Getopt g = new Getopt("SMSService", args, "c:m:");
	int c;
	String configFilePath = "./automation.conf";
	String machineID = "127.0.0.1";
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c':
		configFilePath = g.getOptarg();
		break;
	    case 'm':
		machineID = g.getOptarg();
		break;
	    }
	}

	StationConfiguration stationConfiguration = new StationConfiguration(machineID);
	stationConfiguration.readConfig(args[0]);
	String instanceName = stationConfiguration.getServiceInstanceOnMachine(RSController.SMS_SERVICE, machineID);
	SMSService smsService = new SMSService(stationConfiguration, instanceName, machineID);
    }

}
