/*
package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.utilities.processmonitor.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;

import java.util.*;
import gnu.getopt.Getopt;

public class ChannelService implements IPCNode, IPCNodeCallback,LinkMonitorClientListener, ProcessMonitorListener {

    protected native void initJackJNI();
    protected native int openJackClient (String jackClientName);
    protected native int connectPorts	(String inPort, String outPort);
    protected native int disconnectPorts(String inPort, String outPort);
    protected native String[] listInputPorts();
    protected native String[] listOutputPorts();

    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected String machineID;
    protected String serviceInstanceName;
    protected int channelServiceStubPtr;
    protected LinkMonitorClient linkMonitor = null;
    protected ProcessMonitor jackMonitor;
    protected String registrationID;
    protected DiagnosticUtilities diagUtil;

    //state info
    protected final static int CONFIGURING = 0;
    protected final static int LISTENING   = 1;

    protected int state = CONFIGURING;

    public final static String CONNECT_MSG    = "CONNECT_MSG";
    public final static String DISCONNECT_MSG = "DISCONNECT_MSG";
    public final static String SUCCESS = "SUCCESS";
    public final static String FAILURE = "FAILURE";

    public final static String CHANNEL_SERVICE_JCNAME = "CHANNEL_SERVICE_CLIENT";

    protected final static int CHANNEL_MONO   = 0;
    protected final static int CHANNEL_STEREO = 1;

    protected final static int PORT_INPUT  = 0;
    protected final static int PORT_OUTPUT = 1;

    protected LogUtilities logger;

    static {
	System.loadLibrary("channelservice");
    }

    // called when ChannelManager is local to RS Controller
    public ChannelService(IPCServer ipcServer, StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.ipcServer = ipcServer;
	this.stationConfiguration = stationConfiguration;
	this.machineID = machineID;
	this.serviceInstanceName = serviceInstanceName;
	registrationID = GUIDUtils.getGUID();
	logger = new LogUtilities(serviceInstanceName);
	logger.debug("ChannelService: Entering. Initializing with serviceInstanceName="+serviceInstanceName+" machineID="+machineID);
	logger.info("ChannelService: Initializing.");
	//initializeJack();
	ipcServer.registerIPCNode(serviceInstanceName, this, registrationID);
	if (sendReadyMessage(true) == -1) {
	    logger.fatal("ChannelService: Unable to send Ready message");
	    //System.exit(-1);
	}
	else
	    logger.debug("ChannelService: Ready message sent.");
	logger.debug("ChannelService: Leaving");
    }


    // called when ChannelManager is started as a separate Java process
    public ChannelService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.stationConfiguration = stationConfiguration;
	this.machineID = machineID;
	this.serviceInstanceName = serviceInstanceName;
	registrationID = GUIDUtils.getGUID();
	logger = new LogUtilities(serviceInstanceName);
	logger.info("ChannelService: Initializing.");
	logger.debug("ChannelService: Entering. Initializing with serviceInstanceName="+serviceInstanceName+" machineID="+machineID);
	
        ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER), 
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	if (ipcServer.registerIPCNode(serviceInstanceName, this, registrationID) == -1) {
	    logger.fatal("ChannelService: Unable to connect to ipcServer.");
	    //System.exit(-1);
	}
	else
	    logger.debug("ChannelService: Registered IPC node.");
	linkMonitor = new LinkMonitorClient(this, serviceInstanceName, stationConfiguration);
	linkMonitor.start();

	//initializeJack();

	if (sendReadyMessage(true) == -1) {
	    logger.fatal("ChannelService: Unable to send Ready message");
	    //System.exit(-1);
	}
	else
	    logger.debug("ChannelService: Ready message sent.");
	logger.debug("ChannelService: Leaving");
    }

    
    // read error in connection of IPCServerStub with IPCServerImpl
    public synchronized void connectionError() {
	logger.debug("ConnectionError: Entering");
	logger.error("ConnectionError:");
	setState(CONFIGURING);

	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) != 0) {
	    logger.fatal("ConnectionError: Unable to establish connection to IPCServer.");
	    stopJack();
	    //System.exit(-1);
	}
	else
	    logger.debug("ConnectionError: Connection established with IPCServer.");

	initializeJack();
	logger.debug("ConnectionError: Leaving");
    }

    protected int sendReadyMessage(boolean ready) {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(serviceInstanceName, RSController.RESOURCE_MANAGER,
								   ready);
	return ipcServer.handleOutgoingMessage(readyMessage);
    }
    
    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }


    // method common to both local and remote instantiations of ChannelManager, defined in IPCNode
    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering. From: "+message.getSource()+" Type: "+message.getMessageClass());

	if (message instanceof ChannelCommandMessage) {
	    ChannelCommandMessage commandMessage = (ChannelCommandMessage) message;
	    handleChannelCommandMessage (commandMessage.getSource(),
					 commandMessage.getCommand(),
					 commandMessage.getInPort(),
					 commandMessage.getOutPort());
	} else if (message instanceof HeartbeatMessage) {
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	} else if (message instanceof ServiceErrorRelayMessage) {
	    // XXX ignore for now. UI_SERVICE went down, etc

	} else if (message instanceof MachineStatusRequestMessage){
	    MachineStatusRequestMessage requestMessage = (MachineStatusRequestMessage)message;
	    handleMachineStatusRequestMessage(requestMessage.getWidgetName(), requestMessage.getCommand(), requestMessage.getOptions());
	}

	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }
    
    // read message and make JNI calls to change stream configurations
    protected void handleChannelCommandMessage (String client, String command, String inPort, String outPort){
	logger.info("HandleChannelCommandMessage: Entering. Message text:"+client+"_"+command+"_"+inPort+"_"+outPort);
	switch(state()){
	case CONFIGURING:
	    logger.warn("HandleChannelCommandMessage: Received command while in CONFIGURING state.");
	    if (sendMessage(new ChannelAckMessage(getName(), client, ChannelService.FAILURE, command)) == -1){
		logger.error("HandleChannelCommandMessage: Unable to send NACK.");
		disconnect();
	    }
	    else
		logger.debug("HandleChannelCommandMessage: NACK sent.");
	    break;

	case LISTENING:
	    if (command.equals(ChannelService.CONNECT_MSG)){
		if (connectPortsHelper (inPort, outPort) == 0){
		    logger.info("HandleChannelCommandMessage: Connected jack ports: "+inPort+" "+outPort);
		    if (sendMessage(new ChannelAckMessage(getName(), 
							 client, 
							 ChannelService.SUCCESS, 
							 command)) == -1) {
			logger.error("HandleChannelCommandMessage: Unable to send ACK.");
			stopJack();
			disconnect();
		    }
		    else
			logger.debug("HandleChannelCommandMessage: ACK message sent.");
		}
		else{
		    logger.error("HandleChannelCommandMessage: Failed to connect jack ports: "+inPort+" "+outPort);
		    if (sendMessage(new ChannelAckMessage(getName(), 
							 client, 
							 ChannelService.FAILURE, 
							 command)) == -1) {
			logger.error("HandleChannelCommandMessage: Unable to send NACK.");
			stopJack();
			disconnect();
		    }
		    else
			logger.debug("HandleChannelCommandMessage: NACK message sent.");
		}
	    } else if (command.equals(ChannelService.DISCONNECT_MSG)){
		if (disconnectPortsHelper (inPort, outPort) == 0){
		    logger.info("HandleChannelCommandMessage: Disconnected jack ports: "+inPort+" "+outPort);
		    if (sendMessage(new ChannelAckMessage(getName(), 
							 client, 
							 ChannelService.SUCCESS, 
							 command)) == -1) {
			logger.error("HandleChannelCommandMessage: Unable to send ACK.");
			stopJack();
			disconnect();
		    }
		    else
			logger.debug("HandleChannelCommandMessage: ACK message sent.");
		}
		else{
		    logger.error("HandleChannelCommandMessage: Failed to disconnect jack ports: "+inPort+" "+outPort);
		    if (sendMessage(new ChannelAckMessage(getName(), 
							 client, 
							 ChannelService.FAILURE, 
							 command)) == -1) {
			logger.error("HandleChannelCommandMessage: Unable to send NACK.");
			stopJack();
			disconnect();
		    }
		    else
			logger.debug("HandleChannelCommandMessage: NACK message sent.");
		}
	    }
	}
	logger.debug("HandleChannelCommandMessage: Leaving");
    }


    protected void handleMachineStatusRequestMessage(String widget, String command, String[] options){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = ((StationNetwork)stationConfiguration.getStationNetwork()).getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), RSController.UI_SERVICE, widget, command, new String[] {dirName, used, available});
	    
	    if (sendMessage(responseMessage) == -1){
		logger.error("HandleMachineStatusRequestMessage: Unable to send responseMessage.");
		connectionError();
	    } else {
		logger.debug("HandleMachineStatusRequestMessage: Response sent.");
	    }
	}	
    }

    protected int connectPortsHelper (String inPort, String outPort){
	logger.debug("connectPortsHelper: Entering");

	String[] inChannels  = getChannels (inPort, PORT_INPUT);
	String[] outChannels = getChannels(outPort, PORT_OUTPUT);
	int retVal = 0;
	int channelOneStatus = 0;
	int channelTwoStatus = 0;

	logger.debug("connectPortsHelper: "+inChannels.length+" inChannels and "+outChannels.length+" outChannels found.");
	//XXX Need to figure out the correct policy.
       
	if ((inChannels.length == 2) && (outChannels.length == 2)){
	}
	else if ((inChannels.length == 0) || (outChannels.length == 0)){
	    retVal = -1;
	}
	else if ((inChannels.length == 1) && (outChannels.length == 2)){
	    inChannels = new String[]{inChannels[0],inChannels[0]};
	}
	else if ((inChannels.length == 2) && (outChannels.length == 1)){
	    outChannels = new String[]{outChannels[0],outChannels[0]};
	}
	else if ((inChannels.length == 1) && (outChannels.length == 1)){
	    retVal = connectPorts (inChannels[0], outChannels[0]);
	    logger.debug("connectPortsHelper: Leaving with return value "+retVal);
	    return retVal;
	}
	if (retVal == 0){
	    channelOneStatus = connectPorts (inChannels[0], outChannels[0]);
	    channelTwoStatus = connectPorts (inChannels[1], outChannels[1]);
	    logger.debug("connectPortsHelper: channelOneStatus="+channelOneStatus+" channelTwoStatus="+channelTwoStatus);
	    if (channelOneStatus == 0 && channelTwoStatus == 0)
		retVal = 0;
	    else if (channelOneStatus != 0 && channelTwoStatus != 0){
		retVal = -1;
	    }
	    else if (channelOneStatus == 0){
		disconnectPorts (inChannels[0], outChannels[0]);
		retVal = -1;
	    }
	    else {
		disconnectPorts (inChannels[1], outChannels[1]);
		retVal = -1;
	    }
	}
	logger.debug("connectPortsHelper: Leaving with return value "+retVal);
	return retVal;
    }

    protected int disconnectPortsHelper (String inPort, String outPort){
	logger.debug("DisconnectPortsHelper: Entering");
	String[] inChannels  = getChannels (inPort, PORT_INPUT);
	String[] outChannels = getChannels(outPort, PORT_OUTPUT);
	int retVal=0;
	int channelOneStatus = 0;
	int channelTwoStatus = 0;

	logger.debug("DisconnectPortsHelper: "+inChannels.length+" inChannels and "+outChannels.length+" outChannels found.");
	//XXX Need to figure out the correct policy.

	if ((inChannels.length == 2) && (outChannels.length == 2)){
	    
	}
	else if ((inChannels.length == 0) || (outChannels.length == 0)){
	    retVal = -1;
	}
	else if ((inChannels.length == 1) && (outChannels.length == 2)){
	    inChannels = new String [] {inChannels[0], inChannels[0]};
	}
	else if ((inChannels.length == 2) && (outChannels.length == 1)){
	    outChannels = new String [] {outChannels[0], outChannels[0]};
	}
	else if ((inChannels.length == 1) && (outChannels.length == 1)){
	    retVal = disconnectPorts (inChannels[0], outChannels[0]);
	    logger.debug("DisconnectPortsHelper: Leaving with return value "+retVal);
	    return retVal;
	}

	if (retVal == 0){
	    channelOneStatus = disconnectPorts (inChannels[0], outChannels[0]);
	    channelTwoStatus = disconnectPorts (inChannels[1], outChannels[1]);
	    logger.debug("DisconnectPortsHelper: channelOneStatus="+channelOneStatus+" channelTwoStatus="+channelTwoStatus);
	    if (channelOneStatus == 0 && channelTwoStatus == 0)
		retVal = 0;
	    else if (channelOneStatus == 0){
		disconnectPorts (inChannels[0], outChannels[0]);
		retVal = -1;
	    }
	    else if (channelTwoStatus == 0){
		disconnectPorts (inChannels[1], outChannels[1]);
		retVal = -1;
	    }
	    else
		retVal = -1;
	}
	logger.debug("DisconnectPortsHelper: Leaving with return value "+retVal);
	return retVal;

    }

    protected boolean existsChannel (String portName, int portType){
	String[] ports;
	if (portType == PORT_INPUT)
	    ports = listInputPorts();
	else
	    ports = listOutputPorts();

	for (String port: ports)
	    if (port.equals(portName))
		return true;
	
	return false;
    }

    protected String[] getChannels (String portName, int portType){
	logger.debug("GetChannels: Entering");
	String[] ports;
	boolean matched=false;
	int tries=0;
	ArrayList<String> channels = new ArrayList<String>();
	while (matched == false && tries < 5){
	    tries++;
	    if (portType == PORT_INPUT)
		ports = listInputPorts();
	    else
		ports = listOutputPorts();
	    
	    for (String port: ports){
		logger.debug("GetChannels:"+port);
		if (port.matches(portName+"(_[12]{1})?")){
		    channels.add(port);
		    matched = true;
		    logger.debug("GetChannels: Matched port: "+port);
		}
	    }
	    if (!matched)
		try{
		    Thread.sleep(100);
		}
		catch(Exception e){}
	}
	logger.debug("GetChannels: Leaving. Total "+channels.size()+" ports matched.");
	return channels.toArray(new String[channels.size()]);
    }

    public String getName() {
	return serviceInstanceName;
    }    

    protected int initializeJack(){
	logger.debug("InitializeJack: Entering");
	Runtime.getRuntime().addShutdownHook(new Thread("JackdShutdownHandler"){
		public void run(){
		    logger.info("Shutting down jackd.");
		    stopJack();
		}
	    });
	logger.debug("InitializeJack: Starting Jackd.");
	try{
	    jackMonitor = new ProcessMonitor("jackd --nozombies -d alsa -r "+stationConfiguration.getStringParam(StationConfiguration.ENCODE_SAMPLE_RATE), this);
	    Thread.sleep(200);
	}catch(Exception e){
	    logger.error("InitializeJack: Jack initialization failed.");
	}
	
	String jackClientName = CHANNEL_SERVICE_JCNAME;
	logger.debug("InitializeJack: Initializing JNI.");
	initJackJNI();
	if (openJackClient(jackClientName) == 0){
	    logger.info("InitializeJack: Jack Initialized.");
	    setState(LISTENING);
	    logger.debug("InitializeJack: Leaving");
	    return 0;
	}
	else {
	    if (sendReadyMessage(false) == -1) {
		logger.error("InitializeJack: Unable to send not ready message.");
		stopJack();
		disconnect();
	    }
	    else
		logger.debug("InitializeJack: Not ready message sent");
	    //XXX Reconnect after timeout?
	    logger.debug("InitializeJack: Leaving");
	    //System.exit(-1);
	    return -1;
	}    
    }

    public void processExited (Process process, int exitValue){
	logger.fatal("ProcessExited: Jackd is no longer running.");
	stopJack();
	if (sendReadyMessage(false) == -1){
	    disconnect();
	}
	//System.exit(-1);
    }

    public void handleProcessError (Process process, String error){
	logger.error("HandleProcessError: "+error);
    }

    protected void stopJack() {
	logger.info("StopJack:");
	if (jackMonitor != null)
	    jackMonitor.destroy();
    }
    
    public static void main(String args[]) {
	Getopt g = new Getopt("ChannelService", args, "c:m:");
	int c;
	String configFilePath = "./automation.conf";
	String machineID = "127.0.0.1";
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    case 'm':
		machineID = g.getOptarg();
		break;
	    }
	}

	StationConfiguration stationConfiguration = new StationConfiguration(machineID);
	stationConfiguration.readConfig(configFilePath);

	ChannelService channelService = new ChannelService(stationConfiguration, 
							   stationConfiguration.getServiceInstanceOnMachine(RSController.CHANNEL_SERVICE, machineID),
							   machineID);
    }

    protected void setState (int state){
	int previousState = this.state;
	this.state = state;
	logger.info("SetState: Previous State: "+previousState +" CurrentState: "+state);
    }

    protected int state(){
	return state;
    }

    protected int sendMessage (IPCMessage message){
	return ipcServer.handleOutgoingMessage(message);
    }

    private void disconnect() {
	connectionError();
    }

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	stopJack();
	disconnect();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");
	return ipcServer.handleOutgoingMessage(message);
    }
}
*/