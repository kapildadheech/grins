package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.libmsgs.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;


import org.gramvaani.radio.app.RSApp;

import org.gramvaani.radio.rscontroller.pipeline.RSPipeline;

import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.linkmonitor.*;

import java.io.*;
import java.util.*;

import gnu.getopt.Getopt;



public class LibService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, 
				   IdleMonitorCallback, DefaultUncaughtExceptionCallback, 
				   MediaLibListener, DiagnosticUtilitiesDBCallback, 
				   DiagnosticUtilitiesConnectionCallback {

    public static final String CACHE_INSERT = "CACHE_INSERT";
    public static final String CACHE_UPDATE = "CACHE_UPDATE";
    public static final String CACHE_DELETE = "CACHE_DELETE";

    public static final String UNKNOWN = "Unknown";
    
    public static final String SUCCESS_MSG = "SUCCESS";
    public static final String FAILURE_MSG = "FAILURE";

    
    public static final int START_ARCHIVING_PLAYOUT 	= 0;
    public static final int DONE_ARCHIVING_PLAYOUT 	= 1;
    public static final int DONE_UPLOAD 		= 2;
    public static final int DONE_ENCODING 		= 3;
    public static final int ENCODING_FAILED 		= 4;
    public static final int START_ARCHIVING_RECORD 	= 5;
    public static final int DONE_ARCHIVING_RECORD 	= 6;
    public static final int START_TELEPHONY 		= 7;
    public static final int DONE_TELEPHONY 		= 8;
    public static final int REPLACING_ORIGINAL		= 9;

    public static final int MEDIA_NOT_INDEXED = 0;
    public static final int MEDIA_INDEXED     = 1;

    /*
    public static final String IMPORTED_TELEPHONY_TYPE 	= "telephonyimport";
    public static final String TELEPHONY_TYPE 		= "telephony";
    public static final String UPLOAD_TYPE 		= "upload";
    public static final String MIC_TYPE 		= "mic";
    */

    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected String machineID;
    protected String serviceInstanceName;
    protected LogUtilities logger;
    protected MediaLib medialib;
    protected TimeKeeper timeKeeper;
    protected LinkMonitorClient linkMonitor = null;
    protected IdleMonitor idleMonitor;
    protected LazyEncoder lazyEncoder;
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected DiagnosticUtilities diagUtil;
    protected String registrationID;
    protected String resourceManagerID = "";
    protected Hashtable<String, String> cacheProviders;
    protected HashSet<String> persistentCacheProviders;
    protected FileStoreManager fileStoreManager;

    private boolean pendingEncodes = true;
   
    protected boolean isIndexServiceActive = false;
    protected boolean libServiceRegistered = false;
    protected boolean isLibServiceReady = false;

    public LibService(IPCServer ipcServer, StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.ipcServer = ipcServer;
	this.machineID = machineID;
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	logger = new LogUtilities(RSController.LIB_SERVICE);
	logger.debug("LibService: Entering. Initializing with machineID=" + machineID);
	logger.info("LibService: Initializing");
	registrationID = GUIDUtils.getGUID();
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	//diagUtil.registerDBListener(this);
	cacheProviders = new Hashtable<String, String>();
	populatePersistentProviders();

	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	MediaLib.registerListener(RSController.LIB_SERVICE, this);
	MediaLib.setIPCServer(ipcServer);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	
	fileStoreManager = new FileStoreManager(stationConfiguration, RSController.LIB_SERVICE, ipcServer,
						new String[] {StationNetwork.AUDIO_PLAYOUT, 
							      StationNetwork.AUDIO_PREVIEW},
						timeKeeper,
						StationNetwork.UPLOAD,
						getName());
	
	/*
	if (medialib != null) {
	    if (ipcServer.registerIPCNode(RSController.LIB_SERVICE, this, registrationID) == 0)
		libServiceRegistered = true;
	}
	*/

	registerIPC();
	
	idleMonitor = new IdleMonitor(stationConfiguration, this, RSController.LIB_SERVICE);
	lazyEncoder = new LazyEncoder(medialib, stationConfiguration, RSController.LIB_SERVICE);


	if (medialib == null)
	    diagUtil.dbError(this);

	logger.debug("LibService: Leaving");
    }

    public LibService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {
	this.stationConfiguration = stationConfiguration;
	this.machineID = machineID;
	this.serviceInstanceName = serviceInstanceName;
	registrationID = GUIDUtils.getGUID();
        ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	logger = new LogUtilities(RSController.LIB_SERVICE);
	logger.debug("LibService: Entering. Initializing with machineID="+machineID);
	logger.info("LibService: Initializing.");
	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "LibService");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);

	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);

	cacheProviders = new Hashtable<String, String>();
	populatePersistentProviders();

	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	MediaLib.registerListener(RSController.LIB_SERVICE, this);

	MediaLib.setIPCServer(ipcServer);
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);

	fileStoreManager = new FileStoreManager(stationConfiguration, RSController.LIB_SERVICE, ipcServer,
						new String[] {StationNetwork.AUDIO_PLAYOUT, 
							      StationNetwork.AUDIO_PREVIEW},
						timeKeeper,
						StationNetwork.UPLOAD,
						getName());

	//if (medialib != null) {
	    registerIPC();
	    //}
	linkMonitor = new LinkMonitorClient(this, RSController.LIB_SERVICE, stationConfiguration);
	linkMonitor.start();

	idleMonitor = new IdleMonitor(stationConfiguration, this, RSController.LIB_SERVICE);
	lazyEncoder = new LazyEncoder(medialib, stationConfiguration, RSController.LIB_SERVICE);

	if (medialib == null)
	    diagUtil.dbError(this);

	logger.debug("LibService: Leaving");
    }

    protected void registerIPC(){
	if (!libServiceRegistered && ipcServer.registerIPCNode(RSController.LIB_SERVICE, this, registrationID) == -1) {
	    logger.error("LibService: Unable to connect to ipcServer.");
	    connectionError();
	} else {
	    libServiceRegistered = true;
	}
    }

    protected void populatePersistentProviders() {
	persistentCacheProviders = new HashSet<String>();
	persistentCacheProviders.add(RSController.UI_SERVICE);
    }


    protected void serviceInterests(){
	ServiceInterestMessage interestMessage = new ServiceInterestMessage(RSController.LIB_SERVICE, 
									    RSController.RESOURCE_MANAGER,
									    "", new String[] {RSController.INDEX_SERVICE});
	sendMessage(interestMessage);
    }

    public synchronized void activateMediaLib(boolean activate){
	if (activate){
	    /*medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	    if (checkStatus())
	    sendReadyMessage(true);*/
	} else {
	    //isLibServiceActive = false;
	    sendReadyMessage(false);
	    diagUtil.dbError(this);
	}
    }
    
    /*
    public synchronized void mediaLibUp(MediaLib medialib){
	if (this.medialib == null){
	    this.medialib = medialib;
	    registerIPC();
	    sendReadyMessage(true);
	}
    }
    */

    public synchronized boolean attemptDBConnect(){
	logger.info("AttemptDBConnect: Attempting database reconnection.");
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null){
	    if (!libServiceRegistered){
		if (ipcServer.registerIPCNode(RSController.LIB_SERVICE, this, registrationID) == 0)
		    libServiceRegistered = true;
	    }
	    lazyEncoder.setMediaLib(medialib);
	    logger.info("AttemptDBConnect: Database reconnected");
	    if (checkStatus())
		sendReadyMessage(true);
	    return true;
	}
	else
	    return false;
    }

    //Actively check for all the dependencies.
    protected boolean checkStatus() {
	boolean status = false;
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	if (medialib != null){
	    status = true;
	    if (!isLibServiceReady)
		sendReadyMessage(true);
	}
	return status;
    }

    protected int sendReadyMessage(boolean readiness) {

	if (medialib != null) {
	    medialib.setLibraryActivated(readiness);
	}

	logger.debug("SendReadyMessage: Sending ready message readiness=" + readiness);

	ServiceReadyMessage readyMessage = new ServiceReadyMessage(RSController.LIB_SERVICE, 
								   RSController.RESOURCE_MANAGER,
								   readiness);
	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isLibServiceReady = readiness;
		logger.debug("SendReadyMessage: Ready messsage sent. isLibServiceReady=" + isLibServiceReady);
		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Message timeout exception.");
	    return -1;
	}

    }

       
    public synchronized IPCServer attemptReconnect(){
	if (libServiceRegistered)
	    return ipcServer;

	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    libServiceRegistered = true;
	    timeKeeper.setIPCServer(ipcServer);
	    fileStoreManager.setIPCServer(ipcServer);
	    MediaLib.setIPCServer(ipcServer);
	    linkMonitor.reset();
	    return ipcServer;
	} else {
	    return null;
	}
    }
    
    public void serviceDown(String service){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
    }

    public synchronized void connectionError() {
	logger.error("ConnectionError: Trying to reconnect");
	libServiceRegistered = false;
	timeKeeper.setIPCServer(null);
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);

	logger.debug("ConnectionError: Leaving");
    }

    public synchronized void handleUncaughtException(Thread t, Throwable ex){
	logger.error("HandleUncaughtException: Exception: ", ex);
    }

    public synchronized void keepListeningForPreempts() {
	if (ipcServer == null) {
	    connectionError();
	}
    }

    public synchronized void detectedIdle() {
	logger.debug("DetectedIdle: Idle state detected.");
	if (medialib == null){
	    if (!checkStatus()) {
		logger.error("DetectedIdle: Medialib is null.");
		return;
	    }
	}
	    
	RadioProgramMetadata radioProgram = RadioProgramMetadata.getDummyObject("");
	
	for (int state: new int [] {DONE_ARCHIVING_RECORD, DONE_ARCHIVING_PLAYOUT, DONE_UPLOAD}){
	    radioProgram.setState(state);
	    if (medialib.get(radioProgram) != null){
		pendingEncodes = true;
		break;
	    }
	}
	
	if (pendingEncodes) {
	    logger.debug("DetectIdle: Pending encodes found starting to encode.");
	    lazyEncoder.startEncoding();
	    logger.debug("DetectIdle: Encoding done.");
	    pendingEncodes = false;
	} else {
	    logger.debug("DetectIdle: No encoding jobs pending.");
	}

    }

    public synchronized void preemptBackground() {
	logger.info("PreemptBackground: Preempting background process.");
	if (lazyEncoder != null)
	    lazyEncoder.pauseEncoding();
    }

    public synchronized void databaseError(String error) {
	logger.error("DatabaseError: Received error from medialib: "+error);
	
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering. From: "+message.getSource()+" Type: "+message.getMessageClass() + 
		     "Message: " + message.getMessageString());

	boolean indexUp = false;
	
	if (message instanceof StartCleanupMessage) {
	    StartCleanupMessage cleanupMessage = (StartCleanupMessage)message;
	    handleCleanup(cleanupMessage.getSource(), cleanupMessage.getCommands());
	} else if (message instanceof SyncFileStoreMessage) {
	    syncFileStore();
	} else if (message instanceof ArchivingStartMessage) {
	    ArchivingStartMessage archiverMessage = (ArchivingStartMessage)message;
	    handleArchivingStart(archiverMessage.getSource(), archiverMessage.getArchiveType(), 
				 archiverMessage.getLanguage(), archiverMessage.getFilename(),
				 archiverMessage.getArchivingType());
	} else if (message instanceof ArchivingDoneMessage) {
	    ArchivingDoneMessage archivingMessage = (ArchivingDoneMessage)message;
	    handleArchivingDone(archivingMessage.getSource(), archivingMessage.getArchivedFilename(),
				archivingMessage.getArchType(), archivingMessage.getArchLanguage(), archivingMessage.getArchivingType());
	} else if (message instanceof RadioProgramStatUpdateMessage) {
	    RadioProgramStatUpdateMessage statUpdateMessage = (RadioProgramStatUpdateMessage)message;
	    handleStatUpdate(statUpdateMessage.getSource(), statUpdateMessage.getStatUpdate());
	} else if (message instanceof RadioProgramGetAudioMessage) {
	    RadioProgramGetAudioMessage audioMessage = (RadioProgramGetAudioMessage)message;
	    handleRadioProgramGetAudio(audioMessage.getSource(), audioMessage.getAudioFilename(), audioMessage.getMethod());
	} else if (message instanceof TimeKeeperResponseMessage) {
	    timeKeeper.handleTimeKeeperResponseMessage((TimeKeeperResponseMessage)message);
	} else if (message instanceof HeartbeatMessage && linkMonitor != null){
	    HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
	    linkMonitor.handleMessage(heartbeatMessage);
	} else if (message instanceof ServiceErrorRelayMessage) {
	    ServiceErrorRelayMessage errorMessage = (ServiceErrorRelayMessage)message;
	    if (errorMessage.getServiceName().equals(RSController.UI_SERVICE))
		cacheProviders.remove(RSController.UI_SERVICE);
	    if (errorMessage.getServiceName().equals(RSController.INDEX_SERVICE))
		isIndexServiceActive = false;
	} 
	//Service error message not used anymore
	/*else if (message instanceof ServiceErrorMessage) {
	  ServiceErrorMessage errorMessage = (ServiceErrorMessage) message;
	  if (errorMessage.getSource().equals(RSController.INDEX_SERVICE))
	  isIndexServiceActive = false;
	  } */
	else if (message instanceof ServiceActivateNotifyMessage) {
	    ServiceActivateNotifyMessage notifyMessage = (ServiceActivateNotifyMessage)message;
	    if (notifyMessage.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		timeKeeper.sync();
	    }

	} else if (message instanceof PreemptBackgroundActivityMessage) {
	    preemptBackground();

	} else if (message instanceof RegisterCacheProviderMessage) {
	    RegisterCacheProviderMessage registerMessage = (RegisterCacheProviderMessage)message;
	    handleRegisterMessage(registerMessage.getSource(), registerMessage.getWidgetName());

	} else if (message instanceof InvalidateCacheMessage) {
	    InvalidateCacheMessage cacheMessage = (InvalidateCacheMessage)message;
	    handleInvalidateCacheMessage(cacheMessage.getObjectType(), cacheMessage.getActionType(), cacheMessage.getObjectIDs());
	} else if (message instanceof InvalidateCacheKeyMessage) {
	    if (!message.getSource().equals(RSController.LIB_SERVICE)) {
		InvalidateCacheKeyMessage cacheMessage = (InvalidateCacheKeyMessage)message;
		handleInvalidateCacheKeyMessage(cacheMessage.getObjectType(), 
						cacheMessage.getActionType(), 
						cacheMessage.getOldObjectIDs(),
						cacheMessage.getNewObjectIDs());
	    }

	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage) message;
	    for (String service: notifyMessage.getActivatedServices())
		if (service.equals(RSController.INDEX_SERVICE))
		    indexUp = true;

	} else if (message instanceof ServiceAckMessage) {
	    ServiceAckMessage ackMessage = (ServiceAckMessage) message;
	    for (String service: ackMessage.getActiveServices())
		if (service.equals(RSController.INDEX_SERVICE))
		    indexUp = true;

	} else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());

	} else if (message instanceof StatusCheckRequestMessage){
	    StatusCheckRequestMessage statusMessage = (StatusCheckRequestMessage)message;
	    handleStatusCheckRequestMessage(statusMessage.getSource(), statusMessage.getID());

	} else if (message instanceof MachineStatusRequestMessage){
	    MachineStatusRequestMessage requestMessage = (MachineStatusRequestMessage)message;
	    handleMachineStatusRequestMessage(requestMessage.getWidgetName(), requestMessage.getCommand(), requestMessage.getOptions());
	} else if (message instanceof UploadDoneMessage) {
	    UploadDoneMessage uploadDoneMessage = (UploadDoneMessage)message;
	    handleUploadDoneMessage(uploadDoneMessage.getSource(), uploadDoneMessage.getFileName(), uploadDoneMessage.getUploaderName(), uploadDoneMessage.getWidgetName());
	} else if (message instanceof DeleteProgramsMessage) {
	    DeleteProgramsMessage deleteMessage = (DeleteProgramsMessage)message;
	    deletePrograms(deleteMessage.getPrograms());
	} else {

	    logger.warn("HandleIncomingMessage: Unknown message type.");
	}


	if (indexUp){
	    isIndexServiceActive = true;
	    sendUpdateIndexMessage();
	}

	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }

    protected void deletePrograms(String[] programs) {
	String[] newItems = new String[programs.length];
	int i = 0;
	HashSet<String> playlistsToUpdate = getPlaylistsWithPrograms(programs);
	
	for (String fileName : programs) {
	    deleteFile(fileName);

	    //Check if the file has an 
	    EncodedFiles encodedFilesDummy = EncodedFiles.getDummyObject();
	    encodedFilesDummy.setOldItemID(fileName);
	    EncodedFiles encodedFile = medialib.getSingle(encodedFilesDummy);
	    
	    if (encodedFile != null) {
		String newFileName = encodedFile.getNewItemID();
		String encDirName = stationConfiguration.getStringParam(StationConfiguration.LIB_ENC_DIR);

		File encFile = new File(encDirName + File.separator + newFileName);
		if (encFile.exists()) {
		    //The file has not been propagated to other stores as yet.
		    logger.info("DeletePrograms: Found encoded file : " + newFileName + " of original file " + fileName);
		    deleteFileFromDisk(encDirName + File.separator + newFileName);
		} else {
		    //File has been moved to some or all file stores but has not 
		    //replaced the original file in the system
		    logger.info("DeletePrograms: Found encoded file : " + newFileName + " of original file " + fileName + " partially moved in to the system.");
		    deleteFile(newFileName);
		    HashSet<String> playlists = getPlaylistsWithPrograms(new String[]{newFileName});
		    playlistsToUpdate.addAll(playlists);
		}
		
		if (medialib.remove(encodedFile, false) < 0) {
		    logger.error("DeleteFile: Could not remove encoded files entry for " + fileName + "," + newFileName);
		}
	    }

	    newItems[i] = CACHE_DELETE;
	    i++;
	}
	
	String[] playlistArr = playlistsToUpdate.toArray(new String[playlistsToUpdate.size()]);

	handleInvalidateCacheKeyMessage("RadioProgramMetadata", CACHE_DELETE, programs, newItems);
	handleInvalidateCacheMessage(Metadata.getClassName(Playlist.class), CACHE_UPDATE, playlistArr);
    }

    protected HashSet<String> getPlaylistsWithPrograms(String[] updatedRPMs) {
	PlaylistItem dummy = new PlaylistItem("");
	HashSet<String> playlists = new HashSet<String>();

	for (String rpm: updatedRPMs) {
	    dummy.setItemID(rpm);
	    PlaylistItem[] items = medialib.get(dummy);
	    for (PlaylistItem item: items) {
		//logger.info("GetPlaylistsWithPrograms: Adding: " + item.getPlaylistID());
		playlists.add(String.valueOf(item.getPlaylistID()));
	    }
	}

	return playlists;
    }

    protected void handleUploadDoneMessage(String source, String fileName, String uploaderName, String widgetName) {
	logger.info("HandleUploadDoneMessage: Source= " + source + " FileName= " + fileName + " UploaderName= " + uploaderName);
	StationNetwork stationNetwork = stationConfiguration.getStationNetwork();

	int playoutAttempts = FileStoreManager.ZERO_ATTEMPTS;
	int previewAttempts = FileStoreManager.ZERO_ATTEMPTS;
	
	//Figure out which stores are local to uploader and do not try to upload to them right now.
	//In case of medialibuploadprovider, it will try to copy the file. If it fails, the files will be 
	//pushed during handle cleanup. In case of archiver, the db entry is already updated.
	//Also in case the store is local to us the db entry is already updated
	if (stationNetwork.isLocalStore(StationNetwork.AUDIO_PLAYOUT)
	    || stationNetwork.isStoreLocalToUploader(StationNetwork.AUDIO_PLAYOUT, uploaderName)) {

	    playoutAttempts = FileStoreManager.UPLOAD_DONE;
	} 

	if (stationNetwork.isLocalStore(StationNetwork.AUDIO_PREVIEW)
	   || stationNetwork.isStoreLocalToUploader(StationNetwork.AUDIO_PREVIEW, uploaderName)) {
	
	    previewAttempts = FileStoreManager.UPLOAD_DONE;
	} 

	if (previewAttempts != FileStoreManager.UPLOAD_DONE ||
	   playoutAttempts != FileStoreManager.UPLOAD_DONE) {
	    //Note: the order of attempts specified here should be the same as the order
	    //of destination stores specified during instantiation.
	    logger.info("HandleUploadDoneMessage: Adding file for upload. playoutAttempts=" + playoutAttempts + " previewAttempts=" + previewAttempts);
	    fileStoreManager.addFileToUpload(fileName, new int[] {playoutAttempts, previewAttempts});
	    fileStoreManager.triggerUpload();
	}
	
	//dont send ack message if the call was made locally
	if (source.equals(RSController.LIB_SERVICE))
	    return;
	
	//Not needed
	/*Service serviceInstance = stationConfiguration.getServiceInstance(uploaderName);
	if (serviceInstance != null && serviceInstance.serviceType().equals(RSController.ARCHIVER_SERVICE))
	pendingEncodes = true;*/


	UploadDoneAckMessage ackMessage;
	if (widgetName != null) {
	    ackMessage = new UploadDoneAckMessage(RSController.LIB_SERVICE,
						  source, fileName, widgetName);
	} else {
	    ackMessage = new UploadDoneAckMessage(RSController.LIB_SERVICE,
						  source, fileName);
	}

	sendMessage(ackMessage);
    }

    protected void handleRegistrationAckMessage(boolean ack, String id){
	String oldID = resourceManagerID;
	resourceManagerID = id;
	boolean isLibUp = (medialib != null);
	if (ack) {
	    boolean serviceInterestSent = false;
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);
	    if (oldID.equals("")) {
		logger.debug("HandleRegistrationAckMessage: New registration. OldID=" + oldID);
		serviceInterests();
		serviceInterestSent = true;
		sendReadyMessage(isLibUp);
	    } else if (!oldID.equals(id)){
		logger.error("HandleRegistrationAckMesage: RSController went down.");
		serviceInterests();
		serviceInterestSent = true;
		sendReadyMessage(isLibUp);
	    } else if (!isLibServiceReady) {
		logger.warn("HandleRegistrationAckMessage: Connection went down before ready message was sent.");
		if (!serviceInterestSent)
		    serviceInterests();
		sendReadyMessage(isLibUp);
	    } else {
		logger.debug("HandleRegistrationAckMessage: Reconnected after brief disconnection. isLibServiceReady=" + isLibServiceReady);
	    }
	} else {
	    logger.warn("HandleRegistrationAckMessage: Registration NACK received. Resetting service.");
	    resetService();
	}
    }

    protected void resetService(){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	if (ipcServer.handleOutgoingMessage(message) == -1)
	    connectionError();
    }

    protected void handleStatusCheckRequestMessage(String source, String id){
	if (!checkStatus()){
	    logger.error("HandleStatusCheckRequestMessage: Status check returned false.");
	} else {
	    logger.info("HandleStatusCheckRequestMessage: Status check successful.");
	}
    }

    protected void syncFileStore() {
	fileStoreManager.syncStoresManually();
    }

    protected void handleCleanup(String source, String[] commands) {
	logger.info("HandleCleanup: Entering. source = " + source + " command length = " + commands.length);

	if (medialib == null){
	    medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);
	    if (medialib == null){
		logger.error("HandleCleanup: Cannot do cleanup. Medialib is null");
		return;
	    }
	}
	
	for (String command: commands) {
	    logger.info("HandleCleanup: Entering. source = " + source + ", command = " + command);
	    
	    if (command.equals(StartCleanupMessage.ENCODED_FILES)) {
		cleanupEncodedFiles();
	    } else if (command.equals(StartCleanupMessage.DELETED_FILES)) {
		cleanupDeletedFiles();
	    } 
	}

	//Sending ready message from updateindex
	logger.info("HandleCleanup: Exiting.");
    }

    void cleanupEncodedFiles() {
	ArrayList<String> oldDataList = new ArrayList<String>();
	ArrayList<String> newDataList = new ArrayList<String>();

	String dirName  = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.UPLOAD) +"/";
	String encDirName = stationConfiguration.getStringParam(StationConfiguration.LIB_ENC_DIR) + "/";
	EncodedFiles encodedFile = EncodedFiles.getDummyObject();
	Metadata[] encodedFilesMetadata = medialib.get(encodedFile);
	EncodedFiles[] encodedFiles = new EncodedFiles[encodedFilesMetadata.length];
	int i = 0;
	
	for (Metadata encodedFileMetadata: encodedFilesMetadata)
	    encodedFiles[i++] = (EncodedFiles)encodedFileMetadata;
	for (EncodedFiles file: encodedFiles) {
	    String fileName = file.getOldItemID();
	    String newFileName = file.getNewItemID();
	    File encFile = new File(encDirName+File.separator+newFileName);
	    
	    if (!encFile.exists()) {
		//Check if the new file has reached all the stores. If so then call replace file
		//else just skip this entry as we have already processed this file in local store
		
		RadioProgramMetadata newProgram = medialib.getSingle(RadioProgramMetadata.getDummyObject(newFileName));
		if (newProgram != null) {
		    if (fileStoreManager.isAvailableAtConsumerStores(newProgram)) {
			logger.debug("HandleCleanup: All stores have the encoded file:" + newFileName + ". Initiating replacement of old file:" + fileName + " with the new file in the system.");
			if (replaceFile(fileName, newFileName)) {
			    oldDataList.add(fileName);
			    newDataList.add(newFileName);
			}
		    }
		}
		continue;
		
	    } else {
		
		//Testing for the special case where while encoding of original file
		//was going on, a request to delete the orginal file had come.
		RadioProgramMetadata dummyProgram = RadioProgramMetadata.getDummyObject(fileName);
		RadioProgramMetadata program = medialib.getSingle(dummyProgram);
		if (program == null || program.getUploadStoreAttempts() == FileStoreManager.FILE_DELETED) {
		    logger.info("HandleCleanup: " + fileName + " has been removed from the system. Removing its encoded file from the file store and database entry");
		    if (encFile.delete()) {
			logger.info("HandleCleanup: Deleted " + newFileName + " successfully from the disk.");
		    } else {
			logger.error("HandleCleanup: Failed to delete " + newFileName + " from the disk.");
		    }
		    
		    if (medialib.remove(file, false) < 0) {
			logger.error("HandleCleanup: Failed to remove encoded files entry " + fileName + "," + newFileName + " from the database");
		    }
		}
	    }
	    
	    try {
		if (FileUtilities.changeFilename(newFileName, encDirName, newFileName, dirName) == 0) {
		    logger.debug("HandleCleanup: changed filename from " + 
				 encDirName + "/" + newFileName + " to " + 
				 dirName + "/" + newFileName);
		    
		} else {
		    logger.error("HandleCleanup: Failed to change filename from " + 
				 encDirName + "/" + newFileName + " to " + 
				 dirName + "/" + newFileName);
		    
		    logger.error("HandleCleanup: Not doing database update.");
		    continue;
		}
		
		RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(fileName);
		RadioProgramMetadata newMetadata;
		RadioProgramMetadata oldMetadata;
		Metadata[] programMetadata = medialib.get(metadata);
		
		if (programMetadata.length == 0) {
		    logger.error("HandleCleanup: Could not find Radio Program Metadata for: " + fileName);
		    continue;
		} else {
		    oldMetadata = (RadioProgramMetadata)programMetadata[0];
		}
		
		newMetadata = oldMetadata.clone();
		newMetadata.setItemID(newFileName);
		newMetadata.setState(LibService.REPLACING_ORIGINAL);
		
		fileStoreManager.clearAllFileStoreFields(newMetadata);
		fileStoreManager.setFileStoreField(newMetadata, StationNetwork.UPLOAD, FileStoreManager.UPLOAD_DONE);
		if (medialib.insert(newMetadata) < 0) {
		    logger.error("HandleCleanup: Unable to insert Radio Program Metadata for encoded file: " + newFileName);
			    continue;
		} 
		
		/*
		  if ( (newMetadata.getPlayoutStoreAttempts() == FileStoreManager.UPLOAD_DONE) &&
		  (newMetadata.getPreviewStoreAttempts() == FileStoreManager.UPLOAD_DONE) ) {
		*/
		
		if (fileStoreManager.isAvailableAtConsumerStores(newMetadata)){
		    logger.debug("HandleCleanup: All stores have the encoded file:" + newFileName + ". Initiating replacement of old file:" + fileName + " with the new file in the system.");
		    if (replaceFile(fileName, newFileName)) {
			oldDataList.add(fileName);
			newDataList.add(newFileName);
		    }
		}
		
	    } catch(Exception e) {
		logger.error("HandleCleanup: Unable to change filename from " + dirName + "/" + fileName + " to " + encDirName + "/" + newFileName);
	    }
	}

	if (oldDataList.size() > 0) {
	    handleInvalidateCacheKeyMessage("RadioProgramMetadata", CACHE_UPDATE, oldDataList.toArray(new String[]{}), newDataList.toArray(new String[]{}));
	}
	
	fileStoreManager.handleCleanup();
    }

    protected void cleanupDeletedFiles() {
	String localStoreName = StationNetwork.UPLOAD;

	RadioProgramMetadata whereMetadata = RadioProgramMetadata.getDummyObject("");
	//setFileStoreField(localStoreName, whereMetadata, FileStoreManager.UPLOAD_DONE);
	//setFileStoreField(StationNetwork.UPLOAD, whereMetadata, FileStoreManager.FILE_DELETED);
	
	String whereClause = buildWhereClause(localStoreName);
	Metadata[] metadata = (Metadata[])medialib.selectAllFields(whereMetadata, whereClause, -1);

	//Metadata[] metadata =  medialib.get(whereMetadata);
	RadioProgramMetadata[] programMetadata = new RadioProgramMetadata[metadata.length];
	
	for (int i = 0; i < metadata.length; i++) {
	    programMetadata[i] = (RadioProgramMetadata)metadata[i];
	}

	for (RadioProgramMetadata progMetadata : programMetadata) {
	    deleteFile(progMetadata.getItemID());
	}
    }

    protected String buildWhereClause(String localStoreName) {
	String whereClause = fileStoreManager.getFileStoreDBFieldName(localStoreName) + "=" + FileStoreManager.UPLOAD_DONE;
	whereClause += " and ( ";
	for (int i = 0; i < StationNetwork.stores.length; i++) {
	    String store = StationNetwork.stores[i];
	    if (store.equals(localStoreName))
		continue;
	    String destFieldName = fileStoreManager.getFileStoreDBFieldName(store);
	    whereClause += " " + destFieldName + "=" + FileStoreManager.FILE_DELETED + " ";
	    if (i+1 < StationNetwork.stores.length)
		whereClause += " or ";
	}
	whereClause += " )";
	logger.debug("BuildeWhereClause: WhereClause= " + whereClause);
	return whereClause;
    }

    //XXX: This can be optimized. Deletion of playout history, etc for the item id
    //is done by all file stores but needs to be done only by the first one.
    protected void deleteFile(String fileName) {
	String localStoreName = StationNetwork.UPLOAD;
	String dirName = stationConfiguration.getStationNetwork().getLocalStoreDir();
	RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(fileName);

	PlayoutHistory playout = PlayoutHistory.getDummyObject(fileName);
	if (medialib.remove(playout, false) < 0) 
	    logger.error("DeleteFile: Could not remove playout history for:" + fileName);

	Creator creator = Creator.getDummyObject(fileName);
	if (medialib.remove(creator, false) < 0) 
	    logger.error("DeleteFile: Could not remove creators for:" + fileName);

	ItemCategory category = ItemCategory.getDummyObject(fileName);
	if (medialib.remove(category, false) < 0) 
	    logger.error("DeleteFile: Could not remove item categories for:" + fileName);

	Tags tag = Tags.getDummyObject(fileName);
	if (medialib.remove(tag, false) < 0) 
	    logger.error("DeleteFile: Could not remove tags for:" + fileName);

	PlaylistItem playlistItem = PlaylistItem.getDummyObject(fileName);
	if (medialib.remove(playlistItem, false) < 0)
	    logger.error("DeleteFile: Could not remove file: "+fileName +" from playlists.");
	
	//Do updation of metadata
	updateFileDeletedInDB(localStoreName, fileName);
	
	//Delete the old file
	deleteFileFromDisk(dirName + File.separator + fileName);
    }

    protected void deleteFileFromDisk(String fileName) {
	try {
	    File oldFile = new File(fileName);
	    if (!oldFile.exists()) {
		logger.error("DeleteFile: File "+fileName+" does not exist.");
		return;
	    }

	    if (oldFile.delete()) {
		logger.info("DeleteFile: Deleted the old file: " + fileName);
	    } else {
		logger.error("DeleteFile: Unable to delete the old file: " + oldFile.getName());
	    }
	} catch(Exception e) {
	    logger.error("DeleteFile: Got exception in trying to delete file: " + fileName);
	}
    }

    protected boolean replaceFile(String fileName, String newFileName) {
	String dirName = stationConfiguration.getStationNetwork().getStoreDir(StationNetwork.UPLOAD);


	//Update the database to point to the new metadata entry
	RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(fileName);
	RadioProgramMetadata newMetadata = RadioProgramMetadata.getDummyObject(newFileName);
	
	PlayoutHistory[] history = metadata.populate__history(medialib);
	Creator[] creators = metadata.populate__creators(medialib);
	ItemCategory[] categories = metadata.populate__categories(medialib);
	Tags[] tags = metadata.populate__tags(medialib);
	
	for (PlayoutHistory hist: history){
	    PlayoutHistory newHistory = hist.clone();
	    newHistory.setItemID(newFileName);
	    if (medialib.update(hist, newHistory)<0)
		logger.error("ReplaceFile: Database update failed.");
	    else
		logger.debug("ReplaceFile: Database updated: "+hist.dump()+" with "+newHistory.dump());
	}
	
	for (Creator creator: creators){
	    Creator newCreator = creator.clone();
	    newCreator.setItemID(newFileName);
	    if (medialib.update(creator, newCreator)<0)
		logger.error("ReplaceFile: Database update failed.");
	    else 
		logger.debug("ReplaceFile: Database updated: "+creator.dump()+" with "+newCreator.dump());
	}
	
	for (ItemCategory category: categories){
	    ItemCategory newCategory = category.clone();
	    newCategory.setItemID(newFileName);
	    if (medialib.update(category, newCategory)<0)
		logger.error("ReplaceFile: Database update failed.");
	    else 
		logger.debug("ReplaceFile: Database updated: "+category.dump()+" with "+newCategory.dump());
	    
	}
	
	for (Tags tag: tags){
	    Tags newTag = tag.clone();
	    newTag.setItemID(newFileName);
	    if (medialib.update(tag, newTag)<0)
		logger.error("ReplaceFile: Database update failed.");
	    else 
		logger.debug("ReplaceFile: Database updated: "+tag.dump()+" with "+newTag.dump());
	    
	}
	
	PlaylistItem queryPlaylistItem = new PlaylistItem(-1);
	queryPlaylistItem.setItemID(fileName);
	Metadata playlistItems[] = medialib.get(queryPlaylistItem);
	for (Metadata itemMetadata: playlistItems){
	    PlaylistItem playlistItem = (PlaylistItem) itemMetadata;
	    PlaylistItem newPlaylistItem = playlistItem.clone();
	    newPlaylistItem.setItemID(newFileName);
	    if (medialib.update(playlistItem, newPlaylistItem) < 0)
		logger.error("ReplaceFile: Database update failed.");
	    else
		logger.debug("ReplaceFile: Database updated: "+playlistItem.dump()+" with "+newPlaylistItem.dump());
	}
	
	Hotkey queryHotkey = new Hotkey(fileName);
	Metadata hotkeys[] = medialib.get(queryHotkey);
	for (Metadata hotkeyMetadata: hotkeys){
	    Hotkey hotkey = (Hotkey) hotkeyMetadata;
	    Hotkey newHotkey = hotkey.clone();
	    newHotkey.setItemID(newFileName);
	    if (medialib.update(hotkey, newHotkey) < 0)
		logger.error("ReplaceFile: Database update failed.");
	    else
		logger.debug("ReplaceFile: Database updated: "+hotkey.dump()+" with "+newHotkey.dump());
	}
	

	//XXX: Do all the updates above in a transaction. Rollback the transaction on failure.
	//Do updation of metadata
	updateFileDeletedInDB(StationNetwork.UPLOAD, fileName);


	deleteFileFromDisk(dirName + File.separator + fileName);

	//Remove entry from encoded files
	removeEncodedFileEntry(fileName, newFileName);
	RadioProgramMetadata newFileMetadata = (RadioProgramMetadata) newMetadata.clone();
	newMetadata.setState(DONE_ENCODING);

	if (medialib.update(newFileMetadata, newMetadata) < 0)
	    logger.error("ReplaceFile: Could not update state to DONE_ENCODING");
	else
	    logger.debug("ReplaceFile: State updated to DONE_ENCODING");
	
	return true;
	//should return false when update transation fails. make the change when update is done in a transaction
    }

    protected void removeEncodedFileEntry(String fileName, String newFileName) {
	EncodedFiles encodedFile = EncodedFiles.getDummyObject();
	encodedFile.setNewItemID(newFileName);
	encodedFile.setOldItemID(fileName);
	Metadata[] encodedFilesMetadata = medialib.get(encodedFile);
	if (encodedFilesMetadata.length == 0) {
	    logger.warn("RemoveEncodedFileEntry: Could not find entry: " + fileName + "," + newFileName + " in EncodedFiles.");
	} else {
	    EncodedFiles encodedFileEntry = (EncodedFiles)encodedFilesMetadata[0];
	    if (medialib.remove(encodedFileEntry, false) < 0) {
		logger.error("RemoveEncodedFileEntry: Could not remove encoded file entry: " + fileName + "," + newFileName);
	    }
	}
    }


    protected void updateFileDeletedInDB(String storeName, String fileName) {
	RadioProgramMetadata metadata = RadioProgramMetadata.getDummyObject(fileName);
	RadioProgramMetadata updateMetadata = RadioProgramMetadata.getDummyObject(fileName);
	fileStoreManager.setFileStoreField(updateMetadata, storeName, FileStoreManager.FILE_DELETED);
	
	if (medialib.update(metadata, updateMetadata, false) >= 0) {
	    logger.debug("UpdateFileDeletedInDB: Database update successful");
	    RadioProgramMetadata[] programMetadatas = medialib.get(metadata);
	    if (programMetadatas.length == 0) {
		logger.error("UpdateFileDeletedInDB: Could not get program metadata for: " + fileName);
	    } else {
		metadata = programMetadatas[0];
		if ( (metadata.getPlayoutStoreAttempts() == FileStoreManager.FILE_DELETED) &&
		    (metadata.getPreviewStoreAttempts() == FileStoreManager.FILE_DELETED) &&
		    (metadata.getUploadStoreAttempts() == FileStoreManager.FILE_DELETED)) {
		
		    medialib.remove(metadata, false);
		}
	    }
	} else {
	    logger.error("UpdateFileDeletedInDB: Database update failed");
	}
    }

    protected void updatePrograms(final ArrayList<String> oldDataList, final ArrayList<String> newDataList){
	//Thread t = new Thread("UpdateIndex Thread"){
	//public void run(){
	int totalItems = oldDataList.size();
	int totalNewItems = newDataList.size();
	logger.info("UpdateIndex: Updating Index for: "+totalItems);
	int numItemsPerMessage = 5;
	ArrayList<String> removeProgramList = new ArrayList<String>();
	
	if (isIndexServiceActive){
	    for (int i = 0; i < totalItems; i += numItemsPerMessage){
		int numItems;
		if (i <= totalItems - numItemsPerMessage)
		    numItems = numItemsPerMessage;
		else
				numItems = totalItems % numItemsPerMessage;
		String[] array = new String[numItems];			
		
		
		for (int j = 0; j < numItems; j++){
		    array[j] = oldDataList.get(i+j);
		}
		
		RemoveProgramMessage removeMessage = new RemoveProgramMessage(RSController.LIB_SERVICE,
									      RSController.INDEX_SERVICE,
									      array);
		removeMessage.setRemoveSerially(true);
		RemoveProgramResponseMessage removeResponse;
		
		try{
		    removeResponse = (RemoveProgramResponseMessage) sendSyncCommand(removeMessage);
		    logger.info("UpdateIndex: Removed entries: "+array.length);
		} catch (Exception e){
		    logger.error("UpdateIndex: Unable to send RemoveProgramMessage.", e);
		    //connectionError();
			    }
	    }
	    
	    //XXX confirm that index need not be flushed between update and remove.
	    
	    for (int i = 0; i < totalNewItems; i += numItemsPerMessage){    
		int numNewItems;
		
		if (i <= totalNewItems - numItemsPerMessage)
		    numNewItems = numItemsPerMessage;
		else
		    numNewItems = totalNewItems % numItemsPerMessage;
		
		String[] array = new String[numNewItems];
		
		for (int j = 0; j < numNewItems; j++){
		    array[j] = newDataList.get(i+j);
		}
		
		UpdateProgramMessage updateMessage = new UpdateProgramMessage(RSController.LIB_SERVICE, 
									      RSController.INDEX_SERVICE,
									      array);
		updateMessage.setUpdateSerially(true);

		UpdateProgramResponseMessage updateResponse;
		try{
		    updateResponse = (UpdateProgramResponseMessage) sendSyncCommand(updateMessage);
		    logger.info("UpdateIndex: Added new entries: "+array.length);
		} catch (Exception e){
		    logger.error("UpdateIndex: Unable to send UpdateProgramMessage.",e);
		    //connectionError();
		}
		
		
	    }
	    
	    if (totalItems > 0 || totalNewItems > 0){
		FlushIndexMessage flushMessage = new FlushIndexMessage(RSController.LIB_SERVICE, 
								       RSController.INDEX_SERVICE);
		flushMessage.setFlushSerially(true);
		FlushIndexResponseMessage responseMessage = null;
		try{
		    responseMessage = (FlushIndexResponseMessage) sendSyncCommand(flushMessage);
		} catch (Exception e) {
		    logger.error("UpdateIndex: Unable to send flush message", e);
		}
		
		if (responseMessage == null)
		    logger.error("UpdateIndex: Unable to send flush message.");
		else if (responseMessage.getReturnValue() < 0)
		    logger.error("UpdateIndex: Unable to flush index with error: "+responseMessage.getReturnValue());
	    }
	    
	} //if index service active.
	
	
	//}
	//};
	//t.start();
    }

    protected void handleArchivingStart(String source, String archiveType, String language, String filename, String archivingType) {
	/*logger.debug("HandleArchivingStart: Entering. source="+source+" archiveType="+archiveType+" language="+language+" filename="+filename);
	if (archiveType.equals(ArchiverService.BCAST_MIC)) {
	    RadioProgramMetadata metadata = new RadioProgramMetadata(filename, 0, "", archiveType, timeKeeper.getRealTime(), 
								     archivingType.equals(ArchiverService.ARCHIVING_TYPE[ArchiverService.RECORDING]) ? 
								     START_ARCHIVING_RECORD : START_ARCHIVING_PLAYOUT, 
								     MEDIA_NOT_INDEXED, language,
								     stationConfiguration.getStringParam(StationConfiguration.STATION_NAME));
	    metadata.setTitle("Live recording");
	    ArchivingStartResponseMessage responseMessage;
	    if (medialib.insert(metadata, false) > -1) {
		logger.debug("HandleArchivingStart: Metadata insert successful.");
		responseMessage = new ArchivingStartResponseMessage(RSController.LIB_SERVICE,
								    source,
								    filename, SUCCESS_MSG);
	    } else {
		logger.debug("HandleArchivingStart: Metadata insert failed.");
		responseMessage = new ArchivingStartResponseMessage(RSController.LIB_SERVICE,
								    source,
								    filename, FAILURE_MSG);
	    }
	    if (sendMessage(responseMessage) < 0)
		logger.debug("HandleArchivingStart: Failed to send ArchivingStartResponse message.");
	    else
		logger.debug("HandleArchivingStart: ArchivingStartResponseMessage sent.");
	}
	else
	    logger.warn("HandleArchivingStart: Unknown ArchiveType.");
	logger.debug("HandleArchivingStart: Leaving");
	*/
    }

    protected void handleArchivingDone(String source, String filename, String archType, String archLanguage, String archivingType) {
	/*logger.debug("HandleArchivingDone: Entering");
	final String libFilename = medialib.newBulkItemFilename() + "." + stationConfiguration.getStringParam(StationConfiguration.ARCHIVER_CODEC);
	
	logger.debug("HandleArchivingDone: source="+source+" filename="+filename+" libFilename="+libFilename);

	RadioProgramMetadata dummyObject = RadioProgramMetadata.getDummyObject(filename);

	RadioProgramMetadata archivedMetadata = medialib.getSingle(dummyObject);
	if (archivedMetadata == null) {
	    logger.debug("HandleArchivingDone: No archived metadata found. Inserting metadata.");
	    archivedMetadata = new RadioProgramMetadata(filename, 0, "", archType, timeKeeper.getRealTime(), 
							archivingType.equals(ArchiverService.ARCHIVING_TYPE[ArchiverService.RECORDING]) ?
							DONE_ARCHIVING_RECORD : DONE_ARCHIVING_PLAYOUT, MEDIA_NOT_INDEXED, archLanguage,
							stationConfiguration.getStringParam(StationConfiguration.STATION_NAME));
	    archivedMetadata.setTitle("Live recording");
	    long length = -1L;
	    
	    try{
		//System.err.println("filename: "+stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR)+"/"+filename);
		length = RSPipeline.getDuration(stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR)+"/"+filename);
	    } catch (Exception e){
		logger.error("HandleArchivingDone: Unable to find file length.", e);
	    }
	    archivedMetadata.setLength(length);
	    if (medialib.insert(archivedMetadata, false) > -1)
		logger.debug("HandleArchivingDone: Metadata insert successful.");
	    else
		logger.debug("HandleArchivingDone: Metadata insert failed.");
	} else {
	    logger.debug("HandleArchivingDone: Found previously inserted archive metadata.");

	    long length = -1L;
	    
	    try{
		length = RSPipeline.getDuration(stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR)+"/"+filename);
	    } catch (Exception e){
		logger.error("HandleArchivingDone: Unable to find file length.", e);
	    }
	    archivedMetadata.setLength(length);
	}

	logger.debug("HandleArchivingDone: Updating metadata");

	archivedMetadata.setFilename(libFilename);
	archivedMetadata.setState(archivingType.equals(ArchiverService.ARCHIVING_TYPE[ArchiverService.RECORDING]) ?
						       DONE_ARCHIVING_RECORD: DONE_ARCHIVING_PLAYOUT);

	ArchivingDoneResponseMessage archivingResponse = null;
	UpdateProgramMessage updateMessage = null;
	FlushIndexMessage flushMessage = null;
	try {
	    if (FileUtilities.changeFilename(filename, stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR),
					    libFilename, stationConfiguration.getStringParam(StationConfiguration.LIB_BASE_DIR)) >= 0 &&
	       medialib.update(dummyObject, archivedMetadata, false) > -1) {
		
		if (archivingType.equals(ArchiverService.ARCHIVING_TYPE[ArchiverService.PLAYOUT])) {
		    PlayoutHistory playoutHistory = new PlayoutHistory(libFilename, stationConfiguration.getStringParam(StationConfiguration.STATION_NAME),
								       timeKeeper.getRealTime());
		    medialib.insert(playoutHistory, false);
		}

		
		updateMessage = new UpdateProgramMessage(RSController.LIB_SERVICE, 
							 RSController.INDEX_SERVICE,
							 new String[]{libFilename});

		flushMessage = new FlushIndexMessage (RSController.LIB_SERVICE, RSController.INDEX_SERVICE);
		
		archivingResponse = new ArchivingDoneResponseMessage(RSController.LIB_SERVICE,
								     source,
								     filename, libFilename, SUCCESS_MSG);
		sendInvalidateCacheMessage(MediaLib.getTableName(dummyObject), CACHE_INSERT, new String[]{filename});
	    } else {
		throw new Exception("MediaLib update failed");
	    }
	} catch(Exception e) {
	    archivingResponse = new ArchivingDoneResponseMessage(RSController.LIB_SERVICE,
								 source,
								 filename, libFilename, FAILURE_MSG,
								 RSController.ERROR_DB_FAILED);
	}

	if (sendMessage(archivingResponse) < 0)
	    logger.debug("HandleArchivingDone: Failed to send ArchivingDoneResponse message.");
	else
	    logger.debug("HandleArchivingdone: ArchivingDoneResponseMessage sent.");

	pendingEncodes = true;
	
	if (updateMessage != null){
	    final UpdateProgramMessage upMessage = updateMessage;
	    final FlushIndexMessage flushIndexMessage = flushMessage;
	    Thread t = new Thread("HandleArchivingDone:UpdateProgram"){
		    public void run(){
			try{
			    sendSyncCommand(upMessage);
			    logger.info("ArchivingDone: Added new entries in index: "+libFilename);
			} catch (Exception e){
			    logger.error("ArchivingDone: Unable to send UpdateProgramMessage for index update.",e);
			}
			
			try{
			    sendSyncCommand(flushIndexMessage);
			} catch (Exception e) {
			    logger.error("ArchivingDone: Unable to send FlushIndexMessage to Index Service", e);
			}
		    }
		};
	    t.start();
	}
	logger.debug("HandleArchivingDone: Leaving");
	*/
    }

    protected void handleStatUpdate(String source, PlayoutHistory playoutHistory) {
	logger.debug("HandleStatUpdate: Entering: " + playoutHistory);
	RadioProgramStatUpdateResponseMessage statResponse;
	if (medialib.insert(playoutHistory) > -1) {
	    logger.debug("HandleStatUpdate: Statistics update successful.");
	    statResponse = new RadioProgramStatUpdateResponseMessage(RSController.LIB_SERVICE, source, SUCCESS_MSG);
	    sendInvalidateCacheMessage(MediaLib.getTableName(playoutHistory), CACHE_INSERT, new String[]{playoutHistory.getItemID()});
	}
	else {
	    logger.debug("HandleStatUpdate: Statistics update failed.");
	    statResponse = new RadioProgramStatUpdateResponseMessage(RSController.LIB_SERVICE, source, FAILURE_MSG);
	}
	if (sendMessage(statResponse)<0)
	    logger.debug("HandleStatUpdate: Failed to send statResponse message.");
	else
	    logger.debug("HandleStatUpdate: statResponse sent.");

	logger.debug("HandleStatUpdate: Leaving");
    }

    protected void handleRadioProgramGetAudio(String source, String audioFilename, String method) {
	logger.debug("HandleRadioProgramGetAudio: Entering Source= "+source+" audiofile= "+audioFilename+" method= "+method);
	String retUrl = "";
	if (method.equals(RadioProgramGetAudioMessage.STREAMING))
	    retUrl = medialib.getStreamUrl(audioFilename);
	else
	    retUrl = medialib.getDownloadUrl(audioFilename);

	RadioProgramGetAudioResponseMessage audioResponse;		
	if (retUrl != null && !retUrl.equals("")) {
	    logger.debug("HandleRadioProgramGetAudio: URL="+retUrl+" successfully retrieved.");
	    audioResponse = 
		new RadioProgramGetAudioResponseMessage(RSController.LIB_SERVICE, source, 
							retUrl, SUCCESS_MSG);
	} else {
	    logger.debug("HandleRadioProgramGetAudio: Failed to get url.");
	    audioResponse = 
		new RadioProgramGetAudioResponseMessage(RSController.LIB_SERVICE, source, 
							"", FAILURE_MSG);
	}
	if (sendMessage(audioResponse)<0)
	    logger.debug("HandleRadioProgramGetAudio: Failed to send audioResponse message.");
	else
	    logger.debug("HandleRadioProgramGetAudio: audioResponse sent.");

	logger.debug("HandleRadioProgramGetAudio: Leaving");
    }

    protected void handleRegisterMessage(String source, String widgetName) {
	logger.debug("HandleRegisterMessage: Entering source = " + source + " widgetName = " + widgetName);

	String currProviders = cacheProviders.get(source);
	if (currProviders == null)
	    currProviders = widgetName;
	else {
	    if (currProviders.indexOf(widgetName) == -1)
		currProviders = currProviders + "," + widgetName;
	}
	cacheProviders.put(source, currProviders);

	RegisterCacheProviderAckMessage ackMessage = new RegisterCacheProviderAckMessage(RSController.LIB_SERVICE, source, widgetName, SUCCESS_MSG);
	sendMessage(ackMessage);

	logger.debug("HandleRegisterMessage: Leaving");
    }

    protected void handleInvalidateCacheMessage(String objectType, String actionType, String[] objectIDs) {
	logger.debug("HandleInvalidateCacheMessage: Entering objectType = " + objectType + " actionType = " + actionType);

	Enumeration<String> e = cacheProviders.keys();
	while (e.hasMoreElements()) {
	    String serviceName = e.nextElement();
	    InvalidateCacheMessage invalidateMessage = new InvalidateCacheMessage(RSController.LIB_SERVICE, serviceName,
										  StringUtilities.getArrayFromCSV(cacheProviders.get(serviceName)), 
										  objectType, actionType, objectIDs);
	    sendMessage(invalidateMessage);
	}

	logger.debug("HandleInvalidateCacheMessage: Leaving");
    }

    protected void handleInvalidateCacheKeyMessage(String objectType, String actionType, String[] oldObjectIDs, String[] newObjectIDs) {
	logger.debug("HandleInvalidateCacheKeyMessage: Entering objectType = " + objectType + " actionType = " + actionType);
	
	/*Enumeration<String> e = cacheProviders.keys();
	while (e.hasMoreElements()) {
	    String serviceName = e.nextElement();
	    InvalidateCacheKeyMessage invalidateMessage = new InvalidateCacheKeyMessage(RSController.LIB_SERVICE, serviceName,
											StringUtilities.getArrayFromCSV(cacheProviders.get(serviceName)), 
											objectType, actionType, oldObjectIDs, newObjectIDs);
	    sendMessage(invalidateMessage);
	    }*/

	ArrayList<String> oldDataList = new ArrayList<String>(Arrays.asList(oldObjectIDs));
	ArrayList<String> newDataList = new ArrayList<String>();
	for (String id : newObjectIDs) {
	    if (!id.equals(CACHE_DELETE)) {
		newDataList.add(id);
	    }
	}
	
	updatePrograms(oldDataList, newDataList);

	int numItemsPerMessage = 11; //because there are 11 items per page in library widget
	ArrayList<String> tmpOldIDs = new ArrayList<String>();
	ArrayList<String> tmpNewIDs = new ArrayList<String>();

	for (String dest : persistentCacheProviders) {
	    for (int i = 0; i < oldObjectIDs.length; i += numItemsPerMessage) {
		for (int j = 0; j < numItemsPerMessage; j++) {
		    if (i+j >= oldObjectIDs.length)
			break;
		    tmpOldIDs.add(oldObjectIDs[i+j]);
		    tmpNewIDs.add(newObjectIDs[i+j]);
		}
		
		InvalidateCacheKeyMessage invalidateMessage = new InvalidateCacheKeyMessage(RSController.LIB_SERVICE, 
											    dest,
											    new String[]{}, 
											    objectType, 
											    actionType, 
											    tmpOldIDs.toArray(new String[]{}), 
											    tmpNewIDs.toArray(new String[]{}));
		sendMessage(invalidateMessage);
		tmpOldIDs.clear();
		tmpNewIDs.clear();
	    }
	}

	logger.debug("HandleInvalidateCacheKeyMessage: Leaving");
    }

    protected void handleMachineStatusRequestMessage(String widget, String command, String[] options){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = ((StationNetwork)stationConfiguration.getStationNetwork()).getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), RSController.UI_SERVICE, widget, command, new String[] {dirName, used, available});
	    
	    if (sendMessage(responseMessage) == -1){
		logger.error("HandleMachineStatusRequestMessage: Unable to send responseMessage.");
		connectionError();
	    } else {
		logger.debug("HandleMachineStatusRequestMessage: Response sent.");
	    }
	}
    }


    protected void sendInvalidateCacheMessage(String objectType, String actionType, String[] objectIDs) {
	// short circuit, since going from lib-service to lib-service
	handleInvalidateCacheMessage(objectType, actionType, objectIDs);
    }

    protected void sendUpdateIndexMessage(){
	UpdateIndexMessage indexMessage = new UpdateIndexMessage(RSController.LIB_SERVICE, RSController.INDEX_SERVICE);
	sendMessage(indexMessage);
    }

    public String getName() {
	return RSController.LIB_SERVICE;
    }

    public static void main(String args[]) {
	Getopt g = new Getopt("LibService", args, "c:m:");
	int c;
	String configFilePath = "./automation.conf";
	String machineID = "127.0.0.1";
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c':
		configFilePath = g.getOptarg();
		break;
	    case 'm':
		machineID = g.getOptarg();
		break;
	    }
	}


	StationConfiguration stationConfiguration = new StationConfiguration(machineID);
	stationConfiguration.readConfig(configFilePath);

	LibService libService = new LibService(stationConfiguration,
					       stationConfiguration.getServiceInstanceOnMachine(RSController.LIB_SERVICE, machineID),
					       machineID);
    }

    protected int sendMessage(IPCMessage ipcMessage) {

	if (ipcServer == null)
	    return -1;

	int retVal = ipcServer.handleOutgoingMessage(ipcMessage);
	if (retVal == -1) {
	    logger.error("SendMessage: Unable to send message " + ipcMessage.getClass());
	    connectionError();
	} else if (retVal == -2) {
	    logger.error("SendMessage: Unable to send message " + ipcMessage.getClass());
	    // Do nothing
	}
	return retVal;
    }

    protected IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null){
	    return null;
	}
	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
	    connectionError();
	    logger.error("SendSyncCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	}
	return responseMessage;
    }

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");

	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }

}
