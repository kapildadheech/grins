package org.gramvaani.radio.rscontroller.services;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.messages.libmsgs.*;
import org.gramvaani.radio.rscontroller.messages.indexmsgs.*;
import org.gramvaani.radio.timekeeper.*;
import org.gramvaani.linkmonitor.*;
import org.gramvaani.radio.diagnostics.*;
import org.gramvaani.radio.diagnostics.DiagnosticUtilities.DiskSpace;

import java.io.*;
import java.util.*;

import gnu.getopt.Getopt;

public class IndexService implements IPCNode, IPCNodeCallback, LinkMonitorClientListener, DefaultUncaughtExceptionCallback, DiagnosticUtilitiesConnectionCallback, MediaLibListener {
    protected IPCServer ipcServer;
    protected StationConfiguration stationConfiguration;
    protected String machineID;
    protected String serviceInstanceName;
    protected LogUtilities logger;
    protected MediaLib medialib;
    protected IndexLib indexlib;
    protected TimeKeeper timeKeeper;
    protected LinkMonitorClient linkMonitor = null;
    protected DefaultUncaughtExceptionHandler defaultUncaughtExceptionHandler;
    protected DiagnosticUtilities diagUtil;
    protected Boolean libServiceUp = false;
    protected String registrationID;
    protected String resourceManagerID = "";
    protected Boolean isIndexServiceReady = false;
    protected Boolean networkDown = false;

    public IndexService(IPCServer ipcServer, StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {

	this.ipcServer = ipcServer;
	this.machineID = machineID;
	this.stationConfiguration = stationConfiguration;
	this.serviceInstanceName = serviceInstanceName;
	logger = new LogUtilities(RSController.INDEX_SERVICE);
	logger.debug("IndexService: Entering. Initializing with machineID="+machineID);
	logger.info("IndexService: Initializing");
	registrationID = GUIDUtils.getGUID();
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	MediaLib.registerListener(RSController.INDEX_SERVICE, this);
	MediaLib.setIPCServer(ipcServer);
	checkStatus();
	ipcServer.registerIPCNode(RSController.INDEX_SERVICE, this, registrationID);
	logger.debug("IndexService: Leaving");
    }

    public IndexService(StationConfiguration stationConfiguration, String serviceInstanceName, String machineID) {

	this.stationConfiguration = stationConfiguration;
	this.machineID = machineID;
	this.serviceInstanceName = serviceInstanceName;
        ipcServer = new IPCServerStub(stationConfiguration.getStringParam(StationConfiguration.IPC_SERVER),
				      stationConfiguration.getIntParam(StationConfiguration.IPC_SERVER_PORT),
				      stationConfiguration.getClassParam(StationConfiguration.MESSAGE_FACTORY),
				      stationConfiguration.getIntParam(StationConfiguration.SYNC_MESSAGE_TIMEOUT),
				      stationConfiguration.getStringParam(StationConfiguration.PERSISTENT_QUEUE_DIR));
	logger = new LogUtilities(RSController.INDEX_SERVICE);
	logger.debug("IndexService: Entering. Initializing with machineID="+machineID);
	logger.info("IndexService: Initializing.");

	defaultUncaughtExceptionHandler = new DefaultUncaughtExceptionHandler(this, "IndexService");
	Thread.setDefaultUncaughtExceptionHandler(defaultUncaughtExceptionHandler);
	registrationID = GUIDUtils.getGUID();
	diagUtil = DiagnosticUtilities.getDiagnosticUtilities(stationConfiguration, ipcServer);
	MediaLib.registerListener(RSController.INDEX_SERVICE, this);
	MediaLib.setIPCServer(ipcServer);
	checkStatus();
	timeKeeper = TimeKeeper.getRemoteTimeKeeper(ipcServer, this, this, stationConfiguration);
	if (ipcServer.registerIPCNode(RSController.INDEX_SERVICE, this, registrationID) == -1) {
	    logger.error("IndexService: Unable to connect to ipcServer.");
	    connectionError();
	}
	
	linkMonitor = new LinkMonitorClient(this, RSController.INDEX_SERVICE, stationConfiguration);
	linkMonitor.start();
	logger.debug("IndexService: Leaving");
    }
    
    public synchronized void activateMediaLib(boolean activate){
	if (activate){
	} else {
	    if (isIndexServiceReady){
		//isIndexServiceReady = false;
		logger.error("ActivateMediaLib: Database connection error. We may not recover from this.");
		//Flash the error somewhere visible
		sendReadyMessage(false);
	    }
	}
    }

    protected boolean checkStatus() {
	boolean status = false;
	medialib = MediaLib.getMediaLib(stationConfiguration, timeKeeper);	
	if (medialib != null) {
	    indexlib = IndexLib.getIndexLib(stationConfiguration, logger, medialib);
	    if (indexlib != null) {
		indexlib.setIPCServer(ipcServer);
		status = true;
	    }
	}
	logger.info("CheckStatus: Returning "+status);
	return status;
    }

    public synchronized void handleUncaughtException(Thread t,Throwable ex){
	logger.debug("HandleUncaughtException: Exiting..");
	//System.exit(-1);
    }
    

    public void serviceDown(String service){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
    }

    public synchronized IPCServer attemptReconnect(){
	//if (ipcServer != null){
	if (ipcServer.registerIPCNode(getName(), this, registrationID, true) == 0) {
	    MediaLib.setIPCServer(ipcServer);
	    timeKeeper.setIPCServer(ipcServer);
	    linkMonitor.reset();
	    checkStatus();
	    return ipcServer;
	} else {
	    return null;
	}
    }
    
    //XXX: make this use sendmessage.
    protected int sendReadyMessage(Boolean readiness) {
	ServiceReadyMessage readyMessage = new ServiceReadyMessage(RSController.INDEX_SERVICE, 
								   RSController.RESOURCE_MANAGER,
								   readiness);

	/*int retVal = sendMessage(readyMessage);
	if (retVal != -1) {
	    isIndexServiceReady = readiness;
	}
	
	return retVal;*/

	try {
	    if (sendSyncCommand(readyMessage) == null) {
		logger.error("SendReadyMessage: Sending ready message failed.");
		return -1;
	    } else {
		isIndexServiceReady = readiness;
		logger.debug("SendReadyMessage: Ready messsage sent. isIndexServiceReady=" + isIndexServiceReady);
		return 0;
	    }
	} catch(Exception e) {
	    logger.error("SendReadyMessage: Message timeout exception.");
	    return -1;
	}
    }

    protected IPCSyncResponseMessage sendSyncCommand(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	IPCSyncResponseMessage responseMessage = null;

	if (ipcServer == null){
	    return null;
	}
	if ((responseMessage = ipcServer.handleOutgoingSyncMessage(message)) == null) {
	    logger.error("SendSyncCommand: Unable to send message to: "+message.getDest()+" Type: "+message.getMessageClass());
	    connectionError();
	}
	return responseMessage;
    }

    public synchronized void connectionError() {
	logger.debug("ConnectionError: Entering");
	logger.error("ConnectionError: ");
	networkDown = true;
	timeKeeper.setIPCServer(null);
	if (ipcServer != null)
	    ipcServer.disconnect(serviceInstanceName);
	diagUtil.serviceConnectionError(this);
	logger.debug("ConnectionError: Leaving");
    }

    public int handleNonBlockingMessage(IPCMessage message){
	logger.debug("HandleNonBlockingMessage: From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof HeartbeatMessage){
	    if (linkMonitor != null){
		HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
		linkMonitor.handleMessage(heartbeatMessage);
	    }
	}
	return 0;
    }

    public synchronized int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering. From: "+message.getSource()+" Type: "+message.getMessageClass());
	if (message instanceof TimeKeeperResponseMessage) {
	    timeKeeper.handleTimeKeeperResponseMessage((TimeKeeperResponseMessage)message);
	} else if (message instanceof HeartbeatMessage && linkMonitor != null){
	    HeartbeatMessage heartbeatMessage = (HeartbeatMessage) message;
	    linkMonitor.handleMessage(heartbeatMessage);
	} else if (message instanceof ServiceErrorRelayMessage) {
	    ServiceErrorRelayMessage errorRelayMessage = (ServiceErrorRelayMessage) message;
	    if (errorRelayMessage.getServiceName().equals(RSController.LIB_SERVICE)){
		logger.debug("HandleIncomingMessage: ServiceErrorRelayMessage received for "+RSController.LIB_SERVICE);
		if (isIndexServiceReady)
		    if (!checkStatus())
			sendReadyMessage(false);
	    }
	} else if (message instanceof ServiceActivateNotifyMessage) {
	    ServiceActivateNotifyMessage notifyMessage = (ServiceActivateNotifyMessage)message;
	    if (notifyMessage.getActivatedServiceName().equals(RSController.UI_SERVICE)) {
		timeKeeper.sync();
	    }else if (notifyMessage.getActivatedServiceName().equals(RSController.LIB_SERVICE)) {
		if (!isIndexServiceReady){
		    if (checkStatus()){
			sendReadyMessage(true);
		    }
		    else {
			logger.error("HandleIncomingMessage: Database connection failed inspite of lib service being ready. May not recover from this error");
			//XXX Flash error somewhere visible.
		    }
		}
	    }
	} else if (message instanceof ServiceNotifyMessage) {
	    ServiceNotifyMessage notifyMessage = (ServiceNotifyMessage)message;
	    String[] services = notifyMessage.getActivatedServices();
	    for (String service : services){
		if (service.equals(RSController.LIB_SERVICE)) {
		    if (!isIndexServiceReady){
			if (checkStatus()){
			    sendReadyMessage(true);
			}
			else {
			    logger.error("HandleIncomingMessage: Database connection failed inspite of lib service being ready. May not recover from this error");
			    //XXX Flash error somewhere visible.
			}
		    }
		}
	    } 
	} else if (message instanceof AddAnchorTermMessage) {
	    AddAnchorTermMessage anchorMessage = (AddAnchorTermMessage)message;
	    handleAddAnchorTermMessage(anchorMessage.getAnchorID(), anchorMessage.getFilterID(), anchorMessage.getAnchorTerm(),
				       anchorMessage.getMessageID(), anchorMessage.getSource());
	} else if (message instanceof UpdateProgramMessage) {
	    UpdateProgramMessage updateMessage = (UpdateProgramMessage)message;
	    handleUpdateProgramMessage(updateMessage.getProgramIDs(), updateMessage.getMessageID(), updateMessage.getSource(), updateMessage.getUpdateSerially());
	} else if (message instanceof RemoveProgramMessage) {
	    RemoveProgramMessage removeMessage = (RemoveProgramMessage)message;
	    handleRemoveProgramMessage(removeMessage.getProgramIDs(), removeMessage.getMessageID(), removeMessage.getSource(), removeMessage.getRemoveSerially());
	} else if (message instanceof AnchorSearchMessage) {
	    AnchorSearchMessage anchorMessage = (AnchorSearchMessage)message;
	    handleAnchorSearchMessage(anchorMessage.getAnchorText(), anchorMessage.getMessageID(), anchorMessage.getSource());
	} else if (message instanceof ProgramSearchMessage) {
	    ProgramSearchMessage programMessage = (ProgramSearchMessage)message;
	    handleProgramSearchMessage(programMessage.getQueryString(), programMessage.getWhereClause(), programMessage.getJoinTableStr(),
				       programMessage.getStartingOffset(), programMessage.getMaxResults(),
				       programMessage.getMessageID(), programMessage.getSource());
	} else if (message instanceof FlushIndexMessage) {
	    FlushIndexMessage flushMessage = (FlushIndexMessage)message;
	    handleFlushIndexMessage(flushMessage.getMessageID(), flushMessage.getSource(), flushMessage.flushSerially());
	} else if (message instanceof UpdateIndexMessage) {
	    handleUpdateIndexMessage();
	} else if (message instanceof RefreshIndexMessage){
	    handleRefreshIndexMessage();
	} else if (message instanceof RegistrationAckMessage) {
	    RegistrationAckMessage registrationAckMessage = (RegistrationAckMessage)message;
	    handleRegistrationAckMessage(registrationAckMessage.getAck(), registrationAckMessage.getRegistrationID());
	} else if (message instanceof GraphStatsMessage) {
	    GraphStatsMessage statsMessage = (GraphStatsMessage)message;
	    handleGraphStatsMessage(statsMessage.getQueryString(), statsMessage.getWhereClause(), statsMessage.getJoinTableStr(),
				    statsMessage.getMinTelecastTime(), statsMessage.getMaxTelecastTime(), statsMessage.getGraphType(),
				    statsMessage.getMessageID(), statsMessage.getSource());
	} else if (message instanceof MachineStatusRequestMessage){
	    MachineStatusRequestMessage requestMessage = (MachineStatusRequestMessage)message;
	    handleMachineStatusRequestMessage(requestMessage.getWidgetName(), requestMessage.getCommand(), requestMessage.getOptions());
	}

	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }
    
    protected void handleRegistrationAckMessage(boolean ack, String id){
	String oldID = resourceManagerID;
	boolean sendMessage = false;
	resourceManagerID = id;
	if (ack) {
	    ipcServer.sendPersistentMessages(RSController.RESOURCE_MANAGER);	    
	    if (networkDown)
		networkDown = false;

	    if (oldID.equals("")) {
		sendMessage = true;
	    } else if (!oldID.equals(id)) {
		logger.error("HandleRegistrationAckMessage: RSController went down. Resending service interests.");
		sendMessage = true;
	    } else if (!isIndexServiceReady) {
		if (checkStatus()) {
		    sendReadyMessage(true);
		}
	    }
	    if (sendMessage){
		
		ServiceInterestMessage serviceInterestMessage = 
		    new ServiceInterestMessage(serviceInstanceName, 
					       RSController.RESOURCE_MANAGER,
					       "", new String[]{RSController.LIB_SERVICE});
		sendMessage(serviceInterestMessage);

		if (checkStatus()){
		    sendReadyMessage(true);
		}
	    }
	} else {
	    resetService();
	}
    }

    protected void resetService(){
	registrationID = GUIDUtils.getGUID();
	resourceManagerID = "";
	RegisterMessage message = new RegisterMessage(serviceInstanceName, registrationID);
	sendMessage(message);
    }

    protected void handleAddAnchorTermMessage(String anchorID, String filterID, String anchorTerm, String messageID, String source) {
	int retval = indexlib.addAnchorTerm(anchorID, filterID, anchorTerm);
	AddAnchorTermResponseMessage responseMessage = new AddAnchorTermResponseMessage(RSController.INDEX_SERVICE, source, messageID, retval);
	if (sendMessage(responseMessage) < 0)
	    logger.error("handleAddAnchorTermMessage: unable to send response message");
	else
	    logger.info("handleAddAnchorTermMessage: response sent");
    }

    protected void handleUpdateProgramMessage(String[] programIDs, String messageID, String source, boolean serialize) {
	int[] retVals = new int[programIDs.length];
	for (int i = 0; i < programIDs.length; i++){
	    retVals[i] = indexlib.updateProgram(programIDs[i], serialize);
	}

	UpdateProgramResponseMessage responseMessage = new UpdateProgramResponseMessage(RSController.INDEX_SERVICE, source, messageID, retVals);
	if (sendMessage(responseMessage) < 0)
	    logger.error("handleUpdateProgramMessage: unable to send response message");
	else
	    logger.info("handleUpdateProgramMessage: response sent");
    }

    protected void handleRemoveProgramMessage(String[] programIDs, String messageID, String source, boolean removeSerially){
	int[] retVals = new int[programIDs.length];
	for (int i = 0; i < programIDs.length; i++){
	    retVals[i] = indexlib.removeProgram(programIDs[i], removeSerially);
	}

	RemoveProgramResponseMessage responseMessage = new RemoveProgramResponseMessage(RSController.INDEX_SERVICE, source, messageID, retVals);
	if (sendMessage(responseMessage) < 0)
	    logger.error("handleRemoveProgramMessage: unable to send response message.");
	else
	    logger.info("handleRemoveProgramMessage: response sent.");
    }

    protected void handleAnchorSearchMessage(String anchorText, String messageID, String source) {
	Hashtable<String, ArrayList<String>> searchResponse = indexlib.searchAnchorIndex(anchorText);
	AnchorSearchResponseMessage responseMessage = new AnchorSearchResponseMessage(RSController.INDEX_SERVICE, source, searchResponse, messageID);
	if (sendMessage(responseMessage) < 0)
	    logger.error("handleAnchorSearchMessage: unable to send response message");
	else
	    logger.info("handleAnchorSearchMessage: response sent");	    
    }

    protected void handleProgramSearchMessage(String queryString, String whereClause, String joinTableStr,
					      int startingOffset, int maxResults, String messageID, String source) {
	String[] searchResponse = indexlib.searchProgramIndex(queryString, whereClause, joinTableStr, startingOffset, maxResults);
	int numSearchResults = indexlib.numSearchResults(queryString, whereClause, joinTableStr);
	ProgramSearchResponseMessage responseMessage = new ProgramSearchResponseMessage(RSController.INDEX_SERVICE, source, searchResponse, 
											numSearchResults, messageID);
	if (sendMessage(responseMessage) < 0)
	    logger.error("handleProgramSearchMessage: unable to send response message");
	else
	    logger.info("handleProgramSearchMessage: response sent");
    }

    protected void handleFlushIndexMessage(String messageID, String source, boolean flushSerially) {
	int retval = indexlib.flushProgramIndex(flushSerially);
	FlushIndexResponseMessage flushResponseMessage = new FlushIndexResponseMessage(RSController.INDEX_SERVICE, 
										       source, messageID, retval);
	
	//indexlib.dumpProgramIndex();

	if (sendMessage(flushResponseMessage) < 0)
	    logger.error("handleFlushIndexMessage: unable to send response message");
	else
	    logger.info("handleFlushIndexMessage: response sent");
    }

    protected void handleUpdateIndexMessage() {
	if (medialib == null || indexlib == null) {
	    logger.error("HandleUpdateIndexMessage: Unable to update. MediaLib: "+medialib+" IndexLib: "+indexlib);
	    return;
	}

	RadioProgramMetadata queryProgram = new RadioProgramMetadata("");
	queryProgram.setIndexed(LibService.MEDIA_NOT_INDEXED);
	RadioProgramMetadata[] programs = medialib.get(queryProgram);
       
	int success = 0, failed = 0;
	if (programs != null){
	    for (RadioProgramMetadata program: programs){
		if (indexlib.updateProgram(program.getItemID()) >=0)
		    success++;
		else
		    failed++;
	    }
	    indexlib.flushProgramIndex();
	}
	logger.info("HandleUpdateIndexMessage: Updated index. "+success+" succeeded. "+failed+" failed.");
    }

    protected void handleRefreshIndexMessage(){
	if (medialib == null || indexlib == null){
	    logger.error("HandleRefreshIndexMessage: Unable to refresh. Medialib: "+medialib+" IndexLib: "+indexlib);
	    return;
	}
	indexlib.refreshIndex();
    }

    protected void handleGraphStatsMessage(String queryString, String whereClause, String joinTableStr,
					   long minTelecastTime, long maxTelecastTime, int graphType, String messageID, String source) {
	GraphStats graphStats = indexlib.getGraphStats(queryString, whereClause, joinTableStr, minTelecastTime, maxTelecastTime, graphType);
	GraphStatsResponseMessage responseMessage = 
	    new GraphStatsResponseMessage(RSController.INDEX_SERVICE, source, 
					  graphStats.getMinTelecastTime(), graphStats.getMaxTelecastTime(),
					  graphStats.getXLabels(), graphStats.getDataItems(),
					  graphStats.getApproxBinInterval(), graphStats.getMaxDataItem(),
					  messageID);
	if (sendMessage(responseMessage) < 0)
	    logger.error("handleGraphStatsMessage: unable to send response message");
	else
	    logger.info("handleGraphStatsMessage: response sent");
    }


    protected void handleMachineStatusRequestMessage(String widget, String command, String[] options){
	DiskSpace diskSpace;
	String dirName, used, available;
	
	if (command.equals(DiagnosticUtilities.DISK_SPACE_MSG)){
	    dirName = ((StationNetwork)stationConfiguration.getStationNetwork()).getLocalStoreDir();
	    if (dirName == null) {
		logger.error("HandleMachineStatusRequestMessage: No local store directory found.");
		dirName = "";
		used = "0";
		available = "0";
	    } else {
		diskSpace = diagUtil.getDiskSpace(dirName);
		used = Long.toString(diskSpace.getUsedSpace());
		available = Long.toString(diskSpace.getAvailableSpace());
	    }

	    MachineStatusResponseMessage responseMessage = new MachineStatusResponseMessage(getName(), RSController.UI_SERVICE, widget, command, new String[] {dirName, used, available});
	    
	    if (sendMessage(responseMessage) == -1){
		logger.error("HandleMachineStatusRequestMessage: Unable to send responseMessage.");
		connectionError();
	    } else {
		logger.debug("HandleMachineStatusRequestMessage: Response sent.");
	    }
	}
    }


    public String getName() {
	return RSController.INDEX_SERVICE;
    }

    public static void main(String args[]) {
	Getopt g = new Getopt("IndexService", args, "c:m:");
	int c;
	String configFilePath = "./automation.conf";
	String machineID = "127.0.0.1";
	while ((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c':
		configFilePath = g.getOptarg();
		break;
	    case 'm':
		machineID = g.getOptarg();
		break;
	    }
	}


	StationConfiguration stationConfiguration = new StationConfiguration(machineID);
	stationConfiguration.readConfig(configFilePath);


	IndexService indexService = new IndexService(stationConfiguration,
						     stationConfiguration.getServiceInstanceOnMachine(RSController.INDEX_SERVICE, machineID), machineID);
    }

    protected int sendMessage(IPCMessage ipcMessage) {

	if (ipcServer == null)
	    return -1;

	int retVal = ipcServer.handleOutgoingMessage(ipcMessage);
	if (retVal == -1) {
	    logger.error("SendMessage: Unable to send message " + ipcMessage.getClass());
	    connectionError();
	} else if (retVal == -2) {
	    logger.error("SendMessage: Unable to send message " + ipcMessage.getClass());
	    // Do nothing
	}
	return retVal;
    }

    public void linkUp(){
	logger.info("LinkUp:");
    }

    public void linkDown(){
	logger.error("LinkDown:");
	connectionError();
    }

    public int sendHeartbeatMessage(HeartbeatMessage message){
	logger.debug("SendHeartbeatMessage:");

	if (ipcServer != null)
	    return ipcServer.handleOutgoingMessage(message);
	else
	    return -1;
    }

}
