package org.gramvaani.radio.rscontroller;

import org.gramvaani.simpleipc.IPCMessage;
import org.gramvaani.utilities.LogUtilities;

import java.util.Hashtable;
import java.lang.reflect.Method;

public class MessageHandlerUtility {
    static final Hashtable<Class<?>, Hashtable<Class<? extends IPCMessage>, Method>> masterHash = new Hashtable<Class<?>, Hashtable<Class<? extends IPCMessage>, Method>>();

    static final LogUtilities logger = new LogUtilities("MessageHandlerUtility");

    @SuppressWarnings("unchecked")
    protected static Hashtable<Class<? extends IPCMessage>, Method> buildMethodHash(Class<?> klass){
	Hashtable<Class<? extends IPCMessage>, Method> methodHash = new Hashtable<Class<? extends IPCMessage>, Method>();

	for (Method method: klass.getDeclaredMethods()){
	    if (method.isAnnotationPresent(MessageHandler.class)){
		Class<?>[] types = method.getParameterTypes();
		if (types.length != 1){
		    LogUtilities.getDefaultLogger().error("BuildMethodHash: "+klass.getSimpleName() + ": num params: "+types.length);
		    continue;
		}
		method.setAccessible(true);
		methodHash.put((Class<? extends IPCMessage>)types[0], method);
	    }
	}

	return methodHash;
    }

    public static void handleMessage(Object obj, IPCMessage message){
	Class objClass = obj.getClass();
	Class<? extends IPCMessage> messageClass = message.getClass();

	Hashtable<Class<? extends IPCMessage>, Method> methodHash;
	if ((methodHash = masterHash.get(objClass)) == null){
	    methodHash = buildMethodHash(objClass);
	    masterHash.put(objClass, methodHash);
	}

	Method method = methodHash.get(messageClass);
	try {
	    if (method == null){
		logger.warn("HandleMessage: No handler for "+objClass.getSimpleName()+":"+messageClass.getSimpleName());
	    } else {
		method.invoke(obj, message);
	    }
	} catch (Exception e){
	    logger.error(objClass.getSimpleName()+":HandleMessage:"+messageClass.getSimpleName(), e);
	}
    }
}