package org.gramvaani.radio.timekeeper;

import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.utilities.*;

public class TimeKeeper implements Runnable {
    protected boolean isLocal;
    protected long clockSkew, lastRefreshTime;
    protected StationConfiguration stationConfig;
    protected IPCNode ipcNode;
    protected IPCNodeCallback ipcNodeCallback;
    protected IPCServer ipcServer;
    protected LogUtilities logger;
    protected Thread timeThread = null;

    /* Now using Thread.wait and notify
      private SleepSynchronizer sleepSynchronizer;
    */

    private long realTime = -1;

    public TimeKeeper(IPCNode ipcNode, IPCNodeCallback ipcNodeCallback, IPCServer ipcServer, 
		      StationConfiguration stationConfig, boolean isLocal) {
	clockSkew = 0;
	this.ipcNode = ipcNode;
	this.ipcNodeCallback = ipcNodeCallback;
	this.ipcServer = ipcServer;
	this.stationConfig = stationConfig;
	logger = new LogUtilities("TimeKeeper");
	/* Now using Thread.wait and notify
	  this.sleepSynchronizer = new SleepSynchronizer();
	*/
	this.isLocal = isLocal;
	logger.debug("TimeKeeper: Initialized");
    }

    public synchronized void sync() {
	if (timeThread == null) {
	    timeThread = new Thread(this, "TimeKeeper thread");
	    timeThread.start();
	} else {
	    if (System.currentTimeMillis() - lastRefreshTime > 1000) {
		timeThread.interrupt();
	    }
	}
    }

    public void run() {
	while(true) {
	    TimeKeeperRequestMessage timeKeeperMessage = new TimeKeeperRequestMessage(ipcNode.getName(),
										      RSController.UI_SERVICE);
	    if(sendMessage(timeKeeperMessage)<0)
		logger.debug("Run: Failed to send timeKeeperMessage.");
	    else
		logger.debug("Run: timeKeeperMessage sent successfully.");
		
	    long prevTime = System.currentTimeMillis();
	    long currTime = -1;
	    realTime = -1;
	    while(realTime == -1 && ((currTime = System.currentTimeMillis()) - prevTime < 1000)) {
		try {
		    logger.debug("Run: About to begin wait: " + currTime);
		    synchronized(this) {
			wait(1000 - (currTime - prevTime));
		    }
		} catch(InterruptedException e) {
		    logger.debug("Run: Wait interrupted: " + System.currentTimeMillis());
		}
	    }
		
	    if(realTime > -1) {
		logger.debug("Run: Wait successful: " + System.currentTimeMillis());
		clockSkew = realTime - getLocalTime();
		lastRefreshTime = realTime - clockSkew;
		logger.debug("Run: Wait successful: " + System.currentTimeMillis() + " clockSkew="+clockSkew+" lastRefreshTime="+lastRefreshTime);
	    } else {
		logger.debug("Run: Wait timeout: " + System.currentTimeMillis());
	    }
	    
	    try {
		Thread.sleep(stationConfig.getLongParam(StationConfiguration.CLOCK_SYNC_TIMEOUT));
	    } catch(InterruptedException e) {
		// do nothing
	    }
	}
    }

    public synchronized long getLocalTime() {
	return System.currentTimeMillis();
    }

    public synchronized long getRealTime() {
	if(isLocal) {
	    return getLocalTime();
	} else {
	    /* Now using Thread.wait and notify
	       sleepSynchronizer.putToSleep();
	       // wait until interruption
	       if(sleepSynchronizer.success()) {
	       long realTime = sleepSynchronizer.realTime();
	       sleepSynchronizer.reset();
	       currentTime = getLocalTime();
	       lastRefresh = currentTime;
	       
	       clockSkew = realTime - currentTime;
	       return realTime;
	       }
	    */
	    
	    return getLocalTime() + clockSkew;
	}
    }

    public synchronized void setIPCServer(IPCServer ipcServer){
	this.ipcServer = ipcServer;
    }

    protected int sendMessage(IPCMessage message) {
	int retVal = -2;
	if(ipcServer != null)
	    retVal = ipcServer.handleOutgoingMessage(message);
	if(retVal == -1) {
	    logger.error(ipcNode.getName() + ": Unable to send message " + message.getClass());
	    ipcNodeCallback.connectionError();
	    return -1;
	} else if(retVal == -2) {
	    logger.error(ipcNode.getName() + ": Unable to send message " + message.getClass());
	    // Do nothing
	}

	return 0;
    }

    /* Now using Thread.wait and notify
    class SleepSynchronizer {
	boolean sleeping = false;
	Thread currentThread = null;
	boolean success = false;
	long realTime = -1;

	public void putToSleep() {
	    reset();
	    sleeping = true;
	    success = false;
	    try {
		currentThread = Thread.currentThread();
		currentThread.sleep(1000);
	    } catch(Exception e) {
		sleeping = false;
	    }
	}

	public synchronized void wakeUp(long realTime) {
	    if(sleeping) {
		currentThread.interrupt();
		success = true;
		this.realTime = realTime;
	    } else {
		success = false;
	    }
	}

	public synchronized boolean success() {
	    return success;
	}

	public synchronized long realTime() {
	    return realTime;
	}

	public synchronized void reset() {
	    success = false;
	    realTime = -1;
	}
    }
    */

    public synchronized void handleTimeKeeperResponseMessage(TimeKeeperResponseMessage timeKeeperMessage) {
	long realTime = timeKeeperMessage.getRealTime();
	this.realTime = realTime;
	logger.debug("HandleTimeKeeperResponseMessage: About to notify");
	notifyAll();
	
	/* Now using Thread.wait and notify
	  sleepSynchronizer.wakeUp(realTime);
	*/
    }

    private static TimeKeeper localTimeKeeper = null;
    private static TimeKeeper remoteTimeKeeper = null;
    public static TimeKeeper getLocalTimeKeeper(IPCServer ipcServer, IPCNode ipcNode, IPCNodeCallback ipcNodeCallback, 
						StationConfiguration stationConfig) {
	if(localTimeKeeper == null)
	    localTimeKeeper = new TimeKeeper(ipcNode, ipcNodeCallback, ipcServer, stationConfig, true);
	return localTimeKeeper;
    }
    public static TimeKeeper getRemoteTimeKeeper(IPCServer ipcServer, IPCNode ipcNode, IPCNodeCallback ipcNodeCallback, 
						 StationConfiguration stationConfig) {
	if(remoteTimeKeeper == null)
	    remoteTimeKeeper = new TimeKeeper(ipcNode, ipcNodeCallback, ipcServer, stationConfig, false);
	return remoteTimeKeeper;
    }
}

