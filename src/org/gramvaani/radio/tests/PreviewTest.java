package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.providers.PlayoutProvider;

import java.util.Hashtable;
import java.util.Random;

// XXX implements PlayoutPreviewListenerCallback

public class PreviewTest extends RSTest {
    
    PlayoutProvider previewProvider;
    static final int maxSleepTime = 60;
    final int minSleepTime = 2000;

    public PreviewTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);
	previewProvider = (PlayoutProvider) env.get(RSApp.PREVIEW_PROVIDER);
    }
    
    public PreviewTest(Hashtable<String, Object> env){
	super(env);
	previewProvider = (PlayoutProvider) env.get(RSApp.PREVIEW_PROVIDER);
    }

    private void doneTesting(){
	logger.info("PreviewTest: Done Testing.");
	previewProvider.stop("test.mp3", null);
    }
    public void start(){
	long randTime;
	Random generator = new Random(startTime);
	try {
	    // UI must start only after 2sec
	    currentTime = System.currentTimeMillis();
	    //while(currentTime - startTime < runDuration)
	    //{
		    Thread.sleep(2500);
		    logger.info("PreviewTest: About to play.");
		    previewProvider.play("test.mp3", null);
		    Thread.sleep(runDuration);
		    /*randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(randTime);

		    currentTime = System.currentTimeMillis();
		    if(currentTime - startTime >= runDuration){
			logger.info("PreviewTest: Run duration completed. Exiting.");
			doneTesting();
			return;
		    }

		    logger.info("PreviewTest: About to pause.");
		    previewProvider.pause("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(randTime);

		    currentTime = System.currentTimeMillis();
		    if(currentTime - startTime >= runDuration){
			logger.info("PreviewTest: Run duration completed. Exiting.");
			doneTesting();
			return;
		    }

		    logger.info("PreviewTest: About to play after pause.");
		    previewProvider.play("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(randTime);

		    currentTime = System.currentTimeMillis();
		    if(currentTime - startTime >= runDuration){
			logger.info("PreviewTest: Run duration completed. Exiting.");
			doneTesting();
			return;
		    }

		    logger.info("PreviewTest: About to stop.");
		    previewProvider.stop("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(randTime);

		    currentTime = System.currentTimeMillis();
		    if(currentTime - startTime >= runDuration){
			logger.info("PreviewTest: Run duration completed. Exiting.");
			doneTesting();
			return;
		    }
		    logger.info("PreviewTest: About to play after stop.");
		    previewProvider.play("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(randTime);
		    */
		    logger.info("PreviewTest: Stopping finally for this iteration.");
		    previewProvider.stop("test.mp3", null);
		    //}
	} catch(Exception e) {
	    e.printStackTrace(System.out);
	}
    }
}