package org.gramvaani.radio.tests;

import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;

import java.lang.reflect.Constructor;
import java.util.*;
import java.io.*;

import gnu.getopt.Getopt;

public class RSTestController {

    public static final String ARCHIVER_SERVICE	= "ARCHIVER_SERVICE";
    public static final String AUDIO_SERVICE 	= "AUDIO_SERVICE";
    public static final String CHANNEL_SERVICE 	= "CHANNEL_SERVICE";
    public static final String LIB_SERVICE 	= "LIB_SERVICE";
    public static final String LOGGER		= "LOGGER";
    public static final String MACHINE_ID 	= "MACHINE_ID";
    public static final String MEDIA_LIB 	= "MEDIA_LIB";
    public static final String MIC_SERVICE 	= "MIC_SERVICE";
    public static final String RESOURCE_MANAGER = "RESOURCE_MANAGER";
    public static final String RSCONTROLLER	= "RSCONTROLLER";
    public static final String SMS_SERVICE 	= "SMS_SERVICE";
    public static final String STATION_CONFIGURATION = "STATION_CONFIGURATION";
    public static final String TELEPHONY_SERVICE= "TELEPHONY_SERVICE";
    public static final String UI_SERVICE 	= "UI_SERVICE";
    public static final String RS_APP           = "RS_APP";

    final LogUtilities logger;
    Hashtable<String, Object> env;
    StationConfiguration stationConfig;
    String machineID;
    String testFileName;
    TestBuilder testBuilder;
    

    public RSTestController (Hashtable<String, Object> env, String testFileName){
	//RSController rsController = new RSController(stationConfig);
	//UIService uiService = new UIService(stationConfig, machineID);
	logger = new LogUtilities("RSTestController");

	env.put(LOGGER, logger);
	/*
	Hashtable<String, Object> env = new Hashtable<String, Object>();
	env.put(STATION_CONFIG, (Object) stationConfig);
	env.put(MACHINE_ID, 	(Object) machineID);
	env.put(UI_SERVICE, 	(Object) uiService);
	*/
	testBuilder = new TestBuilder(testFileName, env);
	logger.info("RSTestController: Initialized Test Controller.");
	
    }

    public void start(){
	long runDuration = testBuilder.getRunDuration();
	for (RSTestGroup testGroup: testBuilder.getTestGroups()){
	    logger.debug("Start: Initializing group.");
	    testGroup.initTests(runDuration);
	    for (int i = 0; i < testGroup.getNumRepeats(); i++){
		ArrayList<Thread> threadList = new ArrayList<Thread>();
		for (final RSTest test: testGroup.getTests()){
		    Thread t = new Thread("TestWorker:"+test.getName()) {
			    public void run(){
				test.start();
			    }
			};
		    t.start();
		    threadList.add(t);
		}
		
		for (Thread t: threadList){
		    while(true){
			try{
			    t.join();
			    break;
			} catch (InterruptedException e){}
		    }
		}

		logger.debug("Start: Ran group "+(i+1)+" times.");
	    }
	} 
	logger.debug("Start: All testgroups completed.");
	
	//Create a file named testDone to indicate to the shell script that testing is done.
	File f = new File("testDone");
	if(!f.exists()){
	    try {
	    f.createNewFile();
	    logger.debug("Start: New file testDone created.");
	    }
	    catch(Exception e){
		logger.error("Start: Failed to create file testDone.");
		return;
	    }
	}
	else {
	    logger.warn("Start: File testDone already exists.");
	}
    }

    /*
    public static void main(String[] args){
	Getopt g = new Getopt("RSpp", args, "c:m:f:");
	int c;
	String configFilePath = "./automation.conf";
	String machineID = "127.0.0.1";
	String testFileName = "test.conf";
	while((c = g.getopt()) != -1) {
	    switch(c) {
	    case 'c': 
		configFilePath = g.getOptarg();
		break;
	    case 'm':
		machineID = g.getOptarg();
		break;
	    case 'f':
		testFileName = g.getOptarg();
		break;
	    }
	}
	
	StationConfiguration stationConfiguration = new StationConfiguration();
	stationConfiguration.readConfig(configFilePath);

	RSTestController testController = new RSTestController (stationConfiguration, machineID, testFileName);
    }
    */
    
}

class TestBuilder {
    ArrayList<RSTestGroup> testGroups;
    Hashtable<String, Object> env;
    LogUtilities logger;
    int maxParallelTests = 0;
    long runDuration = 180000;//3 minutes

    public TestBuilder(String testFileName, Hashtable<String, Object> env){
	this.env = env;
	logger = new LogUtilities("TestBuilder");
	boolean firstLine=true;
	try{
	    BufferedReader in = new BufferedReader(new FileReader(testFileName));
	    String str = null;
	    testGroups = new ArrayList<RSTestGroup> ();
	    while ((str = in.readLine()) != null){
		str.trim();
		if (str.equals("") || str.startsWith("#"))
		    continue;
		if(firstLine == true){
		    runDuration = Long.parseLong(str);
		    firstLine = false;
		    continue;
		}
		RSTestGroup testGroup = new RSTestGroup(str, env);
		testGroups.add(testGroup);
		if (testGroup.getNumTests() > maxParallelTests)
		    maxParallelTests = testGroup.getNumTests();
	    }
	}catch (Exception e){
	    logger.fatal("TestBuilder: Error reading test file.", e);
	    System.exit(-1);
	}
    }
    
    public ArrayList<RSTestGroup> getTestGroups(){
	return testGroups;
    }

    public long getRunDuration(){
	return runDuration;
    }

    public int getMaxParallelTests(){
	return maxParallelTests;
    }
    
}

class RSTestGroup {
    ArrayList<Class<?>> testClasses;
    ArrayList<RSTest> tests;
    Hashtable<String, Object> env;

    int numRepeats = 0;
    LogUtilities logger;

    public RSTestGroup(String str, Hashtable<String, Object> env){
	testClasses = new ArrayList<Class<?>>();
	tests = new ArrayList<RSTest>();
	String[] tokens = str.split(",");
	logger = new LogUtilities("RSTestGroup");
	this.env = env;

	numRepeats = Integer.parseInt(tokens[0]);
	
	for (int i= 1; i < tokens.length; i++){
	    String className = "org.gramvaani.radio.tests."+tokens[i].trim();
	    Class<?> testClass = null;
	    try{
		testClass = Class.forName(className);
		testClasses.add(testClass);
	    }catch (Exception e){
		logger.error("RSTestGroup: Could not load test class: "+className);
	    }
	    
	}
    }

    public boolean initTests(long runDuration){
	boolean initSucceeded = true;
	for (Class<?> testClass: testClasses){
	    Constructor testConstructor = null;
	    try{
		testConstructor = testClass.getConstructor(env.getClass(), long.class);
		logger.debug("InitTests: Before calling the constructor.");
		RSTest test = (RSTest) testConstructor.newInstance(new Object[]{env, runDuration});
		tests.add(test);
		logger.debug("InitTests: Initialized test: "+testClass.getName());
	    }catch(Exception e){
		logger.error("InitTests: Could not instantiate test: "+testClass.getName(), e);
		initSucceeded = false;
	    }

	}
	
	return initSucceeded;
    }

    public ArrayList<RSTest> getTests(){
	return tests;
    }

    public int getNumRepeats(){
	return numRepeats;
    }

    public int getNumTests(){
	return tests.size();
    }
}
