package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.providers.MediaLibUploadProvider;
import org.gramvaani.radio.app.providers.MediaLibUploadListenerCallback;

import java.util.Hashtable;

public class MediaLibUploadTest extends RSTest implements MediaLibUploadListenerCallback {
    
    MediaLibUploadProvider uploadProvider;
    public MediaLibUploadTest (Hashtable<String, Object> env, long runtime){
	super(env, runtime);

	uploadProvider = (MediaLibUploadProvider) env.get(RSApp.MEDIALIB_UPLOAD_PROVIDER);
	uploadProvider.registerWithProvider(this);
    }

    public void start(){
	try {

	    String filename = "test.mp3";
	    logger.info("About to upload: "+filename);
	    uploadProvider.uploadFile(filename, "TestUpload");
	    Thread.sleep(5000);
	    uploadProvider.uploadDir(".", "UploadTest");

	} catch(Exception e) {
	    //e.printStackTrace(System.out);
	    logger.error("MediaLibUploadTest: Start: Encountered excetption.", e);
	}

    }

    public void activateMediaLibUpload(boolean state, String[] error){
	if(state)
	    logger.info("ActivateMediaLibUpload: Provider activated.");
	else
	    logger.error("ActivateMediaLibUpload: Provider deactivated.");
    }

    public void uploadError(String fileName, String error, int numRemaining){
	logger.error("UploadError: Could not upload "+fileName+" error: "+error+" numRem: "+ numRemaining);
    }

    public void uploadSuccess(String fileName, String libFileName, int numRemaining, boolean isDuplicate){
	logger.info("UploadSuccess: Uploaded file "+fileName+" to "+libFileName+". remaining: "+numRemaining+" duplicate:"+isDuplicate);
    }
    
    public void updateProgress(String fileName, long uploadedLength, long totalLength){

    }

    public void uploadGiveup(String fileName) {
    
    }

    public void uploadProviderError(String errorCode, String[] errorParams){
	logger.error("UnknownError: Encountered unknown error: "+errorCode);
    }

}