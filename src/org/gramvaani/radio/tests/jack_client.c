/* @file simple_client.c
 *
 * @brief This is very simple client that demonstrates the basic
 * features of JACK as they would be used by many applications.
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <jack/jack.h>

int exitJack=0;

/**
 * This is the shutdown callback for this JACK application.
 * It is called by JACK if the server ever shuts down or
 * decides to disconnect the client.
 */
void
jack_shutdown (void *arg)
{
  exitJack = 1;
  exit (1);
}

int
main (int argc, char *argv[])
{
  jack_client_t *client;
  const char **ports;
  time_t *curTime =(time_t *)malloc(sizeof(time_t));
  int i;

  if (argc < 2) {
    printf ("usage: jack_simple_client <name>\n");
    return 1;
  }
  
  /* try to become a client of the JACK server */
  if ((client = jack_client_new (argv[1])) == 0) {
    printf ("jack server not running\n");
    return 1;
  }
  
  
  /* tell the JACK server to call `jack_shutdown()' if
     it ever shuts down, either entirely, or if it
     just decides to stop calling us.
  */
  jack_on_shutdown (client, jack_shutdown, 0);
  
  
  /* tell the JACK server that we are ready to roll */
  if (jack_activate (client)) {
    printf ("cannot activate client");
    return 1;
  }
  
  
    time(curTime);
    printf("%ld\n",*curTime);
    /* get the ports.*/
       
    //Input ports
    if ((ports = jack_get_ports (client, NULL, NULL, JackPortIsInput|JackPortIsPhysical)) != NULL) {
      i=0;
      while(ports[i]!=NULL){
	printf("%s\tinput\n",ports[i]);
	i++;
      }
      free (ports);
    }
    
    //Output ports
    if ((ports = jack_get_ports (client, NULL, NULL, JackPortIsOutput|JackPortIsPhysical)) != NULL) {
      i=0;
      while(ports[i]!=NULL){
	printf("%s\toutput\n",ports[i]);
	i++;
      }
      free(ports);
    }

    jack_client_close (client);
    exit (0);
        
}
