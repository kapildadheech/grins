package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.utilities.*;

import java.util.Hashtable;
import org.gstreamer.*;
import org.gstreamer.elements.*;
import org.gstreamer.event.*;

public class GstStressTest extends RSTest {

    public GstStressTest(Hashtable<String, Object> env){
	super(env);
    }

    public GstStressTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);
    }

    protected void doInitTest(){
	GstPipe gstPipe;
	try {
	    for(int i=0; i<100; i++) {
		gstPipe = new GstPipe();
		Thread.sleep(100);
		gstPipe.start();
		Thread.sleep(100);
		gstPipe.stop();
	    }
	}catch(Exception e) {
	    logger.error("doInitTest: Thread interrupted.");
	}
    }

    protected void doPlayPauseTest() {
	GstPipe gstPipe = new GstPipe();
	try {
	    for(int i=0; i<100; i++) {
		gstPipe.start();
		Thread.sleep(100);
		gstPipe.pause();
	    }
	    gstPipe.stop();
	}catch(Exception e){
	    logger.error("doPlayPauseTest: Thread interrupted.");
	}
    }

    protected void doMultiPipeTest() {
	GstPipe[] gstPipes = new GstPipe[100];
	for(int i=0; i<100; i++) {
	    gstPipes[i] = new GstPipe();
	    gstPipes[i].start();
	}
	try {
	    Thread.sleep(5000);
	}catch(Exception e){}
	for(int i=0; i<100; i++) {
	    gstPipes[i].stop();
	}
    }
    //Test three things.
    //1. Initializing and stopping Gstreamer several times.
    //2. Pausing and playing a pipeline several times.
    //3. Running multiple pipelines.
    public void start(){
	GstPipe gstPipe;
	int count = 0;
	try {
	    while(currentTime - startTime < runDuration){
		Thread.sleep(5000);
		logger.info("GstStressTest: About to start iteration.");
		doInitTest();
		doPlayPauseTest();
		doMultiPipeTest();
		count++;
		logger.info("GstStressTest:"+count+" iteration completed.");
		currentTime = System.currentTimeMillis();
	    }
	} catch(Exception e) {
	    e.printStackTrace(System.out);
	}
    }
    
}


class GstPipe implements Runnable, Bus.ERROR{
    Thread thread;
    Pipeline pipe;
    String clientName = "GstStressTest";
    Element audioSrc, dec, audioConvert, resample, audioSink;
    LogUtilities logger;

    public GstPipe(){
	logger = new LogUtilities("GstPipe");
	logger.debug("GstPipe: Creating new pipeline");

	String[] args = Gst.init("GstStressTest", new String[] {});
	pipe = new Pipeline ("GstPipe");
	try {
	    audioSrc = ElementFactory.make("filesrc", "filesrc");
	    audioSrc.set("location","test.mp3");
	    dec = ElementFactory.make("mad","mad");
	    audioConvert = ElementFactory.make("audioconvert","audioconvert");
	    resample = ElementFactory.make("audioresample","audioresample");
	    audioSink = ElementFactory.make("fakesink", "fakesink");
	}catch(Exception e){
	    logger.error("GstPipe: Could not create all gstreamer elements.", e);
	}
	pipe.addMany(audioSrc, dec, audioConvert, resample, audioSink);
	pipe.linkMany(audioSrc, dec, audioConvert, resample, audioSink);
	pipe.setState(State.NULL);

	thread = new Thread(this, clientName);
	thread.start();
	while(true){
	    try{
		Thread.sleep(500);
		break;
	    }catch (Exception e){
		logger.debug("GstPipe: ThreadInterrupted.");
	    }
	}
    }

    public void run(){
	logger.debug("Run: Entering");
	pipe.getBus().connect(this);
	
	logger.debug("Run: Leaving");
	logger.debug("Run: Gstreamer entering Gst.main.");
	Gst.main();
	logger.debug("Run: Exited Gst.main.");
    }

    public void errorMessage(GstObject source, int errorCode, String error){
	logger.error("BusError: "+error);
    }

    public synchronized boolean start(){
	logger.debug("Start: Entering.");
	
	StateChangeReturn stateChangeReturn = pipe.setState(State.PLAYING);

	if (stateChangeReturn == StateChangeReturn.FAILURE){
	    logger.error("Start: Leaving. Gstreamer: Unable to start pipeline.");
	    return false;
	} else {
	    logger.info("Start: Leaving. Gstreamer: Started pipeline successfully.");
	    return true;
	}
    }
    
    public synchronized boolean pause(){
	logger.debug("Pause: Entering");
	
	StateChangeReturn stateChangeReturn = pipe.setState(State.PAUSED);

	if (stateChangeReturn == StateChangeReturn.FAILURE){
	    logger.error("Pause: Leaving. Gstreamer: Unable to pause pipeline.");
	    return false;
	} else {
	    logger.info("Pause: Leaving. Gstreamer: Pipeline paused successfully.");
	    return true;
	}
    }
    
    public synchronized boolean stop(){
	if(pipe.setState(State.NULL) == StateChangeReturn.FAILURE){
	    logger.error("Stop: Gstreamer: Unable to stop pipeline.");
	    return false;
	} else {
	    logger.info("Stop: Gstreamer: Stopped pipeline successfully.");
	    Gst.quit();
	    return true;
	}
    }

   
}
