package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.providers.PlayoutProvider;

import java.util.Hashtable;
import java.util.Random;

// XXX implements PlayoutPreviewListenerCallback

public class PlayoutTest extends RSTest {
    
    PlayoutProvider playoutProvider;
    static final int maxSleepTime = 60;
    final int minSleepTime = 2000;

    public PlayoutTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);
	playoutProvider = (PlayoutProvider) env.get(RSApp.PLAYOUT_PROVIDER);
    }
    
    public PlayoutTest(Hashtable<String, Object> env){
	super(env);
	playoutProvider = (PlayoutProvider) env.get(RSApp.PLAYOUT_PROVIDER);
    }

    private void doneTesting(){
	logger.info("PlayoutTest: Done Testing.");
	playoutProvider.stop("test.mp3", null);
    }
    public void start(){
	long randTime;
	Random generator = new Random(startTime);
	try {
	    // UI must start only after 2sec
	    currentTime = System.currentTimeMillis();
	    while(currentTime - startTime < runDuration)
		{
		    Thread.sleep(2000);
		    logger.info("PlayoutTest: About to play.");
		    playoutProvider.play("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(runDuration);

		    currentTime = System.currentTimeMillis();
		    /*if(currentTime - startTime >= runDuration){
			logger.info("PlayoutTest: Run duration completed. Exiting.");
			doneTesting();
			return;
		    }

		    logger.info("PlayoutTest: About to pause.");
		    playoutProvider.pause("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(5000);



		    currentTime = System.currentTimeMillis();
		    if(currentTime - startTime >= runDuration){
			logger.info("PlayoutTest: Run duration completed. Exiting.");
			doneTesting();
			return;
		    }


		    logger.info("PlayoutTest: About to play after pause.");
		    playoutProvider.play("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);

		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(5000);


		    currentTime = System.currentTimeMillis();
		    if(currentTime - startTime >= runDuration){
			logger.info("PlayoutTest: Run duration completed. Exiting.");
			doneTesting();
			return;
		    }


		    logger.info("PlayoutTest: About to stop.");
		    playoutProvider.stop("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(5000);

		    currentTime = System.currentTimeMillis();
		    if(currentTime - startTime >= runDuration){
			logger.info("PlayoutTest: Run duration completed. Exiting.");
			doneTesting();
			return;
		    }
		    logger.info("PlayoutTest: About to play after stop.");
		    playoutProvider.play("test.mp3", null);
		    randTime = 1000 * generator.nextInt(maxSleepTime);
		    randTime = randTime>minSleepTime?randTime:minSleepTime;
		    Thread.sleep(3000);
		    */
		    logger.info("PlayoutTest: Stopping finally for this iteration.");
		    playoutProvider.stop("test.mp3", null);
		}
	} catch(Exception e) {
	    e.printStackTrace(System.out);
	}
    }
}