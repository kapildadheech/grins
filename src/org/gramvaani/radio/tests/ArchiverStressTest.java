package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.providers.ArchiverProvider;
import org.gramvaani.radio.rscontroller.services.ArchiverService;

import java.util.Hashtable;

// XXX implements ArchiverListenerCallback

public class ArchiverStressTest extends RSTest {

    ArchiverProvider archiverProvider;
    final int maxSleepTime = 60;
    public ArchiverStressTest(Hashtable<String, Object> env){
	super(env);

	archiverProvider = (ArchiverProvider) env.get(RSApp.ARCHIVER_PROVIDER);
    }

    public ArchiverStressTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);
	archiverProvider = (ArchiverProvider) env.get(RSApp.ARCHIVER_PROVIDER);
    }

    public void start(){
	try {
	    while(currentTime - startTime < runDuration){
		Thread.sleep(5000);
		logger.info("ArchiverStressTest: About to start archiving.");
		archiverProvider.startArchive(ArchiverService.PLAYOUT, "ArchiverStressTest", true);
		Thread.sleep(runDuration);
		
		logger.info("ArchiverStressTest: About to stop archiving.");
		archiverProvider.stopArchive(ArchiverService.PLAYOUT, true);

		currentTime = System.currentTimeMillis();
	    }
	} catch(Exception e) {
	    e.printStackTrace(System.out);
	}
    }
    
}
