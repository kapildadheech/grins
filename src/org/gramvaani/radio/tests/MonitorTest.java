package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.providers.MonitorProvider;
import org.gramvaani.radio.app.providers.PlayoutProvider;
import java.util.Hashtable;
import java.util.Random;

public class MonitorTest extends RSTest {

    MonitorProvider monitorProvider;
    PlayoutProvider previewProvider;
    final int maxSleepTime = 60;
    final int minSleepTime = 2000;
    public MonitorTest(Hashtable<String, Object> env){
	super(env);
	monitorProvider = (MonitorProvider) env.get(RSApp.MONITOR_PROVIDER);
    }

    public MonitorTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);
	monitorProvider = (MonitorProvider) env.get(RSApp.MONITOR_PROVIDER);
    }

    private void doneTesting(){
	logger.info("MonitorTest: Done testing");
	monitorProvider.stopMonitor();
	//previewProvider.stop("test.mp3", null);
    }

    public void start(){
	long randTime;
	Random generator = new Random(startTime);
	try {
	    //while(currentTime - startTime < runDuration){
		Thread.sleep(5000);
		logger.info("MonitorTest: About to start monitoring.");
		monitorProvider.startMonitor();
	
		randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>minSleepTime?randTime:minSleepTime;
		Thread.sleep(runDuration);
		/*
		currentTime = System.currentTimeMillis();
		if(currentTime - startTime >= runDuration){
		    logger.info("MonitorTest: Run duration completed. Exiting.");
		    doneTesting();
		    return;
		}
		
		logger.info("MonitorTest: About to stop monitoring.");
		monitorProvider.stopMonitor();
		randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>minSleepTime?randTime:minSleepTime;
		Thread.sleep(randTime);
		
		currentTime = System.currentTimeMillis();
		if(currentTime - startTime >= runDuration){
		    logger.info("MonitorTest: Run duration completed. Exiting.");
		    doneTesting();
		    return;
		}
		
		logger.info("MonitorTest: About to start monitoring second time.");
		monitorProvider.startMonitor();
		randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>minSleepTime?randTime:minSleepTime;
		Thread.sleep(randTime);
		
		currentTime = System.currentTimeMillis();
		if(currentTime - startTime >= runDuration){
		    logger.info("MonitorTest: Run duration completed. Exiting.");
		    doneTesting();
		    return;
		}
		
		logger.info("MonitorTest: About to stop monitoring second time (final for the iteration).");
		*/
		logger.info("MonitorTest: About to stop monitoring.");
		monitorProvider.stopMonitor();
		
		//}
	} catch(Exception e) {
	    e.printStackTrace(System.out);
	}
	
    }
    
}
