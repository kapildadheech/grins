package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.providers.ArchiverProvider;
import org.gramvaani.radio.rscontroller.services.ArchiverService;

import java.util.Hashtable;
import java.util.Random;

// XXX implements ArchiverListenerCallback

public class ArchiverTest extends RSTest {

    ArchiverProvider archiverProvider;
    final int maxSleepTime = 60;
    final int minSleepTime = 2000; //millisec

    public ArchiverTest(Hashtable<String, Object> env){
	super(env);

	archiverProvider = (ArchiverProvider) env.get(RSApp.ARCHIVER_PROVIDER);
    }

    public ArchiverTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);
	archiverProvider = (ArchiverProvider) env.get(RSApp.ARCHIVER_PROVIDER);
    }

    private void doneTesting(){
	logger.info("ArchiverTest: Done testing");
	archiverProvider.stopArchive(ArchiverService.PLAYOUT, true);
    }

    public void start(){
	long randTime;
	Random generator = new Random(startTime);
	try {
	    //while(currentTime - startTime < runDuration){
		Thread.sleep(5000);
		logger.info("ArchiverTest: About to start archiving.");
		archiverProvider.startArchive(ArchiverService.PLAYOUT, "ArchiverTest", true);
		Thread.sleep(runDuration);
		/*randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>500?randTime:500;
		//Thread.sleep(randTime);
		Thread.sleep(5000);

		currentTime = System.currentTimeMillis();
		if(currentTime - startTime >= runDuration){
		    logger.info("ArchiverTest: Run duration completed. Exiting.");
		    doneTesting();
		    return;
		}
		
		logger.info("ArchiverTest: About to stop archiving.");
		archiverProvider.stopArchive();
		randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>minSleepTime?randTime:minSleepTime;
		Thread.sleep(5000);
		
		//Thread.sleep(randTime);
		//

		currentTime = System.currentTimeMillis();
		if(currentTime - startTime >= runDuration){
		    logger.info("ArchiverTest: Run duration completed. Exiting.");
		    doneTesting();
		    return;
		}
		
		logger.info("ArchiverTest: About to start archiving second time.");
		archiverProvider.startArchive();
		randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>minSleepTime?randTime:minSleepTime;
		Thread.sleep(5000);

		currentTime = System.currentTimeMillis();
		if(currentTime - startTime >= runDuration){
		    logger.info("ArchiverTest: Run duration completed. Exiting.");
		    doneTesting();
		    return;
		}
		
		logger.info("ArchiverTest: About to stop archiving second time (final for the iteration).");
		*/
		logger.info("ArchiverTest: About to stop archiving.");
		archiverProvider.stopArchive(ArchiverService.PLAYOUT, true);
		//}
	} catch(Exception e) {
	    e.printStackTrace(System.out);
	}
	
    }
    
}
