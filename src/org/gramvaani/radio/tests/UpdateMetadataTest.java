package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.providers.CacheProvider;
import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;

import java.util.Hashtable;

// XXX implements ProviderListenerCallback

public class UpdateMetadataTest extends RSTest {

    CacheProvider cacheProvider;

    public UpdateMetadataTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);

	cacheProvider = (CacheProvider) rsApp.getProvider(RSApp.CACHE_PROVIDER);
	metadataProvider.setCacheProvider(cacheProvider);
    }
    
    public void start(){
	try {
	    
	    Thread.sleep(5000);

	    metadataProvider.addLanguage("english");
	    metadataProvider.addLanguage("hindi");
	    metadataProvider.addLanguage("punjabi");
	    metadataProvider.addCreatorAffiliation(Creator.FUNDS);
	    metadataProvider.addCreatorAffiliation(Creator.PRODUCER);
	    metadataProvider.addCreatorAffiliation(Creator.SCRIPT);
	    metadataProvider.addCreatorRole(Entity.VOLUNTEER);
	    metadataProvider.addCreatorRole(Entity.STAFF);
	    metadataProvider.addCreatorRole(Entity.PERSON);
	    metadataProvider.addCreatorRole(Entity.RADIO_STATION);
	    metadataProvider.addCreatorRole(Entity.COMPANY);
	    metadataProvider.addCreatorRole(Entity.NGO);
	    metadataProvider.addCreatorLocation("new delhi");
	    metadataProvider.addCreatorLocation("lucknow");
	    metadataProvider.addCreatorLocation("orchha");
	    metadataProvider.addCreatorLocation("jaipur");
	    //metadataProvider.addTags("picnic, food, cricket, football, trekking, hello");
	    
	    InitCategories initCategories = new InitCategories(stationConfig, metadataProvider.getUpdateHandler());
	    initCategories.start();

	    //metadataProvider.dumpAnchorIndex();

	    MediaLib medialib = MediaLib.getMediaLib(stationConfig, uiService.getTimeKeeper());
	    
	    Entity[] entities = new Entity[10];
	    entities[0] = new Entity("entity_a", "a.a", "PSTN", "1", "a@a", "new delhi", Entity.VOLUNTEER);
	    entities[1] = new Entity("entity_b", "b.a", "PSTN", "2", "b@a", "lucknow", Entity.STAFF);
	    entities[2] = new Entity("entity_c", "c.a", "PSTN", "3", "c@a", "orchha", Entity.NGO);
	    entities[3] = new Entity("entity_d", "d.a", "PSTN", "4", "d@a", "new delhi", Entity.RADIO_STATION);
	    entities[4] = new Entity("entity_e", "e.a", "PSTN", "5", "e@a", "jaipur", Entity.COMPANY);
	    entities[5] = new Entity("entity_f", "f.a", "PSTN", "6", "f@a", "new delhi", Entity.VOLUNTEER);
	    entities[6] = new Entity("entity_g", "g.a", "PSTN", "7", "g@a", "lucknow", Entity.STAFF);
	    entities[7] = new Entity("entity_h", "h.a", "PSTN", "8", "h@a", "orchha", Entity.VOLUNTEER);
	    entities[8] = new Entity("entity_i", "i.a", "PSTN", "9", "i@a", "jaipur", Entity.NGO);
	    entities[9] = new Entity("entity_j", "j.a", "PSTN", "0", "j@a", "lucknow", Entity.COMPANY);
	    int i = 0;
	    for(Entity entity: entities) {
		medialib.insert(entity);
		entities[i++] = medialib.getSingle(entity);
	    }
	    
	    Metadata[] categoriesMetadata = medialib.get(new Category(""));
	    Category[] categories = new Category[categoriesMetadata.length];
	    i = 0;
	    for(Metadata categoryMetadata: categoriesMetadata)
		categories[i++] = (Category)categoryMetadata;

	    RadioProgramMetadata[] radioPrograms = new RadioProgramMetadata[1000];
	    String[] languages = new String[]{"english", "hindi", "punjabi"};
	    String[] stations = new String[]{"station_1", "station_2", "station_3"};
	    String[] affiliations = new String[]{Creator.FUNDS, Creator.SCRIPT, Creator.PRODUCER};
	    String[] tags = new String[]{"picnic, food", "cricket, football, trekking", "cricket, hello", "football, foot",
					 "cricket, picnic", "football, cricket"};
	    for(i = 100; i < 110; i++) {
		radioPrograms[i] = new RadioProgramMetadata("filename_" + i, 0, "", ArchiverService.BCAST_MIC, 0, 
							    LibService.DONE_ARCHIVING_PLAYOUT,
							    LibService.MEDIA_NOT_INDEXED,"",
							    languages[i % languages.length], 
							    stations[i % stations.length]);
		radioPrograms[i].setTitle("title_"+i);
		medialib.insert(radioPrograms[i]);
		radioPrograms[i] = medialib.getSingle(radioPrograms[i]);



		for(int j = 0; j < 5; j++)
		    metadataProvider.addProgramCategory(radioPrograms[i].getItemID(), categories[(i + j) % categories.length].getNodeID());
		


		metadataProvider.addProgramCreator(radioPrograms[i].getItemID(), 
						   entities[i % entities.length].getEntityID(), 
						   affiliations[i % affiliations.length]);

		metadataProvider.setProgramTags(radioPrograms[i].getItemID(), tags[i % tags.length]);
		metadataProvider.updateProgramIndex(radioPrograms[i].getItemID());
	    }

	    for(i = 0; i <10; i++){
		radioPrograms[i] = new RadioProgramMetadata("test"+i+".flac", 0, "", ArchiverService.BCAST_MIC, 0, 
							    LibService.DONE_ARCHIVING_PLAYOUT, 
							    LibService.MEDIA_NOT_INDEXED,"",
							    languages[i % languages.length], 
							    stations[i % stations.length]);
		radioPrograms[i].setTitle("test_title_"+i);
		medialib.insert(radioPrograms[i]);
		radioPrograms[i] = medialib.getSingle(radioPrograms[i]);
		
		for(int j = 0; j < 5; j++)
		    metadataProvider.addProgramCategory(radioPrograms[i].getItemID(), categories[(i + j) % categories.length].getNodeID());
		
		metadataProvider.addProgramCreator(radioPrograms[i].getItemID(), 
						   entities[i % entities.length].getEntityID(), 
						   affiliations[i % affiliations.length]);
		
		metadataProvider.setProgramTags(radioPrograms[i].getItemID(), tags[i % tags.length]);
		metadataProvider.updateProgramIndex(radioPrograms[i].getItemID());
	    }
	    //	    metadataProvider.dumpProgramIndex();
	    metadataProvider.flushProgramIndex();

	} catch(Exception e) {
	    logger.error("Error in running test", e);
	}
    }
}