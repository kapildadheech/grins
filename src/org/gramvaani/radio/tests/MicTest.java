package org.gramvaani.radio.tests;

import org.gramvaani.radio.app.RSApp;
import org.gramvaani.radio.app.providers.MicProvider;

import java.util.Hashtable;
import java.util.Random;

public class MicTest extends RSTest {
    

    MicProvider micProvider;
    final int maxSleepTime = 60;
    final int minSleepTime = 2000; 
    public MicTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);

	micProvider = (MicProvider) env.get(RSApp.MIC_PROVIDER);
    }

    public MicTest(Hashtable<String, Object> env){
	super(env);

	micProvider = (MicProvider) env.get(RSApp.MIC_PROVIDER);
    }

    private void doneTesting(){
	logger.info("MicTest: Done testing.");
	micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE);
    }

    public void start(){
	long randTime;
	Random generator = new Random(startTime);
	try{
	    while(currentTime - startTime < runDuration){
		Thread.sleep(5000);
		logger.info("MicTest: About to Enable.");

		micProvider.enable("", MicProvider.PLAYOUT_PORT_ROLE);
		randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>minSleepTime?randTime:minSleepTime;
		Thread.sleep(5000);
		
		currentTime = System.currentTimeMillis();
		if(currentTime - startTime >= runDuration){
		    logger.info("MicTest: Run duration completed. Exiting.");
		    doneTesting();
		    return;
		}
		
		logger.info("MicTest: About to Disable Mic.");
		micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE);
		randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>500?randTime:500;
		Thread.sleep(3000);
		
		currentTime = System.currentTimeMillis();
		if(currentTime - startTime >= runDuration){
		    logger.info("MicTest: Run duration completed. Exiting.");
		    doneTesting();
		    return;
		}
		
		logger.info("MicTest: About to Enable Mic again.");
		micProvider.enable("", MicProvider.PLAYOUT_PORT_ROLE);
		randTime = 1000 * generator.nextInt(maxSleepTime);
		randTime = randTime>minSleepTime?randTime:minSleepTime;
		Thread.sleep(3000);
		logger.info("MicTest: Finally about to Disable mic.");
		micProvider.disable(MicProvider.PLAYOUT_PORT_ROLE);
	    }
	}
	catch(Exception e){
	    e.printStackTrace(System.out);
	}
     }
}