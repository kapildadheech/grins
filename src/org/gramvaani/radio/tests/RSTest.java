package org.gramvaani.radio.tests;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.app.*;
import org.gramvaani.radio.app.providers.*;
import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.utilities.*;
import java.util.*;

public abstract class RSTest {
    Hashtable <String, Object> env;

    LogUtilities logger;
    UIService uiService;
    StationConfiguration stationConfig;
    RSApp rsApp;
    String machineID;

    // XXX these should not be defined here. widget instances should be directly obtained from the RSApp widget hashtable after consulting the config file

    PlayoutProvider playoutProvider;
    ArchiverProvider archiverProvider;
    MicProvider micProvider;
    LibraryProvider libraryProvider;
    MediaLibUploadProvider uploadProvider;
    MetadataProvider metadataProvider;

    boolean isDone;
    long startTime;
    long currentTime;
    long runDuration;

    public RSTest (Hashtable<String, Object> env, long runTime){
	this.env 	= env;
	runDuration     = runTime;
	startTime       = System.currentTimeMillis();
	uiService 	= (UIService) env.get(RSTestController.UI_SERVICE);
	stationConfig 	= (StationConfiguration) env.get(RSTestController.STATION_CONFIGURATION);
	machineID	= (String) env.get(RSTestController.MACHINE_ID);
	logger		= (LogUtilities) env.get(RSTestController.LOGGER);
	rsApp           = (RSApp) env.get(RSTestController.RS_APP);
	
	playoutProvider	= (PlayoutProvider) env.get(RSApp.PLAYOUT_PROVIDER);
	archiverProvider= (ArchiverProvider) env.get(RSApp.ARCHIVER_PROVIDER);
	micProvider 	= (MicProvider) env.get(RSApp.MIC_PROVIDER);
	libraryProvider = (LibraryProvider) env.get(RSApp.LIBRARY_PROVIDER);
	uploadProvider  = (MediaLibUploadProvider) env.get(RSApp.MEDIALIB_UPLOAD_PROVIDER);
	metadataProvider= (MetadataProvider) env.get(RSApp.METADATA_PROVIDER);
	
    }

    public RSTest (Hashtable<String, Object> env){
	this.env 	= env;

	runDuration     = 180000;//3 minutes
	startTime       = System.currentTimeMillis();
	uiService 	= (UIService) env.get(RSTestController.UI_SERVICE);
	stationConfig 	= (StationConfiguration) env.get(RSTestController.STATION_CONFIGURATION);
	machineID	= (String) env.get(RSTestController.MACHINE_ID);
	logger		= (LogUtilities) env.get(RSTestController.LOGGER);
	rsApp           = (RSApp) env.get(RSTestController.RS_APP);
	
	playoutProvider	= (PlayoutProvider) env.get(RSApp.PLAYOUT_PROVIDER);
	archiverProvider= (ArchiverProvider) env.get(RSApp.ARCHIVER_PROVIDER);
	micProvider 	= (MicProvider) env.get(RSApp.MIC_PROVIDER);
	libraryProvider = (LibraryProvider) env.get(RSApp.LIBRARY_PROVIDER);
	uploadProvider  = (MediaLibUploadProvider) env.get(RSApp.MEDIALIB_UPLOAD_PROVIDER);
	metadataProvider= (MetadataProvider) env.get(RSApp.METADATA_PROVIDER);
    }

    public String getName(){
	return this.getClass().getName();
    }

    public abstract void start();
}