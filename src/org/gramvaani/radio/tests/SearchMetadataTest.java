package org.gramvaani.radio.tests;

import org.gramvaani.radio.rscontroller.services.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.medialib.*;
import org.gramvaani.radio.app.gui.*;

import java.util.Hashtable;

public class SearchMetadataTest extends RSTest {

    public SearchMetadataTest(Hashtable<String, Object> env, long runTime){
	super(env, runTime);
    }
    
    public void start() {
	try {
	    Thread.sleep(5000);
	    LibraryController libraryController = new LibraryController(null, null, rsApp);
	    libraryController.init();

	    String sessionID = libraryController.newSearchSession();

	    /*	    
	    SearchFilter[] defaultFilters = libraryProvider.getDefaultDisplayFilters(sessionID);
	    LanguageFilter languageFilter = (LanguageFilter)defaultFilters[0];
	    languageFilter.activateAnchor(LanguageFilter.LANGUAGE_ANCHOR, "english");
	    libraryProvider.activateFilter(sessionID, languageFilter);
	    SearchResult searchResult = libraryProvider.textSearch(sessionID, "english");

	    String[] languages = languageFilter.getLanguages(MediaLib.getMediaLib(stationConfig, uiService.getTimeKeeper()));
	    for(String language: languages) {
		logger.info("start: language = " + language);
	    }
	    */

	    SearchResult searchResult = libraryController.textSearch(sessionID, "picnic");
	    
	    SearchFilter[] displayFilters = searchResult.getDisplayFilters();
	    SearchFilter[] activeFilters = searchResult.getActiveFilters();
	    RadioProgramMetadata[] programs = searchResult.getPrograms();

	    logger.info("start: display filters = " + displayFilters.length);
	    for(SearchFilter filter: displayFilters)
		logger.info("start: display filter " + filter.getFilterName());
	    logger.info("start: active filters = " + activeFilters.length);
	    for(SearchFilter filter: activeFilters)
		logger.info("start: active filter " + filter.getFilterName());
	    logger.info("start: programs = " + programs.length);
	    for(RadioProgramMetadata program: programs)
		logger.info("start: program " + program.getItemID());
	    
	} catch(Exception e) {
	    logger.error("Error in running test", e);
	}
    }
}


