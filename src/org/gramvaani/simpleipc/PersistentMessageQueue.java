package org.gramvaani.simpleipc;

import java.io.*;

import org.gramvaani.utilities.*;
import org.gramvaani.simpleipc.*;
import org.gramvaani.radio.stationconfig.*;
 
public class PersistentMessageQueue {
    private File queueFile;
    private RandomAccessFile queueRAF = null;
    private LogUtilities logger;
    private long tail, head;
    private Class<?> messageFactory;
    private String queueDir;
    private String queueName;
    private String tmpQueueName;
    private final int QUEUE_FILE_TRANSFER_BUFFER_SIZE = 1024;

    public PersistentMessageQueue(String queueName, String queueLocation, Class messageFactory) throws Exception {
	this.messageFactory = messageFactory;
	this.queueDir = queueLocation;
	this.queueName = queueName;
	this.tmpQueueName = queueName + "_tmp";

	queueFile = new File(queueLocation + File.separator + queueName);
	logger = new LogUtilities(queueName+"_Queue");

	File dir = new File(queueLocation);
	if(!dir.exists()) {
	    dir.mkdirs();
	} else if(!dir.isDirectory()) {
	    throw new IOException("PersistentMessageQueue: " + queueLocation + " exists, but it is not a directory.");
	}
	
	if(queueFile.exists()) {
	    logger.info("PersistentMessageQueue: File " + queueLocation + File.separator + queueName + " already exists.");
	} else {
	    if(!queueFile.createNewFile()) {
		logger.error("PersistentMessageQueue: Failed to create a new file: " + queueLocation + File.separator + queueName);
		throw new IOException("Failed to create a new file: " + queueLocation + File.separator + queueName);
	    } else {
		logger.info("PersistentMessageQueue: Message queue created successfully.");
	    }
	}

	queueRAF = new RandomAccessFile(queueFile, "rws");
        head = 0;
	tail = queueRAF.length();
    }

    public synchronized boolean add(IPCMessage message) {
	if(queueRAF == null) {
	    logger.error("Add: queueRAF is null.");
	    return false;
	}

	try {
	    queueRAF.seek(tail);
	    queueRAF.write(message.getBytes());
	    tail = queueRAF.getFilePointer();
	    logger.debug("Add: Added message to the queue.");
	} catch (IOException e) {
	    logger.error("Add: Exception occured in writing message to the file at position: " + tail, e);
	    return false;
	}
	
	return true;
    }

    public synchronized IPCMessage peekFirst() {
	byte[] lengthBytes = new byte[4];
	int length;

	if(queueRAF == null) {
	    logger.error("Add: queueRAF is null.");
	    return null;
	}

	if(isEmpty()) {
	    logger.info("PeekFirst: No messages present in the queue.");
	    return null;
	}

	try {
	    queueRAF.seek(head);
	    queueRAF.readFully(lengthBytes);

	    length = IPCMessage.byteArrayToInt(lengthBytes);
	    
	    byte[] messageBytes = new byte[length - lengthBytes.length];
	    queueRAF.readFully(messageBytes);
	    
	    String messageString = new String(messageBytes);
	    IPCMessage message = IPCMessage.deserialize(messageString, 
							messageFactory);
	    return message;
	} catch(Exception e) {
	    logger.error("PeekFirst: Error occured in reading message from position: " + head, e);
	    return null;
	}
    }

    public synchronized IPCMessage removeFirst() {
	IPCMessage message = peekFirst();
	if(message != null) {
	    try {
		head = queueRAF.getFilePointer();
		logger.debug("RemoveFirst: Removed first message from the queue.");
	    } catch(IOException e) {
		logger.error("RemoveFirst: GetFilePointer failed. Message will not be removed from the file.", e);
	    }

	    File tmpFile = new File(queueDir + File.separator + tmpQueueName);
	    try {
		if(!tmpFile.createNewFile()) {
		    logger.error("RemoveFirst: Could not create a temporary file: " + queueDir + File.separator + tmpQueueName);
		    logger.error("Removal of message from the file will not happen.");
		} else {
		    RandomAccessFile tmpRAF = new RandomAccessFile(tmpFile, "rws");
		    byte[] buffer = new byte[QUEUE_FILE_TRANSFER_BUFFER_SIZE];
		    int bytesRead = 0;
		    while((bytesRead = queueRAF.read(buffer)) != -1) {
			tmpRAF.write(buffer, (int)tmpRAF.getFilePointer(), bytesRead);
		    }
		    
		    queueRAF.close();
		    tmpRAF.close();

		    if(FileUtilities.changeFilename(tmpQueueName, queueDir, queueName, queueDir) != 0) {
			logger.error("RemoveFirst: Could not rename temporary file:" + tmpQueueName + " to queue file.");
		    } else {
			logger.debug("RemoveFirst: File rename done.");
		    }

		    queueFile = new File(queueDir + File.separator + queueName);
		    queueRAF = new RandomAccessFile(queueFile, "rws");
		    tail -= head;
		    head = 0;
		}
	    } catch(Exception e) {
		logger.error("RemoveFirst: Error in file operations", e);
		logger.error("Removal of message from the file will not happen.");
	    }
	}

	return message;
    }

    public synchronized boolean isEmpty() {
	return (head == tail);
    }

    public synchronized void close() {
	if(queueRAF == null) {
	    logger.error("Add: queueRAF is null.");
	    return;
	}

	try {
	    queueRAF.close();
	} catch(IOException e) {
	    logger.error("Close: Caught IOException while closing the queue file.", e);
	}
    }
}