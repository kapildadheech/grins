package org.gramvaani.simpleipc;

import java.util.*;
import java.nio.*;
import java.lang.reflect.*;

import org.gramvaani.utilities.*;

public class IPCMessage {
    public static final String DEST_BCAST = "BCAST";

    public static final int REGISTER_MESSAGE = 0;
    public static final int ACK_MESSAGE = 1;
    public static final int ERROR_MESSAGE = 2;
    public static final int NOTIFICATION_MESSAGE = 3;
    public static final int SERVICE_COMMAND = 4;
       
    protected String source = "";
    protected String dest = "";
    protected int messageType = -1;
    protected int messageLength = -1;
    protected Hashtable<String, String> params = null;
    protected LogUtilities logger;
    protected boolean isPersistent = false;
    protected long localSequenceNumber;

    static long localSequenceCounter = 0;

    public IPCMessage() {
	params = new Hashtable<String, String>();
	logger = new LogUtilities("IPCMessage");

	setSequence();
    }

    public IPCMessage(String source, String dest){
	params = new Hashtable<String, String>();
	setSource(source);
	setDest(dest);
	logger = new LogUtilities("IPCMessage: Source="+source+" Destination="+dest);

	setSequence();
    }

    public IPCMessage(String source, String dest, Hashtable<String,String> params){
	setSource(source);
	setDest(dest);
	this.params = params;
	logger = new LogUtilities("IPCMessage: Source="+source+" Destination="+dest);
	
	setSequence();
    }

    public IPCMessage(String source, String dest, String[][] args){
	setSource(source);
	setDest(dest);
	params = new Hashtable<String,String>();
	for (String[] arg: args)
	    params.put(arg[0], arg[1]);
	logger = new LogUtilities("IPCMessage: Source="+source+" Destination="+dest);

	setSequence();
    }

    public IPCMessage(String messageString) {
	logger = new LogUtilities("IPCMessage");
	logger.debug("IPCMessage: messageString="+messageString);

	params = new Hashtable<String, String>();
	messageLength = messageString.length() + 4;
	StringTokenizer st = new StringTokenizer(messageString, "\n");
	source = st.nextToken().substring(2);
	dest = st.nextToken().substring(2);
	String msgType = st.nextToken().substring(2);
	if (!msgType.equals("")) {
	    try {
		messageType = Integer.parseInt(msgType);
	    } catch(Exception e) {
		messageType = -1;
		logger.error("Corrupt Message Type");
	    }
	}
	while (st.hasMoreTokens()) {
	    String token = st.nextToken();
	    String name = token.substring(0, token.indexOf(":"));
	    String value = token.substring(token.indexOf(":") + 1);
	    params.put(name, value);
	}

	setSequence();
    }

    void setSequence(){
	localSequenceNumber = localSequenceCounter++;
    }

    public long getLocalSequence(){
	return localSequenceNumber;
    }

    public void addParam(String paramName, String paramValue) {
	params.put(paramName, paramValue);
    }

    public String getParam(String paramName) {
	return params.get(paramName);
    }

    public void setSource(String source) {
	this.source = source;
    }

    public String getSource() {
	return source;
    }

    public void setDest(String destination) {
	this.dest = destination;
    }

    public String getDest() {
	return dest;
    }

    public void setMessageType(int messageType) {
	this.messageType = messageType;
    }

    public int getMessageType() {
	return messageType;
    }

    public boolean isPersistent() {
	return isPersistent;
    }

    public void setPersistent(boolean persistence) {
	isPersistent = persistence;
    }

    public Hashtable<String,String> getParams(){
	return new Hashtable<String,String>(params);
    }

    public String getMessageString() {
	return new String (getBytes());
    }

    public String getMessageClass(){
	return this.getClass().getName().split(".*[.]")[1];
    }

    public byte[] getBytes() {
	StringBuffer sb = new StringBuffer();
	sb.append("s:"); if (source != null && !source.equals("")) sb.append(source); sb.append("\n");
	sb.append("d:"); if (dest != null && !dest.equals("")) sb.append(dest); sb.append("\n");
	sb.append("t:"); sb.append(messageType); sb.append("\n");

	for (String name: params.keySet()){
	    String value = params.get(name);
	    if (!name.equals("")) { 
		sb.append(name); sb.append(":"); sb.append(value); sb.append("\n"); 
	    }
	}

	messageLength = sb.length() + 4;
	byte[] lenBytes = intToByteArray(messageLength);
	byte[] stringBytes = sb.toString().getBytes();
	byte[] packetBytes = new byte[stringBytes.length + 4];
	for (int i=4; i<packetBytes.length; i++) {
	    packetBytes[i] = stringBytes[i-4];
	}
	packetBytes[0] = lenBytes[0]; packetBytes[1] = lenBytes[1]; packetBytes[2] = lenBytes[2]; packetBytes[3] = lenBytes[3];
	return packetBytes;
    }

    public String serialize() {
	byte[] msgBytes = getBytes();
	String str = new String(msgBytes, 4, msgBytes.length - 4);
	str = str.replace('\n', ';');
	return str;
    }

    public static IPCMessage deserialize(String str, Class<?> messageFactory) {
	try {
	    Method m = messageFactory.getDeclaredMethod("createMessage", new Class[]{String.class});
	    IPCMessage message = (IPCMessage)(m.invoke(null, new Object[]{str.replace(';', '\n')}));
	    return message;
	} catch(Exception e) {
	    LogUtilities.getDefaultLogger().error("unable to deserialize", e);
	    return null;
	}
    }

    public static byte[] intToByteArray(int i) {
	ByteBuffer byteBuffer = ByteBuffer.allocate(4);
	byteBuffer.putInt(i);
	return byteBuffer.array();
    }
   
    public static int byteArrayToInt(byte[] b) {
	ByteBuffer byteBuffer = ByteBuffer.allocate(4);
	byteBuffer.put(b);
	return byteBuffer.getInt(0);
    }

    public void dumpMessage() {
	logger.info("message dump:" + getMessageString());
    }

}
