package org.gramvaani.simpleipc;

public interface IPCNode {

    public int handleIncomingMessage(IPCMessage message);
    public int handleNonBlockingMessage(IPCMessage message);
    public String getName();

}
