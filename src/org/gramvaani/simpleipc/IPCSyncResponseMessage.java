package org.gramvaani.simpleipc;

import org.gramvaani.utilities.*;

import java.util.*;

public class IPCSyncResponseMessage extends IPCMessage {

    public IPCSyncResponseMessage() {
	super();
    }

    public IPCSyncResponseMessage(String source, String dest){
	super(source, dest);
    }

    public IPCSyncResponseMessage(String source, String dest, Hashtable<String,String> params){
	super(source, dest, params);
    }

    public IPCSyncResponseMessage(String source, String dest, String[][] args) {
	super(source, dest, args);
    }

    public IPCSyncResponseMessage(String messageString) {
	super(messageString);
    }


    public void setMessageID(String messageID) {
	params.put("messageid", messageID);
    }

    public String getMessageID() {
	return params.get("messageid");
    }

}