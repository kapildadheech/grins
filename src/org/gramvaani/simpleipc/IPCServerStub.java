package org.gramvaani.simpleipc;

import org.gramvaani.simpleipc.*;
import org.gramvaani.utilities.*;
import org.gramvaani.radio.rscontroller.messages.*;

import java.util.*;
import java.net.*;
import java.io.*;
import java.nio.*;
import java.nio.charset.*;
import java.lang.reflect.*;
import java.util.concurrent.LinkedBlockingQueue;

public class IPCServerStub implements Runnable, IPCServer {
    private static final int BUFFER_SIZE = 8192;

    protected Thread thread, blockingWorker, nonBlockingWorker;
    protected String ipcServerAddress;
    protected int ipcServerPort;
    private Socket soc;
    private InputStream in;
    private OutputStream out;
    protected IPCNode ipcNode;
    protected Class<?> messageFactory;
    protected int syncMessageTimeout;

    protected Hashtable<String, Object> syncMessages;
    protected LinkedBlockingQueue<IPCMessage> blockingMessages;
    protected LinkedBlockingQueue<IPCMessage> nonBlockingMessages;

    private long totalBytesRead, maxBytesToRead;
    private byte[] readBuf;
    private StringBuffer sb;

    private boolean threadRun = true;
    private LogUtilities logger;
    protected PersistentMessageQueue persistentQueue = null;
    protected String persistentQueueDir;
    protected boolean resendingMessages;

    public IPCServerStub(String ipcServerAddress, int ipcServerPort, Class messageFactory, int syncMessageTimeout, String persistentQueueDir) {
	this.ipcServerAddress = ipcServerAddress;
	this.ipcServerPort = ipcServerPort;
	this.messageFactory = messageFactory;
	this.syncMessageTimeout = syncMessageTimeout;
	this.persistentQueueDir = persistentQueueDir;
	syncMessages = new Hashtable<String, Object>();
	blockingMessages = new LinkedBlockingQueue<IPCMessage>();
	nonBlockingMessages = new LinkedBlockingQueue<IPCMessage>();

	logger = new LogUtilities();
	logger.debug("IPCServerStub: Initialized");
	
	startWorkers();
    }

    
    public void run() {
	logger.debug("Run: Entering");
	sb = new StringBuffer();
	totalBytesRead = 0;
	maxBytesToRead = 4;
	readBuf = new byte[BUFFER_SIZE];

	int nBytes = 0;
	int bufPosition = 0, messageLength = 0;
	ByteBuffer readBuffer = ByteBuffer.allocate(10 * BUFFER_SIZE);

	try {
	    while ((nBytes = in.read(readBuf)) > -1){
		readBuffer.put(readBuf, 0, nBytes);
		
		bufPosition = readBuffer.position();
		while ((messageLength = readBuffer.getInt(0)) <= bufPosition &&
		       messageLength > 0 ){
		    byte[] tmpBuf = new byte[messageLength - 4];
		    readBuffer.position(4);
		    readBuffer.get(tmpBuf, 0, messageLength - 4);
		    readBuffer.position(messageLength);
		    readBuffer.compact();
		    bufPosition -= messageLength;
		    readBuffer.position(bufPosition);

		    if (addToMessageQueue(tmpBuf) == -1) {
			logger.error("Run: HandleIncomingMessage failed.");
			break;
		    }
		}
	    }
	    
	} catch (IOException e) {
	    logger.warn("Run: Server disconnected.");
	}

	if (threadRun) {
	    logger.error("Run: Calling connectionError. nBytes:"+nBytes+" messageLength:"+messageLength+" bufposition:"+bufPosition);
	    ((IPCNodeCallback)(ipcNode)).connectionError();	
	}else {
	    logger.debug("Run: Socket closed by disconnect. Leaving...");
	}
    }

    public synchronized int registerIPCNode(String name, IPCNode ipcNode, String id){
	return registerIPCNode(name, ipcNode, id, false);
    }

    public synchronized int registerIPCNode(String name, IPCNode ipcNode, String id, boolean suppressException) {
	this.ipcNode = ipcNode;

	if (persistentQueue == null) {
	    try {
		persistentQueue = new PersistentMessageQueue(name+"_IPCServer", persistentQueueDir , messageFactory);
		addQueueCloseHook();
	    } catch(Exception e) {
		logger.error("RegisterIPCNode: Could not create persistent queue. Persistent message delivery shall not be possible.", e);
		persistentQueue = null;
	    }
	}

	try {
	    logger.debug("RegisterIPCNode: Opening socket to server: "+ipcServerAddress+":"+ipcServerPort);
	    soc = new Socket(ipcServerAddress, ipcServerPort);
	    in = soc.getInputStream();
	    out = soc.getOutputStream();
	} catch(IOException e) {
	    if (suppressException)
		logger.error("RegisterIPCNode: Unable to open connection: "+ipcNode.getName());
	    else
		logger.error("RegisterIPCNode: Unable to open connection: "+ipcNode.getName(), e);
	    return -1;
	}

	logger.debug("RegisterIPCNode: Socket connection established successfully.");

	long curTime = System.currentTimeMillis();
	thread = new Thread(this);
	thread.setName(name+":IPCServerStub Thread");
	logger.setName(name+":IPCServerStub:"+curTime);


	thread.start();

	RegisterMessage message = new RegisterMessage(name, id);
	return handleOutgoingMessage(message);
    }

    protected void addQueueCloseHook() {
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    logger.info("ShutdownHook: Closing persistent queue");
		    persistentQueue.close();
		}
	    });
    }

    public synchronized void disconnect(String name){
	logger.debug("Disconnect: Stopping connection with the server.");
	//persistentQueue.close();
	threadRun = false;
	try {
	    if (soc != null)
		soc.close();
	} catch(IOException e) {
	    logger.warn("Disconnect: Caught IOException while closing the socket.");
	}
    }

    protected void startWorkers(){
	blockingWorker = new Thread(){
		public void run(){
		    while (true){
			try{
			    IPCMessage message = blockingMessages.take();
			    logger.debug("BlockingWorker:Run: About to process message of type: "+message.getMessageClass());
			    handleIncomingMessage(message);
			} catch (Exception e){
			    logger.warn("BlockingWorker: Run: Thread interrupted. ", e);
			}
		    }
		}
	    };

	nonBlockingWorker = new Thread(){
		public void run(){
		    while (true){
			try{
			    IPCMessage message = nonBlockingMessages.take();
			    logger.debug("NonBlockingWorker:Run: About to process message of type: "+message.getMessageClass());
			    ipcNode.handleNonBlockingMessage(message);
			} catch (Exception e){
			    logger.warn("NonBlockingWorker:Run: Thread interrupted. ", e);
			}
			
		    }
		}
	    };

	blockingWorker.start();
	nonBlockingWorker.start();
    }

    protected int addToMessageQueue(byte[] messageBytes){
	IPCMessage message = null;
	try {
	    Method m = messageFactory.getDeclaredMethod("createMessage", 
							new Class[] {String.class});
	    message = (IPCMessage)(m.invoke (null,
					     new Object[] {new String(messageBytes)}));
	    
	} catch(Exception e1) {
	    logger.error("AddToMessageQueue: Could not create message.", e1);
	    return -1;
	}

	if (message instanceof HeartbeatMessage) {
	    nonBlockingMessages.add(message);
	} else if (message instanceof IPCSyncResponseMessage) {
	    IPCSyncResponseMessage syncResponseMessage = (IPCSyncResponseMessage)message;
	    String messageID = syncResponseMessage.getMessageID();

	    synchronized(syncMessages) {
		Object syncObject = syncMessages.remove(messageID);
		
		if (syncObject != null) {
		    synchronized(syncObject) {
			logger.debug("HandleIncomingMessage: SyncResponseMessage: "+syncResponseMessage.getMessageID()+" obj: "+syncObject);
			syncMessages.put(syncResponseMessage.getMessageID(), syncResponseMessage);
			syncObject.notifyAll();
		    }
		} else {
		    logger.error("HandleIncomingMessage: Timeout for "+syncResponseMessage.getMessageID()+ " type:"+syncResponseMessage.getMessageClass());
		}
	    }
	} else {	    
	    blockingMessages.add(message);
	}
	
	return 0;
    }

    protected int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Received message from: "+message.getSource()+" of type: "+message.getMessageClass());	
	ipcNode.handleIncomingMessage(message);
	
	return 0;
    }

    public synchronized IPCSyncResponseMessage handleOutgoingSyncMessage(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	return handleOutgoingSyncMessage(message, -1);
    }

    public synchronized IPCSyncResponseMessage handleOutgoingSyncMessage(IPCSyncMessage message, long timeout) throws IPCSyncMessageTimeoutException {
	if (timeout < 0)
	    timeout = syncMessageTimeout;

	logger.debug("HandleOutgoingSyncMessage: source:" + message.getSource() + " destination:" + message.getDest() + " type:" + message.getMessageClass());
	byte[] messageBytes = message.getBytes();

	if (messageBytes.length > BUFFER_SIZE)
	    logger.error("HandleIncomingMessage: Attempting to send a large message of length: "+messageBytes.length);

	String messageID = message.getMessageID();
	Object syncObject = new Object();
	syncMessages.put(messageID, syncObject);

	long currentTime;
	long endTime;
	try {
	    currentTime = System.currentTimeMillis();
	    
	    synchronized(syncObject) {
		try {
		    synchronized(out){
			out.write(messageBytes);
		    }
		} catch(IOException ex) {
		    logger.error("HandleOutgoingSyncMessage: Server Disconnected.");
		    return null;
		}
		
		logger.debug("HandleOutGoingSyncMessage: MessageID: "+message.getMessageID()+" obj: "+syncObject+" length: "+messageBytes.length+" type: "+message.getMessageClass());
		
		syncObject.wait(timeout);
		
		//XXX race condition when message has arrived, and syncObj is retrieved before next line.
		
		synchronized(syncMessages){
		    endTime = System.currentTimeMillis();
		    if (Math.abs(endTime - currentTime - timeout) < 20) {
			syncMessages.remove(messageID);
			throw new IPCSyncMessageTimeoutException("timeout expired");
		    }
		}
		
	    }
	    
	    logger.debug("HandleOutGoingSyncMessage: Time taken: "+(endTime - currentTime));
	    logger.debug("HandleOutGoingSyncMessage: Response received. ID:"+message.getMessageID());

	    //syncStates.remove(message.getMessageID());

	    IPCSyncResponseMessage responseMessage = null;
	    Object msgObject = syncMessages.remove(message.getMessageID());

	    if (msgObject instanceof IPCSyncResponseMessage)
		responseMessage = (IPCSyncResponseMessage) msgObject;

	    return responseMessage;

	} catch(InterruptedException e) {
	    logger.warn("handleOutgoingSyncMessage: thread interrupted -- " + message.getMessageID());
	    return null;
	}
    }

    public int handleOutgoingMessage(IPCMessage message) {
	if (!message.getMessageClass().equals("HeartbeatMessage"))
	    logger.debug("HandleOutgoingMessage: " + message.getSource() + " to:" + message.getDest() + " type:" + message.getMessageClass());
	byte[] messageBytes = message.getBytes();


	try {
	    if (out != null)
		synchronized(out){
		    out.write(messageBytes);
		}
	    else {
		logger.error("HandleOutgoingMessage: Socket output stream is null.");
		if (message.isPersistent() && persistentQueue != null && !resendingMessages) {
		    persistentQueue.add(message);
		}
		return -1;
	    }
	} catch(IOException ex) {
	    logger.error("HandleOutgoingMessage: Server Disconnected. MessageType: "+message.getMessageClass()+" Dest: "+message.getDest());
	    if (message.isPersistent() && persistentQueue != null && !resendingMessages) {
		persistentQueue.add(message);
	    }
	    return -1;
	}
	logger.debug("HandleOutgoingMessage: Leaving");
	return 0;
    }

    
    public static IPCServer initConnection(String ipcServerAddress, int ipcServerPort, 
					   Class messageFactory, IPCNode ipcNode, int retries, 
					   int syncMessageTimeout, String registrationID, String persistentQueueDir) {

	for (int i =  0; i < retries; i++) {
	    IPCServerStub ipcServerStub = new IPCServerStub(ipcServerAddress, ipcServerPort, messageFactory, syncMessageTimeout, persistentQueueDir);
	    if (ipcServerStub.registerIPCNode(ipcNode.getName(), ipcNode, registrationID, true) > -1) {
		LogUtilities.getDefaultLogger().info("IPCServerStub: InitConnection: Reconnection Successful.");
		return ipcServerStub;
	    }

	    try {
		Thread.sleep(1000);
	    } catch(InterruptedException e) {
		// do nothing
	    }
	}

	LogUtilities.getDefaultLogger().error("IPCServerStub: InitConnection: Reconnection failed after "+retries+" attempts.");
	return null;
    }

    /*
      Destination field not used in stub
      XXX: Explicit prioritization of persistent messages over other messages on reconnection 
      has not been done here. Since the call is made on registration ack by the
      handle incoming thread itself, we should be safe. But this may break in the future.
    */
    public synchronized void sendPersistentMessages(String destination) {
	if (persistentQueue == null) {
	    return;
	}
	resendingMessages = true;
	IPCMessage message = persistentQueue.peekFirst();
	while (message != null) {
	    if (handleOutgoingMessage(message) == 0) {
		persistentQueue.removeFirst();
	    } else {
		logger.error("SendPersistentMessages: HandleOutgoingMessage failed. Calling connection error and giving up.");
		resendingMessages = false;
		((IPCNodeCallback)(ipcNode)).connectionError();
		return;
	    }
	    message = persistentQueue.peekFirst();
	}
	resendingMessages = false;
    }

    //Not used in the stub
    public void startServer() { }
}
