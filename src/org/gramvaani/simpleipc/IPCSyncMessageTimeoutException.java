package org.gramvaani.simpleipc;

public class IPCSyncMessageTimeoutException extends Exception {

    public IPCSyncMessageTimeoutException(String exceptionMessage) {
	super(exceptionMessage);
    }

}