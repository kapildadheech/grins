package org.gramvaani.simpleipc;

import org.gramvaani.utilities.*;

import java.util.*;

public class IPCSyncMessage extends IPCMessage {
    public final static String SYNC_MESSAGE = "SYNC_MESSAGE";

    public IPCSyncMessage() {
	super();
	String messageID = GUIDUtils.getGUID();
	params.put("messageid", messageID);
    }

    public IPCSyncMessage(String source, String dest){
	super(source, dest);
	String messageID = GUIDUtils.getGUID();
	params.put("messageid", messageID);
    }

    public IPCSyncMessage(String source, String dest, Hashtable<String,String> params){
	super(source, dest, params);
	String messageID = GUIDUtils.getGUID();
	params.put("messageid", messageID);
    }

    public IPCSyncMessage(String source, String dest, String[][] args){
	super(source, dest, args);
	String messageID = GUIDUtils.getGUID();
	params.put("messageid", messageID);
    }

    public IPCSyncMessage(String messageString) {
	super(messageString);
    }

    public String getMessageID() {
	return params.get("messageid");
    }

}