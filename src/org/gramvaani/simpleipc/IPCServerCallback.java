package org.gramvaani.simpleipc;

public interface IPCServerCallback {

    public void newIPCNode(String name, Boolean isRemote, String registrationID);
    public void nodeError(String name);
    public void serverFailed();

}
