package org.gramvaani.simpleipc;

import java.util.*;
import java.util.concurrent.*;
import java.nio.channels.*;
import java.net.*;
import java.io.*;

import org.gramvaani.utilities.*;
import org.gramvaani.radio.rscontroller.*;
import org.gramvaani.radio.rscontroller.messages.*;

public class IPCServerImpl implements Runnable, IPCServer {
    public static final String THREAD_NAME = "IncomingMessageWorker";
    protected Thread thread;
    protected int port;
    private Hashtable<SelectionKey, IPCNodeStub> ipcNodeStubs;
    private Hashtable<String, IPCNode> ipcNodes;
    private Hashtable<String, OutgoingMessageWorker> messageWorkers;
    private Hashtable<String, Object> syncMessages;
    private Selector selector;
    private SelectionKey serverKey;
    private ServerSocketChannel server;
    protected IPCServerCallback callback;
    protected Class messageFactory;
    int syncMessageTimeout;

    protected LogUtilities logger;
    protected final Hashtable<String, PersistentMessageQueue> persistentQueues = new Hashtable<String, PersistentMessageQueue>();
    protected String persistentQueueDir = "/tmp";
    protected ArrayList<String> persistentQueueDest;

    public IPCServerImpl(int port, IPCServerCallback callback, Class messageFactory, int syncMessageTimeout, String persistentQueueDir, ArrayList<String> persistentQueueDestinations) {
	this.port = port;
	this.callback = callback;
	this.syncMessageTimeout = syncMessageTimeout;
	this.persistentQueueDir = persistentQueueDir;
	ipcNodeStubs = new Hashtable<SelectionKey, IPCNodeStub>();
	ipcNodes = new Hashtable<String, IPCNode>();
	messageWorkers = new Hashtable<String, OutgoingMessageWorker>();
	syncMessages = new Hashtable<String, Object>();
	this.messageFactory = messageFactory;
	thread = new Thread(this);
	logger = new LogUtilities("IPCServerImpl");
	persistentQueueDest = persistentQueueDestinations;
	addPersistentQueues();
	logger.debug("IPCServerImpl: Initialized");
	//thread.start();

	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run() {
		    logger.info("ShutdownHook: Closing persistent queues");
		    PersistentMessageQueue queue;
		    for (String dest : persistentQueueDest) {
			queue = persistentQueues.get(dest);
			if (queue != null) {
			    queue.close();
			}

		    }
		}
	    });
    }

    public void startServer() {
	thread.start();
    }

    public void run() {
	logger.debug("Run: Entering");
	try {
	    selector = Selector.open();
	    server = ServerSocketChannel.open();
	    server.configureBlocking(false);
	    server.socket().bind(new InetSocketAddress(port));
	    serverKey = server.register(selector, SelectionKey.OP_ACCEPT);
	} catch(IOException e) {
	    // XXX Unable to start server
	    logger.error("Run: Unable to start server.", e);
	    callback.serverFailed();
	}
	logger.debug("Run: Server bound to port "+port);

	while (true) {
	    try {
		selector.select();
		Set keys = selector.selectedKeys();
		for (Iterator i = keys.iterator(); i.hasNext(); ) {
		    SelectionKey key = (SelectionKey)(i.next());
		    i.remove();

		    if (key == serverKey) {
			logger.debug("Run: Server key");
			if (key.isAcceptable()) {
			    try {

				SocketChannel client = server.accept();
				client.configureBlocking(false);
				SelectionKey clientKey = client.register(selector, SelectionKey.OP_READ, new StringBuffer());
				newIPCNode(client, clientKey);

			    } catch(IOException e1) {
				logger.error("Run: Unable to process connection request.", e1);
				continue;
			    } 
			} else {
			    logger.error("Run: Invalid socket input at server.");
			    continue;
			}
		    }
		    else {
			logger.debug("Run: NodeStub key");
			IPCNodeStub ipcNodeStub = ipcNodeStubs.get(key);
			if (!key.isValid()) {
			    logger.error("Run: Error in key, Client got disconnected.");
			    disconnect(key, ipcNodeStub);
			    continue;
			}
			
			try {
			    ipcNodeStub.handleChannelRead();
			} catch(IOException e) {
			    logger.error("Run: Error in handleChannelRead: Client got disconnected");
			    callback.nodeError(ipcNodeStub.getName());
			    //disconnect(key, ipcNodeStub);
			}
		    }
		}
	    } catch(IOException e3) {
		logger.fatal("Fatal Error in Server", (Exception)e3);
		callback.serverFailed();
	    }
	}
	
    }

    private synchronized void newIPCNode(SocketChannel client, SelectionKey clientKey) {
	IPCNodeStub ipcNodeStub = new IPCNodeStub(client, this, clientKey, messageFactory);
	ipcNodeStubs.put(clientKey, ipcNodeStub);
	logger.info("NewIPCNode: Added IPCNode: "+ipcNodeStub.getName());
    }

    public synchronized void disconnect(SelectionKey key, IPCNodeStub ipcNodeStub) {
	logger.debug("Disconnect: Entering");
	if (key != null) {
	    ipcNodeStubs.remove(key);
	    key.cancel();
	} else {
	    logger.error("Disconnect: Trying to disconnect null key");
	}

	if (ipcNodeStub != null) {
	    ipcNodes.remove(ipcNodeStub.getName());
	    ipcNodeStub.disconnect();
	    logger.info("Disconnect: Disconnected node: "+ipcNodeStub.getName());
	}else {
	    logger.info("Disconnect: NodeStub was null");
	}
	logger.debug("Disconnect: Leaving");
    }
    
    public synchronized void disconnect(String serviceName){
	logger.debug("Disconnect: Entering.");

	for (SelectionKey key: ipcNodeStubs.keySet()){
	    IPCNodeStub searchIPCNode = ipcNodeStubs.get(key);
	    if (serviceName.equals(searchIPCNode.getName())) {
		logger.info("Disconnect: Required node found. Disconnecting...");
		disconnect(key, searchIPCNode);
		logger.debug("Disconnect: done. Leaving");
		return;
	    }
	}
    }

    private synchronized void disconnect(IPCNode ipcNode) {
	logger.debug("Disconnect: Entering");

	for (SelectionKey key: ipcNodeStubs.keySet()){
	    IPCNodeStub searchIPCNode = ipcNodeStubs.get(key);
	    if (ipcNode == searchIPCNode) {
		logger.debug("Disconnect: Required node found. Disconnecting...");
		disconnect(key, (IPCNodeStub)(ipcNode));
		logger.debug("Disconnect: done. Leaving");
		return;
	    }
	}

	// should never come here because local ipc node should never generate an IO error in handling messages
	logger.error("Local ipc node created IO error: " + ipcNode.getName());
    }

    public synchronized int registerIPCNode(String name, IPCNode ipcNode, Boolean isRemote, String registrationID) {
	logger.info("Registering Node: " + name);
	ipcNodes.put(name, ipcNode);
	OutgoingMessageWorker worker = messageWorkers.get(name);

	if (worker == null) {
	    messageWorkers.put(name, new OutgoingMessageWorker(ipcNode, this));
	} else {
	    worker.resetWorker();
	    worker.setIPCNode(ipcNode);
	}

        callback.newIPCNode(name, isRemote, registrationID);
	logger.debug("RegisterIPCNode: Leaving");
	return 0;
    }

    //Not used in IPCServerImpl
    public synchronized int registerIPCNode(String name, IPCNode ipcNode, String registrationID, boolean suppressException) {
	return -1;
    }

    protected synchronized void addPersistentQueues() {

	for (String dest : persistentQueueDest) {
	    logger.info("AddPersistentQueue: Adding queues for destination: " + dest);
	    
	    try {
		PersistentMessageQueue queue = new PersistentMessageQueue(dest, 
									  persistentQueueDir, messageFactory);
		persistentQueues.put(dest, queue);
	    } catch(Exception e) {
		logger.error("AddPersistentQueue: Failed to create persistent queue: " + dest + " queueDir: " + persistentQueueDir, e);
	    }
	}
    }

    public synchronized int registerIPCNode(String name, IPCNode ipcNode, String registrationID) {
	return registerIPCNode(name, ipcNode, false, registrationID);
    }

    public IPCSyncResponseMessage handleOutgoingSyncMessage(IPCSyncMessage message) throws IPCSyncMessageTimeoutException {
	return handleOutgoingSyncMessage(message, -1);
    }

    public IPCSyncResponseMessage handleOutgoingSyncMessage(IPCSyncMessage message, long timeout) throws IPCSyncMessageTimeoutException {
	if (timeout < 0)
	    timeout = syncMessageTimeout;

	String dest = message.getDest();
	if (dest != null) {
	    IPCNode ipcNode = ipcNodes.get(dest);
	    if (ipcNode != null) {
		logger.info("HandleOutgoingSyncMessage: "+message.getSource()+" to: "+message.getDest() + " type: "+ message.getMessageClass());
		String messageID = message.getMessageID();
		Object syncObject = new Object();
		synchronized(this) {
		    syncMessages.put(messageID, syncObject);
		}
		try {
		    long currentTime = System.currentTimeMillis();
		    synchronized(syncObject) {
			messageWorkers.get(dest).handleMessage(message);
			logger.debug("HandleOutGoingSyncMessage: Message sent to message worker.");
			syncObject.wait(timeout);
		    }
		    if (Math.abs(System.currentTimeMillis() - currentTime - timeout) < 20) {
			throw new IPCSyncMessageTimeoutException("timeout expired");
		    }
		    IPCSyncResponseMessage responseMessage = (IPCSyncResponseMessage)(syncMessages.get(message.getMessageID()));
		    logger.debug("HandleOutGoingSyncMessage: Response received.");
		    return responseMessage;
		} catch(InterruptedException e) {
		    logger.warn("handleOutgoingSyncMessage: thread interrupted: " + message.getMessageID());
		    return null;
		}
	    } else {
		logger.error("HandleOutgoingSyncMessage: Unable to find dest: " + dest);
		logger.error(message.getMessageString());
		return null;
	    }
	}

	logger.error("HandleOutgoingSyncMessage: dest is null");
	return null;
    }

    public synchronized int handleOutgoingMessage(IPCMessage message) {
	String dest = message.getDest();
	if (dest != null && dest.equals(IPCMessage.DEST_BCAST)) {
	    logger.debug("HandleOutgoingMessage: Broadcast message from: "+message.getSource()+". Sending to all nodes.");
	    for (IPCNode ipcNode: ipcNodes.values()){
		messageWorkers.get(ipcNode.getName()).handleMessage(message);
	    }
	}
	else if (dest != null) {
	    IPCNode ipcNode = ipcNodes.get(dest);
	    if (ipcNode != null) {
		if (!message.getMessageClass().equals("HeartbeatMessage"))
		    logger.info("HandleOutgoingMessage: Message from: "+message.getSource()+" to: "+message.getDest() + " type: "+ message.getMessageClass());
		messageWorkers.get(ipcNode.getName()).handleMessage(message);
		logger.debug("HandleOutGoingMessage: Message delivered.");

	    }else {
		logger.error("HandleOutgoingMessage: Unable to find dest: " + dest);
		logger.error(message.getMessageString());
		if (message.isPersistent()) {
		    PersistentMessageQueue queue = persistentQueues.get(message.getDest());
		    if (queue != null) {
			queue.add(message);
		    } else {
			logger.error("HandleOutgoingMessage: Could not find persistent queue for : " + message.getDest());
		    }
		}
		return -2;
	    }	    
	}
	logger.debug("HandleOutgoingMessage: Leaving");
	return 0;
    }
    
    public void sendPersistentMessages(String dest) {
	OutgoingMessageWorker worker = messageWorkers.get(dest);
	if (dest != null) {
	    worker.sendPersistentMessages(dest);
	}
    }

    class OutgoingMessageWorker extends Thread implements Comparator<IPCMessage> {
	IPCNode ipcNode;
	IPCServerImpl ipcServer;
	boolean running = true;
	Thread thread;
	boolean pendingMessagesSent = false;
	int MESSAGE_WORKER_SLEEP_TIME = 100;
	int QUEUE_INITIAL_CAPACITY = 20;
	PriorityQueue<IPCMessage> messageQueue;
	
	public OutgoingMessageWorker (IPCNode ipcNode, IPCServerImpl ipcServer){
	    this.ipcNode = ipcNode;
	    this.ipcServer = ipcServer;
	    messageQueue = new PriorityQueue<IPCMessage>(QUEUE_INITIAL_CAPACITY, this);
	    thread = new Thread(this, ipcNode.getName()+":"+THREAD_NAME);
	    thread.start();
	}

	public void setIPCNode(IPCNode ipcNode) {
	    this.ipcNode = ipcNode;
	}

	public void handleMessage (IPCMessage message){
	    Object syncObject = null;
	    IPCSyncResponseMessage syncResponseMessage = null;

	    if (message instanceof IPCSyncResponseMessage) {
		syncResponseMessage = (IPCSyncResponseMessage)message;
		syncObject = syncMessages.get(syncResponseMessage.getMessageID());
	    }

	    if (syncObject != null) {
		synchronized(syncObject) {
		    syncMessages.put(syncResponseMessage.getMessageID(), syncResponseMessage);
		    syncObject.notifyAll();
		}
	    } else {
		synchronized(messageQueue) {
		    messageQueue.add(message);

		    if (pendingMessagesSent || (getPriority(message) == SEND_IMMEDIATELY) || 
		       (ipcNode != null && ipcNode.getName().equals(RSController.RESOURCE_MANAGER))) 
			messageQueue.notifyAll();
		}
	    }
	}

	public void resetWorker() {
	    pendingMessagesSent = false;
	}

	public void sendPersistentMessages(String dest) {
	    PersistentMessageQueue queue;

	    queue = persistentQueues.get(dest);
	    if (!sendMessagesFromPersistentQueue(queue)) {
		return;
	    }

	    pendingMessagesSent = true;
	    return;
	}

	public boolean sendMessagesFromPersistentQueue(PersistentMessageQueue persistentQueue) {
	    if (persistentQueue == null) {
		return true;
	    }
	    
	    IPCMessage message = persistentQueue.peekFirst();
	    while (message != null) {
		if (ipcNode != null && ipcNode.handleIncomingMessage(message) == 0) {
		    logger.info("SendMessagesFromPersistentQueue: Message sent. source=" + message.getSource() + " dest=" + message.getDest() + " : " +message.getMessageClass());
		    persistentQueue.removeFirst();
		} else {
		    logger.error("SendMessageFromPersistentQueue: Client disconnected: " + message.getDest() + " Could not send message.");
		    if (ipcNode != null)
			callback.nodeError(ipcNode.getName());
		    else
			logger.error("SendMessageFromPersistentQueue: IPCNode is null. Could not make a callback.");
		    return false;
		}
		message = persistentQueue.peekFirst();
	    }

	    return true;
	}

	protected boolean sendMessage(IPCMessage message) {

	    if (ipcNode == null || ipcNode.handleIncomingMessage(message) == -1) {
		logger.error("SendMessage: Client disconnected: "+message.getDest()+" Could not send message.");
		if (message.isPersistent()) {
		    PersistentMessageQueue queue = persistentQueues.get(message.getDest());
		    if (queue != null) {
			queue.add(message);
		    }
		}
		if (ipcNode != null)
		    callback.nodeError(ipcNode.getName());

		return false;
	    } else {
		logger.debug("SendMessage: Message sent successfully.");
		return true;
	    }
	}

	public int compare(IPCMessage msg1, IPCMessage msg2) {
	    int priorityDiff = getPriority(msg1) - getPriority(msg2);
	    
	    if (priorityDiff != 0)
		return priorityDiff;
	    else
		return (int) (msg1.getLocalSequence() - msg2.getLocalSequence());
	    
	}

	public boolean equals(Object obj) {
	    return (obj == this);
	}

	private boolean sendBeforePersistentMsgs(IPCMessage message) {
	    if ((message instanceof RegistrationAckMessage) ||
		(message instanceof HeartbeatMessage) ||
		(message instanceof ServiceReadyAckMessage) ||
		(message instanceof ServiceReadyMessage) ||
		(message instanceof ServiceAckMessage) ||
		(message instanceof ServiceNotifyMessage) ||
		(message instanceof ServiceActivateNotifyMessage) ||
		(message instanceof StartCleanupMessage) ||
		(message instanceof ServiceErrorRelayMessage))
		return true;
	    else
		return false;
	}

	/*
	  Note that while specifying ServiceNotifyMessage as a high priority message is required as
	  service readiness can depend on it, if a service is doing too much in response to a notify message
	  then those activites can potentially happen before persistent message are received by it.
	*/
	public static final int SEND_IMMEDIATELY = 1;
	public static final int SEND_AFTER_PERSISTENT_MSGS = 2;
	protected int getPriority(IPCMessage message) {
	    if(sendBeforePersistentMsgs(message))
		return SEND_IMMEDIATELY;
	    else
		return SEND_AFTER_PERSISTENT_MSGS;
	}

	public void run() {
	    while (true) {
		IPCMessage message = null;
		
		synchronized(messageQueue) {
		    if (messageQueue.size() == 0) {
			try {
			    messageQueue.wait();
			} catch (Exception e) {
			    logger.error("Run: Caught exception while waiting for messages.", e);
			}
		    }

		    message = messageQueue.poll();
		} 
		
		if (message != null && (pendingMessagesSent || (getPriority(message) == SEND_IMMEDIATELY) || 
				       (ipcNode != null && ipcNode.getName().equals(RSController.RESOURCE_MANAGER))))
		    sendMessage(message);
		
	    }
	} 
    }
}
