package org.gramvaani.simpleipc;

public interface IPCServer {

    public int registerIPCNode(String name, IPCNode ipcNode, String registrationID);
    public int registerIPCNode(String name, IPCNode ipcNode, String registrationID, boolean suppressException);
    public int handleOutgoingMessage(IPCMessage message);
    public IPCSyncResponseMessage handleOutgoingSyncMessage(IPCSyncMessage message) throws IPCSyncMessageTimeoutException;
    public IPCSyncResponseMessage handleOutgoingSyncMessage(IPCSyncMessage message, long timeout) throws IPCSyncMessageTimeoutException;
    public void disconnect(String serviceName);
    public void startServer();
    public void sendPersistentMessages(String dest);
}
