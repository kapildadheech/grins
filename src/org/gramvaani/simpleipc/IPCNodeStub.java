package org.gramvaani.simpleipc;

import java.nio.channels.*;
import java.nio.*;
import java.nio.charset.*;
import java.io.*;
import java.lang.reflect.*;

import org.gramvaani.utilities.*;
import org.gramvaani.radio.rscontroller.messages.*;
import org.gramvaani.radio.rscontroller.*;

public class IPCNodeStub implements IPCNode {
    private static final int BUFFER_SIZE = 10 * 8192;
    private static final long CHANNEL_WRITE_SLEEP = 10L;
    private CharsetDecoder asciiDecoder;
    private long totalBytesRead, maxBytesToRead;
    private ByteBuffer readBuffer, writeBuffer;
    private StringBuffer sb;

    protected IPCServerImpl ipcServer;
    private SocketChannel client;
    private SelectionKey clientKey;
    protected String name = "";
    protected String registrationID = "";
    protected Class<?> messageFactory;
    private LogUtilities logger;
   
    public IPCNodeStub(SocketChannel client, IPCServerImpl ipcServer, SelectionKey clientKey, Class messageFactory) {
        this.ipcServer = ipcServer;
	this.client = client;
	this.clientKey = clientKey;
	this.messageFactory = messageFactory;
	asciiDecoder = Charset.forName("US-ASCII").newDecoder();
	totalBytesRead = 0;
	maxBytesToRead = 4;
	readBuffer = ByteBuffer.allocate(BUFFER_SIZE);
	logger = new LogUtilities();
	logger.debug("IPCNodeStub: Initialized.");
    }

    public void handleChannelRead() throws IOException {
	logger.debug("HandleChannelRead: Entering");
	int messageLength = 0;
	try {
	    if (client.read(readBuffer) == -1)
		throw new IOException();
	    int bufPosition = readBuffer.position();

	    while ((messageLength = readBuffer.getInt(0)) <= bufPosition &&
		   messageLength > 0 ){
		byte[] tmpBuf = new byte[messageLength - 4];
		readBuffer.position(4);
		readBuffer.get(tmpBuf, 0, messageLength - 4);
		readBuffer.position(messageLength);
		readBuffer.compact();
		bufPosition -= messageLength;
		readBuffer.position(bufPosition);
		handleOutgoingMessage(tmpBuf);
		logger.debug("HandleChannelRead: Channel read successfully");
	    }

	}catch (Exception e){
	    logger.error("HandleChannelRead: Unable to read.", e);
	    throw new IOException ("HandleChannelRead: Unable to read.");
	}
	logger.debug("HandleChannelRead: Leaving");
    }

    protected void handleOutgoingMessage(byte[] messageBytes) throws Exception
    {
	logger.debug("HandleOutgoingMessage: Entering");
	IPCMessage message = null;
	try {

	    message = MessageFactory.createMessage(new String(messageBytes));

	    if (message.getMessageType() == IPCMessage.REGISTER_MESSAGE) {
		RegisterMessage msg = new RegisterMessage(message.getMessageString());
		name = message.getSource();
		registrationID = msg.getRegistrationID();
		logger.setName(name+":IPCNodeStub");
		ipcServer.registerIPCNode(message.getSource(), this, true, registrationID);
	    } else {
		if (ipcServer.handleOutgoingMessage(message) == -2) {
		    logger.error("Unable to deliver message to: "+message.getDest()+". Destination not found.");
		    //Not using service error message anymore as service notify message - inactive service -
		    //delivers the required information before this.
		    /*ServiceErrorMessage errorMessage = new ServiceErrorMessage(RSController.IPC_SERVER, 
									       message.getSource(), "destination not found",
									       ServiceErrorMessage.NOT_FOUND, message.serialize());
									       handleIncomingMessage(errorMessage);*/
		}
	    }
	} catch(Exception e1) {
	    logger.error("HandleOutgoingMessage: Could not create message.", e1);
	    throw new Exception(e1);
	}
	logger.debug("HandleOutGoingMessage: Leaving");
    }

    // disconnection from the remote ipc node
    public void disconnect() {
	if (client != null) {
	    try {
		client.close();
	    } catch(IOException e) {
		logger.error("Disconnect: Client closed.");
	    }
	    client = null;
	}
    }

    public int handleNonBlockingMessage(IPCMessage message){
	return 0;
    }

    // messages coming in from the ipc server to be written out to remote ipc node
    public int handleIncomingMessage(IPCMessage message) {
	logger.debug("HandleIncomingMessage: Entering: "+message.getMessageClass());
	if ((client == null) || !(client.isConnected() && client.isOpen())){
	    logger.error("HandleIncomingMessage: Client disconnected.");
	    return -1;
	}
	byte[] messageBytes = message.getBytes();

	if (messageBytes.length > BUFFER_SIZE)
	    logger.error("HandleIncomingMessage: Attempting to send a large message of length: "+messageBytes.length);

	writeBuffer = ByteBuffer.allocate(messageBytes.length);
	writeBuffer.put(messageBytes);
	long totalBytesWritten = 0;
	long maxBytesToWrite = messageBytes.length;
	writeBuffer.rewind();
	try {
	    while (totalBytesWritten < maxBytesToWrite) {
		totalBytesWritten += client.write(writeBuffer);
	    }
	    try {
		Thread.sleep(CHANNEL_WRITE_SLEEP);
	    } catch(InterruptedException e) {
		logger.info("HandleIncomingMessage: Thread interrupted");
	    }
	} catch(IOException e) {
	    logger.error("HandleIncomingMessage: Client disconnected.", e);
	    return -1;
	}
	logger.debug("HandleIncomingMessage: Leaving");
	return 0;
    }

    public String getName() {
	return name;
    }

}
