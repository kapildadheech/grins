package org.gramvaani.linkmonitor;

import org.gramvaani.radio.rscontroller.messages.HeartbeatMessage;

public interface LinkMonitorServerListener {
    public void nodeUp(String node);
    public void nodeDown(String node);
    public int sendHeartbeatMessage(HeartbeatMessage heartbeatMessage);
}