package org.gramvaani.linkmonitor;

import org.gramvaani.radio.rscontroller.messages.HeartbeatMessage;

public interface LinkMonitorClientListener {
    public void linkUp();
    public void linkDown();
    public int sendHeartbeatMessage(HeartbeatMessage heartbeatMessage);
}