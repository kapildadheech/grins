package org.gramvaani.linkmonitor;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.messages.HeartbeatMessage;
import org.gramvaani.utilities.*;


import java.util.*;
import java.util.concurrent.*;

public class LinkMonitorServer implements Runnable {

    protected LinkMonitorServerListener listener;
    protected long timeout, refreshTime, heartbeatSequence;
    protected Thread thread;
    protected ConcurrentHashMap<String, NodeInfo> nodeInfos;
    protected StationConfiguration stationConfig;

    protected LogUtilities logger;

    public LinkMonitorServer (LinkMonitorServerListener listener, StationConfiguration stationConfig, String logPrefix){
	this.listener = listener;
	this.stationConfig = stationConfig;
	thread = new Thread(this, "LinkMonitorServer Thread");
	
	refreshTime = stationConfig.getIntParam(StationConfiguration.SERVER_REFRESH_TIME);
	timeout = stationConfig.getIntParam(StationConfiguration.SERVER_TIMEOUT);

	nodeInfos = new ConcurrentHashMap<String, NodeInfo>();
	logger = new LogUtilities("LinkMonitorServer");
	logger.debug("LinkMonitorServer: Initializing with refreshTime="+refreshTime+" timeout="+timeout);
    }

    public void start(){
	logger.debug("Start: Starting LinkMonitorServer");
	thread.start();
    }

    public void run (){
	while (true){
	    for (NodeInfo node: nodeInfos.values()){
		node.setHeartbeatSequenceSent(node.getHeartbeatSequenceReceived()+1);
		logger.debug("Run: Sending HeartbeatMessage to: "+node.getName()+" sequence no="+node.getHeartbeatSequenceSent());
		HeartbeatMessage message = new HeartbeatMessage("", node.getName(), new Long(node.getHeartbeatSequenceSent()).toString());
		listener.sendHeartbeatMessage(message);
		
	    }
	    threadSleep(timeout);
	    for (NodeInfo node: nodeInfos.values()){
		if (node.getHeartbeatSequenceSent() <= node.getHeartbeatSequenceReceived()){
		    if (!node.isNodeUp()){
			node.setNodeUp(true);
			listener.nodeUp (node.getName());
			logger.info("Run: Node up: " + node.getName());
			logger.debug("Run: Sequence sent="+node.getHeartbeatSequenceSent()+
				     " Sequence received="+node.getHeartbeatSequenceReceived());
		    }
		} else {
		    if (node.isNodeUp()){
			node.setNodeUp(false);
			logger.error("Run: Node Down: " + node.getName());
			logger.error("Run: Sequence sent="+node.getHeartbeatSequenceSent() + 
				     " Sequence received=" + node.getHeartbeatSequenceReceived());
			listener.nodeDown (node.getName());
		    }
		}
	    }
	    threadSleep(refreshTime);
	    
	}
    }

    public synchronized void handleMessage(HeartbeatMessage message){
	String source = message.getSource();
	long seqNo = Long.parseLong(message.getHeartbeatSequence());
	logger.debug("HandleMessage: Received HeartbeatMessage from: "+source+" sequence no="+seqNo);
	if (nodeInfos.get(source) != null)
	    nodeInfos.get(source).setHeartbeatSequenceReceived(seqNo);
    }

    public synchronized void addNode(String nodeName){
	logger.info("AddNode: Adding node to monitor: "+nodeName);
	NodeInfo node = new NodeInfo (nodeName);
	node.setNodeUp(true);
	nodeInfos.put(nodeName, node);
    }

    public synchronized void removeNode(String nodeName){
	logger.info("RemoveNode: Removing node: "+nodeName);
	nodeInfos.remove(nodeName);
    }

    private void threadSleep(long sleepDuration){
	long startTime = System.currentTimeMillis();
	long timePassed;
	while ((timePassed = (System.currentTimeMillis() - startTime)) < sleepDuration){

	    //timePassed can be less than zero, if the system time has been decreased. so as minimal protection,
	    if (timePassed < 0)
		break;

	    try{
		
		Thread.sleep(sleepDuration - timePassed);
		
	    }catch (Exception e){
		logger.debug("ThreadSleep: Thread interrupted");
	    }
	}
    }

    class NodeInfo {
	protected boolean isNodeUp = false;
	protected String nodeName;
	protected long heartbeatSequenceSent = 0;
	protected long heartbeatSequenceReceived = 0;

	public NodeInfo (String nodeName){
	    this.nodeName = nodeName;
	}
	public String getName(){
	    return nodeName;
	}
	public boolean isNodeUp(){
	    return isNodeUp;
	}
	public void setNodeUp(boolean isNodeUp){
	    this.isNodeUp = isNodeUp;
	}
	public void setHeartbeatSequenceSent(long heartbeatSequence){
	    this.heartbeatSequenceSent = heartbeatSequence;
	}
	public long getHeartbeatSequenceSent(){
	    return heartbeatSequenceSent;
	}
	public void setHeartbeatSequenceReceived(long heartbeatSequence){
	    this.heartbeatSequenceReceived = heartbeatSequence;
	}
	public long getHeartbeatSequenceReceived(){
	    return heartbeatSequenceReceived;
	}

    }
}