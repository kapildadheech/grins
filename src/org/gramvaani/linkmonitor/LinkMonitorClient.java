package org.gramvaani.linkmonitor;

import org.gramvaani.radio.stationconfig.*;
import org.gramvaani.radio.rscontroller.messages.HeartbeatMessage;
import org.gramvaani.radio.rscontroller.RSController;
import org.gramvaani.utilities.*;

public class LinkMonitorClient implements Runnable {
    protected LinkMonitorClientListener listener;
    protected Thread thread;
    protected long refreshTime;
    protected boolean isAlive = true, isLinkUp = true;
    protected StationConfiguration stationConfig;
    protected String clientName;
    protected LogUtilities logger;
    protected long startTime;

    public LinkMonitorClient(LinkMonitorClientListener listener, String clientName, StationConfiguration stationConfig){
	this.listener = listener;
	this.stationConfig = stationConfig;
	this.clientName = clientName;
	refreshTime = stationConfig.getIntParam(StationConfiguration.CLIENT_REFRESH_TIME);	
	logger = new LogUtilities(clientName+":LinkMonitorClient");
	logger.debug("LinkMonitorClient: Initializing with client="+clientName+" refreshTime="+refreshTime);
	thread = new Thread(this, clientName+":LinkMonitorClient");
    }

    public void start(){
	logger.debug("Start: Starting LinkMonitorClient");
	thread.start();
    }

    public void reset() {
	isLinkUp = true;
	startTime = System.currentTimeMillis();
	thread.interrupt();
    }

    public void run (){
	while (true){
	    startTime = System.currentTimeMillis();
	    long timePassed;
	    while ((timePassed = (System.currentTimeMillis() - startTime)) < refreshTime){
		try{
		    
		    Thread.sleep(refreshTime - timePassed);
		
		}catch (Exception e){
		    logger.warn("Run: Thread Interrupted");
		}
	    }

	    if (isAlive){
		clearFlag();
		if(!isLinkUp){
		    isLinkUp = true;
		    listener.linkUp();
		}
		    
	    } else {
		if (isLinkUp){
		    isLinkUp = false;
		    listener.linkDown();
		}
		
	    }
	}
    }

    protected synchronized void clearFlag(){
	isAlive = false;
    }

    public synchronized void handleMessage(HeartbeatMessage message){
	isAlive = true;
	logger.debug("HandleMessage: Heart beat message received with sequence no "+message.getHeartbeatSequence());
	sendHeartbeat(message.getHeartbeatSequence());
    }

    public void sendHeartbeat(String heartbeatSequence){
	HeartbeatMessage message = new HeartbeatMessage (clientName, RSController.RESOURCE_MANAGER, heartbeatSequence);
	if (listener.sendHeartbeatMessage(message) == -1){
	    logger.debug("SendHeartbeat: Failed to send heartbeat message. Declaring the link as down.");
	    if(isLinkUp){
		isLinkUp = false;
		listener.linkDown();
	    }
	}
    }

    public boolean isLinkUp(){
	return isLinkUp;
    }

}