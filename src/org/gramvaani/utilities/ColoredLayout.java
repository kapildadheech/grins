package org.gramvaani.utilities;

import org.apache.log4j.*;
import org.apache.log4j.Priority;
import org.apache.log4j.spi.LoggingEvent;

import java.util.Date;

public class ColoredLayout extends SimpleLayout {

    static final String ATTRIB_NORMAL    = "00";
    static final String ATTRIB_BRIGHT    = "01";
    static final String ATTRIB_DIM       = "02";
    static final String ATTRIB_UNDERLINE = "03";
    static final String ATTRIB_BLINK     = "05";
    static final String ATTRIB_REVERSE   = "07";
    static final String ATTRIB_HIDDEN    = "08";
    
    static final String FG_BLACK   = "30";
    static final String FG_RED     = "31";
    static final String FG_GREEN   = "32";
    static final String FG_YELLOW  = "33";
    static final String FG_BLUE    = "34";
    static final String FG_MAGENTA = "35";
    static final String FG_CYAN    = "36";
    static final String FG_WHITE   = "37";
    
    static final String BG_BLACK   = "40";
    static final String BG_RED     = "41";
    static final String BG_GREEN   = "42";
    static final String BG_YELLOW  = "43";
    static final String BG_BLUE    = "44";
    static final String BG_MAGENTA = "45";
    static final String BG_CYAN    = "46";
    static final String BG_WHITE   = "47";
    
    static final String COLON  = ":";
    static final String HYPHEN = " - ";
    static final String NEWLINE= "\n";

    static final String normalAttribControlString;
    static final String consoleFgColor, consoleBgColor;

    static {
	String currentConsoleColors = System.getenv("COLORFGBG");

	if (currentConsoleColors != null){
	    consoleFgColor = currentConsoleColors.split(";")[0];
	    consoleBgColor = currentConsoleColors.split(";")[1];
	} else {
	    consoleFgColor = "30";
	    consoleBgColor = "47";
	}
	
	normalAttribControlString = getControlString(ATTRIB_NORMAL, consoleFgColor, consoleBgColor).toString();
    }
    
    public String format (LoggingEvent event){
	StringBuilder str = new StringBuilder();
	str.append(event.getTimeStamp());
	str.append(COLON);
	str.append(getLevelString(event));
	str.append(HYPHEN);
	str.append(event.getMessage());
	str.append(NEWLINE);
	return str.toString();
    }

    public String getHeader() {
	StringBuilder str = new StringBuilder();

	str.append("\n\n\n");
	str.append(getControlString(ATTRIB_NORMAL, FG_BLUE, levelBg));
	str.append("*****          Logging From ");
	long now = System.currentTimeMillis();
	str.append("(");
	str.append(now);
	str.append(") - ");
	str.append(new Date(now));
	str.append("          *****\n");
	str.append(normalAttribControlString);
	str.append("\n\n\n");

	return str.toString();
    }
    
    public String getFooter(){
	return null;
    }

    static final String levelBg = "01";

    private String getLevelString (LoggingEvent event){
	String levelFg = consoleFgColor;
	String levelAttrib = ATTRIB_NORMAL;

	switch(event.getLevel().toInt()){
	case Priority.DEBUG_INT:
	    levelAttrib = ATTRIB_DIM;
	    break;
	case Priority.INFO_INT:
	    levelFg = FG_GREEN;
	    break;
	case Priority.WARN_INT:
	    levelFg = FG_YELLOW;
	    break;
	case Priority.ERROR_INT:
	    levelFg = FG_RED;
	    break;
	case Priority.FATAL_INT:
	    levelFg = FG_RED;
	    break;
	}
	
	StringBuilder str = getControlString(levelAttrib, levelFg, levelBg);
	str.append(event.getLevel().toString());
	str.append(normalAttribControlString);
	
	return str.toString();
    }

    static final String BEGIN_ESC = "[";
    static final String SEMI_COLON= ";";
    static final String END_ESC   = "m";
    
    static StringBuilder getControlString (String attrib, String fgColor, String bgColor){
	StringBuilder str = new StringBuilder();
	str.append(BEGIN_ESC);
	str.append(attrib);
	str.append(SEMI_COLON);
	str.append(fgColor);
	str.append(SEMI_COLON);
	str.append(bgColor);
	str.append(END_ESC);
	return str;
    }

    

}
