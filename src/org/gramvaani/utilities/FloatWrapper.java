package org.gramvaani.utilities;

public class FloatWrapper {
    float value;

    public FloatWrapper(float value){
	this.value = value;
    }

    public float floatValue(){
	return value;
    }
}
