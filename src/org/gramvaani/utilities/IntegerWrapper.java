package org.gramvaani.utilities;

public class IntegerWrapper {
    int value;

    public IntegerWrapper(int value){
	this.value = value;
    }

    public int intValue(){
	return value;
    }
}
