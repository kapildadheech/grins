package org.gramvaani.utilities.processmonitor;

public interface ProcessMonitorListener {
    public void processExited (Process process, int exitValue);
    public void handleProcessError (Process process, String error);
}