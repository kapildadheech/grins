package org.gramvaani.utilities.processmonitor;

import java.io.*;

public class ProcessMonitor implements Runnable {
    Process process;
    ProcessMonitorListener listener;
    Thread thread, errorReader;
    boolean hasExited = false;
    boolean forcedExit = false;
    int exitValue;

    public ProcessMonitor(final String command, final ProcessMonitorListener listener) throws IOException{
	this.process = Runtime.getRuntime().exec(command);
	this.listener = listener;
	errorReader = new Thread (){
		public void run(){
		    BufferedReader bufReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		    String error;
		    try{
			while(true){
			    error = bufReader.readLine();
			    if (error == null)
				break;
			    else
				listener.handleProcessError(process, error);
			}
		    }catch (IOException e){
		    }
		}
	    };
	errorReader.start();
	thread = new Thread(this, "ProcessMonitor");
	thread.start();
    }

    public void run(){
	while(true){
	    try{
		exitValue = process.waitFor();
		break;
	    }catch (Exception e){
		listener.handleProcessError(process, "Run: Exception encountered: "+e.getMessage());
	    }
	}
	hasExited = true;
	if(listener != null && (!forcedExit)){
	    listener.processExited(process, exitValue);
	}
    }

    public boolean hasExited(){
	return hasExited;
    }

    public void destroy() {
	forcedExit = true;
	process.destroy();
    }
}