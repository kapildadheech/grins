package org.gramvaani.utilities;

import java.io.*;
import java.nio.channels.FileChannel;

import java.security.MessageDigest;

import java.beans.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.*;
import java.util.*;

public class FileUtilities {
    public static final String[] SUPPORTED_EXTNS = new String[] {"wav", "mp3"};

    public static boolean writeToXLS(String filePath, ArrayList<String> data, String separator, boolean containsHeading) {
	FileOutputStream out;
	try{
	    out = new FileOutputStream(filePath);
	} catch(Exception e) {
	    LogUtilities.getDefaultLogger().error("Error opening file " + filePath + " for writing.", e);
	    return false;
	}

	int columnCount = data.get(0).split(separator).length;

	Workbook wb = new HSSFWorkbook();
	Sheet s = wb.createSheet();

	Font hFont = wb.createFont();
	hFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
	CellStyle boldStyle = wb.createCellStyle();
	boldStyle.setFont(hFont);

	for(int rownum = 0; rownum < data.size(); rownum++) {
	    Row r = s.createRow(rownum);
	    String[] entry = data.get(rownum).split(separator);
	    
	    for(short cellnum = (short)0; cellnum < entry.length; cellnum++) {
		Cell c = r.createCell(cellnum);
		c.setCellValue(entry[cellnum]);
		if(rownum == 0 && containsHeading) {
		    c.setCellStyle(boldStyle);
		}
	    }
	}

	for(int i = 0; i < columnCount; i++) {
	    s.autoSizeColumn(i);
	}

	try{
	    wb.write(out);
	    out.close();
	} catch(Exception e) {
	    LogUtilities.getDefaultLogger().error("Error error writing to " + filePath, e);
	    return false;
	}

	return true;
    }

    public static int changeFilename(String oldFilename, String oldPath, String newFilename, String newPath) throws Exception {
	return changeFilename(oldPath + "/" + oldFilename, newPath + "/" + newFilename);
    }

    public static int changeFilename(String oldFilename, String newFilename) throws Exception {
	File file = new File(oldFilename);
	file.renameTo(new File(newFilename));
	return 0;
    }

    public static long copyFile(String srcFilename, String destFilename) {
	long retval;
	FileChannel srcChannel, dstChannel;

	try {
	    srcChannel = new FileInputStream(srcFilename).getChannel();
	    dstChannel = new FileOutputStream(destFilename).getChannel();
	    
	    retval = dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
	    srcChannel.close();
	    dstChannel.close();
	    
	    File destFile = new File(destFilename);
	    destFile.setReadable(true, false);
	    destFile.setWritable(true, false);

	    //retval = copyFileOrThrowException(srcFilename, destFilename);
	    return retval;
	} catch(Exception e) {
	    return -1;
	}
	
    }

    //Currently Java file copy seems to take a lot of cpu. To avoid causing link downs between RSAPP and RSC
    //we copy 1MB and then sleep 100ms. Eventually, when java nio is optimized for linux we expect the cpu
    //utilization to go down drammatically. This code should be changed to use java nio then.
    /*
      The method seems to have a memory leak somewhere. So not using it. 
      Java 1.6.0.24 seemed to not have the cpu problem observed at teri.

    public static int copyFileOrThrowException(String srcFilename, String destFilename) throws Exception {
	try{
	    File srcFile = new File(srcFilename);
	    File destFile = new File(destFilename);
	    
	    InputStream in = new BufferedInputStream(new FileInputStream(srcFile));
	    OutputStream out = new BufferedOutputStream(new FileOutputStream(destFile));
	    
	    byte[] buf = new byte[1024 * 1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
		out.write(buf, 0, len);
		try {
		    Thread.sleep(100);
		} catch (Exception e) {}
	    }
	    
	    destFile.setReadable(true, false);
	    destFile.setWritable(true, false);

	    in.close();
	    out.close();

	    return 0;
	} catch (Exception e) {
	    LogUtilities.getDefaultLogger().error("Unable to copy file: "+srcFilename+" to "+destFilename, e);
	    throw(e);
	}
    }
    */

    public static int writeToFile(String filePath, String[] text, boolean append){
	if (filePath == null) {
	    LogUtilities.getDefaultLogger().error("File path is null. text=" + text);
	    return -2;
	} else if (text == null) {
	    LogUtilities.getDefaultLogger().error("String to write is null. FilePath=" + filePath);
	    return -3;
	}

	try{
	    File file = new File(filePath);
	    PrintWriter out = new PrintWriter(new FileWriter(file, append));

	    for(int i=0; i<text.length; i++) {
		out.println(text[i]);
	    }

	    out.flush();
	    out.close();
	    return 0;
	} catch (Exception e){
	    LogUtilities.getDefaultLogger().error("Unable to write to file: "+filePath, e);
	    return -1;
	}
    }

    public static boolean isSupported(String fileName){
	fileName = fileName.toLowerCase();
	for (String extn: SUPPORTED_EXTNS)
	    if (fileName.endsWith(extn))
		return true;
	return false;
    }

    static byte[] getCheckSum(String algorithm, String fileName) {
	try{
	    InputStream fileStream = new FileInputStream(fileName);
	    byte[] buffer = new byte[1024];

	    MessageDigest digest = MessageDigest.getInstance(algorithm);
	    int read;
	    while ((read = fileStream.read(buffer)) > 0){
		digest.update(buffer, 0, read);
	    }
	    fileStream.close();
	    return digest.digest();
	} catch (Exception e){
	    LogUtilities.getDefaultLogger().error("Unable to calculate "+algorithm+" checksum of file: "+fileName, e);
	    return null;
	}
    }

    public static byte[] getMd5Sum(String filename){
	return getCheckSum("MD5", filename);
    }

    public static byte[] getSha1Sum(String filename){
	return getCheckSum("SHA-1", filename);
    }

    public static String getMd5Hex(String fileName) {
	return StringUtilities.bytesToHex(getMd5Sum(fileName));
    }

    public static String getSha1Hex(String fileName) {
	return StringUtilities.bytesToHex(getSha1Sum(fileName));
    }

    public static void setPermissions(String path, boolean recursive) {
	File fileFolder = new File(path);
	
	if(!fileFolder.exists()) {
	    LogUtilities.getDefaultLogger().error("SetFolderPermissions: " + path + " does not exist.");
	    return;
	}

	fileFolder.setReadable(true, false);
	fileFolder.setWritable(true, false);

	if(recursive && fileFolder.isDirectory()) {
	    fileFolder.setExecutable(true, false);
	    String[] children = fileFolder.list();
	    for(String child: children) {
		setPermissions(path + System.getProperty("file.separator") + child, true);
	    }
	}
    }

    public static void setNewFolderPermissions(javax.swing.JFileChooser fileChooser){
	fileChooser.addPropertyChangeListener(new PropertyChangeListener(){
		public void propertyChange(PropertyChangeEvent e){
		    if (!e.getPropertyName().equals("SelectedFileChangedProperty"))
			return;

		    if (e.getNewValue() == null)
			return;

		    String dirName = e.getNewValue().toString();
		    try{
			File file = new File(dirName);
			if (file.isDirectory() && dirName.indexOf("NewFolder") != -1) {
			    file.setReadable(true, false);
			    file.setWritable(true, false);
			}
		    } catch (Exception ex){
			LogUtilities.getDefaultLogger().error("SetNewFolderPermissions: Could not set for :"+dirName, ex);
		    }
		}
	    });
    }
}