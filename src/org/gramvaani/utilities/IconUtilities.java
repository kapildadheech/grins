package org.gramvaani.utilities;

import org.gramvaani.radio.stationconfig.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

public class IconUtilities {

    static StationConfiguration stationConfig;

    public static void setStationConfig(StationConfiguration stationConf) {
	stationConfig = stationConf;
    }

    public static ImageIcon getIcon(String iconID, int size) {
	return getIcon(iconID, size, size, true);
    }

    public static ImageIcon getIcon(String iconID, int width, int height) {
	return getIcon(iconID, width, height, true);
    }

    public static ImageIcon getIcon(String iconID, int width, int height, boolean preserveAspect) {
	if (!preserveAspect)
	    return new ImageIcon((new ImageIcon(stationConfig.getIconPath(iconID))).getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
	else {
	    ImageIcon origImageIcon = new ImageIcon(stationConfig.getIconPath(iconID));
	    int origHeight = origImageIcon.getIconHeight();
	    int origWidth = origImageIcon.getIconWidth();
	    double aspect = (double)origWidth / origHeight;
	    double resizeRatio = Math.max((double)origHeight / height, (double)origWidth / width);
	    int newHeight = (int)(origHeight / resizeRatio);
	    int newWidth = (int)(newHeight * aspect);
	    
	    return new ImageIcon(origImageIcon.getImage().getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH));
	}
    }

    public static ImageIcon getIcon(String iconID) {
	return new ImageIcon(stationConfig.getIconPath(iconID));
    }


}