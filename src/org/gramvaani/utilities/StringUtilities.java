package org.gramvaani.utilities;

import java.text.DateFormat;
import java.util.*;
import java.util.regex.*;

public class StringUtilities {

    public static String[] getArrayFromCSV(String csv) {
	if(csv == null || csv.trim().equals(""))
	    return new String[]{};
	StringTokenizer st = new StringTokenizer(csv, ",");
	ArrayList<String> a = new ArrayList<String>();
	while(st.hasMoreTokens()) {
	    a.add(st.nextToken().trim());
	}
	return a.toArray(new String[a.size()]);
    }

    public static long[] getLongArrayFromCSV(String csv) {
	String[] a = getArrayFromCSV(csv);
	long[] b = new long[a.length];
	int i = 0;
	for(String aa: a) {
	    b[i++] = Long.parseLong(aa);
	}
	return b;
    }

    public static <T extends Object> String getCSVFromArray(T[] a) {
	if (a == null || a.length == 0)
	    return "";
	StringBuilder str = new StringBuilder();
	for (Object o: a){
	    str.append(',');
	    str.append(o);
	}
	str.deleteCharAt(0);
	return str.toString();
    }

    public static String getCSVFromArray(String[] a, int startOffset, int endOffset) {
	if(a == null || a.length == 0 || startOffset >= a.length || endOffset > a.length)
	    return "";
	String resourceList = "";
	for(int i = startOffset; i < endOffset; i++) {
	    resourceList = resourceList + "," + a[i];
	}
	return resourceList.equals("") ? "" : resourceList.substring(1);
    }

    public static String getCSVFromArray(long[] a) {
	if(a == null || a.length == 0)
	    return "";
	String resourceList = "";
	for (long resource: a){
	    resourceList = resourceList + "," + (new Long(resource)).toString();
	}
	return resourceList.equals("") ? "" : resourceList.substring(1);
    }

    public static String getCSVFromIterable(Iterable iterable){
	if (iterable == null)
	    return "";
	
	StringBuilder str = new StringBuilder();
	
	for (Object object: iterable){
	    str.append(object);
	    str.append(",");
	}

	if(str.length() != 0)
	    str.deleteCharAt(str.length() - 1);

	return str.toString();
    }

    public static String joinFromIterable(Iterable iterable, String separator){
	if (iterable == null)
	    return "";
	if (separator == null)
	    separator = "";
	
	StringBuilder str = new StringBuilder();
	
	for (Object object: iterable){
	    str.append(object);
	    str.append(separator);
	}

	if (str.length() != 0)
	    str.delete(str.length() - separator.length(), str.length());

	return str.toString();
    }

    static final String commaEscapeChar = "@";
    static final String commaSubstitute = commaEscapeChar + ",";

    public static String getCSVFromHashtable(Hashtable<String,String> table){
	if (table == null || table.size() == 0)
	    return "";

	StringBuilder str = new StringBuilder();

	for (String key: table.keySet()){
	    str.append(key);
	    str.append(":");
	    str.append(table.get(key).replaceAll(",", commaSubstitute));
	    str.append(",");
	}

	return str.toString();
    }
    
    static final Pattern escapedCommaPattern = Pattern.compile("[^"+commaEscapeChar+"][,]");
    
    public static Hashtable<String, String> getHashtableFromCSV(String csv){
	Hashtable<String, String> table = new Hashtable<String, String>();
	if (csv == null || csv.length() == 0)
	    return table;
	
	Matcher escapedCommaMatcher = escapedCommaPattern.matcher(csv);

	int prevMatch = 0;
	while (escapedCommaMatcher.find()){
	    MatchResult result = escapedCommaMatcher.toMatchResult();

	    String tuple = csv.substring(prevMatch, result.end() - 1);
	    int sepPos = tuple.indexOf(":");
	    table.put(tuple.substring(0, sepPos), tuple.substring(sepPos+1, tuple.length()).replaceAll(commaSubstitute, ","));
	    prevMatch = result.end();
	}
	

	return table;
    }


    public static String getCSVFromBooleanHashtable(Hashtable<String,Boolean> table){
	if (table == null || table.size() == 0)
	    return "";

	StringBuilder str = new StringBuilder();

	for (String key: table.keySet()){
	    str.append(key);
	    str.append(":");
	    str.append((table.get(key)).toString());
	    str.append(",");
	}

	return str.toString();
    }

    public static Hashtable<String, Boolean> getBooleanHashtableFromCSV(String csv) {
	Hashtable<String, Boolean> table = new Hashtable<String, Boolean>();
	if (csv == null || csv.length() == 0)
	    return table;
	
	String[] tokens = csv.split(",");
	for(String token : tokens) {
	    String[] keyVal = token.split(":");
	    table.put(keyVal[0], new Boolean(keyVal[1]));
	}

	return table;
    }

    public static String[] sanitize(String[] strArr) {
	for(int i=0; i<strArr.length; i++) {
	    String str = sanitize(strArr[i]);
	}
	
	return strArr;
    }

    public static String sanitize(String str) {
	if(str == null || str.equals(""))
	    return "";
	str = str.trim();
	str = str.replaceAll("\\s+", " ");
	str = str.replaceAll("\'", "");
	str = str.replaceAll("\"", "");
	str = str.replaceAll("\\\\", ""); 
	return str;
    }

    public static String concat(String[] array, String separator){
	if (array == null || array.length == 0)
	    return "";
	StringBuilder builder = new StringBuilder(array[0]);
	
	for (int i = 1; i < array.length; i++){
	    builder.append(separator);
	    builder.append(array[i]);
	}

	return builder.toString();
    }

    public static String concat(String[] array){
	return concat(array, ", ");
    }

    static final String[] units = new String[] {"B", "KB", "MB", "GB"};
    static final int unitSize = 1024;

    public static String bytesToReadable(long memory){
	int i;
	long divisor = 1;

	for (i = 0; i < 4; i++){
	    if(memory / divisor >= unitSize){
		divisor *= unitSize;
	    } else {
		break;
	    }
	}

	if(i == 4)
	    i = 3;

	return Long.toString(memory/divisor) + units[i];
    }
    
    /*
    public static String kiloBytesToReadable(long memory){
	return bytesToReadable(memory * unitSize);
    }
    */

    static final String hexChars = "0123456789abcdef";

    public static String bytesToHex(byte[] byteArray){
	if (byteArray == null)
	    return null;

	StringBuilder str = new StringBuilder(2 * byteArray.length);

	for (byte b: byteArray){
	    str.append(hexChars.charAt((b & 0xF0) >> 4));
	    str.append(hexChars.charAt((b & 0x0F)));
	}
	
	return str.toString();
    }
    
    /*
    public static String lengthStringFromMillis(long millis){
	if (millis < 0)
	    return "0:0";
	int sec = (int)(millis/1000) % 60;
	int min = (int)(millis/1000/60) % 60;
	int hr  = (int)(millis/1000/3600);
	
	return hr+":"+min+":"+sec;
    }
    */

    public static String durationStringFromMillis(long millis){
	if (millis < 0)
	    return "0:00";
	
	int sec = (int)(millis/1000) % 60;
	int min = (int)(millis/1000/60) % 60;
	int hr  = (int)(millis/1000/3600);

	String hrStr, minStr, secStr;

	secStr = String.format("%02d", sec);
	if (hr < 1) {
	    minStr = String.format("%2d", min);
	    return minStr+":"+secStr;
	} else {
	    return hr + ":" + String.format("%02d", min) + ":" + secStr;
	}
    }

    public static String getDateTimeString(long time){
	String dateTime = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(new Date(time));
	return dateTime;
    }

    //Windows doesn't support filenames containing certain characters.

    public static String sanitizeFilename(String fileName){
	return fileName.replace(":", "-").replace("/", "-");
    }

    public static boolean isAlphaNumeric(String string) {
	String regex = "[a-zA-Z0-9]*";
	return string.matches(regex);
    }

    public static boolean isAlphaNumericSpace(String string) {
	String regex = "[a-zA-Z0-9\\s]*";
	return string.matches(regex);
    }

    public static String lTrim(String string) {
	return string.replaceAll("^\\s+","");
    }

    public static String rTrim(String string) {
	return string.replaceAll("\\s+$","");
    }

    public static String trim(String string) {
	string = lTrim(string);
	return rTrim(string);
    }
}