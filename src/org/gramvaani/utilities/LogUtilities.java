package org.gramvaani.utilities;

import org.apache.log4j.*;
import org.apache.log4j.Priority;
import org.apache.log4j.spi.LoggingEvent;

import java.util.*;
import java.io.File;

public class LogUtilities {

    protected static Logger staticLogger;
    protected static LogUtilities defaultLogger;
    protected static Level defaultLevel;
    protected static Hashtable<String, Level> logLevels;
    
    protected static final int MAX_LOG_FILE_COUNT = 14;

    protected Logger logger;
    protected String name;
    protected Level level;

    static {
	staticLogger = Logger.getLogger("grins");
	ConsoleAppender consoleAppender = null;

	consoleAppender = new ConsoleAppender(new ColoredLayout(), "System.err");
	staticLogger.addAppender(consoleAppender);
	String logLevelString = System.getenv("GV_LOG");

	logLevels = new Hashtable<String, Level>();
	processLogLevel(logLevelString);
	
	staticLogger.setLevel(defaultLevel);
    }
    

    protected static void processLogLevel(String logLevelString){
	if (logLevelString == null || logLevelString.equals("")){
	    defaultLevel = Level.INFO;
	    return;
	}

	String entries[] = logLevelString.split(",");

	defaultLevel = Level.toLevel(entries[0]);

	for (int i = 1; i < entries.length; i++){
	    String entry[] = entries[i].split("=");
	    logLevels.put(entry[0].toLowerCase(), Level.toLevel(entry[1]));
	}
    }


    public LogUtilities (){
	logger = staticLogger;
	name = "";
	level = lookupLevel(name);
    }

    public LogUtilities (String name){
	if (name != null)
	    this.name = name + ": ";
	else
	    this.name = "";
	level = lookupLevel(name);
	logger = staticLogger;
    }

    protected Level lookupLevel(String name){
	if (name.length() == 0)
	    return defaultLevel;

	Level retVal = logLevels.get(name.toLowerCase());

	if (retVal == null)
	    return defaultLevel;
	else
	    return retVal;
    }

    protected synchronized void setLevel(){
	logger.setLevel(level);
    }

    protected void setDefaultLevel(){
	logger.setLevel(defaultLevel);
    }

    public void debug (String message){
	setLevel();
	logger.debug(name + message);
	setDefaultLevel();
    }
 
    public void debug (String message, Throwable t){
	setLevel();
	logger.debug(name + message, t);
	setDefaultLevel();
    }

    public void info (String message){
	setLevel();
	logger.info(name + message);
	setDefaultLevel();
    }

    public void warn (String message){
	setLevel();
	logger.warn(name + message);
	setDefaultLevel();
    }

    public void warn (String message, Throwable t){
	setLevel();
	logger.warn(name + message, t);
	setDefaultLevel();
    }

    public void error (String message){
	setLevel();
	logger.error(name + message);
	setDefaultLevel();
    }
    
    public void error (String message, Throwable t){
	setLevel();
	logger.error(name + message, t);
	setDefaultLevel();
    }

    public void fatal (String message){
	setLevel();
	logger.fatal(name + message);
	setDefaultLevel();
    }

    public void fatal (String message, Exception e){
	setLevel();
	logger.fatal(name + message, e);
	setDefaultLevel();
    }

    public synchronized void setName (String name){
	if (name != null)
	    this.name = name+": ";

	level = lookupLevel(name);
    }

    public static LogUtilities getDefaultLogger (){
	if (defaultLogger == null)
	    defaultLogger = new LogUtilities();
	return defaultLogger;
    }
    
    public static void logToFile(String filename, String maxFileSize) {
	if (filename == null)
	    if (staticLogger != null) {
		staticLogger.error("LogToFile: null filename");
		return;
	    }

	filename = filename + ".log";

	String logsDir = System.getenv("GRINS_LOG_PATH");

	if (logsDir == null) //in case of tomcat
	    logsDir = System.getProperty("GRINS_LOG_PATH");

	if (logsDir != null && logsDir.length() > 0 && (new File(logsDir)).exists()) {
	    filename = logsDir + File.separator + filename;
	}

	try {
	    RollingFileAppender fileAppender = new RollingFileAppender(new ColoredLayout(), filename);
	    fileAppender.setMaxFileSize(maxFileSize);
	    fileAppender.setMaxBackupIndex(LogUtilities.MAX_LOG_FILE_COUNT);
	    
	    if (staticLogger != null)
		staticLogger.addAppender(fileAppender);
	} catch (Exception e){
	    System.err.println("LogToFile: Unable to open logfile: "+filename);
	    e.printStackTrace(System.err);
	}
    }

    /*
    public static void addShutdownHook(){
	Runtime.getRuntime().addShutdownHook(new Thread(){
		public void run(){
		    LogManager.shutdown();
		}
	    });
    }
    */

}