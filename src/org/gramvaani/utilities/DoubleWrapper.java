package org.gramvaani.utilities;

public class DoubleWrapper {
    double value;

    public DoubleWrapper(double value){
	this.value = value;
    }

    public double doubleValue(){
	return value;
    }
}