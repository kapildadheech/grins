package org.gramvaani.utilities;

import java.util.UUID;

public class GUIDUtils {

    public static String getGUID(){
	return UUID.randomUUID().toString();
    }
}