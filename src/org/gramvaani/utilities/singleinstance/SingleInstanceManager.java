package org.gramvaani.utilities.singleinstance;

import org.gramvaani.utilities.LogUtilities;

import java.net.*;
import java.io.*;

public class SingleInstanceManager {

    private static SingleInstanceListener subListener;

    //public static final int SINGLE_INSTANCE_NETWORK_SOCKET = 64165;

    // Must end with newline 
    public static final String SINGLE_INSTANCE_SHARED_KEY = "$$RSAppInstance$$\n";

    static final int minSocketID = 2048;

    public static boolean registerInstance(String key) {
	final LogUtilities logger = new LogUtilities("SingleInstanceManager");


        // returnValueOnError should be true if lenient (allows app to run on network error) or false if strict.
        boolean returnValueOnError = true;

	final int socketID = (Math.abs(key.hashCode()) % (65535 - minSocketID)) + minSocketID;

        try {
            final ServerSocket socket = new ServerSocket(socketID, 10, InetAddress
                    .getLocalHost());
            logger.info("Listening for application instances on socket " + socketID);

            Thread instanceListenerThread = new Thread(new Runnable() {
                public void run() {
                    boolean socketClosed = false;
                    while (!socketClosed) {
                        if (socket.isClosed()) {
                            socketClosed = true;
                        } else {
                            try {
                                Socket client = socket.accept();
                                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                                String message = in.readLine();
                                if (SINGLE_INSTANCE_SHARED_KEY.trim().equals(message.trim())) {
                                    logger.debug("Shared key matched - new application instance found at: "+socketID);
                                    fireNewInstance();
                                }
                                in.close();
                                client.close();
                            } catch (IOException e) {
                                socketClosed = true;
                            }
                        }
                    }
                }
            });
            instanceListenerThread.start();
            // listen
        } catch (UnknownHostException e) {
            logger.error(e.getMessage(), e);
            return returnValueOnError;
        } catch (IOException e) {
            logger.debug("Port "+socketID+" is already taken.  Notifying first instance.");
            try {
                Socket clientSocket = new Socket(InetAddress.getLocalHost(), socketID);
                OutputStream out = clientSocket.getOutputStream();
                out.write(SINGLE_INSTANCE_SHARED_KEY.getBytes());
                out.close();
                clientSocket.close();
                logger.debug("Successfully notified first instance.");
                return false;
            } catch (UnknownHostException e1) {
                logger.error(e.getMessage(), e);
                return returnValueOnError;
            } catch (IOException e1) {
                logger.error("Error connecting to local port for single instance notification");
                logger.error(e1.getMessage(), e1);
                return returnValueOnError;
            }

        }
        return true;
    }

    public static void setSingleInstanceListener(SingleInstanceListener listener) {
        subListener = listener;
    }

    private static void fireNewInstance() {
	if (subListener != null) {
	    subListener.newInstanceCreated();
	}
    }

    public interface SingleInstanceListener {
	public void newInstanceCreated();
    }

}

