#!/bin/bash

tomcathome=$1;
tomcatconf=$2;
gramvaanihome=$3;

mkdir -p $tomcathome/webapps/gramvaani/WEB-INF
cp -r $gramvaanihome/classes $tomcathome/webapps/gramvaani/WEB-INF/.
cp -r $gramvaanihome/lib $tomcathome/webapps/gramvaani/WEB-INF/.
cp $gramvaanihome/web.xml $tomcathome/webapps/gramvaani/WEB-INF/.
cp $gramvaanihome/gramvaani.xml $tomcatconf/Catalina/localhost/gramvaani.xml
cp $gramvaanihome/automation.conf $tomcathome/webapps/gramvaani/.


